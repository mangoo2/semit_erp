
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonal` int(11) NOT NULL,
  `asunto` varchar(255) NOT NULL,
  `mensaje` text NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` tinyint(4) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `tipoticket` tinyint(4) NOT NULL,
  `servidor` varchar(255) NOT NULL,
  `personalmangoo` int(11) NOT NULL,
  `fecharespuesta` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ticket_file`;
CREATE TABLE IF NOT EXISTS `ticket_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idticket` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `codigo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `ticket_respuesta`;
CREATE TABLE IF NOT EXISTS `ticket_respuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idticket` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `mensaje` text NOT NULL,
  `tipo` tinyint(1) NOT NULL COMMENT '1-administrador, 0-personal semit',
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ticket_respuesta_file`;
CREATE TABLE IF NOT EXISTS `ticket_respuesta_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idticket` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `codigo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
