-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 17-02-2025 a las 16:35:39
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semitmx_adminsys4`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sol_des`
--

DROP TABLE IF EXISTS `sol_des`;
CREATE TABLE IF NOT EXISTS `sol_des` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idsucursal` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `cant` int(11) NOT NULL,
  `idproduto` int(11) DEFAULT NULL,
  `name` varchar(300) NOT NULL,
  `tipo_descuento` int(11) NOT NULL,
  `s_mont_por` int(11) NOT NULL,
  `s_mont_efe` decimal(10,2) NOT NULL,
  `s_mont_final` decimal(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 en espera 1 autorizado 2 rechazado',
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
