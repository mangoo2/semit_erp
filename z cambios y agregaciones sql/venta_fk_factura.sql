-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-09-2023 a las 12:19:07
-- Versión del servidor: 5.7.23-23
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semitmx_adminsys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_fk_factura`
--

CREATE TABLE `venta_fk_factura` (
  `id` bigint(20) NOT NULL,
  `idventa` bigint(20) NOT NULL,
  `idfactura` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `venta_fk_factura`
--



--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `venta_fk_factura`
--
ALTER TABLE `venta_fk_factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factura_fk_venta` (`idventa`),
  ADD KEY `factura_fk_idfactura` (`idfactura`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `venta_fk_factura`
--
ALTER TABLE `venta_fk_factura`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `venta_fk_factura`
--
ALTER TABLE `venta_fk_factura`
  ADD CONSTRAINT `factura_fk_idfactura` FOREIGN KEY (`idfactura`) REFERENCES `f_facturas` (`FacturasId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `factura_fk_venta` FOREIGN KEY (`idventa`) REFERENCES `venta_erp` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
