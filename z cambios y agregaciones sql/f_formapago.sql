-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-09-2023 a las 13:29:50
-- Versión del servidor: 5.7.23-23
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semitmx_adminsys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_formapago`
--

CREATE TABLE `f_formapago` (
  `id` int(11) NOT NULL,
  `clave` varchar(2) NOT NULL,
  `formapago` varchar(50) NOT NULL,
  `formapago_text` text NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_formapago`
--

INSERT INTO `f_formapago` (`id`, `clave`, `formapago`, `formapago_text`, `activo`) VALUES
(1, '01', 'Efectivo', '01 Efectivo', 1),
(2, '02', 'ChequeNominativo', '02 Cheque nominativo', 1),
(3, '03', 'TransferenciaElectronicaFondos', '03 Transferencia electrónica de fondos', 1),
(4, '04', 'TarjetasDeCredito', '04 Tarjetas de Crédito', 1),
(5, '05', 'MonederoElectronico', '05 Monedero Electrónico', 1),
(6, '06', 'DineroElectronico', '06 Dinero Electrónico', 1),
(7, '08', 'ValesDeDespensa', '08 Vales de despensa', 1),
(8, '12', 'DacionPago', '12 Dación en pago', 1),
(9, '13', 'PagoSubrogacion', '13 Pago por subrogación', 1),
(10, '14', 'PagoConsignacion', '14 Pago por consignación', 1),
(11, '15', 'Condonacion', '15 Condonación', 1),
(12, '17', 'Compensacion', '17 Compensación', 1),
(13, '23', 'Novacion', '23 Novación', 1),
(14, '24', 'Confusion', '24 Confusión', 1),
(15, '25', 'RemisionDeuda', '25 Remisión de deuda', 1),
(16, '26', 'PrescripcionoCaducidad', '26 Prescripción o caducidad', 1),
(17, '27', 'SatisfaccionAcreedor', '27 A satisfacción del acreedor', 1),
(18, '28', 'TarjetaDebito', '28 Tarjeta de Débito', 1),
(19, '29', 'TarjetaServicio', '29 Tarjeta de Servicio', 1),
(20, '30', 'AplicacionAnticipos', '30 Aplicación de anticipos', 1),
(21, '99', 'PorDefinir', '99 Por definir', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_formapago`
--
ALTER TABLE `f_formapago`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_formapago`
--
ALTER TABLE `f_formapago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
