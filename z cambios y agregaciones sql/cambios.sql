ALTER TABLE `clientes` ADD `user` TINYINT NULL DEFAULT '1' AFTER `pais`;

CREATE TABLE `f_regimenfiscal` (
  `id` int(11) NOT NULL,
  `clave` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `f_regimenfiscal`
--

INSERT INTO `f_regimenfiscal` (`id`, `clave`, `descripcion`, `activo`) VALUES
(1, 601, 'General de Ley Personas Morales', 1),
(2, 603, 'Personas Morales con Fines no Lucrativos', 1),
(3, 605, 'Sueldos y Salarios e Ingresos Asimilados a Salarios', 1),
(4, 606, 'Arrendamiento', 1),
(5, 607, 'Régimen de Enajenación o Adquisición de Bienes', 1),
(6, 608, 'Demás ingresos', 1),
(7, 609, 'Consolidación', 1),
(8, 610, 'Residentes en el Extranjero sin Establecimiento Permanente en México', 1),
(9, 611, 'Ingresos por Dividendos (socios y accionistas)', 1),
(10, 612, 'Personas Físicas con Actividades Empresariales y Profesionales', 1),
(11, 614, 'Ingresos por intereses', 1),
(12, 615, 'Régimen de los ingresos por obtención de premios', 1),
(13, 616, 'Sin obligaciones fiscales', 1),
(14, 620, 'Sociedades Cooperativas de Producción que optan por diferir sus ingresos', 1),
(15, 621, 'Incorporación Fiscal', 1),
(16, 622, 'Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras', 1),
(17, 623, 'Opcional para Grupos de Sociedades', 1),
(18, 624, 'Coordinados', 1),
(19, 625, 'Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas', 1),
(20, 626, 'Régimen Simplificado de Confianza', 1),
(21, 628, 'Hidrocarburos', 1),
(22, 629, 'De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales', 1),
(23, 630, 'Enajenación de acciones en bolsa de valores', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_regimenfiscal`
--
ALTER TABLE `f_regimenfiscal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_regimenfiscal`
--
ALTER TABLE `f_regimenfiscal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;




ALTER TABLE `cliente_fiscales` ADD `direccion` TEXT NULL DEFAULT NULL AFTER `correo`;

ALTER TABLE `clientes` CHANGE `usuario` `usuario` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `contrasena` `contrasena` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
