-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-09-2023 a las 13:31:19
-- Versión del servidor: 5.7.23-23
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semitmx_adminsys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_erp`
--

CREATE TABLE `venta_erp` (
  `id` bigint(20) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `folio` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `id_razonsocial` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `descuento` decimal(10,2) NOT NULL,
  `iva` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `facturar` tinyint(1) NOT NULL DEFAULT '0',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_erp_detalle`
--

CREATE TABLE `venta_erp_detalle` (
  `id` bigint(20) NOT NULL,
  `idventa` bigint(20) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `precio_unitario` decimal(10,2) NOT NULL,
  `descuento` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_erp_formaspagos`
--

CREATE TABLE `venta_erp_formaspagos` (
  `id` bigint(20) NOT NULL,
  `idventa` bigint(20) NOT NULL,
  `formapago` varchar(3) COLLATE utf8_unicode_ci NOT NULL COMMENT 'se metera la clave de la tabla f_formapago',
  `monto` decimal(10,2) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `venta_erp`
--
ALTER TABLE `venta_erp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta_erp_detalle`
--
ALTER TABLE `venta_erp_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `v_erp_detalle` (`idventa`),
  ADD KEY `v_erp_producto` (`idproducto`);

--
-- Indices de la tabla `venta_erp_formaspagos`
--
ALTER TABLE `venta_erp_formaspagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `v_erp_formpagos` (`idventa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `venta_erp`
--
ALTER TABLE `venta_erp`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta_erp_detalle`
--
ALTER TABLE `venta_erp_detalle`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta_erp_formaspagos`
--
ALTER TABLE `venta_erp_formaspagos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `venta_erp_detalle`
--
ALTER TABLE `venta_erp_detalle`
  ADD CONSTRAINT `v_erp_detalle` FOREIGN KEY (`idventa`) REFERENCES `venta_erp` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `v_erp_producto` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_erp_formaspagos`
--
ALTER TABLE `venta_erp_formaspagos`
  ADD CONSTRAINT `v_erp_formpagos` FOREIGN KEY (`idventa`) REFERENCES `venta_erp` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
