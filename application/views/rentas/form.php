<style type="text/css">
    select option[disabled] {background-color:#cecbcb}
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10"> 
              <h3 style="color: #1770aa;">Nueva renta</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Rentas" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <form id="form_datos" method="post">
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
            <div class="row">
              <div class="col-md-2" style="display: none;">
                <label>Factura</label>
                <span class="switch switch-icon">
                  <label>
                    <input type="checkbox" name="requiere_factura" id="requiere_factura" value="0" <?php if(isset($ren) && $ren->requiere_factura=="1") echo "checked"; ?>>
                    <span></span>
                  </label>
                </span>
              </div>
              <!--<div class="col-md-2">
                <a style="cursor: pointer;" onclick="modal_user()"><img style="width: 45px" src="<?php echo base_url() ?>public/img/ventasp/64.png" title="Cliente Nuevo"></a>
                <a style="cursor: pointer;" onclick="buscar_cliente()"><img style="width: 45px" src="<?php echo base_url() ?>public/img/ventasp/63.png" title="Editar Cliente"></a>
              </div> -->
            </div>
            <div class="col-lg-12 mb-1"><br></div> 
            <div class="row"> 
              <div class="col-md-4">
                <label>Cliente</label>
                <select id="id_cliente" name="id_cliente" class="form-select btn-pill digits">
                    <?php if(isset($ren) && $ren->id_cliente>0) {
                        echo '<option value="'.$ren->id_cliente.'">'.$ren->nombre.'</option>';
                    } ?>
                </select>
              </div>
              <div class="col-lg-4">
                <label>Servicio</label>
                <select <?php if($id>0) echo "disabled"; ?> id="id_servicio" name="id_servicio" class="form-select btn-pill digits" >
                  <?php if(isset($ren) && $ren->id_servicio>0) {
                    echo '<option value="'.$ren->id_servicio.'">'.$ren->descripcion.'</option>';
                  }?>
                </select>
              </div>
              <div class="col-lg-4">
                <input type="hidden" name="id_prod" id="id_prod" value="0">
                <input type="hidden" id="concentra" value="0">
                <label>Equipos</label><!--<button type="button" class="btn btn-primary" onclick="modal_series()"><i class="fas fa-eye"></i></button>-->
                <select <?php if($id>0) echo "disabled"; ?> id="id_serie" name="id_serie" class="form-select btn-pill digits" >
                  <?php if(isset($ren) && $ren->id_serie>0) {
                    echo '<option value="'.$ren->id_serie.'">'.$ren->serie.'</option>';
                  }?>
                </select>
              </div>
            </div>  
            <div class="col-lg-12 mb-1"><br></div> 
            <div class="row">           
              <div class="col-lg-2">
                <label>Periodo</label>
                <select id="periodo" name="periodo" class="form-select btn-pill digits" >
                    <option value="0"></option>
                    <option <?php if(isset($ren) && $ren->periodo=="1") echo "selected"; ?> value="1">1 día</option>
                    <option <?php if(isset($ren) && $ren->periodo=="2") echo "selected"; ?> value="2">15 días</option>
                    <option <?php if(isset($ren) && $ren->periodo=="3") echo "selected"; ?> value="3">30 días</option>
                </select>
              </div>
              
              <div class="col-lg-2">
                <label>Precio</label>
                <input readonly type="text" name="costo" id="costo" class="form-control" value="<?php if(isset($ren)) echo $ren->costo; ?>">
              </div>
              <div class="col-lg-2">
                <label>Deja documentos</label>
                <select id="deja_docs" name="deja_docs" class="form-select btn-pill digits" >
                  <!--<option value=""></option>-->
                  <option <?php if(isset($ren) && $ren->deja_docs==0){ echo "selected"; } ?> value="0">No</option>
                  <option <?php if(isset($ren) && $ren->deja_docs==1){ echo "selected"; } ?> value="1">Si</option>
                </select>
              </div>
              <div class="col-lg-2">
                <label>Fecha inicio</label>
                <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control" value="<?php if(isset($ren)) echo $ren->fecha_inicio; else echo date("Y-m-d") ?>">
              </div>
              <div class="col-lg-2">
                <label>Hora inicio</label>
                <input readonly type="time" name="hora_inicio" id="hora_inicio" class="form-control" value="<?php if(isset($ren)) echo $ren->hora_inicio; else echo date("H:i"); ?>">
              </div>
            </div>
            <div class="col-lg-12 mb-1"><br></div> 
            <div class="row">           
                <div class="col-lg-2"></div>
                <div class="col-lg-2" id="cont_depo">
                    <label>Depósito</label>
                    <input readonly type="text" name="deposito" id="deposito" class="form-control" value="<?php if(isset($ren)) echo $ren->deposito; ?>">
                </div>
                <div class="col-lg-2">
                    <label>Fecha fin</label>
                    <input readonly type="date" name="fecha_fin" id="fecha_fin" class="form-control" value="<?php if(isset($ren)) echo $ren->fecha_fin; ?>">
                </div>
                <div class="col-lg-2">
                    <label>Hora fin</label>
                    <input readonly type="time" name="hora_fin" id="hora_fin" class="form-control" value="<?php if(isset($ren)) echo $ren->hora_fin; else echo date("H:i") ?>">
                </div>
            </div>
            <div class="col-lg-12 mb-1"><br></div> 
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-2">
                    <label>Costo flete entrega</label>
                    <input type="number" min="0" name="costo_entrega" id="costo_entrega" class="form-control" value="<?php if(isset($ren)) echo $ren->costo_entrega; ?>">
                </div>
                <div class="col-lg-2">
                    <label>Costo flete recolección</label>
                    <input readonly type="number" min="0" name="costo_recolecta" id="costo_recolecta" class="form-control" value="<?php if(isset($ren)) echo $ren->costo_recolecta; ?>">
                </div>
                <div class="col-lg-2" id="cont_depo">
                    <label>Monto Total</label>
                    <input readonly type="text" id="total" class="form-control" value="<?php if(isset($ren)) echo ($ren->deposito+$ren->costo+$ren->costo_entrega+$ren->costo_recolecta); ?>">
                </div>
            </div>
            <div class="col-lg-12 mb-1"><br></div> 
            <div class="col-md-12 form-group d-flex cont_docs">
              <div class="col-md-6 cont_docs">
                <h4>INE (ambos lados)</h4><hr>
                <input class="form-control" type="hidden" id="id_file" value="<?php if(isset($ine)) echo $ine->id; ?>">
                <input class="form-control" type="hidden" id="file_aux" value="<?php if(isset($ine)) echo $ine->documento; ?>">
                <input class="form-control" type="file" name="doc_ine" id="doc_ine" accept=".pdf,.png,.jpg,.jpeg,.bmp">
              </div>
            
              <div class="col-md-6 cont_docs">
                <h4>Comprobante de Domicilio</h4><hr>
                <input class="form-control" type="hidden" id="id_file2" value="<?php if(isset($comp)) echo $comp->id; ?>">
                <input class="form-control" type="hidden" id="file_aux2" value="<?php if(isset($comp)) echo $comp->documento; ?>">
                <input class="form-control" type="file" name="doc_comp" id="doc_comp" accept=".pdf,.png,.jpg,.jpeg,.bmp">
              </div>
            </div>
        </form>  
        <div class="row"> 
          <div class="col-md-12"><br></div>
          <div class="col-md-12" align="center">
            <button class="btn btn-primary btn_registro" type="button" style="width: 30%;" onclick="save()">Guardar</button>                      
          </div>
        </div>
      </div>
    </div>
</div>    

<div class="modal fade" id="modal_user_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cliente nuevo</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row g-3">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="icon-tab" role="tablist">
                  <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#icon-home" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>Datos generales</a></li>
                  <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#profile-icon" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Datos fiscales</a></li>
                  <li class="nav-item"><a class="nav-link" id="pagos-icon-tab" data-bs-toggle="tab" href="#pagos-icon" role="tab" aria-controls="pagos-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Condiciones de pago</a></li>
                  <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-bs-toggle="tab" href="#contact-icon" role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Sesión en pagína web</a></li>
                </ul>
                
                <div class="tab-content" id="icon-tabContent">
                  <div class="tab-pane fade show active" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">
                    <br>
                    <form id="form_registro_datos" method="post">
                        <input type="hidden" id="idreg1" name="id">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nombre del cliente</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="nombre" id="razon_social">
                                    </div>
                                </div>
                            </div>

                        </div>    

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Correo electrónico</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()">
                                    </div>   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Calle y número</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="calle">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Código postal</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="cp" id="codigo_postal" oninput="cambiaCP2()">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Colonia</label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="colonia" name="colonia" onchange="regimefiscal()" required>
                                            <option value="0" selected disabled>Seleccione</option>
                                            
                                        </select>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Saldo</label>
                                    <div class="col-lg-5"><input class="form-control" type="number" name="saldo" id="saldo">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Suspender</label>
                                    <div class="col-lg-5">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input type="checkbox" name="desactivar" id="desactivar" onclick="activar_campo()">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>   
                                </div>
                            </div>
                            <?php 
                                $motivo_sty='style="display: none;"';
                            ?>
                            <div class="col-lg-12">
                                <div class="motivo_txt" <?php echo $motivo_sty ?>>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Motivo</label>
                                        <div class="col-lg-5">
                                            <textarea class="form-control" rows="3" name="motivo"></textarea>
                                        </div>   
                                    </div>
                                </div>    
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Estado</label>
                                    <div class="col-lg-5">
                                        <select name="estado" id="estado" class="form-control">
                                          <?php foreach ($estadorow->result() as $item) { ?>
                                          <option value="<?php echo $item->id;?>"><?php echo $item->estado;?></option>
                                          <?php } ?> 
                                        </select>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Municipio</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="municipio">
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos()">Guardar</button>                      
                  </div>
                
                  <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
                    <br>
                    <form id="form_registro_datos_fiscales" method="post">
                        <input type="hidden" id="idreg2" name="id">

                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Activar datos fiscales</label>
                                <span class="switch switch-icon">
                                <label>
                                    <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscales" >
                                    <span></span>
                                </label>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Razón social</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" id="razon_social_modal" name="razon_social" onchange="validar_razon_social()">
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">RFC</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="rfc" id="rfc" onchange="validar_rfc_cliente()">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">CP</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="cp">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Régimen fiscal</label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()">
                                            <option value="0" selected disabled>Seleccione</option>
                                            <?php foreach ($get_regimen->result() as $item) {
                                                    echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>'; 
                                            }?>
                                        </select>
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Uso de CFDI</label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="uso_cfdi" name="uso_cfdi">
                                            <option value="0" selected disabled>Seleccione</option>
                                            <?php foreach ($get_cfdi->result() as $item) {
                                                echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                                            }?>
                                        </select>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos_fiscales()">Guardar</button>        
                  </div>
                  <div class="tab-pane fade" id="pagos-icon" role="tabpanel" aria-labelledby="pagos-icon-tab">
                    <br>
                    <form id="form_registro_pagos" method="post">
                        <input type="hidden" id="idreg4" name="id">
                        <div class="row">
                            <?php 
                                $dias_saldo1='style="display:none"'; 
                                $dias_saldo2='style="display:none"'; 
                             ?>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En días</label>
                                    <div class="col-lg-1">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input type="radio" name="dias_saldo" id="dias_saldo" value="1" onclick="tipo_pago(1)">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>   
                                    <div class="col-lg-2 diaspagotxt" <?php echo $dias_saldo1 ?>>
                                        <select class="form-control" id="diaspago" name="diaspago" required>
                                            <option value="0" selected disabled>Seleccionar días</option>
                                            <option value="15">15</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div> 
                                    <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En saldo</label>
                                    <div class="col-lg-1">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input type="radio" name="dias_saldo" id="dias_saldo" value="2" onclick="tipo_pago(2)">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>  
                                    <div class="col-lg-2 saldopagotxt" <?php echo $dias_saldo2 ?>> 
                                        <input class="form-control" type="number" id="saldopago" name="saldopago">
                                    </div>  
                                </div>
                            </div>
                        </div>    
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_pagos()">Guardar</button>        
                  </div>
                  <div class="tab-pane fade" id="contact-icon" role="tabpanel" aria-labelledby="contact-icon-tab">
                    <br>
                    <form id="form_registro_usuario" method="post">
                        <input type="hidden" id="idreg3" name="id">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Contraseña</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password">
                                    </div>  
                                    <div class="col-5">
                                        <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                            <i class="icon-xl fas fa-eye" style="color: white"></i>
                                        </a>
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password">
                                    </div>   
                                </div>
                            </div> 
                        </div>    
                    </form>
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_usuario()">Guardar</button>
                  </div>  
                
                </div>
                
            </div>
            <?php /* 
            <form class="form" id="form_cliente">
                <div class="col-md-12">
                    <label class="form-label">Razón social</label>
                    <input class="form-control" type="text" id="razon_social" name="razon_social" required oninput="validar_razon_social()">
                </div>
                <div class="col-md-12">
                    <label class="form-label">RFC</label>
                    <input class="form-control" type="text" name="rfc" id="rfc" required oninput="validar_rfc_cliente()">
                </div>
                <div class="col-md-12">
                    <label class="form-label">CP</label>
                    <input class="form-control" type="text" name="cp" id="cp" required>
                </div>  
                <div class="col-md-12">
                    <label class="form-label">Régimen fiscal</label>
                    <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()" required>
                        <option value="0" selected disabled>Seleccione</option>
                        <?php foreach ($get_regimen->result() as $item) {

                            echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                        }?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Uso de CFDI</label>
                    <select class="form-control" id="uso_cfdi" name="uso_cfdi" required>
                        <option value="0" selected disabled>Seleccione</option>
                        <?php foreach ($get_cfdi->result() as $item) {
                            echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>';
                        }?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Correo electrónico</label>
                    <input class="form-control" type="email" name="correo" id="correo" required>
                </div>
                <div class="col-md-12" align="center">
                    <a class="btn" style="background: #009bdb; color: white" type="button" onclick="adddir()">Agregar dirección</a>
                </div>
                <div class="col-md-12 div_form_direccion" style="display:none;">
                    <label class="form-label">Dirección</label>
                    <textarea class="form-control" id="direccion" name="direccion"></textarea>
                </div>
            </form>
            */ ?>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-bs-dismiss="modal">Cerrar</button>
        <!-- <button class="btn" style="background: #93ba1f;" type="button" id="save_form_df">Guardar</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_cliente" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Editar Cliente</b></h1>
      </div>
      <div class="modal-body">
        <div class="row">
            <!-- -->
            <div class="col-md-4" align="center">
            </div>  
            <div class="col-md-2" align="center">
                
            </div>  
            <div class="col-md-6" align="right">
                <a type="button" class="btn realizarventa btn-sm" onclick="add_venta_select()">Utilizar para renta</a>
                <a type="button" class="btn realizarventa btn-sm" onclick="limpiar_select_cliente()">Limpiar búsqueda</a>
            </div>  
        </div>
        <br>    
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <select id="input_search_clientex" class="form-select btn-pill digits" style="background: #019cde; border-color: #019cde; color: white; width: 100%">
                        <option>Búsqueda de cliente</option>
                    </select>
                </div>
            </div>   
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="text_cliente"></div>
            </div>
        </div>

      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modal_series" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Series disponibles del equipo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="id_prod" value="0">
                        <table class="table table-sm table-striped table-bordered" id="table_series">
                            <thead>
                                <tr style="background: #009bdb;">
                                    <th scope="col" style="color: white;">Código</th>
                                    <th scope="col" style="color: white;">Equipo</th>
                                    <th scope="col" style="color: white;">Serie</th>
                                    <th scope="col" style="color: white;">Prox. Mtto.</th>
                                    <th scope="col" style="color: white;"></th>
                                </tr>
                            </thead>
                            <tbody class="series_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="asignaSerie()" class="btn btn-primary" type="button" data-bs-dismiss="modal">Aceptar</button>
                <button class="btn btn-info" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>