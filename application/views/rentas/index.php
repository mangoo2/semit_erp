<style type="text/css">
  #table_datos_filter{
    display: none;
  }
</style>
<div class="page-body">
  <div class="container-fluid dashboard-default-sec">
    <h3>Listado de rentas 
      <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
      <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
    </h3>
    <div class="row">
      <div class="col-lg-4">
        <div class="form-group row">
          <div class="col-12">
            <input class="form-control" type="search" placeholder="Buscar contrato" id="searchtext" oninput="search()">
          </div>
        </div>
      </div>  
      <div class="col-lg-4"></div> 
      <div class="col-lg-3" align="right"> 
        <a href="<?php echo base_url() ?>Rentas/alta" class="btn btn-primary">Nuevo registro</a>
      </div> 
      
    </div>  
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12">
            <table class="table table-sm" id="table_datos">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Cliente</th>
                  <th scope="col">Servicio</th>
                  <th scope="col">Serie</th>
                  <th scope="col">Fecha inicio</th>
                  <th scope="col">Fecha fin</th>
                  <th scope="col">Sucursal</th>
                  <th scope="col">Total</th>
                  <!--<th scope="col">Factura</th>-->
                  <th scope="col">Estatus</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>

<div class="modal fade" id="modal_renovas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalle de renovaciones</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row g-3">
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Contrato:</h5> 
              </div>
              <div class="col-md-4">
                <span id="id_contra"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Cliente: </h5>
              </div>
              <div class="col-md-4">
                <span id="name_cli"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Servicio: </h5>
              </div>
              <div class="col-md-4">
                <span id="name_serv"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Serie: </h5>
              </div>
              <div class="col-md-4">
                <span id="num_serie"></span>
              </div>
            </div>      
            <hr>    
            <div class="col-md-12">
              <table class="table table-sm" id="table_renovas">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <!--<th scope="col">Cliente</th>
                    <th scope="col">Servicio</th>
                    <th scope="col">Serie</th>-->
                    <th scope="col">Fecha inicio</th>
                    <th scope="col">Fecha fin</th>
                    <th scope="col">Sucursal</th>
                    <th scope="col">Estatus</th>
                    <th scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody id="det_renovas">
                </tbody>
              </table>
            </div>
          </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_pays" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pago de contrato</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <input type="hidden" id="idrc" value="0">
          <div class="row g-3">
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Contrato:</h5> 
              </div>
              <div class="col-md-4">
                <span id="id_contrap"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Cliente: </h5>
              </div>
              <div class="col-md-4">
                <span id="name_clip"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Servicio: </h5>
              </div>
              <div class="col-md-4">
                <span id="name_servp"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Serie: </h5>
              </div>
              <div class="col-md-4">
                <span id="num_seriep"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <h5 class="modal-title">Total de renta: </h5>
              </div>
              <div class="col-md-4">
                <span id="tot_renta"></span>
              </div>
            </div>        
            <hr>
          </div>
          <!--<form id="form_pays" method="post">
            <div class="row col-md-12">
              <div class="col-lg-2">
                <label>Monto</label>
                <input type="number" name="monto" id="monto" class="form-control" value="" required>
              </div>
              <div class="col-lg-2">
                <label>Fecha</label>
                <input type="date" name="fecha" id="fecha" class="form-control" value="" required>
              </div>
              <div class="col-lg-2">
                <label>Folio / Referencia</label>
                <input type="number" name="folio" id="folio" class="form-control" value="" required>
              </div>
              <div class="col-lg-2">
                <label>Método</label>
                <select id="periodo" name="periodo" class="form-select btn-pill digits" >
                  <option value="1">Efectivo</option>
                  <option value="2">Tarjeta de Crédito</option>
                  <option value="3">Tarjeta de Débito</option>
                  <option value="4">Transferencia</option>
                  <option value="5">Cheque</option>
                </select>
              </div>
              <div class="col-lg-1">
                <br><button class="btn btn-primary" type="button" id="addpay"> <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
              </div>
            </div> 
          </form>
          <div class="col-md-12">
            <table class="table table-sm" id="table_pays">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Monto</th>
                  <th scope="col">Folio</th>
                  <th scope="col">Método</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody id="det_pays">
              </tbody>
            </table>
          </div>-->
          <div id="cont_pay" class="col-md-12 mx-auto">
            <h3 style="color: #1770aa; text-align: center;" id="status_renta">Renta sin pago, generar pago en pantalla de <a target="_blank" href="<?php echo base_url() ?>Ventasp">ventas</a></h3>
          </div>
          <div id="cont_folio" style="text-align: center;" class="col-md-12 mx-auto">
            <div class="col-lg-3 mx-auto">
              <label>Folio físico de contrato</label>
              <input type="text" id="folio" class="form-control" value="">
            </div>
          </div>

        </div>    
      </div>    
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_contrato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Datos de Pagaré</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form id="form_datos" method="post">
            <input type="hidden" class="form-control" id="id_renta" name="id_renta">
            <input type="hidden" class="form-control" id="id_det_cont" name="id" value="0">
            <div class="row g-3">
              <div class="col-lg-3">
                <label>En</label>
                <input type="text" class="form-control" id="en_pagare" name="en_pagare" value="Toluca">
              </div>
              <div class="col-lg-3">
                <label>El</label>
                <input type="text" class="form-control" id="el_pagare" name="el_pagare" value="<?php echo date("Y-m-d"); ?>">
              </div>
              <div class="col-lg-3">
                <label>Cantidad</label>
                <input type="number" class="form-control" id="cant_pagare" name="cant_pagare">
              </div>
              <div class="col-lg-3">
                <label>% Interés</label>
                <input type="number" class="form-control" id="porc_interes" name="porc_interes" value="5">
              </div>
            </div>
            <div class="row g-3">
              <div class="col-lg-4">
                <label>Nombre</label>
                <input type="text" class="form-control" id="nom_pagare" name="nom_pagare">
              </div>
              <div class="col-lg-4">
                <label>Dirección</label>
                <input type="text" class="form-control" id="dir_pagare" name="dir_pagare">
              </div>
              <div class="col-lg-2">
                <label>Población</label>
                <input type="text" class="form-control" id="pobla_pagare" name="pobla_pagare">
              </div>
              <div class="col-lg-2">
                <label>Teléfono</label>
                <input type="text" class="form-control" id="tel_pagare" name="tel_pagare">
              </div>
            </div>
          </form>
        </div>
      </div>    
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <div>
          <button type="button" class="btn btn-success save_contra">Generar Contrato</button>
        </div>
      </div>
    </div>
  </div>
</div>
