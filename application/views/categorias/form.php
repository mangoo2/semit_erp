<div class="page-body">
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
      <div class="col-lg-10"> 
        <h3 style="color: #1770aa;">Nueva categoría</h3>
      </div>   
      <div class="col-lg-2" align="right"> 
        <a href="<?php echo base_url() ?>Cservicios" class="btn btn-primary">Regresar</a>
      </div>    
    </div>  
    <br>
  </div>        
    
  <div class="card card-custom gutter-b">
    <div class="card-body">
      <form id="form_datos" method="post">
        <input type="hidden" name="categoriaId" id="id" value="<?php if(isset($cat)) echo $cat->categoriaId; else echo "0"; ?>">
        <div class="row form-group"> 
          <div class="col-md-4">
            <label>Clave <span style="color: red">*</span></label>
            <input type="text" name="clave" id="clave" class="form-control" value="<?php if(isset($cat)) echo $cat->clave;?>">
          </div>
          <div class="col-md-8">
            <label>Categoria <span style="color: red">*</span></label>
            <input type="text" name="categoria" id="categoria" class="form-control" value="<?php if(isset($cat)) echo $cat->categoria;?>">
          </div>
        </div>  
      </form>  
      <div class="row"> 
        <div class="col-md-12" align="center">
          <button class="btn btn-primary btn_registro" type="button" style="width: 30%;" id="save">Guardar</button>                      
        </div>
      </div>
    </div>
  </div>
</div>    
