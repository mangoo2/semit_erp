<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {
        width: 100%;
        margin: 50px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
    .img_icon{
      width: 40px;
    }
    .oculto{
      display: none;
    }
    .visible{
      display: block;
    }
    .select2-container{
      width: 100% !important;
    }
    .sucusar_text2{
      display: none;
    }
    .cont_cod_provee {
      border: 1px solid red; 
      padding: 20px;
    }
</style>
<input type="hidden" id="idcategoria_aux" value="<?php echo $categoria ?>">
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <input type="hidden" id="idreg" value="<?php echo $id ?>">
      <div class="card-body">
        <ul class="nav nav-tabs" id="icon-tab" role="tablist">
          <li class="nav-item"><a class="nav-link <?php if($this->session->userdata('perfilid')!=11) echo "active"; ?> tab1" id="icon-home-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')!=11) echo 'href="#icon-home"'; else echo 'href="checkTab(1)" style="cursor: no-drop;'; ?> role="tab" aria-controls="icon-home" aria-selected="<?php if($this->session->userdata('perfilid')!=11) echo "true"; else echo "false"; ?>"><img src="<?php echo base_url().'public/img/25.webp';?>" class="img_icon"> Ajustes básicos</a></li>
          <li class="nav-item"><a class="nav-link tab2" id="profile-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')!=11) echo 'href="#profile-icon"'; else echo 'href="checkTab(2)" style="cursor: no-drop;'; ?> role="tab" aria-controls="profile-icon" aria-selected="false"><img src="<?php echo base_url().'public/img/26.webp';?>" class="img_icon">Cantidades</a></li>
          <li class="nav-item"><a class="nav-link tab3" id="contact-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')!=11) echo 'href="#contact-icon"'; else echo 'href="checkTab(3)" style="cursor: no-drop;'; ?> role="tab" aria-controls="contact-icon" aria-selected="false"><img src="<?php echo base_url().'public/img/27.webp';?>" class="img_icon">Logística</a></li>
          <li class="nav-item"><a class="nav-link tab4" id="precios-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')!=11) echo 'href="#precios-icon"'; else echo 'href="checkTab(4)" style="cursor: no-drop;'; ?> role="tab" aria-controls="precios-icon" aria-selected="false"><img src="<?php echo base_url().'public/img/28.webp';?>" class="img_icon">Precios</a></li>
          <li class="nav-item"><a class="nav-link <?php if($this->session->userdata('perfilid')==11) echo "active"; ?>" id="pagina-web-tab" data-bs-toggle="tab" href="#pagina-web" role="tab" aria-controls="pagina-web" aria-selected="<?php if($this->session->userdata('perfilid')==11) echo "true"; else echo "false"; ?>"><img src="<?php echo base_url().'public/img/29.webp';?>" class="img_icon">Pagina WEB</a></li>
          <!--<li class="nav-item"><a class="nav-link" id="sat-tab" data-bs-toggle="tab" href="#sat-icon" role="tab" aria-controls="sat-icon" aria-selected="false"><img src="<?php echo base_url().'public/img/30.webp';?>" class="img_icon">SAT</a></li>-->
          <li class="nav-item"><a class="nav-link tab6" id="compra-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')!=11) echo 'href="#compra-icon"'; else echo 'href="checkTab(6)" style="cursor: no-drop;'; ?> role="tab" aria-controls="compra-icon" aria-selected="false"><img src="<?php echo base_url().'public/img/28.webp';?>" class="img_icon">Compra</a></li>
        </ul>
        <div class="tab-content" id="icon-tabContent">
          <div class="tab-pane fade <?php if($this->session->userdata('perfilid')!=11) echo "show active"; ?>" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">

            <form id="form_ajustes" method="post">
              <input type="hidden" id="idreg1" name="id" value="<?php echo $id ?>">
              <br>
              <div class="row">
                <h3>Categoría</h3>
                <div class="col-md-12">
                  <div class="col-md-11">
                    <div class="categoria_txt"></div>
                    <br>
                  </div>
                  <!--<div class="col-md-1">
                    <label class="form-label" style="color: white">__</label><br>
                    <button class="btn btn-pill btn-primary" type="button" onclick="modal_categoria()"><i class="fa fa-plus"></i></button>
                  </div>  -->
                </div>
              </div>
              <div class="row">
                <hr style="background-color: #009bdb;">
                <h3>Generales</h3>
                <div class="col-md-3">
                  <label class="form-label">Código/ID de artículo<span style="color: red">*</span></label>
                  <input class="form-control" type="text" name="idProducto" id="idProducto" oninput="verificar_codigo()" value="<?php echo $idProducto ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label">Código de barras</label>
                  <input class="form-control" type="text" name="codigoBarras" value="<?php echo $codigoBarras ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label">Nombre<span style="color: red">*</span></label>
                  <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label" for="activo">Desactivar</label>
                  <span class="switch switch-icon">
                    <label>
                        <input type="checkbox" name="activo" id="activo" <?php if($activo=='N'){ echo 'checked';} ?>>
                        <span></span>
                    </label>
                  </span>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3">
                  <label class="form-label">Tipo</label>
                  <input type="hidden" id="perfusu" value="<?php echo $this->session->userdata('perfilid'); ?>">
                  <select class="form-control" name="tipo" id="tipo" onchange="validar_tipo()">
                    <?php if($this->session->userdata('perfilid')!=7 && $this->session->userdata('perfilid')!=10){ ?>
                      <option value="" <?php if($tipo=="") echo 'selected' ?>></option>  
                      <option value="0" <?php if($tipo==0) echo 'selected' ?>>Stock</option>  
                      <option value="1" <?php if($tipo==1) echo 'selected' ?>>Series</option>
                      <option value="2" <?php if($tipo==2) echo 'selected' ?>>Lote y caducidad</option>
                      <option value="3" <?php if($tipo==3) echo 'selected' ?>>Insumo</option>
                      <option value="4" <?php if($tipo==4) echo 'selected' ?>>Refacciones</option>
                    <?php }
                    if($this->session->userdata('perfilid')==7 || $this->session->userdata('perfilid')==10){ ?>
                      <option value="" <?php if($tipo=="") echo 'selected' ?>></option>  
                      <option value="3" <?php if($tipo==3) echo 'selected' ?>>Insumo</option>
                      <option value="4" <?php if($tipo==4) echo 'selected' ?>>Refacciones</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label col-sm-12">Unidad SAT <span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="unidad_sat" name="unidad_sat">
                      <?php foreach ($f_u->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label">Servicio SAT <span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="servicioId_sat" name="servicioId_sat">
                      <?php foreach ($f_s->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                </div>
              </div>

              <hr style="background-color: #009bdb;">
              <h3>Subtipo</h3>
              <div class="row g-3">
                <div class="col-md-3">
                  <label class="form-label">Tipo</label>
                  <div style="height: 34px;">
                    <span class="switch switch-icon" style="float: left;">
                      <label>
                        <input type="checkbox" id="concentrador" name="concentrador" <?php if($concentrador=="1") echo "checked";?> >
                        <span></span>  
                      </label>
                    </span>
                    Concentrador
                  </div>
                  <div style="height: 34px;">
                    <span class="switch switch-icon" style="float: left;">
                      <label>
                        <input type="checkbox" id="tanque" name="tanque" <?php if($tanque=="1") echo "checked";?> >
                        <span></span>  
                      </label>
                    </span>
                    Tanque de Oxígeno
                  </div>

                  <div class="row g-3 cont_capa" style="display:none">
                    <div class="col-md-8">
                      <label class="form-label">Capacidad (L)</label>
                      <input class="form-control" type="number" id="capacidad" name="capacidad" value="<?php echo $capacidad; ?>">
                    </div>
                  </div>
                </div>
              </div>
              
              <hr style="background-color: #009bdb;">
              <h3>Referencia</h3>
              <div class="row g-3">
                <div class="col-md-3">
                  <label class="form-label">Referencia</label>
                  <?php foreach ($refe->result() as $itemr) { ?>
                    <div style="height: 34px;">
                    <span class="switch switch-icon" style="float: left;">
                    <label>
                      <!-- se reemplaza disabled por readonly si no, no guarda el valor de referencia -->
                      <input readonly type="checkbox" id="ref_<?php echo $itemr->id;?>" name="referencia[]" value="<?php echo $itemr->id;?>">
                      <span></span> 
                    </label>
                  </span>
                  <?php echo $itemr->name;?>
                  </div>
                  <?php }?>
                </div>
              </div>

              <div class="row g-3">
                <br>
              </div>
              <hr style="background-color: #009bdb;">
              <div class="row g-3">
                <div class="col-md-12">
                  <h3>Descripción</h3>
                  <textarea class="form-control" name="descripcion" id="descripcion"><?php echo $descripcion ?></textarea>
                </div>
              </div>
              <hr style="background-color: #009bdb;">
              <div class="row g-3">
                <div class="col-md-12">
                  <h3>Observaciones</h3>
                  <textarea class="form-control" name="observaciones" id="observaciones"><?php echo $observaciones ?></textarea>
                </div>
              </div>
              

              <hr style="background-color: #009bdb;">
              <h3>Visualización del producto</h3>
              <div class="row g-3">
                <div class="col-md-12">
                  <label class="form-label">Mostrar en página</label>
                  <span class="switch switch-icon">
                    <label>
                      <input <?php if($this->session->userdata('perfilid')!=1) echo "disabled"; ?> type="checkbox" name="mostrar_pagina" id="mostrar_pagina" <?php if($mostrar_pagina==1) echo 'checked' ?>>
                      <span></span>
                    </label>
                  </span>
                </div>
              </div>

              <hr style="background-color: #009bdb;">
              <!--<h3>Referencia</h3>-->
              <div class="row g-3">
                <!--<div class="col-md-3">
                  <label class="form-label">Referencia</label>
                  <?php foreach ($refe->result() as $itemr) { ?>
                    <div style="height: 34px;">
                    <span class="switch switch-icon" style="float: left;">
                    <label>
                        <input type="checkbox" name="referencia[]" value="<?php echo $itemr->id;?>">
                        <span></span>
                        
                    </label>
                    
                  </span>
                  <?php echo $itemr->name;?>
                  </div>
                  <?php }?>
                </div>-->

                <div class="col-md-12" id="cont_cod_provee">
                  <div class="row">
                    <div class="col-md-6">
                      <label>Proveedor</label>
                      <select class="form-control" id="idproveedor">

                      </select>
                    </div>
                    <div class="col-md-5">
                      <label>Código de proveedor</label>
                      <input class="form-control" type="text" onchange="validarCodigProv(this.value)" id="codigo" value="">
                    </div> 
                    <div class="col-md-1">
                      <label class="form-label" style="color: white">__</label><br>
                      <button class="btn btn-pill btn-primary" type="button" onclick="add_proveedorres()"><i class="fa fa-plus"></i></button>
                    </div> 
                  </div> 
                  <div class="row">
                    <div class="col-md-12">
                      <br>
                      <table class="table table-sm" id="table_datos_proveedor" style="width:100%">
                        <tbody class="t_docy_pro">
                        </tbody>
                      </table>  
                    </div>
                  </div>
                </div>


                <div class="col-md-3" style="display: none">
                  <label>Unidad de medida</label>
                  <input type="text" name="unidadMedida" class="form-control" value="<?php echo $unidadMedida;?>">
                </div>
                <div class="col-md-3" style="display: none">
                  <label>Comisión</label>
                  <input type="text" name="comision" class="form-control" value="<?php echo $comision;?>">
                </div>
              </div>
              
              <br>
            </form>
            <div align="center">
              <?php if($this->session->userdata('perfilid')!=11){ ?>
                <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_ajustes()">Guardar</button>
              <?php } ?>
            </div>  
          </div>
          <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
            <br>
            <input type="hidden" id="idreg2" name="id" value="<?php echo $id ?>">
            <input type="hidden" id="suc_sess" value="<?php echo $this->session->userdata('sucursal') ?>">
            <?php  $cont_s=0;  ?>
            <?php  $cont_l=0;  ?>
            <table id="table_cantidades" width="100%">
              <tbody>
                <?php foreach ($suc_pro->result() as $item_sp) {
                if($this->session->userdata('sucursal')!=7 || $this->session->userdata('sucursal')==7 && $item_sp->idsuc==7){ ?>
                  <tr class="tr_c">
                    <td>
                      <!--------------------------------------------------------------------->
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-3">
                            <h3><?php echo $item_sp->id_alias." - ".$item_sp->name_suc;?></h3>
                          </div>
                        </div>
                        <?php 
                          $style_ocultarx='';
                          $style_ocultary='';
                          $style_ocultarz='';
                          if($tipo==1){
                            $style_ocultarx='style="display:none"';
                            $style_ocultary='';
                            $style_ocultarz='style="display:none"';
                          }else if($tipo==2){  
                            $style_ocultarx='style="display:none"';
                            $style_ocultary='style="display:none"';
                            $style_ocultarz='';
                          }else{
                            $style_ocultarx=''; 
                            $style_ocultary='style="display:none"';
                            $style_ocultarz='style="display:none"'; 
                          } 
                        ?>
                        <!--<form id="form_cantidades" method="post">-->
                          <div class="txt_cantidades_stock" <?php echo $style_ocultarx ?>>
                            <input class="form-control" type="hidden" id="id_suc_pro" value="<?php if($item_sp->id!="") echo $item_sp->id; else echo "0"; ?>">
                            <input class="form-control" type="hidden" id="sucursalid_suc_pro" value="<?php echo $item_sp->idsuc; ?>">
                            <div class="row g-3">
                              <div class="col-md-4">
                                <label class="form-label">Stock</label>
                                <input readonly <?php if($this->session->userdata('perfilid')!=1) echo "disabled"; ?> class="form-control" type="number" id="stock" value="<?php echo ($item_sp->stock-$item_sp->traslado_stock_cant); ?>">
                              </div>
                              <div class="col-md-4">
                                <label class="form-label">Stock mínimo</label>
                                <input class="form-control" type="number" id="stockmin" value="<?php echo $item_sp->stockmin ?>">
                              </div>
                              <div class="col-md-4">
                                <label class="form-label">Stock máximo</label>
                                <input class="form-control" type="number" id="stockmax" value="<?php echo $item_sp->stockmax ?>">
                              </div>
                            </div>
                          </div>
                          <div class="txt_cantidades_serie" <?php echo $style_ocultary ?>>
                            <div class="row g-3">
                              <div class="col-md-6">
                                <table class="table table-sm dataTable no-footer dtr-inline tabla_cantidades_sucursal">
                                  <tbody class="tbody_<?php echo $item_sp->idsuc ?>">
                                      <?php 
                                        $html='';                                             
                                          /*$result_serie=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('activo'=>1,'productoid'=>$id,'sucursalid'=>$item_sp->idsuc,"disponible"=>1));*/
                                          $result_serie=$this->ModelProductos->get_suc_series($id,$item_sp->idsuc);
                                          foreach ($result_serie->result() as $x) {
                                            if($x->idcompras!=0 && $x->estatus_compra=="1" || $x->idcompras=="0"){
                                              $html.='<tr class="tr_s row_s_'.$x->sucursalid.'_'.$cont_s.'">
                                                <td><input type="hidden" id="idx" value="'.$x->id.'">
                                                    <input type="hidden" id="sucursalx" value="'.$x->sucursalid.'">
                                                    <input type="hidden" id="seriex" class="seriex_'.$cont_s.'" data-serie="'.$x->serie.'" value="'.$x->serie.'">
                                                    <input type="hidden" id="editar_x" class="editar_x_'.$cont_s.'" value="0">
                                                    <span class="editar_txt_'.$cont_s.'">'.$x->serie.'<input type="hidden" id="seriey" value="'.$x->serie.'"></span></td>';
                                                  if($x->id!=$x->id_serie_tras && $x->id_gd==0 || $x->id==$x->id_serie_tras && $x->status_tras==2){
                                                    if($perfilid==1){
                                                      //$html.='<td><button type="button" class="btn btn-pill btn-cli-edit" onclick="editar_serie('.$x->id.','.$x->sucursalid.','.$cont_s.')"></button></td>';
                                                      $html.='<td></td>';
                                                    }else{
                                                      $html.='<td></td>';
                                                    }
                                                  }if($x->id==$x->id_serie_tras && $x->status_tras!=2){
                                                    $html.='<td><span class="btn btn-warning">En tránsito</span></td>';
                                                  }
                                                  if($x->id_gd!=0 && $x->disponible==0){
                                                    $html.='<td><span class="btn btn-warning">En garantía</span></td>';
                                                  }
                                              $html.='</tr>';
                                              $cont_s++;
                                            }
                                          }
                                          echo $html;
                                      ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>    
                          </div> 
                          <div class="txt_cantidades_lote" <?php echo $style_ocultarz ?>>
                            
                            <div class="row g-3">
                              <div class="col-md-7">
                                <table class="table table-sm dataTable no-footer dtr-inline tabla_cantidades_sucursal_lote">
                                  <thead>
                                    <tr>
                                      <td>Cantidad</td>
                                      <td>Lote</td>
                                      <td>Caducidad</td>
                                      <td>Código de Barras</td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                  </thead>
                                  <tbody class="tbody_l_<?php echo $item_sp->idsuc ?>">
                                      <?php 
                                        $html='';$total_lotes=0;                                             
                                        //$result_lote=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$id,'sucursalid'=>$item_sp->idsuc));
                                        
                                        $result_lote=$this->ModelProductos->get_suc_lotes($id,$item_sp->idsuc);
                                        foreach ($result_lote->result() as $l) {
                                          $cod_barras=$l->cod_barras;
                                          if($cod_barras==""){
                                            $cod_barras=$id.$item_sp->idsuc.intval(preg_replace('/[^0-9]+/', '', $l->lote), 10).date("Ymd",strtotime($l->caducidad));
                                          }
                                          $style="";
                                          if($l->cant_lote_tras>0){
                                            $style='style="background-color:rgb(255, 255, 143);"';
                                          }
                                          if($l->status>0){
                                            $txt_tras='(En tránsito: '.$l->cant_lote_tras.')';
                                          }else{
                                            $txt_tras="";
                                            $style="";
                                          }
                                          if($l->idcompra!=0 && $l->estatus_compra=="1" || $l->idcompra==0){
                                            $total_lotes+=$l->cantidad;
                                            $html.='<tr '.$style.' class="tr_l row_l_'.$l->sucursalid.'_'.$cont_l.'">
                                              <td><input type="hidden" id="idl" value="'.$l->id.'">
                                                <input type="hidden" id="sucursall" value="'.$l->sucursalid.'">
                                                <input type="hidden" id="cantidadl" value="'.$l->cantidad.'">
                                                <input type="hidden" id="lotel" value="'.$l->lote.'">
                                                <input type="hidden" id="caducidadl" value="'.$l->caducidad.'">
                                                <input type="hidden" id="cod_barrasl" value="'.$cod_barras.'">
                                                '.$l->cantidad.' '.$txt_tras.'
                                                </td>
                                              <td>'.$l->lote.'</td>
                                              <td>'.$l->caducidad.'</td>
                                              <td>'.$cod_barras.'</td>
                                              <td><button type="button" class="btn btn-info" onclick="printCode('.$l->id.')"><i class="fa fa-print icon_font"></i></button></td>
                                              <td>';
                                                if($perfilid==1){
                                                  //$html.='<button type="button" class="btn btn-pill btn-danger" onclick="delete_lote('.$l->id.','.$l->sucursalid.','.$cont_l.')"><i class="fa fa-trash icon_font"></i> </button>';
                                                }
                                              $html.='</td>
                                            </tr>'; 
                                          }                  
                                        }
                                        //$html.='<tr><td>'.$total_lotes.'</td><td colspan="4"></td><tr>';
                                        echo $html;
                                      ?>
                                  </tbody>

                                </table>
                              </div>
                            </div>    
                          </div>  
                          <!-- <div class="row g-3">
                            <div class="col-md-12">
                              <label class="form-label">Ubicación del producto</label>
                              <input class="form-control" type="text" id="ubicacion" value="<?php // echo $item_sp->ubicacion ?>">
                            </div>
                          </div>   -->
                        <!--</form>-->
                      
                      </div><br>
                      <!--------------------------------------------------------------------->
                    </td>
                  </tr>
                <?php $cont_l++; } } ?>
              </tbody>
            </table>
            <?php 
              echo '<input class="form-control" type="hidden" id="cont_s_n" value="'.$cont_s.'">';
              echo '<input class="form-control" type="hidden" id="cont_s_l" value="'.$cont_l.'">';
            ?>
            <div align="center">
              <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_cantidades()">Guardar</button>
            </div>
          </div>
          <div class="tab-pane fade" id="contact-icon" role="tabpanel" aria-labelledby="contact-icon-tab">
            
            <br>
            <form id="form_logistica" method="post">
              <input type="hidden" id="idreg3" name="id" value="<?php echo $id ?>">
              <h3>Catacterísticas de envío</h3>
              <div class="row g-3">
                <div class="col-md-3">
                  <label class="form-label">Ancho</label>
                  <input class="form-control" type="number" name="ancho" value="<?php echo $ancho ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label">Alto</label>
                  <input class="form-control" type="number" name="alto" value="<?php echo $alto ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label">Largo</label>
                  <input class="form-control" type="number" name="largo" value="<?php echo $largo ?>">
                </div>
                <div class="col-md-3">
                  <label class="form-label">Peso</label>
                  <input class="form-control" type="number" name="peso" value="<?php echo $peso ?>">
                </div>
              </div>
              <hr style="background-color: #009bdb;">
              <h3>Tiempos de entrega</h3>
              <div class="row g-3">
                <div class="col-md-12">
                  <label class="form-label">Plazos de entrega para los productos en inventario</label>
                  <input class="form-control" type="text" name="tiempo_carga" value="<?php echo $tiempo_carga ?>">
                </div>
              </div>
              <hr style="background-color: #009bdb;">
              <h3>Costos de envío</h3>
              <div class="row g-3">
                <div class="col-md-6">
                  
                  <label class="form-label">Costo de envío</label>
                  <input class="form-control" type="text" name="costo_envio" value="<?php echo $costo_envio ?>">
                </div>
                <div class="col-md-6">
                  <label class="form-label">Envío gratis</label>
                  <span class="switch switch-icon">
                        <label>
                            <input type="checkbox" name="envio_gratis" <?php if($envio_gratis==1) echo 'checked' ?>>
                            <span></span>
                        </label>
                    </span>
                </div>
              </div>
            </form> 
            <br> 
            <div align="center">
              <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_logistica()">Guardar</button>
            </div>
          </div>
          <div class="tab-pane fade" id="precios-icon" role="tabpanel" aria-labelledby="precios-icon-tab">
            <br>
            <input type="hidden" id="idreg4" name="id" value="<?php echo $id ?>">
            <table id="table_precios" width="100%">
                <tbody>

                  <?php foreach ($suc_pro->result() as $item_sp) { 
                    $sp_class='';
                    if($item_sp->idsuc==2){
                      $sp_class='';
                    }else{
                      $sp_class='class="sucusar_text sucusar_text2"';
                    }
                  ?>
                  
                  <tr <?php echo $sp_class ?>>
                    <td>
                      <!--------------------------------------------------------------------->
                        <div class="card-body">
                          <div class="row" style="display:none;">
                            <div class="col-md-3">
                              <h3><?php echo $item_sp->id_alias." - ".$item_sp->name_suc;?></h3>
                            </div>
                            <div class="col-md-3">
                              <?php if($item_sp->idsuc==2){ ?>
                              <div style="height: 34px;">
                                <span class="switch switch-icon" style="float: left;">
                                  <label>
                                    <input type="checkbox" id="mostrar_sucu"  onchange="mostrar_sucu()">
                                    <span></span>
                                  </label>
                                </span>
                                <label for="mismo_precio">Mostrar sucursales</label>
                              </div>
                              <?php } ?>
                            </div>  
                            <div class="col-md-6">
                              <?php if($item_sp->idsuc==2){ ?>
                              <div style="height: 34px;">
                                <span class="switch switch-icon" style="float: left;">
                                  <label>
                                    <input type="checkbox" name="mismo_precio" id="mismo_precio" checked disabled onchange="mismo_precio(<?php echo $item_sp->idsuc ?>)">
                                    <span></span>
                                  </label>
                                </span>
                                <label for="mismo_precio">Mismos Precios en todas las sucursales</label>
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                          <!--<form id="form_precios" method="post">-->
                            <input class="form-control" type="hidden" id="id_suc_pro" value="<?php if($item_sp->id!="") echo $item_sp->id; else echo "0"; ?>">
                            <input class="form-control" type="hidden" id="sucursalid_suc_pro" value="<?php echo $item_sp->idsuc; ?>">
                            <h4>Precios</h4>
                            <div class="row g-3">
                              <div class="col-md-3">
                                <label class="form-label">Precio de venta</label>
                                <input onchange="ocultar_por2(<?php echo $item_sp->idsuc ?>)" class="form-control inputprecios precio_<?php echo $item_sp->idsuc; ?>" type="number" name="precio" id="precioxx" value="<?php echo $item_sp->precio; ?>" >
                              </div>
                              <div class="col-md-2">
                                <label class="form-label" for="incluye_iva1">Grava IVA</label>
                                <span class="switch switch-icon">
                                  <label>
                                      <input type="radio" class="incluye_iva" name="incluye_iva_<?php echo $item_sp->idsuc; ?>" id="incluye_iva1" value="1" <?php if($item_sp->incluye_iva==='1'){ echo 'checked';} ?> onchange="incluye_iva(<?php echo $item_sp->idsuc; ?>)">
                                      <span></span>
                                  </label>
                                </span>
                                <div class="oculto oculto_<?php echo $item_sp->idsuc; ?> <?php if($item_sp->incluye_iva==='1'){ echo 'visible';} ?>">
                                  <select onchange="ocultar_por2(<?php echo $item_sp->idsuc ?>)" class="form-control iva_suc iva_<?php echo $item_sp->idsuc; ?>" id="iva">
                                    <option value="0" <?php if($item_sp->iva==0){ echo 'selected';} ?>>0 %</option>
                                    <option value="16" <?php if($item_sp->iva==16){ echo 'selected';} ?>>16 %</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-2">
                                <label class="form-label" for="incluye_iva0">no Grava IVA</label>
                                <span class="switch switch-icon">
                                  <label>
                                      <input type="radio" class="incluye_iva" name="incluye_iva_<?php echo $item_sp->idsuc; ?>" id="incluye_iva0" value="0" <?php if($item_sp->incluye_iva==='0'){ echo 'checked';} ?> onchange="incluye_iva(<?php echo $item_sp->idsuc; ?>)">
                                      <span></span>
                                  </label>
                                </span>
                              </div>
                              <?php 
                                $monto_iva=0;
                                if($item_sp->incluye_iva==='1'){
                                  if($item_sp->iva==16){
                                    $multi=$item_sp->precio*0.16;
                                    $monto_iva=round($item_sp->precio+$multi,0); 
                                  }else{
                                    $monto_iva=$item_sp->precio;
                                  }
                                }else{
                                  $monto_iva=$item_sp->precio;
                                } 
                              ?>  
                              <div class="col-md-3">
                                <label class="form-label">Precio final</label>
                                <input  class="form-control precio_f preciof_<?php echo $item_sp->idsuc; ?>" type="number" value="<?php echo $monto_iva; ?>" onchange="ocultar_por2(<?php echo $item_sp->idsuc ?>)" id="precio_ff">
                              </div>
                            </div>
                            <hr style="background-color: #009bdb;">
                            <h4 style="display: none">Descuentos</h4>
                            <div class="row g-3" style="display: none">
                              <div class="col-md-3">
                                <label class="form-label">Aplicar un descuento de</label>
                                <input class="form-control" type="number" name="descuento" id="descuento" value="<?php echo $item_sp->desc_monto; ?>">
                              </div>
                              <div class="col-md-3">
                                <label class="form-label" style="color:white">__</label>
                                <select class="form-control" name="tipo_descuento" id="tipo_descuento">
                                   <option value="1" <?php if($item_sp->desc_tipo_descuento==1) echo 'selected' ?>>$</option>
                                   <option value="2" <?php if($item_sp->desc_tipo_descuento==2) echo 'selected' ?>>%</option>
                                </select>
                              </div>
                            </div>
                          <!--</form>-->
                        </div>  
                        <br>
                      <!--------------------------------------------------------------------->
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
            <div align="center">
              <?php if($this->session->userdata('perfilid')!=11){ ?>
                <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_precios()">Guardar</button>
              <?php } ?>
            </div>
          </div>
          <div class="tab-pane fade <?php if($this->session->userdata('perfilid')==11) echo "show active"; ?>" id="pagina-web" role="tabpanel" aria-labelledby="pagina-web-tab">
            <input type="hidden" id="idreg5" name="id" value="<?php echo $id ?>">
            <h3>Imágenes del producto</h3>
              <div class="row g-3">
                <div class="col-md-4">
                  <input type="file" id="files" name="files[]" multiple accept="image/*">
                </div>
                <div class="col-md-8">
                  <div class="lis_catacter"></div>
                </div>
              </div>
              <div class="row g-3">
                <br>
              </div>
              <hr style="background-color: #009bdb;">
              <div class="row g-3">
                <div class="col-md-12">
                  <h3>Resumen</h3>
                  <textarea id="resumen" name="resumen" cols="30" rows="10"><?php echo $resumen ?></textarea>
                </div>
              </div>
              
              <hr style="background-color: #009bdb;">
              <h3>Características</h3>
              <div class="caracteristicas_1">
                <div class="caracteristicas_2">
                </div>
              </div>

              <div align="center">
                <button class="btn btn-primary btn_web" style="width: 30%" type="button" onclick="guardar_web()">Guardar</button>
              </div>
          </div>
          <!--<div class="tab-pane fade" id="sat-icon" role="tabpanel" aria-labelledby="sat-tab">
            <form id="form_registro_sat" method="post">
              <input type="hidden" id="idreg6" name="id" value="<?php echo $id ?>">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label col-sm-12">Unidad<span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="unidad_sat" name="unidad_sat">
                      <?php foreach ($f_u->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label">Servicio<span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="servicioId_sat" name="servicioId_sat">
                      <?php foreach ($f_s->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                </div>
              </div>
            </form>  
            <div align="center">
              <button class="btn btn-primary btn_sat" style="width: 30%" type="button" onclick="guardar_sat()">Guardar</button>
            </div>
          </div>-->
          <div class="tab-pane fade" id="compra-icon" role="tabpanel" aria-labelledby="compra-tab">
            <form id="form_registro_compra" method="post">
              <input type="hidden" id="idreg7" name="id" value="<?php echo $id ?>">
              <div class="row">
                <div class="col-md-3">
                  <label class="form-label">Costo compra</label>
                  <input class="form-control" onchange="ocultar_porc(0)" type="number" name="costo_compra" id="costo_compra" value="<?php echo $costo_compra ?>">
                </div>
                <div class="col-md-2">
                  <label class="form-label" for="incluye_iva2">Grava IVA</label>
                  <span class="switch switch-icon">
                    <label>
                      <input type="radio" onchange="ocultar_porc(1)" name="incluye_iva2" id="incluye_iva2" value="1" <?php if($incluye_iva_comp==='1'){ echo 'checked';} ?>>
                      <span></span>
                    </label>
                  </span>
                  <div class="oculto_comp" style="display:none">
                    <select onchange="ocultar_porc()" class="form-control" id="iva_comp">
                      <option value="0" <?php if($iva_comp==0){ echo 'selected';} ?>>0 %</option>
                      <option value="16" <?php if($iva_comp=="16"){ echo 'selected';} ?>>16 %</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <label class="form-label" for="incluye_iva0_2">no Grava IVA</label>
                  <span class="switch switch-icon">
                    <label>
                      <input type="radio" class="incluye_iva" name="incluye_iva2" id="incluye_iva0_2" value="0" <?php if($incluye_iva_comp==='0'){ echo 'checked';} ?> onchange="ocultar_porc(0)">
                      <span></span>
                    </label>
                  </span>
                </div>
                <?php 
                  $monto_iva=0; $sub_isr=0; $monto_final=0; $multi=0; 
                  if($id==0){
                    $monto_final=0;
                  }else{
                    if($incluye_iva_comp==='1'){
                      if($iva_comp==16){
                        $multi=$costo_compra*0.16;
                        $monto_iva=round($costo_compra+$multi,2);
                      }else{
                        $monto_iva=$costo_compra;
                      }
                    }else{
                      $monto_iva=$costo_compra;
                    } 
                    if($porc_isr>0){
                      $sub_isr= $costo_compra*($porc_isr/100);
                    }
                    $monto_final= $monto_iva-$sub_isr;
                  }
                  //log_message("error","monto_final: ".$monto_final);
                ?>  
                <div class="col-md-3">
                  <label class="form-label">Precio final</label>
                  <input readonly class="form-control" type="number" id="precioc_final" value="<?php echo number_format($monto_final,5,".",""); ?>">
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-2">
                  <label class="form-label" for="isr">ISR</label>
                  <span class="switch switch-icon">
                    <label>
                      <input onchange="ocultar_porc(0)" type="checkbox" name="isr" id="isr" value="0" <?php if($isr=='1'){ echo 'checked';} ?>>
                      <span></span>
                    </label>
                  </span>
                  <div class="oculto_isr" style="display:none">
                    <label class="form-label">Porcentaje ISR</label>
                    <input onchange="ocultar_porc(0)" class="form-control" type="number" name="porc_isr" id="porc_isr" value="<?php echo $porc_isr; ?>">
                  </div>
                </div>
              </div>
              <div class="row" style="display:none">
                <div class="col-md-2">
                  <label class="form-label" for="ieps">IEPS</label>
                  <span class="switch switch-icon">
                    <label>
                      <input type="checkbox" name="ieps" id="ieps" value="0" <?php if($ieps=='1'){ echo 'checked';} ?>>
                      <span></span>
                    </label>
                  </span>
                  <div class="oculto_ieps" style="display:none">
                    <label class="form-label">Porcentaje IEPS</label>
                    <input class="form-control" type="number" name="porc_ieps" id="porc_ieps" value="<?php echo $porc_ieps; ?>">
                  </div>
                </div>
              </div>

            </form>  
            <div align="center">
              <br>
              <button class="btn btn-primary btn_compra" style="width: 30%" type="button" onclick="guardar_compra()">Guardar</button>
            </div>
          </div>

        </div>
      </div>
    </div>  
  </div>
</div>    

<div class="modal fade" id="modalcategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Categorías</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row g-3">
          <div class="col-md-10">
            <form class="form" method="post" id="form_categoria" style="width: 100%">
              <label class="form-label">Nombre</label>
              <input type="hidden" name="categoriaId" id="idcategoria_categoria" value="0">
              <input class="form-control" type="text" name="categoria" id="nombre_categoria">
            </form>
          </div>
          <div class="col-md-2">
            <label class="form-label" style="color: white">__</label><br>
            <button class="btn btn-primary btn_categoria" type="button" onclick="add_categoria()"><i class="fa fa-save"></i></button>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm" id="table_categoria" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-secondary" type="button">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalcodebar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Imprimir código</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row g-3">
            <div class="col-md-12 mx-auto" id="iframeCode">
            </div>
            <div class="col-md-3 mx-auto">
              <button class="btn btn-primary" type="button" id="imprimiretiqueta">Imprimir</button>  
            </div>
            
          </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>