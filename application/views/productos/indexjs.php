<script src="<?php echo base_url(); ?>assets/slick/slick.js?v2022" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url();?>public/js/index_productos.js?v=<?php echo date('YmdGis') ?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		
	});
	function referencias(clave){
		var valor='';
		var miObjeto = {
				<?php foreach ($refe->result() as $x) { 
					echo "'$x->id':'$x->name',";
				}?>
			};
		var claveBuscada = clave;
		if (claveBuscada in miObjeto) {
		    var valorBuscado = miObjeto[claveBuscada];
		    valor=valorBuscado;
		    //console.log(`El valor de '${claveBuscada}' es '${valorBuscado}'`);
		} else {
		    //console.log(`'${claveBuscada}' no encontrado en el objeto`);
		}
		return valor;
	}
</script>