<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=lista_prods".date('Ymd').".xls"); 
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
  <thead>
    <tr>
    	<th rowspan="2">Código/ID de artículo</th>
      <th rowspan="2">Código de barras</th>
      <th rowspan="2">Nombre</th>
      <th rowspan="2">Categoría</th>
      <th rowspan="2">Unidad SAT</th>
      <th rowspan="2">Servicio SAT</th>
      <th rowspan="2">Proveedor</th>
      <th rowspan="2">Código de proveedor</th>
      <th rowspan="2">Tipo de producto</th>
      <th rowspan="2">Precio sin IVA</th>
      <th rowspan="2">IVA</th>
      <th rowspan="2">Precio con IVA</th>
      <th rowspan="2">Costo compra sin IVA</th>
      <th rowspan="2">IVA compra</th>
      <th rowspan="2">Costo compra con IVA</th>
      <th rowspan="2">ISR</th>
      <?php foreach($sucs as $s){
        echo '<th colspan="4">'. $s->name_suc.'</th>';
      } ?>
      
    </tr>
    <tr>
      <?php foreach($sucs as $s){
        echo '
          <th>Stock</th>
          <th>Stock minimo</th>
          <th>Stock máximo</th>
          <th>Detalles</th>';
      } ?>
    </tr>
  </thead>
  <tbody>
  	<?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
      $html="";
      foreach ($prods as $p) {
        $html.="<tr>";
          $html.="<td>&nbsp;".$p->idProducto."&nbsp;</td>";
          $html.="<td>&nbsp;".$p->codigoBarras."&nbsp;</td>";
          $html.="<td>".$p->nombre."</td>";
          $html.="<td>".$p->categoria."</td>";
          $html.="<td>".$p->unidad_sat."</td>";
          $html.="<td>".$p->servicioId_sat."</td>";
          $html.="<td>".$p->proveedor."</td>";
          $html.="<td>&nbsp;".$p->cod_prov."&nbsp;</td>";
          $html.="<td>".$p->tipo_prod."</td>";

          $html.="<td>".$p->precio_sucursal_8."</td>";
          $html.="<td>".$p->iva_txt."</td>";
          $html.="<td>".$p->precio_con_iva."</td>";
          $html.="<td>".$p->costo_compra."</td>";
          $html.="<td>".$p->iva_comp_txt."</td>";
          $html.="<td>".$p->costo_compra_iva."</td>";
          $html.="<td>".$p->porc_isr."</td>";
          
          $html.=$p->stock;  
          /*$html.="<td>stockmin:".$p->stockmin."</td>";
          $html.="<td>stockmax:".$p->stockmax."</td>";*/ 
          //$html.=$p->detalle; 

          
        $html.="</tr>";
      }
      echo $html;
      ?>
  </tbody>
</table>
