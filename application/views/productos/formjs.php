<script src="<?php echo base_url(); ?>assets/fileinput/fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/styles.js"></script>
<script src="<?php echo base_url(); ?>assets/slick/slick.js?v2022" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url();?>public/js/form_productos.js?v=2<?php echo date('YmdGis') ?>"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        <?php 
            if($id>1){ 
                $referenciarow=explode(',',$referencia);
				for ($i = 0; $i < count($referenciarow); ++$i) {
                    $referenciarowx = $referenciarow[$i];
                    
                    ?>
                        $("input[type='checkbox'][value='<?php echo $referenciarowx;?>']").attr('checked',true);
                    <?php 
                }
                echo "ocultar_porc(".$incluye_iva_comp.")";
            } 
        ?>
    });
</script>