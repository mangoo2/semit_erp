<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<input type="hidden" id="perfil" value="<?php echo $perfilid ?>">
<style type="text/css">
  .slider {
    width: 100%;
    margin: 50px auto;
  }

  .slick-slide {
    margin: 0px 20px;
  }

  .slick-slide img {
    width: 100%;
  }

  .slick-prev:before,
  .slick-next:before {
    color: black;
  }

  .slick-slide {
    transition: all ease-in-out .3s;
    opacity: .2;
  }
  
  .slick-active {
    opacity: .5;
  }

  .slick-current {
    opacity: 1;
  }

  .dataTables_filter label, .dataTables_paginate .pagination{
    float: right;
  }
  #table_datos_filter{
    display: none;
  }
  .consulta_stock_pro{
    font-size: 10px;
  }
  .tal{
    text-align:left;
  }
  .tar{
    text-align:right;
  }
</style>
<div class="page-body">
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
        <div class="col-lg-3">
          <div class="form-group row">
              <div class="col-12">
                  <input class="form-control" type="search" placeholder="Buscar producto" id="searchtext" oninput="search()">
              </div>
          </div>
        </div> 
        <div class="col-lg-3">
          <select class="form-control" id="idcategoria" onchange="reload_registro()">
            <option value="0">Categorías</option>
            <?php foreach ($get_categorias as $x){ ?>
              <option value="<?php echo $x->categoriaId ?>"><?php echo $x->categoria ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-lg-2">
          <select class="form-control" id="idproductos" onchange="reload_registro()">
            <option value="1">Productos activos</option>
            <option value="0">Productos inactivos</option>
          </select>
        </div>
        <div class="col-lg-2" >
          <?php if($perfilid==1){ ?>
            <button class="btn btn-success exportExcel" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Excel</button>
          <?php } ?>
        </div>
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos/registro" class="btn btn-primary">Nuevo registro</a>
        </div>    
    </div>  
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12">
            <table class="table table-sm" id="table_datos" style="width:100%">
              <thead>
                <tr>
                  <th scope="col" width="1%">#</th>
                  <th width="8%">Código</th>
                  <th width="5%">Foto</th>
                  <th width="41%">Nombre</th>
                  <th width="10%">Categoría</th>
                  <th swidth="8%">Precio</th>
                  <th scope="col" width="10%">Stock sucursales</th>
                  <!--<th scope="col">-->
                    <?php /*foreach($sucs as $s){
                      echo ' '.$s->name_suc.' |&nbsp&nbsp';
                    }*/ ?>
                  <!--</th>-->
                  <!--<th scope="col">Almacén General</th>
                  <th scope="col">Stock Pino Suárez </th>
                  <th scope="col">Stock Jesús Carranza</th>
                  <th scope="col">Stock Metepec</th>
                  <th scope="col">Stock Alfredo del Mazo</th>
                  <th scope="col">Almacén WEB</th>
                  <th scope="col">Almacén Rentas</th>-->
                  <th scope="col" width="7%">Estatus</th>
                  <!--<th scope="col" width="6%">Página</th>--> 
                  <th scope="col" width="10%">Acciones</th>
                </tr>
              </thead>
              <tbody >
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>

<div class="modal fade" id="modalimagenes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Imágenes</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_imagenes"></div> 
              </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalprecios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Precios por sucursal</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_precios"></div> 
              </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalcodigo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Verificar código</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t1" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t2').focus(); } }">
                </div>
                <div class="col-lg-2">  
                    <input type="text" name="codigo" id="codigo_t2" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t3').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t3" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t4').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t4" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t5').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t5" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;">
                </div>
                <div class="col-lg-12">
                    <span class="validar_codigo" style="color: red"></span>
                </div>    
            </div>    
        </div>
      </div>  
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" onclick="validar_codigo()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalcodigobarras" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Impresión de etiquetas</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-6">
                <h3><span style="color: #152342;">Código de producto:</span><span style="color: #93ba1f;" class="codigo"></span></h3>
                <h3><span style="color: #152342;">Producto:</span><span style="color: #93ba1f;" class="producto"></span></h3>  
                <h3><span style="color: #152342;">Categoría:</span><span style="color: #93ba1f;" class="categoria"></span></h3>
                <h3><span style="color: #152342;">Precio:</span><span style="color: #93ba1f;" class="precio"></span></h3>
                <input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
                <div class="row g-3">
                  <div class="col-md-6">
                    <h3><span style="color: #152342;">No. de etiquetas:</span><span style="color: #93ba1f;" class=""></span></h3>
                  </div>
                  <div class="col-md-6">
                    <input class="form-control" type="number" name="numprint" id="numprint"  min="1" value="1">
                  </div>  
                </div>  
                <button class="btn btn-primary" type="button" id="imprimiretiqueta">Imprimir</button>
              </div>
              <div class="col-md-6" id="iframeetiqueta">
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modallote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Stock por lote</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_lote"></div> 
              </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" onclick="setIndex()" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalinfosuc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Stock sucursales</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row infosucursal">
            
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalseries" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Número de series por sucursal</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row g-3">
            <div class="col-md-12">
              <div class="text_series"></div> 
            </div>
          </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" onclick="setIndex()" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>