<style type="text/css">
  
    @media (min-width: 992px) {
      .modal-lg, .modal-xl {
          max-width: 980px;
      }
    }
     .required{
      color: red !important;
    }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <!--<div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>  -->  
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <!--------------------------------------------------->
          <!------------------------------------------------>
              
              <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-2">
                  <label>Tipo Figura</label>
                  <select class="form-control" id="tipfig" onchange="loadtable_op()">
                    <option value="01">Operador</option>
                    <option value="02">Propietario</option>
                    <option value="03">Arrendador</option>
                    <option value="04">Notificado</option>

                  </select>
                </div>
                <div class="col-md-10" style="text-align: end;">
                  <a onclick="add_operador(0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table" id="table_operador">
                <thead class="thead-light">
                  <tr>
                    <th>#</th>
                    <th>TIPO FIGURA</th>
                    <th>RFC DEL OPERADOR</th>
                    <th>NO. LICENCIA </th>
                    <th>NOMBRE</th>
                    <th>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL</th>
                    <th>RESIDENCIA FISCAL</th>
                    <th>CALLE</th>
                    <th>ESTADO</th>
                    <th>PAÍS</th>
                    <th>CÓDIGO POSTAL</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <!------------------------------------------------>
        <!--------------------------------------------------->

      </div>
    </div>  
  </div>
</div>    

<div class="modal fade" id="modal_add_op" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_ope">
              <input type="hidden" name="id" id="id_op">
              <div class="row">
                <div class="col-md-6">
                  <label>Tipo Figura</label>
                  <select class="form-control" id="TipoFigura" name="TipoFigura">
                    <option value="01">Operador</option>
                    <option value="02">Propietario</option>
                    <option value="03">Arrendador</option>
                    <option value="04">Notificado</option>

                  </select>
                </div>
                <div class="col-md-6" title="RFCFigura">
                  <label>RFC/Id</label>
                  <input type="text" name="rfc_del_operador" id="rfc_del_operador" class="form-control" >
                </div>
                <div class="col-md-6 solooperador" title="NumLicencia">
                  <label>NO. LICENCIA</label>
                  <input type="text" name="no_licencia" id="no_licencia" class="form-control" >
                </div>

                <div class="col-md-6" title="NombreFigura">
                  <label>NOMBRE<span class="required">*</span></label>
                  <input type="text" name="operador" id="operador" class="form-control" required>
                </div>
                <div class="col-md-6">
                  <label>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL</label>
                  <input type="text" name="num_identificacion" id="num_identificacion" class="form-control" >
                </div>
                <div class="col-md-6" title="ResidenciaFiscalFigura">
                  <label>RESIDENCIA FISCAL</label>
                  <select name="residencia_fiscal" id="residencia_fiscal" class="form-control" title="Atributo condicional para registrar la clave del país de residencia de la figura de transporte que interviene en el traslado de los bienes y/o mercancías para los efectos fiscales correspondientes">
                    <option></option>
                    <option value="MEX">Mexico</option>
                  </select>
                </div>
                
              </div>

              <div class="row">
                
                <div class="col-md-6">
                  <label>Calle</label>
                  <input type="text" name="calle" id="calle" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>Numero Exterior</label>
                  <input type="text" name="num_ext" id="num_ext" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>Numero Interior</label>
                  <input type="text" name="num_int" id="num_int" class="form-control" >
                </div>


                <div class="col-md-6">
                  <label>Colonia</label>
                  <input type="text" name="colo" id="colo" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>Localidad</label>
                  <input type="text" name="loca" id="loca" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>Referencia</label>
                  <input type="text" name="ref" id="ref" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>Municipio</label>
                  <input type="text" name="muni" id="muni" class="form-control" >
                </div>


              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>ESTADO<span class="required">*</span></label>
                  <select class="form-control" name="estado" id="estado" required>
                          <option></option>
                      <?php foreach ($result_estados->result() as $item) { ?>
                          <option value="<?php echo $item->c_Estado ?>"><?php echo $item->descripcion ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>PAÍS<span class="required">*</span></label>
                  <input type="text" name="pais" id="pais" value="MEX" class="form-control" readonly required>
                </div>
                <div class="col-md-6">
                  <label>CÓDIGO POSTAL<span class="required">*</span></label>
                  <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" required>
                </div>
              </div>
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-success" type="button" id="btn_save_op" >Cerrar</button>
      </div>
    </div>
  </div>
</div>