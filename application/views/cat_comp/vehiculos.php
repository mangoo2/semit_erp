<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {width: 100%; margin: 50px auto; } .slick-slide {margin: 0px 20px; } .slick-slide img {width: 100%; } .slick-prev:before, .slick-next:before {color: black; } .slick-slide {transition: all ease-in-out .3s;opacity: .2; } .slick-active {opacity: .5; } .slick-current {opacity: 1; } .img_icon{width: 40px; } .oculto{display: none;} .visible{display: block;} .select2-container{width: 100% !important;} .sucusar_text2{display: none;}
    table td, table th{
      font-size: 12px;
    }
    .tab-content{
      padding-top: 10px;
    }
    .divbuttonstable{
      min-width: 110px;
    }
    .required{
      color: red;
    }
    @media (min-width: 992px) {
      .modal-lg, .modal-xl {
          max-width: 980px;
      }
    }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <!--------------------------------------------------->
          <!------------------------------------------------>
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_trans(0,0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_transporte">
                <thead class="thead-light">
                    <tr>
                        <th>Código</th>
                        <th>Nombre </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            <!------------------------------------------------>
        <!--------------------------------------------------->

      </div>
    </div>  
  </div>
</div>    


<div class="modal fade" id="modal_add_trans" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_vehiculos">
                
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button"  data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-success" type="button" id="btn_save_vehiculos">Guardar</button>
      </div>
    </div>
  </div>
</div>