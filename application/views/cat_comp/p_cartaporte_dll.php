<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {width: 100%; margin: 50px auto; } .slick-slide {margin: 0px 20px; } .slick-slide img {width: 100%; } .slick-prev:before, .slick-next:before {color: black; } .slick-slide {transition: all ease-in-out .3s;opacity: .2; } .slick-active {opacity: .5; } .slick-current {opacity: 1; } .img_icon{width: 40px; } .oculto{display: none;} .visible{display: block;} .select2-container{width: 100% !important;} .sucusar_text2{display: none;}
    table td, table th{
      font-size: 12px;
    }
    .tab-content{
      padding-top: 10px;
    }
    .divbuttonstable{
      min-width: 110px;
    }
    .required{
      color: red;
    }
    @media (min-width: 992px) {
      .modal-lg, .modal-xl {
          max-width: 980px;
      }
    }
    @media (min-width: 1200px) {
      .modal-lg, .modal-xl {
          max-width: 1100px;
      }
    }
    @media (min-width: 1400px) {
      .modal-lg, .modal-xl {
          max-width: 1200px;
      }
    }
    .font-weight-700{
      font-weight:700;
    }
    .form-control[readonly] {
      background-color: #f3f6f9;
    }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Cat_complemento/cat_carta_porte" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <input type="hidden" id="id_pcp" value="<?php echo $r_pcp->id;?>" class="form-control" readonly>
        <!--------------------------------------------------->
          <!------------------------------------------------>
            <form class="form" id="form_platilla">
              <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                  <label>No</label>
                  <input type="number" id="txtNumDocumento" value="<?php echo $r_pcp->txtNumDocumento;?>" class="form-control" readonly>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3">
                  <label>Tipo de documento</label>
                  <select name="selectTipoComprobante" id="selectTipoComprobante" class="form-control" required>
                      <option disabled selected class="option_disabled">Seleccione una option</option>
                      <option value="T" <?php if($r_pcp->selectTipoComprobante=='T'){ echo 'selected';}?> >Traslado</option>
                      <option value="I" <?php if($r_pcp->selectTipoComprobante=='I'){ echo 'selected';}?> >Ingreso</option>
                  </select>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3">
                  <label>Transporte internacional</label>
                  <select name="TranspInternac" id="TranspInternac" class="form-control" required>
                      <option value="">Seleccione una option</option>
                      <option value="Si" <?php if($r_pcp->TranspInternac=='Si'){ echo 'selected';} ?>>Si</option>
                      <option value="No" <?php if($r_pcp->TranspInternac=='No'){ echo 'selected';} ?>>No</option>
                  </select>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3">
                  <label>Pais origen/destino</label>
                  <input  class="form-control" disabled>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <label>Identificador</label>
                  <input type="text" name="txtIdentificador" id="txtIdentificador" value="<?php echo $r_pcp->txtIdentificador;?>" class="form-control">
                </div>
              </div>
            </form>
            <div class="row" style="margin-top: 10px;">
                <ul class="nav nav-tabs" id="icon-tab" role="tablist">
                  <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#transporte" role="tab" aria-controls="transporte" aria-selected="true">Autotransporte federal</a></li>
                  <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#ubicacion" role="tab" aria-controls="profile-icon" aria-selected="false" >Ubicacion</a></li>
                  <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-bs-toggle="tab" href="#figuras" role="tab" aria-controls="contact-icon" aria-selected="false" onclick="consul_fig_add('01')">Figuras</a></li>
                  
                </ul>
                <div class="tab-content" id="icon-tabContent">
                  <div class="tab-pane fade show active" id="transporte" role="tabpanel" aria-labelledby="icon-home-tab">
                    <!-------------------------estos campos vienen del catalogo de vehiculos----------------------->
                      <form class="form" id="form_vehiculos">
                        <div class="row">
                          <!--------------------------------------------->
                            <div class="col-md-4">
                              <label>Vehiculo</label>
                              <select name="selectVehiculos" id="selectVehiculos" class="form-control" required onchange="info_veiculo()">
                                  <?php 
                                    echo '<option disabled selected class="option_disabled">Seleccione una option</option>';
                                    foreach ($result_vehiculos->result() as $item) {
                                        if($r_pcp->selectVehiculos==$item->id){ $r_s_ve='selected';}else{ $r_s_ve='';}
                                        echo '<option value="'.$item->id.'" '.$r_s_ve.'>'.$item->txtNombre.'</option>';
                                    }
                                  ?>
                              </select>
                            </div>
                            <div class="col-md-8">
                                <label  class="active" data-translate="">Tipo de permiso STC</label>
                                <select id="tipo_permiso_sct" class="form-control" readonly></select>
                            </div>
                            <div class="input-field col-md-4 ">
                              <label id="" for="txtNoPermiso" class="active" data-translate="">No. Permiso</label>
                                  <input id="num_permiso_sct" class="form-control" type="text" readonly required>
                            </div>
                            <div class="input-field col-md-4">
                                <label id="" for="nombre_aseguradora" class="active" data-translate="">Aseguradora de responsabilidad civil*</label>
                                <input id="nombre_aseguradora" readonly class="form-control" type="text" required>
                            </div>
                            <div class="col-md-4">
                              <label id="" for="num_poliza_seguro" class="active" data-translate="">Póliza de responsabilidad civil*</label>
                              <input id="num_poliza_seguro"  type="text" class="form-control" readonly required>
                            </div>
                            <!-- campos nuevos -->
                            <div class="col-md-8">
                                <label  class="active" data-translate="">Aseguradora medio ambiente</label>
                                <input id="AseguraMedAmbiente" readonly type="text" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label  class="active" data-translate="">Póliza medio ambiente</label>
                                <input id="PolizaMedAmbiente" readonly type="text" class="form-control">
                            </div>
                             <div class="col-md-8">
                                <label  class="active" data-translate="">Aseguradora de carga</label>
                                <input id="AseguraCarga" readonly type="text" class="form-control">
                            </div>
                             <div class="col-md-4">
                                <label  class="active" data-translate="">Póliza de carga</label>
                                <input id="PolizaCarga" readonly type="text" class="form-control">
                            </div>
                             <div class="col-md-8">
                                <label  class="active" data-translate="">Prima de seguro</label>
                                <input id="PrimaSeguro" readonly type="number" class="form-control">
                            </div>
                            <!-- end campos nuevos -->
                            <div class="col col-sm-12 col-md-12 col-lg-12 mb-2">
                                <label  class="active" data-translate="">Tipo de configuración vehicular</label>
                                <select id="configuracion_vhicular" class="form-control" disabled></select>
                            </div>
                            <div class="input-field col-sm-12 col-md-6 col-lg-6">
                                <label id="" for="txtPlaca" class="active" data-translate="">Placa</label>
                                
                                <input id="placa_vehiculo_motor" readonly type="text" class="form-control">
                                
                            </div>
                            <div class="input-field col-sm-12 col-md-6 col-lg-6">
                                <label id="" for="txtAño" class="active" data-translate="">Año de modelo</label>
                                <div>
                                    <input id="anio_modelo_vihiculo_motor" readonly type="number" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <label  class="active" data-translate="">Tipo de subremolque 1</label>
                                <select class="form-control" readonly id="subtipo_remolque" disabled requerido>
                                    <option></option>
                                    <option value="CTR001" > Caballete</option><option value="CTR002" > Caja</option>
                                    <option value="CTR003" > Caja Abierta</option><option value="CTR004" > Caja Cerrada</option>
                                    <option value="CTR005" > Caja De Recolección Con Cargador Frontal</option><option value="CTR006" > Caja Refrigerada</option>
                                    <option value="CTR007" > Caja Seca</option><option value="CTR008" > Caja Transferencia</option>
                                    <option value="CTR009" > Cama Baja o Cuello Ganso</option><option value="CTR010" > Chasis Portacontenedor</option>
                                    <option value="CTR011" > Convencional De Chasis</option><option value="CTR012" > Equipo Especial</option>
                                    <option value="CTR013" > Estacas</option><option value="CTR014" > Góndola Madrina</option>
                                    <option value="CTR015" > Grúa Industrial</option><option value="CTR016" > Grúa </option>
                                    <option value="CTR017" > Integral</option><option value="CTR018" > Jaula</option>
                                    <option value="CTR019" > Media Redila</option><option value="CTR020" > Pallet o Celdillas</option>
                                    <option value="CTR021" > Plataforma</option><option value="CTR022" > Plataforma Con Grúa</option>
                                    <option value="CTR023" > Plataforma Encortinada</option><option value="CTR024" > Redilas</option>
                                    <option value="CTR025" > Refrigerador</option><option value="CTR026" > Revolvedora</option>
                                    <option value="CTR027" > Semicaja</option><option value="CTR028" > Tanque</option>
                                    <option value="CTR029" > Tolva</option><option value="CTR031" > Volteo</option>
                                    <option value="CTR032" > Volteo Desmontable</option>
                                </select>
                            </div>
                            <div class="input-field col-sm-12 col-md-4 col-lg-4">
                                <label id="" for="txtPlacaSubRemolque1" class="active" data-translate="">Placa</label>
                                <div>
                                    <input id="placa_remolque" readonly type="text" class="form-control" requerido>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <label  class="active" data-translate="">Tipo de subremolque 2</label>
                                <select class="form-control" readonly disabled id="subtipo2_remolque" >
                                    <option></option>
                                    <option value="CTR001" > Caballete</option><option value="CTR002" > Caja</option>
                                    <option value="CTR003" > Caja Abierta</option><option value="CTR004" > Caja Cerrada</option>
                                    <option value="CTR005" > Caja De Recolección Con Cargador Frontal</option><option value="CTR006" > Caja Refrigerada</option>
                                    <option value="CTR007" > Caja Seca</option><option value="CTR008" > Caja Transferencia</option>
                                    <option value="CTR009" > Cama Baja o Cuello Ganso</option><option value="CTR010" > Chasis Portacontenedor</option>
                                    <option value="CTR011" > Convencional De Chasis</option><option value="CTR012" > Equipo Especial</option>
                                    <option value="CTR013" > Estacas</option><option value="CTR014" > Góndola Madrina</option>
                                    <option value="CTR015" > Grúa Industrial</option><option value="CTR016" > Grúa </option>
                                    <option value="CTR017" > Integral</option><option value="CTR018" > Jaula</option>
                                    <option value="CTR019" > Media Redila</option><option value="CTR020" > Pallet o Celdillas</option>
                                    <option value="CTR021" > Plataforma</option><option value="CTR022" > Plataforma Con Grúa</option>
                                    <option value="CTR023" > Plataforma Encortinada</option><option value="CTR024" > Redilas</option>
                                    <option value="CTR025" > Refrigerador</option><option value="CTR026" > Revolvedora</option>
                                    <option value="CTR027" > Semicaja</option><option value="CTR028" > Tanque</option>
                                    <option value="CTR029" > Tolva</option><option value="CTR031" > Volteo</option>
                                    <option value="CTR032" > Volteo Desmontable</option>
                                </select>
                            </div>
                            <div class="input-field col-sm-12 col-md-4 col-lg-4">
                                <label id="" for="txtPlacaSubRemolque2" class="active" data-translate="">Placa</label>
                                <div>
                                    <input id="placa2_remolque" readonly type="text" class="form-control" >
                                </div>
                            </div>
                          <!-------------------------------------------------------->
                        </div>
                        
                      </form>
                    <!------------------------------------------------>
                  </div>
                  <div class="tab-pane fade" id="ubicacion" role="tabpanel" aria-labelledby="profile-icon-tab">
                    <!------------------------------------------------->
                      <div class="row" id="frmUbiTrasladoOrigen">
                          <div class="col col-sm-12 col-md-12 col-lg-12 mb-1">
                              <p class="font-weight-700">Origen</p>
                          </div>
                          <form id="form_ubi_o" class="row">
                            <input type="hidden" name="id_ubi_o" value="<?php echo $uo_id;?>">
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <label id="" for="" class="active" data-translate="">Almacén</label>
                                <select id="selectalmacenesorigen" name="selectalmacenesorigen" class="form-control" onchange="selectubica()" required>
                                  <option></option>
                                  <?php 
                                    foreach ($result_sucu->result() as $item_suc) {
                                      if($uo_id_ubicacion==$item_suc->id){ $sel='selected';}else{$sel='';}
                                      echo '<option value="'.$item_suc->id.'" '.$sel.'>'.$item_suc->id_alias.' '.$item_suc->name_suc.'</option>';
                                    }
                                  ?>
                                </select>
                            </div>
                            <div class="input-field col-sm-12 col-md-4 col-lg-4">
                                <label id="" for="" class="active" data-translate="">Fecha salida</label>
                                <div>
                                    <input id="fechasalida" name="fechasalida" class="flatpickr flatpickr-input form-control" value="<?php echo $uo_fechasalida;?>" type="datetime-local" required >
                                </div>
                                
                            </div>
                            
                          </form>

                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_calle" class="active" data-translate="">Calle</label>
                              <div>
                                  <input id="cp_calle" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_n_ex" class="active" data-translate="">Num. exterior</label>
                              <div>
                                  <input id="cp_n_ex" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_n_int" class="active" data-translate="">Num. interior</label>
                              <div>
                                  <input id="cp_n_int" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_colonia" class="active" data-translate="">Colonia</label>
                              <div>
                                  <input id="cp_colonia" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_localidad" class="active" data-translate="">Localidad</label>
                              <div>
                                  <input id="cp_localidad" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_ref" class="active" data-translate="">Referencia</label>
                              <div>
                                  <input id="cp_ref" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_minicipio" class="active" data-translate="">Municipio</label>
                              <div>
                                  <input id="cp_minicipio" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                           <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_estado" class="active" data-translate="">Estado</label>
                              <div>
                                  <input id="cp_estado" type="text" class="form-control" disabled="disabled">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_cl_estado" class="active" data-translate="">Clave estado</label>
                              <div>
                                  <input id="cp_cl_estado" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_pais" class="active" data-translate="">Pais</label>
                              <div>
                                  <input id="cp_pais" type="text" class="form-control" readonly>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label id="" for="cp_cp" class="active" data-translate="">Código postal</label>
                              <div>
                                  <input id="cp_cp" type="text" readonly class="form-control">
                              </div>
                              
                          </div>
                      </div>
                      <div class="row">
                        <div class="col col-sm-12 col-md-8 col-lg-8 mb-1">
                              <p class="font-weight-700">Destino</p>
                          </div>
                        <div class="col col-sm-12 col-md-4 col-lg-4 mb-1">
                          <button type="button" class="btn btn-success" onclick="adddestino()">Agregar Destino</button>
                        </div>
                      </div>
                      <div class="row">
                        <table class="table" id="table_destinos">
                          <thead class="thead-light">
                            <tr>
                              <th>#</th>
                              <th>Socio / Almacén</th>
                              <th>Fecha hora</th>
                              <th>Distancia</th>
                              <th>Calle</th>
                              <th># Exterior</th>
                              <th># Interior</th>
                              <th>Colonia</th>
                              <th>Localidad</th>
                              <th>Referencia</th>
                              <th>Municipio</th>
                              <th>Estado</th>
                              <th>Clave estado</th>
                              <th>País</th>
                              <th>CP</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tb_destinos"></tbody>
                          </table>
                      </div>
                    <!------------------------------------------------->
                  </div>
                  <div class="tab-pane fade" id="figuras" role="tabpanel" aria-labelledby="contact-icon-tab">
                    <!------------------------------------------------->
                      <ul class="nav nav-tabs" id="icon-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#operadores" role="tab" aria-controls="transporte" aria-selected="true" onclick="consul_fig_add('01')">Operadores</a></li>
                        <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#propietarios" role="tab" aria-controls="profile-icon" aria-selected="false"  onclick="consul_fig_add('02')">Propietarios</a></li>
                        <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-bs-toggle="tab" href="#arrendatarios" role="tab" aria-controls="contact-icon" aria-selected="false" onclick="consul_fig_add('03')" >Arrendatarios</a></li>
                        <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-bs-toggle="tab" href="#notificados" role="tab" aria-controls="contact-icon" aria-selected="false"  onclick="consul_fig_add('04')">Notificados</a></li>
                        
                      </ul>
                      <div class="tab-content" id="icon-tabContent">
                        <div class="tab-pane fade show active" id="operadores" role="tabpanel" aria-labelledby="icon-home-tab">
                          <!------------------------------------------------>
                            <div class="row" id="frmFigOpe">
                                <div class="mb-2"></div>
                                <div class="col-sm-12 col-md-3 col-lg-3">
                                    <label for="select_fig_01" class="active" data-translate="">Empleado</label>
                                    <select class="form-control" id="select_fig_01" onchange="view_fig('01')">
                                      <option></option>
                                      <?php 
                                        foreach ($res_fig_01->result() as $item1) {
                                          echo '<option value="'.$item1->id.'">'.$item1->operador.'</option>';
                                        }
                                      ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <?php echo $this->ModeloDevhtml->tiguras_view('01');?>
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                              <div class="col-md-12" style="text-align:right;">
                                <button type="button" class="btn btn-success" onclick="addfigura('01')" >Agregar</button>
                              </div>
                            </div>
                            <div class="row">
                              <table class="table" id="table_fig_01">
                                <thead class="thead-light">
                                <tr >
                                  <th  style="width: 3%;">#</th>
                                  <th  style="width: 7%;">Nombre</th>
                                  <th  style="width: 6%;">RFC</th>
                                  <th  style="width: 5%;">Residencia fiscal</th>
                                  <th  style="width: 6%;">Licencia operador</th>
                                  <th  style="width: 6%;">Calle</th>
                                  <th  style="width: 5%;">#Exterior</th>
                                  <th  style="width: 5%;">#Interior</th>
                                  <th  style="width: 6%;">Colonia</th>
                                  <th  style="width: 7%;">CP</th>
                                  <th  style="width: 6%;">Localidad</th>
                                  <th  style="width: 6%;">Refencia</th>
                                  <th  style="width: 7%;">Municipio</th>
                                  <th  style="width: 6%;">Estado</th>
                                  <th  style="width: 6%;">País</th>
                                  <th style="width: 5%;"></th>
                                  </tr>
                                </thead>
                                <tbody class="tbody_fig_01"></tbody>

                                </table>
                            </div>
                            
                          <!------------------------------------------------>
                        </div>
                        <div class="tab-pane fade" id="propietarios" role="tabpanel" aria-labelledby="profile-icon-tab">
                          <!------------------------------------------------->
                            <div class="row" id="frmFigPropietarios">
                                <div class="mb-2"></div>
                                <div class="col-sm-12 col-md-3 col-lg-3">
                                    <label for="select_fig_02" class="active" data-translate="">Socio de negocios</label>
                                    <select class="form-control" id="select_fig_02" onchange="view_fig('02')">
                                      <option></option>
                                      <?php 
                                        foreach ($res_fig_02->result() as $item2) {
                                          echo '<option value="'.$item2->id.'">'.$item2->operador.'</option>';
                                        }
                                      ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <?php echo $this->ModeloDevhtml->tiguras_view('02');?>
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                              <div class="col-md-12" style="text-align:right;">
                                <button type="button" class="btn btn-success" onclick="addfigura('02')">Agregar</button>
                              </div>
                            </div>
                            <div class="row">
                              <table class="table" id="table_fig_02">
                                <thead class="thead-light">

                                <tr >
                                  <th  style="width: 3%;">#</th>
                                  <th  style="width: 7%;">Nombre</th>
                                  <th  style="width: 6%;">RFC</th>
                                  <th  style="width: 5%;">Residencia fiscal</th>
                                  <th  style="width: 6%;">Calle</th>
                                  <th  style="width: 5%;">#Exterior</th>
                                  <th  style="width: 5%;">#Interior</th>
                                  <th  style="width: 6%;">Colonia</th>
                                  <th  style="width: 7%;">CP</th>
                                  <th  style="width: 6%;">Localidad</th>
                                  <th  style="width: 6%;">Refencia</th>
                                  <th  style="width: 7%;">Municipio</th>
                                  <th  style="width: 6%;">Estado</th>
                                  <th  style="width: 6%;">País</th>
                                  <th style="width: 6%;"></th>
                                </tr>
                                </thead>
                          <tbody class="tbody_fig_02"></tbody>
                                </table>
                            </div>
                          <!------------------------------------------------->
                        </div>
                        <div class="tab-pane fade" id="arrendatarios" role="tabpanel" aria-labelledby="contact-icon-tab">
                          <!------------------------------------------------->
                            <div class="row" id="frmFigArrendatarios">
                                <div class="mb-2"></div>
                                <div class="col-sm-12 col-md-3 col-lg-3">
                                    <label for="select_fig_03" class="active" data-translate="">Socio de negocios</label>
                                    <select class="form-control" id="select_fig_03" onchange="view_fig('03')">
                                      <option></option>
                                      <?php 
                                        foreach ($res_fig_03->result() as $item3) {
                                          echo '<option value="'.$item3->id.'">'.$item3->operador.'</option>';
                                        }
                                      ?>
                                    </select>
                                    
                                </div>
                            </div>
                            <?php echo $this->ModeloDevhtml->tiguras_view('03');?>
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                              <div class="col-md-12" style="text-align:right;">
                                <button type="button" class="btn btn-success" onclick="addfigura('03')">Agregar</button>
                              </div>
                            </div>
                            <div class="row">
                              <table class="table" id="table_fig_03">
                                <thead class="thead-light">
                                <tr >
                                  <th  style="width: 3%;">#</th>
                                  <th  style="width: 7%;">Nombre</th>
                                  <th  style="width: 6%;">RFC</th>
                                  <th  style="width: 5%;">Residencia fiscal</th>
                                  <th  style="width: 6%;">Calle</th>
                                  <th  style="width: 5%;">#Exterior</th>
                                  <th  style="width: 5%;">#Interior</th>
                                  <th  style="width: 6%;">Colonia</th>
                                  <th  style="width: 7%;">CP</th>
                                  <th  style="width: 6%;">Localidad</th>
                                  <th  style="width: 6%;">Refencia</th>
                                  <th  style="width: 7%;">Municipio</th>
                                  <th  style="width: 6%;">Estado</th>
                                  <th  style="width: 6%;">País</th>
                                  <th style="width: 5%;"></th>
                                </tr>
                              </thead>
                              <tbody class="tbody_fig_03"></tbody>
                              </table>
                            </div>
                          <!------------------------------------------------->
                        </div>
                        <div class="tab-pane fade" id="notificados" role="tabpanel" aria-labelledby="contact-icon-tab">
                          <!------------------------------------------------->
                            <div class="row" id="frmFigNotificados">
                                <div class="mb-2"></div>
                                <div class="col-sm-12 col-md-3 col-lg-3">
                                    <label for="select_fig_04" class="active" data-translate="">Socio de negocios</label>
                                    <select class="form-control" id="select_fig_04" onchange="view_fig('04')">
                                      <option></option>
                                      <?php 
                                        foreach ($res_fig_04->result() as $item4) {
                                          echo '<option value="'.$item4->id.'">'.$item4->operador.'</option>';
                                        }
                                      ?>
                                    </select>
                                </div>
                            </div>
                            <?php echo $this->ModeloDevhtml->tiguras_view('04');?>
                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                              <div class="col-md-12" style="text-align:right;">
                                <button type="button" class="btn btn-success" onclick="addfigura('04')">Agregar</button>
                              </div>
                            </div>
                            <div class="row">
                              <table class="table" id="table_fig_04">
                                <thead class="thead-light">
                                <tr >
                                  <th  style="width: 3%;">#</th>
                                  <th  style="width: 7%;">Nombre</th>
                                  <th  style="width: 6%;">RFC</th>
                                  <th  style="width: 5%;">Residencia fiscal</th>
                                  <th  style="width: 6%;">Calle</th>
                                  <th  style="width: 5%;">#Exterior</th>
                                  <th  style="width: 5%;">#Interior</th>
                                  <th  style="width: 6%;">Colonia</th>
                                  <th  style="width: 7%;">CP</th>
                                  <th  style="width: 6%;">Localidad</th>
                                  <th  style="width: 6%;">Refencia</th>
                                  <th  style="width: 7%;">Municipio</th>
                                  <th  style="width: 6%;">Estado</th>
                                  <th  style="width: 6%;">País</th>
                                  <th style="width: 5%;"></th>
                                </tr>
                              </thead>
                              <tbody class="tbody_fig_04"></tbody>
                              </table>
                              
                            </div>
                          <!------------------------------------------------->
                        </div>
                        
                        

                      </div>
                    <!------------------------------------------------->
                  </div>
                  
                  

                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12">
                <button type="button" class="btn btn-success" id="save_general">Guardar</button>
                <a type="button" class="btn btn-danger" href="<?php echo base_url().'Cat_complemento/cat_carta_porte'?>">Cancelar</a>
              </div>
            </div>

            <!------------------------------------------------>
        <!--------------------------------------------------->

      </div>
    </div>  
  </div>
</div>    