<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {width: 100%; margin: 50px auto; } .slick-slide {margin: 0px 20px; } .slick-slide img {width: 100%; } .slick-prev:before, .slick-next:before {color: black; } .slick-slide {transition: all ease-in-out .3s;opacity: .2; } .slick-active {opacity: .5; } .slick-current {opacity: 1; } .img_icon{width: 40px; } .oculto{display: none;} .visible{display: block;} .select2-container{width: 100% !important;} .sucusar_text2{display: none;}
    table td, table th{
      font-size: 12px;
    }
    .tab-content{
      padding-top: 10px;
    }
    .divbuttonstable{
      min-width: 110px;
    }
    .required{
      color: red;
    }
    @media (min-width: 992px) {
      .modal-lg, .modal-xl {
          max-width: 980px;
      }
    }
    @media (min-width: 1200px) {
      .modal-lg, .modal-xl {
          max-width: 1100px;
      }
    }
    @media (min-width: 1400px) {
      .modal-lg, .modal-xl {
          max-width: 1200px;
      }
    }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Inicio" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <!--------------------------------------------------->
          <!------------------------------------------------>
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_platilla(0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_platilla">
                <thead class="thead-light">
                    <tr>
                        <th>Número documento</th>
                        <th>Identificador</th>
                        <th>Tipo documento</th>
                        <th>Fecha salida</th>
                        <th>hora salida</th>
                        <th>Fecha llegada</th>
                        <th>Hora llegada</th>
                        <th>Vehiculo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            <!------------------------------------------------>
        <!--------------------------------------------------->

      </div>
    </div>  
  </div>
</div>    


<div class="modal fade" id="modal_add_platilla" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_platilla">
              <input type="hidden" name="id" id="fp_id">
              <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Numero Documento</label>
                      <input type="number" name="txtNumDocumento" id="txtNumDocumento" class="form-control" required>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Identificador</label>
                      <input type="text" name="txtIdentificador" id="txtIdentificador" class="form-control">
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Tipo de documento</label>
                      <select name="selectTipoComprobante" id="selectTipoComprobante" class="form-control" required>
                          <option disabled selected class="option_disabled">Seleccione una option</option>
                          <option value="T">Traslado</option>
                          <option value="I">Ingreso</option>
                      </select>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Tipo de fecha</label>
                      <select name="selectTipoFecha" id="selectTipoFecha" class="form-control" required>
                          <option disabled selected class="option_disabled">Seleccione una option</option>
                          <option value="SA">Salida</option>
                          <option value="LL">Llegada</option>
                      </select>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Fecha inicio</label>
                      <input type="date" name="txtFechaInicio" id="txtFechaInicio" class="form-control" required>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Fecha fin</label>
                      <input type="date" name="txtFechaFin" id="txtFechaFin" class="form-control" required>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>Vehiculo</label>
                      <select name="selectVehiculos" id="selectVehiculos" class="form-control" required>
                          <?php 
                            echo '<option disabled selected class="option_disabled">Seleccione una option</option>';
                            foreach ($result_vehiculos->result() as $item) {
                                echo '<option value="'.$item->id.'">'.$item->txtNombre.'</option>';
                            }
                          ?>
                      </select>
                  </div>
              </div>
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="btn_save_plat" >Guardar</button>
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>