<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {width: 100%; margin: 50px auto; } .slick-slide {margin: 0px 20px; } .slick-slide img {width: 100%; } .slick-prev:before, .slick-next:before {color: black; } .slick-slide {transition: all ease-in-out .3s;opacity: .2; } .slick-active {opacity: .5; } .slick-current {opacity: 1; } .img_icon{width: 40px; } .oculto{display: none;} .visible{display: block;} .select2-container{width: 100% !important;} .sucusar_text2{display: none;}
    table td, table th{
      font-size: 12px;
    }
    .tab-content{
      padding-top: 10px;
    }
    .divbuttonstable{
      min-width: 110px;
    }
    .required{
      color: red !important;
    }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <ul class="nav nav-tabs" id="icon-tab" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#operadores" role="tab" aria-controls="operadores" aria-selected="true">Operadores</a></li>
          <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#dir_o" role="tab" aria-controls="profile-icon" aria-selected="false" onclick="loadtable_dir_o()">Dirección Origen</a></li>
          <li class="nav-item"><a class="nav-link" id="contact-icon-tab" data-bs-toggle="tab" href="#dir_d" role="tab" aria-controls="contact-icon" aria-selected="false" onclick="loadtable_dir_d()">Dirección Destino</a></li>
          <li class="nav-item"><a class="nav-link" id="precios-icon-tab" data-bs-toggle="tab" href="#ruta_o" role="tab" aria-controls="precios-icon" aria-selected="false" onclick="loadtable_list_ro()">Ruta Origen</a></li>
          <li class="nav-item"><a class="nav-link" id="pagina-web-tab" data-bs-toggle="tab" href="#ruta_d" role="tab" aria-controls="pagina-web" aria-selected="false" onclick="loadtable_list_rd()">Ruta Destino</a></li>
          <li class="nav-item"><a class="nav-link" id="pagina-web-tab" data-bs-toggle="tab" href="#transporte" role="tab" aria-controls="pagina-web" aria-selected="false" onclick="loadtable_transporte()">Transportes</a></li>
          
        </ul>
        <div class="tab-content" id="icon-tabContent">
          <div class="tab-pane fade show active" id="operadores" role="tabpanel" aria-labelledby="icon-home-tab">
            <!------------------------------------------------>
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_operador(0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table" id="table_operador">
                <thead class="thead-light">
                  <tr>
                    <th>#</th>
                    <th>RFC DEL OPERADOR</th>
                    <th>NO. LICENCIA </th>
                    <th>NOMBRE DEL OPERADOR</th>
                    <th>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL DEL OPERADOR</th>
                    <th>RESIDENCIA FISCAL DEL OPERADOR</th>
                    <th>CALLE</th>
                    <th>ESTADO</th>
                    <th>PAÍS</th>
                    <th>CÓDIGO POSTAL</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <!------------------------------------------------>
          </div>
          <div class="tab-pane fade" id="dir_o" role="tabpanel" aria-labelledby="profile-icon-tab">
            <!------------------------------------------------->
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_direc(0,0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_dir_o">
                  <thead class="thead-light">
                      <tr>
                          <th>#</th>
                          <th>CALLE</th>
                          <th>NUM. EXT.</th>
                          <th>NUM. INT</th>
                          <th>COLONIA</th>
                          <th>LOCALIDAD</th>
                          <th>REFERENCIA</th>
                          <th>MUNICIPIO</th>
                          <th>ESTADO</th>
                          <th>PAÍS</th>
                          <th>CÓDIGO POSTAL</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            <!------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="dir_d" role="tabpanel" aria-labelledby="contact-icon-tab">
            <!------------------------------------------------->
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_direc(0,1)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_dir_d">
                  <thead class="thead-light">
                      <tr>
                          <th>#</th>
                          <th>CALLE</th>
                          <th>NUM. EXT.</th>
                          <th>NUM. INT</th>
                          <th>COLONIA</th>
                          <th>LOCALIDAD</th>
                          <th>REFERENCIA</th>
                          <th>MUNICIPIO</th>
                          <th>ESTADO</th>
                          <th>PAÍS</th>
                          <th>CÓDIGO POSTAL</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            <!------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="ruta_o" role="tabpanel" aria-labelledby="precios-icon-tab">
            <!------------------------------------------------->
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_ruta(0,0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_list_ro">
                  <thead class="thead-light">
                      <tr>
                          <th>#</th>
                          <th>ID ORIGEN</th>
                          <th>NUM DE ESTACIÓN</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            <!------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="ruta_d" role="tabpanel" aria-labelledby="pagina-web-tab">
            <!------------------------------------------------->
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_ruta(0,1)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_list_rd">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>ID ORIGEN</th>
                            <th>NUM DE ESTACIÓN</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            <!------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="transporte" role="tabpanel" aria-labelledby="pagina-web-tab">
            <!------------------------------------------------>
              <div class="row">
                <div class="col-md-12" style="text-align: end;">
                  <a onclick="add_trans(0)" class="btn btn-primary">Agregar nuevo</a>
                </div>
              </div>
              <table class="table m-0 table-bordered" id="table_transporte">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>NOMBRE DE LA ASEGURADORA </th>
                        <th>NÚMERO PÓLIZA DE SEGURO</th>
                        <th>Número de permiso SCT</th>
                        <th>Configuración vehícular</th>
                        <th>Placa vehículo motor</th>
                        <th>Año modelo vihículo motor</th>
                        <th>Subtipo de remolque</th>
                        <th>Placa remolque</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            <!------------------------------------------------>
          </div>
          

        </div>
      </div>
    </div>  
  </div>
</div>    



<div class="modal fade" id="modal_add_op" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_ope">
              <input type="hidden" name="id" id="id_op">
              <div class="row">
                <div class="col-md-6">
                  <label>RFC DEL OPERADOR</label>
                  <input type="text" name="rfc_del_operador" id="rfc_del_operador" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>NO. LICENCIA</label>
                  <input type="text" name="no_licencia" id="no_licencia" class="form-control" >
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>NOMBRE DEL OPERADOR<span class="required">*</span></label>
                  <input type="text" name="operador" id="operador" class="form-control" required>
                </div>
                <div class="col-md-6">
                  <label>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL DEL OPERADOR</label>
                  <input type="text" name="num_identificacion" id="num_identificacion" class="form-control" >
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>RESIDENCIA FISCAL DEL OPERADOR</label>
                  <input type="text" name="residencia_fiscal" id="residencia_fiscal" class="form-control" title="Atributo condicional para registrar la clave del país de residencia de la figura de transporte que interviene en el traslado de los bienes y/o mercancías para los efectos fiscales correspondientes">
                </div>
                <div class="col-md-6">
                  <label>Calle</label>
                  <input type="text" name="calle" id="calle" class="form-control" >
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>ESTADO<span class="required">*</span></label>
                  <select class="form-control" name="estado" id="estado" required>
                          <option></option>
                      <?php foreach ($result_estados->result() as $item) { ?>
                          <option value="<?php echo $item->c_Estado ?>"><?php echo $item->descripcion ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>PAÍS<span class="required">*</span></label>
                  <input type="text" name="pais" id="pais" value="MEX" class="form-control" readonly required>
                </div>
                <div class="col-md-6">
                  <label>CÓDIGO POSTAL<span class="required">*</span></label>
                  <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" required>
                </div>
              </div>
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="btn_save_op" >Cerrar</button>
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_add_direcciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_direccion">
              <input type="hidden" name="dir_id" id="dir_id">
              <input type="hidden" name="dir_tipo" id="dir_tipo">
              <div class="row">
                <div class="col-md-12">
                  <label>CALLE</label>
                  <input type="text" name="dir_calle" id="dir_calle" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>NUM. EXT.</label>
                  <input type="text" name="dir_num_ext" id="dir_num_ext" class="form-control" >
                </div>
                <div class="col-md-6">
                  <label>NUM. INT.</label>
                  <input type="text" name="dir_num_int" id="dir_num_int" class="form-control" >
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>CÓDIGO POSTAL<span class="required">*</span></label>
                  <input type="text" name="dir_codigo_postal" id="dir_codigo_postal" class="form-control" required>
                </div>
                <div class="col-md-6">
                  <label>PAIS<span class="required">*</span></label>
                  <input type="text" name="dir_pais" id="dir_pais" value="MEX" class="form-control" readonly required>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>Estado<span class="required">*</span></label>
                  <select class="form-control" name="dir_estado" id="dir_estado" required onchange="selectestado_dir()">
                          <option></option>
                    <?php foreach ($result_estados->result() as $item) { ?>
                          <option value="<?php echo $item->c_Estado ?>"><?php echo $item->descripcion ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Colonia</label>
                  <select class="form-control" name="dir_colonia" id="dir_colonia">
                    
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>Localidad</label>
                  <select class="form-control" name="dir_localidad" id="dir_localidad">
                    
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Municipio</label>
                  <select class="form-control" name="dir_municipio" id="dir_municipio">
                    
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Referencia</label>
                  <input type="text" name="dir_referencia" id="dir_referencia" class="form-control">
                </div>
              </div>
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="btn_save_dir" >Cerrar</button>
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_add_ruta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_ruta">
              <input type="hidden" name="id" id="id_rut">
              <input type="hidden" name="tipo" id="rut_tipo">
              <div class="row">
                <div class="col-md-6  addidorigenes">
                  <label>ID <span class="org_des"></span></label>
                  <input type="text" name="idorigen" id="idorigen" class="form-control mascarainput" placeholder="OR000123 / DE000123" data-inputmask="'mask': 'OR999999'" inputmode="text" required>
                </div>
                <div class="col-md-6">
                  <label>NUM DE ESTACIÓN </label>
                  <select class="form-control" name="num_estacion" id="num_estacion" required>
                          
                  </select>
                </div>
              </div>
              
              
              
              
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-success" type="button" id="btn_save_rut" >Cerrar</button>
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>