<style type="text/css">
  
    @media (min-width: 992px) {
      .modal-lg, .modal-xl {
          max-width: 980px;
      }
    }

</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <!--<div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>  -->  
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <!--------------------------------------------------->
          <!------------------------------------------------>
              
              <table class="table m-0 table-bordered" id="table_data">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Clave</th>
                        <th>Nombre </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                    $html='';
                    foreach ($result_sucu->result() as $item) {
                      $html.='<tr>';
                        $html.='<td>'.$item->id_alias.'</td>';
                        $html.='<td>'.$item->clave.'</td>';
                        $html.='<td>'.$item->name_suc.'</td>';
                        $html.='<td><a onclick="edit_suc('.$item->id.')" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>';
                    $html.='</tr>';
                    }
                    echo $html;
                  ?>
                </tbody>
              </table>
            <!------------------------------------------------>
        <!--------------------------------------------------->

      </div>
    </div>  
  </div>
</div>    


<div class="modal fade" id="modal_ubi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h1 class="title" style="color:#019cde"></h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form class="form" id="form_ubicacion">
              <input id="id_ubi" name="id_ubi" type="hidden" class="form-control">
                <div class="row" >
                          
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_calle" class="active" data-translate="">Calle</label>
                              <div>
                                  <input id="cp_calle" name="cp_calle" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_n_ex" class="active" data-translate="">Num. exterior</label>
                              <div>
                                  <input id="cp_n_ex" name="cp_n_ex" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_n_int" class="active" data-translate="">Num. interior</label>
                              <div>
                                  <input id="cp_n_int" name="cp_n_int" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_colonia" class="active" data-translate="">Colonia</label>
                              <div>
                                  <input id="cp_colonia" name="cp_colonia" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_localidad" class="active" data-translate="">Localidad</label>
                              <div>
                                  <input id="cp_localidad" name="cp_localidad" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_ref" class="active" data-translate="">Referencia</label>
                              <div>
                                  <input id="cp_ref" name="cp_ref" type="text" class="form-control">
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_minicipio" class="active" data-translate="">Municipio</label>
                              <div>
                                  <input id="cp_minicipio" name="cp_minicipio" type="text" class="form-control">
                              </div>
                              
                          </div>
                           <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_estado" class="active" data-translate="">Estado<span class="error">*</span></label>
                              <div>
                                <select id="cp_estado" name="cp_estado" class="form-control" required><option></option>
                                  <?php 
                                    foreach ($result_estados->result() as $item_e) {
                                      echo '<option value="'.$item_e->c_Estado.'">'.$item_e->descripcion.'</option>';
                                    }
                                  ?>
                                </select>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_cl_estado" class="active" data-translate="">Clave estado<span class="error">*</span></label>
                              <div>
                                  <input id="cp_cl_estado" name="cp_cl_estado" type="text" class="form-control" readonly required>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_pais" class="active" data-translate="">Pais<span class="error">*</span></label>
                              <div>
                                  <select id="cp_pais" name="cp_pais" class="form-control" required>
                                    <option value="MEX">Mexico</option>
                                  </select>
                              </div>
                              
                          </div>
                          <div class="input-field col-sm-12 col-md-3 col-lg-3">
                              <label  for="cp_cp" class="active" data-translate="">Código postal<span class="error">*</span></label>
                              <div>
                                  <input id="cp_cp" name="cp_cp" type="text" class="form-control" required>
                              </div>
                              
                          </div>
                      </div>
            </form>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button"  data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-success" type="button" id="btn_save_ubi">Guardar</button>
      </div>
    </div>
  </div>
</div>