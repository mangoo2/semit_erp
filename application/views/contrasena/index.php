<style type="text/css">
    .crono {
      color:#000000; 
      font-family: 'Agency FB', arial;
      font-size: 200%;
      text-shadow: 4px 4px 4px #aaa;
      padding-left: 49px;
        
    }
</style>

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Contraseñas temporales</h3>  
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                              <label class="form-label">Contraseña</label>
                              <?php $valor=0;
                              if(isset($_GET["valor"])){
                                $valor=$_GET["valor"];
                              }else{
                                $valor=0;
                              } ?>
                              <input type="hidden" id="valor" value="<?php echo $valor; ?>">
                              <input class="form-control" type="text" id="codigo" readonly="" style="font-size: 15px;">
                            </div>
                            <div class="col-md-1">
                                <label class="form-label" style="color: white">__</label><br>
                                <button class="btn btn-primary btn_registro" onclick="generar_contrasena()">Generar contraseña</button>
                            </div>
                            <div class="col-md-2">
                                <br> 
                                <div id="cronometro">
                                  <div class="crono"> 
                                    <span id="reloj_mi">00</span>:
                                    <span id="reloj_sg">00</span>:
                                    <span id="reloj_cs">00</span>
                                  </div>
                                </div>
                            </div>   
                            <!-- <div class="col-md-2">
                                <label class="form-label" style="color: white">__</label><br>
                                <button class="btn btn-primary" >Historial de traspasos</button>
                            </div>  -->
                        </div>    
                    </div>

                </div>                
            </div>
        <!--end::Container-->
        <center> 
            
        </center>


    </div>
    <!--end::Entry-->
</div>