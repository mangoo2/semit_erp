<style type="text/css">
    .kv-file-upload {
        display: none; 
    }
</style>
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="left"> 
                <h3 style="color: #1770aa;">Nueva solicitud de garantía</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Garantias" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <input type="hidden" id="id" value="0">
                            <div class="row cont_search_tras">
                                <div class="col-lg-2">
                                    <label>Cantidad</label>
                                    <input class="form-control" type="number" id="cantidad">
                                </div>
                                <div class="col-lg-7">
                                    <label>Producto</label>
                                    <select class="form-control" id="idproducto">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Sucursal solicita</label>
                                    <select class="form-control" id="id_sucursal">
                                        <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==4 || $this->session->userdata('perfilid')==5){ ?>
                                            <option value="0">Seleccionar</option>
                                            <?php foreach ($sucrow->result() as $x){ if($x->id==$this->session->userdata('sucursal')) $sel="selected"; else $sel=""; ?>
                                                <option <?php echo $sel; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
                                        <?php } 
                                        } else {
                                            $suc = $this->session->userdata('sucursal');
                                            foreach ($sucrow->result() as $x){ 
                                                if($x->id==$suc) 
                                                    echo '<option class="option_'.$x->id.'" value="'.$x->id.'">'.$x->id_alias.' - '.$x->name_suc.'</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                                <div class="col-lg-9">
                                    <label>Motivo de solicitud</label>
                                    <textarea class="form-control" id="motivo" rows="4"></textarea>
                                </div>
                                <!--<div class="col-lg-1">
                                    <button type="button" style="margin-top: 25px;" class="btn btn-primary btn_add" onclick="get_producto()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                </div>-->
                            </div>
                        </form> 
                    </div>
                </div>
                <!-- All -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_stock">
                            <br>
                            <table class="table table-sm" id="table_datos_stock">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">PRODUCTOS SOLICITADOS A GARANTÍA</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Origen</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody id="body_prods">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row g-3" style="text-align:center">
                    <div class="col-lg-12">
                        <hr><label><h2>EVIDENCIA</h2></label>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-md-6">
                            <input class="form-control" type="file" name="file" id="file">
                        </div>
                    </div>
                </div>
                <div class="row g-3" style="text-align:center">
                    <div class="col-lg-12" id="cont_evide">
                        <br><br>
                        <div class="controls" id="cont_imgs">
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button type="button" class="btn btn-primary" id="savesg" style="width: 60%">Solicitar garantía</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>
