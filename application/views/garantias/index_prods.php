<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <h3>Listado de productos 
          <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
          <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
        </h3>
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="search" placeholder="Buscar producto" id="searchtext" oninput="search()">

                    </div>
                </div>
            </div>
        </div>  
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">    
                    <div class="col-lg-12"><h3 style="color: #1770aa;">Listado de productos en garantía</h3></div>
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th scope="col">Id solicitud</th>
                                    <th scope="col">Código</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Detalle</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Sucursal Origen</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de garantía</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-md-12">
                            <table class="table table-sm" id="prod_detalle">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Detalle</th>
                                        <th>Comentarios</th>
                                    </tr>
                                </thead>
                                <tbody id="det_bita"></tbody>
                            </table>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalfiles" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Carta de proveedor </b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3" style="text-align:center">
                        <input class="form-control" type="hidden" id="id_gd">
                        <div class="col-lg-12">
                            <hr><label><h2>Documento</h2></label>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-md-8">
                                <input class="form-control" type="file" name="file" id="file">
                            </div>
                        </div>
                    </div>
                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12" id="cont_evide">
                            <br><br>
                            <div class="controls" id="cont_imgs">
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                </div>   
            </div>
        </div>
    </div>
</div>