<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
    .img_icon{
        width: 36px;
    }

    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    .kv-file-remove{
        display: none;
    }
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <h3>Listado de garantías 
          <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
          <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
        </h3>
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="search" placeholder="Buscar garantía" id="searchtext" oninput="search()">
                    </div>
                </div>

            </div>
            <div class="col-lg-5"></div>
            <?php if($this->session->userdata("perfilid")==1 || $this->session->userdata('perfilid')==4 || $this->session->userdata('perfilid')==5){ ?>
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Garantias/productos" class="btn btn-primary">Ver Productos en Garantía</a>
                </div>
            <?php } ?>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Garantias/solicitud" class="btn btn-primary">Nueva solicitud de Garantía</a>
            </div>
        </div>  
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">    
                    <div class="col-lg-12"><h3 style="color: #1770aa;">Listado de solicitud de garantías</h3></div>
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Fecha y hora</th>
                                    <th scope="col">Usuario que solicita</th>
                                    <th scope="col">Motivo</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Productos</th>
                                    <th scope="col">Evidencia</th>
                                    <th scope="col">Sucursal solicita</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de solicitud</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-md-12">
                            <table class="table table-sm" id="prod_detalle">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Detalle</th>
                                        <th>Cantidad</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="body_det"></tbody>
                            </table>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                <div class="cont_ok" style="display:none">
                    <button type="button" class="btn btn-success btn_aceptar">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalfiles" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Evidencia de solicitud</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12">
                            <label><h2>EVIDENCIA CARGADA</h2></label>
                        </div>
                    </div>
                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12" id="cont_evide">
                            <br><br>
                            <div class="controls" id="cont_imgs">
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                </div>   
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalOrden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de solicitud</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input class="form-control" type="hidden" id="id_garantia">
                        <input class="form-control" type="hidden" id="id_ordeng" value="0">
                        <div class="col-md-5">
                            <label>Fecha de Orden</label>
                            <input class="form-control" type="date" id="fecha_orden">
                        </div>
                    </div>
                    <div class="col-lg-12"><br></div>
                    <div class="row g-3">
                        <div class="col-md-12">
                          <label>Observaciones</label>
                          <textarea cols="5" class="form-control" id="observ"></textarea>
                        </div>
                      </div>
                </div>
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <label>Motivo de solicitud</label>
                            <textarea disabled class="form-control" name="motivo" id="motivo_solicita" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-6">
                            <label>Nombre</label>
                            <input disabled type="text" class="form-control" id="nom_solicita" name="nom_solicita">
                        </div>
                        <div class="col-lg-6">
                            <label>Dirección</label>
                            <input disabled type="text" class="form-control" id="dir_solicita" name="dir_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-4">
                            <label>Colonia</label>
                            <input disabled class="form-control" id="col_solicita" name="col_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>Celular</label>
                            <input disabled type="text" class="form-control" id="cel_solicita" name="cel_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>Tel. Casa</label>
                            <input disabled type="text" class="form-control" id="tel_solicita" name="tel_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-4">
                            <label>Email</label>
                            <input disabled type="email" class="form-control" id="mail_solicita" name="mail_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>RFC</label>
                            <input disabled type="text" class="form-control" id="rfc_solicita" name="rfc_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-6">
                            <label>Contacto</label>
                            <input disabled type="text" class="form-control" id="nom_contacto" name="nom_contacto">
                        </div>
                        <div class="col-lg-4">
                            <label>Teléfono</label>
                            <input disabled type="text" class="form-control" id="tel_contacto" name="tel_contacto">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                <div>
                    <button type="button" class="btn btn-success save_orden">Generar Orden</button>
                </div>
            </div>
        </div>
    </div>
</div>