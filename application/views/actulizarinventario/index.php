<style type="text/css">
  progress {
    border: none;
    border-radius: 2rem;
    width: 100%;
    height: 9px;
    animation: prog 2s linear;
    }

    ::-webkit-progress-bar-value {background-color: #1DA1F2; border-radius: 2rem;}
    ::-webkit-progress-value {background-color: #1DA1F2; border-radius: 2rem;}
    ::-moz-progress-bar {background-color: #1DA1F2; border-radius: 2rem;}

    @keyframes prog {
    0% {width: 0%; background-color: #f1f1f1;}
    100% {width: 100% color: #000;}
  }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Actualizar productos</h3>  
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="texto_proceso">
                                    <progress max="100" value="0"></progress>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="texto_proceso2">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4" align="center">
                                <button class="btn btn-primary btn_registro" onclick="productos_all()">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>