<style type="text/css">
  .realizarventa{
      text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
  }
  .img_icon{
    width: 36px;
  }
  .img_icon_c{
    width: 33px;
  }
</style>
<div class="page-body">
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
      <div class="col-lg-4">
        <div class="form-group row">
          <div class="col-12">
            <input class="form-control" type="search" placeholder="Buscar cotización" id="searchtext" oninput="search()">
          </div>
        </div>
      </div> 
      <div class="col-lg-3">
        <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
        <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
      </div> 
      <div class="col-lg-2">
        <?php if($idpersonal==1){?>
          <select class="form-control" id="sucursal" onchange="reload_registro()">
            <option value="0">Todas las sucursales</option>
            <?php foreach ($sucrow as $x){ ?>
                <option value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
            <?php } ?>
          </select>
        <?php }else{ ?>
          <input type="hidden" id="sucursal" value="<?php echo $sucursal ?>"> 
        <?php } ?>       
      </div>  
    </div>  
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12">
            <table class="table table-sm" id="table_datos">
              <thead>
                <tr>
                  <th scope="col">Folio</th>
                  <th scope="col">Sucursal</th>
                  <th scope="col">Cliente</th>
                  <th scope="col">Vendedor</th>
                  <th scope="col">Fecha creación</th>
                  <th scope="col">Productos</th>
                  <th scope="col">Acciones</th>
                  <th scope="col">Estatus</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>


<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Productos</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <div class="text_tabla_productos"></div>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_cotizacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Reenviar cotización</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">    
          <div class="row">
              <div class="col-md-12">
                <label class="form-label">Correo electrónico</label>
                <input class="form-control" type="email" id="correo_cl">
              </div>
          </div>    
        </div>    
      </div>
      <div class="modal-footer">
        <a type="button" class="btn realizarventa btn-sm" data-bs-dismiss="modal">Cerrar</a>
        <a type="button" class="btn realizarventa btn-sm" style="width: 40%" onclick="enviar_correo_cotizacion()">Enviar correo</a>
      </div>
    </div>
  </div>
</div>