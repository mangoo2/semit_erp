<style type="text/css">
  #table_datos_filter{
    display: none;
  }
  .actionactivo{
    background-color: #93ba1f !important;border-color: #93ba1f !important;
  }
  .bloques_card{
    width: 50%;
    float: left;
    padding: 8px;
  }
  .table_datos_tb .table_datos_top{
    display: none;
  }
</style>
<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">     
            <div class="row">
                <div class="col-lg-2">
                    <label>Sucursal</label>
                    <select class="form-control" id="suc_selected" onchange="loadtable()">
                        <?php
                            $op='<option value="0" data-clave="x">Seleccionar Sucursal</option>';
                            foreach ($sucrow->result() as $item) {
                                $op.='<option value="'.$item->id.'" data-clave="'.$item->clave.'">'.$item->name_suc.'</option>';
                            }
                            echo $op;
                        ?>
                    </select>
                </div>  
                <div class="col-lg-2">
                    <label>Vendedor</label>
                    <select class="form-control" id="eje_selected" onchange="loadtable()">
                        <option selected=""  value="0">Seleccionar vendedor</option>
                            <?php foreach ($get_vendedores->result() as $item) {
                                echo '<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
                            }?>
                    </select>
                </div>  
                <div class="col-lg-2">
                    <label>Mes</label>
                    <select class="form-control" id="mes_selected" onchange="loadtable()">
                        <option value="0">Seleccione mes</option>
                        <option value="01">Enero</option>
                        <option value="02">Febrero</option>
                        <option value="03">Marzo</option>
                        <option value="04">Abril</option>
                        <option value="05">Mayo</option>
                        <option value="06">Junio</option>
                        <option value="07">Julio</option>
                        <option value="08">Agosto</option>
                        <option value="09">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                      </select>
                </div>
                <div class="col-lg-2">
                    <label>Exportar comisiones</label>
                    <div class="col-lg-12">
                        <button title="Exportar registros de comisiones" id="btn_export" type="button" class="btn btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    </div>
                </div>  
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b" style="margin-top: 10px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="card-body table_datos_tb">
                                <h3 style="color: #1770aa;">Comisiones de productos y recargas</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12"><br></div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="card-body table_datos_tb_serv">
                                <h3 style="color: #1770aa;">Comisiones por servicios de mtto.</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12"><br></div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div id="cont_prods">
                                <h3 style="color: #1770aa;">Productos Vendidos</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12"><br></div>


                <div class="row">
                    <div class="col-lg-6">
                        <div class="">
                            <div class="card-body">
                                <div id="basic-apex2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-body">
                            <div class="">
                                <div class="col-md-12" style="text-align: center;">
                                    <h3 style="color: #1770aa;">Mejores vendedores</h3>
                                </div>
                                <div class="col-md-12 info_top_vent"></div>
                            </div>
                        </div>
                    </div>
                </div>
                              
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<div class="modal fade" id="modalDetalle" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Ventas Canceladas</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-12" id="cont_cancel"> 

                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>                   
                </div>   
            </div>
        </div>
    </div>
</div>
