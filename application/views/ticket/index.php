<style type="text/css">
  #table_datos_filter{
    display: none;
  }
  .actionactivo{
    background-color: #93ba1f !important;border-color: #93ba1f !important;
  }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">

            <div class="row">
              <div class="col-lg-4">
                <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
                <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
              </div>
            </div>    
            <br>  
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar persona" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <select class="form-control" id="estatus" onchange="reload_registro()">
                        <option value="0">Seleccionar estatus</option>
                        <option value="1">Por revisar</option>
                        <option value="2">Revisión</option>
                        <option value="3">Finalizado</option>
                    </select>
                </div>  
                <div class="col-lg-2">
                    <select class="form-control" id="tipoticket" onchange="reload_registro()">
                        <option value="0">Seleccionar tipo de ticket</option>
                        <option value="1">Error de sistema</option>
                        <option value="2">Ajuste de base de datos</option>
                        <option value="3">Cambio o Ajuste</option>
                        <option value="4">Nuevo modulo</option>
                        <option value="5">Capacitacion</option>
                        <option value="6">Error de red</option>
                        <option value="7">Error de servidor</option>
                    </select>
                </div> 
                <div class="col-lg-4" align="right"> 
                    <a href="<?php echo base_url() ?>Ticket/registro" class="btn btn-primary">Nuevo ticket</a>
                </div>   
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Folio ticket</th>
                                        <th scope="col">Personal</th>
                                        <th scope="col">Fecha y hora de tickets</th>
                                        <th scope="col">Asunto</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col">Tipo de tickets</th>
                                        <th scope="col">Personal Mangoo</th>
                                        <th scope="col">Fecha respuesta</th>
                                        <th scope="col">Ver ticket</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
