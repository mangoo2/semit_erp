<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Ticket" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <input type="hidden" id="key" value="<?php echo $key ?>">
                        <form id="form_registro" method="post">
                            <input type="hidden" name="id" id="idreg" value="<?php echo $id ?>">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Asunto<span style="color: red">*</span></label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="text" name="asunto" value="<?php echo $asunto ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                  <h3>Mensaje</h3>
                                  <textarea id="mensaje" name="mensaje" cols="30" rows="10"><?php echo $mensaje ?></textarea>
                                </div>
                            </div>     
                        </form>    
                        <h3>Imágenes</h3>
                        <div class="row g-3">
                            <div class="col-md-4">
                                <input type="file" id="files" name="files[]" multiple accept="image/*">
                            </div>
                            <div class="col-md-8">
                                <div class="lis_catacter"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <button class="btn btn-primary btn_registro" style="width: 100%" onclick="guardar_registro()">Guardar ticket</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>