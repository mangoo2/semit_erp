<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Ticket" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                           <input type="text" id="key">
                           <div class="row">
                                <div class="col-lg-12">
                                    <button class="btn btn-primary btn_registro" type="button" style="width: 100%; font-size: 30px;" onclick="responder_ticket()">Responder ticket <i class="fas fa-eye" style="font-size: 30px;"></i></button> 
                                </div>
                            </div> 
                            <br>
                            <div class="txt_respuesta" style="display: none">
                                <form id="form_registro" method="post">
                                    <input type="hidden" name="id" id="idreg" value="0">
                                    <input type="hidden" name="idticket" id="idreg" value="<?php echo $idticket ?>">
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <h3>Mensaje</h3>
                                          <textarea id="mensaje" name="mensaje" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>     
                                </form> 
                                <h3>Imágenes</h3>
                                <div class="row g-3">
                                    <div class="col-md-4">
                                        <input type="file" id="files" name="files[]" multiple accept="image/*">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="lis_catacter"></div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <button class="btn btn-primary btn_registro" style="width: 100%" onclick="guardar_registro()">Guardar respuesta</button>
                                    </div>
                                </div>  
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Desplegar skaeditor y agregar imagenes -->
                                    <!-- agregar plugin de color -->
                                    <!-- Nombre del queiesta levantando y fecha y hora respuesta 
                                        cuandro pequeño iamgenes dar click sale en un modal mas grande
                                        texto y en un modal las imagenes
                                    --> 
                                        <div class="default-according style-1" id="accordionoc">
                                          <div class="card">
                                            <div class="card-header bg-primary">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link text-white" data-bs-toggle="collapse" data-bs-target="#collapseicon" aria-expanded="true" aria-controls="collapse11"><i class="icofont icofont-briefcase-alt-2"></i> Collapsible Group Item #<span>1</span></button>
                                              </h5>
                                            </div>
                                            <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-bs-parent="#accordionoc">
                                              <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                            </div>
                                          </div>
                                          <div class="card">
                                            <div class="card-header bg-primary">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link collapsed text-white" data-bs-toggle="collapse" data-bs-target="#collapseicon1" aria-expanded="false"><i class="icofont icofont-support"></i> Collapsible Group Item #<span>2</span></button>
                                              </h5>
                                            </div>
                                            <div class="collapse" id="collapseicon1" aria-labelledby="headingeight" data-bs-parent="#accordionoc">
                                              <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                            </div>
                                          </div>
                                          <div class="card">
                                            <div class="card-header bg-primary">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link collapsed text-white" data-bs-toggle="collapse" data-bs-target="#collapseicon2" aria-expanded="false" aria-controls="collapseicon2"><i class="icofont icofont-tasks-alt"></i> Collapsible Group Item #<span>3</span></button>
                                              </h5>
                                            </div>
                                            <div class="collapse" id="collapseicon2" data-bs-parent="#accordionoc">
                                              <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                            </div>
                                          </div>
                                        </div>
                                    <!--  -->
                                    <!--  -->
                                </div>
                            </div>        
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

