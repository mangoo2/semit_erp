<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">
<style type="text/css">
  .slider {
        width: 100%;
        /*margin: 50px auto;*/
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }

    .slick-track{
      opacity: 1;
      width: 553px !important;
      transform: translate3d(0px, 0px, 0px);
    }

</style>
<input type="hidden" id="idcategoria_aux" value="<?php echo $categoria ?>">
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Productos" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <input type="hidden" id="idreg" value="<?php echo $id ?>">
      <div class="card-body">
        <button class="btn btn-primary" onclick="vistas_add()">Agregar pestaña</button>
        <hr>
        <ul class="nav nav-tabs pestana" id="icon-tab" role="tablist">
        </ul>
        <div class="tab-content pestana2" id="icon-tabContent">
          
        </div>
      </div>
    </div>  
  </div>
</div>   