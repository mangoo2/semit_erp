<style type="text/css">
    #sucursal option:disabled {
        background: #e4e6ef;
    }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Usuarios" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <form id="form_registro" method="post">
                            <input type="hidden" name="personalId" value="<?php echo $personalId ?>">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nombre del empleado<span style="color: red">*</span></label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Puesto</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="puesto" value="<?php echo $puesto ?>">
                                        </div>
                                        <label class="col-lg-2 col-form-label">Celular</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="number" name="celular" value="<?php echo $celular ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Correo</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="email" name="correo" value="<?php echo $correo ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <h4>Foto</h4>
                                    <div class="image-input image-input-outline" id="kt_image_1">
                                        <?php if($foto==''){ ?>
                                            <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>images/avatar.png)"></div>
                                        <?php }else { ?>
                                            <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>uploads/personal/<?php echo $foto ?>)"></div>
                                        <?php }?>    
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pencil-alt"></i>
                                            <input type="file" name="profile_avatar" id="foto_avatar" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" name="profile_avatar_remove" />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                </div>  
                            </div>    

                            <div class="row">
                                <div class="col-lg-12">
                                    <h4>Dirección</h4>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Calle</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" type="text" name="calle" value="<?php echo $calle ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">                                        
                                        <label class="col-lg-2 col-form-label">No. ext</label>
                                        <div class="col-lg-2">
                                            <input class="form-control" type="number" name="no_ext" value="<?php echo $no_ext ?>">
                                        </div>
                                        <label class="col-lg-2 col-form-label">No. int</label>
                                        <div class="col-lg-2">
                                            <input class="form-control" type="number" name="no_int" value="<?php echo $no_int ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Colonia</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="colonia" value="<?php echo $colonia ?>">
                                        </div>
                                        <label class="col-lg-2 col-form-label">Municipio</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="municipio" value="<?php echo $municipio ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Estado</label>
                                        <div class="col-lg-4">
                                            <select name="estado" id="estado" class="form-control">
                                                <option value="">Seleccionar</option>
                                              <?php foreach ($estadorow->result() as $item) { ?>
                                                      <option value="<?php echo $item->id;?>" <?php if($estado==$item->id){ echo 'selected';}?> ><?php echo $item->estado;?></option>
                                                    <?php } ?> 
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-form-label">C.P</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="number" name="cp" value="<?php echo $cp ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">País</label>
                                        <div class="col-lg-4">
                                            <select name="pais" id="pais" class="form-control">
                                              <option value="MEX">MEX - México</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Seguro social</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="nss" value="<?php echo $nss ?>">
                                        </div>
                                    </div>
                                    
                                    <hr>
                                    <div class="row">
                                        <input type="hidden" name="UsuarioID" value="<?php echo $UsuarioID ?>">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="example-search-input" class="col-lg-4 col-form-label">Perfil<span style="color: red">*</span></label>
                                                <div class="col-lg-7">
                                                    <select class="form-control" name="perfilId" id="perfilId" onchange="perfil_change()">
                                                        <option value="">Seleccionar</option>
                                                        <?php foreach ($perfil as $x){ ?>
                                                            <option data-tecnico="<?php echo $x->tipo_tecnico; ?>" value="<?php echo $x->perfilId ?>" <?php if($x->perfilId==$perfilId) echo 'selected' ?>><?php echo $x->nombre ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label">Comisión</label>
                                                <div class="col-lg-4">
                                                    <input class="form-control" type="number" name="comision" id="comision" min="0" max="30" value="<?php echo $comision ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label">Nombre de usuario<span style="color: red">*</span></label>
                                                <div class="col-lg-7">
                                                    <input class="form-control" type="text" name="Usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()" value="<?php echo $Usuario ?>">
                                                </div>   
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label">Contraseña<span style="color: red">*</span></label>
                                                <div class="col-lg-7">
                                                    <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password" oninput="" value="<?php echo $contrasena ?>">
                                                </div>  
                                                <div class="col-lg-1">
                                                    <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                                        <i class="icon-xl fas fa-eye" style="color: white"></i>
                                                    </a>
                                                </div> 
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label">Confirmar contraseña<span style="color: red">*</span></label>
                                                <div class="col-lg-7">
                                                    <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" value="<?php echo $contrasena2 ?>">
                                                </div>   
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row sucursal_div">
                                                <label for="example-search-input" class="col-lg-4 col-form-label">Sucursal</label>
                                                <div class="col-lg-7">
                                                    <select class="form-control" name="sucursal" id="sucursal" onchange="select_sucursal()">
                                                        <option value="0">Seleccionar</option>
                                                        <?php foreach ($sucrow->result() as $x){ ?>
                                                            <option class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>" <?php if($x->id==$sucursal) echo 'selected' ?>><?php echo $x->name_suc ?></option>
                                                        <?php $id_ult=$x->id; } ?>
                                                    </select>
                                                    <input type="hidden" id="id_ult" value="<?php echo $id_ult; ?>">
                                                </div>
                                            </div>
                                            <?php 
                                                
                                                if($sucursal==2){
                                                    $ocultar='style="display:block;"';
                                                }else{
                                                    $ocultar='style="display:none;"';
                                                }
                                            ?>
                                            <div class="pin_div" <?php echo $ocultar ?>>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Pin de venta</label>
                                                    <div class="col-lg-7">
                                                        <input class="form-control" type="number" name="pin" oninput="v_pin()" id="pin" value="<?php echo $pin ?>">
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="form-group row cont_porc_tec">
                                                <label class="col-lg-4 col-form-label">$ Comisión x servicio</label>
                                                <div class="col-lg-4">
                                                    <input class="form-control" type="number" name="comision_tecnico" id="comision_tecnico" min="0" value="<?php echo $comision_tecnico ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" style="color: #019cde;"><b>Desactivar usuario</b></label>
                                                <div class="col-lg-2">
                                                    <span class="switch switch-icon">
                                                        <label>
                                                            <input type="checkbox" name="acceso" id="acceso" onclick="check_acceso()" <?php if($acceso==0) echo 'checked' ?>>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>  
                                                <div class="col-lg-5"> 
                                                    <div class="ocultar_motivo" style="display: none">
                                                        <textarea rows="4" class="form-control" name="motivo_desactivar"><?php echo $motivo_desactivar ?></textarea>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>    
                        </form>    
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <button class="btn btn-primary btn_registro" style="width: 100%" onclick="guardar_registro()">Guardar empleado</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>