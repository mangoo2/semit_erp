<?php
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_usuarios_".$anio.".xls");
   
?>

<table  border="1">
    <thead>
        <tr>
            <th>Empleado</th>
            <th>Usuario del sistema</th>
            <th>Perfil</th>
            <th>Puesto</th>
            <th>Sucursal</th>
            <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($get_usuarios as $x){
            $estatus='';
            if($x->acceso==0){
                    $estatus='Suspendido';
                }else{
                    $estatus='Activo';
                }
         ?>
        <tr>
            <td><?php echo utf8_decode($x->nombre) ?></td>
            <td><?php echo utf8_decode($x->Usuario) ?></td>
            <td><?php echo utf8_decode($x->perfil) ?></td>
            <td><?php echo utf8_decode($x->puesto) ?></td>
            <td><?php echo utf8_decode($x->name_suc) ?></td>
            <td><?php echo utf8_decode($estatus) ?></td>
        </tr>  
        <?php } ?>
    </tbody>
</table>
