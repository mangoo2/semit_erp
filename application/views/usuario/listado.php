<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar usuario" id="empleado_busqueda" autocomplete="new-text" oninput="reload_registro()">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-2">
                    <select class="form-control" id="desactivar" onchange="reload_registro()">
                        <option value="2">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Suspendido</option>
                      </select>
                </div> 
                <div class="col-lg-2">
                    <select class="form-control" id="sucursal" onchange="reload_registro()">
                        <option value="0">Todas las sucursales</option>
                        <?php foreach ($sucrow->result() as $x){ ?>
                            <option value="<?php echo $x->id ?>"><?php echo $x->name_suc ?></option>
                        <?php } ?>
                      </select>
                </div>
                <div class="col-lg-4" align="right"> 
                    <a onclick="get_excel()" style="cursor: pointer;"><img style="width: 38px;" src="<?php echo base_url() ?>public/img/excel.png"></a>
                    <a href="<?php echo base_url() ?>Usuarios/registro" class="btn btn-primary">Nuevo usuario</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Empleado</th>
                                        <th scope="col">Usuario del sistema</th>
                                        <th scope="col">Perfil</th>
                                        <th scope="col">Puesto</th>
                                        <th scope="col">Sucursal</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modal_suspension" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Motivo de suspensión</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <div class="motivotxt"></div>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>