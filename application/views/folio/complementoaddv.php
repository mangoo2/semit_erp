<style type="text/css">
    .error {
        margin: 0px;
        color: red;
    }
    .colort,.colorr{
        font-size: 16px;
        font-weight: bold;
    }
    .colort{
        color: #009bdb;
    }
    .colorr{
        color: #c6d420;
    }
</style>
<?php 
    $totalpagos=0;
    foreach ($faccomp->result() as $item) {
        if($item->Estado==1){
            $totalpagos=$totalpagos+$item->ImpPagado;
        }
    }
    
    $restante = $fac->total-$totalpagos;
?>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <h3>Listado de ventas 
              <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
              <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
            </h3>
            <div class="row">
                <div class="col-md-1 colort">
                    Cliente:
                </div>
                <div class="col-md-11 colorr">
                    <?php echo $fac->Nombre;?>
                </div>
                <div class="col-md-1 colort">
                    Saldo:
                </div>
                <div class="col-md-1 colorr">
                    $ <?php echo number_format($fac->total, 2, '.', ',');?>
                </div>
                <div class="col-md-1 colort">
                    Restante:
                </div>
                <div class="col-md-1 colorr">
                    $ <?php echo number_format($restante, 2, '.', ',');;?>
                </div>
                <div class="col-md-8">
                    <?php
                        if($perfilid==1){ 
                            if($restante>0){ ?>
                            <a href="<?php echo base_url().'Folio/complementoadd/'.$facturaid; ?>" class="btn btn-primary" target="_blank">Agregar Complemento</a>
                    <?php 
                            } 
                        } ?>
                </div>
            </div> 
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">

                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Número de parcialidad</th>
                                        <th scope="col">Monto</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($faccomp->result() as $item) {
                                        $btn_filefac='<a href="'.base_url().'Folio/complementodoc/'.$item->complementoId.'" class="btn btn-primary" target="_blank"><i class="fas fa-file-pdf"></i></a>
                                                      <a href="'.base_url().$item->rutaXml.'" class="btn btn-primary" target="_blank" download=""><i class="far fa-file-code"></i></a>  ';
                                        if($item->Estado==0){
                                            $tag_estado='<span class="badge badge-danger">Cancelada</span>';
                                            
                                        }
                                        if($item->Estado==1){
                                            $tag_estado='<span class="badge badge-success">Timbrada</span>';
                                        }
                                        if($item->Estado==2){
                                            $tag_estado='<span class="badge badge-warning">Sin timbrar</span>';
                                            $btn_filefac='<button type="button" class="btn btn-primary" onclick="retimbrar_com('.$item->complementoId.')"><span class="fa-stack"><i class="fas fa-file fa-stack-2x"></i><i class="fas fa-sync fa-stack-1x" style="color: red;"></i></span></button>';
                                        }
                                        $html='<tr>
                                                <td scope="col">'.$item->complementoId.'</td>
                                                <td scope="col">'.$item->fechatimbre.'</td>
                                                <td scope="col">'.$item->nombre.'</td>
                                                <td scope="col">'.$item->NumParcialidad.'</td>
                                                <td scope="col">'.$item->ImpPagado.'</td>
                                                <td scope="col">'.$tag_estado.'</td>
                                                <td scope="col">'.$btn_filefac.'</td>
                                            </tr>';
                                        echo $html;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>