<style type="text/css">
.error {margin: 0px;color: red;}
.colort,.colorr{font-size: 16px;font-weight: bold;}
.colort{color: #009bdb;}
.colorr{color: #c6d420;}
.select2-container{width: 100% !important;}
.div-btn{width: 110px;}
   
  .dataTables_filter label, .dataTables_paginate .pagination{
    float: right;
  }
  #tabla_facturacion_filter{
    display: none;
  }
  #tabla_facturacion_pp_filter{
    display: none;
  }
</style>
<input type="number" id="perfilid" value="<?php echo $perfilid;?>">
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <h3>Listado de facturas
    <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
    <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
    </h3>
    <div class="row">
      <div class="col-lg-3">
          <div class="form-group row">
              <div class="col-12">
                <div class="searchtext">
                  <input class="form-control" type="search" placeholder="Buscar facturas" id="searchtext" oninput="search()">
                </div>
                <div class="searchtext2" style="display: none">  
                  <input class="form-control" type="search" placeholder="Buscar complementos" id="searchtext2" oninput="search2()">
                </div>  
              </div>
          </div>
      </div> 
    </div>
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <ul class="nav nav-tabs nav-primary" id="pills-warningtab" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="pills-warninghome-tab" data-bs-toggle="pill" href="#pills-warninghome" role="tab" aria-controls="pills-warninghome" aria-selected="true" onclick="vista_x()"><img style="width: 20px;" src="<?php echo base_url() ?>public/img/bill-invoice.svg"> Facturas</a></li>
          <li class="nav-item"><a class="nav-link" id="pills-warningprofile-tab" data-bs-toggle="pill" href="#pills-warningprofile" role="tab" aria-controls="pills-warningprofile" aria-selected="false" onclick="loadtable_fpp()"><img style="width: 20px;" src="<?php echo base_url() ?>public/img/pay.svg">&nbsp;Complementos</a></li>
          <!--<li class="nav-item"><a class="nav-link" id="pills-warningcontact-tab" data-bs-toggle="pill" href="#pills-warningcontact" role="tab" aria-controls="pills-warningcontact" aria-selected="false"><i class="icofont icofont-contacts"></i>Contact</a></li>-->
        </ul>
        <div class="tab-content" id="pills-warningtabContent">
          <div class="tab-pane fade active show" id="pills-warninghome" role="tabpanel" aria-labelledby="pills-warninghome-tab">
            <!-------------------------------------------------------->
            <div class="row">
              <div class="col-md-12" style="text-align: end;">
                <br>
                <a class="btn btn-primary" href="<?php echo base_url();?>Facturas/add">Generar Factura</a>&nbsp;&nbsp;&nbsp;
                <?php if($perfilid==1){ ?>
                  <button class="btn btn-danger" onclick="cancelarfacturas()">Cancelar Factura</button>
                <?php } ?>&nbsp;&nbsp;
                <br><br>
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-2" style="display: <?php echo $vendedor; ?>;">
                <label>Vendedor</label>
                <select id="personals" class="form-control filtrosdatatable" onchange="loadtable()">
                  <option value="0">Todos</option>
                  <?php foreach ($personalrows->result() as $item) { ?>
                  <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> </option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-2">
                <label>Cliente</label>
                <select id="idcliente" class="form-control filtrosdatatable" onchange="loadtable()">
                  <?php if(isset($_GET['clienteid'])){ ?>
                  <option value="<?php echo $_GET['clienteid'];?>"><?php echo $_GET['cliente'];?></option>
                  <?php } ?>
                  <option value="0">Todos</option>
                </select>
              </div>
              
              <div class="col-md-2">
                <label>Fecha Inicio</label>
                <input type="date" id="finicial" class="form-control filtrosdatatable" value="<?php echo $fechainicial_reg;?>" onchange="loadtable()">
              </div>
              <div class="col-md-2">
                <label>Fecha Fin</label>
                <input type="date" id="ffinal" class="form-control filtrosdatatable" onchange="loadtable()">
              </div>
              <div class="col-md-2 selectweb">
                <label>Método de pago</label>
                <select id="rs_metodopago" class="form-control filtrosdatatable" onchange="loadtable()">
                  <option value="0">Todas</option>
                  <option value="1">Pago en una sola exhibición</option>
                  <option value="2">Pago en parcialidades o diferido</option>
                </select>
              </div>
              <div class="col-md-2 selectweb">
                <label>Tipo CFDI</label>
                <select id="rs_tcfdi" class="form-control filtrosdatatable" onchange="loadtable()">
                  <option value="0">Todas</option>
                  <option value="1">Facturas</option>
                  <option value="2">Factura 01 Nota Credito</option>
                  <option value="3">Factura 04 Sustitucion</option>
                  <!--<option value="4">Factura de Anticipo recibido</option>
                  <option value="5">Factura con relacion de Anticipo</option>
                  <option value="6">Factura Egreso de Anticipo</option>-->
                  <option value="7">Carta porte</option>
                </select>
              </div>
              
              
            </div>
            
            <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display table" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Folio</th>
                  <th>Cliente</th>
                  <th>RFC</th>
                  <th>Monto</th>
                  <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                  <th>Fecha</th>
                  <th>Método de pago</th>
                  <th>Personal</th>
                  <th>Tipo</th>
                  <th></th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <!-------------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="pills-warningprofile" role="tabpanel" aria-labelledby="pills-warningprofile-tab">
            <!-------------------------------------------------------->
            <div class="row">
              <div class="col-md-12" style="text-align: end;">
                <a class="btn btn-danger" onclick="cancelarcomplementos()">Cancelar Complemento</a>
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-2" style="display: <?php echo $vendedor; ?>;">
                <label>Vendedor</label>
                <select id="personals_pp" class="browser-default form-control" onchange="loadtable_fpp()">
                  <option value="0">Todos</option>
                  <?php foreach ($personalrows->result() as $item) { ?>
                  <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-2">
                <label>Cliente</label>
                <select id="idcliente_pp" class="browser-default form-control" onchange="loadtable_fpp()">
                  <?php if(isset($_GET['clienteid'])){ ?>
                  <option value="<?php echo $_GET['clienteid'];?>"><?php echo $_GET['cliente'];?></option>
                  <?php } ?>
                  <option value="0">Todos</option>
                </select>
              </div>
              
              <div class="col-md-2">
                <label>Fecha Inicio</label>
                <input type="date" id="finicial_pp" class="form-control" value="<?php echo $fechainicial_reg;?>" onchange="loadtable_fpp()">
              </div>
              <div class="col-md-2">
                <label>Fecha Fin</label>
                <input type="date" id="ffinal_pp" class="form-control" onchange="loadtable_fpp()">
              </div>
              
              
              
            </div>
            <table id="tabla_facturacion_pp" class="responsive-table display table" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Cliente</th>
                  <th>Folios</th>
                  <th>Monto</th>
                  <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                  <th>Fecha</th>
                  <th>creado</th>
                  <th></th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <!-------------------------------------------------------->
          </div>
          <div class="tab-pane fade" id="pills-warningcontact" role="tabpanel" aria-labelledby="pills-warningcontact-tab">
            
          </div>
        </div>
        
        
      </div>
    </div>
    <!--end::Container-->
  </div>
  <!--end::Entry-->
</div>

<div class="modal fade" id="modal_fact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Reenviar cotización</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">    
          <div class="row">
              <div class="col-md-12">
                <label class="form-label">Correo electrónico</label>
                <input class="form-control" type="email" id="correo_cl">
              </div>
          </div>    
        </div>    
      </div>
      <div class="modal-footer">
        <a type="button" class="btn realizarventa btn-sm" data-bs-dismiss="modal">Cerrar</a>
        <a type="button" class="btn realizarventa btn-sm" style="width: 40%" onclick="enviar_correo_cotizacion()">Enviar correo</a>
      </div>
    </div>
  </div>
</div>