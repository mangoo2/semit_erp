<input type="hidden" id="mesactual" value="<?php echo date('m');?>">
<input type="hidden" id="anioactual" value="<?php echo date('Y');?>">
<input type="hidden" id="succlave" value="<?php echo $succlave;?>">
<style type="text/css">
.error{    color: red;  margin: 0 0 0;}
.test5{    display: none !important;  }
.table_pg td{    padding: 5px;  }
.ifrafac{
    width: 100%;
    height: 412px;
    border: 0;
  }
  .infoventaspg td{
    font-size: 11px;
  }
  .tdtablepg{
    min-width: 130px;
  }
  .loading-overlay{
    z-index: 1050 !important;
  }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <h3>Listado de facturas
    <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
    <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
    </h3>
    
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <?php if($perfilid==1){ ?>
        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-primary right" type="button" onclick="mpublicogeneral()">Generar global</button>
          </div>
        </div>
        <?php } ?>
        <!------------------------------------------>
        <form id="validateSubmitForm" method="post" autocomplete="off">
          <div class="row">
            <div class="col-md-6">
              <label for="idcliente">Cliente</label> <!--Llenar -->
              <!--<input id="idcliente" name="idcliente" type="text" value="" >-->
              <select class="form-control" name="idcliente" id="idcliente" required>
                
              </select>
              
            </div>
            <div class="col-md-6">
              <label for="">RFC</label> <!--Llenar -->
              <select class="form-control " name="rfc" id="rfc" required onchange="verficartiporfc()">
                
              </select>
            </div>
            <div class="col-md-6"></div><div class="col-md-6 infodatosfiscales"></div>
          </div>
          <div class="row agregardatospublicogeneral">
            
          </div>
          <div class="row">
            <div class="col-md-6">
              
              <label class="active">Método de pago</label>
              <select id="FormaPago" name="FormaPago" class="form-control" required>
                <?php foreach ($metodo->result() as $key) { ?>
                <option value="<?php echo $key->metodopago; ?>"><?php echo $key->metodopago_text; ?></option>
                <?php } ?>
              </select>
              
            </div>
            <div class="col-md-6">
              <label class="active">Forma de pago</label>
              <select  id="MetodoPago" name="MetodoPago" class="form-control" required>
                <option value="" disabled selected>Selecciona una opción</option>
                <?php foreach ($forma->result() as $key) { ?>
                <option value="<?php echo $key->clave; ?>"><?php echo $key->formapago_text; ?></option>
                <?php } ?>
              </select>
              
            </div>
          </div>
          <div class="row" style="display:none;">
            <div class="input-field col-md-6">
              <input id="" disabled="" name="" type="text" value="">
              <label for="">Fecha Aplicación</label> <!--Llenar -->
            </div>
            <div class="input-field col-md-6">
              <input id="" disabled="" name="" type="text" value="">
              <label for="">Lugar Expedición</label> <!--Llenar -->
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="" class="active">Tipo Moneda</label> <!--Llenar -->
              <select id="moneda" name="moneda" class="span12 form-control browser-default" required>
                <option value="MXN">MXN Peso Mexicano</option>
                <option value="USD">Dólares Norteamericanos</option>
                <option value="EUR">Euro</option>
              </select>
              
            </div>
            <div class="col-md-6">
              <label class="active">Uso CFDI</label>
              <select id="uso_cfdi" name="uso_cfdi" class="form-control " required>
                <option value="" disabled selected>Selecciona una opción</option>
                <?php foreach ($cfdi->result() as $key) { ?>
                <option value="<?php echo $key->uso_cfdi; ?>" class="option_<?php echo $key->uso_cfdi; ?> pararf <?php echo str_replace(',',' ',$key->pararf) ?>" disabled><?php echo $key->uso_cfdi_text; ?></option>
                <?php } ?>
              </select>
              
            </div>
            <div class="col-md-6">
              <label for="CondicionesDePago">Condicion de Pago</label>
              <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="form-control" onpaste="return false;">
            </div>
            <div class="col-md-6 divtipoComprobante">
              <label for="CondicionesDePago">Tipo Comprobante</label>
              <select id="TipoComprobante" name="TipoComprobante" class="form-control" required>
                <option value="I" >Ingreso</option>
                <option value="E" >Egreso</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              
              
              <input type="checkbox" class="filled-in" id="facturarelacionada" >
              <label for="facturarelacionada">Factura Relacionada</label>
              
              <div class="form-group divfacturarelacionada" style="display:none;">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Tipo Relacion</label>
                    <select class="form-control" id="TipoRelacion" onchange="selecttiporelacion()">
                      
                      <option value="01">01 Nota de crédito de los documentos relacionados</option>
                      <!--<option value="02">02 Nota de débito de los documentos relacionados</option>
                      <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                      -->
                      <option value="04">04 Sustitución de los CFDI previos</option>
                      <!--
                      <option value="05">05 Traslados de mercancias facturados previamente</option>
                      <option value="06">06 Factura generada por los traslados previos</option>-->
                      <option value="07">07 CFDI por aplicación de anticipo</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Folio Fiscal</label>
                    <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row tarjeta_datos" style="display: none;">
            <div class="input-field col-md-6">
              <i class="material-icons prefix">credit_card</i>
              <input id="tarjeta" name="tarjeta" type="number" value="">
              <label for="">Últimos Dígitos:</label> <!--Llenar -->
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <hr>
            </div>
            <div class="col-md-12">
              <div class="default-according" id="accordion">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                    <a class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Pedimento</a>
                    </h5>
                  </div>
                  <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-bs-parent="#accordion" style="">
                    <div class="card-body">
                      <div class="input-field col-md-6">
                        <i class="material-icons prefix"></i>
                        <input id="numproveedor" name="numproveedor" type="text" value="">
                        <label for="">Número de proveedor</label> <!--Llenar -->
                      </div>
                      <div class="input-field col-md-6">
                        <i class="material-icons prefix"></i>
                        <input id="numordencompra" name="numordencompra" type="text" value="">
                        <label for="">Número de orden de compra</label> <!--Llenar -->
                      </div>
                      <div class="input-field col-md-6">
                        <textarea id="observaciones" name="observaciones" class="materialize-textarea" placeholder=" "></textarea>
                        <label for="">Observaciones</label>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            
          </div>
        </form>
        
        
        <!-- inicio -->
        <div class="row">
          <div class="col s12">
            <table id="table_conceptos" class="table striped">
              <thead>
                <tr>
                  <th width="8%">Cantidad</th>
                  <th width="14%">Unidad SAT</th>
                  <th width="14%">Concepto SAT</th>
                  <th width="23%">Descripción</th>
                  <th width="8%">Precio</th>
                  <th width="8%">Descuento</th>
                  <th width="10%">Monto</th>
                  <th width="7%">Aplicar iva</th>
                  <th width="10%"></th>
                </tr>
                <tr>
                  <th >
                    <input type="number" name="scantidad" id="scantidad" class="form-control" value="1" onchange="calculartotal()">
                  </th>
                  <th >
                  <select name="ssunidadsat" id="sunidadsat" class="form-control"></select>
                </th>
                <th >
                  <select name="sconseptosat" id="sconseptosat" class="form-control">
                    <option value="01010101">No existe en el catálogo</option>
                    
                  </select>
                </th>
                <th >
                  <input type="text" name="sdescripcion" id="sdescripcion" class="form-control">
                </th>
                <th >
                  <input type="number" name="sprecio" id="sprecio" class="form-control" value="0" onchange="calculartotal()">
                </th>
                <th class="" style="text-align: center;"></th>
                <th class="montototal" style="text-align: center;"></th>
                
                <th >
                  <input type="checkbox" class="filled-in" id="aplicariva" checked="checked">
                  <label for="aplicariva"></label>
                </th>
                <th >
                  <a class="btn btn-primary agregarconcepto" onclick="agregarconcepto()">Agregar</a>
                </th>
              </tr>
            </thead>
            <tbody class="addcobrar">
              
            </tbody>
          </table>
          
        </div>
      </div>
      <div class="row">
        <div class="col s8">
          
        </div>
        <div class="col s4">
          <div class="row">
            <div class="col-md-7" style="text-align: right;">
              Subtotal
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="Subtotalinfo" id="Subtotalinfo" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
              <input type="hidden" name="Subtotal" id="Subtotal" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row">
            <div class="col-md-7" style="text-align: right;">
              Descuento
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="descuentof" id="descuentof" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row">
            <div class="col-md-7" style="text-align: right;">
              I.V.A
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="iva" id="iva" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row" style="display: none">
            <div class="col-md-7" style="text-align: right;">
              <input type="checkbox" id="risr" />
              <label for="risr" onclick="calculartotales_set(1000)">10 % Retencion I.S.R.</label>
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="isr" id="isr" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row" style="display: none">
            <div class="col-md-7" style="text-align: right;">
              <input type="checkbox" id="riva" />
              <label for="riva" onclick="calculartotales_set(1000)">10.67 % Retencion I.V.A.</label>
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="ivaretenido" id="ivaretenido" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row" style="display: none">
            <div class="col-md-7" style="text-align: right;">
              <input type="checkbox" id="5almillar" />
              <label for="5almillar" onclick="calculartotales_set(1000)">5 al millar.</label>
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="5almillarval" id="5almillarval" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row" style="display: none">
            <div class="col-md-7" style="text-align: right;">
              <input type="checkbox" id="aplicaout" />
              <label for="aplicaout" onclick="calculartotales_set(1000)">6% Retencion servicios de personal.</label>
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="outsourcing" id="outsourcing" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          <div class="row">
            <div class="col-md-7" style="text-align: right;">
              Total
            </div>
            <div class="col s5">
              <span style="float: left;">$</span>
              <input type="" name="total" id="total" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
            </div>
          </div>
          
        </div>
      </div>
      <!-- fin -->
      <div class="row">
        <div class="botonfinalizar"></div>
        <div class="input-field col-md-9">
        </div>
        <div class="input-field col-md-3 guadarremove">
          <button class="btn btn-primary right registrofac_preview" type="button">Facturar</button>
          <!--<button class="btn cyan waves-effect waves-light right registrofac_save" type="button" style="margin-top: 10px;" onclick="registrofac(0,<?php echo $facturaId;?>)">Guardar<i class="material-icons right">save</i></button>-->
        </div>
      </div>
      <!------------------------------------------>
    </div>
  </div>
  <!--end::Container-->
</div>
<!--end::Entry-->
</div>




<div class="modal fade" id="modal_previefactura" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" style="padding: 0;">
        <div class="row">
          <div class="col-md-12 preview_iframe">
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-secondary registrofac" type="button">Facturar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mpublicogeneral" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" style="min-width: 900px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Obtener Ventas para Publico En General</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" >
        <div class="row">
        <?php
          $fecha = new DateTime();
          $fecha2 = new DateTime();
          $fecha->modify('first day of this month');
          $pg_fechai = $fecha->format('Y-m-d'); 
          $fecha2->modify('last day of this month');
          $pg_fechaf = $fecha2->format('Y-m-d');
        ?>
        <div class="col-md-3">
          <label>Fecha inicio</label>
          <input type="date" id="pg_fechai" class="form-control" value="<?php echo $pg_fechai;?>">
        </div>
        <div class="col-md-3">
          <label>Fecha Final</label>
          <input type="date" id="pg_fechaf" class="form-control" value="<?php echo $pg_fechaf;?>">
        </div>
        <div class="col-md-3">
          <label>Sucursal</label>
          <select class="form-control" id="vgs_sucursal">
            <?php foreach ($res_suc->result() as $item) { 
                if($suc==$item->id){
                  $selected=' selected ';
                }else{
                  $selected='';
                }
                echo '<option value="'.$item->id.'" '.$selected.' data-suc="'.$item->clave.'" data-cpcp="'.$item->cp_cp.'">'.$item->id.' - '.$item->name_suc.'</option>';
            }
            ?>
            
          </select>
        </div>
        
        <div class="col-md-2">
          <a class="btn btn-primary" onclick="obtenerpg()">Obtener</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 infoventaspg">
          
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <!--<button class="btn btn-secondary pgok" data-bs-dismiss="modal" type="button">Aceptar</button>-->
      </div>
    </div>
  </div>
</div>