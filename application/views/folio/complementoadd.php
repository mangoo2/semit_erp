<style type="text/css">
    
    .form-control[readonly] {
        background-color: #eeeeee;
    }
    input{
        background-color: white;
      }
      .error{
        color: red;
        margin: 0px;
      }
      .form-control .error{
        border: 1px solid #ea1e09 !important;
      }
      .IdDocumentos,.facturasuuid{
        font-size: 13px !important;
      }
      .intermitente{
        border: 1px solid black;
        padding: 20px 20px;
        box-shadow: 0px 0px 20px;
        animation: infinite resplandorAnimation 2s;
        
      }
    @keyframes resplandorAnimation {
      0%,100%{
        box-shadow: 0px 0px 20px red;
      }
      50%{
      box-shadow: 0px 0px 0px red;
      
      }

    }
    #tabledocumentosrelacionados .col-md-3,#tabledocumentosrelacionados .col-md-2,#tabledocumentosrelacionados .col-md-1{
        float: left;
    }
    .fixed-action-btn{
        position: fixed;
        right: 23px;
        bottom: 23px;
        padding-top: 15px;
        margin-bottom: 0;
        z-index: 997;
    }
    .ifrafac{
    width: 100%;
    height: 412px;
    border: 0;
  }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <h3>Listado de ventas 
              <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
              <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
            </h3>
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <input type="hidden" id="idclienteregresa" value="<?php echo $cliente;?>">
                    <!--------------------------------------->
                        <form id="validateSubmitForm" method="post" autocomplete="off"> 
                          <input type="hidden" name="clienteId" id="clienteId" class="form-control" value="<?php echo $cliente;?>" readonly required>
                            <div class="default-according">
                              <div class="card">
                                <div class="card-header" id="headingOne">
                                  <h5 class="mb-0">
                                    <a class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Datos del emisor</a>
                                  </h5>
                                </div>
                                <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-bs-parent="#accordion" style="">
                                    <div class="card-body">
                                        <!------------------------------------>
                                            <div class="row">
                                              <div class="col-md-6">
                                               <label>Nombre o razón social:</label>
                                               <input type="text" name="razonsocial" id="razonsocial" class="form-control" value="<?php echo $Nombre;?>" readonly required>
                                              </div>
                                              <div class="col-md-6">
                                               <label>RFC:</label>
                                               <input type="text" name="rfcemisor" id="rfcemisor" class="form-control" value="<?php echo $rFCEmisor;?>" readonly required>
                                              </div>
                                              <div class="col-md-6">
                                                <label>Régimen fiscal: <?php echo $Regimen;?></label>
                                                <select id="Regimen" name="Regimen" class="form-control browser-default">
                                                <?php if($Regimen==601){ ?>
                                                  <option value="601">601 General de Ley Personas Morales</option>
                                                <?php } ?>
                                                <?php if($Regimen==603){ ?>
                                                  <option value="603">603 Personas Morales con Fines no Lucrativos</option>
                                                <?php } ?>
                                                <?php if($Regimen==605){ ?>
                                                  <option value="605">605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                <?php } ?>
                                                <?php if($Regimen==606){ ?>
                                                  <option value="606">606 Arrendamiento</option>
                                                <?php } ?>
                                                <?php if($Regimen==607){ ?>
                                                  <option value="607">607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                <?php } ?>
                                                <?php if($Regimen==608){ ?>
                                                  <option value="608">608 Demás ingresos</option>
                                                <?php } ?>
                                                <?php if($Regimen==609){ ?>
                                                  <option value="609">609 Consolidación</option>
                                                <?php } ?>
                                                <?php if($Regimen==610){ ?>
                                                  <option value="610">610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                                <?php } ?>
                                                <?php if($Regimen==611){ ?>
                                                  <option value="611">611 Ingresos por Dividendos (socios y accionistas)</option>
                                                <?php } ?>
                                                <?php if($Regimen==612){ ?>
                                                  <option value="612">612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                                <?php } ?>
                                                <?php if($Regimen==614){ ?>
                                                  <option value="614">614 Ingresos por intereses</option>
                                                <?php } ?>
                                                <?php if($Regimen==615){ ?><option value="615">615 Régimen de los ingresos por obtención de premios</option>
                                                <?php } ?>
                                                <?php if($Regimen==616){ ?>
                                                  <option value="616">616 Sin obligaciones fiscales</option>
                                                <?php } ?>
                                                <?php if($Regimen==620){ ?>
                                                  <option value="620">620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                                <?php } ?>
                                                <?php if($Regimen==621){ ?>
                                                  <option value="621">621 Incorporación Fiscal</option>
                                                <?php } ?>
                                                <?php if($Regimen==622){ ?>
                                                  <option value="622">622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                <?php } ?>
                                                <?php if($Regimen==623){ ?>
                                                  <option value="623">623 Opcional para Grupos de Sociedades</option>
                                                <?php } ?>
                                                <?php if($Regimen==624){ ?>
                                                  <option value="624">624 Coordinados</option>
                                                <?php } ?>
                                                <?php if($Regimen==628){ ?>
                                                  <option value="628">628 Hidrocarburos</option>
                                                <?php } ?>
                                                <?php if($Regimen==629){ ?>
                                                  <option value="629">629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                                  <?php } ?>
                                                <?php if($Regimen==630){ ?>
                                                  <option value="630">630 Enajenación de acciones en bolsa de valores</option>
                                                <?php } ?>
                                                             
                                                </select> 
                                              </div>
                                              <div class="col-md-6">
                                                 <label>Tipo:</label>
                                                 <select name="tipof" id="tipof" class="form-control browser-default">
                                                   <option value="P">P Pago</option>
                                                 </select>
                                              </div>
                                            </div>
                                        <!------------------------------------>
                                    </div>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Datos del Receptor</a>
                                  </h5>
                                </div>
                                <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-bs-parent="#accordion" style="">
                                    <div class="card-body">
                                        <!------------------------------------>
                                            <div class="row">
                                              <div class="col-md-6">
                                               <label>Nombre o razón social:</label>
                                               <input type="text" name="razonsocialreceptor" id="razonsocialreceptor" class="form-control" value="<?php echo $razonsocialreceptor;?>" readonly required>
                                              </div>
                                              <div class="col-md-6">
                                               <label>RFC:</label>
                                               <input type="text" name="rfcreceptor" id="rfcreceptor" class="form-control" value="<?php echo $rfcreceptor;?>" readonly required>
                                              </div>
                                              <div class="col-md-6">
                                                <label>Uso:</label>
                                                <select id="UsoCFDI" name="UsoCFDI" class="form-control browser-default">
                                                  <option value="P01">P01 Por definir</option>
                                                </select>
                                              </div>
                                            </div>
                                        <!------------------------------------>
                                    </div>
                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header" id="headingThree">
                                  <h5 class="mb-0">
                                    <a class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Comprobante</a>
                                  </h5>
                                </div>
                                <div class="collapse show" id="collapseThree" aria-labelledby="headingThree" data-bs-parent="#accordion">
                                    <div class="card-body">
                                        <!------------------------------------>
                                            <div class="row">
                                              <div class="col-md-4">
                                               <label>Fecha y hora de expedición:</label>
                                               <input type="datetime-local" name="Fecha" id="Fecha" class="form-control" value="<?php echo $Fecha;?>" required readonly>
                                              </div>
                                              <div class="col-md-4">
                                               <label>Codigo postal:</label>
                                               <input type="text" name="LugarExpedicion" id="LugarExpedicion" class="form-control" value="<?php echo $LugarExpedicion;?>" readonly required>
                                              </div>
                                              <div class="col-md-4">
                                                <label>Moneda:</label>
                                                <select id="Moneda" name="Moneda" class="form-control browser-default">
                                                  <option value="XXX">XXX los códigos asignados para las transacciones en que intervenga ninguna moneda</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                               <label>Folio:</label>
                                               <input type="text" name="Folio" id="Folio" class="form-control" value="<?php echo $Folio;?>"  required>
                                              </div>
                                              <div class="col-md-4">
                                               <label>Serie:</label>
                                               <input type="text" name="Serie" id="Serie" class="form-control" value="<?php echo $serie;?>"  required readonly>
                                              </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="checkbox" class="filled-in" id="facturarelacionada" >
                                                  <label for="facturarelacionada">Factura Relacionada</label>
                                                </div>
                                            </div>
                                            <div class="form-group row divfacturarelacionada" style="display:none;">
                                                <div class="col-md-4">
                                                    <label>Tipo Relacion</label>
                                                    <select class="form-control browser-default" id="TipoRelacion">
                                                        <!--
                                                        <option value="01">01 Nota de crédito de los documentos relacionados</option>
                                                        <option value="02">02 Nota de débito de los documentos relacionados</option>
                                                        <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                                                        -->
                                                        <option value="04">04 Sustitución de los CFDI previos</option>
                                                        <!--
                                                        <option value="05">05 Traslados de mercancias facturados previamente</option>
                                                        <option value="06">06 Factura generada por los traslados previos</option>
                                                        <option value="07">07 CFDI por aplicación de anticipo</option>
                                                        -->
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Folio Fiscal</label>
                                                    <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                                                </div>
                                            </div>
                                        <!------------------------------------>
                                    </div>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header" id="headingcuatro">
                                  <h5 class="mb-0">
                                    <a class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapsecuatro" aria-expanded="true" aria-controls="collapsecuatro">Recepción de pagos</a>
                                  </h5>
                                </div>
                                <div class="collapse show" id="collapsecuatro" aria-labelledby="headingcuatro" data-bs-parent="#accordion">
                                    <div class="card-body">
                                        <!------------------------------------>
                                            <div class="row">
                                              <div class="col-md-4">
                                               <label>Fecha y hora de pago *:</label>
                                               <input type="datetime-local" name="Fechatimbre" id="Fechatimbre" class="form-control" value=""  required>
                                              </div>
                                              <div class="col-md-4">
                                                <label>Forma de pago *:</label>
                                                <select id="FormaDePagoP" name="FormaDePagoP" class="form-control browser-default" required>
                                                  <option></option>
                                                  <?php foreach ($forma as $key) { ?>
                                                    <option value="<?php echo str_pad($key->id, 2, "0", STR_PAD_LEFT); ?>" <?php if($key->id==$formapago){echo 'selected';} ?> ><?php echo $key->formapago_text; ?></option>
                                                  <?php } ?>
                                                </select>
                                              </div>
                                              <div class="col-md-4">
                                               <label>Moneda *:</label>
                                               <select id="ModedaP" name="ModedaP" class="form-control browser-default">
                                                 <option value="MXN">MXN Peso Mexicano</option>
                                               </select>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                               <label>Monto *:</label>
                                               <input type="number" name="Monto" id="Monto" class="form-control" value=""  required>
                                              </div>
                                              <div class="col-md-4">
                                               <label>Total de Importes *:</label>
                                               <div class="totalimport"></div>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                               <label>Número de operación:</label>
                                               <input type="text" name="NumOperacion" id="NumOperacion" class="form-control" onpaste="return false;"  >
                                              </div>
                                              <div class="col-md-4">
                                               <label>Cuenta Beneficiario:</label>
                                               <input type="text" name="CtaBeneficiario" id="CtaBeneficiario" class="form-control" value=""  >
                                              </div>
                                            </div>
                                        <!------------------------------------>
                                    </div>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header" id="headingcinco">
                                  <h5 class="mb-0">
                                    
                                    <a class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapsecinco" aria-expanded="true" aria-controls="collapsecinco">Documentos relacionados</a>
                                  </h5>
                                </div>
                                <div class="collapse show" id="collapsecinco" aria-labelledby="headingcinco" data-bs-parent="#accordion">
                                    <div class="card-body">
                                        <!------------------------------------>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <a class="btn btn-primary" onclick="adddocumento(<?php echo $cliente.','.$Folio ?>)">Agregar nuevo</a>
                                              </div>
                                              <div class="col-md-12">
                                                <table id="tabledocumentosrelacionados" class="table">
                                                  <tbody class="tabletbodydr">
                                                    <tr class="documento_add_<?php echo $FacturasId;?>">
                                                      <td>
                                                        <div class="col-md-3">
                                                          <input type="hidden" id="idfactura" class="form-control" value="<?php echo $FacturasId;?>"  readonly required>
                                                          <input type="hidden" id="fserie" class="form-control" value="<?php echo $fac->serie;?>"  readonly required>
                                                          <input type="hidden" id="ffolio" class="form-control" value="<?php echo $fac->Folio;?>"  readonly required>
                                                          <input type="hidden" id="MetodoDePagoDR" class="form-control" value="<?php echo $MetodoDePagoDR;?>"  readonly required>
                                                         <label>Id del documento:</label>
                                                         <input type="text" id="IdDocumento" class="form-control IdDocumentos" value="<?php echo $uuid;?>" readonly required>
                                                        </div>
                                                        <div class="col-md-2">
                                                         <label>Número de parcialidad:</label>
                                                         <input type="text" id="NumParcialidad" class="form-control" value="<?php echo $copnum;?>"  required>
                                                        </div>
                                                        <div class="col-md-2">
                                                         <label>Importe de saldo anterior:</label>
                                                         <input type="text" id="ImpSaldoAnt" class="form-control ImpSaldoAnt_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly required>
                                                        </div>
                                                        <div class="col-md-2">
                                                         <label>Importe de Pagado:</label>
                                                         <input type="text" id="ImpPagado" class="form-control ImpPagado ImpPagado_<?php echo $Folio;?>" value="0" oninput="calcularsinsoluto(<?php echo $Folio;?>)">
                                                        </div>
                                                        <div class="col-md-2">
                                                         <label>Importe de Pagado:</label>
                                                         <input type="text" id="ImpSaldoInsoluto" class="form-control ImpSaldoInsoluto_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly>
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                        <!------------------------------------>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </form>
                        <input type="hidden" name="totalimportes" id="totalimportes" value="">
                        <div class="row center fixed-action-btn" style="bottom: 100px; right: 19px;">
                          <a href="<?php echo base_url();?>Facturas?clienteid=<?php echo $cliente;?>&cliente=<?php echo $razonsocialreceptor;?>" class="btn btn-secondary" style="background-color: grey !important;">Regresar</a>
                        </div>
                        <div class="row center fixed-action-btn" style="bottom: 50px; right: 19px;">
                          <a class="btn btn-success" id="btn_savecomplemento_previe">Agregar complemento</a>
                        </div>
                    <!--------------------------------------->
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<div class="modal fade" id="modaldocumentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Documentos del cliente</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            <div class="row">
              <div class="col s12">
                <h3>Documentos del cliente</h3>
              </div>
            </div>
            <div class="row">
              <div class="col s12 listadodocumentos">
              </div>
            </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_previefactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Preview Complemento</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body preview_iframe">
            <h4>Preview Complemento</h4>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button href="#!" class="modal-close btn btn-primary registrofac" id="btn_savecomplemento">Aceptar</button>
      </div>
    </div>
  </div>
</div>