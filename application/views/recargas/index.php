<style type="text/css">
  .dataTables_filter label, .dataTables_paginate .pagination{
    float: right;
  }
  #table_rechange_filter{
    display: none;
  }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
      <div class="col-lg-3">
          <div class="form-group row">
              <div class="col-12">
                  <input class="form-control" type="search" placeholder="Buscar recargas" id="searchtext" oninput="search()">
              </div>
          </div>
      </div> 
      <div class="col-lg-7"></div>
      <div class="col-lg-2" align="right"> 
        <a href="<?php echo base_url() ?>Recargas/alta" class="btn btn-primary">Nuevo registro</a>
      </div>    
    </div>
    <div class="row">
      <div class="col-lg-12"><br></div>   
    </div>
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12">
            <table class="table table-sm" id="table_rechange" style="width:100%">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Código</th>
                  <th scope="col">Capacidad</th>
                  <th scope="col">Precio venta</th>
                  <th scope="col">Costo compra (por litro)</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>
