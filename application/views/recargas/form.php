<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
      <div class="col-lg-10" align="right"> 
      </div>   
      <div class="col-lg-2" align="right"> 
        <a href="<?php echo base_url() ?>Recargas" class="btn btn-primary">Regresar</a>
      </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <input type="hidden" id="band_tp" value="<?php echo $band_tp;?>">
        <input type="hidden" id="tipo_rec" value="<?php echo $tipo;?>">
        <form id="form_recar" method="post">
          <br>
          <?php if(isset($rec) || $tipo==""){ ?>
            <input type="hidden" id="id" name="id" value="<?php if(isset($rec)) echo $rec->id; else echo "0"; ?>">
            <input type="hidden" id="sucursal" name="sucursal" value="<?php if(isset($rec)) echo $rec->sucursal; ?>">
            <div class="row">
              <div class="col-md-2">
                <label class="form-label">Código<span style="color: red">*</span></label>
                <input class="form-control" <?php if(isset($rec)) echo "readonly"; ?> type="text" name="codigo" id="codigo" value="<?php if(isset($rec)) echo $rec->codigo; ?>">
              </div>
              <div class="col-md-2">
                <label class="form-label">Capacidad (L)<span style="color: red">*</span></label>
                <input class="form-control" <?php if(isset($rec)) echo "readonly"; ?> type="number" id="capacidad" name="capacidad" value="<?php if(isset($rec)) echo $rec->capacidad; ?>">
              </div>
              <div class="col-md-2" <?php if(isset($rec) && $rec->tipo==0) echo 'style="display:none"'; ?>>
                <label class="form-label">Precio de venta<span style="color: red">*</span></label>
                <input class="form-control" <?php if(isset($rec) && $rec->tipo==0) echo "readonly"; ?> type="number" id="preciov" name="preciov" value="<?php if(isset($rec)) echo $rec->preciov; ?>">
              </div>
              <div class="col-md-2">
                <label class="form-label">Costo de compra (por litro)<span style="color: red">*</span></label>
                <input class="form-control" type="number" id="precioc" name="precioc" value="<?php if(isset($rec)) echo $rec->precioc; ?>">
              </div>
              <div class="col-md-2" <?php if(isset($rec) && $rec->tipo==0) echo 'style="display:none"'; ?>>
                <label class="form-label">Utilidad ($)</label>
                <input class="form-control" type="number" id="utilidad" readonly value="">
              </div>
              <div class="col-md-2">
                <label class="form-label">Tipo</label>
                <select class="form-control tipo" name="tipo" id="tipo" <?php if(isset($rec->tipo)) echo 'disabled' ?>>
                  <option value="1" <?php if(isset($rec->tipo) && $rec->tipo==1) echo 'selected' ?>>Recarga</option>  
                  <option value="0" <?php if(isset($rec->tipo) && $rec->tipo==0) echo 'selected' ?>>Tanque</option>
                </select>
              </div>
            </div> 
            <div class="row g-3">
              <br>
            </div>   
            <div class="row g-3" id="cont_det" style="display: none;">
              <div class="col-md-2">
                <label class="form-label">Stock (L)</label>
                <input class="form-control" type="number" id="stock" readonly value="<?php if(isset($rec)) echo ($rec->stock-$rec->traslado_stock_cant); ?>">
              </div>
              <div class="col-md-2">
                <label class="form-label">En tanques</label>
                <input class="form-control" type="number" id="stock_tanques" readonly value="">
              </div>
            </div>        
            <div class="row g-3">
              <br>
            </div>
          <?php } ?>
        </form>
        <?php if(isset($recargas)){ ?>
          <table id="table_sucs" width="100%">
            <tbody id="body_det">
            <?php foreach ($recargas as $rec) {
            /*if($rec->tipo==0){
              echo "<h2>Sucursal: ".$rec->name_suc."</h2><hr>";
            } */
            if(isset($rec) && $rec->stock>0){
              $entan=round(($rec->stock-$rec->traslado_stock_cant)/$rec->capacidad);
            }else{
              $entan=0;
            } ?>
            <tr>
              <td colspan="7"><hr><h2>Sucursal: <?php echo $rec->name_suc; ?></h2><hr></td>
            </tr>
            <tr id="tr_det">
              <td width="1%">
                <div class="row">
                  <input type="hidden" id="id" name="id" value="<?php if(isset($rec)) echo $rec->id; else echo "0"; ?>">
                  <input type="hidden" id="sucursal" name="sucursal" value="<?php if(isset($rec)) echo $rec->sucursal; ?>">
                </div>
              </td>
              <td width="19%">
                <div class="col-md-12">
                  <label class="form-label">Código<span style="color: red">*</span></label>
                  <input class="form-control" <?php if(isset($rec)) echo "readonly"; ?> type="text" name="codigo" id="codigo" value="<?php if(isset($rec)) echo $rec->codigo; ?>">
                </div>
              </td>
              <td width="16%">
                <div class="col-md-12">
                  <label class="form-label">Capacidad (L)<span style="color: red">*</span></label>
                  <input class="form-control" <?php if(isset($rec)) echo "readonly"; ?> type="number" id="capacidad" name="capacidad" value="<?php if(isset($rec)) echo $rec->capacidad; ?>">
                </div>
              </td>
              <td width="16%">
                <div class="col-md-12">
                  <label class="form-label">Costo de compra (por litro)<span style="color: red">*</span></label>
                  <input class="form-control" type="number" id="precioc" name="precioc" value="<?php if(isset($rec)) echo $rec->precioc; ?>">
                </div>
              </td>
              <td width="16%">
                <div class="col-md-12">
                  <label class="form-label">Tipo</label>
                  <select class="form-control tipo" name="tipo" id="tipo" <?php if(isset($rec->tipo)) echo 'disabled' ?>>
                    <option value="1" <?php if(isset($rec->tipo) && $rec->tipo==1) echo 'selected' ?>>Recarga</option>  
                    <option value="0" <?php if(isset($rec->tipo) && $rec->tipo==0) echo 'selected' ?>>Tanque</option>
                  </select>
                </div>
              </td>
              <td width="16%">
                <div class="col-md-12">
                  <label class="form-label">Stock (L)</label>
                  <input class="form-control" type="number" id="stock" readonly value="<?php if(isset($rec)) echo ($rec->stock-$rec->traslado_stock_cant); ?>">
                </div>
              </td>
              <td width="16%">
                <div class="col-md-12">
                  <label class="form-label">En tanques</label>
                  <input class="form-control" type="number" id="stock_tanques" readonly value="<?php echo $entan; ?>">
                </div>
              </td>
            </tr>     
        <?php } ?>
            </tbody>
          </table>
        <?php } ?>        
        <br>
        <div align="center">
          <button class="btn btn-primary btn_save" style="width: 30%" type="button">Guardar</button>
        </div>  
      </div>
    </div>  
  </div>
</div>    
