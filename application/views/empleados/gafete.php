<style type="text/css">
	.margen_todo {
	    border-style: solid;
	    border-width: 13px;
	    border-radius: 12px;
	    border-color: <?php echo $get_empleado->color ?>;
	}
</style>
<table class="margen_todo" width="90%">
	<tbody>
		<tr>
			<td width="50%" class="margen_todo" align="center">
				<table >
					<tbody>
						<tr align="center">
							<td>
								<br>
								<img style="width: 400px" src="<?php echo base_url() ?>images/novela.png">
								<br>
								<br>
							</td>
						</tr>
						<tr align="center">
							<td>
								<br>
								<input id="text" type="hidden" value="<?php echo $get_empleado->personalId ?>">
								<div id="qrcode"></div>
								<br>
								<h3>NO. DE EMPLEADO: <span class="numero_emp"></span> </h3>
								<br>
							</td>
						</tr>
						<tr align="center">
							<td>
								<br>
								<img style="width: 400px" src="<?php echo base_url() ?>images/gc.png">
								<br>
								<br>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="50%" class="margen_todo">	
				<table>
					<tbody>
						<tr align="center">
							<td>
								<br>
								<h3> Para cualquier información referente a esta producción, comunicarte a:<br> (55-68-92-45) </h3>
								<br>
								<br>
							</td>
						</tr>
						<tr align="center">
							<td>
								<?php 
							    $html='';
				                if($get_empleado->foto){ ?>
				                    <img id="img_avatar" style="width: 300px; height: 300px; border-radius: 150px;" src="<?php echo base_url() ?>uploads/personal/<?php echo $get_empleado->foto ?>">
				                <?php }else{ ?> 
				                    <img src="<?php echo base_url() ?>images/avatarh.PNG">     	
				                <?php  } ?> 
							</td>
						</tr>
						<tr align="center">
							<td>
								<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:12px solid <?php echo $get_empleado->color ?>;"></div>
								<h1><?php echo $get_empleado->nombre ?> </h1>
								<h1><?php echo $get_empleado->puesto ?> </h1>
								<div style="width: 9cm; border-bottom: 12px solid <?php echo $get_empleado->color ?>;"></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>				
<?php /*
<table class="margen_todo">

ALTER TABLE `personal` ADD `foto` VARCHAR(255) NOT NULL AFTER `usuario`;
ALTER TABLE `personal` ADD `color` VARCHAR(20) NOT NULL AFTER `foto`;
	<tbody>
		<tr align="center">
			<td><img style="width: 400px" src="<?php echo base_url() ?>images/gc.png"></td>
		</tr>
		<tr style="font-size:10%;">
            <td style="border-bottom: 3px solid #026934;">
            </td>
        </tr>
		<tr align="center">
			<td>
			    <br> 
				    <img src="<?php echo base_url() ?>images/avatarh.PNG">
			    </td>
		</tr>
		<tr align="center">
			<td><h1><?php echo $get_empleado->nombre ?> </h1></td>
		</tr>
		<tr align="center">
			<td><h1><?php echo $get_empleado->puesto ?> </h1></td>
		</tr>
		<tr align="center">
			<td>
				<input id="text" type="hidden" value="<?php echo $get_empleado->personalId ?>">
				<div id="qrcode"></div>
				<br>
				<br>
			</td>
		</tr>
		<tr align="center">
			<td><h3>NO. DE EMPLEADO: <span class="numero_emp"></span> </h3></td>
		</tr>
	</tbody>
</table>
<br>
<table class="margen_todo">
	<tbody>
		<tr align="center">
			<td><img style="width: 400px" src="<?php echo base_url() ?>images/gc.png"></td>
		</tr>
		<tr style="font-size:10%;">
            <td style="border-bottom: 3px solid #026934;">
            </td>
        </tr>
		<tr align="center">
			<td>
				<br>
				<br>
				<br>
				<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
				<h2>Firma del empleado</h2>
			</td>
		</tr>
		<tr align="center">
			<td>
				<br>
				<br>
				<br>
				<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
				<h2>Firma del jefe inmediato</h2>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
			</td>
		</tr>
	</tbody>
</table>
*/
?>


<script src="<?php echo base_url();?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url();?>public/core/js/qrcode.js"></script>
<script src="<?php echo base_url();?>public/js/qr.js"></script>
<script type="text/javascript">
	function pad (str, max) { str = str.toString(); return str.length < max ? pad("0" + str, max) : str; }

</script>
<script type="text/javascript">
	$(document).ready(function() {
		    var num_emp=$('#text').val();
		    $('.numero_emp').html(pad(num_emp,4)); 

	        window.print();
    }); 

</script>