<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10"> 
              <h3 style="color: #1770aa;">Nuevo Perfil</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Perfiles" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <form id="form_datos" method="post">
          <input type="hidden" name="perfilId" id="id" value="<?php if(isset($perf)) echo $perf->perfilId; else echo "0";?>">
          <!--<div class="row form-group"> 
            <div class="col-md-4">
              <label>Nombre del perfil<span style="color: red">*</span></label>
              <input type="text" name="nombre" id="nombre" class="form-control" value="<?php if(isset($perf)) echo $perf->nombre;?>">
            </div>
            <div class="col-lg-2">
              <br>
              <label class="form-label" style="font-size: 16px; color:#009bdb; text-align: right;" >Tipo Venta</label>
            </div> 
            <div class="col-lg-1">
              <br>
              <span class="switch switch-icon">
                <label>
                  <input <?php if(isset($perf) && $perf->tipo_venta) echo "checked";?> type="checkbox" name="tipo_venta" id="tipo_venta" value="0">
                  <span style="border: 2px solid #009bdb;"></span>
                </label>
              </span>
            </div>
          </div>  -->
        
          <div class="row"> 
            <div class="col-md-4">
              <label>Nombre del perfil<span style="color: red">*</span></label>
              <input type="text" name="nombre" id="nombre" class="form-control" value="<?php if(isset($perf)) echo $perf->nombre;?>">
            </div>
            <div class="col-lg-1" style="margin-top:30px; text-align: right;">
              <label class="form-label" style="font-size: 16px; color:#009bdb;" >Tipo Venta</label>
            </div>    
            <div class="col-lg-1" style="margin-top:30px;">
              <span class="switch switch-icon">
                <label>
                  <input <?php if(isset($perf) && $perf->tipo_venta==1) echo "checked";?> type="checkbox" id="tipo_venta" class="check" value="0">
                  <span style="border: 2px solid #009bdb;"></span>
                </label>
              </span>
            </div>
            <div class="col-lg-1" style="margin-top:30px; text-align: right;">
              <label class="form-label" style="font-size: 16px; color:#009bdb;" >Tipo Técnico</label>
            </div>    
            <div class="col-lg-1" style="margin-top:30px;">
              <span class="switch switch-icon">
                <label>
                  <input <?php if(isset($perf) && $perf->tipo_tecnico==1) echo "checked";?> type="checkbox" id="tipo_tecnico" class="check" value="0">
                  <span style="border: 2px solid #009bdb;"></span>
                </label>
              </span>
            </div>
          </div>
        </form> 
        <div class="row">
          <div class="col-md-12">
            <br><br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php if(isset($perf->perfilId))
              $perfilId=$perf->perfilId;
              else
                $perfilId=0;
            foreach ($menus as $m){
              $subm=$this->General_model->get_submenus($m->MenuId,$perfilId);
              echo '<div class="col-md-12 mx-auto">
                      <h2 class="title">'.$m->Nombre.'</h2><br>';
              foreach ($subm as $s){ 
                $Perfil_detalleId=$s->Perfil_detalleId;
                if($s->MenusubIdpd==$s->MenusubId){
                  $check="checked";
                }else{
                  $check="";
                }
                if($s->MenusubId==4 || $s->MenusubId==18 || $s->MenusubId==19 || $s->MenusubId==20 || $s->MenusubId==21 || $s->MenusubId==23 || $s->MenusubId==27 || $s->MenusubId==38 || $s->MenusubId==41){
                  $class_dis="classpv";
                }else{
                  $class_dis="classnopv";
                }
                echo '
                      <table class="table" id="tabla_perms" width="100%">
                        <tr>
                          <td width="15%">
                            <div class="row">
                              <div class="col-lg-6" style="text-align: right;">
                                <label class="form-label" style="font-size: 16px; color:#009bdb;">'.$s->Nombre.'</label>
                              </div> 
                            </div>
                          </td>
                          <td width="20%">
                            <input type="hidden" id="id" value="'.$Perfil_detalleId.'">
                            <div class="row">
                              <div class="col-lg-2">
                                <span class="switch switch-icon">
                                  <label>
                                    <input class="check_ms '.$class_dis.'" '.$check.' type="checkbox" id="menu_sub" data-menuid="'.$m->MenuId.'" value="'.$s->MenusubId.'">
                                    <span style="border: 2px solid #009bdb;"></span>
                                  </label>
                                </span>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                  <!--</div>-->';
              }
              echo '<hr></div>';
            } ?>
          </div>
        </div> 
        <div class="row"> 
          <div class="col-md-12" align="center">
            <button class="btn btn-primary btn_registro" type="button" style="width: 30%;" onclick="guardar_registro_datos()">Guardar</button>                 
          </div>
        </div>
      </div>
    </div>
</div>    
