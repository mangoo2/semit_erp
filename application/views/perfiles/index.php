<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar perfil" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div>   
                <!--<div class="col-lg-8" align="right"> 
                    <a href="<?php echo base_url() ?>Perfiles/registro" class="btn btn-primary">Nuevo registro</a>
                </div> -->  
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
