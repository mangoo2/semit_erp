<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=saldo_provee_".date('YmdGis').".xls"); 
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellpadding="5"  cellspacing="0" width="100%" style="color: #012d6a;">
  <thead>
    <tr style="background: #F2F2F2"><th colspan="5"><b>De <?php echo ucwords($getv[0]->mes_anio)." a ".ucwords($getv[count($getv)-1]->mes_anio); ?></b></th></tr>
    <tr style="background: #F2F2F2"><th colspan="5"><b><?php echo $getv[0]->provee; ?></b></th></tr>
    <tr>
      <!--<th>Proveedor</th>-->
      <th>Mes</th>
      <th>Subtotal</th>
      <th>IVA</th>
      <th>ISR</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    <?php $supertot=0; $html="";
    foreach ($getv as $c) {
        $html .= '<tr>';
        //$html .= '<td>'.$c->provee.'</td>';
        $html .= '<td> <input type="hidden" id="name_mes" value="'.$c->mes_anio.'">'.ucwords($c->mes_anio).'</td>';
        $html .= '<td>$'.number_format($c->subtotal,2,".",",").'</td>';
        $html .= '<td>$'.number_format($c->iva,2,".",",").'</td>';
        $html .= '<td>$'.number_format($c->isr,2,".",",").'</td>';
        $html .= '<td><input type="hidden" id="total" value="'.number_format($c->total,2).'">$'.number_format($c->total,2,".",",").'</td>';
        $html .= '</tr>';
        $supertot=$supertot+$c->total;
    } echo $html; ?>
    <tr style="background: #F2F2F2">
      <td colspan="3"></td>
      <td><b>Total compras: </b></td>
      <td><input type="hidden" id="supertot" value="<?php echo $supertot; ?>"><?php echo "$".number_format($supertot,2,".",",");?></td>
    </tr>
  </tbody>
</table>
