<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=resultados".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellpadding="5"  cellspacing="0" width="100%">
  <thead>
      <tr>
      	<th>Sucursal</th>
        <th>Subtotal</th>
        <th>IVA</th>
        <th>Total vendido</th>
      </tr>
  </thead>
  <tbody>
    <?php $supertot=0;
    foreach ($getv as $c) {
      echo '<tr style="color: #012d6a;">';
      echo '<td> <input type="hidden" id="name_suc" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
      echo '<td>$'.number_format($c->subtotal,2,".",",").'</td>';
      echo '<td>$'.number_format($c->iva,2,".",",").'</td>';
      echo '<td><input type="hidden" id="total" value="'.$c->total.'">$'.number_format($c->total,2,".",",").'</td>';
      echo '</tr>';
      $supertot=$supertot+$c->total;
    } ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="2"></td>
      <td class="httabled"><b>Total vendido: </b></td>
      <td class="httabled"><input type="hidden" id="supertot" value="<?php echo $supertot; ?>">$ <?php echo number_format($supertot,2,".",",");?> </td>
    </tr>
  </tfoot>
</table>
<table border="0">
  <tr><td><br><br></td></tr>
</table>
<table border="1" cellpadding="5" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="httabled">Sucursal</th>
      <th class="httabled">Total Ventas</th>
      <th class="httabled">Utilidad</th>
    </tr>
  </thead>
  <tbody class="body_utilidad">
    <?php $supertot=0; $sub_ganancia = 0;
    foreach ($get_uti as $c) {
      $sub_ganancia = $c->total - $c->sub_compra_iva;
      echo '<tr style="color: #012d6a;">';
      echo '<td> <input type="hidden" id="name_suc_uti" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
      echo '<td>$'.number_format($c->total,2,".",",").'</td>';
      echo '<td><input type="hidden" id="total_utilidad" value="'.$sub_ganancia.'">$'.number_format($sub_ganancia,2,".",",").'</td>';
      echo '</tr>';
      $supertot=$supertot+$sub_ganancia;
    } ?>          
    <tr>
      <td></td>
      <td class="httabled"><b>Total utilidad: </b></td>
      <td class="httabled"><input type="hidden" id="suptot_uti" value="<?php echo $supertot;?>">$ <?php echo number_format($supertot,2,".",","); ?> </td>
    </tr>
  </tbody>
</table>