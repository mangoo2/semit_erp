<?php
require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['idcotizacion']=$idcotizacion;
$GLOBALS['folio']=$folio;
$GLOBALS['reg']=$reg;
$GLOBALS['razon_social']=$razon_social;
$GLOBALS['celular']=$celular; //datos del vendedor, verificar sea el dato
$GLOBALS['correo']=$correo; //datos del vendedor, verificar sea el dato


//=======================================================================================
class MYPDF extends TCPDF {
    //Page header Cotización: '.$GLOBALS['idcotizacion'].'
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0); 
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="38%">
                    </td>
                    <td width="20%" style="font-size: 12px;color: #012d6a;"><br><br>
                       <img style="width:80%" src="'.base_url().'public/img/reportes/mundo.png"> semit.mx
                    </td>
                    <td width="25%" style="font-size: 12px;color: #012d6a;"><br><br>
                       <img style="width:80%" src="'.base_url().'public/img/reportes/correo.png"> '.$GLOBALS['correo'].'
                    </td>
                    <td width="25%" style="font-size: 12px;color: #012d6a;"><br><br>
                       <img style="width:80%" src="'.base_url().'public/img/reportes/tel.png"> '.$GLOBALS['celular'].'
                    </td>
                </tr><tr>
                    <td width="17%">
                    </td>
                    <td width="63%" style="font-size:15px;color: #012d6a;"><br><br>Cliente:'.$GLOBALS['razon_social'].'
                    </td>
                    <td width="20%" style="font-size:15px;color: #012d6a;"><br><br>Folio:'.$GLOBALS['folio'].'
                    </td>
                </tr><tr>
                    <td width="17%">
                    </td>
                    <td width="63%" style="font-size:15px;color: #012d6a;">
                    </td>
                    <td width="20%" style="font-size:15px;color: #012d6a;">Fecha:'.$GLOBALS['reg'].'
                    </td>
                </tr>
            </table><br><br><br><br>';
            $html.='<table border="0"><tr>
                    <td width="95%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/reportes/barra.png">
                    </td>
                </tr></table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 
    }
} 


$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cotización');
$pdf->SetTitle('Cotización');
$pdf->SetSubject('Cotización');
$pdf->SetKeywords('Cotización');

// set default header data
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(true);
$pdf->SetFooterMargin(true);
$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('helvetica', '',10);
// add a page
$pdf->SetMargins(0, 65, 8, true);
$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
    .httabled{
        font-size:12px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }
    
    .td_l{
        font-size:11px;
    }

  </style>';

$html.='<table border="0" cellpadding="2"><tr>
      <th width="5%" class="httabled"><b></b></th>
      <th width="10%" class="httabled"><b>CANT.</b></th>
      <th width="44%" class="httabled"><b>PRODUCTO</b></th>
      <th width="14%" class="httabled"><b>PRECIO U</b></th>
      <th width="12%" class="httabled"><b>IVA</b></th>
      <th width="14%" class="httabled"><b>SUBTOTAL</b></th>
    </tr>';
    foreach ($result_productos->result() as $item){
        if($item->tipo_prodcot==0){
            $nombre=$item->nombre;  
            $precio_con_iva = $item->precio_unitario;
            $incluye_iva = $item->incluye_iva;
            $iva = $item->iva;
        }if($item->tipo_prodcot==1){
            $nombre=$item->recarga." Recarga de oxígeno de ".$item->capacidad. "L";
            $precio_con_iva = number_format($item->precio_unitario,2);
            $incluye_iva = 0; //0=no, 1=si
            $iva=0;
        }if($item->tipo_prodcot==3){
            $nombre=$item->servicio;
            $precio_con_iva = number_format($item->precio_unitario,2);
            $incluye_iva = $item->iva_serv; //0=no, 1=si
            $iva=16;
        }
        
        $sub_iva=0;
        /*log_message('error','nombre: '.$nombre);
        log_message('error','incluye_iva: '.$incluye_iva);
        log_message('error','iva: '.$iva);
        log_message('error','tipo_prodcot: '.$item->tipo_prodcot);*/
        if($item->tipo_prodcot==0){
            $precio_con_ivax=0;
            if($incluye_iva==1 || $iva>0){
                $precio_con_ivax=round($item->precio_unitario,2);
                if($iva==0){
                    $incluye_iva=0;
                    $precio_con_ivax=round($item->precio_unitario,2);
                }else{
                    $precio_con_ivax=round($item->precio_unitario,2);
                    $sub_iva=round($item->precio_unitario*.16,2);
                    //log_message('error','sub_iva: '.$sub_iva);
                }
            }else{
                if($iva>0){
                    $siva=1.16;
                    $precio_con_ivax=round($item->precio_unitario,2);
                }else{
                    $precio_con_ivax=round($item->precio_unitario,2);  
                }
            }
            $sub_iva=$sub_iva*$item->cantidad;
            $sub_importe=($item->cantidad*$precio_con_ivax);
            $importe=($item->cantidad*$precio_con_ivax)+$sub_iva;
        }else{ //para servs
            $sub_iva=0;
            if($iva>0){
                $precio_con_ivax=round($item->precio_unitario,2);
                $sub_iva=round($item->precio_unitario*.16,2);
            }else{
                $precio_con_ivax=round($item->precio_unitario,2);  
            }
            
            $sub_iva=$sub_iva*$item->cantidad;
            $sub_importe=($item->cantidad*$precio_con_ivax);
            $importe=($item->cantidad*$precio_con_ivax)+$sub_iva;
        }
        $html.='<tr>
            <td class="td_l"></td>
            <td class="td_l">'.$item->cantidad.'</td>
            <td class="td_l">'.$nombre.'</td>
            <td class="td_l">$'.number_format($precio_con_ivax,2,'.',',').'</td>
            <td class="td_l">$'.number_format($sub_iva,2,'.',',').'</td>
            <td class="td_l">$'.number_format($importe,2,'.',',').'</td>
        </tr>';
    }
$html.='</table>';

$subtotal=0;
$iva=0;
$total=0;
foreach ($result_cotizacion->result() as $y) {
    $subtotal=$y->subtotal;
    $iva=$y->iva;
    $total=$y->total;
}

$html.='<br><br><table border="0" cellpadding="2">
            <tr>
              <td width="61%"></td>
              <td width="20%" style="font-size:15px; color:#012d6a; text-align: right;">Subtotal:'.'</td>
              <td style="font-size:15px; color:#012d6a;">$'.number_format($subtotal,2,'.',',').'</td>
            </tr>
            <tr>
              <td width="61%"></td>
              <td width="20%" style="font-size:15px; color:#012d6a; text-align: right;">IVA:'.'</td>
              <td style="font-size:15px; color:#012d6a;">$'.number_format($iva,2,'.',',').'</td>
            </tr>
            <tr>
              <td width="61%"></td>
              <td width="20%" style="font-size:15px; color:#012d6a; text-align: right;"><b>Total:'.'</b></td>
              <td style="font-size:15px; color:#012d6a;"><b>$'.number_format($total,2,'.',',').'</b></td>
            </tr>
        </table>';

$html.='<br><br><table border="0" cellpadding="2">
            <tr>
              <td width="3%"></td>
              <td width="80%" style="font-size:14px; color:#012d6a;"><b>Clausulas:</b></td>
            </tr>';
            foreach ($result_clausulas as $x){
                $html.='<tr>
                  <td width="3%"></td>
                  <td width="80%" style="font-size:12px; color:#012d6a;">'.$x->clausula.'</td>
                </tr>';
            }
        $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');

$url=FCPATH.'doc_cotizaciones/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'Cotizacion_'.$idcotizacion.'.pdf', 'FI');
//$pdf->Output('Cotizacion.pdf', 'I');
?>