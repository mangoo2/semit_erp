<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');  
    $GLOBALS['folioarqueo']=10; 
    $GLOBALS['personal']='Administrador';  
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $img_header = FCPATH.'public/img/header.JPG';
      $this->Image($img_header, 0, 0, 210, 63, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $html='<style type="text/css">
                .title1{
                    color:#035397;
                    font-size:33px;
                }
                .titletd{
                    font-size:21px;
                }
                .title2{
                    color:#152342;
                }
                .title3{
                    color:#b9c61e;
                }
            </style>
            <table border="1">
                <tr>
                    <td style="width:64%;height:80px"></td>
                    <td style="width:36%"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="title1">Arqueo de caja</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="titletd"><span class="title2">No. Arqueo: </span><span class="title3">'.$GLOBALS['folioarqueo'].'</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="titletd"><span class="title2">Empleado:</span> <span class="title3">'.$GLOBALS['personal'].'</span></td>
                </tr>
            </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
        $img_header = FCPATH.'public/img/footer.JPG';
      $this->Image($img_header, 125, 252, 63, 45, '', '', '', false, 100, '', false, false, 0);
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '65', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();
$html='<style type="text/css">
        .c_blue{
            color:#152342;
        }
        .t_c{
            text-align:center;
        }
        .t_bold{
            font-weight:bold;
        }
        .t_size_12{
            font-size:12px;
        }
       </style>';
$html.='<table border="0">
            <tr>
                <td class="c_blue t_c t_bold t_size_12">SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</td>
            </tr>
            <tr>
                <td class="c_blue t_c t_size_12">Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</td>
            </tr>
        </table>';
$html.='<p></p><p></p>';
$html.='<table border="1">';
$html.='            <tr>
            <td></td>
            <td></td>
            <td></td>
            </tr>';
$html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');

//$pdf->IncludeJS('print();');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>