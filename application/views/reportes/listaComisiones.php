<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=comisiones_".$mes.".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
  <thead>
    <tr><th colspan="8">COMISIONES POR TIPO - PRODUCTOS Y RECARGAS</th></tr>
    <tr>
    	<th>Vendedor</th>
      <th>Subtotal</th>
      <th>IVA</th>
      <th>Total</th>
      <th>Sucursal</th>
      <th>Comisión</th>
      <th>Total comisión</th>
      <th>Tipo</th>
    </tr>
  </thead>
  <tbody>
  	<?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
      $html="";
      foreach ($detsp as $s) {
        $subtotal_general = $s->subtotal_prods+$s->subtotal_recarga; 
        if($subtotal_general>0 && $s->comision>0){
          $montocomision=($subtotal_general*$s->comision)/100;
        }else{
          $montocomision=0;
        }
        $html .= '<tr>';
          $html .= '<td>'.$s->nombre.'</td>';
          $html .= '<td>$'.number_format(($subtotal_general), 2, '.', ',').'</td>';   
          $html .='<td>$'.number_format(($s->iva_prods+$s->iva_recarga),2,'.',',').'</td>';
          $html .='<td>$'.number_format(($s->total_prods+$s->total_recarga),2,'.',',').'</td>';
          $html .= '<td>'.$s->name_suc.'</td>';
          $html .= '<td>'.$s->comision.' %</td>';
          $html .= '<td>$'.number_format(($montocomision), 2, '.', ',').'</td>';
          $html .= '<td>Productos - Recargas</td>';
        $html .= '</tr>';
        ${'comision_vendedor_'.$s->personalId}=$montocomision;
        ${'comision_vendedor_serv_'.$s->personalId}=0;
        ${'id_personal_'.$s->personalId}=$s->personalId;
      }
      echo $html;
      ?>
  </tbody>
</table>

<table border="0"><tr><td><br></td></tr></table>  

<table border="1" id="tabla" cellspacing="0" width="100%">
  <thead>
    <tr><th colspan="9">COMISIONES POR TIPO - MANTENIMIENTOS</th></tr>
    <tr>
      <th>Vendedor</th>
      <th>Subtotal</th>
      <th>IVA</th>
      <th>Total</th>
      <th>Sucursal</th>
      <th>Comisión</th>
      <th># Servicios</th>
      <th>Total comisión</th>
      <th>Tipo</th>
    </tr>
  </thead>
  <tbody>
    <?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
      $html="";
      foreach ($dets as $s) {
        if($s->subtotal_servs>0 && $s->comision_tecnico>0){
          //$montocomision=($s->subtotal_servs*$s->comision_tecnico)/100;
          $montocomision = $s->sub_num_servs * $s->comision_tecnico;
        }else{
          $montocomision=0;
        }
        $html .= '<tr>';
          $html .= '<td>'.$s->nombre.'</td>';
          $html .= '<td>$'.number_format(($s->subtotal_servs), 2, '.', ',').'</td>';   
          $html .= '<td>$'.number_format(($s->iva_servs), 2, '.', ',').'</td>';
          $html .= '<td>$'.number_format(($s->total_servs), 2, '.', ',').'</td>';
          $html .= '<td>'.$s->name_suc.'</td>';
          $html .= '<td>'.$s->comision_tecnico.'</td>';
          $html .= '<td>'.$s->sub_num_servs.'</td>';
          $html .= '<td>$'.number_format(($montocomision), 2, '.', ',').'</td>';
          $html .= '<td>Servicios</td>';
        $html .= '</tr>';
        ${'comision_vendedor_serv_'.$s->personalId}=$montocomision;
        ${'id_personal_'.$s->personalId}=$s->personalId;
      }
      echo $html;
      ?>
  </tbody>
</table>

<table border="0"><tr><td><br></td></tr></table>  

<table border="1" id="tabla_all" cellspacing="0" width="100%">
  <thead>
    <tr><th colspan="6">COMISIONES POR VENDEDOR</th></tr>
    <tr>
      <th>Vendedor</th>
      <th>Subtotal</th>
      <th>IVA</th>
      <th>Total</th>
      <th>Sucursal</th>
      <th>Total comisión</th>
    </tr>
  </thead>
  <tbody class="table_datos_tb">
    <?php $tot_com=0; foreach ($all as $s) {
      $subtotal_general = $s->subtotal; 
      $montocomision=0;
      if(isset(${'id_personal_'.$s->personalId}) && $s->personalId==${'id_personal_'.$s->personalId}){
        $montocomision=${'comision_vendedor_'.$s->personalId}+${'comision_vendedor_serv_'.$s->personalId};
      }
      if($s->subtotal>0){
        $subtotal_general_m = $s->subtotal;
      }else{
        $subtotal_general_m=0;
      }
      $tot_com=$tot_com+$montocomision;

      echo '<tr>';
        echo '<td>'.$s->nombre.'</td>';
        echo '<td>$'.number_format(($subtotal_general), 2, '.', ',').'</td>';   
        echo '<td>$'.number_format(($s->iva), 2, '.', ',').'</td>';
        echo '<td>$'.number_format(($s->total), 2, '.', ',').'</td>';
        echo '<td>'.$s->name_suc.'</td>';
        echo '<td>$'.number_format(($montocomision), 2, '.', ',').'</td>';
      echo '</tr>';
    } ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="4"></td>
      <td><b>TOTAL:</b></td>
      <td><b><?php echo "$".number_format($tot_com,2,".",","); ?></b></td>
    </tr>
  </tfoot>
</table>