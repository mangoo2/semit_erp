<?php
//require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['result_tra'] = $result_tra;
$GLOBALS['status'] = $status;
$GLOBALS['reg'] = $reg;
$GLOBALS['fechaingreso'] = $fechaingreso;
$GLOBALS['idtras'] = $idtras;

//=======================================================================================
class MYPDF extends TCPDF {
    //Page header Cotización: '.$GLOBALS['idcotizacion'].'
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0);
        $status=$GLOBALS['status'];
        if($status==0 || $status==1){
            $txt="SOLICITUD DE TRASPASO ".date("d-m-Y H:i A",strtotime($GLOBALS['reg']));
        }else{
            $txt="ENTRADA DE MERCANCíA ".date("d-m-Y H:i A",strtotime($GLOBALS['fechaingreso']));
        }
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="38%">
                    </td>
                    <td width="59%" style=" text-align: right;">
                       <span style="font-size: 22px;color: #012d6a;">Solicitud de traspaso</span><br>
                       <span style="font-size: 18px;color: red;">ID traspaso: '.$GLOBALS['idtras'].'</span>
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size: 12px;color: #012d6a; text-align: center;"><br><br>
                        SERVICIOS Y EQUIPOS MÉDICOS INTERNACIONALES DE TOLUCA
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size: 14px;color: #012d6a; text-align: center;">
                        <br><b><u>'.$txt.'</u></b>
                    </td>
                </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 
    }
} 


$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Traspaso');
$pdf->SetTitle('Traspaso');
$pdf->SetSubject('Traspaso');
$pdf->SetKeywords('Traspaso');

// set default header data
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(true);

$pdf->SetFooterMargin(50);
// set auto page breaks
$pdf->SetAutoPageBreak(true,49);

$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '',9);
// add a page
$pdf->SetMargins(1, 65, 1, true);
$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
    .httabled{
        font-size:10px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }
    .info{
        font-size:12px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        text-align:center;
        font-weight:bold;
    }
    .rechazo{
        font-size:10px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        text-align:center;
    }
    .tr_rechazo{
        background-color:rgb(255,64,64,0.7);
    }
    .td_l{
        font-size:9px;
    }
    .resumen{
        background: linear-gradient(-45deg, #93ba1f 50%, rgba(93, 97, 141, 0.5) 50%);
    }
  </style>';

    $html.='<table border="0" cellpadding="2">
        <thead>
            <tr>
              <th width="6%" class="httabled"><b>Id</b></th>
              <th width="16%" class="httabled"><b>Fecha de solicitud</b></th>
              <th width="17%" class="httabled"><b>Usuario solicita</b></th>
              <th width="15%" class="httabled"><b>Sucursal entrada</b></th>
              <th width="14%" class="httabled"><b>Sucursal salida</b></th>
              <th width="24%" class="httabled"><b>Productos</b></th>
              <th width="8%" class="httabled"><b>Cant.</b></th>
            </tr>
        </thead>';
    $id_reg=0;
    foreach ($result_tra as $item){
        $id_reg=$item->id_tras;
        if($item->rechazado==1){
            $style='style="background-color:red"';
        }else{
            $style="";
        }
        if($item->tipo==0){
            $nombre=$item->idProducto." / ".$item->nombre;
            $html.='<tr '.$style.'>
                <td width="6%" class="td_l">'.$item->id_tras.'</td>
                <td width="16%" class="td_l">'.date("d-m-Y h:i A" , strtotime($item->reg)).'</td>
                <td width="17%" class="td_l">'.$item->personal.'</td>
                <td width="15%" class="td_l">'.$item->name_sucxy.'</td>
                <td width="14%" class="td_l">'.$item->name_sucx.'</td>
                <td width="24%" class="td_l">'.$nombre.'</td>
                <td width="8%" style="text-align:center" class="td_l">'.$item->cantidad.'</td>
            </tr>';
            if($item->rechazado==1){
                $html.='<tr><td class="rechazo" colspan="7"><b>Motivo de rechazo:</b> '.$item->motivo_rechazo.'</td></tr>';
            }
            //log_message('error','tipo_prod : '.$item->tipo_prod);
            //log_message('error','status : '.$item->status);
            if($item->tipo_prod==1 && $item->status!=0 && $item->rechazado==0){
                $html.='<tr class="resumen"><td colspan="8" class="info">RESUMEN</td></tr>';
                $result_s=$this->ModelTraspasos->get_traspasos_series_info($id_reg,$item->idproducto);
                foreach ($result_s as $ps){
                    $nombre="<br><b>Serie:</b> ".$ps->serie;
                    $html.='<tr>
                        <td width="6%" class="td_l">'.$item->id_tras.'</td>
                        <td width="16%" class="td_l">'.$item->reg.'</td>
                        <td width="17%" class="td_l">'.$item->personal.'</td>
                        <td width="15%" class="td_l">'.$item->name_sucxy.'</td>
                        <td width="14%" class="td_l">'.$item->name_sucx.'</td>
                        <td width="24%" class="td_l">'.$nombre.'</td>
                        <td width="8%" style="text-align:center" class="td_l">1</td>
                    </tr>';
                }
                $html.='<tr><td colspan="8" class="info"></td></tr>';
            }if($item->tipo_prod==2 && $item->status!=0 && $item->rechazado==0){

                $html.='<tr class="resumen"><td colspan="8" class="info">RESUMEN</td></tr>';
                $result_s=$this->ModelTraspasos->get_traspasos_lotes_info($id_reg,$item->idproducto);
                foreach ($result_s as $ps){
                    $nombre="<br><b>Lote:</b> ".$ps->lote;
                    $html.='<tr>
                        <td width="6%" class="td_l">'.$item->id_tras.'</td>
                        <td width="16%" class="td_l">'.$item->reg.'</td>
                        <td width="17%" class="td_l">'.$item->personal.'</td>
                        <td width="15%" class="td_l">'.$item->name_sucxy.'</td>
                        <td width="14%" class="td_l">'.$item->name_sucx.'</td>
                        <td width="24%" class="td_l">'.$nombre.'</td>
                        <td width="8%" style="text-align:center" class="td_l">'.$ps->cantidad.'</td>
                    </tr>';
                }
                $html.='<tr><td colspan="8" class="info"></td></tr>';
            }/*else{
                $nombre=$item->nombre;
                $html.='<tr>
                    <td class="td_l"></td>
                    <td class="td_l">'.$item->id_tras.'</td>
                    <td class="td_l">'.$item->reg.'</td>
                    <td class="td_l">'.$item->personal.'</td>
                    <td class="td_l">'.$item->name_sucxy.'</td>
                    <td class="td_l">'.$nombre.'</td>
                    <td class="td_l">'.$item->cantidad.'</td>
                    <td class="td_l">'.$item->name_sucx.'</td>
                </tr>';
            }*/
        }else{
            $nombre=$item->codigo." - Recarga de oxígeno";
            $html.='<tr '.$style.'>
                <td width="6%" class="td_l">'.$item->id_tras.'</td>
                <td width="16%" class="td_l">'.date("d-m-Y h:i A" , strtotime($item->reg)).'</td>
                <td width="17%" class="td_l">'.$item->personal.'</td>
                <td width="15%" class="td_l">'.$item->name_sucxy.'</td>
                <td width="14%" class="td_l">'.$item->name_sucx.'</td>
                <td width="24%" class="td_l">'.$nombre.'</td>
                <td width="8%" style="text-align:center" class="td_l">'.$item->cantidad.'</td>
            </tr>';
        }
    }
    $html.='</table>';



$pdf->writeHTML($html, true, false, true, false, '');

//$url=FCPATH.'doc_cotizaciones/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
//$pdf->Output($url.'Traspaso_'.$id_reg.'.pdf', 'FI');
$pdf->Output('Traspaso_'.$id_reg.'.pdf', 'I');
?>