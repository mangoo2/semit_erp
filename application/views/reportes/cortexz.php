<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');  
   
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
    $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 110,35, '', '', '', false, 330, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr>
            </table>';
      $this->writeHTML($html, true, false, true, false, '');
      /*$img_header = FCPATH.'public/img/SEMIT.jpg';
      $this->Image($img_header, 5, 5, '50', 22, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $html='';
      $this->writeHTML($html, true, false, true, false, '');*/
  }
    // Page footer
  public function Footer() {
        //$img_header = FCPATH.'public/img/footer.JPG';
      //$this->Image($img_header, 125, 252, 63, 45, '', '', '', false, 100, '', false, false, 0);
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Arqueo');
$pdf->SetSubject('Arqueo');
$pdf->SetKeywords('Arqueo');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('5', '30', '5');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 8);
// add a page
$pdf->AddPage();
$html=$r_xz_head=$this->ModeloCatalogos->generareportexz($idarqueo,0,1);

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->AddPage();
$html=$r_xz_head=$this->ModeloCatalogos->generareportexz($idarqueo,1,1);

$pdf->writeHTML($html, true, false, true, false, '');

//$pdf->IncludeJS('print();');
//$pdf->Output('Captura.pdf', 'I');
$pdf->Output(FCPATH.'uploads/cortesxz/costexz_'.$idarqueo.'.pdf', 'FI');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>