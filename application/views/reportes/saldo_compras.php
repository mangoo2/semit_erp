

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">    
        <div class="card card-custom gutter-b" style="margin-top: 10px;"> 
            <div class="row">
                <div class="col-md-6">
                    <label>Proveedor</label>
                    <select id="id_provee" class="form-select">
                    </select>
                </div>
                <div class="col-lg-2">
                    <label>Fecha inicio</label>
                    <input class="form-control" type="date" id="fechai">
                </div>
                <div class="col-lg-2">
                    <label>Fecha fin</label>
                    <input class="form-control" type="date" id="fechaf" value="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="col-lg-2">
                    <label></label>
                    <div class="col-lg-12">
                        <button title="Exportar registro" id="btn_export_excel" type="button" class="btn btn-success">Exportar tabla <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>  
        <div class="card card-custom gutter-b" style="margin-top: 10px;">
            <div class="row">
                <div class="col-lg-5">
                    <div class="card-body">
                        <div class="" style="min-height: 180px;">
                            <div class="col-md-12" style="text-align: center;">
                                <h3 style="color: #1770aa;">Compras totales</h3>
                            </div>
                            <div class="col-md-12 info_compras"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="">
                        <div class="card-body">
                            <h3 style="color: #1770aa; text-align: center;">Gráfica de Compras</h3>
                            <div style="height:50px"></div>
                            <div id="basic-apex2"></div>
                            <div style="height:10px"></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

    </div>
</div>
