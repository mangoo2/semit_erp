<?php
//require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['provee'] = $result_productos[0]->provee;
$GLOBALS['codigo_pro'] = $result_productos[0]->codigo_pro;
$GLOBALS["empleado"] = $this->session->userdata("empleado");
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header Cotización: '.$GLOBALS['idcotizacion'].'
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0);
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="38%">
                    </td>
                    <td width="59%" style=" text-align: right;">
                       <span style="font-size: 22px;color: #012d6a;">Reporte por Proveedor</span><br>
                       <span style="font-size: 12px;color: #012d6a;">Generado por: '.$GLOBALS['empleado'].'</span><br>
                       <span style="font-size: 12px;color: #012d6a;">Fecha de generación: '.date("d-m-Y").'</span>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="font-size: 14px;color: #012d6a; text-align: center;">
                        <br><b><i>Nombre del proveedor: '.$GLOBALS['provee'].'</i></b>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="font-size: 14px;color: #012d6a; text-align: center;">
                        <br><b><i>Código de proveedor: '.$GLOBALS['codigo_pro'].'</i></b>
                    </td>
                </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Traspaso');
$pdf->SetTitle('Traspaso');
$pdf->SetSubject('Traspaso');
$pdf->SetKeywords('Traspaso');

// set default header data
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(true);

$pdf->SetFooterMargin(50);
// set auto page breaks
$pdf->SetAutoPageBreak(true,49);

$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '',9);
// add a page
$pdf->SetMargins(1, 65, 1, true);
$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
    .httabled{
        font-size:10px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
        font-weight: bold;
        text-align:center;
    }
    .td_l{
        font-size:9px;
    }
  </style>';

    $html.='<table border="0" cellpadding="2">
        <thead>
            <tr>
                <th width="10%" class="httabled">Código</th>
                <th width="45%" class="httabled">Nombre de Producto</th>
                <th colspan="2" width="30%" class="httabled">Costo</th>
                <!--<th width="15%" class="httabled">Compra</th>
                <th width="15%" class="httabled">Venta</th>-->
                <th width="15%" class="httabled">Código Proveedor</th>
            </tr>
            <tr>
                <th width="55%" class="httabled"></th>
                <th width="15%" class="httabled">Compra</th>
                <th width="15%" class="httabled">Venta</th>
                <th width="15%" class="httabled"></th>
            </tr>
        </thead>';
    $id_reg=0;
    foreach ($result_productos as $p){
        $html.='<tr>
            <td width="10%" class="td_l">'.$p->idProducto.'</td>
            <td width="45%" class="td_l">'.$p->nombre.'</td>
            <td width="15%" class="td_l">$'.number_format($p->costo_compra,2,".",",").'</td>
            <td width="15%" class="td_l">$'.number_format($p->precio,2,".",",").'</td>
            <td width="15%" class="td_l">'.$p->cod_provee.'</td>
        </tr>';
    }
    $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Productos_proveedor'.$id.'.pdf', 'I');
?>