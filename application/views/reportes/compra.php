<?php
require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');

$GLOBALS['idcompra']=$idcompra;
$GLOBALS['reg']=$reg;
$GLOBALS['reg2']=$reg2;
$GLOBALS['fecha_entrega']=$fecha_entrega;
$GLOBALS['hora_entrega']=$hora_entrega;
$GLOBALS['direccion']=$direccion;
$GLOBALS['proveedor']=$proveedor;
$GLOBALS['codigo']=$codigo;
$GLOBALS['cp']=$cp;
$GLOBALS['direccion_entrega']=$direccion_entrega;
$GLOBALS['rfc']=$rfc;
$GLOBALS['factura']="";
if(isset($factura)){
    $GLOBALS['factura']=$factura;    
}
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0); 
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:18px;color: #012d6a; text-align: right;">Orden de compra
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Folio: '.$GLOBALS['idcompra'].'
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha de creación: '.$GLOBALS['reg'].'
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha de entrega: '.$GLOBALS['fecha_entrega'].'
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Hora de entrega: '.$GLOBALS['hora_entrega'].'
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Factura: '.$GLOBALS['factura'].'
                    </td>
                </tr>
                <tr>
                    <td width="4%">
                    </td>
                    <td width="47%" style="font-size:12px;color: #012d6a;"><br><br>Proveedor<br>'.$GLOBALS['proveedor'].'<br>CÓDIGO: '.$GLOBALS['codigo'].'<br>RFC: '.$GLOBALS['rfc'].'<br>'.$GLOBALS['direccion'].' '.$GLOBALS['cp'].'
                    </td>
                    <td width="4%">
                    </td>
                    <td width="45%" style="font-size:12px;color: #012d6a; text-align: right;"><br><br>Dirección de entrega<br>
                    '.$GLOBALS['direccion_entrega'].'
                    </td>
                </tr>
            </table>';
          $html.='<table><tr><th height="35px"></th></tr></table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 

        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', '', 8);
        // Custom footer HTML
        $this->html = '<table border="0"><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: center;"><b>
                      Para el recibo de la mercancia es indespensable traer: Original<br>
                      de factura y una copia así como copia de la Orden de Compra.<br>
                      Gracias</b>
                    </td>
                </tr>
              </table>';
        $this->writeHTML($this->html, true, false, true, false, '');

        //$this->setBarcode(date('Y-m-d H:i:s'));
        $style = array(
            'position' => 'C',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        
        $codigo=$GLOBALS['idcompra'].date("YmdGis", strtotime($GLOBALS['reg2']));
        $this->SetY(-36);
        $this->writeHTML($this->write1DBarcode($codigo, 'CODABAR', '', '', '', 18, 0.5, $style, 'N'), true, false, true, false, '');
        
        //$this->write1DBarcode($codigo, 'CODABAR', '', '', '', 18, 0.5, $style, 'N');
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Compra');
$pdf->SetTitle('Compra');
$pdf->SetSubject('Compra');
$pdf->SetKeywords('Compra');

// set default header data
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(true);

$pdf->SetFooterMargin(50);
// set auto page breaks
$pdf->SetAutoPageBreak(true, 50);

$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('helvetica', '',10);
// add a page
$pdf->SetMargins(0, 78, 7, true);
$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
    .httabled{
        font-size:11px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }

    .td_l{
        font-size:9.5px;
    }
  </style>';

$html.='<table border="0" cellpadding="4" width="100%">
    <thead>
    <tr>
      <th width="1%" class="httabled"><b></b></th>
      <th width="10%" class="httabled"><b>Código</b></th>
      <th width="30%" class="httabled"><b>Producto</b></th>
      <th width="8%" class="httabled"><b>Cant.</b></th>
      <th width="10%" class="httabled"><b>Precio U.</b></th>
      <th width="10%" class="httabled"><b>Subtotal</b></th>
      <th width="10%" class="httabled"><b>IVA</b></th>
      <th width="10%" class="httabled"><b>ISR</b></th>
      <th width="11%" class="httabled"><b>Importe</b></th>
    </tr>
    </thead>
    <tbody>';
    foreach ($result_productos->result() as $item){
        $importe=($item->cantidad*$item->precio_unitario);
        if($item->tipo==0){
            $nombre=$item->nombre;
            if($item->tipo_prod==1){
                $nombre=$nombre."<br>".$item->serie;
            }
            if($item->tipo_prod==2){
                $nombre=$nombre."<br>".$item->lote;
            }
            $idproducto=$item->idProducto;
        }else{
            $nombre="Recarga de oxígeno de ".$item->capacidad."L";
            $idproducto=$item->codigo;
        }
        $incluye_iva = $item->incluye_iva_comp;
        $iva = $item->iva_comp;
        $sub_iva=0; $sub_isr=0;
        if($item->tipo==0){
            $precio_con_ivax=0;
            if($incluye_iva==1){
                $precio_con_ivax=round($item->precio_unitario,6);
                if($iva==0){
                    $incluye_iva=0;
                    //$precio_con_ivax=round($item->precio_unitario/1.16,2);
                    $precio_con_ivax=round($item->precio_unitario,6);
                }else{
                    //$precio_con_ivax=round($item->precio_unitario*1.16,2);
                    //$precio_con_ivax=round($item->precio_unitario/1.16,2);
                    $precio_con_ivax=round($item->precio_unitario,6);
                    $sub_iva=round($item->precio_unitario*.16,2);
                    //log_message('error','sub_iva: '.$sub_iva);
                }
            }else{
                if($iva>0){
                    $siva=1.16;
                    //$precio_con_ivax=round($item->precio_unitario/$siva,2);
                    //$precio_con_ivax=round($item->precio_unitario*$siva,2);
                    $precio_con_ivax=round($item->precio_unitario,6);
                }else{
                    $precio_con_ivax=round($item->precio_unitario,6);  
                }
            }
            $sub_iva=$sub_iva*$item->cantidad;
            $sub_importe=($item->cantidad*$precio_con_ivax);
            if($item->porc_isr>0){
               $sub_isr= $sub_importe*($item->porc_isr/100);
            }
            $importe=($item->cantidad*$precio_con_ivax)+$sub_iva-$sub_isr;
        }else{
            $sub_iva=0; $sub_isr=0;
            $cant_litros = $item->cantidad * 9500;
            $sub_importe=($cant_litros*$item->precio_unitario);
            $importe=($cant_litros*$item->precio_unitario);
            $precio_con_ivax=$item->precio_unitario;
        }
        $html.='<tr>
            <td width="1%"></td>
            <td width="10%" class="td_l">'.$idproducto.'</td>
            <td width="30%" class="td_l">'.$nombre.'</td>
            <td width="8%" class="td_l">'.$item->cantidad.'</td>
            <td width="10%" class="td_l">$'.number_format($precio_con_ivax,6).'</td>
            <td width="10%" class="td_l">$'.number_format($sub_importe,6).'</td>
            <td width="10%" class="td_l">$'.number_format($sub_iva,4).'</td>
            <td width="10%" class="td_l">$'.number_format($sub_isr,4).'</td>
            <td width="11%" class="td_l">$'.number_format($importe,4,'.',',').'</td>
        </tr>';

    }
$html.='</tbody></table>';

$subtotal=0;
$iva=0;
$total=0;
foreach ($result_compra->result() as $y) {
    $subtotal=$y->subtotal;
    $iva=$y->iva;
    $isr=$y->isr;
    $total=$y->total;
}

$html.='<br><br><table border="0" cellpadding="3">
        <tr>
          <td width="100%" style="font-size:12px; color:#012d6a; text-align: right;">Subtotal: $'.number_format($subtotal,4,'.',',').'</td>
          <td style="font-size:12px; color:#012d6a;"></td>
        </tr>
        <tr>
          <td width="100%" style="font-size:12px; color:#012d6a; text-align: right;">IVA: $'.number_format($iva,4,'.',',').'</td>
        </tr>
        <tr>
          <td width="100%" style="font-size:12px; color:#012d6a; text-align: right;">ISR: $'.number_format($isr,4,'.',',').'</td>
        </tr>
        <tr>
          <td width="100%" style="font-size:12px; color:#012d6a; text-align: right;"><b>Total: $'.number_format($total,4,'.',',').'</b></td>
        </tr>
        <tr>
          <td width="2%"></td>
          <td class="httabled" width="98%" style="font-size:12px; color:#012d6a;"><b>Observaciones:</b></td>
        </tr>
        <tr>
          <td width="2%"></td>
          <td width="98%" style="font-size:12px; color:#012d6a;">'.$observaciones.'</td>
        </tr>
    </table>

    <table>
        <tr><th></th></tr>
    </table>';

$pdf->writeHTML($html, true, false, true, false, '');

/*$style = array(
    'position' => 'C',
    'align' => 'C',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => true,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);

$this->load->model('General_model');
$codigo=$GLOBALS['idcompra'].date("YmdGis", strtotime($GLOBALS['reg2']));
$codegenerate = $this->write1DBarcode($codigo, 'CODABAR', '', '', '', 18, 0.5, $style, 'N');
$this->General_model->edit_record('id',$id,array("codigo"=>$codigo),'compra_erp');*/

//$url=FCPATH.'doc_compra_/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
//$pdf->Output($url.'compra_'.$idcotizacion.'.pdf', 'FI');
$pdf->Output('compra'.$idcompra.'.pdf', 'I');
?>