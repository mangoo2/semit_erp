<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');  
    $GLOBALS['folioarqueo']=$folioarqueo; 
    $GLOBALS['personal']=$personal;  
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr>
            </table>';
      $html.='<style type="text/css">
                .title1{
                    color:#035397;
                    font-size:22px;
                }
                .titletd{
                    font-size:15px;
                }
                .title2{
                    color:#152342;
                }
                .title3{
                    color:#b9c61e;
                }
            </style>
            <table border="0">
                <tr>
                    <td style="width:64%;"></td>
                    <td style="width:36%"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="title1">Arqueo de caja</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="titletd"><span class="title2">No. Arqueo: </span><span class="title3">'.$GLOBALS['folioarqueo'].'</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="titletd"><span class="title2">Empleado:</span> <span class="title3">'.$GLOBALS['personal'].'</span></td>
                </tr>
            </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
        $img_header = FCPATH.'public/img/footer.JPG';
      $this->Image($img_header, 125, 252, 63, 45, '', '', '', false, 100, '', false, false, 0);
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Arqueo');
$pdf->SetSubject('Arqueo');
$pdf->SetKeywords('Arqueo');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '65', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();
$html='<style type="text/css">
        .c_blue{
            color:#152342;
        }
        .c_green{
            color:#c6d420;
        }
        .t_c{
            text-align:center;
        }
        .t_bold{
            font-weight:bold;
        }
        .t_size_12{
            font-size:12px;
        }

        .httablecom{
            border-bottom: 1px solid #019cde;
        }
        .httabletop{
            border-top: 1px solid #019cde;
        }
       </style>';
$html.='<table border="0">
            <tr>
                <td class="c_blue t_c t_bold t_size_12">SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</td>
            </tr>
            <tr>
                <td class="c_blue t_c t_size_12">Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</td>
            </tr>
            <tr>
                <td class="c_blue t_c t_size_12"><b>Sucursal:</b> '.$sucursal.'</td>
            </tr>
        </table>';
$html.='<p></p><p></p>';
$html.='<table border="0">';
$html.='            <tr><td width="20%"></td>
            <td width="60%">
                <table border="0" cellpadding="3">
                    <thead>
                        <tr><td  class="httablecom httabletop"><b style="color:#019cde">Venderdor: </b><b><span class="c_green">'.$personal.'</span></b></td><td class="httablecom httabletop"></td></tr>
                    </thead>';
          $html.='';
                    $suma=0;
                    foreach ($result_dll as $item) {
                        $html.='<tr><td class="httablecom">'.$item->formapago_alias.' '.$item->banco.'</td><td class="httablecom">$ '.number_format(($item->monto),2,'.',',').'</td></tr>';
                        $suma+=$item->monto;
                    }
                    $html.='<tr><td>Total</td><td>$ '.number_format(($suma),2,'.',',').'</td></tr>';
        $html.='</table>
            </td><td width="20%" ></td></tr>';
$html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');

//$pdf->IncludeJS('print();');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>