<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
//===============================
  //=========================================================
  $fechasalida='';$su_des='x';
              $datosFactura_cp=$this->ModeloCatalogos->getselectwheren('f_facturas_carta_porte',array('FacturasId'=>$idfactura));
              $datosFactura_cp=$datosFactura_cp->row();
              $datosFactura_u=$this->ModeloCatalogos->getselectwheren('f_facturas_ubicaciones',array('FacturasId'=>$idfactura));
              
              $datosFactura_u_o=$this->ModeloCatalogos->info_ubicaciones_cartaporte($idfactura,'Origen');
              $datosFactura_u_d=$this->ModeloCatalogos->info_ubicaciones_cartaporte($idfactura,'Destino');

              $datosFactura_fms=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancias',array('FacturasId'=>$idfactura));
              $datosFactura_fms=$datosFactura_fms->row();
              $datosFactura_fm=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancia',array('FacturasId'=>$idfactura));
              $datosFactura_tf=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_fed',array('FacturasId'=>$idfactura));
              $datosFactura_tf = $datosFactura_tf->row();
              $datosFactura_ta=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_aereo',array('FacturasId'=>$idfactura));
              $datosFactura_fa=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_arrendatario',array('FacturasId'=>$idfactura));
              $datosFactura_fo=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_operadores',array('FacturasId'=>$idfactura));
              $datosFactura_fp=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_propietario',array('FacturasId'=>$idfactura));
              $datosFactura_ft=$this->ModeloCatalogos->getselectwheren('f_facturas_figura_transporte',array('FacturasId'=>$idfactura));
              $datosFactura_ft=$datosFactura_ft->row();

              
              $datos_cv=$this->ModeloCatalogos->getselectwheren('f_c_configautotransporte',array('clave'=>$datosFactura_tf->ConfigVehicular));
              $ConfigVehicular_cl=$datosFactura_tf->ConfigVehicular;
              $ConfigVehicular='';
              foreach ($datos_cv->result() as $itemcv) {
                $ConfigVehicular=$itemcv->descripcion;
              }
              
              $datos_per=$this->ModeloCatalogos->getselectwheren('f_c_tipopermiso',array('clave'=>$datosFactura_tf->PermSCT));
              $PermSCT_name='';
              foreach ($datos_per->result() as $itemp) {
                $PermSCT_name=$itemp->descripcion;
              }
              foreach ($datosFactura_u->result() as $itemu) {
                if($itemu->TipoUbicacion=='Origen'){
                  $fechasalida=$itemu->FechaHoraSalidaLlegada;
                }
              }
              foreach ($datosFactura_u_d->result() as $item_d) {
                $su_des='S'.$item_d->id_alias;
              }
              foreach ($datosFactura_u_o->result() as $item_o) {
                $direccion_salida=$item_o->domicilio;
              }
     
  //==================================================================
  $GLOBALS["logotipo"]=$logotipo;

  $GLOBALS["Folio"]=$Folio;
  $GLOBALS["Nombrerasonsocial"]=$Nombrerasonsocial;
  $GLOBALS["folio_fiscal"]=$folio_fiscal;
  $GLOBALS["rrfc"]=$rrfc;
  $GLOBALS["rdireccion"]=$rdireccion;
  $GLOBALS["nocertificadosat"]=$nocertificadosat;
  $GLOBALS['regimenf']=$regimenf;
  $GLOBALS["certificado"]=$certificado;
  $GLOBALS["cfdi"]=$cfdi;
  $GLOBALS["fechatimbre"]=$fechatimbre;
  $GLOBALS["Estado"]=1;//en dado caso de que se encuentre cancelado
  $GLOBALS["cp"]=$cp;
  $GLOBALS["cliente"]=$cliente;
  $GLOBALS["clirfc"]=$clirfc;
  $GLOBALS["clidireccion"]=$clidireccion.' C.P. '.$cp;
  $GLOBALS["isr"]=$isr;
  $GLOBALS['numordencompra']=$numordencompra;
  $GLOBALS["ivaretenido"]=$ivaretenido;
  $GLOBALS["cedular"]=$cedular;
  $GLOBALS["cincoalmillarval"]=$cincoalmillarval;
  $GLOBALS["outsourcing"]=$outsourcing;

  $GLOBALS["f_relacion"]=$f_relacion;
  $GLOBALS["f_r_tipo"]=$f_r_tipo;
  $GLOBALS["f_r_uuid"]=$f_r_uuid;

  $GLOBALS["ticketmensaje"]=$configticket->mensaje;


  if ($numproveedor!='') {
      $GLOBALS['numproveedor']=$numproveedor;
      $GLOBALS['numproveedorv']='block';
  }else{
      $GLOBALS['numproveedor']='';
      $GLOBALS['numproveedorv']='none';
  }

  if ($Caducidad!='') {
      $GLOBALS['Caducidad']=$Caducidad;
      $GLOBALS['Caducidadv']='block';
  }else{
      $GLOBALS['Caducidad']='';
      $GLOBALS['Caducidadv']='none';
  }

  if ($Lote!='') {
      $GLOBALS['Lote']=$Lote;
      $GLOBALS['Lotev']='block';
  }else{
      $GLOBALS['Lote']='';
      $GLOBALS['Lotev']='none';
  }

  if ($numordencompra!='') {
      $GLOBALS['numordencompra']=$numordencompra;
      $GLOBALS['numordencomprav']='block';
  }else{
      $GLOBALS['numordencompra']='';
      $GLOBALS['numordencomprav']='none';
  }
  $GLOBALS['observaciones']=$observaciones;
  $GLOBALS["total"]=$total;
  $GLOBALS["moneda"]=$moneda;
  if ($moneda=='MXN') {
    $GLOBALS['abreviaturamoneda']='MXN';
  }else{
    $GLOBALS['abreviaturamoneda']='USD';
  }
  $GLOBALS["subtotal"]=$subtotal;
  $GLOBALS["iva"]=$iva;
  $GLOBALS["tarjeta"]=$tarjeta;
  $GLOBALS["selloemisor"]=$selloemisor;
  $GLOBALS["sellosat"]=$sellosat;
  $GLOBALS["cadenaoriginal"]=$cadenaoriginal;
  if ($cadenaoriginal=='') {
    $serie='';
  }else{
    $series=explode("|", $cadenaoriginal);
    $serie=$series[3];
  }
  $GLOBALS["serie"]=$serie;

  $GLOBALS["FormaPago"]=$FormaPago;
  $GLOBALS["FormaPagol"]=$FormaPagol;
  $GLOBALS["MetodoPago"]=$MetodoPago;
  $GLOBALS["MetodoPagol"] =$MetodoPagol; 
  $GLOBALS["tipoComprobante"]=$tipoComprobante;
  $GLOBALS["facturadetalles"]=$facturadetalles;
  $GLOBALS["RegimenFiscalReceptor"]=$RegimenFiscalReceptor;
  $GLOBALS["LugarExpedicion"]=$LugarExpedicion;
  $GLOBALS["LugarExpedicion_suc"]=$LugarExpedicion_suc;
  //======================================================
  $sucu=$sucu->result();
  $GLOBALS["sucu_name1"]=$sucu[0]->name_suc;
  $GLOBALS["sucu_name2"]=$sucu[1]->name_suc;
  $GLOBALS["sucu_name3"]=$sucu[2]->name_suc;
  $GLOBALS["sucu_name4"]=$sucu[3]->name_suc;

  $GLOBALS["sucu_dom1"]=$sucu[0]->domicilio;
  $GLOBALS["sucu_dom2"]=$sucu[1]->domicilio;
  $GLOBALS["sucu_dom3"]=$sucu[2]->domicilio;
  $GLOBALS["sucu_dom4"]=$sucu[3]->domicilio;

  $GLOBALS["sucu_tel1"]=$sucu[0]->tel;
  $GLOBALS["sucu_tel2"]=$sucu[1]->tel;
  $GLOBALS["sucu_tel3"]=$sucu[2]->tel;
  $GLOBALS["sucu_tel4"]=$sucu[3]->tel;
  //====================================================
//====================================================
class MYPDF extends TCPDF {
  //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='MXN') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            if($t == $this->Void) 
            { 
              $i = floatval($i) + 1; 
              $x = $x / 1000; 
              if($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
  //===========================================================
      private $totalPages; // Variable para almacenar el número total de páginas
      // Constructor
      public function __construct() {
          parent::__construct();
          $this->totalPages = 0; // Inicializar la variable
      }
      public function setTotalPages($totalPages) {
        $this->totalPages = $totalPages;
      }
  //===========================================================
  //Page header
  public function Header() {
      $html = '';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td align="center"></td>
        </tr>
        <tr>
          <td align="right" class="footerpage">
          Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      $this->writeHTML($html2, true, false, true, false, '');
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$SetFooterMarginvalor=16;
$pdf->SetFooterMargin($SetFooterMarginvalor);

// set auto page breaks
$pdf->SetAutoPageBreak(true, $SetFooterMarginvalor);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$htmlp='';
$htmlp.='<table border="0" cellpadding="1">
        <tr>
          <td rowspan="4" width="50%"><img src="'.$logotipo.'" height="50px"></td>
          <td width="25%"><b>No. Traspaso</b></td>
          <td width="25%"><b>Fecha Traspaso</b></td>
        </tr>
        <tr>
          <td>'.$no_traspaso.'</td>
          <td>'.$fecha_traspaso.'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Fecha Expedicion :</b>'.$GLOBALS["fechatimbre"].'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Serie del Certificado :</b>'.$GLOBALS["certificado"].'</td>
        </tr>
        <tr>
          <td><b>SERVICIOS Y EQUIPOS MÉDICOS INTERNACIONALES</b></td>
          <td colspan="2"><b>Serie del Cert. SAT:</b>'.$GLOBALS["nocertificadosat"].'</td>
        </tr>
        <tr>
          <td rowspan="3">'.$GLOBALS["sucu_dom1"].'</td>
          <td colspan="2"><b>Uso de CFDI :</b>'.$GLOBALS["cfdi"].'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Lugar de expedicion :</b>'.$LugarExpedicion_suc.'</td>
        </tr>
        <tr>
          <td colspan="2"><b>Tipo de comprobante :</b>Traslado</td>
        </tr>
        <tr>
          <td colspan="3"></td>
        </tr>
        <tr>
          <td><b>Facturado a:</b></td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td>'.$GLOBALS["cliente"].'</td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td><b>Dirección:</b>'.$direccion_salida.'</td>
          <td colspan="2">'.$su_des.'</td>
        </tr>

       </table>';

$htmlp.='
      <style type="text/css">
        .httable{
          vertical-align: center;
          border-bottom: 2px solid #9e9e9e;
          border-top: 1px solid #9e9e9e;

        }
        .httablepro{
            vertical-align: center;
            border-bottom: 1px solid #9e9e9e;
          }
        .magintablepro{
            margin-top:0px;
            margin-bottom:0px;
            margin: 0px;
        }
        .font8{
            font-size: 7.5px;
          }
        .bcj{
          background-color:#152342;
          color:white;
        }
      </style>
  <table border="0" align="center" cellpadding="2">
    <thead>
    <tr valign="middle">
      <th width="10%" class=" bcj"><b>Codigo</b></th>
      <th width="10%" class="bcj"><b>Cantidad</b></th>
      <th width="48%" class="bcj"><b>Descripcion</b></th>
      <th width="10%" class="bcj"><b>Unidad</b></th>      
      <th width="11%" class="bcj"><b>De Almacen</b></th>
      <th width="11%" class="bcj"><b>A almacen</b></th>
    </tr></thead><tbody>
    ';
$rowr=0;
foreach ($facturadetalles as $item) {
  
  //for ($i = 1; $i <= 43; $i++) {
    $rowr++;
    if($rowr==15 || $rowr==29 || $rowr==43){
      $htmlp.='<tr><td colspan="6"></td></tr>';
    }
    $cod="";
    /*if($item->tipo_prod==1 || $item->tipo_prod==2){
      $cod = $item->idProducto;
    }else{
      $cod = $item->codigo;
    } */ 
      $htmlp.='<tr class="magintablepro">';
        $htmlp.='<td width="10%" class="httablepro font8" align="center">'.$item->servicioId.'</td>';
        $htmlp.='<td width="10%" class="httablepro font8" align="center">'.$item->Cantidad.'</td>';
        $htmlp.='<td width="48%" class="httablepro font8">'.$item->Descripcion2.'</td>';
        $htmlp.='<td width="10%" class="httablepro font8" align="center">'.$item->Unidad .' / '.$item->nombre.'</td>';
        $htmlp.='<td width="11%" class="httablepro font8" align="center">'.$suc_s.'</td>';
        $htmlp.='<td width="11%" class="httablepro font8" align="center">'.$suc_e.'</td>';
      $htmlp.='</tr>';
  //}
  
}
$htmlp.='</tbody></table>';
$htmlp.='<table><tr><td></td></tr></table>';
$styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $urlfolio_fe=substr($GLOBALS["selloemisor"], -8); 
          $url='https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?&id='.$GLOBALS["folio_fiscal"].'&re='.$GLOBALS['rrfc'].'&rr='.$GLOBALS["clirfc"].'&tt=0000000000.000000&fe='.$urlfolio_fe;
          $params = $pdf->serializeTCPDFtagParameters(array($url, 'QRCODE,Q', '', '', 40, 40, $styleQR, 'N'));
$htmlp.='<table border="0" cellpadding="1">
            <tr>
              <td width="15%" rowspan="7"><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
              <td width="85%"> <b>Numero Carta Porte:</b>'.$GLOBALS["Folio"].'</td>
            </tr>
            <tr>
              <td> <b>Forma de pago:</b>99</td>
            </tr>
            <tr>
              <td> <b>Metodo de pago :</b>PUE</td>
            </tr>
            <tr>
              <td> <b>Folio Fiscal: </b>'.$GLOBALS["folio_fiscal"].'</td>
            </tr>
            <tr>
              <td> <b>Fecha y hora de certificación:</b>'.$GLOBALS["fechatimbre"].'</td>
            </tr>
            <tr>
              <td> <b>Moneda:</b>XXX</td>
            </tr>
            <tr>
              <td> <b>Tipo:</b>T - Traslado</td>
            </tr>
            
          </table>';

$htmlp.='<table border="1" cellpadding="2" align="center">
          <tr><td><b>Sello Digital del CFDI</b></td></tr>
          <tr><td>'.$GLOBALS["selloemisor"].'</td></tr>
          <tr><td><b>Sello Digital del SAT</b></td></tr>
          <tr><td>'.$GLOBALS["sellosat"].'</td></tr>
          <tr><td><b>Cadena Original del complemente de certificación digital del SAT</b></td></tr>
          <tr><td>'.substr(utf8_encode($GLOBALS["cadenaoriginal"]), 0, 635).'</td></tr>
        </table>';
$htmlp.='<table align="center"><tr><td><b>Complemento Carta Porte</b></td></tr></table>';
$htmlp.='<table >
              <tr><td>Medio de transporte</td><td>Transporte Internacional</td><td>Tipo de transporte Internacional</td><td>Via de transporte internacional</td></tr>
              <tr><td></td><td>No</td><td></td><td></td></tr>
              <tr>
                  <td>Permiso STCC</td>
                  <td>'.$datosFactura_tf->PermSCT.'</td>
                  <td colspan="2">'.$PermSCT_name.'</td>
              </tr>
              <tr>
                  <td>Tipo de Auto transporte</td>
                  <td>'.$ConfigVehicular_cl.'</td>
                  <td colspan="2">'.$ConfigVehicular.'</td>
              </tr>
          </table>';
$htmlp.='<table>
          <tr>
            <td><b>Número de permiso</b></td>
            <td><b>Aseguradora</b></td>
            <td><b>Número de Póliza </b></td>
            <td><b>Placas</b></td>
            <td><b>Año</b></td>
          </tr>
          <tr>
            <td>'.$datosFactura_tf->NumPermisoSCT.'</td>
            <td>'.$datosFactura_tf->NombreAseg.'</td>
            <td>'.$datosFactura_tf->NumPolizaSeguro.'</td>
            <td>'.$datosFactura_tf->PlacaVM.'</td>
            <td>'.$datosFactura_tf->AnioModeloVM.'</td>
          </tr>
        </table>';  

$htmlp.='<table align="center"><tr><td><b>Mercancias</b></td></tr></table>';
$htmlp.='<table border="0" align="center" cellpadding="2">
    <thead>
    <tr valign="middle">
      <th width="11%" class=" "><b>ClaveBienes Transportados</b></th>
      
      <th width="40%" class=""><b>Descripcion</b></th>
      <th width="10%" class=""><b>Cantidad</b></th>
      <th width="10%" class=""><b>clave de Unidad</b></th>      
      <th width="10%" class=""><b>Peso en KG</b></th>
      <th width="11%" class=""><b>Valor Mercancia</b></th>
      <th width="11%" class=""><b>Peso Bruto Total</b></th>
    </tr></thead><tbody>
    ';
$rowr=0;
/*
foreach ($facturadetalles as $item) {
  if($item->iva>0){
    $valor=$item->Importe*1.16;
  }else{
    $valor=$item->Importe;
  }
  $peso=1;
  
  $pesob=$item->Cantidad*$peso;
  $htmlp.='
      <tr class="magintablepro">
        <td width="11%" class=" font8" align="center">'.$item->servicioId.'</td>
        <td width="40%" class=" font8">'.$item->Descripcion2.'</td>
        <td width="10%" class=" font8" align="center">'.$item->Cantidad.'</td>
        <td width="10%" class=" font8" align="center">'.$item->Unidad .' / '.$item->nombre.'</td>        
        <td width="10%" class=" font8" align="center">'.$item->Cantidad.'</td>
        <td width="11%" class=" font8" align="center">'.round($valor, 2).'</td>
        <td width="11%" class=" font8" align="center">'.$item->Cantidad.'</td>
      </tr>';
  //}
  
}
*/
foreach ($datosFactura_fm->result() as $item) {

      $htmlp.='<tr class="magintablepro">';
        $htmlp.='<td width="11%" class=" font8" align="center">'.$item->BienesTransp.'</td>';
        $htmlp.='<td width="40%" class=" font8">'.$item->Descripcion.'</td>';
        $htmlp.='<td width="10%" class=" font8" align="center">'.$item->Cantidad.'</td>';
        $htmlp.='<td width="10%" class=" font8" align="center">'.$item->ClaveUnidad .' / '.$item->Unidad.'</td>';
        $htmlp.='<td width="10%" class=" font8" align="center">'.$item->PesoEnKg.'</td>';
        $htmlp.='<td width="11%" class=" font8" align="center">'.round($item->ValorMercancia, 2).'</td>';
        $htmlp.='<td width="11%" class=" font8" align="center">'.$item->PesoEnKg.'</td>';
      $htmlp.='</tr>';
  //}
  
}
$htmlp.='</tbody></table>';
$htmlp.='<table align="center"><tr><td>NumTotalMercancias: '.count($facturadetalles).'</td></tr></table>';
$htmlp.='<table align="center"><tr><td><b></b></td></tr></table>';

$htmlp.='<table align="center"><tr><td><b>Remitente</b></td></tr></table>';
$htmlp.='<table align="center">
              <tr>
                <td><b>Nombre</b></td>
                <td><b>RFC</b></td>
                <td><b>Pais</b></td>
                <td><b>Fecha Salida</b></td>
                <td><b>Dirección</b></td>
              </tr>
              <tr>
                <td>'.$GLOBALS["cliente"].'</td>
                <td>'.$GLOBALS["clirfc"].'</td>
                <td>MEX</td>
                <td>'.$fechasalida.'</td>
                <td>'.$direccion_salida.'</td>
              </tr>
          </table>';
$htmlp.='<table align="center"><tr><td><b></b></td></tr></table>';

$htmlp.='<table align="center"><tr><td><b>Destinatario</b></td></tr></table>';
$htmlp.='<table align="center">
            <tr>
              <td width="12%"><b>Almacén</b></td>
              <td width="12%"><b>Distancia</b></td>
              <td width="12%"><b>Fecha de</b></td>
              <td width="12%"><b>Hora de llegada</b></td>
              <td width="52%"><b>Dirección</b></td>
            </tr>';
            foreach ($datosFactura_u_d->result() as $item_d) {
                $htmlp.='<tr>';
                  $htmlp.='<td>ALM0'.$item_d->id_alias.'</td>';
                  $htmlp.='<td>'.$item_d->DistanciaRecorrida.'</td>';
                  $htmlp.='<td>'.date('Y-m-d', strtotime($item_d->FechaHoraSalidaLlegada)).'</td>';
                  $htmlp.='<td>'.date('h:i A', strtotime($item_d->FechaHoraSalidaLlegada)).'</td>';
                  $htmlp.='<td>'.$item_d->domicilio.'</td>';
                $htmlp.='</tr>';
            }
            
$htmlp.='</table>';
$htmlp.='<table align="center"><tr><td><b></b></td></tr></table>';

$htmlp.='<table align="center"><tr><td><b>Operadores</b></td></tr></table>';
$htmlp.='<table align="center">
            <tr>
              <td><b>RFC</b></td>
              <td><b>Nombre</b></td>
              <td><b>Licencia de operador</b></td>
              <td><b>País</b></td>
              <td><b>Dirección</b></td>
            </tr>';
            foreach ($datosFactura_fo->result() as $itemp) {
              $htmlp.='<tr>
              <td>'.$itemp->RFCOperador.'</td>
              <td>'.$itemp->NombreOperador.'</td>
              <td>'.$itemp->NumLicencia.'</td>
              <td></td>
              <td></td>
            </tr>';
            }
            
$htmlp.='</table>';





$pdf->writeHTML($htmlp, true, false, true, false, '');

// Obtener el número total de páginas
$totalPages = $pdf->getNumPages();
//log_message('error','$totalPages: '.$totalPages);
// Establecer el número total de páginas en la instancia de PDF
$pdf->setTotalPages($totalPages);

//$pdf->Output(__DIR__.'Factura_'.$Folio.'.pdf', 'FI');
$url=FCPATH.'/hulesyg/facturaspdf/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'Factura_'.$Folio.'.pdf', 'FI');
if(isset($idfactura)){
  //$pdf->Output('hulesygrapas/hulesyg/facturas/'.$idfactura.'.pdf', 'F');
}
//$pdf->Output($idfactura.'.pdf', 'F');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>