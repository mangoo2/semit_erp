<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $style = array(
      'position' => 'C',
      'align' => 'C',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'hpadding' => 'auto',
      'vpadding' => 'auto',
      'fgcolor' => array(0,0,0),
      'bgcolor' => false, //array(255,255,255),
      'text' => true,
      'font' => 'helvetica',
      'fontsize' => 10,
      'stretchtext' => 4
  );
  foreach ($getproducto as $p){
    $idps=$p->id;
    $codigo = $p->cod_barras;
    $productoid = $p->productoid;
    $sucursalid = $p->sucursalid;
    $lote = $p->lote;
    $caducidad = $p->caducidad;
  }
  if($codigo==""){
    $codigo=$productoid.$sucursalid.intval(preg_replace('/[^0-9]+/', '', $lote), 10).date("Ymd",strtotime($caducidad));
  }
    
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 155), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Semit');
$pdf->SetTitle('Etiquetas Lote');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins('6', '1', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);// margen del footer

$pdf->SetFont('dejavusans', '', 17);
// add a page
$pdf->AddPage('L');

$pdf->write1DBarcode($codigo, 'CODABAR', '', '', '', 35, 0.8, $style, 'N');

$pdf->IncludeJS('print(true);');

$pdf->Output('EtiquetaPorLote.pdf', 'I');
?>