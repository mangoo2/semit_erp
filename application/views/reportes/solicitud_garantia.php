<?php
//require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
//$GLOBALS['garant'] = $garant;
$GLOBALS['det'] = $det;

//=======================================================================================
class MYPDF extends TCPDF {
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,65, '', '', '', false, 330, '', false, false, 0);        
        //$txt="ORDEN DE TRABAJO ".date("d-m-Y H:i A",strtotime($GLOBALS['det'][0]->reg));
        $txt="ORDEN DE TRABAJO ";
        
        $html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="38%">
                    </td>
                    <td width="59%" style="font-size: 22px;color: #012d6a; text-align: right;">
                       Solicitud de garantía
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size: 12px;color: #012d6a; text-align: center;"><br><br>
                        SERVICIOS Y EQUIPOS MÉDICOS INTERNACIONALES DE TOLUCA <br>
                        Calle Av Benito Juárez CP 52140 Estado de México
                    </td>
                </tr>
                <!--<tr>
                    <td width="100%" style="font-size: 14px;color: #012d6a; text-align: center;">
                        <br><br><b><u>'.$txt.'</u></b>
                    </td>
                </tr>-->
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 253, 210, 46, '', '', '', false, 330, '', false, false, 0); 
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Semit');
$pdf->SetTitle('Solicitud de Garantía');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(1,65,1);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(45); //48

// set auto page breaks
$pdf->SetAutoPageBreak(true, 45);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '',9);

$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
        .tableinfo{
            font-size:11px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            text-align:right;
        }
        .httabled{
            font-size:11px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            background-color:#93ba1f;
        }
        .info{
            font-size:12px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            text-align:center;
            font-weight:bold;
        }
    </style>';

    $html.='<table border="0" cellpadding="2" class="tableinfo">
            <!--<tr>
                <th colspan="3" rowspan="6" width="50%"></th>
            </tr>-->
            <tr>
                <th width="25%"></th>
                <th width="25%"><b>Fecha de solicitud:</b></th>
                <th width="50%">'.date("d-m-Y H:i A",strtotime($det[0]->reg)).'</th>
            </tr>
            <tr>
                <th width="25%"></th>
                <th width="25%"><b>Usuario que solicita:</b></th>
                <th width="50%">'.$det[0]->personal.'</th>
            </tr>
            <tr>
                <th width="25%"></th>
                <th width="25%"><b>Sucursal que solicita:</b></th>
                <th width="50%">'.$det[0]->suc_solicita.'</th>
            </tr>
            <tr>
                <th width="25%"></th>
                <th width="25%"><b>Motivo de solicitud:</b></th>
                <th width="50%">'.$det[0]->motivo_all.'</th>
            </tr>
        </table>';

    $html.='<table border="0" cellpadding="2">
            <tr>
              <th width="10%" class="httabled"><b>Id</b></th>
              <th width="60%" class="httabled"><b>Código / Producto</b></th>
              <th width="15%" class="httabled"><b>Cantidad</b></th>
              <th width="15%" class="httabled"><b>Estatus</b></th>
            </tr>';
        foreach ($det as $s){
            $txt="";
            if($s->tipo==2){
                $txt=" Lote: ".$s->lote;
            }
            if($s->tipo==1){
                $txt=" Serie: ".$s->serie;
            }
            if($s->estatus==1 || $s->estatus_garantia==1){
                $estatus="Solicitado";
            }if($s->estatus==2 || $s->estatus_garantia==2){
                $estatus="Con proveedor";
            }if($s->estatus==3 || $s->estatus_garantia==3){
                $estatus="Rechazado";
            }
            if($s->estatus==1 || $s->estatus_garantia==1 || $s->estatus==2 || $s->estatus_garantia==2){
                $color='style="color:green;"';
            }
            if($s->estatus==3 || $s->estatus_garantia==3){
                $color='style="color:red;"';
            }
            $html.='<tr>
                    <td>'.$s->id_garantia.'</td>
                    <td>'.$s->producto.' '.$txt.'</td>
                    <td>'.$s->cant.'</td>
                    <td '.$color.'>'.$estatus.'</td>
                </tr>';  
            if($s->estatus==3){
                $html.='<tr><td style="text-align:center; border-bottom: 1px solid red; border-top: 1px solid red;" colspan="4"><b>Motivo de rechazo:</b> '.$s->motivo_rechazo.'</td></tr>';
            }
            if($s->estatus_garantia==3){
                $html.='<tr><td style="text-align:center; border-bottom: 1px solid red; border-top: 1px solid red;" colspan="4"><b>Motivo de rechazo:</b> '.$s->motivo_rechazo_all.'</td></tr>';
            }
    }
    $html.='</table>';

    $html.='<style type="text/css">
            .table{text-align:center; align-items:center; }
        </style>';

    //log_message('error',' count imagen: '.count($evide));
    $i=1;
    foreach($evide as $e){
        if(count($evide)>1){
            ${'th_imgs_'.$i}= '<th height="170px">
                <img height="240px" width="220px" src="'.FCPATH.'uploads/garantia/'.$e->imagen.'">
            </th>';
        }else{
            ${'th_imgs_'.$i}= '<th colspan="2" height="280px">
                <img height="280px" width="260px" src="'.FCPATH.'uploads/garantia/'.$e->imagen.'">
            </th>';
        }
        $i++;
        //log_message('error','imagen: '.$e->imagen);
        //log_message('error','th_imgs: '.${'th_imgs_'.$i});
    }
    $htmlimg=""; $html_tr2=""; $b=1; 
    for($c=0; $c<count($evide); $c++){
        //log_message('error','b: '.$b);
        if($i==2){ //solo 1 imagen
            $htmlimg.= "<tr>";
            $htmlimg.=${'th_imgs_'.$b};
            $htmlimg.="</tr>"; 
        }else{
            if($b==1 || $b%3 == 0){
                //log_message('error','abre tr: '.$b);
                $htmlimg.= "<tr>";
            }
            if($b%5==0){
                //log_message('error','agre tr %5: '.$b);
                $htmlimg.="<tr>"; 
            }
            $htmlimg.=${'th_imgs_'.$b}; 
            if($b%3==0 && $b==count($evide) || $b%5==0 && $b==count($evide)){
                $htmlimg.="<th></th>"; 
            }
            if($b%2 == 0 || $b%3==0 && $b==count($evide)){
                //log_message('error','cierra tr %2: '.$b);
                $htmlimg.="</tr>";
            }
            if($b%5==0){
                //log_message('error','cierra tr %5: '.$b);
                $htmlimg.="</tr>"; 
            }
        }
        $b++;
    }
    if($i==1){ //sin imagen
        $htmlimg.='<tr><th colspan="2" height="300px"></th></tr>';
    }

    //log_message('error','htmlimg: '.$htmlimg);
    $html.='<table><tr><td height="30px"></td></tr></table>
        <table border="1" class="table" cellpadding="2" align="center" width="100%">
            <thead>
                <tr class="httabled">
                    <th colspan="2"><b>EVIDENCIA FOTOGRÁFICA</b></th>
                </tr>
            </thead>';
        $html.="<tbody>
            ".$htmlimg."
        </tbody>";
    $html.='</table>
    <table border="0" class="table" cellpadding="2" align="center" width="100%">
        <tr>
            <td colspan="2" height="110px" class="foot_det"></td>
        </tr>
        <tr>
            <td colspan="2" class="foot_det"><b>_______________________________________________________________________</b> </td>
        </tr>
        <tr>
            <td colspan="2" class="foot_det"><b>NOMBRE Y FIRMA DEL EMPLEADO</b> </td>
        </tr>
    </table>';

//log_message('error','html: '.$html);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Solicitud_'.$id.'.pdf', 'I');
?>