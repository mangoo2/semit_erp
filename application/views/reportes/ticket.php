<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');    
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 280), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('4', '6', '4');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 6);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();
if($r_v->activo==0){
    //$img_file = base_url().'public/img/ticket_cancelado.png';
    $img_file = base_url().'public/img/ticket_devuelto.png';
    $pdf->Image($img_file, 0, 20, 80, 130, '', '', '', false, 330, '', false, false, 0);
}
$logos = base_url().'public/img/SEMIT.jpg';
$html='<style type="text/css">
              .font8{
                font-size: 8px;
              }
              .font9{
                font-size: 9px;
              }
              .font10{
                font-size: 10px;
              }
              .b_left{
                border-left: 1px solid black;
              }
              .b_right{
                border-right: 1px solid black;
              }
              .b_top{
                border-top: 1px solid black;
              }
              .b_bottom{
                border-bottom: 1px solid black;
              }
              .httablelinea{
                  /*border-bottom: 1px solid #9e9e9e;*/
                  border-bottom-style: dashed;
                }
              .tdtj{text-align: justify;}
              .tdtc{text-align: center;}
              
            </style>';

    $html.='<table border="0" align="center">
                <tr>
                    <td ><img src="'.$logos.'" width="100px"></td>
                </tr>
                <tr>
                    <td class="font9">'.$configticket->titulo.'</td>
                </tr>
                <tr>
                    <td class="font9"><b>RFC:</b>'.$configfac->Rfc.' | <b>TEL:</b>'.$r_suc->tel.'</td>
                </tr>
                <tr>
                    <td class="font9"><b>DOMICILIO:</b>'.$r_suc->domicilio.'</td>
                </tr>';
        $html.='</table>';
        $html.='<table border="0" align="center" cellpadding="4">
                    <tr>
                        <td class="font9 b_top b_bottom"><b>TICKET No. '.$r_v->folio.'</b></td>
                    </tr>';
        $html.='</table><table></table>';
        $html.='<table><tr><td></td></tr></table>';
        
            $razon_social=$r_cli_f_cliente.' '.$r_suc->clave;
        
        $clavecliente=$r_suc->clave.str_pad($idcliente, 5, "0", STR_PAD_LEFT).' | '.$razon_social;
        $html.='<table border="0" align="center">
                    <tr>
                        <td class="font9"><b>Fecha: </b>'.$fecha.' | <b>Hora: </b>'.$hora.'</td>
                    </tr>
                    <tr>
                        <td class="font9"><b>Emitido en: </b>'.$r_suc->clave.' | '.$r_suc->name_suc.'</td>
                    </tr>
                    <tr>
                        <td class="font9"><b>Vendedor: </b>'.$r_per->nombre.'</td>
                    </tr>
                    <tr>
                        <td class="font9"><b>Cliente: '.$clavecliente.'</b></td>
                    </tr>';
        $html.='</table>';
        $html.='<table><tr><td></td></tr></table>';
        $html.='<table border="0" align="center" cellpadding="4">
                    <tr>
                        <td class="font9 b_top b_bottom"><b>Productos y/o servicios</b></td>
                    </tr></table>';
        $html.='<table><tr><td></td></tr></table>';
        $html.='<table border="0" >
                    <tr>
                        <th width="45%" class="font9"><b>Código</b></th>
                        <th width="15%" class="font9"><b>Cant.</b></th>
                        <th width="20%" class="font9"><b>Precio U.</b></th>
                        <th width="20%" class="font9"><b>Importe</b></th>
                    </tr>
                    <tr>
                        <th class="font9" colspan="4"><b>Descripción</b></th>
                    </tr>';
        foreach ($r_vd->result() as $item) {
            if($item->tipo!=2 && $item->tipo!=3){
                $importe=$item->cantidad*$item->precio_unitario;
                if($item->tipo==0){
                    $nombre=$item->idProducto." / ".$item->nombre;
                    if($item->tipo_prod==1){
                        $nombre = $item->idProducto." / ".$item->nombre."<br>SERIE:".$item->serie;
                    }if($item->tipo_prod==2){
                        $nombre = $item->idProducto." / ".$item->nombre."<br>LOTE: ".$item->lote;
                    }  
                }if($item->tipo==1){ //recargas de oxigeno
                    $nombre=$item->codigo." / "."Recarga de clindro de oxígeno de ".$item->capacidad." L";
                }
                $html.='<tr>
                            <td class="font8">'.$nombre.'</td>
                            <td class="font8" style="text-align:center;">'.$item->cantidad.'</td>
                            <td class="font8">$ '.number_format($item->precio_unitario,2,'.',',').'</td>
                            <td class="font8">$ '.number_format($importe,2,'.',',').'</td>
                        </tr>
                        <tr>
                            <td class="font10 httablelinea" colspan="4"></td>
                        </tr>';
            }//cierra if de solo prods
        }
        foreach ($s_vd->result() as $item) {
            $importe=$item->cantidad*$item->precio_unitario;
            $html.='<tr>
                        <td class="font8">'.$item->numero.' / '.$item->descripcion.'</td>
                        <td class="font8" style="text-align:center;">'.$item->cantidad.'</td>
                        <td class="font8">$ '.number_format($item->precio_unitario,2,'.',',').'</td>
                        <td class="font8">$ '.number_format($importe,2,'.',',').'</td>
                    </tr>
                    <tr>
                        <td class="font10 httablelinea" colspan="4"></td>
                    </tr>';
        }
        foreach ($renta_vd->result() as $r) {
            $importe=$r->cantidad*$r->precio_unitario;
            $precio_unitario = $r->precio_unitario / 1.16;
            $importe = $importe / 1.16;
            $txt_periodo="";
            if($r->periodo==1){
                $txt_periodo="(1 día)";
            }if($r->periodo==2){
                $txt_periodo="(15 días)";
            }if($r->periodo==3){
                $txt_periodo="(30 días)";
            }

            $desc = $r->clave." / ".$r->descripcion." ".$txt_periodo." Serie: ".$r->serie;
            $html.='<tr>
                        <td class="font8">'.$desc.'</td>
                        <td class="font8" style="text-align:center;">'.$r->cantidad.'</td>
                        <td class="font8">$ '.number_format($precio_unitario,2,'.',',').'</td>
                        <td class="font8">$ '.number_format($importe,2,'.',',').'</td>
                    </tr>
                    <tr>
                        <td class="font10 httablelinea" colspan="4"></td>
                    </tr>';
        }
        $html.='</table>';
        $html.='<table><tr><td></td></tr></table>';
        $html.='<table border="0" align="right">
                    <tr>
                        <td class="font9"><b>SUBTOTAL: $ '.number_format($r_v->subtotal,2,'.',',').'</b></td>
                    </tr>';
                    if($r_v->descuento!='0.00'){
                        $html.='<tr>
                            <td r_v="font9"><b>DESCUENTO: $ '.number_format($r_v->descuento,2,'.',',').'</b></td>
                        </tr>'; 
                    }
                    $html.='<tr>
                        <td class="font9"><b>IVA: $ '.number_format($r_v->iva,2,'.',',').'</b></td>
                    </tr>
                    <tr>
                        <td class="font9"><b>TOTAL: $ '.number_format($r_v->total,2,'.',',').'</b></td>
                    </tr>
                </table>';
        $html.='<table><tr><td></td></tr></table>';
                $caract   = array('pesos');
                $caract2  = array(''); 

        $html.='<table><tr><td class="font9">('.strtoupper(str_replace($caract, $caract2, $total_letra)).')</td></tr></table>';

        $html.='<table border="0" align="center" cellpadding="4">
                    <tr>
                        <td class="font9 b_top b_bottom"><b>Formas de pago</b></td>
                    </tr></table>';
        $c_o_fp   = array('04 ','28 ', '01 ');
        $c_r_fp  = array('','',''); 
        $montog=0;
        foreach ($r_vfp->result() as $item) {
            $montog=$montog+$item->monto;

            $tipo_venta='';
            if($r_v->tipo_venta==1){
                $tipo_venta='Crédito';
            }else{
                $tipo_venta=str_replace($c_o_fp,$c_r_fp,$item->formapago_text);
            }
            $html.='<table border="0" >
                    <tr>
                        <td class="font9" width="70%"><b>Formas de pago: '.$tipo_venta.'</b></td>
                        <td class="font9" width="30%"><b>$ '.number_format($item->monto,2,'.',',').'</b></td>
                    </tr>';
                    if($item->clave=='04' || $item->clave=='28'){
                        $html.='<tr><td class="font9" colspan="2">Últimos 4 dígitos '.$item->ultimosdigitos.'</td></tr>';
                    }

            $html.='</table>';
        }
        
        $cambio=$montog-$r_v->total;
        $html.='<table><tr><td class="font9"><b>Cambio: </b>'.number_format($cambio,2,'.',',').'</td></tr></table>';
        $html.='<table border="0" >
                    <tr>
                        <td class="font9 b_top tdtc"><b>COMENTARIOS</b></td>
                    </tr>
                    <tr>
                        <td class="font8 tdtj">'.$r_v->observaciones.'</td>
                    </tr>
                </table>';
        $html.='<table border="0" align="center" cellpadding="4">
                    <tr>
                        <td class="font9 b_top"><b>FACTURACIÓN ELECTRÓNICA</b></td>
                    </tr>
                    <tr>
                        <td class="font9 ">Si usted requiere factura, pueda ingresar y realizarla a través del siguiente enlace <b>https://facturacion.semit.mx</b></td>
                    </tr>
                </table>';
        $html.='<table border="0" >
                    <tr>
                        <td class="font9 b_top tdtc"><b>OBSERVACIONES</b></td>
                    </tr>
                    <tr>
                        <td class="font8 tdtj">'.$configticket->mensaje.'</td>
                    </tr>
                </table>';
        $html.='<table><tr><td></td></tr></table>';
        $html.='<table><tr><td class="font8">RECIBO MERCANCIA E INDICACIONES DEL USO DEL PRODUCTO</td></tr></table>';
        $html.='<table><tr><td></td></tr></table>';
        $html.='<table border="0" align="center" cellpadding="4">
                    <tr>
                        <td class="font9 b_top"><b>FIRMA DEL CLIENTE</b></td>
                    </tr>
                    <tr>
                        <td class="font9 "><b>¡GRACIAS POR SU COMPRA!</b></td>
                    </tr>
                </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->IncludeJS('print();');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>