<?php

require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['det'] = $det;
$GLOBALS['id'] = $id;
$GLOBALS['id_ord'] = $det->id;

//=======================================================================================
class MYPDF extends TCPDF {
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,45, '', '', '', false, 330, '', false, false, 0);
        $txt="ORDEN DE TRABAJO ";
        
        $html='<table border="0"><tr>
                <td width="70%">
                </td>
                <td>
                    <img src="'.base_url().'public/img/SEMIT.jpg">
                </td>
            </tr>
            <!--<tr>
                <td width="38%">
                </td>
                <td width="59%" style="font-size: 22px;color: #012d6a; text-align: right;">
                   Solicitud de garantía
                </td>
            </tr>-->
            <tr>
                <td width="100%" style="font-size: 12px;color: #012d6a; text-align: center;"><br><br>
                    SERVICIOS Y EQUIPOS MÉDICOS INTERNACIONALES DE TOLUCA <br>
                    Calle Av Benito Juárez CP 52140 Estado de México
                </td>
            </tr>
            <tr>
                <td width="100%" style="font-size: 20px;color: #012d6a; text-align: right;">
                    <b>'.$txt.'</b>
                </td>
            </tr>
        </table>
        <style type="text/css">
            .tableinfo{
                font-size:11px;
                border: 1px solid #93ba1f;
                color:black;
                text-align:right;
            }
        </style>
        <table border="0" cellpadding="2">
            <tr>
                <td width="70%"></td>
                <td width="30%" class="tableinfo" style="font-size: 20px;color: red; text-align: center;">
                    <b>MTTO-INT 000'.$GLOBALS["id_ord"].'</b>
                </td>
            </tr>
        </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Semit');
$pdf->SetTitle('Orden de Trabajo Interno');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(4,65,4);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(40);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 41);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '',9);

$pdf->AddPage('P', 'A4');

    $html='<style type="text/css">
        .tableinfo{
            font-size:11px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            text-align:left;
        }
        .httabled{
            font-size:11px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            background-color:#93ba1f;
        }
        .info{
            font-size:12px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            color:black;
            text-align:center;
            font-weight:bold;
        }
        .tableinfo th{
            font-weight:bold;
        }
        .table_det{
            border: 1px solid #93ba1f;
            text-align:left;
        }
        .th_det{
            font-size:11px;
            text-align:center;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            border-left: 1px solid #93ba1f;
            color:black;
            background-color:#93ba1f;
        }
        .td_det{
            font-size:11px;
            border-bottom: 1px solid #93ba1f;
            border-top: 1px solid #93ba1f;
            border-left: 1px solid #93ba1f;
            color:black;
        }
        .foot_det{
            font-size:11px;
            text-align:center;
        }
    </style>';

    $html.='<table border="0" cellpadding="2" class="tableinfo">
            <tr>
                <th width="20%">Fecha:</th>
                <td width="30%">'.date("d-m-Y",strtotime($det->fecha)).'</td>
                <th width="20%">Sucursal:</th>
                <td width="30%">'.$det->suc_solicita.'</td>
            </tr>
            <tr>
                <th width="20%">Nombre:</th>
                <td width="80%">'.$det->suc_solicita.'</td>
            </tr>
            <tr>
                <th width="20%">Dirección:</th>
                <td width="80%">'.$det->domicilio.'</td>
            </tr>
            <!--<tr>
                <th width="20%">Celular:</th>
                <td width="30%"></td>
                <th width="20%">E-mail:</th>
                <td width="30%"></td>
            </tr>-->
            <tr>
                <th width="20%">Teléfono:</th>
                <td width="30%">'.$det->tel.'</td>
                <th width="20%">RFC:</th>
                <td width="30%"></td>
            </tr>
        </table>';

    $html.='<table border="0" cellpadding="5" class="table_det">
        <thead>
            <tr>
                <th width="10%" class="th_det"><b>Cantidad</b></th>
                <th width="90%" class="th_det"><b>Descripción</b></th>
            </tr>
        </thead>
        <tbody>';

        $txt = '<p>Serie: '.$det->serie.'</p>';
        $html.='<tr>
                <td style="text-align:center" width="10%" class="td_det">1</td>
                <td width="90%" class="td_det">'.$det->observ_mtto.' '.$txt.'</td>
            </tr>';  
    
    $html.='<tr>
            <td colspan="3" height="150px" class="td_det"><br><b>Observaciones:</b> 
            <br>'.$det->observ.'</td>
        </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" height="80px" class="foot_det"><b>SE DEVUELVE EQUIPO PARA REVISIÓN E INSPECCIÓN, PERMITIENDO A SERVICIOS Y EQUIPOS MÉDICOS INTERNACIONALES DE TOLUCA S.A. DE C.V. TOMAR LAS MEDIDAS PERTINENTES, SUJETOS A UN COBRO EXTRA POR DAÑOS PARCIALES O TOTALES DEL EQUIPO</b> </td>
            </tr>
            <tr>
                <td colspan="3" class="foot_det"><b>_______________________________________________________________________</b> </td>
            </tr>
            <tr>
                <td colspan="3" class="foot_det"><b>NOMBRE Y FIRMA</b> </td>
            </tr>
        </tfoot>
    </table>
    <table width="100%" border="0" cellpadding="1" class="tableinfo" align="left">
        <tr>
            <td colspan="3">ATENTIDO POR: <b>'.$det->personal.'</b></td>
        </tr>
    </table>';

    $html.='<table width="100%" border="0" cellpadding="3" class="tableinfo" align="center">
        <tr>
            <td><b>Pino Suárez </b></td>
            <td><b>Alfredo del Mazo</b></td>
            <td><b>Jesús Carranza</b></td>
            <td><b>Metepec</b></td>
        </tr>
        <tr>
            <td>José Maria Pino Suarez, No. 722, Col. Cuauhtémoc Toluca, Edo. de México</td>
            <td>Boulevard Alfredo del Mazo No. 727 local 3, Plaza El Punto. Col. Científicos.</td>
            <td>Jesús Carranza Sur, No. 323, (entre las Torres y Tollocan) Toluca México</td>
            <td>Av. Benito Juárez, No. 528, Barrio San Mateo (a 300 m del Centro Médico Toluca)</td>
        </tr>
        <tr>
            <td>TEL (722) 454 22 23</td>
            <td>TEL (722) 454 02 12</td>
            <td>TEL (722) 454 02 16</td>
            <td>TEL (722) 454 02 19</td>
        </tr>
    </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('orden_trabajo_'.$id.'.pdf', 'I');
?>