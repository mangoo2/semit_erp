

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">    
        <div class="card card-custom gutter-b" style="margin-top: 10px;"> 
            <div class="row">
                <div class="col-lg-2">
                    <label>Sucursal</label>
                    <select class="form-control" id="id_suc">
                        <?php
                            $op='<option value="0" data-clave="x">Seleccionar Sucursal</option>';
                            foreach ($sucus as $s) {
                                $op.='<option value="'.$s->id.'" data-clave="'.$s->clave.'">'.$s->id_alias.' '.$s->name_suc.'</option>';
                            }
                            echo $op;
                        ?>
                    </select>
                </div>
                <div class="col-lg-2">
                    <label>Fecha inicio</label>
                    <input class="form-control" type="date" id="fechai">
                </div>
                <div class="col-lg-2">
                    <label>Fecha fin</label>
                    <input class="form-control" type="date" id="fechaf" value="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="col-lg-3">
                    <label></label>
                    <div class="col-lg-12">
                        <button title="Exportar registros de comisiones" id="btn_export" type="button" class="btn btn-danger">Exportar resultados <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                        <button title="Exportar registros de comisiones" id="btn_export_excel" type="button" class="btn btn-success">Exportar tablas <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    </div>
                </div>  
            </div>
        </div>  
        <div class="card card-custom gutter-b" style="margin-top: 10px;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-body">
                        <div class="">
                            <div class="col-md-12" style="text-align: center;">
                                <h3 style="color: #1770aa;">Ventas de sucursal(es)</h3>
                            </div>
                            <div class="col-md-12 info_ventas"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                        <div class="card-body">
                            <h3 style="color: #1770aa; text-align: center;">Gráfica de Ventas</h3>
                            <div id="basic-apex2"></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

        <div class="card card-custom gutter-b" style="margin-top: 10px;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-body">
                        <div class="">
                            <div class="col-md-12" style="text-align: center;">
                                <h3 style="color: #1770aa;">Utilidad en productos</h3>
                            </div>
                            <div class="col-md-12 info_utilidad"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                        <div class="card-body">
                            <h3 style="color: #1770aa; text-align: center;">Gráfica de utilidad</h3>
                            <div id="graph_utilidad"></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
