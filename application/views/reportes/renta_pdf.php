<?php
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['id']=$det->id;
$GLOBALS['reg']=$det->fecha_reg;
$GLOBALS['cliente']=$det->nombre;
$GLOBALS['fecha_inicio']=$det->fecha_inicio;
$GLOBALS['hora_inicio']=$det->hora_inicio;
$GLOBALS['fecha_fin']=$det->fecha_fin;
$GLOBALS['hora_fin']=$det->hora_fin;
$GLOBALS['name_suc']=$det->name_suc;
$GLOBALS['equipo']=$det->equipo;
$GLOBALS['serie']=$det->serie;
$GLOBALS["txt_ren"]="";
if($det->id_renta_padre>0){
    $GLOBALS["txt_ren"]='<tr>
            <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Renovación
            </td>
        </tr>';
}
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image

        $img_file = base_url().'public/img/contrato_img2.jpg';  
        //$height = $this->getPageHeight();
        //$width = $this->getPageWidth();
        //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        
        //$this->Image($img_file, 0,0, '', 275, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);
        /*$html='<table border="0"><tr>
                    <td width="70%">
                    </td>
                    <td>
                        <img src="'.base_url().'public/img/SEMIT.jpg">
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:18px;color: #012d6a; text-align: right;">Renta de equipo
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Folio: '.$GLOBALS['id'].'
                    </td>
                </tr>
                '.$GLOBALS['txt_ren'].'
                <tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha de creación: '.$GLOBALS['reg'].'
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Cliente: '.$GLOBALS['cliente'].'
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha inicio: '.date("d-m-Y ",strtotime($GLOBALS['fecha_inicio'])).' '.date("H:i a ",strtotime($GLOBALS['hora_inicio'])).'
                    </td>
                </tr><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha fin: '.date("d-m-Y ",strtotime($GLOBALS['fecha_fin'])).' '.date("H:i a ",strtotime($GLOBALS['hora_fin'])).'
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Sucursal: '.$GLOBALS['name_suc'].'
                    </td>
                </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');*/

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
    // Page footer
    public function Footer() {
        /*$img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 

        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', '', 8);
        // Custom footer HTML
        $this->html = '<table border="0"><tr>
                    <td width="100%" style="font-size:12px;color: #012d6a; text-align: center;"><b>
                      Contrato de renta de equipo</b>
                    </td>
                </tr>
              </table>';
        $this->writeHTML($this->html, true, false, true, false, '');*/
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Semit');
$pdf->SetTitle('Renta de Equipo');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(0,0,0);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(0);

// set auto page breaks
$pdf->SetAutoPageBreak(false);

$pdf->SetFont('dejavusans', '',10);
// add a page
$pdf->SetMargins(0, 0, 0);
$pdf->AddPage('P', 'A4');
$html="";
/*$html='<style type="text/css">
    .httabled{
        font-size:12px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }

    .td_l{
        font-size:11px;
    }

  </style>';

if($det->deja_docs==0){
    $deja_docs="No";
}else{
    $deja_docs="Si";
}
$html.='<table border="0" cellpadding="2"><tr>
      <th width="2%" class="httabled"><b></b></th>
      <th width="38%" class="httabled"><b>Servicio</b></th>
      <th width="30%" class="httabled"><b>Serie</b></th>
      <th width="12%" class="httabled"><b>Precio</b></th>
      <th width="8%" class="httabled"><b>Deja Docs.</b></th>
      <th width="10%" class="httabled"><b>Deposito</b></th>
    </tr>
    <tr>
        <td></td>
        <td>'.$det->descripcion.'</td>
        <td>'.$det->serie.'</td>
        <td>$'.number_format($det->costo,2,".",",").'</td>
        <td>'.$deja_docs.'</td>
        <td>$'.number_format($det->deposito,2,".",",").'</td>
    </tr>';

$html.='</table>';*/

$pdf->SetXY(36, 43);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 7);

$pdf->Cell(100, 50, $GLOBALS['cliente'], 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(15, 51);
/*$pdf->Cell(100, 50, $GLOBALS['equipo'].", con número de serie ".$GLOBALS['serie'], 0, 0, 'L', 0, '', 0, false, 'B', 'B');*/
$pdf->Cell(100, 50, $GLOBALS['equipo'], 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(38, 125);
$pdf->Cell(100, 50, $det->costo_entrega, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(43, 142);
$pdf->Cell(100, 50, $det->costo_recolecta, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(39, 146);
$pdf->Cell(100, 50, date("d-m-Y", strtotime($det->fecha_inicio)), 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(132, 146);
$pdf->Cell(100, 50, date("d-m-Y", strtotime($det->fecha_fin)), 0, 0, 'L', 0, '', 0, false, 'B', 'B');

//datos del cliente
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetXY(50, 188);
$pdf->Cell(100, 50, $det->nombre, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(26, 193);
$pdf->Cell(100, 50, $det->calle, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(134, 193);
$pdf->Cell(100, 50, $det->colonia, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(23, 199);
$pdf->Cell(100, 50, $det->cp, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(104, 199);
$pdf->Cell(100, 50, $det->municipio, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(150, 209);
$pdf->Cell(100, 50, $det->serie, 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(154, 214);
$pdf->Cell(100, 50, number_format($det->costo_entrega,2,".",","), 0, 0, 'L', 0, '', 0, false, 'B', 'B');
$pdf->SetXY(161, 219);
$pdf->Cell(100, 50, number_format($det->costo_recolecta,2,".",","), 0, 0, 'L', 0, '', 0, false, 'B', 'B');

//////////////////////Datos del pagaré////////////////////////
$pdf->SetXY(18, 250);
$pdf->Cell(100, 50, $det->en_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(42, 250);
$pdf->Cell(100, 50, $det->el_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(113, 254);
$pdf->Cell(100, 50, "1", 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(73, 250);
$pdf->Cell(100, 50, number_format($det->cant_pagare,2,".",","), 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(44, 262);
$pdf->Cell(100, 50, $det->porc_interes, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(30, 267);
$pdf->Cell(100, 50, $det->nom_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(110, 267);
$pdf->Cell(100, 50, $det->dir_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(30, 272);
$pdf->Cell(100, 50, $det->pobla_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');

$pdf->SetXY(110, 272);
$pdf->Cell(100, 50, $det->tel_pagare, 0, 0, 'L', 0, '', 0, false, 'B', 'B');


//$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('renta_'.$id.'.pdf', 'I');
?>