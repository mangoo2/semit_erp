<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
//===============================
  $GLOBALS["logotipo"]=$logotipo;

  $GLOBALS["Folio"]=$Folio;
  $GLOBALS["Nombrerasonsocial"]=$Nombrerasonsocial;
  $GLOBALS["folio_fiscal"]=$folio_fiscal;
  $GLOBALS["rrfc"]=$rrfc;
  $GLOBALS["rdireccion"]=$rdireccion;
  $GLOBALS["nocertificadosat"]=$nocertificadosat;
  $GLOBALS['regimenf']=$regimenf;
  $GLOBALS["certificado"]=$certificado;
  $GLOBALS["cfdi"]=$cfdi;
  $GLOBALS["fechatimbre"]=$fechatimbre;
  $GLOBALS["Estado"]=1;//en dado caso de que se encuentre cancelado
  $GLOBALS["cp"]=$cp;
  $GLOBALS["cliente"]=$clienteId.' / '.$cliente;
  $GLOBALS["clirfc"]=$clirfc;
  $GLOBALS["clidireccion"]=$clidireccion.' C.P. '.$cp;
  $GLOBALS["isr"]=$isr;
  $GLOBALS['numordencompra']=$numordencompra;
  $GLOBALS["ivaretenido"]=$ivaretenido;
  $GLOBALS["cedular"]=$cedular;
  $GLOBALS["cincoalmillarval"]=$cincoalmillarval;
  $GLOBALS["outsourcing"]=$outsourcing;

  $GLOBALS["f_relacion"]=$f_relacion;
  $GLOBALS["f_r_tipo"]=$f_r_tipo;
  $GLOBALS["f_r_uuid"]=$f_r_uuid;

  $GLOBALS["ticketmensaje"]=$configticket->mensaje;


  if ($numproveedor!='') {
      $GLOBALS['numproveedor']=$numproveedor;
      $GLOBALS['numproveedorv']='block';
  }else{
      $GLOBALS['numproveedor']='';
      $GLOBALS['numproveedorv']='none';
  }

  if ($Caducidad!='') {
      $GLOBALS['Caducidad']=$Caducidad;
      $GLOBALS['Caducidadv']='block';
  }else{
      $GLOBALS['Caducidad']='';
      $GLOBALS['Caducidadv']='none';
  }

  if ($Lote!='') {
      $GLOBALS['Lote']=$Lote;
      $GLOBALS['Lotev']='block';
  }else{
      $GLOBALS['Lote']='';
      $GLOBALS['Lotev']='none';
  }

  if ($numordencompra!='') {
      $GLOBALS['numordencompra']=$numordencompra;
      $GLOBALS['numordencomprav']='block';
  }else{
      $GLOBALS['numordencompra']='';
      $GLOBALS['numordencomprav']='none';
  }
  $GLOBALS['observaciones']=$observaciones;
  $GLOBALS["total"]=$total;
  $GLOBALS["moneda"]=$moneda;
  if ($moneda=='MXN') {
    $GLOBALS['abreviaturamoneda']='MXN';
  }else{
    $GLOBALS['abreviaturamoneda']='USD';
  }
  $GLOBALS["subtotal"]=$subtotal;
  $GLOBALS["iva"]=$iva;
  $GLOBALS["tarjeta"]=$tarjeta;
  $GLOBALS["selloemisor"]=$selloemisor;
  $GLOBALS["sellosat"]=$sellosat;
  $GLOBALS["cadenaoriginal"]=$cadenaoriginal;
  if ($cadenaoriginal=='') {
    $serie='';
  }else{
    $series=explode("|", $cadenaoriginal);
    $serie=$series[3];
  }
  $GLOBALS["serie"]=$serie;

  $GLOBALS["FormaPago"]=$FormaPago;
  $GLOBALS["FormaPagol"]=$FormaPagol;
  $GLOBALS["MetodoPago"]=$MetodoPago;
  $GLOBALS["MetodoPagol"] =$MetodoPagol; 
  $GLOBALS["tipoComprobante"]=$tipoComprobante;
  $GLOBALS["facturadetalles"]=$facturadetalles;
  $GLOBALS["RegimenFiscalReceptor"]=$RegimenFiscalReceptor;
  $GLOBALS["LugarExpedicion"]=$LugarExpedicion;
  $GLOBALS["LugarExpedicion_suc"]=$LugarExpedicion_suc;
  //======================================================
  $sucu=$sucu->result();
  $GLOBALS["sucu_name1"]=$sucu[0]->name_suc;
  $GLOBALS["sucu_name2"]=$sucu[1]->name_suc;
  $GLOBALS["sucu_name3"]=$sucu[2]->name_suc;
  $GLOBALS["sucu_name4"]=$sucu[3]->name_suc;

  $GLOBALS["sucu_dom1"]=$sucu[0]->domicilio;
  $GLOBALS["sucu_dom2"]=$sucu[1]->domicilio;
  $GLOBALS["sucu_dom3"]=$sucu[2]->domicilio;
  $GLOBALS["sucu_dom4"]=$sucu[3]->domicilio;

  $GLOBALS["sucu_tel1"]=$sucu[0]->tel;
  $GLOBALS["sucu_tel2"]=$sucu[1]->tel;
  $GLOBALS["sucu_tel3"]=$sucu[2]->tel;
  $GLOBALS["sucu_tel4"]=$sucu[3]->tel;

  $GLOBALS['nom_personal']=$nom_personal;
  $GLOBALS['ticket']=$ticket;
  $t_descuento=0;
  foreach ($facturadetalles as $item) {
    if($item->descuento>0){
      $t_descuento=1;
    }
  }
  $GLOBALS['t_descuento']=$t_descuento;
  //====================================================
//====================================================
class MYPDF extends TCPDF {
  //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='MXN') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            if($t == $this->Void) 
            { 
              $i = floatval($i) + 1; 
              $x = $x / 1000; 
              if($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
  //===========================================================
      private $totalPages; // Variable para almacenar el número total de páginas
      // Constructor
      public function __construct() {
          parent::__construct();
          $this->totalPages = 0; // Inicializar la variable
      }
      public function setTotalPages($totalPages) {
        $this->totalPages = $totalPages;
      }
  //===========================================================
  //Page header
  public function Header() {
      $logos = $GLOBALS["logotipo"];
      $logos2 = $GLOBALS["logotipo"];
      
      if ($GLOBALS["Estado"]==0) {
        $cancelado='<br><span  style="color:red; font-size:20px"><b>Cancelado</b></span>';
      }else{
        $cancelado='';
      }
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '
          <style type="text/css">
            .info_fac{font-size: 9px;}
            .info_facd{font-size: 8px;}
            .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
            .httableleft{border-left: 1px solid #9e9e9e;}
            .httableright{border-right: 1px solid #9e9e9e;}
            .httabletop{border-top: 1px solid #9e9e9e;}
          </style>
          <style type="text/css">
        .httable{
          vertical-align: center;
          border-bottom: 2px solid #9e9e9e;
          border-top: 1px solid #9e9e9e;

        }
        .httablepro{
            vertical-align: center;
            border-bottom: 1px solid #9e9e9e;
          }
        .httborderbottom{
            border-bottom: 1px solid #9e9e9e;
          }
        .httborderleft{
            border-left: 1px solid #9e9e9e;
          }
        .httborderright{
            border-right: 1px solid #9e9e9e;
          }
        .httbordertop{
            border-top: 1px solid #9e9e9e;
          }
        .magintablepro{
            margin-top:0px;
            margin-bottom:0px;
            margin: 0px;
        }
      </style>';
          $html.='<table width="100%" border="0" cellpadding="4px" class="info_fac">';
            $html.='<tr>';
              $html.='<td width="24%"><img src="'.$logos.'"></td>';
              $html.='<td width="38%" >';
                $html.='<table border="0" class="httborderbottom httborderleft httborderright httbordertop">';
                  $html.='<tr>';
                    $html.='<td><b>Serie '.$GLOBALS["serie"].'</b></td>';
                    $html.='<td><b>Factura No. '.str_pad($GLOBALS["Folio"], 4, "0", STR_PAD_LEFT).'</b></td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2">Nombre o razon social '.$GLOBALS["Nombrerasonsocial"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2"><b>RFC: '.$GLOBALS["rrfc"].'</b></td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2">'.$GLOBALS["rdireccion"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2" class="httborderbottom"><b>Régimen fiscal:</b> '.$GLOBALS['regimenf'].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2" class="httborderbottom"><b>Tipo de comprobante: </b>'.$GLOBALS["tipoComprobante"].'</td>';
                  $html.='</tr>';
                $html.='</table>';
              $html.='</td>';
              $html.='<td width="38%">';
                $html.='<table border="0">';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom">Folio Fiscal</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom">'.$GLOBALS["folio_fiscal"].' '.$cancelado.'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom" >NO. DE SERIE DEL CERTIFICADO DEL SAT</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom" >'.$GLOBALS["nocertificadosat"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom" >NO. DE SERIE DEL CERTIFICADO EMISOR</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom" >'.$GLOBALS["certificado"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom">FECHA Y HORA DE CERTIFICACION</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td align="center" class="httborderbottom">'.$GLOBALS["fechatimbre"].'</td>';
                  $html.='</tr>';
                $html.='</table>';
              $html.='</td>';
            $html.='</tr>';
          $html.='</table>';
            $html.='<table width="100%" border="0" cellpadding="3px" class="info_fac httborderbottom httborderleft httborderright httbordertop" align="center">';
              $html.='<tr>';
                $html.='<td><b>'.$GLOBALS["sucu_name1"].'</b></td>';
                $html.='<td><b>'.$GLOBALS["sucu_name2"].'</b></td>';
                $html.='<td><b>'.$GLOBALS["sucu_name3"].'</b></td>';
                $html.='<td><b>'.$GLOBALS["sucu_name4"].'</b></td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td>'.$GLOBALS["sucu_dom1"].'</td>';
                $html.='<td>'.$GLOBALS["sucu_dom2"].'</td>';
                $html.='<td>'.$GLOBALS["sucu_dom3"].'</td>';
                $html.='<td>'.$GLOBALS["sucu_dom4"].'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td>TEL '.$GLOBALS["sucu_tel1"].'</td>';
                $html.='<td>TEL '.$GLOBALS["sucu_tel2"].'</td>';
                $html.='<td>TEL '.$GLOBALS["sucu_tel3"].'</td>';
                $html.='<td>TEL '.$GLOBALS["sucu_tel4"].'</td>';
              $html.='</tr>';
            $html.='</table>';
            $html.='<table class="info_fac">';
              $html.='<tr>';
                          $html.='<td><b>Lugar de Expedición :</b>'.$GLOBALS["LugarExpedicion_suc"].'</td>';
                          $html.='<td><b>Vendedor :</b>'.$GLOBALS['nom_personal'].'</td>';
                          if($GLOBALS['ticket']!=''){
                            $html.='<td><b>Ticket :</b>'.$GLOBALS['ticket'].'</td>';
                          }
              $html.='</tr>';
            $html.='</table>';
            $html.='<table width="100%" border="0" cellpadding="4px" class="info_fac">';
              $html.='<tr >';
                $html.='<td class="httborderbottom" style="width:15%"><b>NOMBRE :</b> </td>';
                $html.='<td class="httborderbottom" style="width:35%">'.$GLOBALS["cliente"].'</td>';
                $html.='<td class="httborderbottom" style="width:15%"><b>R.F.C:</b></td>';
                $html.='<td class="httborderbottom" style="width:35%">'.$GLOBALS["clirfc"].'</td>';
              $html.='</tr>';
              $html.='<tr >';
                $html.='<td class="httborderbottom"><b>DIRECCIÓN : </b></td>';
                $html.='<td class="httborderbottom">'.$GLOBALS["clidireccion"].'</td>';
                $html.='<td class="httborderbottom"><b>RÉGIMEN FISCAL: </b></td>';
                $html.='<td class="httborderbottom">'.$GLOBALS["RegimenFiscalReceptor"].'</td>';
              $html.='</tr>';
              $html.='<tr >';
                $html.='<td class="httborderbottom"><b>USO DE CFDI: </b></td>';
                $html.='<td class="httborderbottom">'.$GLOBALS["cfdi"].'</td>';
                $html.='<td class="httborderbottom"></td>';
                $html.='<td class="httborderbottom"></td>';
              $html.='</tr>';
              $html.='<tr >';
                $html.='<td class="httborderbottom"><b>COMENTARIOS: </b></td>';
                $html.='<td class="httborderbottom">'.$GLOBALS['observaciones'].'</td>';
                $html.='<td class="httborderbottom"></td>';
                $html.='<td class="httborderbottom"></td>';
              $html.='</tr>';
            $html.='</table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    log_message('error', 'getPage: '.$this->getPage().' $this->totalPages:'.$this->totalPages); 
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $urlfolio_fe=substr($GLOBALS["selloemisor"], -8); 
          $urlfolio='https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?&id='.$GLOBALS["folio_fiscal"].'&re='.$GLOBALS['rrfc'].'&rr='.$GLOBALS["clirfc"].'&tt='.$GLOBALS["total"].'&fe='.$urlfolio_fe;
          $params = $this->serializeTCPDFtagParameters(array($urlfolio, 'QRCODE,L', '', '', 45, 45, $styleQR, 'N'));

    $html2 .= ' 
          <style type="text/css">
              .fontFooter10{font-size: 10px;}
              .fontFooterp{font-size: 9px;margin-top:0px;}
              .fontFooter{font-size: 9px;margin-top:0px;}
              .fontFooterp{font-size: 8px;}
              .fontFooterpt{font-size: 7px;}
              .fontFooterpt6{font-size: 6px;}
              p{margin:0px;}
              .valign{vertical-align:middle;}
              .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
              .footerpage{font-size: 9px;color: #9e9e9e;}
          </style>';





    if ($this->getPage() == $this->totalPages) {
      //$this->SetFooterMargin('120');

    // set auto page breaks
    //$this->SetAutoPageBreak(true, 120);//PDF_MARGIN_BOTTOM
    
          $html2 .= '<table width="100%" border="0" class="fontFooter10">
                      <!--<tr >
                        <td cellpadding="2" style="display:'.$GLOBALS['numproveedorv'].'"><b>Numero de Proveedor:</b> '.$GLOBALS['numproveedor'].'</td>
                        <td cellpadding="2"style="display:'.$GLOBALS['numordencomprav'].'"><b>Numero de orden de compra:</b> '.$GLOBALS['numordencompra'].'</td>
                      </tr>
                      <tr >
                        <td cellpadding="2" style="display:'.$GLOBALS['Caducidadv'].'"><b>Caducidad:</b> '.$GLOBALS['Caducidad'].'</td>
                        <td cellpadding="2"style="display:'.$GLOBALS['Lotev'].'"><b>Lote:</b> '.$GLOBALS['Lote'].'</td>
                      </tr>-->
                      ';
            /*if ($GLOBALS['observaciones']!='') {
              $html2 .= '<tr>
                          <td cellpadding="4"><b>Observaciones:</b> '.$GLOBALS['observaciones'].'</td>
                        </tr>';
            }*/
            $descuentog=0;
            foreach ($GLOBALS["facturadetalles"] as $item) {
              $descuentog=$descuentog+$item->descuento;
            }
            $rowspan=6;
            if ($GLOBALS["isr"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["ivaretenido"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["cincoalmillarval"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["outsourcing"]>0) {
              $rowspan=$rowspan+1;
            }
  $html2 .= '<tr>
              <td  width="48%" rowspan="'.$rowspan.'" valign="top"><b>CANTIDAD EN LETRA::</b> '.($this->ValorEnLetras($GLOBALS["total"],$GLOBALS["moneda"]));
              if($GLOBALS['f_relacion']==1){
                  if($GLOBALS['f_r_tipo']=='01'){
                    $tr_htm='<br><b>Tipo Relación:</b>01 Nota de crédito de los documentos relacionados<br>';
                  }
                  if($GLOBALS['f_r_tipo']=='04'){
                    $tr_htm='<br><b>Motivo Cancelacion:</b> 01 Comprobante emitido con errores con relación<br>
                      <b>Tipo Relación:</b>04 Sustitución de los CFDI previos<br>';
                  }
                  if($GLOBALS['f_r_tipo']=='07'){
                    $tr_htm='<br><b>Tipo Relación:</b>07 CFDI por aplicación de anticipo<br>';
                  }
            $html2.=$tr_htm.'<b>Folio relacionado:</b> '.$GLOBALS['f_r_uuid'];
              }
             $html2 .= '</td> 
              <td width="10%" align="left"></td>
              <td width="27%" align="right" class="httablelinea"><b>SUBTOTAL</b></td>
              <td width="15%" align="center" class="httablelinea">$ '.number_format(($GLOBALS["subtotal"]),2,'.',',').'</td>
            </tr>';
            if($GLOBALS['t_descuento']==1){
              $html2.='<tr>';
                $html2.='<td >&nbsp;</td>';
                $html2.='<td align="right" class="httablelinea"><b>DESCUENTO</b></td>';
                $html2.='<td align="center" class="httablelinea">$ '.number_format($descuentog,2,'.',',').'</td>';
              $html2.='</tr>';
            }
        $html2.='<tr>
          <td >&nbsp;</td>
          <td align="right" class="httablelinea"><b>IMPUESTO TRASLADADO I.V.A. 0.16</b></td>
          <td align="center" class="httablelinea">$ '.number_format($GLOBALS["iva"],2,'.',',').'</td>
        </tr>';

        if ($GLOBALS["isr"]>0) {
            $html2 .= '<tr>
                        <td></td>
                        <td align="right" class="httablelinea"><b>10% Retención ISR</b></td>
                        <td align="center"class="httablelinea">$ '.number_format($GLOBALS["isr"],2,'.',',').'</td>
                      </tr>';
        }
        if ($GLOBALS["ivaretenido"]>0) {
            $html2.='<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>10.67% Retención IVA</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["ivaretenido"],2,'.',',').'</td>
                    </tr>';
        }
        
        if ($GLOBALS["cincoalmillarval"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>5 al millar</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["cincoalmillarval"],2,'.',',').'</td>
                    </tr>';
        }
        if ($GLOBALS["outsourcing"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>6% Retención servicios de personal</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["outsourcing"],2,'.',',').'</td>
                    </tr>';
        }
        
        $html2 .= '<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>TOTAL</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["total"],2,'.',',').'</td>
                    </tr>';
      $html2 .= '</table>';
      if($GLOBALS["tarjeta"]==''){
        $display_targeta=' style="display:none" ';
      }else{
        $display_targeta=' ';
      }
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooter">
        <tr>
          <td width="50%"><b>MÉTODO DE PAGO: </b>'.$GLOBALS["MetodoPagol"].'</td>
          <td width="50%" '.$display_targeta.' ><b>ÚLTIMOS 4 DIGITOS DE LA CUENTA O TARJETA: </b>'.$GLOBALS["tarjeta"].'</td>
        </tr>
        <tr>
          <td width="50%"><b>FORMA DE PAGO: </b>'.$GLOBALS["FormaPagol"].'</td>
          <td width="50%"></td>
        </tr>
        
      </table>';
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooter">
        <tr>
          <td width="13%"><b>OBSERVACIONES:</b></td>
          <td width="87%" style="text-align: justify; font-size:7.5px">'.$GLOBALS["ticketmensaje"].'</td>
        </tr>
      </table>
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooter" align="center">
        <tr><td width="30%"></td><td style="font-size:8px;" width="40%">RECIBO MERCANCIA E INDICACIONES DEL USO DEL PRODUCTO</td><td width="30%"></td></tr>
        <tr><td></td><td class="httablelinea"></td><td></td></tr>
        <tr><td></td><td style="font-size:8px;">FIRMA DEL CLIENTE</td><td></td></tr>
      </table>
      ';
    
    }else{
      //$this->SetFooterMargin('10');

    // set auto page breaks
    //$this->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM
      $html2 .= '
      <table border="0" ><tr><td style="height:200px"></td></tr></table>';
    }
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooterpt" cellpadding="3">
        <tr>
          <td rowspan="6" width="14%" align="center" style="text-align: center;" ><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
          <td width="86%" class="httablelinea">SELLO DIGITAL DEL EMISOR</td>
        </tr>
        <tr>
          <td height="35px" class="httablelinea fontFooterpt">'.$GLOBALS["selloemisor"].'</td>
        </tr>
        <tr>
          <td class="httablelinea">SELLO DIGITAL SAT</td>
        </tr>
        <tr>
          <td height="35px" valign="top" class="httablelinea fontFooterpt">'.$GLOBALS["sellosat"].'
          </td>
        </tr>
        <tr>
          <td class="httablelinea">CADENA ORIGINAL DEL COMPLEMENTO DE CERTIFICACIÓN DIGITAL DEL SAT</td>
        </tr>
        <tr>
          <td height="35px" class="httablelinea fontFooterpt6" colspan="1">'.substr(utf8_encode($GLOBALS["cadenaoriginal"]), 0, 635).'</td>
        </tr>
      </table>';
    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td align="center"></td>
        </tr>
        <tr>
          <td align="right" class="footerpage">
          Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      $this->writeHTML($html2, true, false, true, false, '');
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '82', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$SetFooterMarginvalor=105;//116
$pdf->SetFooterMargin($SetFooterMarginvalor);

// set auto page breaks
$pdf->SetAutoPageBreak(true, $SetFooterMarginvalor);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');

$htmlp='
      <style type="text/css">
        .httable{vertical-align: center;border-bottom: 2px solid #9e9e9e;/*border-top: 1px solid #9e9e9e;*/font-size: 7.5px;}
        .httablepro{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
        .magintablepro{margin-top:0px;margin-bottom:0px;margin: 0px;}
        .font8{font-size: 7.5px;}
      </style>';
      
      if($t_descuento==0){
        $col1=7;//cant
        $col2=10;//uni
        $col3=14;//ser
        $col4=31;//des
        $col5=10;//V.U.
        $col6=10;//descuento
        $col7a=9;$col7b=9;
        $col7=$col7a+$col7b;//impuesto
        $col8=10;//subtotal
      }else{
        $col1=5;//cant
        $col2=10;//uni
        $col3=13;//ser
        $col4=29;//des
        $col5=10;//V.U.
        $col6=8;//descuento
        $col7a=8.5;$col7b=8;
        $col7=$col7a+$col7b;//impuesto
        $col8=9;//subtotal
      }
  $htmlp.='<table border="0" align="center" cellpadding="2">';
    $htmlp.='<thead>';
    $htmlp.='<tr valign="middle">';
      $htmlp.='<th width="'.$col1.'%" class="httable_"><b>CANT.</b></th>';
      $htmlp.='<th width="'.$col2.'%" class="httable_"><b>CLAVE DE UNIDAD</b></th>';
      $htmlp.='<th width="'.$col3.'%" class="httable_"><b>CLAVE DE PROD.<br>Y/O SERVICIO</b></th>';
      $htmlp.='<th width="'.$col4.'%" class="httable_"><b>DESCRIPCIÓN</b></th>';
      $htmlp.='<th width="'.$col5.'%" class="httable_"><b>VALOR UNITARIO</b></th>';
      if($t_descuento==1){
        $htmlp.='<th width="'.$col6.'%" class="httable_"><b>DESCUENTO</b></th>';
      }
      $htmlp.='<th width="'.$col7.'%" class="httable_" colspan="2"><b>IMPUESTO</b></th>';
      $htmlp.='<th width="'.$col8.'%" class="httable_"><b>SUBTOTAL</b></th>';
    $htmlp.='</tr></thead><tbody>';

$rowr=0;
foreach ($facturadetalles as $item) {
  
  //for ($i = 1; $i <= 43; $i++) {
    $rowr++;
    if($rowr==15 || $rowr==29 || $rowr==43){
      $htmlp.='<tr><td colspan="6"></td></tr>';
    }
    $cod="";
    /*if($item->tipo_prod==1 || $item->tipo_prod==2){
      $cod = $item->idProducto;
    }else{
      $cod = $item->codigo;
    } */ 
    if($item->consin_iva==1){
      if($item->iva>0){
        $porc_imp=.16;
        $txt_imp = "002 - IVA 16%";
      }else{
        $porc_imp=0;
        $txt_imp = "002 - IVA 0%"; 
      }
    }else{
      $txt_imp="EXENTO";
    }
    if($item->descuento>0){
      if($item->iva>0){
        $item->iva=number_format((($item->Cantidad*$item->Cu)-$item->descuento)*0.16,5,'.',',');
      }
    }
      $htmlp.='<tr class="magintablepro">';
        $htmlp.='<td width="'.$col1.'%" class="httablepro font8" align="center">'.intval($item->Cantidad).'</td>';
        $htmlp.='<td width="'.$col2.'%" class="httablepro font8" align="center">'.$item->Unidad .' / '.$item->nombre.'</td>';
        $htmlp.='<td width="'.$col3.'%" class="httablepro font8">'.$item->servicioId.'</td>';
        $htmlp.='<td width="'.$col4.'%" class="httablepro font8">'.$item->Descripcion2.'</td>';
        $htmlp.='<td width="'.$col5.'%" class="httablepro font8" align="center">$ '.$item->Cu.'</td>';
        if($t_descuento==1){
          $htmlp.='<td width="'.$col6.'%" class="httablepro font8" align="center">$ '.number_format($item->descuento,2,'.',',').'</td>';
        }
        $htmlp.='<td width="'.$col7a.'%" class="httablepro font8">'.$txt_imp.'</td>';
        $htmlp.='<td width="'.$col7b.'%" class="httablepro font8">$'.$item->iva.'</td>';
        $htmlp.='<td width="'.$col8.'%" class="httablepro font8" align="center">$ '.number_format(($item->Cantidad*$item->Cu)-$item->descuento,2,'.',',').'</td>';
      $htmlp.='</tr>';
  //}
  
}

$htmlp.='</tbody></table>';
$pdf->writeHTML($htmlp, true, false, true, false, '');

// Obtener el número total de páginas
$totalPages = $pdf->getNumPages();
//log_message('error','$totalPages: '.$totalPages);
// Establecer el número total de páginas en la instancia de PDF
$pdf->setTotalPages($totalPages);

//$pdf->Output(__DIR__.'Factura_'.$Folio.'.pdf', 'FI');
$url=FCPATH.'/hulesyg/facturaspdf/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'Factura_'.$Folio.'.pdf', 'FI');
if(isset($idfactura)){
  //$pdf->Output('hulesygrapas/hulesyg/facturas/'.$idfactura.'.pdf', 'F');
}
//$pdf->Output($idfactura.'.pdf', 'F');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
if(isset($_GET['envio'])){
  if($correo!=''){
    $this->ModeloMail->envio_fac($idfactura,$correo);
  }
  
}

?>