<?php
require_once('TCPDF4/examples/tcpdf_include.php');
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
$GLOBALS['fechai']=$fechai;
$GLOBALS['fechaf']=$fechaf;
$GLOBALS['empleado']=$empleado;

//=======================================================================================
class MYPDF extends TCPDF {
    public function Header() {
        $img_file = base_url().'public/img/reportes/header.png';  
        $this->Image($img_file, 0, 0, 210,45, '', '', '', false, 330, '', false, false, 0);
        $txt="Reporte de Ventas ";
        
        $html='<table border="0"><tr>
                <td width="70%">
                </td>
                <td>
                    <img src="'.base_url().'public/img/SEMIT.jpg">
                </td>
            </tr><tr>
                <td width="100%" style="font-size:18px;color: #012d6a; text-align: right;">Reporte de Ventas</td>
            </tr>
            <tr>
                <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Del: '.date("d-m-Y ", strtotime($GLOBALS['fechai'])).'
                </td>
            </tr>
            <tr>
                <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Al: '.date("d-m-Y ", strtotime($GLOBALS['fechaf'])).'
                </td>
            </tr>
            <tr>
                <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Generado por: '.$GLOBALS['empleado'].'</td>
            </tr>
            <tr>
                <td width="100%" style="font-size:12px;color: #012d6a; text-align: right;">Fecha de generación: '.date("d-m-Y H:i a").'</td>
            </tr>
        </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/reportes/footer.png';  
        $this->Image($img_file, 0, 250, 210, 48, '', '', '', false, 330, '', false, false, 0); 
    }
} 


$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('Reporte de ventas');
$pdf->SetSubject('Reporte');
$pdf->SetKeywords('Reporte');

// set default header data
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(true);
$pdf->SetFooterMargin(true);
$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('helvetica', '',10);
// add a page
$pdf->SetMargins(8, 65, 8, true);
$pdf->AddPage('P', 'A4');

$html='<style type="text/css">
    .httabled{
        font-size:12px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }
    
    .td_l{
        font-size:11px;
    }

    .tableinfo{
        font-size:11px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        text-align:left;
    }
    .tableinfo th{
        font-weight:bold;
    }

    .table_det{
        border: 1px solid #93ba1f;
        text-align:left;
    }
    .httabled{
        font-size:11px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }  color:black;

  </style>';

$html.='<table borde="0" cellpadding="5" class="tableinfo table_det">';
    $html.='<thead>';
        $html.='<tr>';
            $html.='<th class="httabled">Sucursal</th>';
            $html.='<th class="httabled">Subtotal</th>';
            $html.='<th class="httabled">IVA</th>';
            $html.='<th class="httabled">Total vendido</th>';
        $html.='</tr>';
    $html.='</thead>';
    $html.='<tbody class="body_ventas">';
$supertot=0;
foreach ($getv as $c) {
    $html .= '<tr style="color: #012d6a;">';
    $html .= '<td> <input type="hidden" id="name_suc" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
    $html .= '<td>$'.number_format($c->subtotal,2,".",",").'</td>';
    $html .= '<td>$'.number_format($c->iva,2,".",",").'</td>';
    $html .= '<td><input type="hidden" id="total" value="'.$c->total.'">$'.number_format($c->total,2,".",",").'</td>';
    $html .= '</tr>';
    $supertot=$supertot+$c->total;
}                
$html.='</tbody>
    <tfoot>
        <tr>
            <td colspan="2"></td>
            <td class="httabled"><b>Total vendido: </b></td>
            <td class="httabled"><input type="hidden" id="supertot" value="'.$supertot.'">$'.number_format($supertot,2,".",",").'</td>
        </tr>
    </tfoot>';
$html.='</table>';
$pdf->writeHTML($html, true, false, true, false, '');


$pdf->AddPage('P', 'A4');
$htmlutilidad='<style type="text/css">
    .httabled{
        font-size:12px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }
    .td_l{
        font-size:11px;
    }
    .tableinfo{
        font-size:11px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        text-align:left;
    }
    .tableinfo th{
        font-weight:bold;
    }

    .table_det{
        border: 1px solid #93ba1f;
        text-align:left;
    }
    .httabled{
        font-size:11px;
        border-bottom: 1px solid #93ba1f;
        border-top: 1px solid #93ba1f;
        color:black;
        background-color:#93ba1f;
    }  color:black;
  </style>';

$htmlutilidad.='<table border="0" cellpadding="5" class="tableinfo table_det">';
    $htmlutilidad.='<thead>';
        $htmlutilidad.='<tr>';
            $htmlutilidad.='<th class="httabled">Sucursal</th>';
            $htmlutilidad.='<th class="httabled">Total Ventas</th>';
            $htmlutilidad.='<th class="httabled">Utilidad</th>';
        $htmlutilidad.='</tr>';
    $htmlutilidad.='</thead>';
    $htmlutilidad.='<tbody class="body_utilidad">';
$supertot=0; $sub_ganancia = 0;
foreach ($get_uti as $c) {
    $sub_ganancia = $c->total - $c->sub_compra_iva;
    $htmlutilidad .= '<tr style="color: #012d6a;">';
    $htmlutilidad .= '<td> <input type="hidden" id="name_suc_uti" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
    $htmlutilidad .= '<td>$'.number_format($c->total,2,".",",").'</td>';
    $htmlutilidad .= '<td><input type="hidden" id="total_utilidad" value="'.$sub_ganancia.'">$'.number_format($sub_ganancia,2,".",",").'</td>';
    $htmlutilidad .= '</tr>';
    $supertot=$supertot+$sub_ganancia;
}                
$htmlutilidad.='<tr>
            <td></td>
            <td class="httabled"><b>Total utilidad: </b></td>
            <td class="httabled"><input type="hidden" id="suptot_uti" value="'.$supertot.'">$'.number_format($supertot,2,".",",").'</td>
        </tr>
    </tbody>';
$htmlutilidad.='</table>';
$pdf->writeHTML($htmlutilidad, true, false, true, false, '');
$pdf->Output('Reporte_'.$fechaf.'.pdf', 'I');
?>