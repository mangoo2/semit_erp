<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10"> 
              <h3 style="color: #1770aa;">Nuevo servicio</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Servicios" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <form id="form_datos" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="row"> 
                <div class="col-md-4">
                  <label>Clave de producto<span style="color: red">*</span></label>
                  <input type="text" name="clave" class="form-control" value="<?php echo $clave;?>">
                </div>
                <div class="col-md-8">
                  <label>Descripción/Nombre<span style="color: red">*</span></label>
                  <input type="text" name="descripcion" class="form-control" value="<?php echo $descripcion;?>">
                </div>
            </div>   
            <hr> 
            <div class="form-group row">
              <label class="col-lg-1 col-form-label" style="color: #1770aa;"><b>Periodos</b><span style="color: red">*</span></label>
              <div class="col-lg-2">
                  <span class="switch switch-icon">
                      <label>
                          <input type="checkbox" name="perido" id="perido" onclick="check_habilitar()" <?php if($perido==1) echo 'checked' ?>>
                          <span></span>
                      </label>
                  </span>
              </div>     
            </div>  
            <?php
              $check_disbled=''; 
              if($perido==1){
                $check_disbled=''; 
              }else{
                $check_disbled='disabled'; 
              }

            ?> 
            <div class="row"> 
              <div class="col-md-12">
                <table class="table table-sm" id="table_datos">
                  <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Precios<span style="color: red">*</span></th>
                        <th scope="col">Depósito en garantia<span style="color: red">*</span></th>
                        <th scope="col"></th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1 día<span style="color: red">*</span></td>
                      <td><input class="form-control " type="hidden" name="dia1" value="1">
                        <input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="precios1" value="<?php echo $precios1;?>"></td>
                      <td><input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="depositogarantia1" value="<?php echo $depositogarantia1;?>"></td>
                      <td>
                        <span class="switch switch-icon">
                          <label>
                            <input type="checkbox" class="input_periodo" <?php echo $check_disbled ?> name="check1" id="check1" <?php if($check1==1) echo 'checked' ?>>
                            <span></span>
                          </label>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>15 días<span style="color: red">*</span></td>
                      <td><input class="form-control" type="hidden" name="dias15" value="15">
                        <input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="precios15" value="<?php echo $precios15;?>"></td>
                      <td><input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="depositogarantia15" value="<?php echo $depositogarantia15;?>"></td>
                      <td>
                        <span class="switch switch-icon">
                          <label>
                            <input type="checkbox" class="input_periodo" <?php echo $check_disbled ?> name="check15" id="check15" <?php if($check15==1) echo 'checked' ?>>
                            <span></span>
                          </label>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>30 días<span style="color: red">*</span></td>
                      <td><input class="form-control" type="hidden" name="dias30" value="30">
                        <input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="precios30" value="<?php echo $precios30;?>"></td>
                      <td><input class="form-control input_periodo" <?php echo $check_disbled ?> type="text" name="depositogarantia30" value="<?php echo $depositogarantia30;?>"></td>
                      <td>
                        <span class="switch switch-icon">
                          <label>
                            <input type="checkbox" class="input_periodo" <?php echo $check_disbled ?> name="check30" id="check30" <?php if($check30==1) echo 'checked' ?>>
                            <span></span>
                          </label>
                        </span>
                      </td>
                    </tr>
                  </tbody>
              </table>
              </div>   
            </div>
        </form>  
        <div class="row"> 
          <div class="col-md-12" align="center">
            <button class="btn btn-primary btn_registro" type="button" style="width: 30%;" onclick="guardar_registro_datos()">Guardar</button>                      
          </div>
        </div>
      </div>
    </div>
</div>    
