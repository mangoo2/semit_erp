<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10"> 
              <h3 style="color: #1770aa;">Nuevo servicio</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Cservicios" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <form id="form_datos" method="post">
            <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
            <div class="row form-group"> 
                <div class="col-md-4">
                  <label>Clave<span style="color: red">*</span></label>
                  <input type="text" name="numero" id="numero" class="form-control" value="<?php echo $numero;?>">
                </div>
                <div class="col-md-8">
                  <label>Descripción/Nombre<span style="color: red">*</span></label>
                  <input type="text" name="descripcion" id="descripcion" class="form-control" value="<?php echo $descripcion;?>">
                </div>
            </div>  
            <div class="row form-group"> 
                <div class="col-md-3">
                  <label>Tipo<span style="color: red">*</span></label>
                  <select class="form-control" id="tipo" name="tipo" required>
                    <option></option>
                    <option value="1" <?php if($tipo==1){ echo 'selected';} ?> >Mantenimiento</option>
                    <option value="2" <?php if($tipo==2){ echo 'selected';} ?> >Correctivo</option>
                    <option value="3" <?php if($tipo==3){ echo 'selected';} ?> >Logistica</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <label>Precio sin iva<span style="color: red">*</span></label>
                  <input type="number" name="precio_siva" id="precio_siva" class="form-control" value="<?php echo $precio_siva;?>" oninput="calcular()">
                </div>
                <div class="col-md-3">
                  <label>Iva<span style="color: red">*</span></label>
                  <select class="form-control" id="iva" name="iva" required onchange="calcular()">
                    <option></option>
                    <option value="1" <?php if($iva==1){ echo 'selected';} ?> >Aplica iva</option>
                    <option value="0" <?php if($iva==0){ echo 'selected';} ?> >Sin Iva</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <label>Precio final</label>
                  <input type="number" id="preciofinal" class="form-control"  value="<?php 
                    $pf=$precio_siva;
                    if($iva==1){
                      $iva=round($precio_siva*0.16,5);
                      $pf=round($precio_siva+$iva,2);
                    }
                    echo $pf;
                  ?>" readonly>
                </div>
            </div>   
             <div class="row form-group">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label col-sm-12">Unidad<span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="Unidad" name="Unidad">
                      <?php foreach ($f_u->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="col-form-label">Servicio<span style="color: red">*</span></label>
                    <select class="form-control col-sm-12" id="servicioId" name="servicioId">
                      <?php foreach ($f_s->result() as $item) {
                        echo '<option value="'.$item->Clave.'">'.$item->Clave.' / '.$item->nombre.'</option>';
                      }?>
                    </select>
                  </div>
                </div>
              </div>
    
            
        </form>  
        <div class="row"> 
          <div class="col-md-12" align="center">
            <button class="btn btn-primary btn_registro" type="button" style="width: 30%;" onclick="guardar_registro_datos()">Guardar</button>                      
          </div>
        </div>
      </div>
    </div>
</div>    
