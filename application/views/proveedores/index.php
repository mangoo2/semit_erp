<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick.css?v2022">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/slick/slick-theme.css?v2022">

<style type="text/css">
  .slider {
        width: 100%;
        margin: 50px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
</style>

<style type="text/css">
    .dataTables_filter label, .dataTables_paginate .pagination{
        float: right;
    }
    #table_datos_filter{
        display: none;
    }
</style>
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="search" placeholder="Buscar proveedor" id="searchtext" oninput="search()">
                    </div>
                </div>
            </div> 
            <div class="col-lg-2">
              <select class="form-control" id="estatus" onchange="reload_registro()">
                <option value="1">Proveedores activos</option>
                <option value="0">Proveedores inactivos</option>
              </select>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-2" >
                <?php if($perfilid==1){ ?>
                    <button class="btn btn-success exportExcel" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Excel</button>
                <?php } ?>
            </div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Proveedores/registro" class="btn btn-primary">Nuevo registro</a>
            </div>    
        </div>  
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">    
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos" style="width:100%">
                            <thead>
                                <tr>
                                    <!--<th scope="col">Id</th>-->
                                    <th scope="col">Código</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Teléfono 1</th>
                                    <th scope="col">Personal de contacto</th>
                                    <th scope="col">Datos fiscales</th>
                                    <th scope="col">Productos</th>
                                    <th scope="col">Saldo</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>

<div class="modal fade" id="modal_datos_fiscales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Datos fiscales</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_datos_fiscales"></div> 
              </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_prods" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Lista de productos vendidos por proveedor</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-md-12">
                            <input type="hidden" id="idprove" value="0">
                            <table class="table table-sm" id="table_prods">
                                <thead>
                                  <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Nombre de Producto</th>
                                    <th scope="col">Compra</th>
                                    <th scope="col">Venta</th>
                                    <th scope="col">Código Proveedor</th>
                                    <th scope="col">Existencia</th>
                                  </tr>
                                </thead>
                                <tbody id="det_prods">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-success exportExcel" type="button">Descargar Excel</button>
                    <button class="btn btn-danger printpdf" type="button">Imprimir PDF</button>
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                </div>   
            </div>
        </div>
    </div>
</div>