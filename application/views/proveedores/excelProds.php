<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=proveecompras_".date('YmdGis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <table border="1" id="tabla" cellpadding="5" cellspacing="0" width="100%" style="color: #012d6a;">
    <thead>
      <tr><th colspan="6"><?php echo $result_productos[0]->provee; ?></th></tr>
      <tr>
        <th class="httabled">Código</th>
        <th class="httabled">Nombre de Producto</th>
        <th colspan="2">Costo</th>
        <!--<th width="15%" class="httabled">Compra</th>
        <th width="15%" class="httabled">Venta</th>-->
        <th width="15%">Código Producto Proveedor</th>
        <th>Existencia</th>
      </tr>
      <tr>
        <th colspan="2"></th>
        <th>Compra</th>
        <th>Venta</th>
        <th colspan="2"></th>
      </tr>
  </thead>
  <tbody>
    <?php $supertot=0; $html="";
    foreach ($result_productos as $p) {
      $stock=0;
      if($p->tipo!=1 && $p->tipo!=2){
        $stock=$p->stock_ps;
      }if($p->tipo==1){
        $stock=$p->stock_series;
      }if($p->tipo==2){
        $stock=$p->stock_lotes;
      }
      $html.='<tr>
            <td>'.$p->idProducto.'</td>
            <td>'.$p->nombre.'</td>
            <td>$'.number_format($p->costo_compra,2,".",",").'</td>
            <td>$'.number_format($p->precio,2,".",",").'</td>
            <td>'.$p->cod_provee.'</td>
            <td>'.$stock.'</td>
        </tr>';
    }
    echo $html;
    ?>
    </tbody>
  </table>
