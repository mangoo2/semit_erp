<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=lista_provees".date('Ymd').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
  <thead>
    <tr>
    	<th>Código</th>
      <th>Nombre</th>
      <th>Teléfono 1</th>
      <th>Teléfono 2</th>
      <th>Teléfono movil</th>
      <th>Correo electrónico</th>
      <th>Persona de contacto</th>
      <th>Dirección</th>
      <th>Razón social</th>
      <th>RFC</th>
      <th>CP</th>
      <th>Saldo</th>
      <th>Estatus</th>
    </tr>
    </tr>
  </thead>
  <tbody>
  	<?php 
      $html="";
      foreach ($prove as $p) {
        $estatus='';
        if($p->estatus==1){
          $estatus='Activo';
        }else{
          $estatus='Inactivo';
        }
        $html.="<tr>";
          $html.="<td>&nbsp;".$p->codigo."&nbsp;</td>";
          $html.="<td>".$p->nombre."</td>";
          $html.="<td>&nbsp;".$p->tel1."&nbsp;</td>";
          $html.="<td>&nbsp;".$p->tel2."&nbsp;</td>";
          $html.="<td>&nbsp;".$p->celular."&nbsp;</td>";
          $html.="<td>".$p->correo."</td>";
          $html.="<td>".$p->contacto."</td>";
          $html.="<td>".$p->direccion."</td>";
          $html.="<td>".$p->razon_social."</td>";
          $html.="<td>".$p->rfc."</td>";
          $html.="<td>".$p->cp."</td>";
          $html.="<td>".$p->total_compras."</td>";
          $html.="<td>".$estatus."</td>";
        $html.="</tr>";
      }
      echo $html;
      ?>
  </tbody>
</table>
