<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <h3>Saldo de Proveedor: <?php echo $provee; ?></h3>
        <div class="row">
            <input type="hidden" id="id" value="<?php echo $id; ?>">
            <div class="col-lg-4">
                <div class="form-group row">
                    <div class="col-12">
                        <label style="color: white">.</label>
                        <input class="form-control" type="search" placeholder="Buscar compra" id="searchtext" oninput="search()">
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group row">
                    <div class="col-12">
                        <div >
                            <label>Fecha inicio</label>
                            <input class="form-control" type="date" placeholder="Buscar entrada" id="fe1">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group row">
                    <div class="col-12">
                        <div >
                            <label>Fecha fin</label>
                            <input class="form-control" type="date" placeholder="Buscar entrada" id="fe2">
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <label></label>
                <div class="col-lg-12">
                    <button title="Exportar registros de compras" id="btn_export_excel" type="button" class="btn btn-success">Exportar  <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                </div>
            </div>  
        </div>  
        <div class="card card-custom gutter-b">
            <div class="card-body">

                <div class="row">    
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th scope="col">Folio</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Fecha y hora</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">Código de proveedor</th>
                                    <th scope="col">Monto de compra</th>
                                    <th scope="col">Productos</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>

<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cant.</th>
                                    <th>Costo</th>
                                    <th>IVA</th>
                                    <th>ISR</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>
                            <tbody class="table_pro"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
