<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row"> 
        <div class="col-lg-10" align="right"> 
        </div>   
        <div class="col-lg-2" align="right"> 
            <a href="<?php echo base_url() ?>Proveedores" class="btn btn-primary">Regresar</a>
        </div>    
    </div>  
    <br>
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <input type="hidden" id="idreg" value="<?php echo $id ?>">
      <div class="card-body">
        <ul class="nav nav-tabs" id="icon-tab" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#icon-home" role="tab" aria-controls="icon-home" aria-selected="true">Datos generales</a></li>
          <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#profile-icon" role="tab" aria-controls="profile-icon" aria-selected="false">Datos fiscales</a></li>
        </ul>
        <div class="tab-content" id="icon-tabContent">
          <div class="tab-pane fade show active" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">

            <form id="form_generales" method="post">
              <input type="hidden" id="idreg1" name="id" value="<?php echo $id ?>">
              <br>
              <div class="row">
                <div class="col-md-3">
                  <label class="form-label">Código/ID de proveedor</label>
                  <input class="form-control" type="text" disabled="" value="<?php echo $codigo ?>">
                </div>
                <div class="col-md-9">
                  <label class="form-label">Nombre<span style="color: red">*</span></label>
                  <input class="form-control" type="text" name="nombre" value='<?php echo $nombre ?>'>
                </div>
              </div>
              <div class="row">  
                <div class="col-md-4">
                  <label class="form-label">Teléfono 1<span style="color: red">*</span></label>
                  <input class="form-control" type="text" name="tel1" value="<?php echo $tel1 ?>">
                </div>
                <div class="col-md-4">
                  <label class="form-label">Teléfono 2</label>
                  <input class="form-control" type="text" name="tel2" value="<?php echo $tel2 ?>">
                </div>
                <div class="col-md-4">
                  <label class="form-label">Teléfono móvil</label>
                  <input class="form-control" type="text" name="celular" value="<?php echo $celular ?>">
                </div>
              </div>

              <div class="row">  
                <div class="col-md-4">
                  <label class="form-label">Correo electrónico<span style="color: red">*</span></label>
                  <input class="form-control" type="text" name="correo" value="<?php echo $correo ?>">
                </div>
                <div class="col-md-4">
                  <label class="form-label">Personal de contacto</label>
                  <input class="form-control" type="text" name="contacto" value="<?php echo $contacto ?>">
                </div>
                <div class="col-md-4">
                  <label class="form-label">Estatus</label>
                  <span class="switch switch-icon">
                    <label>
                        <input type="checkbox" name="estatus" <?php if($estatus==1) echo 'checked' ?>>
                        <span></span>
                    </label>
                  </span>
                </div>
              </div>
              <div class="row">  
                <div class="col-md-12">
                  <label class="form-label">Dirección</label>
                  <textarea  class="form-control" name="direccion"><?php echo $direccion ?></textarea>
                </div>
              </div>  
              <br>
            </form>
            <br>
            <div align="center">
              <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_registro_datos()">Guardar</button>
            </div>
          </div>
          <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
            <br>
            <form id="form_registro_datos_fiscales" method="post">
              <input type="hidden" id="idreg2" name="id" value="<?php echo $id ?>">
              <div class="row">
                <div class="col-md-6">
                  <label class="form-label">Activar datos fiscales</label>
                  <span class="switch switch-icon">
                    <label>
                        <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscales" <?php if($activar_datos_fiscales==1) echo 'checked' ?>>
                        <span></span>
                    </label>
                  </span>
                </div>
              </div>
              <div class="row">  
                <div class="col-md-4">
                  <label class="form-label">Razón social</label>
                  <input class="form-control" type="text" name="razon_social" value='<?php echo $razon_social ?>'>
                </div>
                <div class="col-md-4">
                  <label class="form-label">RFC</label>
                  <input class="form-control" type="text" name="rfc" value="<?php echo $rfc ?>">
                </div>
                <div class="col-md-4">
                  <label class="form-label">CP</label>
                  <input class="form-control" type="text" name="cp" value="<?php echo $cp ?>">
                </div>
                <div class="col-md-6">
                  <label class="form-label">Régimen fiscal</label>
                  <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()">
                      <option value="0" selected disabled>Seleccione</option>
                      <?php foreach ($get_regimen->result() as $item) {
                          if($item->clave==$RegimenFiscalReceptor){
                              echo '<option value="'.$item->clave.'" selected>'.$item->clave.' '.$item->descripcion.'</option>';
                          }else{
                              echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                          }
                          
                      }?>
                  </select>
                </div>
                <div class="col-md-6" style="display: none;">
                  <label class="form-label">Uso de CFDI</label>
                  <select class="form-control" id="uso_cfdi" name="uso_cfdi">
                      <option value="0" selected disabled>Seleccione</option>
                      <?php foreach ($get_cfdi->result() as $item) {
                          if($item->uso_cfdi==$uso_cfdi){
                              echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled selected>'.$item->uso_cfdi_text.'</option>';
                          }else{
                              echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                          }
                      }?>
                  </select>
                </div>
              </div>
            </form> 
            <br>
            <div align="center">
              <button class="btn btn-primary btn_ajustes" style="width: 30%" type="button" onclick="guardar_registro_datos_fiscales()">Guardar</button>
            </div>
            
          </div>
        </div>
      </div>
    </div>  
  </div>
</div>    

<div class="modal fade" id="modalcategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Categorías</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row g-3">
          <div class="col-md-10">
            <form class="form" method="post" id="form_categoria" style="width: 100%">
              <label class="form-label">Nombre</label>
              <input type="hidden" name="categoriaId" id="idcategoria_categoria" value="0">
              <input class="form-control" type="text" name="categoria" id="nombre_categoria">
            </form>
          </div>
          <div class="col-md-2">
            <label class="form-label" style="color: white">__</label><br>
            <button class="btn btn-primary btn_categoria" type="button" onclick="add_categoria()"><i class="fa fa-save"></i></button>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm" id="table_categoria" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-secondary" type="button">Guardar</button>
      </div>
    </div>
  </div>
</div>