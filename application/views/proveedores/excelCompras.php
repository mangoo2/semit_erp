<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=proveecompras_".date('YmdGis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellpadding="5"  cellspacing="0" width="100%">
  <thead>
    <tr><th colspan="6"><b><?php echo $comp[0]->proveedor ?></b></th></tr>
    <tr>
      <th scope="col">Folio</th>
      <th scope="col">Usuario</th>
      <th scope="col">Fecha y hora</th>
      <!--<th scope="col">Proveedor</th>-->
      <th scope="col">Código de proveedor</th>
      <th scope="col">Monto de compra</th>
      <th scope="col">Estatus</th>  
    </tr>
  </thead>
  <tbody>
    <?php $supertot=0;
    foreach ($comp as $c) {
      $estatus="";
      if($c->estatus==1){
        $estatus='OC Ingresada';
      }if($c->estatus!=1 && $c->estatus!=3){
        $estatus='Pendiente de ingreso';
      }

      echo '<tr style="color: #012d6a;">';
      echo '<td>'.$c->id.'</td>';
      echo '<td>'.$c->personal.'</td>';
      echo '<td>'.date("d-m-Y H:s a" , strtotime($c->reg)).'</td>';
      //echo '<td>'.$c->proveedor.'</td>';
      echo '<td>'.$c->codigo.'</td>';
      echo '<td>$'.number_format($c->total,2,".",",").'</td>';
      echo '<td>'.$estatus.'</td>';
      echo '</tr>';
      $supertot=$supertot+$c->total;
    } ?>
  </tbody>
  <tfoot>
    <tr style="color: #012d6a;">
      <td colspan="3"></td>
      <td><b>Total comprado: </b></td>
      <td>$ <?php echo number_format($supertot,2,".",",");?> </td>
      <td></td>
    </tr>
  </tfoot>
</table>
