<style type="text/css">
  .info_config .card .card-body{
    border: 0px;
  }
  .card-sub-info{
    margin-bottom: 10px;
  }
  .card-sub-info .card-body{
    padding: 12px;
  }
  #accordion .card-body{
    border-radius: 0px;
  }
  #accordion .card .card-header {
    border-bottom: 1px solid #3699ff;
  }
  #accordion .card{
    border: 1px solid #3699ff;
  }
  .card-sub-info .static-top-widget .media-body{
    padding-left: 20px;
  }
</style>
<div class="page-body">
  <div class="container-fluid dashboard-default-sec">
     
    <!--begin::Dashboard-->
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row info_config">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <div class="row">
                <div class="col-sm-12 col-xl-6 col-lg-6">
                  <!----------->
                    <div class="card o-hidden border-0">
                      <div class="bg-primary b-r-4 card-body">
                        <div class="media static-top-widget">
                          <div class="align-self-center text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                          </div>
                          <div class="media-body">
                            <span class="m-0">Folios Adquiridos</span>
                            <h4 class="mb-0 counter"><?php echo number_format($timbresvigentes,0,'.',',');?></h4>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!----------->
                </div>
                <div class="col-sm-12 col-xl-6 col-lg-6">
                  <!----------->
                    <div class="card o-hidden border-0">
                      <div class="bg-success b-r-4 card-body">
                        <div class="media static-top-widget">
                          <div class="align-self-center text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                          </div>
                          <div class="media-body">
                            <span class="m-0">Folios Restantes</span>
                            <h4 class="mb-0 counter"><?php echo number_format($facturasdisponibles,0,'.',',');?></h4>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!----------->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-2 col-lg-2">
                
                <div class="row">
                  <div class="col-md-12">
                    <!----------->
                      <div class="card o-hidden border-0 card-sub-info">
                        <div class="bg-success b-r-4 card-body">
                          <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                            <div class="media-body">
                              <span class="m-0">Factura comercial:</span>
                              <h4 class="mb-0 counter"><?php echo number_format($facturastimbradas,0,'.',',');?></h4>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!----------->
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!----------->
                      <div class="card o-hidden border-0 card-sub-info">
                        <div class="bg-success b-r-4 card-body">
                          <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                            <div class="media-body">
                              <span class="m-0">Notas de credito:</span>
                              <h4 class="mb-0 counter"><?php echo number_format($facturastimbradas_notas,0,'.',',');?></h4>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!----------->
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!----------->
                      <div class="card o-hidden border-0 card-sub-info">
                        <div class="bg-success b-r-4 card-body">
                          <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                            <div class="media-body">
                              <span class="m-0">Cancelaciones:</span>
                              <h4 class="mb-0 counter"><?php echo number_format($facturascanceladas,0,'.',',');?></h4>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!----------->
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!----------->
                      <div class="card o-hidden border-0 card-sub-info">
                        <div class="bg-success b-r-4 card-body">
                          <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                            <div class="media-body">
                              <span class="m-0">Complementos de pago:</span>
                              <h4 class="mb-0 counter"><?php echo number_format($complementotimbradas,0,'.',',');?></h4>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!----------->
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!----------->
                      <div class="card o-hidden border-0 card-sub-info">
                        <div class="bg-success b-r-4 card-body">
                          <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                            <div class="media-body">
                              <span class="m-0">Carta porte:</span>
                              <h4 class="mb-0 counter"><?php echo number_format($facturastimbradas_cartaporte,0,'.',',');?></h4>
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database icon-bg"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                            </div>
                          </div>
                        </div>
                      </div>
                    <!----------->
                  </div>
                </div>

            </div>
            <div class="col-sm-12 col-md-5 col-lg-5">
              <h5>Número de facturas Realizadas por mes</h5>
              <canvas id="chart_mes"></canvas>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5">
              <h5>Ventas del año</h5>
              <canvas id="chart_anio"></canvas>
            </div>
          </div>




          
        </div>
        <div>
            <div class="default-according" id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Datos Fiscales</button>
                  </h5>
                </div>
                <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-bs-parent="#accordion" style="">
                  <div class="card-body">
                    <!-------------------------------------->
                      <form class="row" id="formdatosgenerales"  >
                        <input type="hidden" name="ConfiguracionesId"  value="<?php echo $ConfiguracionesId;?>" id="ConfiguracionesId">
                        <div class="row">
                          <div class="col-md-3">
                            <label for="Nombre" class="active">RAZÓN SOCIAL</label>
                            <input type="text" id="Nombre" name="Nombre" class="form-control" value='<?php echo $Nombre;?>'>          
                          </div>
                          <div class="col-md-3">
                            <label for="Curp" class="active">CURP</label>
                            <input type="text" id="Curp" name="Curp" class="form-control" value="<?php echo $Curp;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Rfc" class="active">RFC</label>
                            <input type="text" id="Rfc" name="Rfc" class="form-control" value="<?php echo $Rfc;?>" required>          
                          </div>
                          <div class="col-md-3">
                            <label for="Rfc" class="active">RÉGIMEN FISCAL</label>
                            <select id="Regimen" name="Regimen" class="form-control browser-default">';
                              <?php
                                foreach ($regimes_result->result() as $item) {
                                  if($Regimen==$item->clave){ $selecteeed='selected';}else{ $selecteeed='';}
                                  echo '<option value="'.$item->clave.'" $selecteeed >'.$item->clave.' '.$item->descripcion.'</option>';
                                }
                              ?>           
                            </select>         
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <label for="Calle" class="active">CALLE</label>
                            <input type="text" id="Calle" name="Calle" class="form-control" value="<?php echo $Calle;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="localidad" class="active">LOCALIDAD</label>
                            <input type="text" id="localidad" name="localidad" class="form-control" value="<?php echo $localidad;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Noexterior" class="active">N° EXT.</label>
                            <input type="text" id="Noexterior" name="Noexterior" class="form-control" value="<?php echo $Noexterior;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Nointerior" class="active">N° INT.</label>
                            <input type="text" id="Nointerior" name="Nointerior" class="form-control" value="<?php echo $Nointerior;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Municipio" class="active">MUNICIPIO</label>
                            <input type="text" id="Municipio" name="Municipio" class="form-control" value="<?php echo $Municipio;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Colonia" class="active">COLONIA</label>
                            <input type="text" id="Colonia" name="Colonia" class="form-control" value="<?php echo $Colonia;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="Estado" class="active">ESTADO</label>
                            <input type="text" id="Estado" name="Estado" class="form-control" value="<?php echo $Estado;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="CodigoPostal" class="active">CÓDIGO POSTAL</label>
                            <input type="text" id="CodigoPostal" name="CodigoPostal" class="form-control" value="<?php echo $CodigoPostal;?>">          
                          </div>
                          <div class="col-md-3">
                            <label for="PaisExpedicion" class="active">PAIS</label>
                            <input type="text" id="PaisExpedicion" name="PaisExpedicion" class="form-control" value="<?php echo $PaisExpedicion;?>">          
                          </div>



                        </div>
                      </form>
                      <div class="row">
                        <div class="col s12">
                          <a  class="btn btn-success" onclick="actualizardatos()" >Actualizar</a>
                        </div>
                      </div>
                    <!-------------------------------------->
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Instalacion de certificacio de sello digital</button>
                  </h5>
                </div>
                <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-bs-parent="#accordion" style="">
                  <div class="card-body">
                    <!-------------------------------------->
                      <div class="row">
                        <div class="col-md-6">
                          <input type="file" id="cerdigital" name="cerdigital" class="form-control" required >
                        </div>
                        <div class="col-md-6">
                          <input type="file" id="claveprivada" name="claveprivada" class="form-control" required>
                          <div class="row" style="margin-top: 5px;">
                            <label for="passprivada" class="active col-md-3" style="color: black; font-size: 14px;">Contraseña de clave privada:</label>
                            <div class="col s9">
                              <input type="password" id="passprivada" name="passprivada" class="form-control" required>          
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <a  class="btn btn-success" onclick="instalararchivos()" >Instalar</a>
                        </div>
                      </div>
                    <!-------------------------------------->
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Histórico de folios adquiridos</button>
                  </h5>
                </div>
                <div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-bs-parent="#accordion">
                  <div class="card-body">
                    <!--------------------------------->
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Timbres</th>
                              <th>Fecha inicio</th>
                              <th>Fecha Fin</th>
                              <th>Reg</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                              foreach ($h_timbres->result() as $item_ht) {
                                echo '<tr>
                              <td>'.$item_ht->cantidad.'</td>
                              <td>'.$item_ht->tv_inicio.'</td>
                              <td>'.$item_ht->tv_fin.'</td>
                              <td>'.$item_ht->reg.'</td>
                            </tr>';
                              }
                            ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!--------------------------------->
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>                
    </div>
  </div>
</div>