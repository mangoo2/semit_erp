<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <form id="form_registro" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                  <h3>Aviso de privacidad</h3>
                                  <textarea id="concepto" name="concepto" cols="30" rows="100"><?php echo $avisoprivacidad ?></textarea>
                                </div>
                            </div>    
                        </form>    
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4" align="center">
                                <button class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>