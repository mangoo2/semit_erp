<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=kardex_equipo_".$id_equipo.".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" cellspacing="0" width="100%">
  <thead>
    <tr><th colspan="7">KARDEX DE SERIE</th></tr>
    <tr>
    	<th>#</th>
      <th>Código / Equipo</th>
      <th>Serie</th>
      <th>Fecha Termino</th>
      <th>Tipo movimiento</th>
      <th>Prox. Mtto.</th>
      <th>Observaciones</th>
    </tr>
  </thead>
  <tbody>
  	<?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
      $html="";
      foreach ($mttos as $m){
        if($m->tipo==0){
          $tipo="Preventivo";
        }if($m->tipo==1){
          $tipo="Correctivo";
        }
        $html .= '<tr>';
          $html .= '<td>'.$m->id.'</td>';
          $html .= '<td>'.$m->idProducto.' / '.$m->equipo.'</td>';
          $html .= '<td>'.$m->serie.'</td>';
          $html .= '<td>'.$m->fecha_termino.'</td>';
          $html .= '<td>'.$tipo.'</td>';
          $html .= '<td>'.$m->prox_mtto.'</td>';
          $html .= '<td>'.$m->observ.'</td>';
        $html .= '</tr>';
      }
      echo $html;
      ?>
  </tbody>
</table>