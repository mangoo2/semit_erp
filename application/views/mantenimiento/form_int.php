<style type="text/css">
    .select_option_cliente .select2-selection--single{background-color: #009bdb; color: white !important; }
    .colorsel{
        font-size: 14px; color: white !important;
    }
    .select2-selection__rendered, .select2-selection__clear { color: white !important; }
    .sel_dis, .select2-selection__rendered, .select2-selection__clear { font-weight: bold; color: white; !important; }
</style>
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="left"> 
                <h3 style="color: #1770aa;">Nuevo registro de mantenimiento</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Mttos_internos" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <input type="hidden" id="id" name="id" value="<?php if(isset($mtto)) echo $mtto->id; else echo "0"; ?>">
                            <input type="hidden" id="idrenta" name="id_renta" value="<?php if(isset($renta)) echo $renta->id; else echo "0"; ?>">
                            <div class="row">
                                <div class="col-md-8 select_option_cliente">
                                    <input class="form-control" type="hidden" id="id_serie" name="idprod_ss" value="<?php if(isset($mtto)) echo $mtto->idprod_ss; if(isset($renta)) echo $renta->id_serie; ?>" >
                                    <select id="id_equipo" name="id_equipo" class="form-select sel_dis" <?php if(isset($renta)) echo "disabled"; ?>> 
                                        <?php if(isset($mtto)) {
                                            echo '<option value="'.$mtto->id_equipo.'">'.$mtto->nombre.'</option>';
                                        }if(isset($renta)) {
                                            echo '<option value="'.$renta->id_prod.'">'.$renta->nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><br></p>
                                    <p><br></p>
                                    <p><br></p>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-2">
                                    <label>Serie</label>
                                    <input readonly class="form-control" type="text" id="serie" value="<?php if(isset($mtto)) echo $mtto->serie; if(isset($renta)) echo $renta->serie; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Tipo de mtto.</label>
                                    <select id="tipo" name="tipo" class="form-select">
                                        <option <?php if(isset($mtto->tipo) && $mtto->tipo=="0") echo "selected"; ?> value="0">Preventivo</option>
                                        <option <?php if(isset($mtto->tipo) && $mtto->tipo=="1") echo "selected"; ?> value="1">Correctivo</option>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <label>Fecha término</label>
                                    <input class="form-control" type="date" id="fecha_termino" name="fecha_termino" value="<?php if(isset($mtto)) echo $mtto->fecha_termino; ?>">
                                </div>
                                <div class="col-lg-2">
                                    <label>Prox. Mtto.</label>
                                    <input type="date" class="form-control" name="prox_mtto" id="prox_mtto" value="<?php if(isset($mtto)) echo $mtto->prox_mtto; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Estatus del mtto.</label>
                                    <select name="estatus" id="estatus" class="form-select">
                                        <option <?php if(isset($mtto->estatus) && $mtto->estatus=="1") echo "selected"; ?> value="1">En mtto</option>
                                        <option <?php if(isset($mtto->estatus) && $mtto->estatus=="2") echo "selected"; ?> value="2">Mtto. terminado</option>
                                        <option <?php if(isset($mtto->estatus) && $mtto->estatus=="3") echo "selected"; ?> value="3">Sin reparación</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><br></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <label>Observaciones</label>
                                    <textarea class="form-control" name="observ" id="observ" rows="4"><?php if(isset($mtto)) echo $mtto->observ; ?></textarea>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
                <div class="col-lg-12"> 
                        <p><br><br><br></p>
                    </div>
                <div class="row g-3">
                    <hr>
                    <input type="hidden" id="id_mttoadd" value="0">
                    <input type="hidden" id="id_vtaadd" value="0">
                    <div class="col-lg-12">
                        <h5 style="color: #1770aa;"><b>Asignación insumos</b></h5>
                    </div>
                    <div class="col-lg-8 select_option_cliente" id="cont_search">
                        <div class="input-group select_option_search mb-3">
                            <select id="sel_insumo" class="form-select btn-pill digits">
                                <option>Búsqueda de insumo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12"> 
                        <p><br><br><br></p>
                    </div>
                    <div class="col-lg-8"> 
                        <table class="table display striped" cellspacing="0" id="table_insumos"> 
                            <thead style="background-color: #009bdb;"> 
                                <tr> 
                                    <th style="color: white !important;">Cantidad</th> 
                                    <th style="color: white !important;">Insumo</th> 
                                    <th></th> 
                                </tr> 
                            </thead> 
                            <tbody class="det_ins_serv">
                                <?php if(isset($ins)){
                                    foreach($ins as $i){
                                        echo "<tr class='row_ins row_ins_".$i->id_insumo."'>
                                            <td><input type='hidden' id='id' value='".$i->id."'><input type='hidden' id='id_ins' value='".$i->id_insumo."'>
                                            <input type='hidden' id='id_ps' value='".$i->id_prod_suc."'>
                                            <input type='hidden' id='id_mtto' value='".$i->id_mtto."'>
                                            <input type='hidden' id='stock' value=''>
                                            ".$i->cantidad."</td>
                                            <td>".$i->nombre."</td>
                                            <td><button class='btn btn-danger' onclick='deleteIns(".$i->id_insumo.",".$i->id.")'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>
                                        </tr>";
                                    }
                                } ?>
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button type="button" class="btn btn-primary" id="savesm" style="width: 60%">Guardar Mtto.</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>
