<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
    .img_icon{
        width: 36px;
    }

    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    .kv-file-remove{
        display: none;
    }
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="search" placeholder="Buscar mantenimiento" id="searchtext" oninput="search()">

                    </div>
                </div>
            </div>
            <div class="col-lg-5"></div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Mttos_internos/kardex" class="btn btn-primary">Kardex de mttos.</a>
            </div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Mttos_internos/registro" class="btn btn-primary">Nuevo registro</a>
            </div>
        </div>  
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">    
                    <div class="col-lg-12"><h3 style="color: #1770aa;">Listado de mantenimientos internos</h3></div>
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th width="5%">Id</th>
                                    <th width="12%">Fecha y hora</th>
                                    <th width="20%">Observaciones</th>
                                    <th width="10%">Equipo</th>
                                    <th width="6%">Serie</th>
                                    <!--<th width="9%">Sucursal</th>-->
                                    <th width="9%">Fin mtto.</th>
                                    <th width="9%">Prox. Mtto.</th>
                                    <th width="9%">Tipo</th>
                                    <th width="9%">Estatus</th>
                                    <th width="11%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modalOrden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de orden de trabajo</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input class="form-control" type="hidden" id="id_mttoord">
                        <input class="form-control" type="hidden" id="id_ordeng" value="0">
                        <div class="col-md-5">
                            <label>ID Mtto.</label>
                            <input class="form-control" type="text" id="id_mtto_show" disabled>
                        </div>
                        <div class="col-md-5">
                            <label>Fecha de Orden</label>
                            <input class="form-control" type="date" id="fecha_orden">
                        </div>
                    </div>
                    <div class="col-lg-12"><br></div>
                    <div class="row g-3">
                        <div class="col-md-12">
                          <label>Observaciones</label>
                          <textarea cols="5" class="form-control" id="observ"></textarea>
                        </div>
                      </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                <div>
                    <button type="button" class="btn btn-success save_orden">Generar Orden</button>
                </div>
            </div>
        </div>
    </div>
</div>
