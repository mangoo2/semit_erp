<style type="text/css">
    .select_option_cliente .select2-selection--single{background-color: #009bdb; color: white !important; }
    .colorsel{
        font-size: 14px; color: white !important;
    }
    .kv-file-upload { display:none; }
</style>
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="left"> 
                <h3 style="color: #1770aa;">Nuevo registro de mantenimiento</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Mantenimientos" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <input type="hidden" id="id" name="id" value="<?php if(isset($mtto)) echo $mtto->id; else echo "0"; ?>">
                            <input type="hidden" id="id_ord" value="<?php if(isset($mtto)) echo $mtto->id_ord; else echo "0"; ?>">
                            <div class="row">
                                <div class="col-md-11 select_option_cliente">
                                    <select id="id_cliente" name="id_cliente" class="form-select">
                                        <?php if(isset($mtto)) {
                                            echo '<option value="'.$mtto->id_cliente.'">'.$mtto->nombre.'</option>';
                                        }?>
                                    </select>
                                </div>
                                <!--<div class="col-lg-1">
                                    <button type="button" class="btn btn-primary btn_add" onclick="addCliente()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                </div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><br></p>
                                    <p><br></p>
                                    <p><br></p>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-2">
                                    <label>Cantidad</label>
                                    <input class="form-control" type="number" id="cantidad" name="cantidad" value="<?php if(isset($mtto)) echo $mtto->id; else echo "1"; ?>" min="1">
                                </div>
                                <div class="col-lg-2">
                                    <label>Marca</label>
                                    <input class="form-control" name="marca" id="marca" value="<?php if(isset($mtto)) echo $mtto->marca; ?>">
                                </div>
                                <div class="col-lg-2">
                                    <label>Modelo</label>
                                    <input class="form-control" name="modelo" id="modelo" value="<?php if(isset($mtto)) echo $mtto->modelo; ?>">
                                </div>
                                <div class="col-lg-2">
                                    <label>Serie</label>
                                    <input class="form-control" name="serie" id="serie" value="<?php if(isset($mtto)) echo $mtto->serie; ?>">
                                </div>
                                <div class="col-lg-2">
                                    <label>A cuenta</label>
                                    <input class="form-control" name="a_cuenta" id="a_cuenta" type="number" min="500" value="<?php if(isset($mtto)) echo $mtto->a_cuenta; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Tipo de mtto.</label>
                                    <select id="tipo" name="tipo" class="form-select">
                                        <option <?php if(isset($mtto->tipo) && $mtto->tipo=="0") echo "selected"; ?> value="0">Preventivo</option>
                                        <option <?php if(isset($mtto->tipo) && $mtto->tipo=="1") echo "selected"; ?> value="1">Correctivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><br></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>Descripción</label>
                                    <textarea class="form-control" name="descrip" id="motivo" rows="4"><?php if(isset($mtto)) echo $mtto->descrip; ?></textarea>
                                </div>
                            </div>
                        </form> 
                    </div>

                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12">
                            <hr><label><h2>EVIDENCIA</h2></label>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-md-6">
                                <input class="form-control" type="file" name="file" id="file">
                            </div>
                        </div>
                    </div>
                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12" id="cont_evide">
                            <br><br>
                            <div class="controls" id="cont_imgs">
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button type="button" class="btn btn-primary" id="savesm" style="width: 60%">Guarda Mtto.</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>


<div class="modal fade" id="modal_cli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="color: #1770aa;" class="modal-title" id="exampleModalLabel">Agregar cliente</h3>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-md-12">
                            <form id="form_registro_cli" method="post">
                                <input type="hidden" id="id_climodal" name="id">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Nombre del cliente</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" type="text" id="nombre" name="nombre">
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Correo electrónico</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" type="email" name="correo" id="correo" autocomplete="nope">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Celular</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" type="number" name="celular" id="celular" autocomplete="nope">
                                            </div>   
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Calle y número</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" type="text" name="calle">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Código postal</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" type="text" name="cp" id="codigo_postal" oninput="cambiaCP2()">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Colonia</label>
                                            <div class="col-lg-5">
                                                <select class="form-control" id="colonia" name="colonia" required>
                                                    <option value="0" selected disabled>Seleccione</option>
                                                    
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Estado</label>
                                            <div class="col-lg-5">
                                                <select name="estado" id="estado" class="form-control">
                                                  <?php foreach ($estadorow->result() as $item) { ?>
                                                  <option value="<?php echo $item->id;?>"><?php echo $item->estado;?></option>
                                                  <?php } ?> 
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Municipio</label>
                                            <div class="col-lg-5"><input class="form-control" type="text" name="municipio">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="savecli" style="width: 30%">Guardar</button>
                <button class="btn btn-info" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>