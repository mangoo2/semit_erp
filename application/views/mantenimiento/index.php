<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
    .img_icon{
        width: 36px;
    }

    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    .kv-file-remove{
        display: none;
    }
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <input type="hidden" id="id_last" value="<?php echo $id_last; ?>">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="search" placeholder="Buscar mantenimiento" id="searchtext" oninput="search()">

                    </div>
                </div>
            </div>
            <div class="col-lg-7"></div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Mantenimientos/solicitud" class="btn btn-primary">Nuevo registro</a>
            </div>
        </div>  
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">    
                    <div class="col-lg-12"><h3 style="color: #1770aa;">Listado de mantenimientos externos</h3></div>
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th width="4%">Id</th>
                                    <th width="13%">Cliente</th>
                                    <th width="12%">Fecha y hora</th>
                                    <th width="15%">Descripción</th>
                                    <th width="6%">Marca</th>
                                    <th width="6%">Modelo</th>
                                    <th width="6%">Serie</th>
                                    <th width="9%">Sucursal solicita</th>
                                    <th width="9%">Estatus</th>
                                    <th width="20%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modalOrden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de orden de trabajo</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input class="form-control" type="hidden" id="id_mttoord">
                        <input class="form-control" type="hidden" id="id_ordeng" value="0">
                        <div class="col-md-5">
                            <label>ID Mtto.</label>
                            <input class="form-control" type="text" id="id_mtto_show" disabled>
                        </div>
                        <div class="col-md-5">
                            <label>Fecha de Orden</label>
                            <input class="form-control" type="date" id="fecha_orden">
                        </div>
                    </div>
                    <div class="col-lg-12"><br></div>
                    <div class="row g-3">
                        <div class="col-md-12">
                          <label>Observaciones</label>
                          <textarea cols="5" class="form-control" id="observ"></textarea>
                        </div>
                      </div>
                </div>
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-6">
                            <label>Nombre</label>
                            <input type="text" class="form-control" id="nom_solicita" name="nom_solicita">
                        </div>
                        <div class="col-lg-6">
                            <label>Dirección</label>
                            <input type="text" class="form-control" id="dir_solicita" name="dir_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-4">
                            <label>Colonia</label>
                            <input class="form-control" id="col_solicita" name="col_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>Celular</label>
                            <input type="text" class="form-control" id="cel_solicita" name="cel_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>Tel. Casa</label>
                            <input type="text" class="form-control" id="tel_solicita" name="tel_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-4">
                            <label>Email</label>
                            <input type="email" class="form-control" id="mail_solicita" name="mail_solicita">
                        </div>
                        <div class="col-lg-4">
                            <label>RFC</label>
                            <input type="text" class="form-control" id="rfc_solicita" name="rfc_solicita">
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-lg-6">
                            <label>Contacto</label>
                            <input type="text" class="form-control" id="nom_contacto" name="nom_contacto">
                        </div>
                        <div class="col-lg-4">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" id="tel_contacto" name="tel_contacto">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                <div>
                    <button type="button" class="btn btn-success save_orden">Generar Orden</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pays" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Estatus de pago</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <input type="hidden" id="idrc" value="0">
                    <div class="row g-3">
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="modal-title">Cliente:</h5> 
                            </div>
                            <div class="col-md-4">
                                <span id="cliname"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="modal-title">Marca: </h5>
                            </div>
                            <div class="col-md-4">
                                <span id="marca"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="modal-title">Modelo: </h5>
                            </div>
                            <div class="col-md-4">
                                <span id="modelo"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="modal-title">Serie: </h5>
                            </div>
                            <div class="col-md-4">
                                <span id="serie"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5 class="modal-title">A cuenta: </h5>
                            </div>
                            <div class="col-md-4">
                                <span id="a_cuenta"></span>
                            </div>
                        </div>        
                        <hr>
                    </div>

                    <div id="cont_pay" class="col-md-12 mx-auto">
                        <h3 style="color: #1770aa; text-align: center;" id="status_serv">Sin pago de servicio relacionado, generar pago en pantalla de ventas</a></h3>
                    </div>
                </div>    
            </div>    
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInsumos" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Insumos utlizados en servicio</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input type="hidden" id="id_mttoadd" value="0">
                        <input type="hidden" id="id_vtaadd" value="0">
                        <div class="col-lg-12">
                            <h5 class="modal-title" id="exampleModalLabel"><b>Asignación insumos</b></h5>
                        </div>
                        <div class="col-lg-11" id="cont_search">
                            <div class="input-group select_option_search mb-3">
                                <select id="sel_insumo" class="form-select btn-pill digits">
                                    <option>Búsqueda de insumo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12"> 
                            <p><br><br><br></p>
                        </div>
                        <div class="row g-3">
                            <div class="row">
                                <div class="col-md-2">
                                    <h5 class="modal-title">Servicio:</h5> 
                                </div>
                                <div class="col-md-8">
                                    <span id="serv_name"></span>
                                </div>
                            </div>  
                            <hr>
                        </div>
                        <div class="col-lg-12"> 
                            <table class="table display striped" cellspacing="0" id="table_insumos"> 
                                <thead> 
                                    <tr> 
                                        <th>Cantidad</th> 
                                        <th>Insumo</th> 
                                        <th></th> 
                                    </tr> 
                                </thead> 
                                <tbody class="det_ins_serv">
                                    
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <div id="cont_save">
                        <button class="btn btn-success add_insumos" type="button" onclick="addInsumos_save()">Aceptar</button>
                    </div>
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>                   
                </div>   
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Comentarios del servicio</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input type="hidden" id="id_mtto_det" value="0">
                        <div class="row">
                            <div class="col-lg-12">
                                <label>Comentarios:</label>
                                <textarea readonly class="form-control" id="coment_det" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>                   
                </div>   
            </div>
        </div>
    </div>
</div>
