<style type="text/css">
  .columss_in_1,.columss_in_2,.columss_in_3{
    display: block;
    float: left;
    margin-right: 8px;
    height: 19.5px;
  }
  .columss_in_1{
    min-width: 90px;
    font-size: 11px;
    background-color: #e9f0ee;
  }
  .columss_in_2{
    min-width: 500px;
    font-size: 11px;
    background-color: #e9f0ee;
  }
  .columss_in_3{
    min-width: 80px;
    font-size: 11px;
    background-color: #e9f0ee;
  }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
      <div class="col-lg-6">
        <label>Equipo</label>
        <select class="form-control" id="id_equipo">
        </select>
      </div>
      <div class="col-md-3">
        <label>Sucursal</label>
        <select class="form-control" id="id_sucursal">
          <option value="0">Seleccionar</option>
          <?php foreach ($sucrow->result() as $x){ if($x->id==$this->session->userdata('sucursal')) $sel="selected"; else $sel=""; ?>
            <option <?php echo $sel; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-lg-3">
        <label>Serie</label>
        <select disabled class="form-control" id="id_serie">
        </select>
      </div>
    </div>
    <div class="row"><div class="col-lg-12"><br></div></div>
    <div class="row">
      <div class="col-lg-3">
        <label>Fecha inicio</label>
        <input class="form-control" type="date" id="fechai">
      </div>
      <div class="col-lg-3">
        <label>Fecha fin</label>
        <input class="form-control" type="date" id="fechaf" value="<?php echo date("Y-m-d"); ?>">
      </div>
      <div class="col-lg-1" align="right"> 
        <label form="exportExcel">Exportar Kardex</label>
        <button id="exportExcel" class="btn" type="button"><img style="width: 38px;" src="<?php echo base_url() ?>public/img/excel.png"></button>
      </div>    
    </div>
    <div class="row">
      <div class="col-lg-12"><br></div>   
    </div>
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"><h3 style="color: #1770aa;">Kardex de mantenimientos a equipos internos</h3><br></div>
          <div class="col-lg-12">
            <table class="table table-sm" id="table_kardex" style="width:100%">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Código / Equipo</th>
                  <th scope="col">Serie</th>
                  <th scope="col">Fecha Termino</th>
                  <th scope="col">Tipo movimiento</th>
                  <th scope="col">Prox. Mtto.</th>
                  <th scope="col">Observaciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>
