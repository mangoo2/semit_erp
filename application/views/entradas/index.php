<style type="text/css">
    .error {
        margin: 0px;
        color: red;
    }

    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    
    #table_datos_filter{
        display: none;
    }
    .expand-button {
        position: relative;
    }
    .tr_det_acc{
        display: none;
    }
    .foc{ border:#00FF00 solid; border-radius: 30px !important; }
</style>

<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <h3>Listado de entradas
        </h3>
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group row">
                            <div class="col-12">
                                <label style="color: white">.</label>
                                <input class="form-control" type="search" placeholder="Buscar entrada" id="searchtext" oninput="search()">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"><label style="color: white">.</label>
                        <select class="form-control" id="estatus" onchange="reload_registro()">
                            <option value="0">Todas</option>
                            <option value="1" selected>Pendiente de ingreso</option>
                            <option value="2">OC Ingresada</option>
                            <option value="3">No recibido</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group row">
                            <div class="col-12">
                                <div >
                                    <label>Fecha inicio</label>
                                    <input class="form-control" type="date" placeholder="Buscar entrada" id="fe1" oninput="reload_registrox()">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group row">
                            <div class="col-12">
                                <div >
                                    <label>Fecha fin</label>
                                    <input class="form-control" type="date" placeholder="Buscar entrada" id="fe2" oninput="reload_registrox()">
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group row">
                            <div class="col-12">
                                <label style="color: white">.</label>
                                <input class="form-control" type="search" placeholder="Buscar por OC, escanee código de orden" id="searchcode" onchange="searchOC_Code()">
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="row">    
                    <div class="col-lg-12">

                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th scope="col">Folio</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Fecha y hora</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">Código de proveedor</th>
                                    <th scope="col">Productos</th>
                                    <th scope="col">Factura</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    </div>
    <!--end::Entry-->
</div>


<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalle de Productos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="style_prods1">
        </div>
        <div class="row">
            <div class="row col-md-12">
                <div class="col-md-6">
                    <h3 id="name_prov">Proveedor:</h3>
                </div>
                <div class="col-md-6">
                    <h3 id="fecha_oc">Fecha de compra:</h3>
                </div>
                <table class="table table-sm" style="width: 100%">
                    <thead>
                        <tr>
                            <th colspan="2">Producto</th>
                            <th>Cantidad</th>
                            <th>Precio U</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalingreso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso de Productos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="style_prods">
        </div>
        <div class="html_scr">
        </div>
        <div class="row">
            <div class="row col-md-12">
                <input type="hidden" class="form-control" id="id_oc_ed">
                <div class="col-md-6">
                    <h3 id="name_provi">Proveedor:</h3>
                </div>
                <div class="col-md-6">
                    <h3 id="fecha_oci">Fecha de compra:</h3>
                </div>
                <div class="row col-lg-10">
                    <div class="col-md-1">
                        <i class="fa fa-barcode fa-3x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-6">
                        <input placeholder="Ingrese código de producto o proveedor"  class="form-control" id="search_cod">
                    </div>
                    <div class="col-lg-2" align="center"> 
                        <img src="<?php echo base_url() ?>public/img/ctrlb_nva.png" width="220px">
                    </div>  
                </div>
                <table class="table table-sm" style="width: 100%" id="tabla_productos">
                    <thead>
                        <tr>
                            <th colspan="2">Producto</th>
                            <th>Cantidad</th>
                            <th>Precio U</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody class="table_proy"></tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>Folio de factura</label>
                <input type="text" id="factura" class="form-control" value="">
            </div>
        </div>
        
      </div>
        <div class="modal-footer">
            <!--<a title="Editar OC" onclick="modal_edit_ingre()" class="btn" type="button"><img style="width: 30px;" src="<?php echo base_url(); ?>public/img/edit.svg"></a></a>-->
            <a type="button" class="btn realizarventa btn-sm" onclick="ingresar_productos()">Aceptar</a>
            <button class="btn btn-primary" type="button" onclick="closeIngreso()" data-bs-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldispersion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <h4 align="center">¿Quieres realizar una dispersión de productos a sucursales?</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="text-align: right;">
                <a type="button" class="btn realizarventa" onclick="get_dispersion()">SI</a>
            </div>
            <div class="col-md-6">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">NO</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldispersion_cantidad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Dispersión de productos a sucursales</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="ul_tad"></div>
                <div class="tab-content" id="icon-tabContent">
                </div>    
                <!-- <h4>Resumen</h4>
                <table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro_dis_r">
                        <tr><td colspan="2">Procesando...</td></tr>
                    </tbody>
                </table> -->
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <table class="table" id="table_distribusion" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro_dis">
                        <tr><td colspan="3
                            ">Procesando...</td></tr>
                    </tbody>
                </table>
            </div>
        </div> -->
      </div>
      <div class="modal-footer">
        <a type="button" class="btn realizarventa btn-sm" onclick="confirmar_distribucion()">Confirmar</a>
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_rever" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bitácora de entradas revertidas</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-sm" style="width: 100%" id="table_bita">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>OC</th>
                                    <th>Fecha</th>
                                    <th>Usuario</th>
                                    <th>Motivo</th>
                                </tr>
                            </thead>
                            <tbody class="body_bita"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>