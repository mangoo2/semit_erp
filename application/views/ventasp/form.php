<?php 
    $estatus=0;
    foreach ($results_turno as $x){
        $estatus=$x->estatus; 
    } 
?>
<style type="text/css">
    select option[disabled] {background-color:#cecbcb}
    #form_cliente .error{margin: 2px 0 2px;}
    label.error{color: red !important;}
    input.error, select.error{border: 1px solid red !important;}
    .select_option_search {border: 2px solid #93ba1f; border-radius: 7px; background-color: #009bdb;}
    .select_option_search input{background: #009bdb; color: white; font-size: 16px;}
    #input_search{display: none;}
    .descuentos-btn{float: right;}
    .realizarventa{text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white; font-size: 15px;}
    .select_option_search .select2-container{display: none;width: 90% !important;}
    .select_option_search .select2-selection--single{background-color: #009bdb;color: white;}
    #table_formapagos td{font-size: 14px;}
    #input_barcode::placeholder{color:black;}
    #select2-input_search-container{color: white !important;}
    #select2-input_searchrent-container{color: white !important;}
    #select2-input_searchrec-container{color: white !important;}
    .select_option_cliente .select2-selection--single{background-color: #009bdb; color: white !important;}
    #select2-idrazonsocial-container{color: white !important;font-size: 18px !important;}
    #select2-preventas-container{color: white !important;font-size: 18px !important;}
    .modal-backdrop{
        z-index: 1200;
    }
    .modal{
        z-index: 1250;
    }
    @media (min-width: 992px){
        .modal-lg, .modal-xl {max-width: 1068px;}
    }
    <?php if($preventa==1 || $this->session->userdata('tipo_tecnico')=="1"){ ?>
        .ocultarparapreenta{display: none;}
    <?php } ?>
    .popover-body img{
        width: 76px;
    }
    .class_label{
        font-size: 11px; color: #93ba1f;
    }
    .alt1{
        background: url(<?php echo base_url()?>public/img/alts/alt1.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .alt2{
        background: url(<?php echo base_url()?>public/img/alts/alt2.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .alt3{
        background: url(<?php echo base_url()?>public/img/alts/alt3.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .alt4{
        background: url(<?php echo base_url()?>public/img/alts/alt4.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .alt5{
        background: url(<?php echo base_url()?>public/img/alts/alt5.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .alt6{
        background: url(<?php echo base_url()?>public/img/alts/alt6.png);
        background-size: 72%;
        min-height: 38px;
        background-repeat: no-repeat;
    }
    .popover-body .row{
        min-width: 300px;
    }
    .ocultartemporal{
        display: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: white !important;
    }
    /*.colum_in_1,.colum_in_2,.colum_in_3,.colum_in_4{
        display: block;
        float: left;
        margin-right: 8px;
        height: 19.5px;
    }
    .colum_in_1{
        min-width: 90px;
        font-size: 11px;
        background-color: #e9f0ee;
    }
    .colum_in_2{
        min-width: 650px;
        font-size: 10px;
        background-color: #e9f0ee;
    }
    .colum_in_3{
        min-width: 70px;
        font-size: 11px;
        background-color: #e9f0ee;
    }
    .colum_in_4{
        min-width: 70px;
        font-size: 11px;
        background-color: #e9f0ee;
    }*/
    .select2-results__option{
        height: 33px;
    }
    .select2_colorw{ color: white !important; }
    #select2-idrazonsocial-results li{
        font-size: 10px;
    }
    .swal-button--cancel{
        display: none;
    }
    .btn-desc{
        background: url(<?php echo base_url().'public/img/desc.svg';?>);
        width: 28px;
        height: 28px;
        background-size: 100%;
    }
    .info_de_solicitud{
        text-align: center;
        margin-top: 30px;
        color: #019cde;
    }
    .info_de_solicitud img{
        width: 50px;
    }
</style>

<input type="hidden" id="viewventa" value="0">
<input type="hidden" id="idcotizacion_cl" value="0">
<input type="hidden" id="preventa" value="<?php echo $preventa;?>">
<input type="hidden" id="perfilid" value="<?php echo $perfilid;?>">
<div class="page-body">
    <!-- Container-fluid starts-->
    <div class="container-fluid dashboard-default-sec">
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body" style="border: 2px solid #009bdb; border-radius: 13px;">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="select_option_cliente">
                            <select id="idrazonsocial" class="form-select btn-pill digits idrazonsocial_cliente" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                                <?php
                                    if(isset($rta_cli)){
                                        echo '<option data-id_cli="'.$rta_cli->id_razon.'" value="'.$rta_cli->id_razon.'" data-id_clidcli="'.$rta_cli->id_cliente.'">'.$rta_cli->nombre.'</option>';
                                    }if(isset($serv_cli)){
                                        echo '<option data-id_cli="'.$serv_cli->id_razon.'" value="'.$serv_cli->id_razon.'" data-id_clidcli="'.$serv_cli->id_cliente.'">'.$serv_cli->nombre.'</option>';
                                    }else{
                                        foreach ($get_clientegen->result() as $item) {
                                            echo '<option data-id_cli="'.$item->idcliente.'" value="'.$item->id.'">'.$item->razon_social.'</option>';
                                        } 
                                    }                                
                                ?>
                            </select>
                        </div>
                        <?php if($sucursal==2){ ?>
                        <div class="select_option_vendedor" style="display: none">
                            <br>
                            <select id="idvendedor" class="form-select btn-pill digits" onchange="validar_vendendor()" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                                <option selected=""  value="0">Seleccionar vendedor</option>
                                <?php foreach ($get_vendedores->result() as $item) {
                                    echo '<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
                                }?>
                            </select>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-2">
                        <a style="cursor: pointer;" onclick="modal_user()"><img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/64.png" title="Cliente Nuevo"></a>
                        <a style="cursor: pointer;" onclick="buscar_cliente()"><img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/63.png" title="Editar Cliente"></a>
                        <!--<a style="cursor: pointer;" onclick="modal_e_y_p()" id="modal_e_y_p"><img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/18.png" title="Existencias y Precios"></a>-->
                    </div> 
                    <div class="col-lg-3">
                        <div class="select_option_cliente">
                            <select id="preventas" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="view_presv()">
                                <option value="0">Preventas</option>
                            </select>
                        </div>
                        <br>
                        <select id="idcotizacion" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="view_cotizacion()">
                            <option value="0">Cotizaciones</option>
                            <?php foreach ($rows_cotizacion->result() as $item_cl) {
                                echo '<option value="'.$item_cl->id.'">'.$item_cl->folio.'</option>';
                            } ?>
                        </select>
                        <br>
                        <?php if($this->session->userdata('perfilid')=="1" || $this->session->userdata('perfilid')=="10" || $this->session->userdata('tipo_tecnico')=="1"
                            || $this->session->userdata('sucursal')=="2" && $this->session->userdata('perfilid')!="3" || $this->session->userdata('sucursal')!="2" && $this->session->userdata('perfilid')!="2"){ //rentas ?>
                            <select id="id_renta" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="getRentaDet()">
                                <option value="0">Rentas</option>
                                <?php if(isset($rta_cli)){
                                    echo '<option selected value="'.$rta_cli->id.'"># '.$rta_cli->id.' / '.$rta_cli->descripcion.'</option>';
                                } ?>
                            </select>
                        <?php } ?>
                        <?php if($this->session->userdata('sucursal')=="2" && $this->session->userdata('perfilid')=="3" || $this->session->userdata('sucursal')!="2" && $this->session->userdata('perfilid')=="2"){ //venta mostrador suc nps, o perfil 2 PV.venta ?>
                            <select disabled id="id_renta" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="getRentaDet()">
                                <option value="0">Rentas</option>
                                <?php if(isset($rta_cli)){
                                    echo '<option selected value="'.$rta_cli->id.'"># '.$rta_cli->id.' / '.$rta_cli->descripcion.'</option>';
                                } ?>
                            </select>
                        <?php } ?>
                        <br>
                        <?php if($this->session->userdata('perfilid')=="1" || $this->session->userdata('perfilid')=="10" || $this->session->userdata('tipo_tecnico')=="1"
                            || $this->session->userdata('sucursal')=="2" && $this->session->userdata('perfilid')!="3" || $this->session->userdata('sucursal')!="2" && $this->session->userdata('perfilid')!="2"){ //mttos ?>
                            <select id="id_mtto" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="getMttoDet()">
                                <option value="0">Mttos. disp.</option>
                                <?php if(isset($serv_cli)){
                                    echo '<option selected value="'.$serv_cli->id.'">Mtto. # '.$serv_cli->id.'</option>';
                                } ?>
                            </select>
                        <?php } ?>
                        
                    </div>   
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-2">
                        <b style="color: #019cde ">Alt + 8</b><br>
                        <label class="form-label" style="font-size: 16px; color:#009bdb; text-align: right;" >Ventas de productos</label>
                    </div>    
                    <div class="col-lg-1"><br>
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" name="tipo_salida" checked="" id="venta_prod">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div>
                    <div class="col-lg-3" style="text-align: right; display: none;">
                        <label class="form-label" style="font-size: 16px; color:#009bdb;" >Búsqueda de pre-ventas</label>
                    </div>    
                    <div class="col-lg-1" style="display: none;">
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" name="tipo_salida" class="check_pago">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div>
                    <!--<div class="col-lg-2" style="text-align: right;"> 
                        <label class="form-label" style="font-size: 16px; color:#009bdb;" >Búsqueda de rentas</label>
                    </div>    
                    <div class="col-lg-1">
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" name="tipo_salida" id="rentas" class="check_pago" disabled>
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div> -->
                    <div class="col-lg-1" style="text-align: right;">
                        <b style="color: #019cde ">Alt + 9</b><br>
                        <label class="form-label" style="font-size: 16px; color:#009bdb;" >Recargas</label>
                    </div>    
                    <div class="col-lg-1"><br>
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" name="tipo_salida" id="recargas" class="check_pago">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div>
                    <div class="col-lg-1" style="text-align: right;"><br>
                        <label class="form-label" style="font-size: 16px; color:#009bdb;" >Servicios</label>
                    </div>    
                    <div class="col-lg-1"><br>
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" name="tipo_salida" id="servicios" class="check_pago">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div>
                </div>
                <div style="font-size: 5px;"><br></div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group select_option_search mb-3" id="search_cont">
                            <input id="input_barcode" type="text" class="form-control" placeholder="Código de barras" autocomplete="nope">
                            <div class="col-lg-12" id="cont_input_search">
                                <select id="input_search" class="form-select btn-pill digits" >
                                    <option>Búsqueda de productos</option>
                                </select>
                            </div>
                            <div class="col-lg-12" id="cont_input_searchrec" style="display:none">
                                <select id="input_searchrec" class="form-select btn-pill digits" >
                                    <option>Búsqueda de recargas</option>
                                </select>
                            </div>
                            <div class="col-lg-12" id="cont_input_searchrent" style="display:none">
                                <select id="input_searchrent" class="form-select btn-pill digits" >
                                    <option>Búsqueda de rentas</option>
                                </select>
                            </div>
                            <div class="col-lg-12" id="cont_input_searchserv" style="display:none">
                                <select id="input_searchserv" class="form-select btn-pill digits" >
                                    <option>Búsqueda de servicio</option>
                                </select>
                            </div>
                            <div class="input-group-prepend" style="display: none">
                                <button class="input-group-text btn btn-primary " id="search_input_barcode"><i class="fas fa-barcode icono1" style="color: red;"></i></button>
                            </div>
                            <div class="input-group-prepend" style="display: none">
                                <button class="input-group-text btn btn-primary" id="search_input_search"><i class="fas fa-search icono2"></i></button>
                            </div>
                        </div>
                        <!--<select id="estado" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                            <option>Búsqueda de productos</option>
                        </select>-->
                    </div>   
                </div>
                <br>
                <div class="tabla_v">
                    <div class="row" style="margin-top: 70px;">
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr style="background: #009bdb;">
                                        <th scope="col" style="color: white !important;  font-size: 15px;">Cantidad</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Stock sucursal</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Precio U.</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Precio U. C/IVA</th>
                                        <th scope="col" style="color: white; font-size: 15px; display: none">Descuento</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Importe</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="table_datos_tbody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    
                <div class="tabla_r" style="display: none">
                    <div class="row" style="margin-top: 70px;">
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos_rentas">
                                <thead>
                                    <tr style="background: #009bdb;"><th style="text-align:center; color:white; font-weight:bold" colspan="11">RENTAS</th></tr>
                                    <tr style="background: #009bdb;">
                                        <th style="color: white !important;  font-size: 14px;">Cantidad</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Descripción</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Serie</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Costo</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Deposito</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Periodo</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Fecha inicio</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Entrega</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Recolección</th>
                                        <th scope="col" style="color: white; font-size: 14px;">Total</th>
                                        <th scope="col" style="color: white; font-size: 14px;"></th>
                                    </tr>
                                </thead>
                                <tbody class="table_datos_rentas_tbody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tabla_s" style="display: none">
                    <div class="row" style="margin-top: 70px;">
                        <div class="col-lg-12">
                            <table class="table table-sm table_serv" id="table_datos_servicios">
                                <thead>
                                    <tr style="background: #009bdb;">
                                        <th scope="col" style="color: white !important;  font-size: 15px;">Cantidad</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Clave</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Precio U.</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Precio U. C/IVA</th>
                                        <th scope="col" style="color: white; font-size: 15px; display: none">Descuento</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Importe</th>
                                        <th scope="col" style="color: white; font-size: 15px;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="table_datos_servicios_tbody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
                <div class="tabla_ins">
                    <div class="row" style="margin-top: 70px;">
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_insumos_list">
                                <thead>
                                    <tr style="background: #009bdb;"> 
                                        <th scope="col" style="color: white; font-size: 15px;">Cantidad</th> 
                                        <th scope="col" style="color: white; font-size: 15px;">Servicio</th> 
                                        <th scope="col" style="color: white; font-size: 15px;">Insumo(s) asignado(s)</th> 
                                        <th></th> 
                                    </tr>
                                </thead>
                                <tbody class="datos_insumos_servs">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="subtotal" id="subtotal" readonly>
                        <label><span style="color:#93ba1f"><b>Subtotal:</b></span><span style="color: black"><b class="class_subtotal">$0.00</b></span></label>
                    </div>
                    <div class="col-lg-2 div_descuentos" style="text-align: right; display: none">
                        <input type="hidden" name="descuentos" id="descuentos" readonly>
                        <label><span style="color:#93ba1f"><b>Descuento:</b></span><span style="color: black"><b class="class_descuentos">$0.00</b></span></label>
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="impuestos" id="impuestos" readonly>
                        <label><span style="color:#93ba1f"><b>Impuesto:</b></span><span style="color: black"><b class="class_impuestos">$0.00</b></span></label>
                    </div>
                </div> 
                <div class="row" >
                    <div class="col-lg-9">
                        <!-- <a style="cursor: pointer;" href="<?php // echo base_url() ?>Inicio"><img style="width: 60px" src="<?php // echo base_url() ?>public/img/ventasp/19.png" title="Inicio"></a> -->
                        <a style="cursor: pointer;" href="<?php echo base_url() ?>Inicio"><img style="width: 60px" src="<?php echo base_url() ?>public/img/pv/casa.png" title="Inicio"></a>
                        <a style="cursor: pointer;" onclick="modal_resumen_validar()"><img style="width: 60px" src="<?php echo base_url() ?>public/img/resumen.svg" title="Mis Ventas"></a>
                        <a style="cursor: pointer;" class="demo-startpop"><img style="width: 60px" src="<?php echo base_url() ?>public/img/atajos.svg"title="Atajos"></a>
                    </div>
                    <div class="col-lg-3" style="text-align: right;">
                        <input type="hidden" name="total" id="total" readonly>
                        <b style="font-size: 25px;">Total:</b> <span class="badge m-l-10" style="background: #009bdb; border-radius: 15px;font-size: 25px;"><b class="class_total">$0.00</b></span>
                    </div>
                </div>   
            </div>    
            <br>
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="card-body" style="border: 2px solid #009bdb; border-radius: 13px; padding-top: 10px;">
                        <div class="row">
                            <div class="col-lg-4 ">
                                <div class="btn-group ocultarparapreenta">
                                    <label class="form-label" style="font-size: 16px; color:#009bdb;">Venta facturada  &nbsp;</label>
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" id="venta_facturada">
                                            <span style="border: 2px solid #009bdb;"></span>
                                        </label>
                                    </span>
                                </div>
                            </div>  
                            <div class="col-lg-4">
                                <div class="btn-group ocultarparapreenta ventacredito">
                                    <label class="form-label" style="font-size: 16px; color:#009bdb;">Venta crédito  &nbsp;</label>
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" id="tipo_venta">
                                            <span style="border: 2px solid #009bdb;"></span>
                                        </label>
                                    </span>
                                </div>
                            </div>    
                            <div class="col-lg-4 ocultarparapreenta" style="text-align: right;">
                                <label style="color: black;">Selección de método de pago</label>
                            </div>    
                        </div>
                        <!-- -->
                        <div style="font-size: 5px;"><br></div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="col-md-7 ocultarparapreenta" style="float:left;">
                                    <label>Seleccione formas de pagos</label>
                                    <select class="form-control" id="select_formapagos">
                                        <?php foreach ($get_formaspagosv->result() as $item) {
                                            echo '<option value="'.$item->clave.'">'.$item->formapago_text.'</option>';
                                        }

                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-5 ocultarparapreenta" style="float:left;">
                                    <a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="addmetodo()" style="float: left;margin-top: 27px;">Agregar método</a>
                                </div>
                                
                                 
                            </div>
                            
                            <div class="col-md-5">
                                <div style="cursor: pointer; text-align: right;">
                                    <button class="realizarventa" onclick="realizarventa()"><div style="margin: 18px" class="div_realizarventa"><b>Ingresar<br>venta</b></div></button>
                                    <button class="realizarventa tipo_venta_renta_ocultar" onclick="modal_clausulas()"><div style="margin: 18px" class="div_realizarcotiza"><b>Generar<br>cotización</b></div></button>
                                </div>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="col-md-12" style="float:left;">
                                    <label>Comentarios de venta</label>
                                    <textarea rows="4" class="form-control" id="observaciones"></textarea>
                                </div>
                                <table class="table ocultarparapreenta" id="table_formapagos">
                                    <thead>
                                        <tr>
                                            <th>Forma</th>
                                            <th style="width: 130px;">Monto</th>
                                            <th ></th>
                                            <th ></th>
                                            <th ></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_formapagos"></tbody>
                                </table>
                            </div>
                            <div class="col-md-4" align="right">
                                <br>
                                <a style="cursor: pointer;" onclick="buscar_precio()" title="Solicitar Traspaso">
                                    <img style="width: 60px" src="<?php echo base_url() ?>images/TRASPA-removebg-preview.webp">
                                </a>
                                <?php if($estatus==0){ ?>
                                    <br><br>
                                    <a style="cursor: pointer;" onclick="">
                                        <img style="width: 60px" src="<?php echo base_url() ?>public/img/ventasp/65.png">
                                    </a>
                                <?php }else{ ?>
                                    <!--<br><br>
                                    <b style="color: #93ba1f; font-size: 18px;">Abierto</b>
                                    <a style="cursor: pointer;" onclick="cerrar_turno()">
                                        <img style="width: 60px" src="<?php echo base_url() ?>public/img/ventasp/66.png">
                                    </a>-->
                                <?php } ?>
                                
                                <br><br>
                                <a title="Lista de Ventas" style="cursor: pointer;" href="<?php echo base_url() ?>Listaventas">
                                    <img style="width: 60px" src="<?php echo base_url() ?>public/img/pv/23.png">
                                </a><br>
                                <div style="padding-right: 2%;"><b style="color: #019cde ">Alt + 7</b></div>
                            </div>
                            <div class="col-lg-12" >
                                <br>
                                <b style="font-size: 18px;">Cambio:</b> <span class="badge m-l-10" style="background: #009bdb; border-radius: 15px; font-size: 18px;"><b class="class_cambio">$0.00</b></span>   
                            </div>
                        </div>
                        <!-- -->
                    </div>    
                </div>
            </div>            
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modalInsumos" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Insumos ocupados para servcio</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <input type="hidden" id="id_servadd" value="0">
                        <input type="hidden" id="name_servadd" value="">
                        <input type="hidden" id="precio_servadd" value="">
                        <div class="col-lg-12">
                            <h5 class="modal-title" id="exampleModalLabel"><b>¿Desea agregar insumos utilizados en el servicio?</b></h5>

                        </div>
                        <div class="col-lg-11">
                            <div class="input-group select_option_search mb-3">
                                <select id="sel_insumo" class="form-select btn-pill digits">
                                    <option>Búsqueda de insumo</option>
                                </select>
                            </div>
                        </div>
                        <!--<div class="col-lg-1">
                            <button type="button" class="btn btn-primary" onclick="get_insumo()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                        </div>-->
                        <div class="col-lg-12"> 
                            <table class="table display striped" cellspacing="0" id="table_insumos"> 
                                <thead> 
                                    <tr> 
                                        <th>Cantidad</th> 
                                        <th></th> 
                                        <th>Insumo</th> 
                                        <th></th> 
                                    </tr> 
                                </thead> 
                                <tbody class="det_ins_serv">
                                    
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <div id="cont_save">
                        <button class="btn btn-success add_insumos" type="button" onclick="addInsumos_save()">Aceptar</button>
                    </div>
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>                   
                </div>   
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_user_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cliente nuevo</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row g-3">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="icon-tab" role="tablist">
                  <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#icon-home" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>Datos generales</a></li>
                  <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#profile-icon" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Datos fiscales</a></li>
                  <li class="nav-item"><a class="nav-link tab3" id="pagos-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#pagos-icon"'; else echo 'href="#checkTab(3)" style="cursor: no-drop;'; ?> role="tab" aria-controls="pagos-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Condiciones de pago</a></li>
                  <li class="nav-item"><a class="nav-link" id="programa-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#programa-icon"'; else echo 'href="#checkTab(4)" style="cursor: no-drop;'; ?> role="tab" aria-controls="programa-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Programa cliente frecuente</a></li>
                  <li class="nav-item"><a class="nav-link tab5" id="contact-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#contact-icon"'; else echo 'href="#checkTab(5)" style="cursor: no-drop;'; ?> role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Sesión en pagína web</a></li>
                </ul>
                
                <div class="tab-content" id="icon-tabContent">
                  <div class="tab-pane fade show active" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">
                    <br>
                    <form id="form_registro_datos" method="post">
                        <input type="hidden" id="idreg1" name="id">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nombre del cliente<span style="color: red">*</span></label>
                                    <div class="col-lg-5">
                                        <input class="form-control razon_social_n" type="text" name="nombre" id="razon_social">
                                    </div>
                                </div>
                            </div>

                        </div>    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Teléfono</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="celular" id="celular" value="">
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Correo electrónico<span style="color: red">*</span></label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()">
                                    </div>   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Calle y número</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="calle">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Código postal</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="cp" id="codigo_postal" oninput="cambiaCP2()">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Colonia<a onclick="modal_alert()" style="cursor: pointer"><img style="width: 18px;" src="<?php echo base_url() ?>images/spotlight-svgrepo-com.svg"></a></label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="colonia" name="colonia">
                                            <option value="0" selected disabled>Seleccione</option>
                                            <?php if($colonia!=''){
                                                echo '<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                                            } ?>
                                        </select>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Saldo</label>
                                    <div class="col-lg-5"><input class="form-control" type="number" name="saldo" id="saldo">
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php if($this->session->userdata('perfilid')==1){ ?>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Suspender</label>
                                        <div class="col-lg-5">
                                            <span class="switch switch-icon">
                                                <label>
                                                    <input type="checkbox" name="desactivar" id="desactivar" onclick="activar_campo()">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>   
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Suspender</label>
                                        <div class="col-lg-5">
                                            <span class="switch switch-icon">
                                                <label>
                                                    <input disabled type="checkbox" name="desactivar" id="desactivar">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>   
                                    </div>
                                <?php } ?>
                            </div>
                            <?php 
                                $motivo_sty='style="display: none;"';
                            ?>
                            <div class="col-lg-12">
                                <div class="motivo_txt" <?php echo $motivo_sty ?>>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Motivo</label>
                                        <div class="col-lg-5">
                                            <textarea class="form-control" rows="3" name="motivo"></textarea>
                                        </div>   
                                    </div>
                                </div>    
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Estado</label>
                                    <div class="col-lg-5">
                                        <select name="estado" id="estado" class="form-control">
                                          <?php foreach ($estadorow->result() as $item) { ?>
                                          <option value="<?php echo $item->id;?>"><?php echo $item->estado;?></option>
                                          <?php } ?> 
                                        </select>
                                    </div>   
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Municipio</label>
                                    <div class="col-lg-5"><input class="form-control" type="text" name="municipio">
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos()">Guardar</button>                      
                  </div>
                
                  <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
                    <br>
                    <form id="form_registro_datos_fiscales" method="post">
                        <input type="hidden" id="idreg2" name="id">

                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">Activar datos fiscales</label>
                                <span class="switch switch-icon">
                                <label>
                                    <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscales" >
                                    <span></span>
                                </label>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Razón social</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" id="razon_social_modal" name="razon_social" onchange="validar_razon_social()">
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">RFC</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="rfc" id="rfc" onchange="validar_rfc_cliente()">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">CP</label>
                                    <div class="col-lg-5">
                                        <input class="form-control" type="text" name="cp">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Régimen fiscal</label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()">
                                            <option value="0" selected disabled>Seleccione</option>
                                            <?php foreach ($get_regimen->result() as $item) {
                                                    echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>'; 
                                            }?>
                                        </select>
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Uso de CFDI</label>
                                    <div class="col-lg-5">
                                        <select class="form-control" id="uso_cfdi" name="uso_cfdi">
                                            <option value="0" selected disabled>Seleccione</option>
                                            <?php foreach ($get_cfdi->result() as $item) {
                                                echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                                            }?>
                                        </select>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos_fiscales()">Guardar</button>        
                </div>
                <div class="tab-pane fade" id="pagos-icon" role="tabpanel" aria-labelledby="pagos-icon-tab">
                    <br>
                    <form id="form_registro_pagos" method="post">
                        <input type="hidden" id="idreg4" name="id">
                        <div class="row">
                            <?php 
                                $dias_saldo1='style="display:none"'; 
                                $dias_saldo2='style="display:none"'; 
                             ?>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En días</label>
                                    <div class="col-lg-1">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input type="radio" name="dias_saldo" id="dias_saldo" value="1" onclick="tipo_pago(1)">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>   
                                    <div class="col-lg-2 diaspagotxt" <?php echo $dias_saldo1 ?>>
                                        <select class="form-control" id="diaspago" name="diaspago" required>
                                            <option value="0" selected disabled>Seleccionar días</option>
                                            <option value="15">15</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div> 
                                    <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En saldo</label>
                                    <div class="col-lg-1">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input type="radio" name="dias_saldo" id="dias_saldo" value="2" onclick="tipo_pago(2)">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>  
                                    <div class="col-lg-2 saldopagotxt" <?php echo $dias_saldo2 ?>> 
                                        <input class="form-control" type="number" id="saldopago" name="saldopago">
                                    </div>  
                                </div>
                            </div>
                        </div>    
                    </form>  
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_pagos()">Guardar</button>        
                </div>

                <div class="tab-pane fade" id="programa-icon" role="tabpanel" aria-labelledby="programa-icon-tab">
                    <br>
                    <form id="form_registro_usuario" method="post">
                        <input type="hidden" id="idreg3" name="id" value="">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">ID cliente</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="text" disabled value="">
                                    </div>   
                                </div>
                            </div> 
                        </div>    
                    </form><!-- 
                    <div align="center">
                        <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_usuario()">Guardar</button>
                    </div>  -->
                  </div>  
                  <div class="tab-pane fade" id="contact-icon" role="tabpanel" aria-labelledby="contact-icon-tab">
                    <br>
                    <form id="form_registro_usuario" method="post">
                        <input type="hidden" id="idreg3" name="id">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()">
                                    </div>   
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Contraseña</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password">
                                    </div>  
                                    <div class="col-5">
                                        <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                            <i class="icon-xl fas fa-eye" style="color: white"></i>
                                        </a>
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password">
                                    </div>   
                                </div>
                            </div> 
                        </div>    
                    </form>
                    <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_usuario()">Guardar</button>
                  </div>  
                
                </div>
                
            </div>
            <?php /* 
            <form class="form" id="form_cliente">
                <div class="col-md-12">
                    <label class="form-label">Razón social</label>
                    <input class="form-control" type="text" id="razon_social" name="razon_social" required oninput="validar_razon_social()">
                </div>
                <div class="col-md-12">
                    <label class="form-label">RFC</label>
                    <input class="form-control" type="text" name="rfc" id="rfc" required oninput="validar_rfc_cliente()">
                </div>
                <div class="col-md-12">
                    <label class="form-label">CP</label>
                    <input class="form-control" type="text" name="cp" id="cp" required>
                </div>  
                <div class="col-md-12">
                    <label class="form-label">Régimen fiscal</label>
                    <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()" required>
                        <option value="0" selected disabled>Seleccione</option>
                        <?php foreach ($get_regimen->result() as $item) {

                            echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                        }?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Uso de CFDI</label>
                    <select class="form-control" id="uso_cfdi" name="uso_cfdi" required>
                        <option value="0" selected disabled>Seleccione</option>
                        <?php foreach ($get_cfdi->result() as $item) {
                            echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>';
                        }?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Correo electrónico</label>
                    <input class="form-control" type="email" name="correo" id="correo" required>
                </div>
                <div class="col-md-12" align="center">
                    <a class="btn" style="background: #009bdb; color: white" type="button" onclick="adddir()">Agregar dirección</a>
                </div>
                <div class="col-md-12 div_form_direccion" style="display:none;">
                    <label class="form-label">Dirección</label>
                    <textarea class="form-control" id="direccion" name="direccion"></textarea>
                </div>
            </form>
            */ ?>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-bs-dismiss="modal">Cerrar</button>
        <!-- <button class="btn" style="background: #93ba1f;" type="button" id="save_form_df">Guardar</button> -->
      </div>
    </div>
  </div>
</div>
<!-- 
<style type="text/css">
    .modal-backdropsss {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1040;
        width: 100vw;
        height: 100vh;
        background-color: #000000;
    }
</style>
<div class="modal-backdropsss"
style=""></div> -->

<div class="modal fade" id="modalturno" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Abrir turno</b></h1>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <h3>Usuario: <?php echo $this->session->userdata('empleado'); ?></h3>
                <h3>Fecha y hora: <?php echo $fecha.' '.$hora ?></h3>
            </div>    
            <div class="col-md-12">
                <label style="color: #93ba1f; font-size: 18px;"><b>Monto:</b></label>
                <input class="form-control" type="number" id="monto" style="background: #019cde; color: white; font-size: 27px;"> 
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <?php if($perfilid==1 || $perfilid==2){
            ?><button class="btn" style="background: #93ba1f; color: white; width: 100%;" onclick="validar_turno()" type="button">Abrir turno </button><?php
        }else{
            ?><button class="btn" style="background: #93ba1f; color: white; width: 100%;"  type="button" disabled>Abrir turno </button><?php
        }
        ?>
        <div class="col-md-12 cont_open" style="display:none">
            <label style="color: red; font-size: 18px;"><b>TURNO PREVIO DEL DÍA YA ABIERTO Y CONCLUIDO</b></label>
        </div>
        
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalturno_cerrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Cerrar turno</b></h1>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <h3>Usuario: <?php echo $this->session->userdata('empleado'); ?></h3>
                <h3>Fecha y hora: <?php echo $fecha.' '.$hora ?></h3>
            </div>    
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn" style="background: red; color: white; width: 100%;" onclick="validar_cerrar_turno()" type="button">Cerrar turno </button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalturno_cerrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Cerrar turno</b></h1>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <h3>Usuario: <?php echo $this->session->userdata('empleado'); ?></h3>
                <h3>Fecha y hora: <?php echo $fecha.' '.$hora ?></h3>
            </div>    
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn" style="background: red; color: white; width: 100%;" onclick="validar_cerrar_turno()" type="button">Cerrar turno </button>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>

<!--<div class="modal fade" id="iframeri" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<div class="modal fade" id="iframeri" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <!--<div id="iframereporte"></div>-->
            <!--</div>-->
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button onclick="rechange()" class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalverificar" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content" >
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Solicitud de traspaso</b></h1>
      </div>
      <div class="modal-body" style="min-height: 240px;">
        <div class="row">
            <div class="row">
                <div class="col-lg-1" style="text-align: right;">
                    <label class="form-label" style="font-size: 16px; color:#009bdb;">Recargas</label>
                </div>    
                <div class="col-lg-1">
                    <span class="switch switch-icon">
                        <label>
                            <input type="checkbox" id="recargas_modal" class="check_pago">
                            <span style="border: 2px solid #009bdb;"></span>
                        </label>
                    </span>
                </div>
            </div>
            <div class="col-lg-12" id="cont_search_tras">
                <div class="input-group select_option_search mb-3">
                    <!-- <input id="input_barcodex" type="text" class="form-control" placeholder="Código de barras"> -->
                    <select id="input_searchx" class="form-select btn-pill digits" style="color: red;">
                        <option>Búsqueda de productos</option>
                    </select>
                    <!-- <div class="input-group-prepend">
                        <button class="input-group-text btn btn-primary " id="search_input_barcodex"><i class="fas fa-barcode iconox1" style="color: red;"></i></button>
                    </div> -->
                    <div class="input-group-prepend" style="display: none">
                        <button class="input-group-text btn btn-primary" id="search_input_searchx"><i class="fas fa-search iconox2"></i></button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" id="cont_rechange" style="display:none">
                    <div class="input-group select_option_search mb-3">
                        <select id="id_tanque" class="form-select btn-pill digits" >
                            <option value="0">Seleccione una opción</option>
                            <?php foreach($tanques->result() as $t){
                                echo "<option value='".$t->id."'>".$t->codigo."</option>";
                            } ?>
                        </select>
                    </div>
                </div>   
            </div>   
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="tabla_precios"></div>
                
            </div>
        </div>

      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modal_cliente" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 style="color:#019cde;"><b>Cliente</b></h1>
      </div>
      <div class="modal-body">
        <div class="row">
            <!-- -->
            <div class="col-md-4" align="center">
            </div>  
            <div class="col-md-2" align="center">
                
            </div>  
            <div class="col-md-6" align="right">
                <a type="button" class="btn realizarventa btn-sm" onclick="add_venta_select()">Utilizar para venta</a>
                <a type="button" class="btn realizarventa btn-sm" onclick="limpiar_select_cliente()">Limpiar búsqueda</a>
            </div>  
        </div>
        <br>    
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <select id="input_search_clientex" class="form-select btn-pill digits" style="background: #019cde; border-color: #019cde; color: white; width: 100%">
                        <option>Búsqueda de cliente</option>
                    </select>
                </div>
            </div>   
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="text_cliente"></div>
            </div>
        </div>

      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modalprecios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Precios por sucursal</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_precios"></div> 
              </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_porcentaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Seleccionar descuento</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" style="color: #019cde; font-size: 20px;">Seleccionar porcentaje de descuento</label>
                <select class="form-control" id="confirm_descuento">
                    <option selected="" disabled="">Seleccionar descuento</option>
                    <option value="10">%10</option>
                    <option value="5">%5</option>
                </select>
              </div>
              <div class="col-md-12" align="center">
                <a type="button" class="btn realizarventa btn-sm" onclick="get_descuento()">Confirmar</a>
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalcodigo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Verificar código</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t1" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t2').focus(); } }">
                </div>
                <div class="col-lg-2">  
                    <input type="text" name="codigo" id="codigo_t2" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t3').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t3" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t4').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t4" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t5').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t5" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" oninput="validar_codigo()" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;">
                </div>
                <div class="col-lg-12">
                    <span class="validar_codigo" style="color: red"></span>
                </div>    
            </div>    
        </div>
      </div>  
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" onclick="validar_codigo()">Aceptar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_pin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Validar Pin</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" style="color: #019cde; font-size: 20px;">Introducir PIN</label>
                <input id="input_pin" type="password" class="form-control" oninput="get_validarpin()">
              </div>
              <div class="col-md-12" align="center">
                <a type="button" class="btn realizarventa btn-sm" onclick="get_validarpin()">Confirmar</a>
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_pin_resumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Validar Pin</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" style="color: #019cde; font-size: 20px;">Introducir PIN</label>
                <input id="input_pinx" type="password" class="form-control" oninput="get_validarpinx()">
              </div>
              <div class="col-md-12" align="center">
                <a type="button" class="btn realizarventa btn-sm" onclick="get_validarpinx()">Confirmar</a>
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal_resumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Mi resumen de ventas</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-md-3">
                   <span class="">Sumatoria: </span> <a type="button" class="btn realizarventa btn-sm" onclick="get_resumen_venta_tipo()"><span class="total_resumen">$0.00</span></a>
                </div>
                <label class="col-lg-2 col-form-label" style="text-align: right;">Desde</label>
                <div class="col-lg-2"><input class="form-control" type="date" id="f1" oninput="get_resumen_venta()">
                </div>  
                <label class="col-lg-2 col-form-label" style="text-align: right;">Hasta</label>
                <div class="col-lg-2"><input class="form-control" type="date" id="f2" oninput="get_resumen_venta()">
                </div>   
              <div class="col-md-12" >
                <div class="resumen_ventatxt">
                    <table class="table table-sm" id="table_datos">
                        <thead>
                            <tr>
                                <th scope="col">Folio</th>
                                <th scope="col">Vendedor</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Método de pago</th>
                                <th scope="col">Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_resumen_total" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Desglose de métodos de pago</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12" >
                <div class="resumen_venta_tipopagotxt">
                </div>
              </div>  
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_clausulas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Nueva cotización</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
             <div class="row">
                <div class="col-md-12">
                    <table class="table" id="table_clausulas">
                        <thead>
                            <tr>
                                <th>Clausulas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="tbody_clausulas"></tbody>
                    </table>
                </div>
            </div>    
            <div class="row">
                <div class="col-md-12">
                  <label class="form-label">Correo electrónico</label>
                  <input class="form-control" type="email" id="correo_cl">
                </div>
            </div>    
        </div>    
      </div>
      <div class="modal-footer">
        <div class="row" style="width: -webkit-fill-available !important;">
            <div class="col-md-4">
                <a type="button" class="btn realizarventa btn-sm" style="width: 100%" data-bs-dismiss="modal">Cerrar</a>
            </div>
            <div class="col-md-4">
                <a type="button" class="btn realizarventa btn-sm" style="width: 100%" onclick="enviar_correo_cotizacion()">Enviar por correo</a>
            </div>
            <div class="col-md-4">
                <a type="button" class="btn realizarventa btn-sm" style="width: 100%" onclick="realizarcotizacion(0)">Generar cotización</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_notificacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">¡Atención!</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <h4>Para poder seleccionar una colonia, es necesario introducir el código postal</h4>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<?php 
    
    if($estatus!=0){ ?>
        <input type="hidden" id="estatus_turno" value="<?php echo $estatus ?>">
<?php 
    }else{ ?>
        <style type="text/css">
            .adbn-wrap {
            display: none;
            justify-content: center;
            position: fixed;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgb(0 0 0 / 50%);
            z-index: 999;
        }
        </style>

        <div class="adbn-wrap" style="display: flex; align-items: center;">
        </div>
        <input type="hidden" id="estatus_turno" value="<?php echo $estatus ?>">
    <?php }
?>
<div style="display:none;">
    <select id="select_ban">
        <option value="0">Elige una opción</option>
        <?php  foreach ($get_ban->result() as $itemb){ 
            echo '<option value="'.$itemb->id.'">'.$itemb->name_ban.'</option>';
        } ?>
    </select>
</div>


<div class="modal fade" id="modal_descuentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Solicitar descuento</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" style="padding: 10px;">
            <div class="row">
                <div class="col-md-12">
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 7%;">Cant.</th>
                                <th>Producto</th>
                                <th style="width: 12%;">C/U</th>
                                <th style="width: 12%;"></th>
                                <th style="width: 12%;"></th>
                                <th style="width: 12%;"></th><!--seleccion por o monto-->
                                <th style="width: 12%;">Precio final</th>
                            </tr>
                        </thead>
                        <tr>
                            <td class="s_cant"></td>
                            <td class="s_name"></td>
                            <td class="s_price_cu"></td>
                            <td class="">
                                <label for="tipo_descuento1">Efectivo</label>
                                <input class="" type="radio" name="tipo_descuento" id="tipo_descuento1" value="1" onchange="tipo_desc()">
                            </td>
                            <td class="">
                                <label for="tipo_descuento2">Porcentaje</label>
                                <input class="" type="radio" name="tipo_descuento" id="tipo_descuento2" value="2" onchange="tipo_desc()">
                            </td>
                            <td class="">
                                <select class="form-control" id="s_mont_por" style="display:none;" onchange="cal_por_sol()">
                                    <option value="0">Seleccione</option>
                                    <option value="5">5 %</option>
                                    <option value="8">8 %</option>
                                    <option value="10">10 %</option>
                                    <option value="12">12 %</option>
                                    <option value="15">15 %</option>
                                    <option value="18">18 %</option>
                                    <option value="20">20 %</option>
                                    <option value="25">25 %</option>
                                </select>
                                <input type="number" class="form-control" id="s_mont_efe" placeholder="$ 0.00" style="display:none;" oninput="calcular_sol_pro()">
                            </td>
                            <td class="">
                                <input type="number" class="form-control" id="s_mont_final" readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12 info_de_solicitud"></div>
                
            </div>
           
      </div>
      <div class="modal-footer">
        <!--<button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>-->
        <div class="row" style="width: 100%;">
            <div class="col-md-6 sol_solicitar">
                <button class="btn btn-primary " id="btn-soldescuento" type="button">Solicitar descuento</button>
            </div>
            <div class="col-md-6 sol_aceptar">
                
            </div>
        </div>
      </div>
    </div>
  </div>
</div>