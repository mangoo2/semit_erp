<style type="text/css">
    .error {margin: 0px;color: red;}
    .div-btn{width: 150px;}
    .clasmale:hover{color: #009bdb;}
    .if_c_g{color: #9dca19;font-weight: bold;}
    .if_c_t{color: #152342;font-weight: bold;}
    .tr_garantia{
        background-color: rgb(255, 255, 128, 0.5) !important;
    }
    #modaltimbrado .select2-container{
        width: 100% !important;
    }
    .i_idrazonsocial{
        color: red;
    }
</style>

<div class="page-body">
    <?php
        if($_SERVER['HTTP_HOST']=='localhost'){
            $rutaservertimbra='http://localhost/semit_site/Facturacion';
        }else{
            $rutaservertimbra='https://pagedemo.semit.mx/Facturacion';
        }
    ?>
    <input type="hidden" id="server" value="<?php echo $rutaservertimbra;?>">
    <input type="hidden" id="nombre_per" value="<?php echo $idpersonal ?>">
    <!-- Container-fluid starts-->
    <div class="container-fluid dashboard-default-sec">
        <h3>Listado de ventas 
          <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
          <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
        </h3>

        <div class="row">
            <?php if($this->session->userdata('perfilid')==1){ ?>
            <div class="col-lg-2">
              <select class="form-control" id="sucursalselect" onchange="loadtable()">
                <option value="0">Seleccionar</option>
                <?php
                    foreach ($get_suc as $item) {
                        echo '<option value="'.$item->id.'">'.$item->id_alias.' - '.$item->name_suc.'</option>';
                    }
                ?>
              </select>
            </div>
            <?php }else{ ?>
                <input type="hidden" id="sucursal" value="0">
            <?php } ?>
            
            <div class="col-lg-3">
              <select class="form-control" id="tipo_venta" onchange="loadtable()">
                <option value="0">Venta Normal</option>
                <option value="1">Venta Credito</option>
              </select>
            </div>
            <div class="col-lg-2">
              <select class="form-control" id="activo" onchange="loadtable()">
                <option value="1">Activo</option>
                <option value="0">Cancelado</option>
              </select>
            </div>
            <!-- <div class="col-lg-2">
              <select class="form-control">
                <option>Servicio</option>
              </select>
            </div>  -->
            <?php if($idpersonal==1){ ?>
            <div class="col-lg-2">
              <select class="form-control" id="idvendedor" onchange="loadtable()">
                <option value="0" selected="">Vendedor</option>
                <?php foreach ($get_ventas->result() as $x){
                  echo '<option value="'.$x->personalId.'">'.$x->nombre.'</option>';
                } ?>
              </select>
            </div> 
            <?php }else{ ?>
               <input type="hidden" id="idvendedor" value="0">
            <?php } ?>
            <?php if($this->session->userdata('perfilid')==1){ ?>
                <div class="col-lg-2">
                    <button type="button" id="getCorte" class="btn btn-primary">Corte General</button>
                </div>
            <?php } ?>
        </div>  
        <br>
        <!--begin::Dashboard-->
        <div class="card card-custom gutter-b">
            <div class="card-body">

                <div class="row">    
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr>
                                    <th scope="col">Folio</th>
                                    <th scope="col">Sucursal</th>
                                    <th scope="col">Vendedor</th>
                                    <th scope="col">Fecha y hora</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Monto</th>
                                    <th scope="col">Productos / Servicios</th>
                                    <th scope="col">Tipo de pago</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Factura</th>
                                    <th scope="col"></th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                
        </div>
    </div>
    <!--end::Entry-->
</div>
<div class="iframefacvent" style="display: none;">
    
</div>

<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th>Costo Unitario</th>
                            <th>Descuento</th>
                            <th>Importe</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalformaspagos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formas de pago</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 table_forma">
                
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalpagos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pagos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3">
                <input type="hidden" id="montototalventa">
                Monto <span id="mtv"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3">
                <input type="hidden" id="montototalventarestante">
                Restante <span id="mtv_r">$ 0.0</span>
            </div>
        </div>
        <form id="form_pago">
            <div class="row">
                <div class="col-md-3">
                    <label>Fecha</label>
                    <input type="date" id="pago_fecha" name="pago_fecha" class="form-control" value="<?php echo date('Y-m-d');?>" required>
                </div>
                <div class="col-md-3">
                    <label>Forma</label>
                    <select class="form-control" id="pago_formapago" name="pago_formapago">
                        <?php foreach ($get_formaspagosv->result() as $item) {
                            echo '<option value="'.$item->clave.'">'.$item->formapago_text.'</option>';
                        }?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Pago</label>
                    <input type="number" id="pago_pago" name="pago_pago" class="form-control" min="1" required>
                </div>
                <div class="col-md-3">
                    <label>Comentario</label>
                    <textarea class="form-control" id="pago_comentario" name="pago_comentario"></textarea>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="button" onclick="guardar_pago_compras()">Registrar</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table display striped" cellspacing="0" id="tablepagosg"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <th></th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody class="text_tabla_pago">
                        
                    </tbody> 
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalcorte" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Corte de ventas hasta el momento </h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="row col-md-12">
                <div class="col-lg-3">
                    <label>Sucursal</label>
                    <select class="form-control" id="id_sucursal">
                        <option value="0">Seleccionar</option>
                        <?php
                            foreach ($get_suc as $item) {
                                echo '<option value="'.$item->id.'">'.$item->name_suc.'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-1" style="margin-top: 25px;">
                    <button class="btn btn-primary" type="button" id="generarc">Generar</button>
                </div>
                <div class="col-lg-6">
                    <table class="table" id="tablepagos">
                        <tbody class="tbody_tablepagos">
                            <tr>
                                <td>Total en efectivo: </td>
                                <td class="tot_efe"></td>
                            </tr>
                            <tr>
                                <td>Total en tarjetas: </td>
                                <td class="tot_tarj"></td>
                            </tr>  
                            <tr>
                                <td>Total en cheques: </td>
                                <td class="tot_che"></td>
                            </tr>
                            <tr>
                                <td>Total en transferencias: </td>
                                <td class="tot_tra"></td>
                            </tr>
                        </tbody>
                        <tfoot id="foot_det">
                            <tr>
                                <td>Total de ventas: </td>
                                <td class="supertot"></td>
                            </tr>
                            <tr>
                                <td>Devoluciones: </td>
                                <td class="tot_devo"></td>
                            </tr>
                        </tfoot>
                    </table>           
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalgarantia" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Envíar solicitud a garantía</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <input type="hidden" id="id_venta">
                            <table class="table display striped" cellspacing="0" id="table_solicita"> 
                                <thead> 
                                    <tr> 
                                        <th>Cantidad</th> 
                                        <th>Producto</th> 
                                        <th>Costo Unitario</th>
                                        <th>Descuento</th>
                                        <th>Importe</th> 
                                        <th>Solicitar</th> 
                                    </tr> 
                                </thead> 
                                <tbody class="det_prods_garan">
                                    
                                </tbody> 
                            </table>
                        </div>
                    </div>
                    <form id="form_solicita">
                        <div class="row g-3">
                            <div class="col-lg-12">
                                <label>Motivo de solicitud</label>
                                <textarea class="form-control" name="motivo" id="motivo_solicita" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row g-3">
                            <div class="col-lg-6">
                                <label>Nombre</label>
                                <input type="text" class="form-control" id="nom_solicita" name="nom_solicita">
                            </div>
                            <div class="col-lg-6">
                                <label>Dirección</label>
                                <input type="text" class="form-control" id="dir_solicita" name="dir_solicita">
                            </div>
                        </div>
                        <div class="row g-3">
                            <div class="col-lg-4">
                                <label>Colonia</label>
                                <input class="form-control" id="col_solicita" name="col_solicita">
                            </div>
                            <div class="col-lg-4">
                                <label>Celular</label>
                                <input type="number" class="form-control" id="cel_solicita" name="cel_solicita">
                            </div>
                            <div class="col-lg-4">
                                <label>Tel. Casa</label>
                                <input type="number" class="form-control" id="tel_solicita" name="tel_solicita">
                            </div>
                        </div>
                        <div class="row g-3">
                            <div class="col-lg-4">
                                <label>Email</label>
                                <input type="email" class="form-control" id="mail_solicita" name="mail_solicita">
                            </div>
                            <div class="col-lg-4">
                                <label>RFC</label>
                                <input type="text" class="form-control" id="rfc_solicita" name="rfc_solicita">
                            </div>
                        </div>
                        <hr>
                        <div class="row g-3">
                            <div class="col-lg-6">
                                <label>Contacto</label>
                                <input type="text" class="form-control" id="nom_contacto" name="nom_contacto">
                            </div>
                            <div class="col-lg-4">
                                <label>Teléfono</label>
                                <input type="text" class="form-control" id="tel_contacto" name="tel_contacto">
                            </div>
                        </div>
                    </form>

                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12">
                            <hr><label><h2>EVIDENCIA</h2></label>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-md-8">
                                <input class="form-control" type="file" name="file" id="file">
                            </div>
                        </div>
                    </div>
                    <div class="row g-3" style="text-align:center">
                        <div class="col-lg-12" id="cont_evide">
                            <br><br>
                            <div class="controls" id="cont_imgs">
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
                    <div id="cont_save">
                        <button class="btn btn-success save_sg" type="button" onclick="saveSolicitaGarantia()">Solicitar</button>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modaltimbrado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Timbrado</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p>Captura de datos fiscales para generar factura</p>
            </div>
        </div>
        <form id="form_datos_fis_timb">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 40px;">
                    <label>Seleccione Cliente<span class="i_idrazonsocial">*</span></label>
                    <select class="form-control" id="idrazonsocial" name="idrazonsocial" required></select>
                </div>
                <div class="col-md-2"><label >Razón social</label></div>
                <div class="col-md-10"><input type="text" class="form-control" id="cdf_razon_social" name="cdf_razon_social" required readonly></div>

                <div class="col-md-2"><label >RFC</label></div>
                <div class="col-md-4"><input type="text" class="form-control" id="cdf_rfc" name="cdf_rfc" minlength="12" maxlength="13" required readonly></div>
                <div class="col-md-2"><label >Código postal</label></div>
                <div class="col-md-4"><input type="text" class="form-control" id="cdf_cp" name="cdf_cp" maxlength="5" required readonly></div>

                <div class="col-md-2"><label >Régimen fiscal</label></div>
                <div class="col-md-10">
                    <select class="form-control" id="cdf_rf" name="cdf_rf" onchange="regimefiscal()" required>
                        <option value="">Seleccione</option>
                        <?php foreach ($get_regimen->result() as $item) {                            
                            echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                        }?>
                    </select>
                </div>

                <div class="col-md-2"><label >Uso de CDFI</label></div>
                <div class="col-md-10">
                    <select class="form-control" id="cdf_uso_cfdi" name="cdf_uso_cfdi" required>
                        <option value="">Seleccione</option>
                        <?php foreach ($get_cfdi->result() as $item) {
                            
                                echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                            
                        }?>
                    </select>
                </div>
            </div>
        </form>
            <div class="row" >
                <button class="btn btn-success" id="btn-edit-df-timbre" style="margin-top:10px; width:200px;margin-left: auto;margin-right: auto;">Timbrar Factura</button>
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>