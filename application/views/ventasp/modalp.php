<!--<script src="<?php echo base_url();?>public/js/modal_e_y_p.js?v=<?php echo date('Ymd-Gis') ?>"></script>-->
<style type="text/css">
    /*.mselec .select2-container--default{
        width: 100% !important;
    }
    .class_thead{
        background: #009bdb;
        
    }
    .class_thead th{
        color: white;
    }*/
</style>
<div class="modal fade" id="modal_existencias_precios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="" id="exampleModalLabel" style="color:#019cde">Existencias y precios</h1>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body_quita">
                    <div class="row">
                        <div class="col-md-12 mselec">
                            <label>Producto</label>
                            <select id="id_select_all_pro" class="form-control"></select>
                        </div>
                        <div class="col-md-12">
                            <br><br>
                        </div>
                        <div class="col-md-12">
                            <br><hr><label><div class="col-md-12 nombre_producto_all"></div></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><b>Precio del Producto</b> </div>
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead  class="class_thead">
                                    <th></th>
                                    <th>Precio sin iva</th>
                                    <th>Precio con iva</th>
                                </thead>
                                <tbody class="tbody_lis_pre_e_y_p">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><b>Existencias por almacén</b> </div>
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead  class="class_thead">
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Existencias</th>
                                </thead>
                                <tbody class="tbody_lis_exi_e_y_p">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Mostrar almacenes solo con existencias <input type="checkbox" onchange="refreshdata()" id="soloexit"></label>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <div class="row" style="width: -webkit-fill-available !important;">
                    <div class="col-md-4">
                        <a type="button" class="btn realizarventa btn-sm" style="width: 100%" data-bs-dismiss="modal">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_apertura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="" id="exampleModalLabel" style="color:#019cde">Apertura de caja</h1>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Usuario: <?php echo $this->session->userdata('empleado'); ?></h3>
                            <h3>Fecha y hora: <?php $hora=date("H:i:s"); echo date('d/m/Y').' '.date ( 'h:i a' , strtotime($hora) ); ?></h3>
                        </div>
                        <div class="col-md-12">
                            <label>Sucursal</label>
                            <select id="id_sucu_aper" class="form-control">
                                <option value="0">Elije una sucursal</option>
                                <?php foreach ($sucursales as $x){ ?>
                                    <option value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
                                <?php } ?> 
                            </select>
                        </div>
                            
                        <div class="col-md-12">
                            <label style="color: #93ba1f; font-size: 18px;"><b>Monto:</b></label>
                            <input class="form-control" type="number" id="monto" style="background: #019cde; color: white; font-size: 27px;"> 
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <button class="btn" style="background: #93ba1f; color: white; width: 100%;" onclick="aperturar_caja()" type="button">Aperturar </button>
                <div class="col-md-12 cont_open" style="display:none">
                    <label style="color: red; font-size: 18px;"><b>TURNO PREVIO DEL DÍA YA ABIERTO Y CONCLUIDO</b></label>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="a_modal_descuentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Solicitar descuento para venta</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" style="padding: 10px;">
            <div class="row" style="font-size:14px;color: #187DE4;">
                <div class="col-md-3">Vendedor:</div>
                <div class="col-md-9 info_sol_c_ven"> </div>
                <div class="col-md-3">Sucursal:</div>
                <div class="col-md-9 info_sol_c_suc"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 7%;">Cant.</th>
                                <th>Producto</th>
                                <th style="width: 12%;">C/U</th>
                                <th style="width: 12%;"></th>
                                <th style="width: 12%;"></th>
                                <th style="width: 12%;"></th><!--seleccion por o monto-->
                                <th style="width: 12%;">Precio final</th>
                            </tr>
                        </thead>
                        <tr>
                            <td class="a_s_cant"></td>
                            <td class="a_s_name"></td>
                            <td class="a_s_price_cu"></td>
                            <td class="">
                                <label for="tipo_descuento1">Efectivo</label>
                                <input class="" type="radio" name="a_tipo_descuento" id="a_tipo_descuento1" value="1" onchange="a_tipo_desc()">
                            </td>
                            <td class="">
                                <label for="tipo_descuento2">Porcentaje</label>
                                <input class="" type="radio" name="a_tipo_descuento" id="a_tipo_descuento2" value="2" onchange="a_tipo_desc()">
                            </td>
                            <td class="">
                                <input type="number" class="form-control" id="a_s_mont_por" placeholder="%" style="display:none;"onchange="a_cal_por_sol()">
                                <input type="number" class="form-control" id="a_s_mont_efe" style="display:none;" oninput="a_calcular_sol_pro()">
                            </td>
                            <td class="">
                                <input type="number" class="form-control" id="a_s_mont_final" readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                
                
            </div>
           
      </div>
      <div class="modal-footer">
        <!--<button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>-->
        <div class="row" style="width: 100%;">
            <div class="col-md-6 sol_solicitar">
                <button class="btn btn-primary " onclick="ok_autorizar(1)" type="button">Autorizar</button>
            </div>
            <div class="col-md-6 sol_aceptar">
                <button class="btn btn-danger " onclick="ok_autorizar(0)" type="button">Rechazar</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>