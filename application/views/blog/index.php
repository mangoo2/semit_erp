
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div>
                <div class="col-lg-5"></div>  
                <div class="col-lg-3" align="right"> 
                    <a href="<?php echo base_url() ?>Blog/registro" class="btn btn-primary">Nuevo registro</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Titulo</th>
                                        <th scope="col">Articulo</th>
                                        <th scope="col">Categoría</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>