<link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<input type="hidden" id="idcategoria_aux" value="<?php echo $categoria ?>">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <form id="form_registro" method="post">
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Titulo</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="text" name="titulo" value="<?php echo $titulo ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-lg-12">
                                  <h3>Artículo</h3>
                                  <textarea id="articulo" name="articulo" cols="30" rows="10"><?php echo $articulo ?></textarea>
                                </div>
                            </div>
                            <div class="row g-3">
                                <div class="col-md-11">
                                  <div class="categoria_txt"></div>
                                </div>
                                <div class="col-md-1">
                                  <label class="form-label" style="color: white">__</label><br>
                                  <button class="btn btn-pill btn-primary" type="button" onclick="modal_categoria()"><i class="fa fa-plus"></i></button>
                                </div>  
                            </div>
                            <div class="row g-3">
                                <div class="col-md-12">
                                  <h3>Resumen</h3>
                                  <textarea id="resumen" name="resumen" cols="30" rows="10"><?php echo $resumen ?></textarea>
                                </div>
                            </div>
                            <h3>Imágenes del artículo</h3>
                            <div class="row g-3">
                                <div class="col-md-4">
                                  <input type="file" id="file" name="file"  accept="image/*">
                                </div>
                            </div>
                        </form>    
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4">
                                <button class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar registro</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<div class="modal fade" id="modalcategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Categorías</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row g-3">
          <div class="col-md-10">
            <form class="form" method="post" id="form_categoria" style="width: 100%">
              <label class="form-label">Nombre</label>
              <input type="hidden" name="id" id="idcategoria_categoria" value="0">
              <input class="form-control" type="text" name="nombre" id="nombre_categoria">
            </form>
          </div>
          <div class="col-md-2">
            <label class="form-label" style="color: white">__</label><br>
            <button class="btn btn-primary btn_categoria" type="button" onclick="add_categoria()"><i class="fa fa-save"></i></button>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm" id="table_categoria" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
        <button class="btn btn-secondary" type="button">Guardar</button>
      </div>
    </div>
  </div>
</div>