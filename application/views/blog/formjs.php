<script src="<?php echo base_url(); ?>assets/fileinput/fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assetsapp/js/editor/ckeditor/styles.js"></script>
<script src="<?php echo base_url();?>public/js/form_blog.js?v=<?php echo date('YmdGis') ?>"></script>

<script type="text/javascript">
	var base_url = $('#base_url').val();
	<?php if($file==''){?>
        $("#file").fileinput({
	        showCaption: true,
	        showUpload: false,// quita el boton de upload
	        //rtl: true,
	        allowedFileExtensions: ["jpg","png","webp","jpeg"],
	        browseLabel: 'Seleccionar documentos',
	    });
	<?php }else{ ?>	
        $("#file").fileinput({
	        showCaption: true,
	        showUpload: false,// quita el boton de upload
	        //rtl: true,
	        allowedFileExtensions: ["jpg","png","webp","jpeg"],
	        browseLabel: 'Seleccionar documentos',
	        initialPreview: [
	        '<img src="'+base_url+'uploads/blogimg/<?php echo $file ?>" class="kv-preview-data file-preview-image" >',
	        ],
	    });
	<?php } ?>	

</script>