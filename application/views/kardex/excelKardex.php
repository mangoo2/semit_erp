<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=kardex_".date("Ymd").".xls");
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table  border="1">
    <thead>
        <tr>
            <th>Código / Producto</th>
            <th>Detalles producto</th>
            <th>Fecha</th>
            <th>Sucursal</th>
            <th>Tipo</th>
            <th>Cantidad Inicial</th>
            <th>Cantidad Movimiento</th>
            <th>Cantidad Final</th>
            <th>Costo Unitario</th>
            <th>Costo Total</th>
            <th>Obj. Impuesto</th>
            <th>Detalles</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $x){
            $det='';
            if($x->tipo=="0")
                $det="De stock normal";
            if($x->tipo=="1")
                $det="<b>De serie:</b> ".$x->serie;
            if($x->tipo=="2")
                $det="<b>De lote:</b> ".$x->lote;
            if($x->tipo=="3")
                $det="Insumo";
            if($x->tipo=="4")
                $det="Refacción";

            if($x->tipo=="1" && $x->tipo_mov=="Ajuste de producto")
                $det=$x->serie;

            if($x->tipo=="r")
                $det="Oxígeno";

            $cant="";
            if($x->consulta=="3" || $x->consulta=="4" || $x->consulta=="4_2"){
                if($perfil=="1"){ //administrador
                    $cant=$x->cant_inicial."<br>".$x->cant_inicial_d;
                }else{
                    if($x->idsucursal_sale==$sucursal){
                        $cant=$x->cant_inicial;
                    }
                    if($x->idsucursal_entra==$sucursal){
                        $cant=$x->cant_inicial_d;
                    }
                }
            }else{
                $cant = $x->cant_inicial;
                if($x->consulta==0 && $x->tipo=="r"){ //compra de oxigeno
                    $cant=$x->cant_inicial." (L)";
                }
            }

            $cantidad=$x->cantidad;
            if($x->consulta==0 && $x->tipo=="r"){ //compra de oxigeno
                $cant_lit = $x->cantidad * 9500;
                $cantidad=$x->cantidad." (".$cant_lit." L)";
            }

            $cantfin="";
            if($x->consulta=="3" || $x->consulta=="4" || $x->consulta=="4_2"){
                if($perfil=="1"){ //administrador
                    $cantfin=$x->cant_final."<br>".$x->cant_final_d;
                }else{
                    if($x->idsucursal_sale==$sucursal){
                        $cantfin=$x->cant_final;
                    }
                    if($x->idsucursal_entra==$sucursal){
                        $cantfin=$x->cant_final_d;
                    }
                }
            }else{
                $cantfin = $x->cant_final;
            }
            if($x->consulta==0 && $x->tipo=="r"){ //compra de oxigeno
                $cant_lit = $x->cantidad * 9500;
                $cant_tan = round($x->cant_inicial / 9500, 2);
                $cant_tanq_fin = $cant_lit + $x->cant_inicial;
                $cantfin=$x->cantidad + $cant_tan." (".$cant_tanq_fin." L)";
            }
            $costo=0;
            if($x->iva_comp>0 || $x->iva>0){
                $precio=$x->precio_unitario*1.16;
                
                if($x->consulta!=0 && $x->tipo!="r"){
                    $precio = round(($precio * 100),2) / 100;
                }
            }else{
                $precio=$x->precio_unitario;
            }
            $costo = $precio;
            $subt = $precio * $x->cantidad;
            if($x->consulta==0 && $x->tipo=="r"){
                $subt = $precio * 9500 * $x->cantidad;
            }

            $iva="";
            if($x->incluye_iva==1){
                if($x->iva>0){
                    $iva = "002 - IVA 16%";
                }else{
                    $iva = "001 - IVA 0%"; 
                }
            }else{
                $iva="EXENTO";
            }
            $txt_mov;
            $det2="";
            if($x->factura!=""){
                $det2="Factura: <b>".$x->factura." ".$x->destino."</b>";
            }else{
                $det2="<b>".$x->destino."</b>";
            }
            if($x->tipo_mov=="Venta"){
                $det2=$x->destino;
            }
            $det_mov="";
            if($x->tipo_mov=="Ajuste de producto"){ //falta ajustar este dato cuando el modulo de ajustes esté listo, tambien en excel
                if($x->tipo==0 || $x->tipo==3 || $x->tipo==4){
                    $det_mov="Cant. inicial: ".$x->distribusion."<br>Cant. Mov: ".$x->cantidad;
                }
                if($x->tipo==1){
                    $det_mov=$x->serie;
                }
                if($x->tipo==2){
                    $det_mov="Cant. inicial: ".$x->distribusion."<br>Cant. Mov: ".$x->cantidad;
                }
            }
            $det_mov=$det2."<br>".$det_mov;
         ?>
        <tr>
            <td>&nbsp;<?php echo $x->idProducto;?>&nbsp;</td>
            <td><?php echo $det; ?></td>
            <td><?php echo $x->reg; ?></td>
            <td><?php echo $x->name_suc; ?></td>
            <td><?php echo $x->tipo_mov." <b>".$x->id."</b>"; ?></td>
            <td><?php echo $cant; ?></td>
            <td><?php echo $cantidad; ?></td>
            <td><?php echo $cantfin; ?></td>
            <td><?php echo '$'.number_format($precio,5,'.',',') ?></td>
            <td><?php echo '$'.number_format($subt,5,'.',',') ?></td>
            <td><?php echo $iva; ?></td>
            <td><?php echo $det_mov; ?></td>
        </tr>  
        <?php } ?>
    </tbody>
</table>
