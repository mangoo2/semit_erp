<style type="text/css">
  .status_tipo{
    width: 180px;
    border: 1px solid #00000096;
    border-radius: 12px;
    text-align: center;
    padding: 5px 8px 5px 8px;
    background: grey;
    color: white;
    font-weight: bold;
    box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075) !important;
  }
  .status_tipo.comp{
    background: #019cde;
  }
  .status_tipo.vent{
    background: #4dc2b3;
  }
  .status_tipo.tras{
    background: #a5b306;
  }
  .status_tipo.dev{
    background: #f59595;
  }
  .status_tipo.gara{
    background: #084fbf;
  }
  .status_tipo.ins{
    background: #1dbf08;
  }
  .status_tipo.ret_garan{
    background: #084fbfc9;
  }
  .status_tipo.renta{
    background: #4dd84b;
  }
  
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <h3>Kardex de movimientos 
      <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
      <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
    </h3>
    <div class="row">
      <div class="row">
        <div class="col-lg-1">
          <label class="form-label" style="font-size: 16px; color:#009bdb;">Oxígeno</label>
        </div>    
        <div class="col-lg-1">
          <span class="switch switch-icon">
            <label>
              <input type="checkbox" id="recargas" class="check">
              <span style="border: 2px solid #009bdb;"></span>
            </label>
          </span>
        </div>

        <div class="col-lg-8"></div>
        <div class="col-lg-2" >
          <?php if($perfilid==1){ ?>
            <button class="btn btn-success exportExcel" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Excel</button>
          <?php } ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12"><br></div>
      </div>
      <div class="col-lg-6">
        <label>Producto</label>
        <select class="form-control" id="id_producto">
        </select>
      </div>
      <div class="col-lg-2">
        <label>Fecha inicio</label>
        <input class="form-control" type="date" id="fechai">
      </div>
      <div class="col-lg-2">
        <label>Fecha fin</label>
        <input class="form-control" type="date" id="fechaf" value="<?php echo date("Y-m-d"); ?>">
      </div>
      <div class="col-lg-2">
        <label>Sucursal</label>
        <select class="form-control" id="id_sucursal">
          <option value="0">Seleccionar</option>
          <?php foreach ($suc->result() as $s){
            echo '<option value="'.$s->id.'">'.$s->id_alias." - ".$s->name_suc.'</option>';
          } ?>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12"><br></div>   
    </div>
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12">
            <table class="table table-sm" id="table_kardex" style="width:100%">
              <thead>
                <tr>
                  <th scope="col">Código / Producto</th>
                  <th scope="col">Detalles producto</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Tipo</th>
                  <th scope="col">Cant. Inicial</th>
                  <th scope="col" title="cantidad">Cant. Mov.</th>
                  <th scope="col">Cant. Final</th>
                  <th scope="col">Costo Uni.</th>
                  <th scope="col">Costo Tot.</th>
                  <th scope="col">Obj. Impuesto</th>
                  <th scope="col">Detalles</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>
