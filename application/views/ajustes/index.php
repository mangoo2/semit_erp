<style type="text/css">
  .colum_ajuste_1{
    width: 120px;
    font-size: 11px;
    background-color: #e9f0ee;
  }
  .colum_ajuste_2{
    min-width: 680px;
    font-size: 11px;
    background-color: #e9f0ee;
  }
  .colum_ajuste_1,.colum_ajuste_2{
    display: block;
    float: left;
    margin-right: 8px;
    height: 19.5px;
  }
</style>
<div class="page-body">
  <!-- Container-fluid starts-->
  <div class="container-fluid dashboard-default-sec">
    <div class="row">
      <div class="form-group">
        <div class="col-lg-1">
          <label class="form-label" style="font-size: 16px; color:#009bdb;">Oxígeno</label>
        </div>    
        <div class="col-lg-1">
          <span class="switch switch-icon">
            <label>
              <input type="checkbox" id="recargas" class="check">
              <span style="border: 2px solid #009bdb;"></span>
            </label>
          </span>
        </div>
      </div>
      <div class="col-lg-8">
        <label>Producto</label>
        <select class="form-control" id="id_producto">
        </select>
      </div>
      <div class="col-lg-3">
        <label>Sucursal solicita</label>
        <select class="form-control" id="id_sucursal">
          <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==5){ ?>
            <option value="0">Seleccionar</option>
            <?php foreach ($sucs as $x){ ?>
              <option value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
          <?php } 
          }else{
            $suc = $this->session->userdata('sucursal');
            foreach ($sucrow as $x){ 
              if($x->id==$suc) 
                  echo '<option value="'.$x->id.'">'.$x->id_alias.' - '.$x->name_suc.'</option>';
            } 
          } ?>
        </select>
      </div>
      <div class="col-lg-1">
        <button id="searchProd" type="button" style="margin-top: 25px;" class="btn btn-primary"> <i class="fa fa-search" aria-hidden="true"></i></button>
      </div>

    </div>
    <div class="row">
      <div class="col-lg-12"><br></div>   
    </div>
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <div class="row">    
          <div class="col-lg-12" id="ajuste_form">
            <table class="table table-sm" id="table_inventario" style="width:100%">
              <thead>
                <tr>
                  <th scope="col">Código</th>
                  <th scope="col">Producto</th>
                  <th scope="col">Inventario</th>
                  <th scope="col">Sucursal</th>
                </tr>
              </thead>
              <tbody id="body_inventario">
              </tbody>
            </table>

            <div class="col-md-12">
              <br><hr> 
            </div>
            <div class="col-md-12 d-flex justify-content-center">
              <button class="btn btn-primary btn_save" type="button"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar ajuste</button> 
            </div>
          </div>
          <div class="col-md-12">
            <br><br><hr> 
          </div>
          <div class="col-lg-12" id="det_ajuste" style="display: none;">
            <table class="table table-sm" id="table_kardex" style="width:100%">
              <thead>
                <tr>
                  <th scope="col"># Ajuste</th>
                  <th scope="col">Código / Producto</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Detalles producto</th>
                  <th scope="col">Detalles movimiento</th>
                </tr>
              </thead>
              <tbody id="body_dets_ajuste">
              </tbody>
            </table>
          </div>

        </div>
      </div>                
    </div>
  </div>
</div>
