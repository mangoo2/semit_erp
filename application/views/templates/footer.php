        </div>
    </div>
               
        <footer class="footer" style="margin-left: 0px;">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">2024 - <?php echo date('Y') ?> Mangoo Software todos los derechos reservados. <img style="width: 84px;" src="<?php echo base_url() ?>public/img/mangoo.png"></p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand crafted & made with <i class="fa fa-heart font-secondary"></i></p>
              </div>
            </div>
          </div>
        </footer>
      

    <script src="<?php echo base_url();?>assetsapp/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url();?>assetsapp/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url();?>assetsapp/js/sidebar-menu.js"></script> 
    <script src="<?php echo base_url();?>assetsapp/js/config.js"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url();?>assetsapp/js/bootstrap/popper.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url();?>assetsapp/js/chart/chartjs/chart.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/chart/chartist/chartist.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/chart/chartist/chartist-plugin-tooltip.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/chart/knob/knob.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/chart/apex-chart/apex-chart.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/chart/apex-chart/stock-prices.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/prism/prism.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/clipboard/clipboard.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/counter/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/counter/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/counter/counter-custom.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/custom-card/custom-card.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/owlcarousel/owl.carousel.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/owlcarousel/owl-custom.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/dashboard/dashboard_2.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/sweet-alert/sweetalert.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assetsapp/js/datatable/datatables/datatable.custom.js"></script>
    <script src="<?php echo base_url();?>public/js/select2.full.min.js"></script>
    <script src="<?php echo base_url();?>public/plugins/toastr/toastr.min.js"></script>
    <!-- Plugins JS Ends-->
    
    <script src="<?php echo base_url(); ?>assets/css/cdn.datatables.net_responsive_2.4.1_js_dataTables.responsive.min.js?v2022" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>assets/css/cdn.datatables.net_rowreorder_1.3.3_js_dataTables.rowReorder.min.js?v2022" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/loading/jquery.loading.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/loading/demo.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loader/jquery.loader.css">
    <script src="<?php echo base_url(); ?>public/plugins/loader/jquery.loader.js"></script>

    <script src="<?php echo base_url(); ?>assets/fileinput/fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/confirm/jquery-confirm.min.js"></script>
    <!-- Theme js-->
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <script src="<?php echo base_url();?>assetsapp/js/script.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/scripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>public/js/notificacion.js?v=2<?php echo date('YmdGis') ?>"></script>

    <script type="text/javascript">
        function salir_sesion(){
            window.location.href = base_url+"index.php/Login/logout";
        }
        let lastDevicePixelRatio = window.devicePixelRatio;

        function detectZoomChange() {
            if (window.devicePixelRatio !== lastDevicePixelRatio) {
                console.log("Zoom detected!");
                $('.toggle-sidebar').click(function() {   
                  $('.main-nav').toggleClass('close_icon');
                  $('.page-main-header').toggleClass('close_icon');
                });
                lastDevicePixelRatio = window.devicePixelRatio;
            }
        }
        window.addEventListener('resize', detectZoomChange);


        $("#aperturar").on("click", function(){
            $("#modal_apertura").modal("show");
        });
    </script>
    </body>
    <!--end::Body-->
</html>
<!-------------------------------------------------------->
<?php 
    $perfilid=$this->session->userdata('perfilid');
    $idpersonal=$this->session->userdata('idpersonal');
    $tipo_venta=$this->session->userdata('tipo_venta');
    $tipo_tecnico=$this->session->userdata('tipo_tecnico');
    
    $fecha_hoy = date('Y-m-d');
    if($perfilid==3 or $perfilid==2 or $perfilid=='' or $tipo_tecnico=="1"){
        $sql_usu="SELECT * FROM `usuarios` WHERE perfilId=3 AND personalId=$idpersonal ";
        //echo $sql_usu;
        $query_usu = $this->db->query($sql_usu);
        
        foreach ($query_usu->result() as $item) {
            if($item->suc_date==$fecha_hoy){
                
                //$this->session->set_userdata('perfilid', 2);
                $_SESSION['perfilid']=2;
                //log_message('error', 'cambia a perfil 2 a ');
                //echo 'cambia perfil a';
            }else{
                if($item->suc_date!=null){
                    $sql_usuup="UPDATE usuarios SET suc_date = NULL WHERE UsuarioID = '$item->UsuarioID';";
                    $this->db->query($sql_usuup);
                }
                
                //$this->session->set_userdata('perfilid', $item->perfilid);
                $_SESSION['perfilid']=3;
                if(date("w") == 0 && $item->suc_date==null){ //si es domingo -- no cambió a perfil si es domingo porque suc_date es null en la base
                    $_SESSION['perfilid']=2;
                }
                //log_message('error', 'cambia a perfil original b ');
                //echo 'cambia perfil b';
            }
        }
    }
    if($perfilid==2 || $perfilid==3 && $tipo_venta!="1"){
    //if($perfilid!=1){ //solo admin puede entrar fuera de horario laboral
        $hora=date('G');
        //echo 'hora: '.$hora;
        $entrarec=0;
        if($hora<8){
            $entrarec=1;
        }
        if($hora>20){
            $entrarec=1;
        }

        if($entrarec==1){
            //if($hora<8 or $hora>16){
            if($perfilid!=1){ //solo admin puede entrar fuera de horario laboral
                ?>
                    <script type="text/javascript">
                        $(document).ready(function($) {
                            //$('body').loading({theme: 'dark',message: 'Fuera de horario...'});   // comentado asta que se autorize que ya se puede bloquear
                        });
                    </script>
                <?php
            }
        }
        ?>
        <style type="text/css">
            .toggle-sidebar{
              display: none;
            }
          </style>
        <?php

    }
    //include('../ventasp/modalp.php');
    //require_once('../ventasp/modalp.php');

    $data['sucursales']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
    $this->load->view('ventasp/modalp',$data);
    $this->load->view('ventasp/modalpjs');
?>

<!-------------------------------------------------------->

