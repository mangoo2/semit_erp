<!DOCTYPE html>
<html lang="es">
    <!--begin::Head-->
    <head><base href="">
        <meta charset="utf-8" />
        <link rel="icon" href="<?php echo base_url(); ?>public/img/favICONSEMIT.png" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favICONSEMIT.png" type="image/x-icon">
        <meta name="theme-color" content="#009bdb" />
        <title>SEMIT</title>
        <meta name="description" content="Updates and statistics" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/owlcarousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/prism.css">
    <!-- Plugins css Ends-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/datatables.css">
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/style.css">
    <link id="color" rel="stylesheet" href="<?php echo base_url(); ?>assetsapp/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assetsapp/css/responsive.css">

    <link href="<?php echo base_url();?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo base_url(); ?>assets/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cdn.datatables.net_responsive_2.4.1_css_responsive.dataTables.min.css?v2022">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cdn.datatables.net_rowreorder_1.3.3_css_rowReorder.dataTables.min.css?v2022">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/plugins/toastr/toastr.min.css">
    
    <link href="<?php echo base_url(); ?>assets/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <input type="hidden" id="perfil" value="<?php echo $this->session->userdata('perfilid'); ?>">
    <input type="hidden" id="tipo_tec" value="<?php echo $this->session->userdata('tipo_tecnico'); ?>">
    <input type="hidden" id="sucursal" value="<?php echo $this->session->userdata('sucursal'); ?>">
    <!--end::Head-->
    <!--begin::Body-->
    <style type="text/css">
        /*body{
            font-family: 'Helvetica', sans-serif !important;
            src: url(<?php // echo base_url(); ?>public/fonts/helvetica-255/Helvetica.ttf);
            font-size: 9pt;
        }
        @font-face {
          font-family: Helvetica;
          src: url(<?php // echo base_url(); ?>public/fonts/helvetica-255/Helvetica.ttf);
        }*/

        body {font-family: Galano, Helvetica, Arial, sans-serif !important;}
        .vd_red {color: red;}
        .vd_green {color: green;}
        label{color: #1770aa;}
        .proselected.selected {background-image: linear-gradient(177deg, #ff8d42, #ff8d42);}
        .page-wrapper .page-body-wrapper .page-body {background-color: #ffffff !important;}
        .card-body{border: 2px solid #009bdb; border-radius: 13px;}
        .swal-button {background-color: #93ba1f !important;color: #fff;border: none;box-shadow: none;font-weight: 600;font-size: 12px;padding: 0.45rem 1.75rem;margin: 0;cursor: pointer;border-radius: 0.25rem;}
        .btn-infofac{width: 20px;background-color: transparent !important;        }
        .btn-pdf,.btn-xml,.btn-ticket,.btn-retimbrar,.btn-timbrar,.btn-cancelarventa,.btn-enviofac{width: 34px;height: 36px;float: left;background-color: transparent !important;margin-left: 5px;}
        .btn-pdf{
            background: url(<?php echo base_url() ?>public/img/pdf1b.png);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-xml{
            background: url(<?php echo base_url() ?>public/img/xml2b.png);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-ticket{
            background: url(<?php echo base_url() ?>public/img/ticket.svg);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-retimbrar{
            background: url(<?php echo base_url() ?>public/img/retimbrar.svg);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-timbrar{
            background: url(<?php echo base_url() ?>public/img/timbrar.svg);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-cancelarventa{
            background: url(<?php echo base_url() ?>public/img/cancel.svg);
            background-size: 100%;background-repeat: no-repeat;
        }
        .btn-enviofac{
            background: url(<?php echo base_url() ?>public/img/email.svg);
            background-size: 100%;background-repeat: no-repeat;
        }
        .class_btn_no{float: left;}
        .clasmale{color: #152342;font-size: 29px;margin-left: 5px;}
        .row.wconf{width: 100%;}
        .inputpass{width: 10px; border: 0px; color: red !important;}
        .btn-cli-edit{width: 58px;height: 58px;background: url(<?php echo base_url().'public/img/edit.svg';?>);background-size: 56%;background-repeat: no-repeat;background-position: center;}
        .btn-cli-delete{width: 58px;height: 58px;background: url(<?php echo base_url().'public/img/trash.svg';?>);background-size: 56%;background-repeat: no-repeat;background-position: center;}

        .mselec .select2-container--default{width: 100% !important;}
        .class_thead{background: #009bdb;}
        .class_thead th{color: white;}
        .colum_in_1,.colum_in_2,.colum_in_3,.colum_in_4{display: block;float: left;margin-right: 8px;height: 19.5px;}
    .colum_in_1{min-width: 70px;font-size: 9.5px;background-color: #e9f0ee;}
    .colum_in_2{min-width: 680px;font-size: 9px;background-color: #e9f0ee;}
    .colum_in_3{min-width: 70px;font-size: 9.5px;background-color: #e9f0ee;}
    .colum_in_4{min-width: 100px;font-size: 9.5px;background-color: #e9f0ee;}
    .select2-results__option{height: 33px;    }
    .realizarventa{text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;}
    input.error,input .error,select.error,select .error{margin: 0px !important;border: 1px solid #F44336;}
    label.error,label .error{color: red !important;margin: 0px !important;}
    .jconfirm-content .row{width: 99%;}
    option:disabled {background: #a5a2a2;}
    </style>

    <body>
        <div class="loader-wrapper">
          <div class="theme-loader">    
            <div class="loader-p"></div>
          </div>
        </div>
        <div class="page-wrapper compact-wrapper" id="pageWrapper">
            <input type="password" name="" class="inputpass" autocomplete="on">
            <input class="inputpass"  autocomplete="on">
            <input type="password" style="opacity: 0;position: absolute;" autocomplete="on">
            
