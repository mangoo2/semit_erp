<?php
$color='#026934';
if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
}else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $tipo_tecnico=$this->session->userdata('tipo_tecnico');
    $foto=$this->session->userdata('foto');
    $this->fechainicio = date('Y-m-d');
    $name_suc=$this->session->userdata('name_suc');
}


echo "tipo_tecnico: ".$tipo_tecnico;
$style_b='';
$style_ocultar='';
$style_m_ocultar='';

if($perfilid==2 && $tipo_tecnico=="0"){
  $style_b='style="pointer-events: none;"';
  $style_ocultar='style="display:none !important;"';
  $style_m_ocultar='<style type="text/css">
        .main-nav{
          display: none !important;
        }
        .page-body{
          margin-left: 0px !important;
        }</style>';
}else if($perfilid==3 && $tipo_tecnico=="0"){
  $style_m_ocultar='<style type="text/css">
    .main-nav{
      display: none !important;
    }
    .page-body{
      margin-left: 0px !important;
    }
  </style>';
}

echo "style_m_ocultar: ".$style_m_ocultar;

$entrada=$this->Login_model->alerta_entrada();
$traspaso=$this->Login_model->alerta_traspaso();
?>
<style type="text/css">
  .div_notif {max-height: 50em;overflow-x: hidden;overflow-y: scroll;width: 100%;
  }
</style>
<div class="page-main-header close_icon">
  <div class="main-header-right row m-0">
    <div class="main-header-left">
      <div class="logo-wrapper"><a href="<?php echo base_url(); ?>Inicio"><img style="width: 113px;" class="img-fluid" src="<?php echo base_url();?>public/img/SEMIT.jpg" alt=""></a></div>
      <div class="dark-logo-wrapper"><a href="<?php echo base_url(); ?>Inicio"><img style="width: 113px;" class="img-fluid" src="<?php echo base_url();?>public/img/SEMIT.jpg" alt=""></a></div>

      <div class="toggle-sidebar" <?php echo $style_b ?>><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>

    </div>
    <div class="nav-right col pull-right right-menu p-0">
      <ul class="nav-menus">
        <?php 
            if($_SERVER['HTTP_HOST']=='calidad.semit.mx'){
              echo '<li style="color:red;font-size: 20px;">Ambiente de Pruebas</li>';
            }
        ?>
        <?php if($this->session->userdata('usuarioid')=="1" || $this->session->userdata('usuarioid')=="14"){ ?>
          <li>
            <h3 style="color: #009bdb; font-style: italic;"><button class="btn btn-success" type="button" id="aperturar"><b>Apertura</b></button></h3>
          </li>
        <?php } ?>
        <li>
          <h3 style="color: #009bdb; font-style: italic;"><b><?php echo $this->session->userdata('empleado') ?></b><br><span style=" font-size: 10px;"><?php echo $name_suc?></span></h3>
        </li>
        
        <?php if($perfilid==1 || $perfilid==4){ ?>
        
        <li class="onhover-dropdown">
          <div class="notification-box"><i data-feather="bell"></i><span class="dot-animated"></span></div>
          <ul class="notification-dropdown onhover-show-div">
            <li>
              <p class="f-w-700 mb-0" style="color: #009bdb;">Solicitud de Garantías<span class="pull-right badge badge-primary badge-pill"><span class="num_notifiGara"></span></span></p>
            </li>
            <div class="alertas_notifiGar div_notif"></div>
          </ul>
        </li>
        <?php } ?>

        <?php //if($perfilid!=1 && $perfilid!=6){ ?>
        <li class="onhover-dropdown">
          <div class="notification-box"><i data-feather="bell"></i><span class="dot-animated"></span></div>
          <ul class="notification-dropdown onhover-show-div">
            <li>
              <p class="f-w-700 mb-0" style="color: #009bdb;">Solicitud de Traslados<span class="pull-right badge badge-primary badge-pill"><span class="num_notifi2">0</span></span></p>
            </li>
            <div class="alertas_notifi2 div_notif"></div>
          </ul>
        </li>
        <li class="onhover-dropdown">
          <div class="notification-box"><i data-feather="bell"></i><span class="dot-animated"></span></div>
          <ul class="notification-dropdown onhover-show-div">
            <li>
              <p class="f-w-700 mb-0" style="color: #009bdb;">Solicitud de Renta<span class="pull-right badge badge-primary badge-pill"><span class="num_notifi3">0</span></span></p>
            </li>
            <div class="alertas_notifi3 div_notif"></div>
          </ul>
        </li>
        <?php //} ?>
        <?php if($perfilid==1 ){ ?>
        
        <li class="onhover-dropdown">
          <div class="notification-box_sol_des">
            <!--<i data-feather="bell"></i>-->
            <svg fill="#000000" width="800px" height="800px" viewBox="0 0 24 24" id="discount" data-name="Flat Line" xmlns="http://www.w3.org/2000/svg" class="icon flat-line"><path id="secondary" d="M20.22,9.33c-.3-.91-.1-2.08-.65-2.83S17.84,5.56,17.08,5,15.8,3.39,14.89,3.1,13,3.36,12,3.36s-2-.55-2.89-.26S7.68,4.46,6.92,5,5,5.73,4.43,6.5s-.35,1.92-.65,2.83S2.64,11,2.64,12s.86,1.79,1.14,2.67.1,2.08.65,2.83,1.73.94,2.49,1.49S8.2,20.61,9.11,20.9,11,20.64,12,20.64s2,.55,2.89.26,1.43-1.36,2.19-1.91,1.94-.72,2.49-1.49.35-1.92.65-2.83S21.36,13,21.36,12,20.5,10.21,20.22,9.33Z" style="fill: rgb(44, 169, 188); stroke-width: 2;"></path><line id="primary-upstroke" x1="9.45" y1="9.5" x2="9.55" y2="9.5" style="fill: none; stroke: rgb(0, 0, 0); stroke-linecap: round; stroke-linejoin: round; stroke-width: 2.5;"></line><line id="primary-upstroke-2" data-name="primary-upstroke" x1="14.45" y1="14.5" x2="14.55" y2="14.5" style="fill: none; stroke: rgb(0, 0, 0); stroke-linecap: round; stroke-linejoin: round; stroke-width: 2.5;"></line><path id="primary" d="M9,15l6-6m5.22.33c-.3-.91-.1-2.08-.65-2.83S17.84,5.56,17.08,5,15.8,3.39,14.89,3.1,13,3.36,12,3.36s-2-.55-2.89-.26S7.68,4.46,6.92,5,5,5.73,4.43,6.5s-.35,1.92-.65,2.83S2.64,11,2.64,12s.86,1.79,1.14,2.67.1,2.08.65,2.83,1.73.94,2.49,1.49S8.2,20.61,9.11,20.9,11,20.64,12,20.64s2,.55,2.89.26,1.43-1.36,2.19-1.91,1.94-.72,2.49-1.49.35-1.92.65-2.83S21.36,13,21.36,12,20.5,10.21,20.22,9.33Z" style="fill: none; stroke: rgb(0, 0, 0); stroke-linecap: round; stroke-linejoin: round; stroke-width: 2;"></path></svg>
            <span class="dot-animated_notificacion"></span>
          </div>
          <ul class="notification-dropdown onhover-show-div">
            <li>
              <p class="f-w-700 mb-0" style="color: #009bdb;">Solicitud de Descuentos<span class="pull-right badge badge-primary badge-pill"><span class="num_sol_des"></span></span></p>
            </li>
            <div class="alertas_sol_des div_notif"></div>
          </ul>
        </li>
        <?php } ?>
        <li>
          <a style="cursor: pointer;" onclick="modal_e_y_p()" id="modal_e_y_p"><img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/verifica.png" title="Existencias y Precios"></a>
          <!--<p><label class="form-label" style="font-size: 12px; color:#009bdb;">Alt + 5</label></p>-->
        </li>
        <li>
          <div class="mode"><i class="fa fa-moon-o"></i></div>
        </li>
        
        <li class="onhover-dropdown p-0">
          <button class="btn btn-primary-light" type="button" onclick="salir_sesion()"><a href="<?php echo base_url();?>Login/logout"><i data-feather="log-out"></i>Cerrar sesión</a></button>
        </li>
      </ul>
    </div>
    <div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
  </div>
</div>
<!-- Page Header Ends                              -->
<!-- Page Body Start-->
<div class="page-body-wrapper sidebar-icon">
  <!-- Page Sidebar Start-->
 
  <header class="main-nav close_icon" <?php echo $style_ocultar ?>>
    <?php if($perfilid!=2){ ?>
      <div class="sidebar-user text-center">
        <?php if($personalId==1){ ?>
          <img class="img-90 rounded-circle" style="height: 90px;" src="<?php echo base_url(); ?>public/img/favICONSEMIT.png">
        <?php }else{
          if($foto==''){ ?>
          <img class="img-90 rounded-circle" style="height: 90px;" src="<?php echo base_url(); ?>public/img/favICONSEMIT.png" alt="">
        <?php }else{ ?>
          <!--<img class="img-90 rounded-circle" src="<?php echo base_url();?>uploads/personal/<?php echo $foto ?>" alt="">-->
          <img class="img-90 rounded-circle" style="height: 90px;" src="<?php echo base_url(); ?>public/img/favICONSEMIT.png" alt="">
        <?php }
        } ?>
        <div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
          <h6 class="mt-3 f-14 f-w-600"><?php echo $this->session->userdata('empleado') ?></h6></a>
          <p class="mb-0 font-roboto"><?php echo $this->session->userdata('perfil') ?></p>
      </div>
      <nav>
        <div class="main-navbar">
          <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
          <div id="mainnav">           
            <ul class="nav-menu custom-scrollbar">
              <li class="back-btn">
                <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
              </li>
              <?php 
              $menu=$this->Login_model->getMenus($perfilid);
              $menu_aux=0;
              foreach ($menu as $items) { 
                  $MenuId=intval($items->MenuId);
                  //var_dump($btn_active);die;
                  if($MenuId==$btn_active){
                     $activar='active';
                  }else{
                     $activar=''; 
                  }
                  ?>

                <li class="sidebar-main-title">
                  <div>
                    <h6><?php echo $items->Nombre ?></h6>
                  </div>
                </li>
                <?php $menusub=$this->Login_model->submenus($perfilid,$items->MenuId,0);
                foreach ($menusub as $items){ 
                  $MenuId_sub=intval($items->MenusubId);        
                  if($MenuId_sub==$btn_active_sub){
                     $activar_sub='menu-item-active';
                  }else{
                     $activar_sub=''; 
                  }
               
                  if($items->tipo==0){
                    $entrada_num='';
                    if($MenuId_sub==23){
                      $entrada_num='<span style="background: #c6d420; padding: inherit; border-radius: 25px; color: #152342;" >'.$traspaso->total.'</span>';
                    }
                    if($MenuId_sub==31){
                      $entrada_num='<span style="background: #c6d420; padding: inherit; border-radius: 25px; color: #152342;" >'.$entrada->total.'</span>';
                    }
                  
                  if($this->session->userdata('perfilid')==11 && $MenuId_sub==3){ ?>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo base_url()?><?php echo $items->Pagina ?>"><i data-feather="<?php echo $items->Icon ?>"></i> <span><?php echo $items->Nombre ?></span> <?php echo $entrada_num ?></a></li>
                  <?php } if($this->session->userdata('perfilid')!=11){ ?>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo base_url()?><?php echo $items->Pagina ?>"><i data-feather="<?php echo $items->Icon ?>"></i> <span><?php echo $items->Nombre ?></span> <?php echo $entrada_num ?></a></li>
                  <?php } ?>
                  
                <?php }else{  /*?>
                    <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="<?php echo $items->Icon ?>"></i><span><?php echo $items->Nombre ?></span></a>
                      <ul class="nav-submenu menu-content">
                        <?php $menusub=$this->Login_model->submenus($perfilid,$items->MenuId,$items->MenusubId);
                        foreach ($menusub as $items){ ?>
                          <li><a href="<?php echo base_url()?><?php echo $items->Pagina ?>"><?php echo $items->Nombre ?></a></li>
                        <?php } ?>  
                      </ul>
                    </li>
              <?php  */ } 
                } 
                
                $menusub_sub=$this->Login_model->submenus_group($perfilid,$items->MenuId);
                foreach ($menusub_sub as $items){ 
                  $result_menu_sub=$this->Login_model->get_data('menu_sub','MenusubId',$items->idsubmenu);
                  foreach ($result_menu_sub as $sub){ 
                  ?>  
                    <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="<?php echo $items->Icon ?>"></i><span><?php echo $sub->Nombre ?></span></a>
                      <ul class="nav-submenu menu-content">
                        <?php $menusub=$this->Login_model->submenus($perfilid,$sub->MenuId,$sub->MenusubId);
                        foreach ($menusub as $items){ ?>
                          <li><a href="<?php echo base_url()?><?php echo $items->Pagina ?>"><?php echo $items->Nombre ?></a></li>
                        <?php } ?>  
                      </ul>
                    </li> 
                  <?php 
                  }
                }
                  $menu_aux++;
              }
              ?>
            </ul>
          </div>
          <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
      </nav>
      <?php } ?>    
  </header>
 