<?php if(isset($_GET["idcompra"])){ ?>
    <input type="hidden" id="idcompra_aux" value="<?php echo $_GET["idcompra"] ?>">     
<?php }else{ ?>
    <input type="hidden" id="idcompra_aux" value="0">     
<?php } ?>

<?php if(isset($_GET["idcomprav"])){ ?>
    <input type="hidden" id="idcompra_auxv" value="<?php echo $_GET["idcomprav"] ?>">     
<?php }else{ ?>
    <input type="hidden" id="idcompra_auxv" value="0">     
<?php } ?>

<?php if(isset($_GET["eoce"])){ ?>
    <input type="hidden" id="eoce" value="<?php echo $_GET["eoce"] ?>">     
<?php }else{ ?>
    <input type="hidden" id="eoce" value="0">     
<?php } ?>

<style type="text/css">
    select option[disabled] { background-color:#cecbcb }
    #form_cliente .error{
        margin: 2px 0 2px;
    }
    label.error{
        color: red !important;
    }
    input.error, select.error{
        border: 1px solid red !important;
    }
    .select_option_search {
        border: 2px solid #93ba1f;
        border-radius: 7px;
        background-color: #009bdb;
    }
    .select_option_search input{
        background: #009bdb; color: white; font-size: 16px;
    }
    #input_barcode{
        display: none;
    }
    .descuentos-btn{
        float: right;
    }
    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    .realizarCompra{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 12px;
    }
    .select_option_search .select2-container{
        display: none;
        width: 90% !important;
    }
    .select_option_search .select2-selection--single{
        background-color: #009bdb;
        color: white;
    }
    #table_formapagos td{
        font-size: 14px;
    }
    
    #input_barcode::placeholder{
      color:black;
    }
    
    #select2-input_search-container{
        color: white !important;
    }

    .select_option_cliente .select2-selection--single{
        background-color: #009bdb;
    }
    #select2-idrazonsocial-container{
        color: white !important;
        font-size: 18px !important;
    }

    @media (min-width: 992px){
        .modal-lg, .modal-xl {
            max-width: 1068px;
        }
    }

    .select2-container--default .select2-selection--single .select2-selection__placeholder {
        color: white !important;
    }


    thead tr th { 
        position: sticky;
        top: 0;
        z-index: 100;
        background-color: #009bdb;
    }

    .table_prods_exist { 
        height:200px;
        overflow:scroll;
    }
    input.cant_invalido{
        border: 1px solid #ff0000;
        animation: infinite resaltarcampo 2s;
    }
    @keyframes resaltarcampo {
        0%,100%{
            box-shadow: 0px 0px 6px rgba(250, 10, 10, 1);
            border: 1px solid #ff0000;
        }
        50%{
            box-shadow: 0px 0px 0px;
            border: 1px solid #E4E6EF;
        }
    }

</style>

<input type="hidden" id="viewcompra" value="<?php echo $viewcompra;?>">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="card card-custom gutter-b">
            <div class="card-body" style="border: 2px solid #009bdb; border-radius: 13px;">
                <?php if(!isset($_GET["eoce"])){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div style="cursor: pointer; text-align: left;">
                                <button id="btn_prodbe" class="realizarCompra"><div style="margin: 8px"><b>Productos con baja existencia</b></div></button>
                            </div>
                        </div> 
                        <div class="col-lg-7">
                            <div class="select_option_cliente">
                                <select id="idproveedor" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2" style="display:none;">
                            <a style="cursor: no-drop;" onclick="modal_user()">
                                <img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/64.png">
                            </a>
                            <a style="cursor: pointer;" onclick="modal_e_y_p()" id="modal_e_y_p"><img style="width: 40px" src="<?php echo base_url() ?>public/img/ventasp/18.png" title="Existencias y Precios"></a>
                        </div> 
                        <div class="col-lg-3">
                            <select id="precompras" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;" onchange="view_presv()">
                                <option value="0">Pre-compras</option>
                                <?php foreach ($rows_precompras->result() as $item_pv) {
                                    echo '<option value="'.$item_pv->id.'">'.$item_pv->folio.'</option>';
                                } ?>
                            </select>
                        </div>   
                    </div>
                <?php } ?>
                <?php if(isset($_GET["eoce"]) && $_GET["eoce"]==1){ ?>
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="select_option_cliente">
                                <select id="idproveedor" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                                    
                                </select>
                            </div>
                        </div>  
                    </div>
                <?php } ?>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <h4>Código proveedor: </h4>
                    </div>
                    <div class="col-md-2">
                        <span class="badge m-l-10" style="background: #009bdb; border-radius: 15px;font-size: 20px;"><b class="txt_codigo"></b></span>
                    </div>
                    <div class="col-md-3">
                        <h4 style="text-align: right;">Fecha de entrega:</h4>
                    </div>
                    <div class="col-md-3">
                        <input id="fecha_entrega" type="date" class="form-control"  style="background: #009bdb; color: white;" value="<?php echo $fecha ?>">
                    </div>
                </div>    
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <h4>Persona de contacto:</h4>
                    </div>
                    <div class="col-md-3">
                        <input id="contacto" type="text" class="form-control" disabled="" style="background: #009bdb; color: white;">
                    </div>
                    <div class="col-md-2">
                        <h4 style="text-align: right;">Observaciones:</h4>
                    </div>
                    <div class="col-md-5">
                         <textarea id="observaciones" class="form-control" rows="3" style="background: #009bdb; color: white;"></textarea>
                    </div>
                </div>    
                <br>
                <div class="row">
                    <div class="col-lg-1" style="text-align: left;">
                        <label class="form-label" style="font-size: 16px; color:#009bdb;">Recargas</label>
                    </div>    
                    <div class="col-lg-1">
                        <span class="switch switch-icon">
                            <label>
                                <input type="checkbox" id="recargas" class="check_rechange">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div>
                </div>
                <div style="font-size: 5px;"><br></div>
                <div class="row">
                    <div class="col-lg-12" id="cont_search">
                        <div class="input-group select_option_search mb-3">
                          
                            <input id="input_barcode" type="text" class="form-control" placeholder="Código de barras" autocomplete="nope">
                            <select id="input_search" class="form-select btn-pill digits" >
                                <option>Búsqueda de productos</option>
                            </select>
                            <div class="input-group-prepend">
                                <button class="input-group-text btn btn-primary " id="search_input_barcode"><i class="fas fa-barcode icono1" ></i></button>
                            </div>
                            <div class="input-group-prepend">
                                <button class="input-group-text btn btn-primary" id="search_input_search"><i class="fas fa-search icono2" style="color: red;"></i></button>
                            </div>
                        </div>
                        <!--<select id="estado" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                            <option>Búsqueda de productos</option>
                        </select>-->
                    </div>   
                </div>
                <div class="row">
                    <div class="col-lg-12" id="cont_rechange" style="display:none">
                        <div class="input-group select_option_search mb-3">
                            <select id="id_tanque" class="form-select btn-pill digits" >
                                <option value="0">Seleccione una opción</option>
                                <?php foreach($tanques->result() as $t){
                                    echo "<option value='".$t->id."'>".$t->codigo."</option>";
                                } ?>
                            </select>
                        </div>
                    </div>   
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-sm" id="table_datos">
                            <thead>
                                <tr style="background: #009bdb;">
                                    <th scope="col" style="color: white; font-size: 15px;">Código de producto</th>
                                    <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>
                                    <th scope="col" style="color: white !important;  font-size: 15px;">Cantidad</th>
                                    <th scope="col" style="color: white; font-size: 15px;">Costo</th>
                                    <!--<th scope="col" style="color: white; font-size: 15px;">Descuento</th>-->
                                    <th scope="col" style="color: white; font-size: 15px;">Importe</th>
                                    <th scope="col" style="color: white; font-size: 15px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table_datos_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" id="cont_det" style="display:none">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-2" style="text-align: center;">
                        <input type="hidden" name="subtotal" id="subtotal" readonly>
                        <label><span style="color:#93ba1f"><b>Litros:</b></span><span style="color: black"><b class="class_litros">0</b></span></label>
                    </div>
                </div> 
                <br>
                <div class="row">
                    <div class="col-lg-4">
                
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="subtotal" id="subtotal" readonly>
                        <label><span style="color:#93ba1f"><b>Subtotal:</b></span><span style="color: black"><b class="class_subtotal">$0.00</b></span></label>
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="descuentos" id="descuentos" readonly>
                        <label><span style="color:#93ba1f"><b>Descuento:</b></span><span style="color: black"><b class="class_descuentos">$0.00</b></span></label>
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="impuestos" id="impuestos" readonly>
                        <label><span style="color:#93ba1f"><b>IVA:</b></span><span style="color: black"><b class="class_impuestos">$0.00</b></span></label>
                    </div>
                    <div class="col-lg-2" style="text-align: right;">
                        <input type="hidden" name="impuesto_isr" id="impuesto_isr" readonly>
                        <label><span style="color:#93ba1f"><b>ISR:</b></span><span style="color: black"><b class="class_imp_isr">$0.00</b></span></label>
                    </div>
                </div> 
                <div class="row" style="text-align: right;">
                    <div class="col-lg-12">
                        <input type="hidden" name="total" id="total" readonly>
                        <b style="font-size: 25px;">Total:</b> <span class="badge m-l-10" style="background: #009bdb; border-radius: 15px;font-size: 25px;"><b class="class_total">$0.00</b></span>
                    </div>
                </div>   
            </div>    
            <br><br>
            <div class="row">
                <div class="col-lg-5">
                    <div class="card-body" style="border: 2px solid #009bdb; border-radius: 13px;">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-8" style="display: flex; align-items: center;">
                                        <label style="font-size: 11px; color: #93ba1f;">Eliminar partida</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <img style="width: 76px;" src="<?php echo base_url() ?>public/img/alts/alt1.png">
                                    </div>   
                                </div>
                                <div style="font-size: 5px;"><br></div>
                                <div class="row">
                                    <div class="col-lg-8" style="display: flex; align-items: center;">
                                        <label style="font-size: 11px; color: #93ba1f;" class="label_realizarventa">Realizar compra</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <img style="width: 76px;" src="<?php echo base_url() ?>public/img/alts/alt2.png">
                                    </div>  
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!--<div class="row">
                                    <div class="col-lg-8" style="display: flex; align-items: center;">
                                        <label style="font-size: 11px; color: #93ba1f;">Alta de proveedor</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <img style="width: 76px;" src="<?php echo base_url() ?>public/img/alts/alt4.png">
                                    </div>   
                                </div>-->
                                <div style="font-size: 5px;"><br></div>
                                <div class="row">
                                    <div class="col-lg-8" style="display: flex; align-items: center;">
                                        <label style="font-size: 11px; color: #93ba1f;">Guardar pre-compra</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <img style="width: 76px;" src="<?php echo base_url() ?>public/img/alts/alt5.png">
                                    </div>   
                                </div>
                            </div>    
                        </div>
                    </div> 
                    <br>
                    <?php if(!isset($_GET["eoce"])){ ?>
                        <a style="cursor: pointer;" href="<?php echo base_url() ?>Inicio">
                            <img style="width: 60px" src="<?php echo base_url() ?>public/img/ventasp/19.png">
                        </a>
                        <a style="cursor: pointer;" href="<?php echo base_url() ?>Inicio">
                            <img style="width: 60px" src="<?php echo base_url() ?>public/img/pv/casa.png">
                        </a>
                    <?php } ?>
                </div>
                <div class="col-lg-7">
                    <div class="card-body" style="border: 2px solid #009bdb; border-radius: 13px; padding-top: 10px;">
                        <div style="font-size: 5px;"><br></div>

                        <div class="row">
                            <div class="col-md-7">
                            </div>
                            
                            <div class="col-md-5">
                                <div style="cursor: pointer; text-align: right;">
                                    <?php if(!isset($_GET["eoce"])){ ?>
                                        <button class="realizarventa" onclick="guardarcompras(1)"><div style="margin: 18px" class="div_realizarventa"><b>Guardar<br>Pre-compra</b></div></button>
                                    <?php } ?>
                                    <button class="realizarventa" onclick="guardarcompras(0)"><div style="margin: 18px" class="div_realizarventa"><b>Generar<br>OC</b></div></button>
                                </div>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-md-8">
       
                            </div>
                            <?php if(!isset($_GET["eoce"])){ ?>
                                <div class="col-md-4" align="right">
                                    <br>
                                    <a style="cursor: pointer;" href="<?php echo base_url() ?>OCS">
                                        <img style="width: 60px" src="<?php echo base_url() ?>public/img/pv/23.png">
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- -->
                    </div>    
                </div>
            </div>            
        </div>
    </div>
</div>

<div class="modal fade" id="iframeri" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Editar Producto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="iframereporte"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="viewproductos()" class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_prods" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos con baja existencia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="row col-md-12">
                        <div class="col-md-8">
                            <div class="select_option_cliente">
                                <select id="idproveedor_modal" class="form-select btn-pill digits" style="background: #009bdb; color: white; font-size: 16px; border: 2px solid #93ba1f;">
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3" style="text-align: center;">
                            <label class="form-label" style="font-size: 14px; color:#009bdb;">Ver todos los prods. del proveedor</label>
                        </div>  
                        <div class="col-md-1">
                            <span class='switch switch-icon'>
                                <label>
                                    <input type='checkbox' id='all_prov' class='check' value='0'>
                                    <span style='border: 2px solid #009bdb;'></span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br><hr>
                        <table class="table table-sm table-striped table-bordered table_prods_exist" id="table_prods_exist">
                            <thead class="thead-dark">
                                <tr style="background: #009bdb;">
                                    <th scope="col" style="color: white;">Código</th>
                                    <th scope="col" style="color: white;">Descripción</th>
                                    <th scope="col" style="color: white;">Costo Compra</th>
                                    <th scope="col" style="color: white;">Precio Venta</th>
                                    <?php foreach ($sucus as $s) {
                                        echo '<th scope="col" style="color: white;">'.$s->id_alias.' - '.$s->name_suc.'</th>';
                                    } ?>
                                    <th scope="col" style="color: white;">
                                        <p>Todos</p>
                                        <span class='switch switch-icon'>
                                            <label>
                                                <input type='checkbox' id='all_prods' class='check' value='0'>
                                                <span style='border: 2px solid #009bdb;'></span>
                                            </label>
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="prodsexist_tbody">
                            </tbody>
                        </table>
                    </div>
                    <div style="cursor: pointer; text-align: left;">
                        <button id="add_prodcomp" class="realizarCompra"><div style="margin: 8px"><b>Agregar productos a compra</b></div></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="viewproductos()" class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>