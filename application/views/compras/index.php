<style type="text/css">
    .error {
        margin: 0px;
        color: red;
    }
    #table_datos_filter{
        display: none;
    }
</style>

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <h3>Listado de compras <a href="<?php echo base_url() ?>Comprasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
            </h3>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <label style="color: white">.</label>
                            <input class="form-control" type="search" placeholder="Buscar compra" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <label style="color: white">.</label>
                    <select class="form-control" id="estatus" onchange="reload_registro()">
                        <option value="0">Estatus</option>
                        <option value="1">Pendiente de ingreso</option>
                        <option value="2">OC Ingresada</option>
                        <option value="3">No recibido</option>
                        <option value="4">Pre compra</option>
                    </select>
                </div>
                <div class="col-lg-2">
                    <div class="form-group row">
                        <div class="col-12">
                            <div >
                                <label>Fecha inicio</label>
                                <input class="form-control" type="date" placeholder="Buscar entrada" id="fe1" oninput="reload_registrox()">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group row">
                        <div class="col-12">
                            <div >
                                <label>Fecha fin</label>
                                <input class="form-control" type="date" placeholder="Buscar entrada" id="fe2" oninput="reload_registrox()">
                            </div> 
                        </div>
                    </div>
                </div>
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">

                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Folio</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Fecha y hora registro</th>
                                        <th scope="col">Fecha y hora ingreso</th>
                                        <th scope="col">Proveedor</th>
                                        <th scope="col">Código de proveedor</th>
                                        <th scope="col">Monto de compra</th>
                                        <th scope="col">Productos</th>
                                        <th scope="col">Lotes y Series</th>
                                        <th scope="col">Factura</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cant.</th>
                            <th>Costo</th>
                            <th>IVA</th>
                            <th>ISR</th>
                            <th>Importe</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldeta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lotes y Caducidades</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row" id="cont_table">
                    <div class="col-md-12">
                        <table class="table table-sm" style="width: 100%" id="table_detas">
                            <thead>
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Lotes y Series</th>
                                </tr>
                            </thead>
                            <tbody class="deta_prods"></tbody>
                        </table>
                    </div>
                </div>

                <div class="row" id="cont_deta_lote" style="display: none;">
                    <div class="col-lg-1">
                        <button type="button" id="back_table" title="Regresar" class="btn btn-success"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                    </div>
                    <div class="col-lg-1">
                        <br><br><br>
                    </div>
                    
                    <div class="row col-md-12" id="cont_lotes">
                        <h5 class="modal-title">Código: <span id="cod_prod"></span></h5>
                        <input type="hidden" id="id_cd" value="0">
                        <input type="hidden" id="id_prod_com" value="0">
                        <input type="hidden" id="id_comp" value="0">
                        <input type="hidden" id="cant_comp" class="cant_compri cant_comp" value="0">               
                        <table id="table_lotes_add">
                            <tbody id="body_lotesd">
                                <tr>
                                    <td width="30%"><input type="hidden" id="id" value="0">
                                        <label>Lote</label>
                                        <input type="text" id="lote" class="form-control lotepri lote" value="">
                                    </td>
                                    <td width="30%">
                                        <label>Caducidad</label>
                                        <input type="date" id="caducidad" class="form-control caducidadpri caducidad" value="">  
                                    </td>
                                    <td width="30%">
                                        <label>Cantidad</label>
                                        <input type="number" min="1" id="cantidad" class="form-control cantpri cantidad" value="">
                                    </td>
                                    <td width="10%">
                                        <label>Agregar</label>
                                        <button type="button" id="addLote" class="btn btn-success form-control"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-1">
                        <br><br><br>
                    </div>
                    <div class="col-md-12">
                        <div class="col-lg-2 mx-auto">
                            <button type="button" id="saveLotes" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar Lotes</button>
                        </div>
                    </div>
                </div>
                <div class="row" id="cont_deta_serie" style="display: none;">
                    <div class="col-lg-1">
                        <button type="button" id="back_table2" title="Regresar" class="btn btn-success"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                    </div>
                    <div class="col-lg-1">
                        <br><br><br>
                    </div>
                    <div class="row col-md-12">
                        <input type="hidden" id="id_cd_s" value="0">
                        <input type="hidden" id="cant_comp_s" value="0">
                        <input type="hidden" id="id_prod_com_s" value="0">
                        <input type="hidden" id="id_comp_s" value="0">
                        <h5 class="modal-title">Código: <span id="cod_prod_s"></span></h5>
                        <table id="table_series_add">
                            <tbody id="body_seriesd">
                                <tr class="tr_col_sr">
                                    <td><input type="hidden" id="id" value="0">
                                        <label>Serie</label>
                                        <input type="text" id="serie" onchange="validar_serie(this.value,$(this))" class="form-control seriepri serie" value="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-1">
                        <br><br><br>
                    </div>
                    <div class="col-md-12">
                        <div class="col-lg-2 mx-auto">
                            <button type="button" id="saveSeries" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar Series</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalcodigo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Verificar código</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t1" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t2').focus(); } }">
                </div>
                <div class="col-lg-2">  
                    <input type="text" name="codigo" id="codigo_t2" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t3').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t3" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t4').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t4" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t5').focus(); } }">
                </div>
                <div class="col-lg-2">
                    <input type="text" name="codigo" id="codigo_t5" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none; border-inline: none; text-align: center; padding: 0rem 0rem !important;">
                </div>
                <div class="col-lg-12">
                    <span class="validar_codigo" style="color: red"></span>
                </div>    
            </div>    
        </div>
      </div>  
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" onclick="validar_codigo()">Aceptar</button>
      </div>
    </div>
  </div>
</div>