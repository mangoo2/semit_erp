<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar producto" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-5"></div>   
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos" style="width:100%">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Correo</th>
                                        <th scope="col">Productos</th>
                                        <th scope="col">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
