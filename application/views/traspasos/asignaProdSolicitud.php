<style type="text/css">
    .estilo_btn{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
</style>
<input type="" id="id_reg" value="0">
<input type="hidden" id="id_traspaso" value="<?php echo $id;?>">
<input type="hidden" id="id_origen" value="<?php echo $det[0]->idsucursal_sale;?>">
<input type="hidden" id="id_dest" value="<?php echo  $det[0]->idsucursal_entra; ?>">
<input type="hidden" id="id_compra" value="<?php echo  $det[0]->id_compra; ?>">

<input type="hidden" id="name_dest" value="<?php echo  $det[0]->name_sucxy; ?>">
<input type="hidden" id="name_ori" value="<?php echo  $det[0]->name_sucx; ?>">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row col-md-12"> 
            <div class="col-lg-4" align="left"> 
                <h3 style="color: #1770aa;">Asignación de prods. a solicitud de traspaso</h3>       
            </div>
            <!--<div class="col-lg-2" align="left"> 
                <img src="<?php echo base_url() ?>public/img/ctrlq_nva.png" width="300px">
            </div>
            <div class="col-lg-2"> 
            </div>
            <div class="col-lg-2" align="right"> 
                <img src="<?php echo base_url() ?>public/img/ctrlb_nva.png" width="220px">
                <img src="<?php echo base_url() ?>public/img/ctrlz_delete.png" width="260px">
            </div>-->  
    
            <div class="col-lg-6"> 
            </div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Traspasos" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body">
                <!-- todos -->  
                <div class="row">
                    <div class="col-lg-12">
                        <br>
                        <h3><span style="color:#152342">Sucursal solicitante: </span><span style="color:#93ba1f"><?php echo $det[0]->name_sucxy; ?></span></h3>
                        <h3><span style="color:#152342">Sucursal de salida: </span><span style="color:#93ba1f"><?php echo $det[0]->name_sucx; ?></span></h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-12"><br></div>
                        <div class="col-lg-12">
                            <form id="form_registro" method="post">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label>Cantidad</label>
                                        <input class="form-control" type="number" id="cantidad" min="1">
                                    </div>
                                    <div class="col-lg-10">
                                        <label>Producto</label>
                                        <select class="form-control" id="idproducto">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>   
                            </form> 
                        </div>
                    </div>
                    <div class="col-lg-12"><br></div>
                    <div class="col-lg-12">
                        <table class="table table-sm table-striped table-bordered" id="table_datos_prods">
                            <thead>
                                <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="9">PRODUCTOS</th></tr>
                                <tr>
                                    <th width="6%">Código</th>
                                    <th width="5%">Solicita</th>
                                    <th width="6%">Cantidad</th>
                                    <th width="27%">Descripción</th>
                                    <th width="30%">Detalles</th>
                                    <th width="6%">Unidad</th>
                                    <th width="7%">Origen</th>
                                    <th width="7%">Destino</th>
                                    <th width="7%"></th>
                                </tr>
                            </thead>
                            <tbody id="body_prods">
                                <?php $html=""; $cont_y=0; $cant_tot=0; $htmlsr="";
                                foreach ($det as $x){
                                    $name="";
                                    if($x->tipo==0){
                                        $codigo=$x->idProducto;
                                        $nombre=$x->nombre;
                                        if($x->tipo_prod==0){
                                            $stock=$x->stockps-$x->traslado_stock_cant;
                                            //$stock=$x->stock;
                                            $stock_ent=$x->stockps_ent-$x->traslado_stock_cant_ent;
                                        }
                                        if($x->tipo_prod==1){
                                            $nombre = $x->nombre;
                                            $stock=$x->stock_series-$x->tot_tras;
                                            //$stock_ent=$x->stock_series_ent-$x->tot_tras_ent;
                                            $stock_ent=0;
                                        }if($x->tipo_prod==2){
                                            $nombre = $x->nombre;
                                            $stock=$x->stock_lotes-$x->tras_stock_lote;
                                            $stock_ent=$x->stock_lotes_ent-$x->tras_stock_lote;
                                        }
                                    }else{
                                        $codigo=$x->codigo;
                                        $nombre=" - Recarga de oxígeno de ".$x->capacidad."L";
                                        $stock=$x->stock_recarga-$x->traslado_tanque_cant;
                                        $stock_ent=$x->stock_recarga_ent-$x->traslado_tanque_cant_ent;
                                    }
                                    if($x->tipo_prod==0 || $x->tipo_prod==1 || $x->tipo_prod==2){
                                        $tipo_prod=$x->tipo_prod;
                                    } 
                                    if($x->tipo==1){
                                        $tipo_prod=0;
                                    } 
                                    if($x->rechazado==1){
                                        $disp="block";
                                        $disabled="disabled";
                                    }else{
                                        $disp="none";
                                        $disabled="";
                                    }
                                    if($x->tipo_prod==1){
                                        $discant="disabled";
                                    }else{
                                        $discant="";
                                    }
                                    if($x->tipo_prod==1){ //serie
                                        $cantidad=1;
                                    }else{
                                        $cantidad=$x->cantidad;
                                    }
                                    $color="black";
                                    if($stock<$x->cantidad){
                                        $color="red";
                                    }
                                    $name.='<tr class="rowprod_'.$x->id_ht.'">';
                                        $name.='<td><input type="hidden" id="id_ht" value="'.$x->id_ht.'">';
                                            $name.='<input type="hidden" id="idtraspaso" value="'.$x->idtranspasos.'">';
                                            $name.='<input type="hidden" class="rechazado_'.$x->id_ht.'" id="rechazado" value="0">';
                                            $name.='<input type="hidden" id="tipo" value="'.$x->tipo.'">';
                                            $name.='<input type="hidden" id="tipo_prod" value="'.$x->tipo_prod.'">';
                                            $name.='<input type="hidden" id="idsucursal_entra" value="'.$x->idsucursal_entra.'">';
                                            $name.='<input type="hidden" id="idsucursal_sale" value="'.$x->idsucursal_sale.'">';
                                            $name.='<input type="hidden" id="id_compra" value="'.$x->id_compra.'">';
                                            $name.='<input type="hidden" id="idproducto_series" value="'.$x->idproducto_series.'">';
                                            $name.='<input type="hidden" id="id_lote" value="'.$x->idproducto_lote.'">';
                                            $name.='<input type="hidden" id="idproducto" value="'.$x->idproducto.'">';
                                            $name.='<input type="hidden" id="stockps" value="'.$stock.'">';
                                            $name.='<input type="hidden" id="stockps_entra" value="'.$stock_ent.'">';
                                            $name.=$codigo;
                                        $name.='</td>';
                                        $name.='<td>'.$x->cantidad.'</td>';
                                        $name.='<td>';
                                            $name.='<input style="color: '.$color.'" '.$disabled.' '.$discant.' type="number" class="form-control cantidad_sol cant_'.$x->id_ht.' cantidad_sol_'.$x->id_ht.'" id="cantidad_sol" value="'.$cantidad.'" onchange="changeCant('.$x->tipo.','.$stock.','.round($cantidad,0).','.$x->id_ht.','.$x->tipo_prod.')">';
                                        $name.='</td>';
                                        $name.='<td>'.$nombre.'</td>';
                                    /*if($x->tipo_prod==0){
                                        $html.='
                                        <tfoot>
                                            <tr>
                                                <td><div style="display:'.$disp.'" class="row col-lg-12 cont_rechazo_'.$x->id_ht.'">
                                                    <label>Motivo de rechazo</label>
                                                    <textarea class="form-control motivo_concep_'.$x->id_ht.'" id="motivo_rechazo_conc">'.$x->motivo_rechazo.'</textarea>
                                                </div></td>
                                            </tr>  
                                        </tfoot>';
                                    }*/
                                    $cant_tot=$cant_tot+$x->cantidad;
                                    if($x->tipo_prod==0){ //detalles
                                        $name.='<td></td>';
                                    }
                                    if($x->tipo_prod==1){ //series
                                        $name.='<td style="color:#152342"><div class="row"><div class="col-md-12">';
                                        $name.='<select onchange="validaSerie($(this),this.value)" '.$disabled.' class="sel_deta_prod_ser form-control selec_'.$x->id_ht.'" id="series_x">';
                                            $name.='<option value="0" selected>Selecciona una opción</option>';
                                            //$result_serie=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('activo'=>1,'productoid'=>$x->idproducto,'sucursalid'=>$x->idsucursal_sale,"disponible"=>1));
                                            $result_serie=$this->ModeloCatalogos->get_prod_suc($x->idproducto,$x->idsucursal_sale);
                                            foreach ($result_serie as $se){
                                                if($se->id_traslado_serie==0){
                                                    $name.='<option value="'.$se->id.'">'.$se->serie.'</option>';
                                                }
                                            }    
                                        $name.='</select>
                                        </div></div></td>';
                                    }else if($x->tipo_prod==2){
                                        $col="12";
                                        if($x->cantidad>1){
                                            $col="10";
                                        }
                                        $name.='<td><div class="row">';
                                            $name.='<div class="col-md-'.$col.'">';
                                                $name.='<select '.$disabled.' class="sel_deta_prod_lot form-control selec_'.$x->id_ht.'" id="lote_x" onchange="verificaCantLote('.$x->id_ht.')">';
                                            $name.='<option value="0" selected>Selecciona una opción</option>';
                                            $result_lote=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$x->idproducto,'sucursalid'=>$x->idsucursal_sale,"cantidad > "=>0));
                                            foreach ($result_lote->result() as $lo){
                                                $name.='<option value="'.$lo->id.'" data-cantidad="'.$lo->cantidad.'" data-lotetxt="'.$lo->lote.'">'.$lo->lote.' / Cantidad:'.$lo->cantidad.'</option>';
                                            }              
                                        $name.='</select></div>';
                                        if($x->cantidad>1){
                                            $name.='<div class="col-md-1">';
                                                $name.='<button '.$disabled.' class="btn_add_'.$x->id_ht.' btn btn-pill btn-primary" type="button" onclick="add_traspasos_lotes('.$x->id_ht.')"><i class="fa fa-plus"></i></button>';
                                            $name.='</div>';
                                        }
                                        $name.='</td>';
                                    }
                                    $name.='<td class="unidad_sat unidad_sat_'.$x->unidad_sat.'" data-val="'.$x->unidad_sat.'">'.$x->unidad_sat.'</td>';
                                    $name.='<td>'.$x->name_sucx.'</td>';
                                    $name.='<td>'.$x->name_sucxy.'</td>';
                                    $name.='<td>';
                                        $name.='<button title="Eliminar Concepto" '.$disabled.' class="btn_del_'.$x->id_ht.' btn btn-danger btn_delete_conc_'.$x->id_ht.'" type="button" onclick="deleteProd('.$x->id_ht.','.$tipo_prod.')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                                        $name.='<button '.$disabled.' title="Rechazar concepto" class="btn_recha_'.$x->id_ht.' btn btn-danger btn_rechaza_conc_'.$cont_y.'" type="button" onclick="rechazaProd('.$x->id_ht.','.$tipo_prod.')"><i class="fa fa-close" aria-hidden="true"></i></button>';
                                    $name.='</td>';
                                    $cont_y++;
                                    if($x->tipo_prod==1){
                                        $htmlsr.=$name;
                                        if($x->cantidad>1){
                                            for($i=1; $i<$x->cantidad; $i++){
                                                $htmlsr.=$name;
                                            }
                                        }
                                    }else{
                                        $html.=$name;
                                    }
                                } 
                                echo $html.$htmlsr; 
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align:right" colspan="2">Total:</td>
                                    <td style="text-align:center" id="tot_prod"><?php echo $cant_tot; ?></td>
                                    <td colspan="6"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button class="btn btn-primary btn_transitoy" style="width: 60%" onclick="save_solicitud_all()">Realizar traspaso</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>

