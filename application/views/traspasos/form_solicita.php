<style type="text/css">
    .estilo_btn{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
</style>
<input type="" id="id_reg" value="0">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="left"> 
                <h3 style="color: #1770aa;">Nueva solicitud de traspaso</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Traspasos" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body cont_search_tras">
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-label">Seleccionar día de venta</label>
                        <input class="form-control" type="date" id="dia_venta" max="<?php echo date("Y-m-d"); ?>" value="">
                    </div>
                    <div class="col-md-3">
                        <label>Sucursal solicita</label>
                        <select class="form-control" id="id_sucursal">
                            <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==5){ ?>
                                <option value="0">Seleccionar</option>
                                <?php foreach ($sucrow->result() as $x){ if($x->id==$this->session->userdata('sucursal')) $sel="selected"; else $sel=""; ?>
                                    <option <?php echo $sel; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc; ?></option>
                            <?php } 
                            } else {
                                $suc = $this->session->userdata('sucursal');
                                foreach ($sucrow->result() as $x){ 
                                    if($x->id==$suc) 
                                        echo '<option class="option_'.$x->id.'" value="'.$x->id.'">'.$x->id_alias.' - '.$x->name_suc.'</option>';
                                } 
                            } ?>
                        </select>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-lg-10">
                        <table class="table table-sm" id="table_datos" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Cant. Vendida/Solicitada</th>
                                    <th scope="col">Cant. Disponible <br>Almacén Gral.</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody id="body_prods">
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <div class="row">
                                <div class="col-lg-2">
                                    <label>Cantidad</label>
                                    <input class="form-control" type="number" id="cantidad">
                                </div>
                                <div class="col-lg-9">
                                    <label>Producto</label>
                                    <select class="form-control" id="idproducto">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <button disabled type="button" style="margin-top: 25px;" class="btn btn-primary btn_add" onclick="get_producto()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="tabla_precios"></div>
                            </div>     
                        </form> 
                    </div>
                </div>  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos"></div>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <br>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4 mx-auto">
                        <button class="btn btn-primary" id="sol_tras">Solicitar traspaso</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>
