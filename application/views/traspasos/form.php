<style type="text/css">
    .estilo_btn{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
</style>
<input type="" id="id_reg" value="0">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Traspasos" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-1">
                            <label class="form-label" style="font-size: 16px; color:#009bdb;">Recargas</label>
                        </div>    
                        <div class="col-lg-1">
                            <span class="switch switch-icon">
                                <label>
                                    <input type="checkbox" id="recargas" class="check">
                                    <span style="border: 2px solid #009bdb;"></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-12"><br></div>
                    </div>
                    <div class="card-body cont_search_tras">
                        <form id="form_registro" method="post">
                            <div class="row">
                                <div class="col-lg-10">
                                  <label>Producto</label>
                                  <select class="form-control" id="idproducto">
                                    <option></option>
                                  </select>
                                </div>
                                <div class="col-lg-2">
                                  <label>Cantidad</label>
                                  <input class="form-control" type="number" id="cantidad">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="tabla_precios"></div>
                            </div>     
                        </form>   
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text_transpasos"></div>
                            </div>
                        </div> 
                    </div>
                    <div class="card-body cont_rechange" style="display:none">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group select_option_search mb-3">
                                    <label>Tanque</label>
                                    <select id="id_tanque" class="form-select btn-pill digits" >
                                        <option value="0">Seleccione una opción</option>
                                        <?php foreach($tanques->result() as $t){
                                            echo "<option value='".$t->id."'>".$t->codigo."</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                              <label>Cantidad en litros</label>
                              <input class="form-control" type="number" id="cant_litros">
                            </div>
                            <div class="col-lg-12">
                                <div class="tabla_tanques"></div>
                            </div> 
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <br>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4">
                                <button class="btn btn-primary " onclick="guardar_registro_href()">Guardar registro</button>
                            </div>
                        </div>
                    </div>

                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
