<style type="text/css">    
    #table_datos_filter{
        display: none;
    }
    .img_icon{
        width: 36px;
    }

    .realizarventa{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
</style>
<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar traspaso" id="searchtext" oninput="search()">

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
                    <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3){ ?>
                        <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
                    <?php } ?>
                </div>  
 
                <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3){ ?>
                    <!--<div class="col-lg-2 mx-auto"> 
                        <a href="<?php echo base_url() ?>Traspasos/solicitudManual" class="btn btn-primary">Traspaso Manual PV</a>
                    </div>-->
                <?php } ?>
                    <div class="row col-md-5" align="right"> 
                        <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3 || $this->session->userdata('perfilid')==5) { ?>
                                <div class="col-md-6" align="right"><a href="<?php echo base_url() ?>Traspasos/soliciManualAlmacen" class="btn btn-primary">Traspaso Manual Almacén</a></div>
                        <?php } ?>
                        <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3 || $this->session->userdata('perfilid')==4){ ?>
                                <div class="col-md-6" align="right"><a href="<?php echo base_url() ?>Traspasos/solicitudReco" class="btn btn-primary">Traspaso por venta</a></div>
                        <?php } ?>
                    </div>
                
            </div>  
            
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <select class="form-control" id="estatus" onchange="table()">
                                <option value="4">Estatus</option>
                                <option value="1">En tránsito</option>
                                <option value="2">Ingresado</option>
                                <option value="0">Apartado</option>
                                <option value="3">Rechazado</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Id T.</th>
                                        <th scope="col">Fecha de solicitud</th>
                                        <th scope="col">Usuario que solicita</th>
                                        <th scope="col">Sucursal entrada</th>
                                        <th scope="col">Sucursal salida</th>
                                        <th scope="col">Productos</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detalles de solicitud</h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="card-body">
                <div class="row g-3">
                  <div class="col-md-12">
                    <div class="text_transpasos"></div> 
                  </div>
                </div>
            </div>    
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modaldetalles_apartado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Confirmación de traspaso</b></h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_transpasos_apartado"></div> 
              </div>
            </div>
        </div> 
        <div class="modal-footer">
          <a class="btn realizarventa btn-sm" type="button" data-bs-dismiss="modal">Regresar</a>
          <a type="button" class="btn realizarventa btn-sm btn_transitoy" style="width: 40%" onclick="validar_cerrar_traspaso()">Cerrar traspaso</a>
      </div>   
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldetalles_transito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Confirmación de entrada</b></h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
              <div class="col-md-12">
                <div class="text_transpasos_transito"></div> 
              </div>
            </div>
        </div>    
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalmotivo" tabindex="100" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Motivo de rechazo</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <input type="hidden" id="id_tras" value="0">
                            <textarea class="form-control" id="txt_motivo"></textarea>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <a class="btn btn-primary btn-sm" type="button" data-bs-dismiss="modal">Cerrar</a>
                </div>   
            </div>
        </div>
    </div>
</div>
