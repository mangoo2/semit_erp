<style type="text/css">
    .estilo_btn{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
</style>
<input type="" id="id_reg" value="0">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="left"> 
                <h3 style="color: #1770aa;">Nueva solicitud de traspaso manual</h3>
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Traspasos" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <div class="row">
                                <div class="col-lg-1">
                                    <label class="form-label" style="font-size: 16px; color:#009bdb;">Recargas</label>
                                </div>    
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" id="recargas" class="check">
                                            <span style="border: 2px solid #009bdb;"></span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-lg-3">
                                    <label>Sucursal solicita</label>
                                    <select class="form-control" id="id_sucursal">
                                        <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==5){ ?>
                                            <option value="0">Seleccionar</option>
                                            <?php foreach ($sucrow as $x){ if($x->id==$this->session->userdata('sucursal')) $sel="selected"; else $sel=""; ?>
                                                <option <?php echo $sel; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc; ?></option>
                                        <?php } 
                                        } else {
                                            $suc = $this->session->userdata('sucursal');
                                            foreach ($sucrow as $x){ 
                                                if($x->id==$suc) 
                                                    echo '<option class="option_'.$x->id.'" value="'.$x->id.'">'.$x->id_alias.' - '.$x->name_suc.'</option>';
                                            } 
                                        } ?>
                                    </select>
                                </div>
                                <!--<div class="col-lg-3">
                                    <label>Sucursal que surte</label>
                                    <select class="form-control" id="id_sucursal_salida">
                                        <option value="0">Seleccionar</option>
                                        <?php foreach ($suc_surte as $x){ if($x->id==8) $sel="selected"; else $sel=""; ?>
                                            <option <?php echo $sel; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->name_suc ?></option>
                                        <?php } ?>
                                    </select>
                                </div>-->
                                <div class="col-lg-12"><br></div>
                            </div> 
                            <div class="card-body row cont_search_tras">
                                <div class="col-lg-2">
                                    <label>Cantidad</label>
                                    <input class="form-control" type="number" id="cantidad">
                                </div>
                                <div class="col-lg-9">
                                    <label>Producto</label>
                                    <select class="form-control" id="idproducto">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <button type="button" style="margin-top: 25px;" class="btn btn-info" onclick="get_productos_sucursal()"> <i class="fa fa-eye" aria-hidden="true"></i></button>
                                </div>
                                <!--<div class="col-lg-1">
                                    <button type="button" style="margin-top: 25px;" class="btn btn-primary btn_add" onclick="get_producto()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                </div>-->
                            </div>
                            <div class="col-lg-12">
                                <div class="tabla_precios"></div>
                            </div> 
                            <div class="card-body cont_rechange" style="display:none">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group select_option_search mb-3">
                                            <label>Tanque</label>
                                            <select id="id_tanque" class="form-select btn-pill digits" >
                                                <option value="0">Seleccione una opción</option>
                                                <?php foreach($tanques as $t){
                                                    echo "<option data-stock-alm='".($t->stock-$t->traslado_stock_cant)."' value='".$t->id."'>".$t->codigo." (".($t->stock-$t->traslado_stock_cant)." L)</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Cantidad en litros</label>
                                        <input class="form-control" type="number" id="cant_litros">
                                    </div>
                                    <div class="col-lg-1">
                                        <button type="button" style="margin-top: 25px;" class="btn btn-primary btn_add_reche" onclick="get_recarga()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="tabla_tanques"></div>
                                    </div> 
                                </div> 
                            </div>

                        </form> 
                    </div>
                </div>
                <!-- recargas -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_recarga" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_recarga">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">RECARGAS</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Origen</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody id="body_recarga">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Stock -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_stock" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_stock">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">PRODUCTO DE STOCK</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Origen</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Series -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_series" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_series">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">PRODUCTO DE SERIE</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Origen</th>
                                        <th scope="col">Cantidad</th>
                                        <!--<th scope="col">Series</th>-->
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Lotes -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_lotes" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_lotes">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">PRODUCTO DE LOTE</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Origen</th>
                                        <th scope="col">Cantidad</th>
                                        <!--<th scope="col">Lotes</th>-->
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button class="btn btn-primary btn_transitoy" style="width: 60%" onclick="validar_cerrar_traspaso()">Solicitar traspaso</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>

<div class="modal fade" id="modal_suc_prod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Existencia en sucursales</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_prodsuc">
                                <thead>
                                    <tr>
                                        <th scope="col">Sucursal</th>
                                        <th scope="col">Costo</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Elegir</th>
                                    </tr>
                                </thead>
                                <tbody id="body_prodsuc">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <a class="btn btn-primary btn-sm" type="button" data-bs-dismiss="modal">Cerrar</a>
                </div>   
            </div>
        </div>
    </div>
</div>