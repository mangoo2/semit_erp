<style type="text/css">
    .estilo_btn{
        text-align: center;border: 2px solid #009bdb;border-radius: 8px;background: #93ba1f;color: white;  font-size: 15px;
    }
    .teclas_img{
        width: 50px;
    }
    .tecla_div span{
        color: #019cde;
        font-weight: bold;
    }
    .tecla_div{
        padding-left: 5px;
        padding-right: 5px;
    }
    .tecla_div p{
        font-size: 13px;
    }
</style>
<input type="" id="id_reg" value="0">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row col-md-12"> 
            <div class="col-lg-3" align="left"> 
                <h3 style="color: #1770aa;">Nueva solicitud de traspaso</h3>
                
            </div>
            <div class="col-lg-7"></div>
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Traspasos" class="btn btn-primary">Regresar</a>
            </div>    

            <div class="col-lg-3 tecla_div" align="left">
                <p><img src="<?php echo base_url().'public/img/teclas/alt.png'?>" class="teclas_img">+<img src="<?php echo base_url().'public/img/teclas/r.png'?>" class="teclas_img"><span>Seleccionar Recargas<span></p>
            </div>
            <div class="col-lg-3 tecla_div" align="left">
                <p><img src="<?php echo base_url().'public/img/teclas/ctrl2.png'?>" class="teclas_img">+<img src="<?php echo base_url().'public/img/teclas/q.png'?>" class="teclas_img"><span>Nueva búsqueda por teclado<span></p>
            </div>
            <div class="col-lg-3 tecla_div" align="left">
                <p><img src="<?php echo base_url().'public/img/teclas/alt.png'?>" class="teclas_img">+<img src="<?php echo base_url().'public/img/teclas/b.png'?>" class="teclas_img"><span>Nueva búsqueda por código<span></p>
            </div>
            <div class="col-lg-3 tecla_div" align="left">
                <p><img src="<?php echo base_url().'public/img/teclas/ctrl2.png'?>" class="teclas_img">+<img src="<?php echo base_url().'public/img/teclas/z.png'?>" class="teclas_img"><span>Elimina la última partida<span></p>
            </div>
  
        </div>  
        <br>
        <div class="card card-custom gutter-b" >         
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_registro" method="post">
                            <div class="row">
                                
                                <div class="col-lg-2">
                                    <label>Sucursal solicita</label>
                                    <select class="form-control" id="id_sucursal">
                                        <?php if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==5){ ?>
                                            <option value="0">Seleccionar</option>
                                            <?php foreach ($sucrow as $x){ ?>
                                                <option class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
                                        <?php } 
                                        }else{
                                            $suc = $this->session->userdata('sucursal');
                                            foreach ($sucrow as $x){ 
                                                if($x->id==$suc) 
                                                    echo '<option class="option_'.$x->id.'" value="'.$x->id.'">'.$x->id_alias.' - '.$x->name_suc.'</option>';
                                            } 
                                        } ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <label>Sucursal que surte</label>
                                    <select class="form-control" id="id_sucursal_salida">
                                        <option value="0">Seleccionar</option>
                                        <?php foreach ($suc_surte as $x){ ?>
                                            <option <?php if($x->id=="8" && $this->session->userdata('perfilid')==1 || $x->id=="8" && $this->session->userdata('perfilid')==5) echo "selected"; ?> class="option_<?php echo $x->id ?>" value="<?php echo $x->id ?>"><?php echo $x->id_alias." - ".$x->name_suc ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <label class="form-label" style="font-size: 16px; color:#009bdb;">Recargas</label>
                                </div>    
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" id="recargas" class="check">
                                            <span style="border: 2px solid #009bdb;"></span>
                                        </label>
                                    </span>
                                </div>
                                <div class="col-lg-2">
                                    <label>Seleccionar Operador</label>
                                    <select class="form-control" id="catp_operador" name="catp_operador">
                                        <option value="0"></option>
                                        <?php 
                                            foreach ($rest_op->result() as $itemo) {
                                                if($itemo->rfc_del_operador!='' && $itemo->no_licencia!=''){
                                                    echo '<option value="'.$itemo->id.'">'.$itemo->operador.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <label>Seleccionar Vehiculo</label>
                                    <select class="form-control" id="catp_vehiculo" name="catp_vehiculo">
                                        <option value="0"></option>
                                        <?php 
                                            foreach ($rest_veh->result() as $itemv) {
                                                echo '<option value="'.$itemv->id.'">'.$itemv->txtNombre.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-2"></div>
                                <div class="col-lg-2">
                                    <label>Fecha de salida</label>
                                    <input type="datetime-local" class="form-control" name="catp_salida" id="catp_salida" value="<?php echo $catp_salida;?>" min="<?php echo $catp_salida;?>">
                                </div>
                                <div class="col-lg-2">
                                    <label>Fecha de llegada</label>
                                    <input type="datetime-local" class="form-control" name="catp_llegada" id="catp_llegada" value="<?php echo $catp_llegada;?>" min="<?php echo $catp_llegada;?>">
                                </div>
                                
                                <div class="col-lg-12"><br></div>
                            </div> 
                            <div class="card-body row cont_search_tras">
                                <div class="col-lg-2">
                                    <br>
                                    <label>Cantidad</label>
                                    <input class="form-control" type="number" id="cantidad" value="1" min="1">
                                </div>
                                <div class="col-lg-12"><br></div>
                                <div class="col-lg-11">
                                    <label>Producto <button type="button" title="Busqueda por nombre" class="btn btn-primary" onclick="showSearch(this.id)" id="search_sel"><i class="fa fa-search fa-2x" aria-hidden="true"></i></button> <button type="button" title="Busqueda por código de barras" class="btn btn-primary" onclick="showSearch(this.id)" id="search_inp"><i class="fa fa-barcode fa-2x btn-primary" aria-hidden="true"></i></button></label>
                                    <div id="cont_sel_idp" style="display:none; width: 100%"><select class="form-control" id="idproducto" style="width: 100%">
                                            <option></option>
                                        </select>
                                    </div>
                                    <input placeholder="Ingrese codigo de barras"  class="form-control" id="search_cod">
                                </div>
                                <div class="col-lg-1">
                                    <button type="button" style="margin-top: 45px;" class="btn btn-primary btn_add" onclick="get_producto()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="tabla_precios"></div>
                            </div> 
                            <div class="card-body cont_rechange" style="display:none">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group select_option_search mb-3">
                                            <label>Tanque</label>
                                            <select id="id_tanque" class="form-select btn-pill digits" >
                                                <option value="0">Seleccione una opción</option>
                                                <?php foreach($tanques as $t){
                                                    echo "<option data-codigo='".$t->codigo."' data-stock-alm='".($t->stock-$t->traslado_stock_cant)."' value='".$t->id."' data-peli='0'>".$t->codigo." (".($t->stock-$t->traslado_stock_cant)." L)</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Cantidad en litros</label>
                                        <input class="form-control" type="number" id="cant_litros">
                                    </div>
                                    <div class="col-lg-1">
                                        <button type="button" style="margin-top: 25px;" class="btn btn-primary btn_add_reche" onclick="get_recarga()"> <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="tabla_tanques"></div>
                                    </div> 
                                </div> 
                            </div>

                        </form> 
                    </div>
                </div>
                <!-- todos -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_prods">
                            <br>
                            <table class="table table-sm table-striped table-bordered" id="table_datos_prods">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="8">PRODUCTOS</th></tr>
                                    <tr>
                                        <th scope="col">Código</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Detalles</th>
                                        <th scope="col">Unidad</th>
                                        <th scope="col">Salida</th>
                                        <th scope="col">Entrada</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody id="body_prods">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="text-align:right">Total Productos:</td>
                                        <td style="text-align:center" id="tot_prod"></td>
                                        <td colspan="6"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right">Total Litros:</td>
                                        <td style="text-align:center" id="tot_litros"></td>
                                        <td colspan="6"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- recargas -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_recarga" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_recarga">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">RECARGAS</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Sucursal surte</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody id="body_recarga">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Stock -->  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_stock" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_stock">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="4">PRODUCTO DE STOCK</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Sucursal surte</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Series -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_series" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_series">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="5">PRODUCTO DE SERIE</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Sucursal surte</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Series</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Lotes -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text_transpasos_lotes" style="display: none">
                            <br>
                            <table class="table table-sm" id="table_datos_lotes">
                                <thead>
                                    <tr><th style="text-align: center; background-color: #67c4eb; color:#fff" colspan="6">PRODUCTO DE LOTE</th></tr>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Sucursal surte</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Lotes</th>
                                        <th scope="col">Caducidad</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4" align="center">
                        <button class="btn btn-primary btn_transitoy validar_cerrar_traspaso" style="width: 60%" onclick="validar_cerrar_traspaso()">Realizar traspaso</button>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>


<div class="modal fade" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de selección</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-md-12">
                            <table class="table table-sm" id="prod_detalle">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Detalle</th>
                                        <th>Cantidad</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="det_prods"></tbody>
                            </table>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <!--<button onclick="asignaSerie()" class="btn btn-primary" type="button" data-bs-dismiss="modal">Aceptar</button>-->
                <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>