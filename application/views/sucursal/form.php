<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="right"> 
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    <div class="card card-custom gutter-b">
      <div class="card-body">
        <form id="form_datos" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="row"> 
              <div class="col-md-2">
                <label># de Sucursal</label>
                <input type="text" name="id_alias" id="id_alias" class="form-control" oninput="validar_num_sucursal()" value="<?php echo $id_alias;?>">
              </div>
              <div class="col-md-2">
                <label>Clave</label>
                <input type="text" name="clave" class="form-control" value="<?php echo $clave;?>">
              </div>
              <div class="col-md-2">
                <label>Orden</label>
                <input type="text" name="orden" class="form-control" value="<?php echo $orden;?>">
              </div>
              <div class="col-lg-2" style="margin-top:30px; text-align: right;">
                <label class="form-label" style="font-size: 16px; color:#009bdb;">Incluye Insumos</label>
              </div>    
              <div class="col-lg-1" style="margin-top:30px;">
                <span class="switch switch-icon">
                  <label>
                    <input <?php if($con_insumos==1) echo "checked";?> type="checkbox" id="con_insumos" class="check" value="0">
                    <span style="border: 2px solid #009bdb;"></span>
                  </label>
                </span>
              </div>
            </div>
            <div class="row g-3"><br></div>
            <div class="row g-3"> 
              <div class="col-md-4">
                <label>Nombre de Sucursal</label>
                <input type="text" name="name_suc" class="form-control" value="<?php echo $name_suc;?>">
              </div>
              <div class="col-md-4">
                <label>Teléfono</label>
                <input type="text" name="tel" class="form-control" value="<?php echo $tel;?>">
              </div>
            </div>    
            <div class="row g-3"><br></div>

            <div class="row"> 
              <div class="col-md-8">
                <label>Domicilio</label>
                <input type="text" name="domicilio" class="form-control" value="<?php echo $domicilio;?>">
              </div>
            </div>     
        </form>  
        <br>
        <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos()">Guardar</button>                      
      </div>
    </div>
</div>    
