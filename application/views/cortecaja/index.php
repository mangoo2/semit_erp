<style type="text/css">
    .ar_title{
        color: #152342;
        font-weight: bold;
        font-size: 18px;
    }
    .ar_subtitle{
        color: #c6d420;
        font-weight: bold;
        font-size: 18px;
    }
    #tablepagos{
        color: #152342;
        font-weight: bold;
        font-size: 15px;
    }
    <?php 
        $idsucursalseleect=0;
        $idpersonalselect=0;
        //echo 'perfil: '.$perfilid;
        if($perfilid==3){
            ?>
            .soloadministradores{
                display: none;
            }
            <?php
        }
        if($perfilid==3){
            ?>
                .soloadministradores{
                    display: none;
                }
            <?php
        }
        if($perfilid==2 or $perfilid==3){
            $idsucursalseleect=$idsucursal;
            $idpersonalselect=$idpersonal;
            ?>
            .divsucursal, .divempleado{
                display: none;
            }
            <?php
        }
    ?>
</style>
<input type="hidden" id="perfilid" value="<?php echo $perfilid;?>">
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row">
            <div class="col-lg-10">
                <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
                <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
            </div>
            <div class="col-lg-2" align="right">
                <!--<a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>-->
            </div>
        </div>
        <br>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2 divsucursal">
                    <label>Sucursal</label>
                    <select class="form-control" id="sucursalselect">
                        <option value="0"></option>
                        <?php foreach ($sucrow->result() as $item) { 
                            if($idsucursalseleect==$item->id){
                                $op_selected=' selected';
                            }else{
                                $op_selected='';
                            }
                            echo '<option value="'.$item->id.'" '.$op_selected.' data-idturno="'.$item->turid.'" >'.$item->id_alias.' - '.$item->name_suc.'</option>'; 
                        } ?>
                    </select>
                </div>
                <div class="col-md-2 divempleado">
                    <label>Empleado</label>
                    <select class="form-control" id="empleadoselect">
                        <option value="0"></option>
                        <?php foreach ($emplerow->result() as $item) { 
                            if($idpersonalselect==$item->personalId){
                                $op_selected_emp=' selected';
                            }else{
                                $op_selected_emp='';
                            }
                            if($perfilid==2){
                                $op_selected_emp='';
                            }
                            echo '<option value="'.$item->personalId.'" '.$op_selected_emp.'>'.$item->nombre.'</option>'; 
                        } ?>
                    </select>
                </div>
            </div>
            <!---------------------------------->
            <div id="wizard-2" class="form-wizard">
                <ul>
                    <li><a href="#tab21"  data-toggle="tab"><div class="menu-icon formv1"> 1 </div>Arqueo de caja </a></li>
                    <li><a href="#tab22"  data-toggle="tab"><div class="menu-icon formv2"> 2 </div>Corte XZ </a></li>
                </ul>
                <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-info" > </div>
                </div>
                <div class="tab-content no-bd pd-25">
                    <div class="tab-pane" id="tab21">
                        <!---------------------------------------------->
                        <div class="row">
                            
                            
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <p class="h4"><b>Corte de caja</b></p>
                                <p>SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</p>
                                <p>Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</p>
                                
                                <p class="h4"><b>Folio: <span class="numerode_arqueo"></span></b></p>
                                <input type="hidden" id="numerode_arqueo">

                                <p class="h4"><b>Sucursal: <span class="select_suc"></span></b></p>
                                <input type="hidden" id="suc_arqueo">
                            </div>
                        </div>
                        <div class="row show_hide_nume_arqueo">
                            <div class="col-md-2"><input type="hidden" id="num_arqueo"></div>
                            <div class="col-md-2 num_arqueo"></div>
                            <div class="col-md-8"></div>
                        </div>
                        <div class="row show_hide_emp_arqueo">
                            <div class="col-md-2"><input type="hidden" id="emp_arqueo"><input type="hidden" id="emp_arq_n"></div>
                            <div class="col-md-2 emp_arqueo"></div>
                            <div class="col-md-8"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <table class="table" id="tablepagos">
                                    <tbody class="tbody_tablepagos">
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total: </td>
                                            <td class="total_tablepagos"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row row_confirmarpagos"></div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <div class="">
                                    <span class="btn_regresar"></span>
                                    <button class="btn btn-primary generar_arqueo" onclick="generararqueo()">Generar Arqueo</button>
                                    <a class="btn btn-warning btn_paso1" style="background: #c6d420 !important; border-color: #c6d420 !important;" onclick="imprimirarqueo()">Imprimir</a>
                                    <a class="btn btn-warning btn_paso1 soloadministradores" style="background: #c6d420 !important; border-color: #c6d420 !important;" onclick="guardararqueo()">Guardar y continuar</a>
                                    <a class="btn btn-warning btn_paso2 soloadministradores" onclick="guardararqueop2()" style="display: none; background: #c6d420 !important; border-color: #c6d420 !important;">Guardar confirmación y continuar</a>
                                </div>    
                            </div>
                        </div>
                        <!---------------------------------------------->
                    </div>
                    <div class="tab-pane" id="tab22">
                        
                        <!---------------------------------------------->
                            <?php 
                                if(isset($_GET['cortexz'])){
                                    
                                    $idarqueo=$_GET['idarqueo'];
                                    $r_xz_head=$this->ModeloCatalogos->generareportexz($idarqueo,0,0);
                                    echo $r_xz_head;
                                    $r_xz=$this->ModeloCatalogos->generareportexz($idarqueo,1,0);
                                    echo $r_xz;
                                    
                                }
                            ?>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
                                <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
                                <?php if(isset($_GET['cortexz'])){ 
                                    $idarqueo=$_GET['idarqueo'];
                                ?>
                                <a href="<?php echo base_url().'reportes/cortexz/'.$idarqueo.'/0'; ?>" target="_blank" class="btn btn-primary" style="background: #c6d420 !important; border-color: #c6d420 !important;">Imprimir corte de caja</a>
                                <?php } ?>
                            </div>
                        </div>
                        <!---------------------------------------------->
                    </div>
                    
                    <div class="form-actions-condensed wizard">
                        <div class="row mgbt-xs-0">
                            <div class="col-sm-9 col-sm-offset-2" style="display:none;">
                                
                                <a class="btn vd_btn prev" href="javascript:void(0);">
                                    <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-left"></i></span> Anterior
                                </a>
                                <a class="btn vd_btn next btn-nextclick" href="javascript:void(0);">Siguiente <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a>
                                <button  type="submit" class="btn vd_btn vd_bg-green finish" href="javascript:void(0);">
                                <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Finalizar
                                </button>
                            
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-------------->
            </div>
            <!---------------------------------->
        </div>
    </div>
</div>