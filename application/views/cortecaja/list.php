<style type="text/css">

    <?php 
        $idsucursalseleect=0;
        if($perfilid==1){

        }else{
            $idsucursalseleect=$idsucursal;
            $idpersonalselect=$idpersonal;
            ?>
            .divsucursal{
                display: none;
            }
            <?php
        }
        
    ?>
</style>
<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row">
            <div class="col-lg-10" align="right">
            </div>
            <div class="col-lg-2" align="right">
                <a href="<?php echo base_url() ?>Cortecaja" class="btn btn-primary">Generar</a>
            </div>
        </div>
        <br>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <!---------------------------------->
                <div class="row">
                    <div class="col-md-2 divsucursal">
                        <label>Sucursal</label>
                        <select class="form-control" id="sucursalselect" onchange="load()">
                            <option value="0"></option>
                            <?php foreach ($sucrow->result() as $item) { 
                                if($idsucursalseleect==$item->id){
                                    $op_selected=' selected';
                                }else{
                                    $op_selected='';
                                }
                                if($item->id!=6 && $item->id!=7 && $item->id!=8){// solo se ocultaron estas sucursales del listado
                                    echo '<option value="'.$item->id.'" '.$op_selected.'>'.$item->name_suc.'</option>'; 
                                }
                                
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <table class="table" id="tablecortes">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Sucursal</th>
                                    <th>Día</th>
                                    <th>Usuario apertura</th>
                                    <th>Hora</th>
                                    <th>Usuario cierre</th>
                                    <th>Hora</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            
                        </table>
                    </div>
                    
                </div>
            
            <!---------------------------------->
        </div>
    </div>
</div>