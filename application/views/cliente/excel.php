<?php
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_clientes_".date("Ymd").".xls");
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table  border="1">
    <thead>
        <tr>
            <th rowspan="2">Código / ID</th>
            <th rowspan="2">Nombre</th>
            <th rowspan="2">Teléfono</th>
            <th rowspan="2">Correo</th>
            <th rowspan="2">Calle</th>
            <th rowspan="2">CP</th>
            <th rowspan="2">Colonia</th>
            <th rowspan="2">Saldo</th>
            <th rowspan="2">Estado</th>
            <th rowspan="2">Municipio</th>
            <th colspan="5">DATOS FISCALES</th>
            <th colspan="2">CONDICIONES DE PAGO</th>
            <th rowspan="2">Estatus</th>
        </tr>
        <tr>
            <th>Razón Social</th>
            <th>RFC</th>
            <th>CP</th>
            <th>Régimen Fiscal</th>
            <th>Uso de CFDI</th>
            <th># Días</th>
            <th>Saldo</th>
        </tr>

    </thead>
    <tbody>
        <?php foreach ($get_clientes as $x){
            $estatus='';
            if($x->desactivar==1){
                $estatus='Suspendido';
            }else{
                $estatus='Activo';
            }
         ?>
        <tr>
            <td><?php echo $x->clave.' '.$x->id; ?></td>
            <td><?php echo $x->nombre; ?></td>
            <td><?php echo $x->celular; ?></td>
            <td><?php echo $x->correo; ?></td>
            <td><?php echo $x->calle; ?></td>
            <td><?php echo $x->cp; ?></td>
            <td><?php echo $x->colonia; ?></td>
            <td><?php echo '$'.number_format($x->saldo,2,'.',',') ?></td>
            <td><?php echo $x->edo_name; ?></td>
            <td><?php echo $x->municipio; ?></td>
            <td><?php echo $x->razon_social; ?></td>
            <td><?php echo $x->rfc; ?></td>
            <td><?php echo $x->cp_fiscal; ?></td>
            <td><?php echo $x->reg_fiscal_txt; ?></td>
            <td><?php echo $x->uso_cfdi_text; ?></td>
            <td><?php echo $x->diaspago; ?></td>
            <td><?php echo number_format($x->saldopago,2,'.',','); ?></td>

            <td><?php echo $estatus; ?></td>
        </tr>  
        <?php } ?>
    </tbody>
</table>
