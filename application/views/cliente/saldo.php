<div class="page-body"> 
    <input type="hidden" id="idcliente" value="<?php echo $idcliente ?>">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                    <div class="col-lg-10"> 
                        <h3>Listado de saldos </h3>
                    </div>   
                    <div class="col-lg-2" align="right"> 
                        <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                    </div>    
                </div> 
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">

                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Folio</th>
                                        <th scope="col">Sucursal</th>
                                        <th scope="col">Vendedor</th>
                                        <th scope="col">Fecha y hora</th>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Monto</th>
                                        <th scope="col">Productos / Servicios</th>
                                        <th scope="col">Tipo de pago</th>
                                        <th scope="col">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th>Costo Unitario</th>
                            <th>Descuento</th>
                            <th>Importe</th>
                        </tr>
                    </thead>
                    <tbody class="table_pro"></tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalformaspagos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Formas de pago</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12 table_forma">
                
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>