<style type="text/css">
  #table_datos_filter{
    display: none;
  }
  .actionactivo{
    background-color: #93ba1f !important;border-color: #93ba1f !important;
  }
</style>
<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">

            <div class="row">
              <div class="col-lg-4">
                <a href="<?php echo base_url() ?>Inicio"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/casa.png"></a>
                <a href="<?php echo base_url() ?>Ventasp"><img style="width: 40px" src="<?php echo base_url() ?>public/img/pv/22.png"></a>
              </div>
            </div>      
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar cliente" id="searchtext" oninput="search()">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <select class="form-control" id="tipocliente" onchange="reload_registro()">
                        <option value="0">Tipo de cliente</option>
                        <option value="1">Sistema</option>
                        <option value="2">Página</option>
                    </select>
                </div>  
                <div class="col-lg-2">
                    <select class="form-control" id="desactivar" onchange="reload_registro()">
                        <option value="2">Seleccionar</option>
                        <option value="0">Activos</option>
                        <option value="1">Inactivos</option>
                    </select>
                </div>  
                <div class="col-lg-2">
                    <select class="form-control" id="sucursal_cli" onchange="reload_registro()">
                        <option value="0">Todas las sucursales</option>
                        <?php foreach ($sucrow->result() as $x){ ?>
                            <option value="<?php echo $x->id ?>"><?php echo "0".$x->id." - ".$x->name_suc; ?></option>
                        <?php } ?>
                      </select>
                </div>
                <?php if($this->session->userdata("perfilid")==1){ ?>
                    <div class="col-lg-3" align="right"> 
                        <button onclick="get_excel()" class="btn btn-success exportExcel" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Excel</button>
                        <a href="<?php echo base_url() ?>Clientes/registro" class="btn btn-primary">Nuevo cliente</a>
                    </div>   
                <?php } ?> 
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Datos fiscales</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modal_datosficales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Datos fiscales</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <div class="datos_fiscalestxt"></div>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_suspension" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">Motivo de suspensión</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <div class="motivotxt"></div>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>