<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row"> 
            <div class="col-lg-10" align="right"> 
            </div>   
            <div class="col-lg-2" align="right"> 
                <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
            </div>    
        </div>  
        <br>
    </div>        
    <div class="card card-custom gutter-b">
      <input type="hidden" id="idreg" value="<?php echo $id ?>">
      <div class="card-body">
        <ul class="nav nav-tabs" id="icon-tab" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="icon-home-tab" data-bs-toggle="tab" href="#icon-home" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>Datos generales</a></li>
          <li class="nav-item"><a class="nav-link" id="profile-icon-tab" data-bs-toggle="tab" href="#profile-icon" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Datos fiscales</a></li>
          <li class="nav-item"><a class="nav-link tab3" id="pagos-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#pagos-icon"'; else echo 'href="#checkTab(3)" style="cursor: no-drop;'; ?> role="tab" aria-controls="pagos-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Condiciones de pago</a></li>
          <li class="nav-item"><a class="nav-link" id="programa-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#programa-icon"'; else echo 'href="#checkTab(4)" style="cursor: no-drop;'; ?> role="tab" aria-controls="programa-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Programa cliente frecuente</a></li>
          <li class="nav-item"><a class="nav-link tab5" id="contact-icon-tab" data-bs-toggle="tab" <?php if($this->session->userdata('perfilid')==1) echo 'href="#contact-icon"'; else echo 'href="#checkTab(5)" style="cursor: no-drop;'; ?> role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Sesión en pagína web</a></li>
        </ul>
        <div class="tab-content" id="icon-tabContent">
          <div class="tab-pane fade show active" id="icon-home" role="tabpanel" aria-labelledby="icon-home-tab">
            <br>
            <form id="form_registro_datos" method="post">
                <input type="hidden" id="idreg1" name="id" value="<?php echo $id ?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nombre del cliente<span style="color: red">*</span></label>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                            </div>
                        </div>
                    </div>

                </div>    
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Teléfono</label>
                            <div class="col-lg-5">
                                <input class="form-control" type="number" name="celular" value="<?php echo $celular ?>">
                            </div>
                        </div>
                    </div>

                </div>  
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Correo electrónico<span style="color: red">*</span></label>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()" value="<?php echo $correo ?>">
                            </div>   
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Calle y número</label>
                            <div class="col-lg-5"><input class="form-control" type="text" name="calle" value="<?php echo $calle ?>">
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Código postal<span style="color: red">*</span></label>
                            <div class="col-lg-5"><input class="form-control" type="text" name="cp" id="codigo_postal" oninput="cambiaCP2()" value="<?php echo $cp ?>">
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Colonia<span style="color: red">*</span> <a onclick="modal_alert()"><img style="width: 18px;" src="<?php echo base_url() ?>images/spotlight-svgrepo-com.svg"></a></label>
                            <div class="col-lg-5">
                                <select class="form-control" id="colonia" name="colonia" onchange="regimefiscal()" required>
                                    <option value="0" selected disabled>Seleccione</option>
                                    <?php if($colonia!=''){
                                        echo '<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                                    } ?>
                                </select>
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Saldo</label>
                            <div class="col-lg-5"><input class="form-control" type="number" name="saldo" id="saldo" value="<?php echo $saldo ?>">
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <?php if($this->session->userdata('perfilid')==1){ ?>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Suspender</label>
                                <div class="col-lg-5">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" name="desactivar" id="desactivar" onclick="activar_campo()" <?php if($desactivar==1) echo 'checked' ?>>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>   
                            </div>
                        <?php } else { ?>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Suspender</label>
                                <div class="col-lg-5">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input disabled type="checkbox" name="desactivar" id="desactivar" <?php if($desactivar==1) echo 'checked' ?>>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>   
                            </div>
                        <?php } ?>
                    </div>
                    <?php 
                        $motivo_sty='';
                        if($desactivar==1){
                            $motivo_sty='style="display: block;"';
                        }else{
                            $motivo_sty='style="display: none;"';
                        }
                    ?>
                    <div class="col-lg-12">
                        <div class="motivo_txt" <?php echo $motivo_sty ?>>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Motivo</label>
                                <div class="col-lg-5">
                                    <textarea class="form-control" rows="3" id="motivo" name="motivo"><?php echo $motivo ?></textarea>
                                </div>   
                            </div>
                        </div>    
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Estado</label>
                            <div class="col-lg-5">
                                <select name="estado" id="estado" class="form-control">
                                  <?php foreach ($estadorow->result() as $item) { ?>
                                  <option value="<?php echo $item->id;?>" <?php if($estado==$item->id){ echo 'selected';}?> ><?php echo $item->estado;?></option>
                                  <?php } ?> 
                                </select>
                            </div>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Municipio</label>
                            <div class="col-lg-5"><input class="form-control" type="text" name="municipio" value="<?php echo $municipio ?>">
                            </div>   
                        </div>
                    </div>
                </div>
            </form>  
            <div align="center">
                <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_datos()">Guardar</button>
            </div>                      
          </div>
          <div class="tab-pane fade" id="profile-icon" role="tabpanel" aria-labelledby="profile-icon-tab">
            <br>
            <form id="form_registro_datos_fiscales" method="post">
                <input type="hidden" id="idreg2" name="id" value="<?php echo $id ?>">
                <div class="row">
                    <div class="col-md-6">
                      <label class="form-label">Activar datos fiscales</label>
                      <span class="switch switch-icon">
                        <label>
                            <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscales" <?php if($activar_datos_fiscales==1) echo 'checked' ?>>
                            <span></span>
                        </label>
                      </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Razón social</label>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" id="razon_social" name="razon_social" oninput="validar_razon_social()" value="<?php echo $razon_social ?>">
                            </div>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">RFC</label>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="rfc" id="rfc" oninput="validar_rfc_cliente()" value="<?php echo $rfc ?>">
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">CP</label>
                            <div class="col-lg-5">
                                <input class="form-control" type="text" name="cp"  value="<?php echo $cpy ?>">
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Régimen fiscal</label>
                            <div class="col-lg-5">
                                <select class="form-control" id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" onchange="regimefiscal()">
                                    <option value="0" selected disabled>Seleccione</option>
                                    <?php foreach ($get_regimen->result() as $item) {
                                        if($item->clave==$RegimenFiscalReceptor){
                                            echo '<option value="'.$item->clave.'" selected>'.$item->clave.' '.$item->descripcion.'</option>';
                                        }else{
                                            echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                                        }
                                        
                                    }?>
                                </select>
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Uso de CFDI</label>
                            <div class="col-lg-5">
                                <select class="form-control" id="uso_cfdi" name="uso_cfdi">
                                    <option value="0" selected disabled>Seleccione</option>
                                    <?php foreach ($get_cfdi->result() as $item) {
                                        if($item->uso_cfdi==$uso_cfdi){
                                            echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled selected>'.$item->uso_cfdi_text.'</option>';
                                        }else{
                                            echo '<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                                        }
                                    }?>
                                </select>
                            </div>   
                        </div>
                    </div>
                </div>
            </form>  
            <div align="center">
                <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_datos_fiscales()">Guardar</button>
            </div>   
          </div>
          <div class="tab-pane fade" id="pagos-icon" role="tabpanel" aria-labelledby="pagos-icon-tab">
            <br>
            <form id="form_registro_pagos" method="post">
                <input type="hidden" id="idreg4" name="id" value="<?php echo $id ?>">
                <div class="row">
                    <!-- <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Número de días de crédito</label>
                            <div class="col-lg-5">
                                <input class="form-control" type="number" id="diascredito" name="diascredito" value="<?php // echo $diascredito ?>">
                            </div>
                        </div>
                    </div> -->
                    <?php 
                        $dias_saldo1='style="display:none"'; 
                        $dias_saldo2='style="display:none"'; 
                        if($diaspago>0){
                            $dias_saldo1='style="display:block"'; 
                            //$dias_saldo2='style="display:none"'; 
                        }
                        if($saldopago>0){
                            //$dias_saldo1='style="display:none"'; 
                            $dias_saldo2='style="display:block"'; 
                        }
                     ?>
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En días</label>
                            <div class="col-lg-1">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="radio" name="dias_saldo" id="dias_saldo" value="1" onclick="tipo_pago(1)" <?php if($dias_saldo==1) echo 'checked' ?>>
                                        <span></span>
                                    </label>
                                </span>
                            </div>   
                            <div class="col-lg-2 diaspagotxt" <?php echo $dias_saldo1 ?>>
                                <select class="form-control" id="diaspago" name="diaspago" required>
                                    <option value="0" selected disabled>Seleccionar días</option>
                                    <option value="8" <?php  if($diaspago==8) echo 'selected' ?>>8</option>
                                    <option value="15" <?php  if($diaspago==15) echo 'selected' ?>>15</option>
                                    <option value="30" <?php  if($diaspago==30) echo 'selected' ?>>30</option>
                                </select>
                            </div> 
                            <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En saldo</label>
                            <div class="col-lg-1">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="radio" name="dias_saldo" id="dias_saldo" value="2" onclick="tipo_pago(2)" <?php if($dias_saldo==2) echo 'checked' ?>>
                                        <span></span>
                                    </label>
                                </span>
                            </div>  
                            <div class="col-lg-2 saldopagotxt" <?php echo $dias_saldo2 ?>> 
                                <input class="form-control" type="number" id="saldopago" name="saldopago" value="<?php echo $saldopago ?>">
                            </div>  
                        </div>
                    </div>
                </div>    
            </form>          
            <div align="center">
                <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_pagos()">Guardar</button>
            </div>   
          </div>
          <div class="tab-pane fade" id="programa-icon" role="tabpanel" aria-labelledby="programa-icon-tab">
            <br>
            <form id="form_registro_usuario" method="post">
                <input type="hidden" id="idreg3" name="id" value="<?php echo $id ?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">ID cliente</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" disabled value="<?php echo $clave.$id ?>">
                            </div>   
                        </div>
                    </div> 
                </div>    
            </form><!-- 
            <div align="center">
                <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_usuario()">Guardar</button>
            </div>  -->
          </div>  
          <div class="tab-pane fade" id="contact-icon" role="tabpanel" aria-labelledby="contact-icon-tab">
            <br>
            <form id="form_registro_usuario" method="post">
                <input type="hidden" id="idreg3" name="id" value="<?php echo $id ?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()" value="<?php echo $usuario ?>">
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Contraseña</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password" oninput="" value="<?php echo $contrasena ?>">
                            </div>  
                            <div class="col-5">
                                <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                    <i class="icon-xl fas fa-eye" style="color: white"></i>
                                </a>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                            <div class="col-lg-4">
                                <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" value="<?php echo $contrasena2 ?>">
                            </div>   
                        </div>
                    </div> 
                </div>    
            </form>
            <div align="center">
                <button class="btn btn-primary btn_registro" type="button" style="width: 30%" onclick="guardar_registro_usuario()">Guardar</button>
            </div> 
          </div>  
        </div>
      </div>
    </div>
</div>    

<div class="modal fade" id="modal_notificacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="" id="exampleModalLabel" style="color:#019cde">¡Atención!</h1>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row g-3">
                <div class="col-12">
                    <h4>Para poder seleccionar una colonia, es necesario introducir el código postal</h4>
                </div>
            </div>
        </div>    
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<?php /*        
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">

            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <form id="form_registro" method="post">
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nombre del cliente</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>    

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Correo electrónico</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()" value="<?php echo $correo ?>">
                                        </div>   
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()" value="<?php echo $usuario ?>">
                                        </div>   
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Contraseña</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password" oninput="" value="<?php echo $contrasena ?>">
                                        </div>  
                                        <div class="col-5">
                                            <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                                <i class="icon-xl fas fa-eye" style="color: white"></i>
                                            </a>
                                        </div> 
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                                        <div class="col-lg-4">
                                            <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" value="<?php echo $contrasena2 ?>">
                                        </div>   
                                    </div>
                                </div> 
                            </div>    
                        </form>    
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-4">
                                <button class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar cliente</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
 */ ?>