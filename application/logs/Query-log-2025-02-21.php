SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0026171207427979 2025-02-21 13_12_07

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00053000450134277 2025-02-21 13_12_07

SELECT DISTINCT dat.*, GROUP_CONCAT(stock,'<td>', detalle ,'</td>' ORDER BY dat.orden ASC SEPARATOR '') as stock FROM(
            SELECT ps.sucursalid,s.orden,p.id, p.idProducto, p.nombre, p.codigoBarras, p.tipo, p.costo_compra, p.unidad_sat, p.servicioId_sat, p.incluye_iva_comp, p.iva_comp, p.isr,p.porc_isr, pp.codigo as cod_prov, pro.nombre as proveedor, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
                COALESCE(CASE 
                    WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT '<td>', COALESCE(ps.stock, ''), '</td>', '<td>', COALESCE(ps.stockmin, ''), '</td>', '<td>',COALESCE(ps.stockmax, ''),'</td>' SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td></td>', '<td></td>', '<td></td>') 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    -- tarda mucho la consulta así --
                    /*WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                                 FROM productos_sucursales_serie pss
                                 WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                                 AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                            ) 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )*/
                    /*WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td> </td>', '<td></td>', '<td></td>'
                            ) 
                            ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )*/
                    WHEN p.tipo = 2 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td></td>', '<td>0</td>', '<td>0</td>') 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
                END, CONCAT('<td></td>', '<td></td>', '<td></td>')) AS stock, 
                /*COALESCE(
                   GROUP_CONCAT(
                       DISTINCT CONCAT(
                           '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                        ORDER BY s.orden ASC 
                        SEPARATOR ''
                    ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                ) AS stock, */
                 GROUP_CONCAT(DISTINCT '(',ps.id,', ',ps.sucursalid, ', ',s.orden, ', ',ps.stock, ')' ORDER BY s.orden ASC ) as sucx, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, 
                 CASE
                    WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
                    WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
                    WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
                    ELSE ''
                END AS iva_txt, 
                CASE
                    WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
                    WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
                    WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
                    ELSE ''
                END AS iva_comp_txt, 
                CASE
                    WHEN p.tipo = 0 THEN 'Stock'
                    WHEN p.tipo = 1 THEN 'Serie'
                    WHEN p.tipo = 2 THEN 'Lote y caducidad'
                    WHEN p.tipo = 3 THEN 'Insumo'
                    WHEN p.tipo = 4 THEN 'Refacciones'
                    ELSE ''
                END AS tipo_prod, 
                CASE 
                    WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
                    WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
                    WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
                    WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
                    ELSE 0
                END AS costo_compra_iva, 
                CASE
                    WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
                    WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
                    WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
                    WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
                    ELSE 0
                END AS precio_con_iva, 
                CASE 
                    WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE('', '') 
                    WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT ' Serie: ', pss.serie SEPARATOR '<br>'), '')
                    WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT ' Lote: ', psl.lote, ' / ', psl.caducidad,' (stock: ',psl.cantidad,')' SEPARATOR '<br>'), '')
                END AS detalle, c.categoria
            FROM productos p
            LEFT JOIN categoria c ON c.categoriaId=p.categoria
            LEFT JOIN productos_proveedores pp ON pp.idproducto=p.id
            LEFT JOIN proveedores pro ON pro.id=pp.idproveedor
            JOIN sucursal s ON s.activo=1
            LEFT JOIN productos_sucursales ps ON ps.productoid=p.id and ps.activo=1 and ps.sucursalid=s.id
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND pss.sucursalid=ps.sucursalid AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND p.tipo = 1
            LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND psl.sucursalid=ps.sucursalid AND psl.activo = 1 AND p.tipo = 2 and psl.cantidad > 0
            WHERE (p.activo = 1 OR p.activo = 'Y')
            AND s.activo = '1'
            GROUP BY p.id,s.id
            ORDER BY ps.sucursalid ASC, pss.sucursalid ASC, psl.sucursalid ASC, s.orden ASC, p.idProducto ASC
            ) as dat
            GROUP by id 
 Execution Time:5.3363869190216 2025-02-21 13_12_07

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0050909519195557 2025-02-21 16_35_32

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00068497657775879 2025-02-21 16_35_32

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.0011880397796631 2025-02-21 16_35_32

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0019891262054443 2025-02-21 16_35_32

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.001917839050293 2025-02-21 16_35_32

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0032470226287842 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0016419887542725 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013649463653564 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00037312507629395 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00092220306396484 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00037908554077148 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080013275146484 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026583671569824 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010249614715576 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0003809928894043 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059318542480469 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025296211242676 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00039196014404297 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088191032409668 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011169910430908 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00090599060058594 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037693977355957 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057792663574219 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065016746520996 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036287307739258 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013349056243896 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014047622680664 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00030612945556641 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058579444885254 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034999847412109 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062203407287598 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006251335144043 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003349781036377 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059199333190918 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00078010559082031 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025796890258789 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073909759521484 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003359317779541 2025-02-21 16_35_32

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.001474142074585 2025-02-21 16_35_32


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069594383239746 2025-02-21 16_35_32

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028014183044434 2025-02-21 16_35_32

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.018212080001831 2025-02-21 16_35_32

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0016288757324219 2025-02-21 16_35_34

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00089001655578613 2025-02-21 16_35_34

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.021296977996826 2025-02-21 16_35_34

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0011940002441406 2025-02-21 16_35_34

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0025589466094971 2025-02-21 16_35_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0023839473724365 2025-02-21 16_35_34

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0081591606140137 2025-02-21 16_35_34

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00049996376037598 2025-02-21 16_35_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0032649040222168 2025-02-21 16_35_35

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0015058517456055 2025-02-21 16_35_35

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00064802169799805 2025-02-21 16_35_35

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0041098594665527 2025-02-21 16_38_11

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00066113471984863 2025-02-21 16_38_11

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00052499771118164 2025-02-21 16_38_11

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00051784515380859 2025-02-21 16_38_11

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0005500316619873 2025-02-21 16_38_11

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.000885009765625 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075602531433105 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00090408325195312 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0004429817199707 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006861686706543 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00044608116149902 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.000701904296875 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031900405883789 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067496299743652 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076580047607422 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00032210350036621 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0014681816101074 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040316581726074 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00029897689819336 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010240077972412 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00099706649780273 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00092101097106934 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00052309036254883 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076794624328613 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008690357208252 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00046515464782715 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081706047058105 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00047206878662109 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073695182800293 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00048303604125977 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072813034057617 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076198577880859 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037717819213867 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053811073303223 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065112113952637 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037598609924316 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011727809906006 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081086158752441 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00055718421936035 2025-02-21 16_38_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00084114074707031 2025-02-21 16_38_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066113471984863 2025-02-21 16_38_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043106079101562 2025-02-21 16_38_11

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048398971557617 2025-02-21 16_38_11

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00058889389038086 2025-02-21 16_38_11

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00075387954711914 2025-02-21 16_38_11

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.021910905838013 2025-02-21 16_38_11

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00066113471984863 2025-02-21 16_38_11

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00085115432739258 2025-02-21 16_38_11

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0019829273223877 2025-02-21 16_38_12

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00091004371643066 2025-02-21 16_38_12

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00034093856811523 2025-02-21 16_38_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.010773897171021 2025-02-21 16_38_12

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0014610290527344 2025-02-21 16_38_12

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00049996376037598 2025-02-21 16_38_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0037949085235596 2025-02-21 16_41_55

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.0007929801940918 2025-02-21 16_41_55

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00061416625976562 2025-02-21 16_41_55

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0010879039764404 2025-02-21 16_41_55

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00078916549682617 2025-02-21 16_41_55

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00118088722229 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00094008445739746 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010631084442139 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00054407119750977 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007469654083252 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00050592422485352 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076413154602051 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00051212310791016 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007469654083252 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00088715553283691 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00068902969360352 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006709098815918 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039410591125488 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0002439022064209 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0016670227050781 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00084185600280762 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087213516235352 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039005279541016 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060796737670898 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081896781921387 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040984153747559 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068306922912598 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084686279296875 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00037288665771484 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00096511840820312 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0004270076751709 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006260871887207 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073885917663574 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037693977355957 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062298774719238 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075817108154297 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00038003921508789 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060701370239258 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007789134979248 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0010271072387695 2025-02-21 16_41_55

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070285797119141 2025-02-21 16_41_55


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069999694824219 2025-02-21 16_41_55

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00041604042053223 2025-02-21 16_41_55

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00068497657775879 2025-02-21 16_41_55

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00051999092102051 2025-02-21 16_41_56

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00079512596130371 2025-02-21 16_41_56

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.022694826126099 2025-02-21 16_41_56

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00074911117553711 2025-02-21 16_41_56

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0012519359588623 2025-02-21 16_41_56

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022311210632324 2025-02-21 16_41_56

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0011351108551025 2025-02-21 16_41_56

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00028896331787109 2025-02-21 16_41_56

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0028200149536133 2025-02-21 16_41_56

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0012688636779785 2025-02-21 16_41_56

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0017800331115723 2025-02-21 16_41_56

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022408962249756 2025-02-21 16_42_14

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00039887428283691 2025-02-21 16_42_14

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00040602684020996 2025-02-21 16_42_14

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00055789947509766 2025-02-21 16_42_14

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00037813186645508 2025-02-21 16_42_14

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00083804130554199 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062012672424316 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00064682960510254 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00030398368835449 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005500316619873 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00029802322387695 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052309036254883 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031495094299316 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004420280456543 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054502487182617 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00025391578674316 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038385391235352 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020909309387207 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023889541625977 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042915344238281 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00046110153198242 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044918060302734 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019192695617676 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030207633972168 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00039815902709961 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019502639770508 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049710273742676 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047612190246582 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00022697448730469 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0021250247955322 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00039386749267578 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044012069702148 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018596649169922 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033307075500488 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052285194396973 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020694732666016 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040912628173828 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00036978721618652 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023102760314941 2025-02-21 16_42_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034403800964355 2025-02-21 16_42_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043797492980957 2025-02-21 16_42_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019192695617676 2025-02-21 16_42_14

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00046110153198242 2025-02-21 16_42_14

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00068020820617676 2025-02-21 16_42_15

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00047397613525391 2025-02-21 16_42_15

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.021585941314697 2025-02-21 16_42_15

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00064301490783691 2025-02-21 16_42_15

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0015668869018555 2025-02-21 16_42_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.028091192245483 2025-02-21 16_42_15

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0018429756164551 2025-02-21 16_42_15

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00041890144348145 2025-02-21 16_42_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0035459995269775 2025-02-21 16_42_15

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0019350051879883 2025-02-21 16_42_15

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0005040168762207 2025-02-21 16_42_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0028088092803955 2025-02-21 16_42_25

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.0004889965057373 2025-02-21 16_42_25

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00029587745666504 2025-02-21 16_42_25

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00035715103149414 2025-02-21 16_42_25

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00033807754516602 2025-02-21 16_42_25

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00059294700622559 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00067591667175293 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00026202201843262 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044393539428711 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00023007392883301 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00039005279541016 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016403198242188 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040102005004883 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00049185752868652 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00068283081054688 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089287757873535 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0012989044189453 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00045394897460938 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075912475585938 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052094459533691 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005650520324707 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022196769714355 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037693977355957 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042581558227539 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020599365234375 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034904479980469 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037813186645508 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00020909309387207 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034403800964355 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001528263092041 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029301643371582 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034499168395996 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014686584472656 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027799606323242 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034809112548828 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018787384033203 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033783912658691 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00033402442932129 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017809867858887 2025-02-21 16_42_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026798248291016 2025-02-21 16_42_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00027203559875488 2025-02-21 16_42_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016498565673828 2025-02-21 16_42_25

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004270076751709 2025-02-21 16_42_25

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0010368824005127 2025-02-21 16_42_25

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00096511840820312 2025-02-21 16_42_25

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.037616014480591 2025-02-21 16_42_25

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00061607360839844 2025-02-21 16_42_25

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00075101852416992 2025-02-21 16_42_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0017080307006836 2025-02-21 16_42_26

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00088906288146973 2025-02-21 16_42_26

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0003359317779541 2025-02-21 16_42_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022759437561035 2025-02-21 16_42_26

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0012409687042236 2025-02-21 16_42_26

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00029087066650391 2025-02-21 16_42_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0015900135040283 2025-02-21 16_43_52

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '0'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00077199935913086 2025-02-21 16_43_52

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '0'
AND `p`.`activo` = 1 
 Execution Time:0.00042605400085449 2025-02-21 16_43_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0034000873565674 2025-02-21 16_43_52

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '0'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0018370151519775 2025-02-21 16_43_52

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '0'
AND `p`.`activo` = 1 
 Execution Time:0.00030398368835449 2025-02-21 16_43_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0038619041442871 2025-02-21 16_43_54

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0016539096832275 2025-02-21 16_43_54

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00059008598327637 2025-02-21 16_43_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.00370192527771 2025-02-21 16_43_54

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0011107921600342 2025-02-21 16_43_54

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00062012672424316 2025-02-21 16_43_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0026459693908691 2025-02-21 16_49_39

SELECT *
FROM `proveedores`
WHERE `id` = '1' 
 Execution Time:0.0005500316619873 2025-02-21 16_49_39

SELECT *
FROM `f_regimenfiscal`
WHERE `activo` = 1 
 Execution Time:0.0018620491027832 2025-02-21 16_49_39

SELECT *
FROM `f_uso_cfdi`
WHERE `activo` = 1 
 Execution Time:0.00082898139953613 2025-02-21 16_49_39

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0003359317779541 2025-02-21 16_49_39

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00028419494628906 2025-02-21 16_49_39

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00068211555480957 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052380561828613 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004889965057373 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0001990795135498 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032496452331543 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00024008750915527 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043201446533203 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019598007202148 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069189071655273 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055599212646484 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00028896331787109 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056219100952148 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021219253540039 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00020980834960938 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035810470581055 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043606758117676 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042104721069336 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019097328186035 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036978721618652 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038480758666992 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018787384033203 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036501884460449 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043106079101562 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00021886825561523 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049400329589844 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021600723266602 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003969669342041 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034999847412109 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026106834411621 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031495094299316 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00048589706420898 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019097328186035 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004270076751709 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00057196617126465 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002129077911377 2025-02-21 16_49_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041484832763672 2025-02-21 16_49_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003960132598877 2025-02-21 16_49_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014495849609375 2025-02-21 16_49_39

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00080204010009766 2025-02-21 16_49_39

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.000518798828125 2025-02-21 16_49_40

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00075793266296387 2025-02-21 16_49_40

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.035315990447998 2025-02-21 16_49_40

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00071001052856445 2025-02-21 16_49_40

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.001072883605957 2025-02-21 16_49_41

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0027859210968018 2025-02-21 16_54_50

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00037598609924316 2025-02-21 16_54_50

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00026297569274902 2025-02-21 16_54_50

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0003509521484375 2025-02-21 16_54_50

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00027012825012207 2025-02-21 16_54_50

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00060200691223145 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004580020904541 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00049185752868652 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00018620491027832 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059008598327637 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0020499229431152 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012671947479248 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030207633972168 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061202049255371 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006108283996582 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00025701522827148 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051593780517578 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025010108947754 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00024008750915527 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00039005279541016 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048398971557617 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00049495697021484 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019383430480957 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034594535827637 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00046300888061523 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002291202545166 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004580020904541 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043392181396484 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00019001960754395 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033998489379883 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018310546875 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031280517578125 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038695335388184 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021100044250488 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034594535827637 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038790702819824 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016903877258301 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033307075500488 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065207481384277 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022578239440918 2025-02-21 16_54_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041604042053223 2025-02-21 16_54_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041413307189941 2025-02-21 16_54_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00051403045654297 2025-02-21 16_54_50

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00040507316589355 2025-02-21 16_54_50

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0003809928894043 2025-02-21 16_54_52

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0011579990386963 2025-02-21 16_54_52

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.018263816833496 2025-02-21 16_54_52

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00067305564880371 2025-02-21 16_54_52

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00092697143554688 2025-02-21 16_54_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0028069019317627 2025-02-21 16_54_53

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00105881690979 2025-02-21 16_54_53

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00024509429931641 2025-02-21 16_54_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0041661262512207 2025-02-21 16_54_53

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0032031536102295 2025-02-21 16_54_53

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0053360462188721 2025-02-21 16_54_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0033180713653564 2025-02-21 16_55_50

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00070691108703613 2025-02-21 16_55_50

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00067806243896484 2025-02-21 16_55_50

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0011250972747803 2025-02-21 16_55_50

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00099682807922363 2025-02-21 16_55_50

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.001708984375 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013890266418457 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013799667358398 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00058293342590332 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010318756103516 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00057411193847656 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0017900466918945 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00086092948913574 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0015089511871338 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014529228210449 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0005500316619873 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010008811950684 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044798851013184 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0005640983581543 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077199935913086 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072598457336426 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076413154602051 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039005279541016 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044703483581543 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066304206848145 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027298927307129 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068306922912598 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00078105926513672 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00039505958557129 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0017910003662109 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0013220310211182 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013139247894287 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011589527130127 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00062894821166992 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076103210449219 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011649131774902 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00058698654174805 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00097489356994629 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010979175567627 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0005340576171875 2025-02-21 16_55_50

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068998336791992 2025-02-21 16_55_50


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00095915794372559 2025-02-21 16_55_50

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00055408477783203 2025-02-21 16_55_50

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.001737117767334 2025-02-21 16_55_50

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00091695785522461 2025-02-21 16_55_51

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00092291831970215 2025-02-21 16_55_51

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.036550045013428 2025-02-21 16_55_51

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00068497657775879 2025-02-21 16_55_51

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0013940334320068 2025-02-21 16_55_51

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0036180019378662 2025-02-21 16_55_52

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0079989433288574 2025-02-21 16_55_52

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0031390190124512 2025-02-21 16_55_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0045878887176514 2025-02-21 16_55_52

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0016310214996338 2025-02-21 16_55_52

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0004279613494873 2025-02-21 16_55_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022609233856201 2025-02-21 16_55_54

SELECT `pp`.`codigo` as `cod_provee`, `p`.`idProducto`, `p`.`nombre`, `p`.`tipo`, `pro`.`nombre` as `provee`, `pro`.`codigo` as `codigo_pro`, CASE 
                WHEN p.incluye_iva_comp = 1 and iva_comp=16 THEN (p.costo_compra*1.16) 
                ELSE p.costo_compra
            END AS costo_compra, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN p.tipo =2 
                THEN (
                    IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_lotes, CASE 
                WHEN p.tipo =1 
                THEN (
                    IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 AND pss.disponible=1 AND pss.productoid=p.id and pss.vendido=0), 0)
                ) 
                ELSE 0 
            END as stock_series, CASE 
                WHEN p.tipo = 0
                THEN (
                    IFNULL((SELECT sum(stock) FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_ps
FROM `productos_proveedores` `pp`
JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos` `p` ON `p`.`id`=`pp`.`idproducto` and `p`.`estatus`=1
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2 and `ps`.`activo`=1
WHERE `pp`.`idproveedor` = '0'
AND `pp`.`activo` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto` ASC 
 Execution Time:0.0041460990905762 2025-02-21 16_55_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0032248497009277 2025-02-21 16_55_54

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.001086950302124 2025-02-21 16_55_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.054374933242798 2025-02-21 16_55_54

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.042804002761841 2025-02-21 16_55_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0027520656585693 2025-02-21 16_56_11

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.0037539005279541 2025-02-21 16_56_11

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0015790462493896 2025-02-21 16_56_20

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.00077295303344727 2025-02-21 16_56_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022988319396973 2025-02-21 16_56_30

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00041794776916504 2025-02-21 16_56_30

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00032687187194824 2025-02-21 16_56_30

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00064301490783691 2025-02-21 16_56_30

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00099086761474609 2025-02-21 16_56_30

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0012459754943848 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088810920715332 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00053811073303223 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00022101402282715 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047993659973145 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00026798248291016 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035500526428223 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001990795135498 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042986869812012 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00045490264892578 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00020599365234375 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041890144348145 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024008750915527 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00017285346984863 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041389465332031 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042200088500977 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041508674621582 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025701522827148 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034785270690918 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00036907196044922 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022506713867188 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033903121948242 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005040168762207 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0002589225769043 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063395500183105 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017213821411133 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035405158996582 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037908554077148 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018596649169922 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038480758666992 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052285194396973 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003509521484375 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043487548828125 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00048112869262695 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026702880859375 2025-02-21 16_56_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041103363037109 2025-02-21 16_56_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003509521484375 2025-02-21 16_56_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026297569274902 2025-02-21 16_56_30

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00036311149597168 2025-02-21 16_56_30

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0010089874267578 2025-02-21 16_56_31

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00035810470581055 2025-02-21 16_56_31

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.044476985931396 2025-02-21 16_56_31

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00057315826416016 2025-02-21 16_56_31

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0011289119720459 2025-02-21 16_56_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022921562194824 2025-02-21 16_56_31

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.002047061920166 2025-02-21 16_56_31

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00032496452331543 2025-02-21 16_56_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0035150051116943 2025-02-21 16_56_31

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0016109943389893 2025-02-21 16_56_31

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00061798095703125 2025-02-21 16_56_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0021419525146484 2025-02-21 16_56_33

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.0017578601837158 2025-02-21 16_56_33

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0010690689086914 2025-02-21 17_18_11

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00036001205444336 2025-02-21 17_18_11

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00012993812561035 2025-02-21 17_18_11

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00022006034851074 2025-02-21 17_18_11

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00015711784362793 2025-02-21 17_18_11

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00084590911865234 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054502487182617 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00046205520629883 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00023913383483887 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033807754516602 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00054311752319336 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005030632019043 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019001960754395 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084495544433594 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00044107437133789 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076103210449219 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032711029052734 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00040411949157715 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071382522583008 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074100494384766 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087690353393555 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00041604042053223 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057697296142578 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00077390670776367 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032401084899902 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064682960510254 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00079607963562012 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00041604042053223 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064682960510254 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030207633972168 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064396858215332 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072312355041504 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043702125549316 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063109397888184 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076198577880859 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024104118347168 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020599365234375 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051403045654297 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016903877258301 2025-02-21 17_18_11

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00025796890258789 2025-02-21 17_18_11


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00056982040405273 2025-02-21 17_18_11

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020694732666016 2025-02-21 17_18_11

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00038909912109375 2025-02-21 17_18_11

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00082516670227051 2025-02-21 17_18_11

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00079607963562012 2025-02-21 17_18_11

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.012132883071899 2025-02-21 17_18_11

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00042891502380371 2025-02-21 17_18_11

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0017569065093994 2025-02-21 17_18_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0029768943786621 2025-02-21 17_18_12

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0012979507446289 2025-02-21 17_18_12

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.0019729137420654 2025-02-21 17_18_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0071120262145996 2025-02-21 17_18_12

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0013961791992188 2025-02-21 17_18_12

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00056290626525879 2025-02-21 17_18_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0018310546875 2025-02-21 17_18_12

SELECT `pp`.`codigo` as `cod_provee`, `p`.`idProducto`, `p`.`nombre`, `p`.`tipo`, `pro`.`nombre` as `provee`, `pro`.`codigo` as `codigo_pro`, CASE 
                WHEN p.incluye_iva_comp = 1 and iva_comp=16 THEN (p.costo_compra*1.16) 
                ELSE p.costo_compra
            END AS costo_compra, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN p.tipo =2 
                THEN (
                    IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_lotes, CASE 
                WHEN p.tipo =1 
                THEN (
                    IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 AND pss.disponible=1 AND pss.productoid=p.id and pss.vendido=0), 0)
                ) 
                ELSE 0 
            END as stock_series, CASE 
                WHEN p.tipo = 0
                THEN (
                    IFNULL((SELECT sum(stock) FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_ps
FROM `productos_proveedores` `pp`
JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos` `p` ON `p`.`id`=`pp`.`idproducto` and `p`.`estatus`=1
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2 and `ps`.`activo`=1
WHERE `pp`.`idproveedor` = '0'
AND `pp`.`activo` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto` ASC 
 Execution Time:0.00092101097106934 2025-02-21 17_18_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0028541088104248 2025-02-21 17_18_13

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.00079607963562012 2025-02-21 17_18_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0039699077606201 2025-02-21 17_18_13

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `estatus` = 1
AND `p`.`activo` = 1 
 Execution Time:0.00089716911315918 2025-02-21 17_18_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0013189315795898 2025-02-21 17_20_42

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00030994415283203 2025-02-21 17_20_42

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00025606155395508 2025-02-21 17_20_42

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0006711483001709 2025-02-21 17_20_42

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00091195106506348 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068902969360352 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001025915145874 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00069403648376465 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012149810791016 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00069689750671387 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012338161468506 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031495094299316 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050806999206543 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052094459533691 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00017809867858887 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061607360839844 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0005340576171875 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00041508674621582 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055384635925293 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088000297546387 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00027608871459961 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001521110534668 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022006034851074 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022697448730469 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014686584472656 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024700164794922 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023794174194336 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0001521110534668 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00021791458129883 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014114379882812 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020694732666016 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022292137145996 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014901161193848 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00021910667419434 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00014686584472656 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.103515625E-5 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011491775512695 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012683868408203 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.6982040405273E-5 2025-02-21 17_20_42

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011181831359863 2025-02-21 17_20_42


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011992454528809 2025-02-21 17_20_42

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.4121017456055E-5 2025-02-21 17_20_42

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00017619132995605 2025-02-21 17_20_42

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0003960132598877 2025-02-21 17_20_42

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00049591064453125 2025-02-21 17_20_42

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.023644924163818 2025-02-21 17_20_42

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0007011890411377 2025-02-21 17_20_42

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.028288125991821 2025-02-21 17_20_42

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0012998580932617 2025-02-21 17_20_43

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0012550354003906 2025-02-21 17_20_43

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.0034711360931396 2025-02-21 17_20_43

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021100044250488 2025-02-21 17_20_43

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00048112869262695 2025-02-21 17_20_43

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00032186508178711 2025-02-21 17_20_43

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0031840801239014 2025-02-21 17_20_47

SELECT p.id,p.nombre,p.usuario,p.motivo,p.desactivar,p.saldo,s.clave
            FROM clientes AS p
            INNER JOIN sucursal AS s ON s.id=p.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=p.id
            WHERE p.activo=1 
 Execution Time:0.0016458034515381 2025-02-21 17_20_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0009300708770752 2025-02-21 17_23_55

SELECT p.id,p.nombre,p.usuario,p.motivo,p.desactivar,p.saldo,s.clave
            FROM clientes AS p
            INNER JOIN sucursal AS s ON s.id=p.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=p.id
            WHERE p.activo=1 
 Execution Time:0.00071001052856445 2025-02-21 17_23_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0033690929412842 2025-02-21 17_25_27

SELECT *
FROM `clientes`
WHERE `id` = '1' 
 Execution Time:0.0039510726928711 2025-02-21 17_25_27

SELECT *
FROM `sucursal`
WHERE `id` = '2' 
 Execution Time:0.00053095817565918 2025-02-21 17_25_27

SELECT *
FROM `cliente_fiscales`
WHERE `idcliente` = '1' 
 Execution Time:0.00084996223449707 2025-02-21 17_25_27

SELECT *
FROM `estado` 
 Execution Time:0.0014939308166504 2025-02-21 17_25_27

SELECT *
FROM `f_regimenfiscal`
WHERE `activo` = 1 
 Execution Time:0.00075197219848633 2025-02-21 17_25_27

SELECT *
FROM `f_uso_cfdi`
WHERE `activo` = 1 
 Execution Time:0.00036501884460449 2025-02-21 17_25_27

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00059700012207031 2025-02-21 17_25_27

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00042200088500977 2025-02-21 17_25_27

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00095295906066895 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00078797340393066 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001230001449585 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00052094459533691 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077986717224121 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00029897689819336 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060009956359863 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040006637573242 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070309638977051 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010209083557129 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00027918815612793 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071883201599121 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017714500427246 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0002439022064209 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028109550476074 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029706954956055 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00058388710021973 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00052309036254883 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072503089904785 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00049901008605957 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012588500976562 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023698806762695 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00032186508178711 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00014805793762207 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020098686218262 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024914741516113 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047397613525391 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00028419494628906 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001380443572998 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018405914306641 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0002601146697998 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019288063049316 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002899169921875 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031208992004395 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018692016601562 2025-02-21 17_25_27

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026798248291016 2025-02-21 17_25_27


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00030708312988281 2025-02-21 17_25_27

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017786026000977 2025-02-21 17_25_27

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00076103210449219 2025-02-21 17_25_27

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0005638599395752 2025-02-21 17_25_27

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00072097778320312 2025-02-21 17_25_27

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.033200979232788 2025-02-21 17_25_27

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0010831356048584 2025-02-21 17_25_27

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0014550685882568 2025-02-21 17_25_27

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0034890174865723 2025-02-21 17_29_38

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00084614753723145 2025-02-21 17_29_38

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00082898139953613 2025-02-21 17_29_38

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00071811676025391 2025-02-21 17_29_38

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011460781097412 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.001039981842041 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013740062713623 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00026178359985352 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011999607086182 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00027203559875488 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00039315223693848 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018000602722168 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043010711669922 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063896179199219 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00029420852661133 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057506561279297 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029301643371582 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0004119873046875 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054311752319336 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062298774719238 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072908401489258 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026202201843262 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047206878662109 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062084197998047 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025391578674316 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049519538879395 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00067281723022461 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00026988983154297 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049614906311035 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025415420532227 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044918060302734 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060200691223145 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025796890258789 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047898292541504 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061798095703125 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026512145996094 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047683715820312 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059294700622559 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040006637573242 2025-02-21 17_29_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064301490783691 2025-02-21 17_29_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044918060302734 2025-02-21 17_29_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015807151794434 2025-02-21 17_29_38

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00026702880859375 2025-02-21 17_29_38

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00025200843811035 2025-02-21 17_29_39

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00063514709472656 2025-02-21 17_29_39

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029837131500244 2025-02-21 17_29_39

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0003809928894043 2025-02-21 17_29_39

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0013308525085449 2025-02-21 17_29_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0040898323059082 2025-02-21 17_29_39

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0013530254364014 2025-02-21 17_29_39

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00095391273498535 2025-02-21 17_29_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021710395812988 2025-02-21 17_29_39

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0005340576171875 2025-02-21 17_29_39

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.0004279613494873 2025-02-21 17_29_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0035429000854492 2025-02-21 17_29_39

SELECT *
FROM `clientes`
WHERE `id` = '1' 
 Execution Time:0.00046801567077637 2025-02-21 17_29_39

SELECT *
FROM `sucursal`
WHERE `id` = '2' 
 Execution Time:0.00031208992004395 2025-02-21 17_29_39

SELECT *
FROM `cliente_fiscales`
WHERE `idcliente` = '1' 
 Execution Time:0.00026202201843262 2025-02-21 17_29_39

SELECT *
FROM `estado` 
 Execution Time:0.00024318695068359 2025-02-21 17_29_39

SELECT *
FROM `f_regimenfiscal`
WHERE `activo` = 1 
 Execution Time:0.00027585029602051 2025-02-21 17_29_39

SELECT *
FROM `f_uso_cfdi`
WHERE `activo` = 1 
 Execution Time:0.00013113021850586 2025-02-21 17_29_39

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00024819374084473 2025-02-21 17_29_39

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00019311904907227 2025-02-21 17_29_39

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00041699409484863 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002129077911377 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063681602478027 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00012111663818359 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00015711784362793 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00016498565673828 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001680850982666 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018692016601562 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034499168395996 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043487548828125 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:9.4890594482422E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018119812011719 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.0122222900391E-5 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:5.6982040405273E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042891502380371 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037789344787598 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003209114074707 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001521110534668 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050902366638184 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059795379638672 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019383430480957 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00017404556274414 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031805038452148 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00010395050048828 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00013899803161621 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.6028366088867E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010013580322266 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012397766113281 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.3180923461914E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010204315185547 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012087821960449 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.3167343139648E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010204315185547 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0001220703125 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.5074691772461E-5 2025-02-21 17_29_39

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010919570922852 2025-02-21 17_29_39


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011706352233887 2025-02-21 17_29_39

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.1975250244141E-5 2025-02-21 17_29_39

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00015616416931152 2025-02-21 17_29_39

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00051593780517578 2025-02-21 17_29_40

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00142502784729 2025-02-21 17_29_40

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.024518013000488 2025-02-21 17_29_40

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00076198577880859 2025-02-21 17_29_40

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00069308280944824 2025-02-21 17_29_40

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021190643310547 2025-02-21 17_43_51

SELECT *
FROM `clientes`
WHERE `id` = '1' 
 Execution Time:0.00042510032653809 2025-02-21 17_43_51

SELECT *
FROM `sucursal`
WHERE `id` = '2' 
 Execution Time:0.00037193298339844 2025-02-21 17_43_51

SELECT *
FROM `cliente_fiscales`
WHERE `idcliente` = '1' 
 Execution Time:0.00028300285339355 2025-02-21 17_43_51

SELECT *
FROM `estado` 
 Execution Time:0.00024819374084473 2025-02-21 17_43_51

SELECT *
FROM `f_regimenfiscal`
WHERE `activo` = 1 
 Execution Time:0.00029611587524414 2025-02-21 17_43_51

SELECT *
FROM `f_uso_cfdi`
WHERE `activo` = 1 
 Execution Time:0.00033402442932129 2025-02-21 17_43_51

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00039792060852051 2025-02-21 17_43_51

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00029301643371582 2025-02-21 17_43_51

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00048708915710449 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036406517028809 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029587745666504 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0001370906829834 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020503997802734 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00011992454528809 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00017809867858887 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:8.5115432739258E-5 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00016093254089355 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0002448558807373 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00013113021850586 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003349781036377 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018405914306641 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00022101402282715 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055408477783203 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036311149597168 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047588348388672 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019288063049316 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038290023803711 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00032591819763184 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024199485778809 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024890899658203 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00025200843811035 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00011897087097168 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069594383239746 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021791458129883 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056982040405273 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055384635925293 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025796890258789 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043988227844238 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062894821166992 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023293495178223 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038290023803711 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022697448730469 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014209747314453 2025-02-21 17_43_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00019001960754395 2025-02-21 17_43_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005490779876709 2025-02-21 17_43_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023102760314941 2025-02-21 17_43_51

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050592422485352 2025-02-21 17_43_51

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00049805641174316 2025-02-21 17_43_51

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00060796737670898 2025-02-21 17_43_51

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011785984039307 2025-02-21 17_43_51

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00050806999206543 2025-02-21 17_43_51

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00078701972961426 2025-02-21 17_43_51

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0033688545227051 2025-02-21 17_43_52

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.0007319450378418 2025-02-21 17_43_52

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00093221664428711 2025-02-21 17_43_52

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00084090232849121 2025-02-21 17_43_52

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0012900829315186 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010731220245361 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010349750518799 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00027203559875488 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053501129150391 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00050210952758789 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00091695785522461 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050997734069824 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00082707405090332 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010831356048584 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00051999092102051 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010299682617188 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043296813964844 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00043201446533203 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089097023010254 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00082182884216309 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010008811950684 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027704238891602 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056099891662598 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086879730224609 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025010108947754 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062704086303711 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073480606079102 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00025010108947754 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053191184997559 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024294853210449 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047898292541504 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010879039764404 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028491020202637 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049996376037598 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00050711631774902 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027894973754883 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075793266296387 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00058507919311523 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018215179443359 2025-02-21 17_43_52

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049185752868652 2025-02-21 17_43_52


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062084197998047 2025-02-21 17_43_52

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021195411682129 2025-02-21 17_43_52

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050091743469238 2025-02-21 17_43_52

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00029516220092773 2025-02-21 17_43_52

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00088715553283691 2025-02-21 17_43_52

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.023833036422729 2025-02-21 17_43_52

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00020694732666016 2025-02-21 17_43_52

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00045394897460938 2025-02-21 17_43_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0044598579406738 2025-02-21 17_43_52

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00075507164001465 2025-02-21 17_43_52

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00055599212646484 2025-02-21 17_43_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0031390190124512 2025-02-21 17_43_52

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0010719299316406 2025-02-21 17_43_52

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00060105323791504 2025-02-21 17_43_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.003242015838623 2025-02-21 17_44_10

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0021328926086426 2025-02-21 17_44_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0035629272460938 2025-02-21 17_44_44

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0018398761749268 2025-02-21 17_44_44

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0039000511169434 2025-02-21 17_44_55

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0020918846130371 2025-02-21 17_44_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0019299983978271 2025-02-21 17_45_06

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0010449886322021 2025-02-21 17_45_06

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0035789012908936 2025-02-21 17_45_17

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0020761489868164 2025-02-21 17_45_17

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0016849040985107 2025-02-21 17_47_48

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0011959075927734 2025-02-21 17_47_48

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0036981105804443 2025-02-21 17_48_01

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0019750595092773 2025-02-21 17_48_01

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0032138824462891 2025-02-21 17_48_20

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0018248558044434 2025-02-21 17_48_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.003154993057251 2025-02-21 17_49_43

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, rf.descripcion as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0016951560974121 2025-02-21 17_49_43

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0037508010864258 2025-02-21 17_51_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00057101249694824 2025-02-21 17_51_23

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00055384635925293 2025-02-21 17_51_23

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00050187110900879 2025-02-21 17_51_23

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00099396705627441 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00081110000610352 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086283683776855 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00027298927307129 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054192543029785 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00025010108947754 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005340576171875 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071001052856445 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00025582313537598 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051999092102051 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028896331787109 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00019311904907227 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050497055053711 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056695938110352 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008549690246582 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025510787963867 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050091743469238 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072598457336426 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027108192443848 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059199333190918 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011460781097412 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00036811828613281 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007481575012207 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034594535827637 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049400329589844 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00067305564880371 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027704238891602 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032901763916016 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00028085708618164 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00011014938354492 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050091743469238 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051021575927734 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-21 17_51_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041294097900391 2025-02-21 17_51_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055098533630371 2025-02-21 17_51_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024318695068359 2025-02-21 17_51_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004880428314209 2025-02-21 17_51_23

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00052404403686523 2025-02-21 17_51_24

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00041413307189941 2025-02-21 17_51_24

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.017423152923584 2025-02-21 17_51_24

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00081300735473633 2025-02-21 17_51_24

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0013930797576904 2025-02-21 17_51_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0020298957824707 2025-02-21 17_51_24

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0009770393371582 2025-02-21 17_51_24

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00092101097106934 2025-02-21 17_51_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021970272064209 2025-02-21 17_51_24

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00055289268493652 2025-02-21 17_51_24

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00037097930908203 2025-02-21 17_51_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0012199878692627 2025-02-21 17_51_54

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00035405158996582 2025-02-21 17_51_54

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00022697448730469 2025-02-21 17_51_54

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00022697448730469 2025-02-21 17_51_54

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00023388862609863 2025-02-21 17_51_54

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00069499015808105 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011050701141357 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055599212646484 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00021195411682129 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030612945556641 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00018501281738281 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002751350402832 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001678466796875 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059986114501953 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076079368591309 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00037598609924316 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074505805969238 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036811828613281 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00024509429931641 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045204162597656 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033283233642578 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003972053527832 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025200843811035 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023293495178223 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00024199485778809 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.7990036010742E-5 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040006637573242 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041294097900391 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00019192695617676 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032687187194824 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020599365234375 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028514862060547 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001518726348877 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028109550476074 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037407875061035 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018000602722168 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028896331787109 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034403800964355 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.7036361694336E-5 2025-02-21 17_51_54

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023698806762695 2025-02-21 17_51_54


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00028300285339355 2025-02-21 17_51_54

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.7036361694336E-5 2025-02-21 17_51_54

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00034403800964355 2025-02-21 17_51_54

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00079107284545898 2025-02-21 17_51_55

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00051999092102051 2025-02-21 17_51_55

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.018677949905396 2025-02-21 17_51_55

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00056099891662598 2025-02-21 17_51_55

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00098299980163574 2025-02-21 17_51_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0019981861114502 2025-02-21 17_51_55

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0011398792266846 2025-02-21 17_51_55

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00028896331787109 2025-02-21 17_51_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.024461030960083 2025-02-21 17_51_55

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.005040168762207 2025-02-21 17_51_55

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00044107437133789 2025-02-21 17_51_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0032100677490234 2025-02-21 17_52_00

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00048708915710449 2025-02-21 17_52_00

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00047898292541504 2025-02-21 17_52_00

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00035381317138672 2025-02-21 17_52_00

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011289119720459 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011188983917236 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010838508605957 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00045299530029297 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067687034606934 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00037980079650879 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073599815368652 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037193298339844 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071597099304199 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055599212646484 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00027203559875488 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033187866210938 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00033307075500488 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070905685424805 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058603286743164 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071310997009277 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037503242492676 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055503845214844 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063800811767578 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025296211242676 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044989585876465 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060892105102539 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0002140998840332 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035691261291504 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027704238891602 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035405158996582 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041699409484863 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020503997802734 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035405158996582 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038409233093262 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016903877258301 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026702880859375 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.000579833984375 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016999244689941 2025-02-21 17_52_00

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00019311904907227 2025-02-21 17_52_00


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00020194053649902 2025-02-21 17_52_00

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.9870223999023E-5 2025-02-21 17_52_00

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00020098686218262 2025-02-21 17_52_00

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00069594383239746 2025-02-21 17_52_01

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00049495697021484 2025-02-21 17_52_01

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.02616810798645 2025-02-21 17_52_01

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00070285797119141 2025-02-21 17_52_01

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010359287261963 2025-02-21 17_52_01

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0028140544891357 2025-02-21 17_52_01

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.000823974609375 2025-02-21 17_52_01

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00040411949157715 2025-02-21 17_52_01

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0027189254760742 2025-02-21 17_52_02

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00071883201599121 2025-02-21 17_52_02

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00054287910461426 2025-02-21 17_52_02

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0072519779205322 2025-02-21 17_52_17

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00074410438537598 2025-02-21 17_52_17

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00070810317993164 2025-02-21 17_52_17

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00082302093505859 2025-02-21 17_52_17

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00072383880615234 2025-02-21 17_52_17

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011439323425293 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005791187286377 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061988830566406 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00021600723266602 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050997734069824 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030207633972168 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052094459533691 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028705596923828 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047087669372559 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043416023254395 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:8.2015991210938E-5 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018811225891113 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015616416931152 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:9.2983245849609E-5 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036001205444336 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003509521484375 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00033998489379883 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017905235290527 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020980834960938 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00020098686218262 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014591217041016 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020408630371094 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00046181678771973 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00018095970153809 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028300285339355 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016283988952637 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001990795135498 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.000579833984375 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016999244689941 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036811828613281 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004119873046875 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017118453979492 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042414665222168 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047683715820312 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016212463378906 2025-02-21 17_52_17

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038599967956543 2025-02-21 17_52_17


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043201446533203 2025-02-21 17_52_17

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016188621520996 2025-02-21 17_52_17

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00046205520629883 2025-02-21 17_52_17

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00085091590881348 2025-02-21 17_52_17

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00048303604125977 2025-02-21 17_52_17

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.027536869049072 2025-02-21 17_52_17

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00089788436889648 2025-02-21 17_52_18

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0012228488922119 2025-02-21 17_52_18

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0019750595092773 2025-02-21 17_52_18

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00085783004760742 2025-02-21 17_52_18

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00023198127746582 2025-02-21 17_52_18

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0029010772705078 2025-02-21 17_52_18

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00068092346191406 2025-02-21 17_52_18

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00031208992004395 2025-02-21 17_52_18

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0010490417480469 2025-02-21 17_53_47

SELECT `pp`.`codigo` as `cod_provee`, `p`.`idProducto`, `p`.`nombre`, `p`.`tipo`, `pro`.`nombre` as `provee`, `pro`.`codigo` as `codigo_pro`, CASE 
                WHEN p.incluye_iva_comp = 1 and iva_comp=16 THEN (p.costo_compra*1.16) 
                ELSE p.costo_compra
            END AS costo_compra, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio, CASE 
                WHEN p.tipo =2 
                THEN (
                    IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_lotes, CASE 
                WHEN p.tipo =1 
                THEN (
                    IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 AND pss.disponible=1 AND pss.productoid=p.id and pss.vendido=0), 0)
                ) 
                ELSE 0 
            END as stock_series, CASE 
                WHEN p.tipo = 0
                THEN (
                    IFNULL((SELECT sum(stock) FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id), 0)
                ) 
                ELSE 0 
            END as stock_ps
FROM `productos_proveedores` `pp`
JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos` `p` ON `p`.`id`=`pp`.`idproducto` and `p`.`estatus`=1
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2 and `ps`.`activo`=1
WHERE `pp`.`idproveedor` = '0'
AND `pp`.`activo` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto` ASC 
 Execution Time:0.00065207481384277 2025-02-21 17_53_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0012309551239014 2025-02-21 17_53_47

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `p`.`activo` = 1 
 Execution Time:0.00050997734069824 2025-02-21 17_53_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0012640953063965 2025-02-21 17_53_47

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `p`.`activo` = 1 
 Execution Time:0.00087904930114746 2025-02-21 17_53_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0012819766998291 2025-02-21 17_53_53

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `p`.`activo` = 1 
 Execution Time:0.00060796737670898 2025-02-21 17_53_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.003399133682251 2025-02-21 17_58_57

SELECT `p`.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras
FROM `proveedores` `p`
WHERE `p`.`activo` = 1 
 Execution Time:0.00092196464538574 2025-02-21 17_58_57

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.00094699859619141 2025-02-21 17_58_58

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00020408630371094 2025-02-21 17_58_58

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00018811225891113 2025-02-21 17_58_58

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00029802322387695 2025-02-21 17_58_58

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00026297569274902 2025-02-21 17_58_58

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00040197372436523 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00025320053100586 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00025200843811035 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00027704238891602 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045490264892578 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0002288818359375 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062108039855957 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033998489379883 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069403648376465 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073599815368652 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00024795532226562 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065493583679199 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022697448730469 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023293495178223 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005791187286377 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006401538848877 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071001052856445 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022506713867188 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042891502380371 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00058197975158691 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028586387634277 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038886070251465 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011031627655029 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00026416778564453 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036716461181641 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017809867858887 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030112266540527 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00021100044250488 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016498565673828 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00017404556274414 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015902519226074 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.5088272094727E-5 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011801719665527 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013089179992676 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.793571472168E-5 2025-02-21 17_58_58

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010180473327637 2025-02-21 17_58_58


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012087821960449 2025-02-21 17_58_58

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.5074691772461E-5 2025-02-21 17_58_58

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00016999244689941 2025-02-21 17_58_58

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00075793266296387 2025-02-21 17_58_59

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00057482719421387 2025-02-21 17_58_59

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.035083055496216 2025-02-21 17_58_59

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00069999694824219 2025-02-21 17_58_59

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.015967130661011 2025-02-21 17_58_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0022861957550049 2025-02-21 17_58_59

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0009608268737793 2025-02-21 17_58_59

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00029897689819336 2025-02-21 17_58_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=25 
 Execution Time:0.0043840408325195 2025-02-21 17_58_59

SELECT `p`.`id`, `p`.`codigo`, `p`.`nombre`, `p`.`tel1`, `p`.`contacto`, `p`.`estatus`, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras, (select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0051078796386719 2025-02-21 17_58_59

SELECT COUNT(1) as total
FROM `proveedores` `p`
WHERE `p`.`estatus` = '1'
AND `p`.`activo` = 1 
 Execution Time:0.00068306922912598 2025-02-21 17_58_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0011241436004639 2025-02-21 17_59_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00036716461181641 2025-02-21 17_59_03

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00030207633972168 2025-02-21 17_59_03

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00024294853210449 2025-02-21 17_59_03

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00046610832214355 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040698051452637 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00030088424682617 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0001528263092041 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024294853210449 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0001380443572998 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024509429931641 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00013303756713867 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00025820732116699 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00024890899658203 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00015401840209961 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024700164794922 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00013899803161621 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00015401840209961 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023794174194336 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00013899803161621 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015497207641602 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.9843063354492E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010490417480469 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0001220703125 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.6995620727539E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010895729064941 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013208389282227 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:5.793571472168E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010395050048828 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.1021575927734E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.1075897216797E-5 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012111663818359 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.2928924560547E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.4890594482422E-5 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011301040649414 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.3167343139648E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010108947753906 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012493133544922 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.2928924560547E-5 2025-02-21 17_59_03

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.7036361694336E-5 2025-02-21 17_59_03


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011396408081055 2025-02-21 17_59_03

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.2928924560547E-5 2025-02-21 17_59_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00013899803161621 2025-02-21 17_59_03

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00060200691223145 2025-02-21 17_59_04

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0003960132598877 2025-02-21 17_59_04

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.012036800384521 2025-02-21 17_59_04

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00079798698425293 2025-02-21 17_59_04

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00075697898864746 2025-02-21 17_59_04

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0038449764251709 2025-02-21 17_59_05

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0018761157989502 2025-02-21 17_59_05

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00068497657775879 2025-02-21 17_59_05

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0012109279632568 2025-02-21 17_59_05

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00057697296142578 2025-02-21 17_59_05

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00037121772766113 2025-02-21 17_59_05

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0010848045349121 2025-02-21 17_59_06

SELECT *
FROM `clientes`
WHERE `id` = '1' 
 Execution Time:0.00073790550231934 2025-02-21 17_59_06

SELECT *
FROM `sucursal`
WHERE `id` = '2' 
 Execution Time:0.00020599365234375 2025-02-21 17_59_06

SELECT *
FROM `cliente_fiscales`
WHERE `idcliente` = '1' 
 Execution Time:0.0001680850982666 2025-02-21 17_59_06

SELECT *
FROM `estado` 
 Execution Time:0.00015616416931152 2025-02-21 17_59_06

SELECT *
FROM `f_regimenfiscal`
WHERE `activo` = 1 
 Execution Time:0.00015497207641602 2025-02-21 17_59_06

SELECT *
FROM `f_uso_cfdi`
WHERE `activo` = 1 
 Execution Time:0.00015997886657715 2025-02-21 17_59_06

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00023198127746582 2025-02-21 17_59_06

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00016999244689941 2025-02-21 17_59_06

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00029993057250977 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002589225769043 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00028395652770996 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00021982192993164 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048494338989258 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00016999244689941 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054717063903809 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029397010803223 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044012069702148 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00053596496582031 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00020503997802734 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004889965057373 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045180320739746 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00040602684020996 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069308280944824 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006871223449707 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00079894065856934 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050187110900879 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058484077453613 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00070691108703613 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026798248291016 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00097107887268066 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00098896026611328 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00044798851013184 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050902366638184 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021195411682129 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020909309387207 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00030183792114258 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00013995170593262 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020909309387207 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00033211708068848 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001521110534668 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018191337585449 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00026392936706543 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027084350585938 2025-02-21 17_59_06

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003659725189209 2025-02-21 17_59_06


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005650520324707 2025-02-21 17_59_06

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00048995018005371 2025-02-21 17_59_06

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00076889991760254 2025-02-21 17_59_06

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00037097930908203 2025-02-21 17_59_07

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00035619735717773 2025-02-21 17_59_07

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.015628814697266 2025-02-21 17_59_07

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0007779598236084 2025-02-21 17_59_07

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00078701972961426 2025-02-21 17_59_07

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0014238357543945 2025-02-21 18_00_10

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, concat(rf.clave,' ',rf.descripcion) as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0010559558868408 2025-02-21 18_00_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0033431053161621 2025-02-21 18_01_09

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00080585479736328 2025-02-21 18_01_09

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00080013275146484 2025-02-21 18_01_09

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00058889389038086 2025-02-21 18_01_09

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00091910362243652 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089597702026367 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010921955108643 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00027298927307129 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00046682357788086 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00026917457580566 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047588348388672 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018596649169922 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028204917907715 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029516220092773 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00015711784362793 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026512145996094 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001378059387207 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00016188621520996 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002598762512207 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024104118347168 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00025701522827148 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014090538024902 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022602081298828 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022196769714355 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015020370483398 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00013899803161621 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015401840209961 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:6.1988830566406E-5 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011205673217773 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.3167343139648E-5 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.3936920166016E-5 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011777877807617 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.4121017456055E-5 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00015997886657715 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00018095970153809 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.4849853515625E-5 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020194053649902 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034999847412109 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.7009201049805E-5 2025-02-21 18_01_09

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001981258392334 2025-02-21 18_01_09


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00030612945556641 2025-02-21 18_01_09

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020098686218262 2025-02-21 18_01_09

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00033998489379883 2025-02-21 18_01_09

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00034213066101074 2025-02-21 18_01_09

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00053977966308594 2025-02-21 18_01_09

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029098987579346 2025-02-21 18_01_10

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0014979839324951 2025-02-21 18_01_10

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0011818408966064 2025-02-21 18_01_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0024309158325195 2025-02-21 18_01_10

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00042390823364258 2025-02-21 18_01_10

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00030684471130371 2025-02-21 18_01_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0015311241149902 2025-02-21 18_01_10

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0011589527130127 2025-02-21 18_01_10

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00098896026611328 2025-02-21 18_01_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0034670829772949 2025-02-21 18_02_51

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00078797340393066 2025-02-21 18_02_51

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00076794624328613 2025-02-21 18_02_51

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0003960132598877 2025-02-21 18_02_51

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011088848114014 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007939338684082 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00083398818969727 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00040698051452637 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072908401489258 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030803680419922 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070977210998535 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029993057250977 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067400932312012 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072693824768066 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00029516220092773 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00081586837768555 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00046515464782715 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00028896331787109 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003821849822998 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060200691223145 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075006484985352 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024199485778809 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054287910461426 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00050592422485352 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026082992553711 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060582160949707 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076389312744141 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00032591819763184 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062799453735352 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003199577331543 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059914588928223 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032496452331543 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060391426086426 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00079107284545898 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00049090385437012 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00081300735473633 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00094819068908691 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050806999206543 2025-02-21 18_02_51

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075221061706543 2025-02-21 18_02_51


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076198577880859 2025-02-21 18_02_51

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044417381286621 2025-02-21 18_02_51

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00053095817565918 2025-02-21 18_02_51

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0005190372467041 2025-02-21 18_02_52

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0011861324310303 2025-02-21 18_02_52

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.030189990997314 2025-02-21 18_02_53

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00042486190795898 2025-02-21 18_02_53

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0011360645294189 2025-02-21 18_02_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0034539699554443 2025-02-21 18_02_53

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00068807601928711 2025-02-21 18_02_53

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00053310394287109 2025-02-21 18_02_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0023360252380371 2025-02-21 18_02_53

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0012350082397461 2025-02-21 18_02_53

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00059008598327637 2025-02-21 18_02_53

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021159648895264 2025-02-21 18_02_54

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, concat(rf.clave,' ',rf.descripcion) as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0008540153503418 2025-02-21 18_02_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0011451244354248 2025-02-21 18_02_56

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00040912628173828 2025-02-21 18_02_56

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00034093856811523 2025-02-21 18_02_56

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0002748966217041 2025-02-21 18_02_56

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00053286552429199 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048494338989258 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059199333190918 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00028491020202637 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031590461730957 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00020790100097656 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038385391235352 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020313262939453 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067996978759766 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054001808166504 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00026392936706543 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049591064453125 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024104118347168 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00026392936706543 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042295455932617 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053286552429199 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055813789367676 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024294853210449 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036811828613281 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044703483581543 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021600723266602 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029611587524414 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003669261932373 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00036096572875977 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003809928894043 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018000602722168 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027799606323242 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00043296813964844 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00011587142944336 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020909309387207 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015497207641602 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.5817108154297E-5 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027108192443848 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0002129077911377 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.1048736572266E-5 2025-02-21 18_02_56

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011897087097168 2025-02-21 18_02_56


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013589859008789 2025-02-21 18_02_56

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.793571472168E-5 2025-02-21 18_02_56

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00017809867858887 2025-02-21 18_02_56

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00041389465332031 2025-02-21 18_02_57

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.0076291561126709 2025-02-21 18_02_57

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00039887428283691 2025-02-21 18_02_57

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00056099891662598 2025-02-21 18_02_57

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0012879371643066 2025-02-21 18_02_58

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0027329921722412 2025-02-21 18_02_58

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00050711631774902 2025-02-21 18_02_58

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00026488304138184 2025-02-21 18_02_58

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.00288987159729 2025-02-21 18_02_58

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00062203407287598 2025-02-21 18_02_58

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00038290023803711 2025-02-21 18_02_58

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0014820098876953 2025-02-21 18_03_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00033712387084961 2025-02-21 18_03_02

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00026798248291016 2025-02-21 18_03_02

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00029301643371582 2025-02-21 18_03_02

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00037193298339844 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004270076751709 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038409233093262 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00020003318786621 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029206275939941 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00019311904907227 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028705596923828 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018596649169922 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028586387634277 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031614303588867 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00019717216491699 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027799606323242 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018906593322754 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0001981258392334 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022697448730469 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020813941955566 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00017404556274414 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.6995620727539E-5 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011110305786133 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00019598007202148 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00010085105895996 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034904479980469 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044894218444824 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00056290626525879 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007929801940918 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020790100097656 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030612945556641 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00038909912109375 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017189979553223 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006859302520752 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065779685974121 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025081634521484 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00016403198242188 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015783309936523 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.1988830566406E-5 2025-02-21 18_03_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051999092102051 2025-02-21 18_03_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00070405006408691 2025-02-21 18_03_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024104118347168 2025-02-21 18_03_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0006101131439209 2025-02-21 18_03_02

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00071597099304199 2025-02-21 18_03_03

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00066399574279785 2025-02-21 18_03_03

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.027436017990112 2025-02-21 18_03_03

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00090503692626953 2025-02-21 18_03_03

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00074601173400879 2025-02-21 18_03_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0019569396972656 2025-02-21 18_03_03

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00058889389038086 2025-02-21 18_03_03

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00095701217651367 2025-02-21 18_03_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0024528503417969 2025-02-21 18_03_03

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00071501731872559 2025-02-21 18_03_03

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00044894218444824 2025-02-21 18_03_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0012128353118896 2025-02-21 18_03_08

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00040698051452637 2025-02-21 18_03_08

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00027203559875488 2025-02-21 18_03_08

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00027894973754883 2025-02-21 18_03_08

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00053000450134277 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034785270690918 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00040197372436523 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00021791458129883 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0008089542388916 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00047016143798828 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077319145202637 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040602684020996 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076007843017578 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082492828369141 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00043702125549316 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069189071655273 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00059103965759277 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00054717063903809 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011258125305176 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012760162353516 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010690689086914 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024509429931641 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031900405883789 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034284591674805 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022315979003906 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077486038208008 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00091314315795898 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00041794776916504 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065994262695312 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029301643371582 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082206726074219 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044584274291992 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067996978759766 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081181526184082 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044512748718262 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067400932312012 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011088848114014 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0012350082397461 2025-02-21 18_03_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011861324310303 2025-02-21 18_03_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001107931137085 2025-02-21 18_03_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00056695938110352 2025-02-21 18_03_08

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00060296058654785 2025-02-21 18_03_08

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00041890144348145 2025-02-21 18_03_09

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0006401538848877 2025-02-21 18_03_09

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.02866792678833 2025-02-21 18_03_09

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00055599212646484 2025-02-21 18_03_10

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00075387954711914 2025-02-21 18_03_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0020577907562256 2025-02-21 18_03_10

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00081396102905273 2025-02-21 18_03_10

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00060391426086426 2025-02-21 18_03_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0023441314697266 2025-02-21 18_03_10

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0010020732879639 2025-02-21 18_03_10

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00067496299743652 2025-02-21 18_03_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0035960674285889 2025-02-21 18_03_18

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.0007469654083252 2025-02-21 18_03_18

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00069713592529297 2025-02-21 18_03_18

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00068187713623047 2025-02-21 18_03_18

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.001147985458374 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011329650878906 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0023810863494873 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00067996978759766 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012719631195068 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00053501129150391 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00094389915466309 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00056290626525879 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00086498260498047 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00095415115356445 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00043797492980957 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007939338684082 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043416023254395 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00043106079101562 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059890747070312 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076889991760254 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00091910362243652 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00042200088500977 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072097778320312 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087499618530273 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00052094459533691 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014901161193848 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0006721019744873 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011341571807861 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00056791305541992 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075292587280273 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086498260498047 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00051498413085938 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077700614929199 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010890960693359 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0004270076751709 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080704689025879 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00092983245849609 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050783157348633 2025-02-21 18_03_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00078201293945312 2025-02-21 18_03_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0009009838104248 2025-02-21 18_03_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037693977355957 2025-02-21 18_03_18

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00051403045654297 2025-02-21 18_03_18

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00085020065307617 2025-02-21 18_03_18

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00049400329589844 2025-02-21 18_03_18

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.015148878097534 2025-02-21 18_03_19

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00046586990356445 2025-02-21 18_03_19

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00095510482788086 2025-02-21 18_03_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0021049976348877 2025-02-21 18_03_19

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00091791152954102 2025-02-21 18_03_19

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.0011911392211914 2025-02-21 18_03_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0013489723205566 2025-02-21 18_03_19

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00055599212646484 2025-02-21 18_03_19

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00078511238098145 2025-02-21 18_03_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0014200210571289 2025-02-21 18_03_37

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00032210350036621 2025-02-21 18_03_37

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00030207633972168 2025-02-21 18_03_37

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00024318695068359 2025-02-21 18_03_37

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00043702125549316 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032806396484375 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00039196014404297 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00017094612121582 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029516220092773 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0001680850982666 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033903121948242 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016498565673828 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062012672424316 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007169246673584 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00025200843811035 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052690505981445 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023412704467773 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00019407272338867 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058889389038086 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00081491470336914 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00096893310546875 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00047206878662109 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069785118103027 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066208839416504 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018906593322754 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038409233093262 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003049373626709 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00014185905456543 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063109397888184 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027608871459961 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050711631774902 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063180923461914 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028204917907715 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048303604125977 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062990188598633 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027298927307129 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050210952758789 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065088272094727 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026202201843262 2025-02-21 18_03_37

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050902366638184 2025-02-21 18_03_37


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00064992904663086 2025-02-21 18_03_37

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027990341186523 2025-02-21 18_03_37

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00056695938110352 2025-02-21 18_03_37

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00096893310546875 2025-02-21 18_03_37

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0006721019744873 2025-02-21 18_03_37

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.023025989532471 2025-02-21 18_03_38

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00074505805969238 2025-02-21 18_03_38

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00097489356994629 2025-02-21 18_03_38

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0016870498657227 2025-02-21 18_03_38

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00054001808166504 2025-02-21 18_03_38

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00035285949707031 2025-02-21 18_03_38

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0025908946990967 2025-02-21 18_03_38

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00061321258544922 2025-02-21 18_03_38

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00040984153747559 2025-02-21 18_03_38

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0010669231414795 2025-02-21 18_03_47

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00039005279541016 2025-02-21 18_03_47

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00037479400634766 2025-02-21 18_03_47

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0002288818359375 2025-02-21 18_03_47

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00038981437683105 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031018257141113 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034904479980469 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00019216537475586 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023388862609863 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00020194053649902 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003349781036377 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023508071899414 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066018104553223 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00090599060058594 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00037908554077148 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070405006408691 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029993057250977 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00041103363037109 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066518783569336 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071287155151367 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00080680847167969 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0012071132659912 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088000297546387 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00099706649780273 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00064992904663086 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00084495544433594 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051999092102051 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00023698806762695 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031089782714844 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021600723266602 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029301643371582 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00032687187194824 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021791458129883 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029993057250977 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031685829162598 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021696090698242 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002899169921875 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031399726867676 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020694732666016 2025-02-21 18_03_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023007392883301 2025-02-21 18_03_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00024104118347168 2025-02-21 18_03_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015521049499512 2025-02-21 18_03_47

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00028109550476074 2025-02-21 18_03_47

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00051307678222656 2025-02-21 18_03_48

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00074887275695801 2025-02-21 18_03_48

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.038018941879272 2025-02-21 18_03_48

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00043296813964844 2025-02-21 18_03_48

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00080609321594238 2025-02-21 18_03_48

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0033061504364014 2025-02-21 18_03_48

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.0010440349578857 2025-02-21 18_03_48

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00036215782165527 2025-02-21 18_03_48

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0026791095733643 2025-02-21 18_03_48

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00061392784118652 2025-02-21 18_03_48

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00045394897460938 2025-02-21 18_03_48

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0041859149932861 2025-02-21 18_07_54

SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, concat(rf.clave,' ',rf.descripcion) as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1 
 Execution Time:0.0021190643310547 2025-02-21 18_07_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0024878978729248 2025-02-21 18_13_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1 
 Execution Time:0.00037693977355957 2025-02-21 18_13_13

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00031518936157227 2025-02-21 18_13_13

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00029897689819336 2025-02-21 18_13_13

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00062203407287598 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010809898376465 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00080108642578125 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00016283988952637 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023698806762695 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00011897087097168 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027799606323242 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00011301040649414 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032305717468262 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00035190582275391 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:9.5844268798828E-5 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032496452331543 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016498565673828 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00011920928955078 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024795532226562 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00019502639770508 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00016021728515625 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.6028366088867E-5 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040388107299805 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061202049255371 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002281665802002 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005030632019043 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029397010803223 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00024080276489258 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022196769714355 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00010800361633301 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020003318786621 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022792816162109 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001060962677002 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00019192695617676 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022697448730469 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.608268737793E-5 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001990795135498 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023388862609863 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:9.608268737793E-5 2025-02-21 18_13_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018501281738281 2025-02-21 18_13_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037097930908203 2025-02-21 18_13_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012612342834473 2025-02-21 18_13_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00030684471130371 2025-02-21 18_13_13

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00065803527832031 2025-02-21 18_13_14

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00037693977355957 2025-02-21 18_13_14

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.025019884109497 2025-02-21 18_13_14

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00077700614929199 2025-02-21 18_13_14

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00093388557434082 2025-02-21 18_13_14

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.0026199817657471 2025-02-21 18_13_14

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00080084800720215 2025-02-21 18_13_14

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00059294700622559 2025-02-21 18_13_14

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=4 
 Execution Time:0.002025842666626 2025-02-21 18_13_14

SELECT `p`.`id`, `p`.`nombre`, `p`.`usuario`, `p`.`motivo`, `p`.`desactivar`, `p`.`saldo`, `s`.`clave`
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255
ORDER BY `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.00075602531433105 2025-02-21 18_13_14

SELECT COUNT(*) as total
FROM `clientes` `p`
LEFT JOIN `sucursal` `s` ON `s`.`id`=`p`.`sucursal`
WHERE `p`.`activo` = 1
AND `p`.`id` != 8255 
 Execution Time:0.00040602684020996 2025-02-21 18_13_14

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0032789707183838 2025-02-21 18_16_15

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00079107284545898 2025-02-21 18_16_15

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00058889389038086 2025-02-21 18_16_15

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00049996376037598 2025-02-21 18_16_15

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00088095664978027 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079107284545898 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00091910362243652 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00041294097900391 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00037097930908203 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006868839263916 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036501884460449 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072002410888672 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061607360839844 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00045490264892578 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045895576477051 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023198127746582 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00022578239440918 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00025606155395508 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024008750915527 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00027203559875488 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015401840209961 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00024104118347168 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001530647277832 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032210350036621 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0002138614654541 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:7.8201293945312E-5 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055503845214844 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023412704467773 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042104721069336 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005948543548584 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023818016052246 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.000518798828125 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059795379638672 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024318695068359 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00046110153198242 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061511993408203 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002439022064209 2025-02-21 18_16_15

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045990943908691 2025-02-21 18_16_15


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062918663024902 2025-02-21 18_16_15

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024199485778809 2025-02-21 18_16_15

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048208236694336 2025-02-21 18_16_15

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00044798851013184 2025-02-21 18_16_15

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00088596343994141 2025-02-21 18_16_15

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.02477502822876 2025-02-21 18_16_15

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00063586235046387 2025-02-21 18_16_15

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.001079797744751 2025-02-21 18_16_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.001288890838623 2025-02-21 18_16_21

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00041294097900391 2025-02-21 18_16_21

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00033807754516602 2025-02-21 18_16_21

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00066208839416504 2025-02-21 18_16_21

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011570453643799 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011470317840576 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012948513031006 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0006098747253418 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00096583366394043 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00060200691223145 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00085020065307617 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00046992301940918 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011398792266846 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011739730834961 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00076699256896973 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011270046234131 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025200843811035 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00058913230895996 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089097023010254 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00085210800170898 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00092482566833496 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033402442932129 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052309036254883 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042200088500977 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026488304138184 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042891502380371 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005040168762207 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0002751350402832 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041699409484863 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025415420532227 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034999847412109 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047421455383301 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026082992553711 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070309638977051 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072789192199707 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045394897460938 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062108039855957 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011849403381348 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00058698654174805 2025-02-21 18_16_21

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011589527130127 2025-02-21 18_16_21


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084304809570312 2025-02-21 18_16_21

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002589225769043 2025-02-21 18_16_21

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00043106079101562 2025-02-21 18_16_21

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00047898292541504 2025-02-21 18_16_21

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00030612945556641 2025-02-21 18_16_21

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.026888847351074 2025-02-21 18_16_21

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00056290626525879 2025-02-21 18_16_21

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00091004371643066 2025-02-21 18_16_21

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0032241344451904 2025-02-21 18_16_26

SELECT p.*
            from productos p 
            where (nombre like '%agua %' or idProducto like '%agua %') and (p.activo=1 or p.activo='Y')
            group by p.id 
            order by nombre asc 
 Execution Time:0.020520925521851 2025-02-21 18_16_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0012290477752686 2025-02-21 18_17_10

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.0004889965057373 2025-02-21 18_17_10

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00041294097900391 2025-02-21 18_17_10

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00026202201843262 2025-02-21 18_17_10

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0008690357208252 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049209594726562 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054502487182617 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00024700164794922 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041103363037109 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00025296211242676 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040316581726074 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025296211242676 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038695335388184 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047993659973145 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00025200843811035 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037097930908203 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024104118347168 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023913383483887 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032496452331543 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034403800964355 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055503845214844 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020813941955566 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00020813941955566 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029993057250977 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027298927307129 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041317939758301 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037908554077148 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00059890747070312 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00094699859619141 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028085708618164 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041699409484863 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041413307189941 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018596649169922 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00025510787963867 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00026512145996094 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014495849609375 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061893463134766 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086283683776855 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00049614906311035 2025-02-21 18_17_10

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064396858215332 2025-02-21 18_17_10


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00077199935913086 2025-02-21 18_17_10

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034999847412109 2025-02-21 18_17_10

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00081181526184082 2025-02-21 18_17_10

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00045514106750488 2025-02-21 18_17_10

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00066900253295898 2025-02-21 18_17_10

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.027406930923462 2025-02-21 18_17_10

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00031709671020508 2025-02-21 18_17_10

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00063490867614746 2025-02-21 18_17_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0032949447631836 2025-02-21 18_17_24

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00081086158752441 2025-02-21 18_17_24

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00077009201049805 2025-02-21 18_17_24

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00074100494384766 2025-02-21 18_17_24

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011870861053467 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010879039764404 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001230001449585 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00057291984558105 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00096583366394043 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00058507919311523 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010449886322021 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032997131347656 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00087213516235352 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051999092102051 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00026512145996094 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032997131347656 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031495094299316 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00022697448730469 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029993057250977 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035500526428223 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00032806396484375 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002140998840332 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074410438537598 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007469654083252 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00042104721069336 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066804885864258 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076413154602051 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00037598609924316 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063490867614746 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030899047851562 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058102607727051 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066900253295898 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032401084899902 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063300132751465 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081706047058105 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00042915344238281 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056195259094238 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076413154602051 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033688545227051 2025-02-21 18_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060486793518066 2025-02-21 18_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014679431915283 2025-02-21 18_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00042319297790527 2025-02-21 18_17_24

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050806999206543 2025-02-21 18_17_24

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00019407272338867 2025-02-21 18_17_24

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00051689147949219 2025-02-21 18_17_24

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.01882791519165 2025-02-21 18_17_24

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00090694427490234 2025-02-21 18_17_25

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0012731552124023 2025-02-21 18_17_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0034079551696777 2025-02-21 18_17_45

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00084495544433594 2025-02-21 18_17_45

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00067687034606934 2025-02-21 18_17_45

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00052905082702637 2025-02-21 18_17_45

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011098384857178 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00091910362243652 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012609958648682 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00056695938110352 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00078701972961426 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0005340576171875 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080084800720215 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036215782165527 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065088272094727 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082516670227051 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00039100646972656 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005500316619873 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032186508178711 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00033712387084961 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056910514831543 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059103965759277 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069212913513184 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032901763916016 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058603286743164 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059986114501953 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032901763916016 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054478645324707 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065398216247559 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00034213066101074 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005030632019043 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032901763916016 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005028247833252 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00080180168151855 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043106079101562 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070095062255859 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072598457336426 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040888786315918 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048398971557617 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051593780517578 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002598762512207 2025-02-21 18_17_45

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034785270690918 2025-02-21 18_17_45


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00048708915710449 2025-02-21 18_17_45

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019216537475586 2025-02-21 18_17_45

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00028514862060547 2025-02-21 18_17_45

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00047898292541504 2025-02-21 18_17_45

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00080204010009766 2025-02-21 18_17_45

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029752016067505 2025-02-21 18_17_45

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00070500373840332 2025-02-21 18_17_46

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0017328262329102 2025-02-21 18_17_46

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0028970241546631 2025-02-21 18_18_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.0005950927734375 2025-02-21 18_18_02

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00043797492980957 2025-02-21 18_18_02

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00039410591125488 2025-02-21 18_18_02

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0007178783416748 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079512596130371 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087881088256836 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00035190582275391 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007469654083252 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030708312988281 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059199333190918 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00070500373840332 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010790824890137 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012900829315186 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00027990341186523 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074195861816406 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036406517028809 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00032591819763184 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006561279296875 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076603889465332 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00083708763122559 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00038409233093262 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005190372467041 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.000640869140625 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023889541625977 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036287307739258 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00024509429931641 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0001521110534668 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012707710266113 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023412704467773 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001518726348877 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022006034851074 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001521110534668 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00023198127746582 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023984909057617 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001521110534668 2025-02-21 18_18_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00014209747314453 2025-02-21 18_18_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0001518726348877 2025-02-21 18_18_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.413459777832E-5 2025-02-21 18_18_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00016617774963379 2025-02-21 18_18_02

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00052595138549805 2025-02-21 18_18_02

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00092101097106934 2025-02-21 18_18_03

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.024026870727539 2025-02-21 18_18_03

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00050497055053711 2025-02-21 18_18_03

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0015408992767334 2025-02-21 18_18_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0030529499053955 2025-02-21 18_18_19

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00074911117553711 2025-02-21 18_18_19

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00070405006408691 2025-02-21 18_18_19

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00082302093505859 2025-02-21 18_18_19

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.001039981842041 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011749267578125 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084400177001953 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00030684471130371 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075411796569824 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030612945556641 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068187713623047 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028800964355469 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065016746520996 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00085687637329102 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00030517578125 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065994262695312 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029397010803223 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023508071899414 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062298774719238 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010018348693848 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0009009838104248 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018978118896484 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028800964355469 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071096420288086 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022792816162109 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054693222045898 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071811676025391 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00027203559875488 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038385391235352 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00087285041809082 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033211708068848 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00045204162597656 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00056314468383789 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057888031005859 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011358261108398 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003669261932373 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061702728271484 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0009160041809082 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043487548828125 2025-02-21 18_18_19

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061392784118652 2025-02-21 18_18_19


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076413154602051 2025-02-21 18_18_19

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002741813659668 2025-02-21 18_18_19

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00049996376037598 2025-02-21 18_18_19

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00066494941711426 2025-02-21 18_18_19

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00043106079101562 2025-02-21 18_18_19

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.031475782394409 2025-02-21 18_18_20

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00067615509033203 2025-02-21 18_18_20

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00093722343444824 2025-02-21 18_18_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0012640953063965 2025-02-21 18_18_25

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00040388107299805 2025-02-21 18_18_25

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00036811828613281 2025-02-21 18_18_25

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00030684471130371 2025-02-21 18_18_25

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00051403045654297 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069808959960938 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052595138549805 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0003199577331543 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038599967956543 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00026488304138184 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037693977355957 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018501281738281 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030612945556641 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034093856811523 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00017213821411133 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028800964355469 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015687942504883 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00018811225891113 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00027203559875488 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00018215179443359 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0001981258392334 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.3909759521484E-5 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00012898445129395 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00014901161193848 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.7949295043945E-5 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00013113021850586 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00015902519226074 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:6.6041946411133E-5 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00012111663818359 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.793571472168E-5 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028514862060547 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047683715820312 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027990341186523 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00035405158996582 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00079202651977539 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00058603286743164 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010111331939697 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00023102760314941 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.1048736572266E-5 2025-02-21 18_18_25

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001218318939209 2025-02-21 18_18_25


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0001370906829834 2025-02-21 18_18_25

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.0796737670898E-5 2025-02-21 18_18_25

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00016498565673828 2025-02-21 18_18_25

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00068402290344238 2025-02-21 18_18_25

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00034093856811523 2025-02-21 18_18_25

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.025228977203369 2025-02-21 18_18_25

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00047802925109863 2025-02-21 18_18_25

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00064492225646973 2025-02-21 18_18_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0029699802398682 2025-02-21 18_18_35

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.0006098747253418 2025-02-21 18_18_35

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00062680244445801 2025-02-21 18_18_35

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00076007843017578 2025-02-21 18_18_35

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0021920204162598 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052380561828613 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00044894218444824 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00020909309387207 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030803680419922 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00020503997802734 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077700614929199 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00055599212646484 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011050701141357 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00091886520385742 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00042200088500977 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070500373840332 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003819465637207 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00035691261291504 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075411796569824 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075602531433105 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00085306167602539 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039196014404297 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066709518432617 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006411075592041 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00038909912109375 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059103965759277 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00056314468383789 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00024890899658203 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033712387084961 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024604797363281 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003049373626709 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00040984153747559 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039505958557129 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010361671447754 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004880428314209 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030016899108887 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026512145996094 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00021791458129883 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012707710266113 2025-02-21 18_18_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063800811767578 2025-02-21 18_18_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00051593780517578 2025-02-21 18_18_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023198127746582 2025-02-21 18_18_35

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00066709518432617 2025-02-21 18_18_35

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00084781646728516 2025-02-21 18_18_35

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00045990943908691 2025-02-21 18_18_35

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.012349128723145 2025-02-21 18_18_35

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00030303001403809 2025-02-21 18_18_35

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00041294097900391 2025-02-21 18_18_35

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0010709762573242 2025-02-21 18_18_38

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.0005028247833252 2025-02-21 18_18_38

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00054097175598145 2025-02-21 18_18_38

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00041103363037109 2025-02-21 18_18_38

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00046896934509277 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032305717468262 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0003511905670166 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0001990795135498 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028491020202637 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00019502639770508 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029206275939941 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019693374633789 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029087066650391 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031685829162598 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00020909309387207 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030088424682617 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020408630371094 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00021195411682129 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028896331787109 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00015401840209961 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00021791458129883 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.319450378418E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010585784912109 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013518333435059 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.8174133300781E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001070499420166 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00014114379882812 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:6.103515625E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010800361633301 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.103515625E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00016117095947266 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00030708312988281 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.2956085205078E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011992454528809 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013208389282227 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.7220458984375E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011610984802246 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012397766113281 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.4121017456055E-5 2025-02-21 18_18_38

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.7036361694336E-5 2025-02-21 18_18_38


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011491775512695 2025-02-21 18_18_38

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.3882598876953E-5 2025-02-21 18_18_38

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00024199485778809 2025-02-21 18_18_38

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00050497055053711 2025-02-21 18_18_39

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00072693824768066 2025-02-21 18_18_39

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.013242959976196 2025-02-21 18_18_39

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00077390670776367 2025-02-21 18_18_39

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010159015655518 2025-02-21 18_18_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0032069683074951 2025-02-21 18_18_47

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00067400932312012 2025-02-21 18_18_47

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00072789192199707 2025-02-21 18_18_47

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00072908401489258 2025-02-21 18_18_47

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0011951923370361 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010170936584473 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013008117675781 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00059413909912109 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010199546813965 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00066089630126953 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010139942169189 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00048208236694336 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056195259094238 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00053906440734863 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0002899169921875 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052404403686523 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031399726867676 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00030708312988281 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004580020904541 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053906440734863 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066208839416504 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029397010803223 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043702125549316 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052213668823242 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032210350036621 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051498413085938 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052094459533691 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00040507316589355 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050616264343262 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031399726867676 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049591064453125 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00057101249694824 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039887428283691 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005638599395752 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004580020904541 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026178359985352 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034499168395996 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00041007995605469 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025606155395508 2025-02-21 18_18_47

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00034403800964355 2025-02-21 18_18_47


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054597854614258 2025-02-21 18_18_47

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024509429931641 2025-02-21 18_18_47

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00040912628173828 2025-02-21 18_18_47

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0006098747253418 2025-02-21 18_18_47

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00048589706420898 2025-02-21 18_18_47

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.0101158618927 2025-02-21 18_18_47

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00063300132751465 2025-02-21 18_18_47

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.004176139831543 2025-02-21 18_18_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0037121772766113 2025-02-21 18_18_53

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00068807601928711 2025-02-21 18_18_53

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0005028247833252 2025-02-21 18_18_53

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00050616264343262 2025-02-21 18_18_53

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00091409683227539 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00083518028259277 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011239051818848 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00036501884460449 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011270046234131 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00086498260498047 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010080337524414 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00060892105102539 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013000965118408 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013859272003174 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00050115585327148 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010459423065186 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00065994262695312 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00065207481384277 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010571479797363 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011360645294189 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012438297271729 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00055313110351562 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072002410888672 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0025489330291748 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00075197219848633 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0011849403381348 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001121997833252 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00042319297790527 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010209083557129 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00067400932312012 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010368824005127 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011072158813477 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0006711483001709 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00099396705627441 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014758110046387 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00061583518981934 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088787078857422 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0009150505065918 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00049304962158203 2025-02-21 18_18_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00082993507385254 2025-02-21 18_18_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010120868682861 2025-02-21 18_18_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050520896911621 2025-02-21 18_18_53

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00082206726074219 2025-02-21 18_18_53

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00070309638977051 2025-02-21 18_18_53

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00063610076904297 2025-02-21 18_18_54

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011466979980469 2025-02-21 18_18_54

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00063800811767578 2025-02-21 18_18_54

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010800361633301 2025-02-21 18_18_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0032310485839844 2025-02-21 18_18_59

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00083804130554199 2025-02-21 18_18_59

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00077915191650391 2025-02-21 18_18_59

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00076889991760254 2025-02-21 18_18_59

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0013401508331299 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012409687042236 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0015509128570557 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00047016143798828 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089383125305176 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00046801567077637 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00088596343994141 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033092498779297 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070905685424805 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00099587440490723 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00042414665222168 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074887275695801 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00042605400085449 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00042605400085449 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075411796569824 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00078606605529785 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00085186958312988 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037121772766113 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064992904663086 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082111358642578 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044393539428711 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080204010009766 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012030601501465 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.000823974609375 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00096511840820312 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00075507164001465 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089693069458008 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001032829284668 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00052285194396973 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072908401489258 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00093293190002441 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00051712989807129 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00085115432739258 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00095701217651367 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045919418334961 2025-02-21 18_18_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080299377441406 2025-02-21 18_18_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00089502334594727 2025-02-21 18_18_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00049996376037598 2025-02-21 18_18_59

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0007319450378418 2025-02-21 18_18_59

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00047779083251953 2025-02-21 18_18_59

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00034117698669434 2025-02-21 18_18_59

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.026616096496582 2025-02-21 18_18_59

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0003969669342041 2025-02-21 18_18_59

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010740756988525 2025-02-21 18_18_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0014700889587402 2025-02-21 18_19_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00052499771118164 2025-02-21 18_19_02

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00049686431884766 2025-02-21 18_19_02

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0003211498260498 2025-02-21 18_19_02

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00054502487182617 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049686431884766 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006248950958252 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00026798248291016 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077486038208008 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00040793418884277 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079512596130371 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037384033203125 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071883201599121 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00090312957763672 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00042295455932617 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067496299743652 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00037908554077148 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00040197372436523 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075387954711914 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00083303451538086 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00096702575683594 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025391578674316 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047898292541504 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081896781921387 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021815299987793 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066590309143066 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00083613395690918 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00032401084899902 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052905082702637 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034499168395996 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050520896911621 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075984001159668 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026416778564453 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047397613525391 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008080005645752 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026988983154297 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004889965057373 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00083398818969727 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026893615722656 2025-02-21 18_19_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053691864013672 2025-02-21 18_19_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011239051818848 2025-02-21 18_19_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00054192543029785 2025-02-21 18_19_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00076699256896973 2025-02-21 18_19_02

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00065112113952637 2025-02-21 18_19_02

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00076007843017578 2025-02-21 18_19_02

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029834985733032 2025-02-21 18_19_02

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00093388557434082 2025-02-21 18_19_03

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010659694671631 2025-02-21 18_19_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0018410682678223 2025-02-21 18_19_07

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00060415267944336 2025-02-21 18_19_07

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00052499771118164 2025-02-21 18_19_07

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00037312507629395 2025-02-21 18_19_07

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00065708160400391 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059986114501953 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073909759521484 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00030708312988281 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00053787231445312 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005497932434082 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040912628173828 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079107284545898 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0014312267303467 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00067305564880371 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00093507766723633 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026106834411621 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00043416023254395 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072693824768066 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074100494384766 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086379051208496 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003669261932373 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065207481384277 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075101852416992 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033998489379883 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048589706420898 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065898895263672 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00025606155395508 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048184394836426 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026392936706543 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043392181396484 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00058698654174805 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025606155395508 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045895576477051 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059103965759277 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025200843811035 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004730224609375 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065708160400391 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003821849822998 2025-02-21 18_19_07

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058317184448242 2025-02-21 18_19_07


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-21 18_19_07

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028300285339355 2025-02-21 18_19_07

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00061798095703125 2025-02-21 18_19_07

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00045585632324219 2025-02-21 18_19_07

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00043201446533203 2025-02-21 18_19_07

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.022816896438599 2025-02-21 18_19_07

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00079607963562012 2025-02-21 18_19_07

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0006260871887207 2025-02-21 18_19_07

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0013878345489502 2025-02-21 18_19_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00050115585327148 2025-02-21 18_19_13

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0004880428314209 2025-02-21 18_19_13

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00032997131347656 2025-02-21 18_19_13

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0005500316619873 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048494338989258 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060391426086426 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00026607513427734 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049400329589844 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00025200843811035 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043892860412598 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027298927307129 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061917304992676 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082087516784668 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00038003921508789 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067615509033203 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045514106750488 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00035810470581055 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056695938110352 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005488395690918 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00078296661376953 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002601146697998 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058102607727051 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00089097023010254 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045514106750488 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077414512634277 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075721740722656 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00029587745666504 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053811073303223 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028800964355469 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047898292541504 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00064682960510254 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025296211242676 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00046300888061523 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071811676025391 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031304359436035 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048303604125977 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007328987121582 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026416778564453 2025-02-21 18_19_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043106079101562 2025-02-21 18_19_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060105323791504 2025-02-21 18_19_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027704238891602 2025-02-21 18_19_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00092697143554688 2025-02-21 18_19_13

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00080585479736328 2025-02-21 18_19_13

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00033879280090332 2025-02-21 18_19_13

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.028496980667114 2025-02-21 18_19_13

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00062894821166992 2025-02-21 18_19_13

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010561943054199 2025-02-21 18_19_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0018610954284668 2025-02-21 18_19_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00061392784118652 2025-02-21 18_19_23

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00050997734069824 2025-02-21 18_19_23

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0004270076751709 2025-02-21 18_19_23

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00070691108703613 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061798095703125 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00070905685424805 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0003211498260498 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056910514831543 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00033402442932129 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057005882263184 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034093856811523 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055694580078125 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011940002441406 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00070500373840332 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00084805488586426 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00060510635375977 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.0005040168762207 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048589706420898 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050091743469238 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059604644775391 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027203559875488 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041890144348145 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047183036804199 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026893615722656 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042200088500977 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047898292541504 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00025391578674316 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040578842163086 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024104118347168 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032997131347656 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00040292739868164 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028610229492188 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003359317779541 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004270076751709 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022411346435547 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032901763916016 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00039291381835938 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022697448730469 2025-02-21 18_19_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036001205444336 2025-02-21 18_19_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00032901763916016 2025-02-21 18_19_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00011396408081055 2025-02-21 18_19_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00028300285339355 2025-02-21 18_19_23

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00066781044006348 2025-02-21 18_19_23

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.0003809928894043 2025-02-21 18_19_23

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.023925065994263 2025-02-21 18_19_23

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00061583518981934 2025-02-21 18_19_24

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00082802772521973 2025-02-21 18_19_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0019831657409668 2025-02-21 18_19_26

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00062394142150879 2025-02-21 18_19_26

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00051689147949219 2025-02-21 18_19_26

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00040388107299805 2025-02-21 18_19_26

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00068902969360352 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056314468383789 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006868839263916 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00030303001403809 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048708915710449 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.0004878044128418 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00092291831970215 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039815902709961 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010149478912354 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00048613548278809 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00091004371643066 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045514106750488 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00047802925109863 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089502334594727 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0008690357208252 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010669231414795 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00047922134399414 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065302848815918 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008549690246582 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036978721618652 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061702728271484 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065398216247559 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00028085708618164 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005338191986084 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025391578674316 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004889965057373 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00057697296142578 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0007328987121582 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065898895263672 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069713592529297 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0004279613494873 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049209594726562 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047612190246582 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021696090698242 2025-02-21 18_19_26

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003809928894043 2025-02-21 18_19_26


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00047993659973145 2025-02-21 18_19_26

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034403800964355 2025-02-21 18_19_26

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00032281875610352 2025-02-21 18_19_26

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00058794021606445 2025-02-21 18_19_26

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00052309036254883 2025-02-21 18_19_26

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011417150497437 2025-02-21 18_19_26

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00049304962158203 2025-02-21 18_19_26

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00099492073059082 2025-02-21 18_19_27

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0014500617980957 2025-02-21 18_19_35

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00049400329589844 2025-02-21 18_19_35

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0004270076751709 2025-02-21 18_19_35

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.0003049373626709 2025-02-21 18_19_35

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0005180835723877 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065708160400391 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063896179199219 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00024294853210449 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004279613494873 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00024700164794922 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041913986206055 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076103210449219 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008549690246582 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00034379959106445 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006859302520752 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00032782554626465 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00032782554626465 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070691108703613 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065088272094727 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0025069713592529 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045108795166016 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077509880065918 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086593627929688 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034689903259277 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00066614151000977 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00078797340393066 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00031900405883789 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063300132751465 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030708312988281 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061702728271484 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069403648376465 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002899169921875 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00062894821166992 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00068306922912598 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003509521484375 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005490779876709 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006868839263916 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031089782714844 2025-02-21 18_19_35

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00064396858215332 2025-02-21 18_19_35


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00070095062255859 2025-02-21 18_19_35

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029993057250977 2025-02-21 18_19_35

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00034999847412109 2025-02-21 18_19_35

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0008089542388916 2025-02-21 18_19_35

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00049996376037598 2025-02-21 18_19_35

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.028928995132446 2025-02-21 18_19_36

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00069284439086914 2025-02-21 18_19_36

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00055789947509766 2025-02-21 18_19_36

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0022940635681152 2025-02-21 18_19_53

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00053501129150391 2025-02-21 18_19_53

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0004420280456543 2025-02-21 18_19_53

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00035285949707031 2025-02-21 18_19_53

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00057601928710938 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054621696472168 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00064706802368164 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00033402442932129 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048112869262695 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00028610229492188 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0004119873046875 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026607513427734 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047397613525391 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00048708915710449 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00031590461730957 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00040888786315918 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030803680419922 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00036001205444336 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00092697143554688 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072622299194336 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012359619140625 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00048208236694336 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068306922912598 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00033307075500488 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021004676818848 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00087785720825195 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087499618530273 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00034022331237793 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061297416687012 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00035905838012695 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050520896911621 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072193145751953 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00039100646972656 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060200691223145 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00068116188049316 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027203559875488 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061607360839844 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069379806518555 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026106834411621 2025-02-21 18_19_53

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048995018005371 2025-02-21 18_19_53


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062990188598633 2025-02-21 18_19_53

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026178359985352 2025-02-21 18_19_53

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00098299980163574 2025-02-21 18_19_53

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00051498413085938 2025-02-21 18_19_54

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00077486038208008 2025-02-21 18_19_54

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.020155906677246 2025-02-21 18_19_54

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0006401538848877 2025-02-21 18_19_54

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00064802169799805 2025-02-21 18_19_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0029470920562744 2025-02-21 18_20_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00062990188598633 2025-02-21 18_20_02

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00052976608276367 2025-02-21 18_20_02

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00047683715820312 2025-02-21 18_20_02

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00077390670776367 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070095062255859 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0008552074432373 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00047183036804199 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079011917114258 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00034284591674805 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065112113952637 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031113624572754 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065994262695312 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00077295303344727 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0011370182037354 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0008540153503418 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050497055053711 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023317337036133 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032901763916016 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00032496452331543 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004119873046875 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002129077911377 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029397010803223 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031280517578125 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021004676818848 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029087066650391 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00034904479980469 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0001530647277832 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00014901161193848 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.1988830566406E-5 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043416023254395 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063514709472656 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024294853210449 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00016093254089355 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00014781951904297 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.1988830566406E-5 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052094459533691 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063180923461914 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-21 18_20_02

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00012087821960449 2025-02-21 18_20_02


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013589859008789 2025-02-21 18_20_02

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.103515625E-5 2025-02-21 18_20_02

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048494338989258 2025-02-21 18_20_02

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00038790702819824 2025-02-21 18_20_02

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00061798095703125 2025-02-21 18_20_03

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.025471210479736 2025-02-21 18_20_03

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00058794021606445 2025-02-21 18_20_03

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00060296058654785 2025-02-21 18_20_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0011467933654785 2025-02-21 18_20_14

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00039005279541016 2025-02-21 18_20_14

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0003211498260498 2025-02-21 18_20_14

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00062298774719238 2025-02-21 18_20_14

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0010979175567627 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010418891906738 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011370182037354 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00045394897460938 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007481575012207 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030303001403809 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061988830566406 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029301643371582 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00048184394836426 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00079488754272461 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00027298927307129 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080680847167969 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025510787963867 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00037622451782227 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00095295906066895 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00090718269348145 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00095796585083008 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024914741516113 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003209114074707 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037908554077148 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00041985511779785 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037097930908203 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00035405158996582 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.0002591609954834 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030088424682617 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00019097328186035 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026297569274902 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029802322387695 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016903877258301 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0002899169921875 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00019407272338867 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.9870223999023E-5 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070905685424805 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006258487701416 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026392936706543 2025-02-21 18_20_14

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053000450134277 2025-02-21 18_20_14


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071001052856445 2025-02-21 18_20_14

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026798248291016 2025-02-21 18_20_14

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00041818618774414 2025-02-21 18_20_14

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00046110153198242 2025-02-21 18_20_14

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00055217742919922 2025-02-21 18_20_14

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.033666133880615 2025-02-21 18_20_14

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00052499771118164 2025-02-21 18_20_14

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0010800361633301 2025-02-21 18_20_14

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.003216028213501 2025-02-21 18_29_30

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.0007169246673584 2025-02-21 18_29_30

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00075101852416992 2025-02-21 18_29_30

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00072002410888672 2025-02-21 18_29_30

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00095701217651367 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010690689086914 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063300132751465 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00046300888061523 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00083684921264648 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00025796890258789 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044798851013184 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021696090698242 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043988227844238 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00089001655578613 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00036287307739258 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068902969360352 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030398368835449 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00039315223693848 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.000701904296875 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072002410888672 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022602081298828 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.0095062255859E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00012111663818359 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00013303756713867 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:6.6995620727539E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00017690658569336 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00019693374633789 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:7.0095062255859E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011801719665527 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.6028366088867E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0001070499420166 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012087821960449 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.5074691772461E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:9.7990036010742E-5 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011706352233887 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.4836273193359E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00011014938354492 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00012779235839844 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.5074691772461E-5 2025-02-21 18_29_30

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00010013580322266 2025-02-21 18_29_30


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00011587142944336 2025-02-21 18_29_30

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:5.2928924560547E-5 2025-02-21 18_29_30

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00015687942504883 2025-02-21 18_29_30

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00062012672424316 2025-02-21 18_29_30

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00043606758117676 2025-02-21 18_29_30

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.022582054138184 2025-02-21 18_29_30

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00055480003356934 2025-02-21 18_29_31

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0011458396911621 2025-02-21 18_29_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0036277770996094 2025-02-21 18_30_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00066518783569336 2025-02-21 18_30_22

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00063920021057129 2025-02-21 18_30_22

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00050687789916992 2025-02-21 18_30_22

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0009150505065918 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036907196044922 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063514709472656 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00022101402282715 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031614303588867 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00022697448730469 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054287910461426 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012516975402832 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00080108642578125 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010538101196289 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00039196014404297 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074291229248047 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00036096572875977 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00032687187194824 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068402290344238 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073814392089844 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086379051208496 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00038981437683105 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069212913513184 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007929801940918 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00041604042053223 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007178783416748 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087690353393555 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00046706199645996 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071096420288086 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033211708068848 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058293342590332 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00071883201599121 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022006034851074 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003049373626709 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00037097930908203 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027680397033691 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050711631774902 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060701370239258 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034189224243164 2025-02-21 18_30_22

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029802322387695 2025-02-21 18_30_22


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00022196769714355 2025-02-21 18_30_22

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00015401840209961 2025-02-21 18_30_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00021815299987793 2025-02-21 18_30_22

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00033783912658691 2025-02-21 18_30_22

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00025391578674316 2025-02-21 18_30_22

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.023430109024048 2025-02-21 18_30_22

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.0005190372467041 2025-02-21 18_30_23

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00045394897460938 2025-02-21 18_30_23

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0035979747772217 2025-02-21 18_31_08

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00075602531433105 2025-02-21 18_31_08

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00078678131103516 2025-02-21 18_31_08

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00072002410888672 2025-02-21 18_31_08

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0014588832855225 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00094223022460938 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012931823730469 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.0006859302520752 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00094103813171387 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00059103965759277 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073385238647461 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045180320739746 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067520141601562 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00068283081054688 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00037503242492676 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0006248950958252 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030708312988281 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00031900405883789 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051307678222656 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052809715270996 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006098747253418 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027990341186523 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007479190826416 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00074100494384766 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00035786628723145 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0022330284118652 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084900856018066 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00044822692871094 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075197219848633 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044989585876465 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070881843566895 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00091886520385742 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044608116149902 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073790550231934 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0023360252380371 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00050616264343262 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053501129150391 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061988830566406 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00038695335388184 2025-02-21 18_31_08

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076198577880859 2025-02-21 18_31_08


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00088906288146973 2025-02-21 18_31_08

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00045394897460938 2025-02-21 18_31_08

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0011630058288574 2025-02-21 18_31_08

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00026702880859375 2025-02-21 18_31_08

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00056314468383789 2025-02-21 18_31_08

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029192924499512 2025-02-21 18_31_08

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00067496299743652 2025-02-21 18_31_09

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.001068115234375 2025-02-21 18_31_09

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.0027270317077637 2025-02-21 18_31_15

select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, '' as serie, '' as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                1 as incluye_iva_comp, 0 as iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=1 and cd.activo=1
            JOIN recargas as r on r.id=cd.idproducto
            where c.activo=1 and cd.idproducto=12 and c.precompra=0
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
            group by c.id  union 
        /*SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, ht.cantidad, 'Dispersión' as tipo_mov, c.factura, '' as serie, '' as lote, concat('Entrada a ',s.name_suc) as destino,
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.idsucursal_entra=2
        JOIN compra_erp as c on c.id=ht.id_compra and c.distribusion=1 and ht.id_compra>0
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        where t.activo=1 and ht.idproducto=12
        AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        union */

        SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, vd.cantidad, 'Venta' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp, 0 as iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
        FROM venta_erp v
        JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=1
        JOIN recargas as r on r.id=vd.idproducto 
        JOIN sucursal as s on s.id=v.sucursal
        where v.activo=1 and vd.tipo=1
        and v.sucursal=2
        AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59' and v.tipo_venta=0 

        union
        SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.preciov as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=12 and t.status=1 and ht.status=1 
        AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        group by ht.id

        union
        SELECT t.id, ht.fechaingreso, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado aceptado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.preciov as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=12 and t.status=2 and ht.status=2
        AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        group by ht.id

        -- devolucion de oxigeno, acá me quedo para integrar al kardex el reporte de devoluciones
        union
        SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            1 as incluye_iva_comp, 0 as iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=1 and vd.activo=1
            JOIN recargas as r on r.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            where vd.tipo=1
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'

        ) as datos group by id ORDER by reg desc 
 Execution Time:0.019181966781616 2025-02-21 18_31_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.002187967300415 2025-02-21 18_32_34

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `id_alias` ASC 
 Execution Time:0.00067687034606934 2025-02-21 18_32_34

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0006568431854248 2025-02-21 18_32_34

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00045013427734375 2025-02-21 18_32_34

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0012190341949463 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00076699256896973 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00089812278747559 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00047993659973145 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00077509880065918 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00039100646972656 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071001052856445 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034904479980469 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070786476135254 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00086092948913574 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00035214424133301 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059008598327637 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002601146697998 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00020909309387207 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050902366638184 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057005882263184 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072789192199707 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025486946105957 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044894218444824 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063896179199219 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025701522827148 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055098533630371 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00068092346191406 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00027799606323242 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055193901062012 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027680397033691 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049805641174316 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012681484222412 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00043296813964844 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00092816352844238 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00074195861816406 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00026488304138184 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054287910461426 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00064706802368164 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028896331787109 2025-02-21 18_32_34

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005340576171875 2025-02-21 18_32_34


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00067019462585449 2025-02-21 18_32_34

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028395652770996 2025-02-21 18_32_34

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050020217895508 2025-02-21 18_32_34

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00044798851013184 2025-02-21 18_32_34

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.001039981842041 2025-02-21 18_32_34

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.028234004974365 2025-02-21 18_32_35

SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='2025-02-21 00:00:00'  
 Execution Time:0.00052309036254883 2025-02-21 18_32_35

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0005338191986084 2025-02-21 18_32_35

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=39 
 Execution Time:0.00093889236450195 2025-02-21 18_32_42

select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, '' as serie, '' as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                1 as incluye_iva_comp, 0 as iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=1 and cd.activo=1
            JOIN recargas as r on r.id=cd.idproducto
            where c.activo=1 and cd.idproducto=12 and c.precompra=0
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
            group by c.id  union 
        /*SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, ht.cantidad, 'Dispersión' as tipo_mov, c.factura, '' as serie, '' as lote, concat('Entrada a ',s.name_suc) as destino,
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.idsucursal_entra=2
        JOIN compra_erp as c on c.id=ht.id_compra and c.distribusion=1 and ht.id_compra>0
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        where t.activo=1 and ht.idproducto=12
        AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        union */

        SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, vd.cantidad, 'Venta' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp, 0 as iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
        FROM venta_erp v
        JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=1
        JOIN recargas as r on r.id=vd.idproducto 
        JOIN sucursal as s on s.id=v.sucursal
        where v.activo=1 and vd.tipo=1
        and v.sucursal=2
        AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59' and v.tipo_venta=0 

        union
        SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.preciov as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=12 and t.status=1 and ht.status=1 
        AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        group by ht.id

        union
        SELECT t.id, ht.fechaingreso, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado aceptado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.preciov as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=12 and t.status=2 and ht.status=2
        AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'
        group by ht.id

        -- devolucion de oxigeno, acá me quedo para integrar al kardex el reporte de devoluciones
        union
        SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            1 as incluye_iva_comp, 0 as iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=1 and vd.activo=1
            JOIN recargas as r on r.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            where vd.tipo=1
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-21 23:59:59'

        ) as datos group by id ORDER by reg desc 
 Execution Time:0.0016481876373291 2025-02-21 18_32_42

