ERROR - 2025-02-24 18:41:11 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
       ' at line 13 - Invalid query: select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            where c.activo=1 and cd.idproducto=39 and c.precompra=0
            JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by c.id  union SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=39
            and v.sucursal=2
            AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=39 and ht.status=1 
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=39
            and g.id_origen=2
            AND g.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=39
            and gd.id_origen=2
            AND bg.fecha BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=39
            and ba.id_sucursal=2
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='2' and pro.id='39' AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='2' and pro.id='39' AND v.delete_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
                union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=39
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND bi.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND m.fecha_serv BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
        ) as datos ORDER by reg asc
ERROR - 2025-02-24 19:06:47 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
       ' at line 13 - Invalid query: select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            where c.activo=1 and cd.idproducto=39 and c.precompra=0
            JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by c.id  union SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=39
            and v.sucursal=2
            AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=39 and ht.status=1 
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=39
            and g.id_origen=2
            AND g.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=39
            and gd.id_origen=2
            AND bg.fecha BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=39
            and ba.id_sucursal=2
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='2' and pro.id='39' AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='2' and pro.id='39' AND v.delete_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
                union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=39
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND bi.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND m.fecha_serv BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
        ) as datos ORDER by reg asc
ERROR - 2025-02-24 19:06:48 --> Query error: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
       ' at line 13 - Invalid query: select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            where c.activo=1 and cd.idproducto=1088 and c.precompra=0
            JOIN sucursal as s on s.id=c.sucursal
            /*and c.sucursal=2*/
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by c.id  union SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=1088
            and v.sucursal=2
            AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=1088 and ht.status=1 
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=1088 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=1088 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=1088
            and g.id_origen=2
            AND g.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=1088
            and gd.id_origen=2
            AND bg.fecha BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=1088
            and ba.id_sucursal=2
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='2' and pro.id='1088' AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='2' and pro.id='1088' AND v.delete_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='2' and pro.id='1088' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='2' and pro.id='1088' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
                union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=1088
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=1088
            and m.id_sucursal=2
            AND bi.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=1088
            and m.id_sucursal=2
            AND m.fecha_serv BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
        ) as datos ORDER by reg asc
ERROR - 2025-02-24 19:08:14 --> Query error: The used SELECT statements have a different number of columns - Invalid query: select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            JOIN sucursal as s on s.id=c.sucursal
            where c.activo=1 and cd.idproducto=39 and c.precompra=0
            /*and c.sucursal=2*/
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by c.id  union SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=39
            and v.sucursal=2
            AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=39 and ht.status=1 
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=39
            and g.id_origen=2
            AND g.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=39
            and gd.id_origen=2
            AND bg.fecha BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=39
            and ba.id_sucursal=2
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='2' and pro.id='39' AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='2' and pro.id='39' AND v.delete_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
                union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=39
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND bi.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND m.fecha_serv BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
        ) as datos ORDER by reg asc
ERROR - 2025-02-24 19:11:05 --> Query error: Unknown column 's.name_suc' in 'field list' - Invalid query: select * from (SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            JOIN sucursal as s on s.id=c.sucursal
            where c.activo=1 and cd.idproducto=39 and c.precompra=0
            /*and c.sucursal=2*/
            AND c.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by c.id  union SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=39
            and v.sucursal=2
            AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=39 and ht.status=1 
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=2 or ht.idsucursal_entra=2)
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=39 and ht.status=2
            AND ht.fechaingreso BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            AND t.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=39
            and g.id_origen=2
            AND g.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=39
            and gd.id_origen=2
            AND bg.fecha BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=39
            and ba.id_sucursal=2
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='2' and pro.id='39' AND v.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='2' and pro.id='39' AND v.delete_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='2' and pro.id='39' AND r.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
                union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=39
            and v.sucursal=2
            AND b.reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND bi.fecha_reg BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=39
            and m.id_sucursal=2
            AND m.fecha_serv BETWEEN '2024-01-01 00:00:00' AND '2025-02-24 23:59:59'
        ) as datos ORDER by reg asc
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
ERROR - 2025-02-24 19:11:29 --> Severity: Notice --> Undefined property: stdClass::$sucursal C:\wamp64\www\semit_erp\application\views\kardex\excelKardex.php 128
