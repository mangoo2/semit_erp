SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0044798851013184 2025-02-20 11_42_55

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00079822540283203 2025-02-20 11_42_55

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        COALESCE(
        GROUP_CONCAT(
            DISTINCT CONCAT(
                '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                        WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT CONCAT('<td>Serie: ', `pss`.`serie`, '</td>') SEPARATOR '') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                        WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT CONCAT('<td>Lote: ', `psl`.`lote`, ' / ', `psl`.`caducidad`, '</td>') SEPARATOR '') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                        ELSE ''
                    END, '</td>'
            ) 
            ORDER BY s.orden ASC 
            SEPARATOR ''
        ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
    ) AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:5.2441329956055 2025-02-20 11_42_55

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0041007995605469 2025-02-20 11_43_24

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.012579917907715 2025-02-20 11_43_24

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        COALESCE(
        GROUP_CONCAT(
            DISTINCT CONCAT(
                '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                        WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT CONCAT('<td>Serie: ', `pss`.`serie`, '</td>') SEPARATOR '') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                        WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT CONCAT('<td>Lote: ', `psl`.`lote`, ' / ', `psl`.`caducidad`, '</td>') SEPARATOR '') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                        ELSE ''
                    END, '</td>'
            ) 
            ORDER BY s.orden ASC 
            SEPARATOR ''
        ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
    ) AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:5.5620889663696 2025-02-20 11_43_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0027651786804199 2025-02-20 11_44_10

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0007319450378418 2025-02-20 11_44_10

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:4.6338140964508 2025-02-20 11_44_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.003248929977417 2025-02-20 11_45_33

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00072813034057617 2025-02-20 11_45_33

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:4.6220490932465 2025-02-20 11_45_33

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0039260387420654 2025-02-20 11_46_11

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0010190010070801 2025-02-20 11_46_11

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:5.3014550209045 2025-02-20 11_46_11

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0032579898834229 2025-02-20 11_58_26

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00064897537231445 2025-02-20 11_58_26

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td> </td>', '<td>0</td>', '<td>0</td>', '<td>', GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:4.0878219604492 2025-02-20 11_58_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0039310455322266 2025-02-20 12_09_57

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00079011917114258 2025-02-20 12_09_57

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
    GROUP_CONCAT(DISTINCT 
        CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
             FROM productos_sucursales_serie pss
             WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
             AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
        ) 
        ORDER BY s.orden ASC SEPARATOR ''
    ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
)
            /*WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:49.017236948013 2025-02-20 12_09_57

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.001072883605957 2025-02-20 12_39_46

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00051617622375488 2025-02-20 12_39_46

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:35.928021192551 2025-02-20 12_39_46

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0041310787200928 2025-02-20 13_09_49

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00069403648376465 2025-02-20 13_09_49

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
    GROUP_CONCAT(DISTINCT 
        CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(pss.serie, '') -- Usar IFNULL para evitar valores nulos, '</td>'
        ) 
        ORDER BY s.orden ASC SEPARATOR ''
    ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
)

            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:3.2817378044128 2025-02-20 13_09_49

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0029211044311523 2025-02-20 13_10_56

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00090384483337402 2025-02-20 13_10_56

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(pss.serie, ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:3.324156999588 2025-02-20 13_10_56

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0011739730834961 2025-02-20 13_13_10

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00046801567077637 2025-02-20 13_13_10

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: ', pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:3.1503190994263 2025-02-20 13_13_10

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0037689208984375 2025-02-20 13_20_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00077104568481445 2025-02-20 13_20_03

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.7618629932404 2025-02-20 13_20_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0010130405426025 2025-02-20 13_21_52

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00036191940307617 2025-02-20 13_21_52

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.520515203476 2025-02-20 13_21_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0018289089202881 2025-02-20 13_22_42

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050091743469238 2025-02-20 13_22_42

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', pss.serie
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6678230762482 2025-02-20 13_22_42

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0032460689544678 2025-02-20 13_23_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048708915710449 2025-02-20 13_23_03

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', 'Serie: ', pss.serie
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.7379760742188 2025-02-20 13_23_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0023231506347656 2025-02-20 13_23_49

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00040793418884277 2025-02-20 13_23_49

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', pss.serie
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br></td>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:3.2983701229095 2025-02-20 13_23_49

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0018541812896729 2025-02-20 13_24_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00074601173400879 2025-02-20 13_24_22

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '', 'Serie: ', pss.serie
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6689240932465 2025-02-20 13_24_22

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0016980171203613 2025-02-20 13_24_33

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0005040168762207 2025-02-20 13_24_33

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.639436006546 2025-02-20 13_24_33

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0010721683502197 2025-02-20 13_25_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0002291202545166 2025-02-20 13_25_03

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6473801136017 2025-02-20 13_25_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0011231899261475 2025-02-20 13_31_16

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00035715103149414 2025-02-20 13_31_16

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        IF(@prev_product_id != p.id, CONCAT('<td>Serie: ', `pss`.`serie`, '</td>'), CONCAT('<br>', pss.serie)
                        )
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            ) 
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.711795091629 2025-02-20 13_31_16

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0036931037902832 2025-02-20 13_35_12

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00063896179199219 2025-02-20 13_35_12

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        IF(@prev_product_id != p.id, CONCAT('<td>Serie: ', `pss`.`serie`, '</td>'), CONCAT('<br>', pss.serie)
                        )
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            ) 
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.7077980041504 2025-02-20 13_35_12

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0015130043029785 2025-02-20 13_41_08

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00054407119750977 2025-02-20 13_41_08

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6752901077271 2025-02-20 13_41_08

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0029351711273193 2025-02-20 13_42_19

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00065994262695312 2025-02-20 13_42_19

SET @prev_id = NULL; 
 Execution Time:0.00026488304138184 2025-02-20 13_42_19

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.5855929851532 2025-02-20 13_42_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.00098204612731934 2025-02-20 13_43_09

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00031399726867676 2025-02-20 13_43_09

SET @prev_id = NULL; 
 Execution Time:0.00010108947753906 2025-02-20 13_43_09

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` AS `cod_prov`, `pro`.`nombre` AS `proveedor`, CASE 
        WHEN p.tipo != 1 AND p.tipo != 2 THEN COALESCE(
            GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        )
        WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abre <td> cuando cambia el producto
                    ELSE CONCAT('<br>', 'Serie: ', pss.serie)  -- Agrega <br> si el producto sigue siendo el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
        WHEN p.tipo = 2 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>') 
                    FROM productos_sucursales_lote psl 
                    WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                    AND psl.activo = 1), '</td>'
                ) ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
        ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
    END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) AS iva, COALESCE(ps.incluye_iva, 0) AS incluye_iva, CASE
        WHEN p.tipo = 0 THEN 'Stock'
        WHEN p.tipo = 1 THEN 'Serie'
        WHEN p.tipo = 2 THEN 'Lote y caducidad'
        WHEN p.tipo = 3 THEN 'Insumo'
        WHEN p.tipo = 4 THEN 'Refacciones'
        ELSE ''
    END AS tipo_prod, CASE 
        WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
        WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
        WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
        WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
        ELSE 0
    END AS costo_compra_iva, CASE
        WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
        WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
        WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
        WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
        ELSE 0
    END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId` = `p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto` = `p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id` = `pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo` = 1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid` = `p`.`id` AND `ps`.`activo` = 1 AND `s`.`activo` = 1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid` = `ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido` = 0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid` = `ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.7376029491425 2025-02-20 13_43_09

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.00092387199401855 2025-02-20 13_44_11

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00029993057250977 2025-02-20 13_44_11

SET @prev_id = NULL; 
 Execution Time:0.0001060962677002 2025-02-20 13_44_11

SET @first_entry = 1; 
 Execution Time:0.00012397766113281 2025-02-20 13_44_11

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6575651168823 2025-02-20 13_44_11

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0016660690307617 2025-02-20 13_46_26

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0003659725189209 2025-02-20 13_46_26

SET @prev_id = NULL; 
 Execution Time:0.00011205673217773 2025-02-20 13_46_26

SET @first_entry = 1; 
 Execution Time:0.00018000602722168 2025-02-20 13_46_26

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.663302898407 2025-02-20 13_46_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0028040409088135 2025-02-20 13_46_52

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00065016746520996 2025-02-20 13_46_52

SET @prev_id = NULL; 
 Execution Time:0.0002601146697998 2025-02-20 13_46_52

SET @first_entry = 1; 
 Execution Time:0.00022602081298828 2025-02-20 13_46_52

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<td><br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.7402629852295 2025-02-20 13_46_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0012578964233398 2025-02-20 13_47_51

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00038409233093262 2025-02-20 13_47_51

SET @prev_id = NULL; 
 Execution Time:0.00016498565673828 2025-02-20 13_47_51

SET @first_entry = 1; 
 Execution Time:0.00024604797363281 2025-02-20 13_47_51

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR '<br>'
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.5963099002838 2025-02-20 13_47_51

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0030441284179688 2025-02-20 13_48_00

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00066995620727539 2025-02-20 13_48_00

SET @prev_id = NULL; 
 Execution Time:0.00040984153747559 2025-02-20 13_48_00

SET @first_entry = 1; 
 Execution Time:0.00036215782165527 2025-02-20 13_48_00

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6202938556671 2025-02-20 13_48_00

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0040569305419922 2025-02-20 13_49_06

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00096392631530762 2025-02-20 13_49_06

SET @prev_id = NULL; 
 Execution Time:0.00024986267089844 2025-02-20 13_49_06

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
            GROUP_CONCAT(
                DISTINCT 
                CASE 
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> cuando el producto es el mismo
                END 
                ORDER BY s.orden ASC SEPARATOR ''
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.562735080719 2025-02-20 13_49_06

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0010881423950195 2025-02-20 14_00_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00029301643371582 2025-02-20 14_00_23

SET @prev_id = NULL; 
 Execution Time:0.0001530647277832 2025-02-20 14_00_23

SET @first_entry = 1; 
 Execution Time:0.00013399124145508 2025-02-20 14_00_23

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
                GROUP_CONCAT(
                DISTINCT
                CASE
                    -- Subconsulta que abre el <td> solo cuando cambia el producto
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> si el producto es el mismo
                END
                ORDER BY s.orden ASC SEPARATOR ''
            
            )
            
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6720128059387 2025-02-20 14_00_23

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0030431747436523 2025-02-20 14_01_26

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00041508674621582 2025-02-20 14_01_26

SET @prev_id = NULL; 
 Execution Time:0.0001070499420166 2025-02-20 14_01_26

SET @first_entry = 1; 
 Execution Time:0.00017786026000977 2025-02-20 14_01_26

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
                GROUP_CONCAT(
                DISTINCT
                CASE
                    -- Subconsulta que abre el <td> solo cuando cambia el producto
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> si el producto es el mismo
                END
                ORDER BY s.orden ASC SEPARATOR ''
            )
            
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6402668952942 2025-02-20 14_01_26

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.00092005729675293 2025-02-20 14_01_50

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004429817199707 2025-02-20 14_01_50

SET @prev_id = NULL; 
 Execution Time:0.00019693374633789 2025-02-20 14_01_50

SET @first_entry = 1; 
 Execution Time:0.00018620491027832 2025-02-20 14_01_50

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN 
                GROUP_CONCAT(
                DISTINCT
                CASE
                    -- Subconsulta que abre el <td> solo cuando cambia el producto
                    WHEN p.id != @prev_id THEN 
                        CONCAT('<td>', 'Serie: ', `pss`.`serie`, '</td>')  -- Abrir <td> cuando el ID del producto cambia
                    ELSE 
                        CONCAT('<br>', 'Serie: ', pss.serie)  -- Agregar <br> si el producto es el mismo
                END
                ORDER BY s.orden ASC SEPARATOR ''
            )
            
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.6687970161438 2025-02-20 14_01_50

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0019240379333496 2025-02-20 14_03_50

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00096392631530762 2025-02-20 14_03_50

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        IF(@prev_product_id != p.id, CONCAT('<td>Serie: ', `pss`.`serie`, '</td>'), CONCAT('<br>', pss.serie)
                        )
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            ) 
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.777468919754 2025-02-20 14_03_50

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0026569366455078 2025-02-20 14_04_01

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00081396102905273 2025-02-20 14_04_01

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:2.68181681633 2025-02-20 14_04_01

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0032839775085449 2025-02-20 14_04_19

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00058698654174805 2025-02-20 14_04_19

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
            GROUP_CONCAT(
                DISTINCT CONCAT(
                    '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                            WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                            WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lote psl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                            ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.82363414764404 2025-02-20 14_04_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0012679100036621 2025-02-20 14_15_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004730224609375 2025-02-20 14_15_22

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT('Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.84622502326965 2025-02-20 14_15_22

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0025219917297363 2025-02-20 14_15_43

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00058197975158691 2025-02-20 14_15_43

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT('Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.94009017944336 2025-02-20 14_15_43

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0010190010070801 2025-02-20 14_16_04

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0003809928894043 2025-02-20 14_16_04

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.88069295883179 2025-02-20 14_16_04

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0023579597473145 2025-02-20 14_16_59

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048303604125977 2025-02-20 14_16_59

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=8
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.93559598922729 2025-02-20 14_16_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0013659000396729 2025-02-20 14_17_23

SELECT *
FROM `productos`
WHERE `id` = '39' 
 Execution Time:0.00053310394287109 2025-02-20 14_17_23

SELECT *
FROM `proveedores`
WHERE `id` = '1' 
 Execution Time:0.00019598007202148 2025-02-20 14_17_23

SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=39 and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='39'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden 
 Execution Time:0.011888027191162 2025-02-20 14_17_23

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00028514862060547 2025-02-20 14_17_23

SELECT *
FROM `f_unidades`
WHERE `Clave` = 'H87' 
 Execution Time:0.00136399269104 2025-02-20 14_17_23

SELECT *
FROM `f_servicios`
WHERE `Clave` = '01010101' 
 Execution Time:0.02902889251709 2025-02-20 14_17_23

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.0011119842529297 2025-02-20 14_17_23

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00067901611328125 2025-02-20 14_17_23

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0017149448394775 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013179779052734 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012409687042236 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00039196014404297 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00073599815368652 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00034403800964355 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057101249694824 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028705596923828 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00079894065856934 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0012660026550293 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00026988983154297 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056195259094238 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00024104118347168 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059700012207031 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00097107887268066 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004270076751709 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017690658569336 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029206275939941 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060415267944336 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002288818359375 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075197219848633 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00024294853210449 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042104721069336 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022602081298828 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043010711669922 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055098533630371 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020098686218262 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038886070251465 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00046205520629883 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017619132995605 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044107437133789 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00031518936157227 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00017189979553223 2025-02-20 14_17_23

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047802925109863 2025-02-20 14_17_23


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00062108039855957 2025-02-20 14_17_23

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024986267089844 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=8 and disponible=1
            group by id 
 Execution Time:0.00080418586730957 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=8
        group by psl.id 
 Execution Time:0.00066900253295898 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=2 and disponible=1
            group by id 
 Execution Time:0.00072693824768066 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=2
        group by psl.id 
 Execution Time:0.00058603286743164 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=4 and disponible=1
            group by id 
 Execution Time:0.00083422660827637 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=4
        group by psl.id 
 Execution Time:0.00086402893066406 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=5 and disponible=1
            group by id 
 Execution Time:0.00067591667175293 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=5
        group by psl.id 
 Execution Time:0.00065183639526367 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=3 and disponible=1
            group by id 
 Execution Time:0.00078487396240234 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=3
        group by psl.id 
 Execution Time:0.0008540153503418 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=6 and disponible=1
            group by id 
 Execution Time:0.00078916549682617 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=6
        group by psl.id 
 Execution Time:0.00072789192199707 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=7 and disponible=1
            group by id 
 Execution Time:0.00077581405639648 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=7
        group by psl.id 
 Execution Time:0.0008080005645752 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=9 and disponible=1
            group by id 
 Execution Time:0.00046896934509277 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=9
        group by psl.id 
 Execution Time:0.00036907196044922 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=10 and disponible=1
            group by id 
 Execution Time:0.00039291381835938 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=10
        group by psl.id 
 Execution Time:0.00031518936157227 2025-02-20 14_17_23

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=39 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=39 and sucursalid=11 and disponible=1
            group by id 
 Execution Time:0.00044608116149902 2025-02-20 14_17_23

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=39 and psl.sucursalid=11
        group by psl.id 
 Execution Time:0.00040102005004883 2025-02-20 14_17_23

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00056004524230957 2025-02-20 14_17_23

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0014181137084961 2025-02-20 14_17_23

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00046586990356445 2025-02-20 14_17_23

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011931896209717 2025-02-20 14_17_23

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0007469654083252 2025-02-20 14_17_23

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0031981468200684 2025-02-20 14_17_24

SELECT *
FROM `productos_caracteristicas`
WHERE `activo` = 1
AND `idproductos` = '39' 
 Execution Time:0.00051093101501465 2025-02-20 14_17_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0022609233856201 2025-02-20 14_17_24

SELECT *
FROM `categoria`
WHERE `activo` = 1
ORDER BY `categoriaId` DESC 
 Execution Time:0.0011699199676514 2025-02-20 14_17_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0023930072784424 2025-02-20 14_17_24

SELECT *
FROM `productos_files`
WHERE `activo` = 1
AND `idproductos` = '39' 
 Execution Time:0.00071001052856445 2025-02-20 14_17_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0026981830596924 2025-02-20 14_17_24

SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=39 
 Execution Time:0.0004580020904541 2025-02-20 14_17_24

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0024080276489258 2025-02-20 14_17_24

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00087094306945801 2025-02-20 14_17_24

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.00047111511230469 2025-02-20 14_17_24

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.0003211498260498 2025-02-20 14_17_24

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00039815902709961 2025-02-20 14_17_24

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00041007995605469 2025-02-20 14_17_24

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00091004371643066 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00097203254699707 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00088906288146973 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00028896331787109 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051712989807129 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00024700164794922 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051116943359375 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025582313537598 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00042414665222168 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00075888633728027 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0004889965057373 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00095200538635254 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030994415283203 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00024890899658203 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00031208992004395 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056004524230957 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069618225097656 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022792816162109 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00029683113098145 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0002291202545166 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014591217041016 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022387504577637 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00029182434082031 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00014996528625488 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00022292137145996 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001370906829834 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00037217140197754 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00026798248291016 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016212463378906 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00019192695617676 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00020599365234375 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:7.6055526733398E-5 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00055408477783203 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0005640983581543 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023603439331055 2025-02-20 14_17_24

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041007995605469 2025-02-20 14_17_24


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00052499771118164 2025-02-20 14_17_24

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00020384788513184 2025-02-20 14_17_24

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0003960132598877 2025-02-20 14_17_24

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00064301490783691 2025-02-20 14_17_25

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00056290626525879 2025-02-20 14_17_25

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.029186010360718 2025-02-20 14_17_25

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00041413307189941 2025-02-20 14_17_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0013711452484131 2025-02-20 14_17_25

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.047557830810547 2025-02-20 14_17_25

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1 
 Execution Time:0.038757085800171 2025-02-20 14_17_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0027461051940918 2025-02-20 14_17_25

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.089082002639771 2025-02-20 14_17_25

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1 
 Execution Time:0.043890953063965 2025-02-20 14_17_25

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.00097990036010742 2025-02-20 14_17_32

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
AND   (
`p`.`id` LIKE '%008081%' ESCAPE '!'
OR  `p`.`referencia` LIKE '%008081%' ESCAPE '!'
OR  `c`.`categoria` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stock` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stockmin` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stockmax` LIKE '%008081%' ESCAPE '!'
OR  `p`.`precio_sin_iva` LIKE '%008081%' ESCAPE '!'
OR  `p`.`precio_con_iva` LIKE '%008081%' ESCAPE '!'
OR  `p`.`mostrar_pagina` LIKE '%008081%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%008081%' ESCAPE '!'
OR  `p`.`mas_vendidos` LIKE '%008081%' ESCAPE '!'
OR  `p`.`codigoBarras` LIKE '%008081%' ESCAPE '!'
OR  `p`.`idProducto` LIKE '%008081%' ESCAPE '!'
OR  `p`.`tipo` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`precio` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`incluye_iva` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`iva` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`precio_final` LIKE '%008081%' ESCAPE '!'
 )
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.018349885940552 2025-02-20 14_17_32

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
AND   (
`p`.`id` LIKE '%008081%' ESCAPE '!'
OR  `p`.`referencia` LIKE '%008081%' ESCAPE '!'
OR  `c`.`categoria` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stock` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stockmin` LIKE '%008081%' ESCAPE '!'
OR  `p`.`stockmax` LIKE '%008081%' ESCAPE '!'
OR  `p`.`precio_sin_iva` LIKE '%008081%' ESCAPE '!'
OR  `p`.`precio_con_iva` LIKE '%008081%' ESCAPE '!'
OR  `p`.`mostrar_pagina` LIKE '%008081%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%008081%' ESCAPE '!'
OR  `p`.`codigoBarras` LIKE '%008081%' ESCAPE '!'
OR  `p`.`idProducto` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`precio` LIKE '%008081%' ESCAPE '!'
OR  `ps`.`precio_final` LIKE '%008081%' ESCAPE '!'
 ) 
 Execution Time:0.019880056381226 2025-02-20 14_17_32

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0008549690246582 2025-02-20 14_17_33

SELECT *
FROM `productos`
WHERE `id` = '1163' 
 Execution Time:0.00032687187194824 2025-02-20 14_17_33

SELECT *
FROM `proveedores`
WHERE `id` = '0' 
 Execution Time:0.00017404556274414 2025-02-20 14_17_33

SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=1163 and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='1163'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden 
 Execution Time:0.022723913192749 2025-02-20 14_17_33

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00033187866210938 2025-02-20 14_17_33

SELECT *
FROM `f_unidades`
WHERE `Clave` = 'H87' 
 Execution Time:0.00099897384643555 2025-02-20 14_17_33

SELECT *
FROM `f_servicios`
WHERE `Clave` = '01010101' 
 Execution Time:0.030209064483643 2025-02-20 14_17_33

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00070095062255859 2025-02-20 14_17_33

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00053501129150391 2025-02-20 14_17_33

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00095009803771973 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071811676025391 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076007843017578 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00050806999206543 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069499015808105 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00033903121948242 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00052905082702637 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027799606323242 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0005488395690918 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063681602478027 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00043010711669922 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0008399486541748 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00054717063903809 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00054001808166504 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00089597702026367 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00092101097106934 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.001046895980835 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00052905082702637 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00091314315795898 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084400177001953 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00044107437133789 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065398216247559 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00095701217651367 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00040292739868164 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059795379638672 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0004270076751709 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00044083595275879 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00060200691223145 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028014183044434 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054311752319336 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00069808959960938 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030994415283203 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053095817565918 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072908401489258 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002439022064209 2025-02-20 14_17_33

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00059103965759277 2025-02-20 14_17_33


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055384635925293 2025-02-20 14_17_33

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021886825561523 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=8 and disponible=1
            group by id 
 Execution Time:0.0011560916900635 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=8
        group by psl.id 
 Execution Time:0.00061416625976562 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=2 and disponible=1
            group by id 
 Execution Time:0.00085711479187012 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=2
        group by psl.id 
 Execution Time:0.00081205368041992 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=4 and disponible=1
            group by id 
 Execution Time:0.00093913078308105 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=4
        group by psl.id 
 Execution Time:0.00092291831970215 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=5 and disponible=1
            group by id 
 Execution Time:0.00080418586730957 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=5
        group by psl.id 
 Execution Time:0.00087189674377441 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=3 and disponible=1
            group by id 
 Execution Time:0.00095200538635254 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=3
        group by psl.id 
 Execution Time:0.00050497055053711 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=6 and disponible=1
            group by id 
 Execution Time:0.00093388557434082 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=6
        group by psl.id 
 Execution Time:0.00070810317993164 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=7 and disponible=1
            group by id 
 Execution Time:0.0011529922485352 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=7
        group by psl.id 
 Execution Time:0.00060701370239258 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=9 and disponible=1
            group by id 
 Execution Time:0.00067782402038574 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=9
        group by psl.id 
 Execution Time:0.00065207481384277 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=10 and disponible=1
            group by id 
 Execution Time:0.00068092346191406 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=10
        group by psl.id 
 Execution Time:0.00062417984008789 2025-02-20 14_17_33

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=11 and disponible=1
            group by id 
 Execution Time:0.00060796737670898 2025-02-20 14_17_33

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=11
        group by psl.id 
 Execution Time:0.00053882598876953 2025-02-20 14_17_33

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00043392181396484 2025-02-20 14_17_33

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0011200904846191 2025-02-20 14_17_33

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00045394897460938 2025-02-20 14_17_34

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011008024215698 2025-02-20 14_17_34

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00060892105102539 2025-02-20 14_17_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0031061172485352 2025-02-20 14_17_34

SELECT *
FROM `productos_files`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.00045299530029297 2025-02-20 14_17_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0010859966278076 2025-02-20 14_17_34

SELECT *
FROM `productos_caracteristicas`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.0003509521484375 2025-02-20 14_17_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0027921199798584 2025-02-20 14_17_34

SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=1163 
 Execution Time:0.00069999694824219 2025-02-20 14_17_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0065178871154785 2025-02-20 14_17_34

SELECT *
FROM `categoria`
WHERE `activo` = 1
ORDER BY `categoriaId` DESC 
 Execution Time:0.0017848014831543 2025-02-20 14_17_34

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.002993106842041 2025-02-20 14_19_59

SELECT *
FROM `productos`
WHERE `id` = '1163' 
 Execution Time:0.00071907043457031 2025-02-20 14_19_59

SELECT *
FROM `proveedores`
WHERE `id` = '0' 
 Execution Time:0.00049400329589844 2025-02-20 14_19_59

SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=1163 and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='1163'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden 
 Execution Time:0.026118993759155 2025-02-20 14_19_59

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00053715705871582 2025-02-20 14_19_59

SELECT *
FROM `f_unidades`
WHERE `Clave` = 'H87' 
 Execution Time:0.0036540031433105 2025-02-20 14_19_59

SELECT *
FROM `f_servicios`
WHERE `Clave` = '01010101' 
 Execution Time:0.058282136917114 2025-02-20 14_19_59

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00054502487182617 2025-02-20 14_19_59

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00035786628723145 2025-02-20 14_19_59

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00074505805969238 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063800811767578 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0007939338684082 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00031805038452148 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058197975158691 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00030517578125 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057411193847656 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029087066650391 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00056195259094238 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006711483001709 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00033283233642578 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00058507919311523 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00018882751464844 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00012803077697754 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00036787986755371 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00033903121948242 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042104721069336 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0001671314239502 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0003960132598877 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042104721069336 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00012111663818359 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00026988983154297 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00057697296142578 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00016498565673828 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00038886070251465 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014495849609375 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00028395652770996 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00042080879211426 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00014781951904297 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00030398368835449 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0004270076751709 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00016903877258301 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041890144348145 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054717063903809 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00024294853210449 2025-02-20 14_19_59

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00039505958557129 2025-02-20 14_19_59


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00055503845214844 2025-02-20 14_19_59

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025510787963867 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=8 and disponible=1
            group by id 
 Execution Time:0.0012140274047852 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=8
        group by psl.id 
 Execution Time:0.00065398216247559 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=2 and disponible=1
            group by id 
 Execution Time:0.00085616111755371 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=2
        group by psl.id 
 Execution Time:0.00062417984008789 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=4 and disponible=1
            group by id 
 Execution Time:0.00061392784118652 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=4
        group by psl.id 
 Execution Time:0.00060701370239258 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=5 and disponible=1
            group by id 
 Execution Time:0.00058388710021973 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=5
        group by psl.id 
 Execution Time:0.00042390823364258 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=3 and disponible=1
            group by id 
 Execution Time:0.00064706802368164 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=3
        group by psl.id 
 Execution Time:0.00055098533630371 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=6 and disponible=1
            group by id 
 Execution Time:0.00056886672973633 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=6
        group by psl.id 
 Execution Time:0.00051403045654297 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=7 and disponible=1
            group by id 
 Execution Time:0.0008690357208252 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=7
        group by psl.id 
 Execution Time:0.00051403045654297 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=9 and disponible=1
            group by id 
 Execution Time:0.00057005882263184 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=9
        group by psl.id 
 Execution Time:0.00043106079101562 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=10 and disponible=1
            group by id 
 Execution Time:0.00055503845214844 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=10
        group by psl.id 
 Execution Time:0.0004889965057373 2025-02-20 14_19_59

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=11 and disponible=1
            group by id 
 Execution Time:0.00059294700622559 2025-02-20 14_19_59

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=11
        group by psl.id 
 Execution Time:0.00049400329589844 2025-02-20 14_19_59

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00029706954956055 2025-02-20 14_19_59

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00023102760314941 2025-02-20 14_19_59

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.00065898895263672 2025-02-20 14_19_59

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.011475086212158 2025-02-20 14_19_59

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00040102005004883 2025-02-20 14_19_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020351409912109 2025-02-20 14_19_59

SELECT *
FROM `categoria`
WHERE `activo` = 1
ORDER BY `categoriaId` DESC 
 Execution Time:0.00020790100097656 2025-02-20 14_19_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0021500587463379 2025-02-20 14_19_59

SELECT *
FROM `productos_files`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.00050902366638184 2025-02-20 14_19_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0011670589447021 2025-02-20 14_19_59

SELECT *
FROM `productos_caracteristicas`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.00027608871459961 2025-02-20 14_19_59

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020170211791992 2025-02-20 14_20_00

SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=1163 
 Execution Time:0.00037002563476562 2025-02-20 14_20_00

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0034010410308838 2025-02-20 14_21_00

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00051999092102051 2025-02-20 14_21_00

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.039895057678223 2025-02-20 14_21_00

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0027141571044922 2025-02-20 14_21_27

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00059294700622559 2025-02-20 14_21_27

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, 0 as `incluye_iva`, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.10506582260132 2025-02-20 14_21_27

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020451545715332 2025-02-20 14_22_00

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00075984001159668 2025-02-20 14_22_00

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=8 AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=8 AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.11762094497681 2025-02-20 14_22_00

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0019690990447998 2025-02-20 14_22_43

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00053310394287109 2025-02-20 14_22_43

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.39495706558228 2025-02-20 14_22_43

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0016438961029053 2025-02-20 14_23_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00082302093505859 2025-02-20 14_23_03

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */

        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.3128719329834 2025-02-20 14_23_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020878314971924 2025-02-20 14_24_56

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00060892105102539 2025-02-20 14_24_56

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, --GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, --GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, --GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.99503898620605 2025-02-20 14_24_56

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0027618408203125 2025-02-20 14_27_46

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00047397613525391 2025-02-20 14_27_46

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`=8
JOIN `sucursal` `s` ON `s`.`id`=`ps`.`sucursalid` and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.34572005271912 2025-02-20 14_27_46

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0030760765075684 2025-02-20 14_28_31

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00065088272094727 2025-02-20 14_28_31

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                           WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                           WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHEREpss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                           WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lotepsl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                           ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:0.93439698219299 2025-02-20 14_28_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0025601387023926 2025-02-20 14_28_54

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00063991546630859 2025-02-20 14_28_54

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                           WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                           WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHEREpss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                           WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lotepsl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                           ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:1.0388610363007 2025-02-20 14_28_54

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020380020141602 2025-02-20 14_29_27

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00049901008605957 2025-02-20 14_29_27

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', IFNULL(concat('Serie: 'pss.serie), ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', 'Serie: ', `pss`.`serie`, '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', `psl`.`lote`, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                           WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                           WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHEREpss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                           WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lotepsl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                           ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:1.7482199668884 2025-02-20 14_29_27

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0011470317840576 2025-02-20 14_31_17

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00040292739868164 2025-02-20 14_31_17

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', '<td>', CASE 
                           WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                           WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHEREpss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                           WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lotepsl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                           ELSE ''
                        END, '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:1.5458669662476 2025-02-20 14_31_17

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0021600723266602 2025-02-20 14_31_36

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00052809715270996 2025-02-20 14_31_36

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1 and `ps`.`sucursalid`=2
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:1.6102728843689 2025-02-20 14_31_36

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.00092101097106934 2025-02-20 14_31_52

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00029706954956055 2025-02-20 14_31_52

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:6.7767469882965 2025-02-20 14_31_52

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0016829967498779 2025-02-20 15_35_44

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004580020904541 2025-02-20 15_35_44

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:48.015669107437 2025-02-20 15_35_44

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0028190612792969 2025-02-20 15_39_30

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00069093704223633 2025-02-20 15_39_30

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:45.937415122986 2025-02-20 15_39_30

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0026209354400635 2025-02-20 15_44_27

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00085282325744629 2025-02-20 15_44_27

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:50.800236940384 2025-02-20 15_44_27

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0014438629150391 2025-02-20 15_48_11

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0050449371337891 2025-02-20 15_48_11

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:62.517225027084 2025-02-20 15_48_11

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020811557769775 2025-02-20 15_52_01

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050616264343262 2025-02-20 15_52_01

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, `*/ CASE -- Cuando el tipo no es 1 ni` 2, se aseguran celdas vacías si no hay registros de productos_sucursales
        WHEN p.tipo != 1 AND p.tipo != 2 THEN 
            COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        '<td>', COALESCE(ps.stock, ''), '</td>', '<td>', COALESCE(ps.stockmin, ''), '</td>', '<td>', COALESCE(ps.stockmax, ''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), '<td></td><td></td><td></td>'  -- Aseguramos celdas vacías cuando no hay datos
            )
        
        -- Cuando el tipo es 1 (serie), se asegura de que se generen celdas vacías si no existen registros de productos_sucursales_serie
        WHEN p.tipo = 1 THEN 
            COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        '<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>') 
                         FROM productos_sucursales_serie pss 
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid 
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), '<td></td><td></td><td></td>'  -- Si no hay registros, generamos celdas vacías
            )

        -- Cuando el tipo es 2 (lote y caducidad), aseguramos celdas vacías si no existen registros de productos_sucursales_lote
        WHEN p.tipo = 2 THEN 
            COALESCE(
                GROUP_CONCAT(
                    DISTINCT 
                    CONCAT(
                        '<td>', COALESCE(psl.cantidad, ''), '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), '<td></td><td></td><td></td>'  -- Si no hay registros, generamos celdas vacías
            )
        
        -- Caso por defecto cuando el tipo no es 1 ni 2
        ELSE 
            '<td></td><td></td><td></td>'  -- Siempre genera celdas vacías
    END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:47.40535902977 2025-02-20 15_52_01

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.002824068069458 2025-02-20 15_54_36

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00079798698425293 2025-02-20 15_54_36

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:44.540145158768 2025-02-20 15_54_36

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0018100738525391 2025-02-20 15_56_39

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00042295455932617 2025-02-20 15_56_39

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.066642999649 2025-02-20 15_56_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020020008087158 2025-02-20 16_01_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00057816505432129 2025-02-20 16_01_22

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:11.798199176788 2025-02-20 16_01_22

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0051968097686768 2025-02-20 16_02_28

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0010309219360352 2025-02-20 16_02_28

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '<br></td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:9.8001179695129 2025-02-20 16_02_28

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0018899440765381 2025-02-20 16_07_06

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0004727840423584 2025-02-20 16_07_06

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.481168031693 2025-02-20 16_07_06

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0055708885192871 2025-02-20 16_09_31

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00060796737670898 2025-02-20 16_09_31

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td> </td>', '<td> </td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            ELSE CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:9.8266201019287 2025-02-20 16_09_31

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0028140544891357 2025-02-20 16_12_57

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00064587593078613 2025-02-20 16_12_57

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>  S/D</td>', '<td> S/D </td>', '<td> S/D </td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
            )
            ELSE CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.408113002777 2025-02-20 16_12_57

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0022120475769043 2025-02-20 16_15_22

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00046682357788086 2025-02-20 16_15_22

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>  S/D</td>', '<td> S/D </td>', '<td> S/D </td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
            )
            ELSE CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.718145847321 2025-02-20 16_15_22

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0029289722442627 2025-02-20 16_15_47

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00061798095703125 2025-02-20 16_15_47

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>  S/D</td>', '<td> S/D </td>', '<td> S/D </td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> </td>', '<td> </td>', '<td> </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
            )
            ELSE CONCAT('<td>S/D </td>', '<td>S/D </td>', '<td>S/D </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.959890842438 2025-02-20 16_15_47

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0020360946655273 2025-02-20 16_16_32

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00048494338989258 2025-02-20 16_16_32

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
        WHEN p.tipo NOT IN (1, 2) THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 1 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td></td>', '<td></td>', '<td></td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 2 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        ELSE 
            CONCAT('<td></td>', '<td></td>', '<td></td>')
    END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.150505065918 2025-02-20 16_16_32

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0088138580322266 2025-02-20 16_19_39

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.018587827682495 2025-02-20 16_19_39

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
        WHEN p.tipo NOT IN (1, 2) THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 1 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td></td>', '<td></td>', '<td></td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 2 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        ELSE 
            CONCAT('<td></td>', '<td></td>', '<td></td>')
    END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:11.406461000443 2025-02-20 16_19_39

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.022742986679077 2025-02-20 16_19_58

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0011940002441406 2025-02-20 16_19_58

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
        WHEN p.tipo NOT IN (1, 2) THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 1 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td></td>', '<td></td>', '<td></td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 2 THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        ELSE 
            CONCAT('<td></td>', '<td></td>', '<td></td>')
    END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:10.931912899017 2025-02-20 16_19_58

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.002140998840332 2025-02-20 16_22_36

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00066590309143066 2025-02-20 16_22_36

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>S/D 1</td>', '<td>S/D 1</td>', '<td>S/D 1</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>S/D 2</td>', '<td>S/D 2</td>', '<td>S/D 2</td>')
            )
            ELSE CONCAT('<td>sd2 </td>', '<td>sd2 </td>', '<td>sd2 </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:11.441581964493 2025-02-20 16_22_36

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0065639019012451 2025-02-20 16_24_03

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.018383026123047 2025-02-20 16_24_03

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
        WHEN p.tipo NOT IN (1, 2) THEN 
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        WHEN p.tipo = 1 THEN 
            -- Para tipo 1 (Serie), devuelve celdas vacías
            CONCAT('<td></td>', '<td></td>', '<td></td>')
        WHEN p.tipo = 2 THEN 
            -- Para tipo 2 (Lote), genera celdas vacías si no hay datos
            COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>') 
                ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
        ELSE 
            CONCAT('<td></td>', '<td></td>', '<td></td>')
    END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:12.789561033249 2025-02-20 16_24_03

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0023989677429199 2025-02-20 16_29_45

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00040483474731445 2025-02-20 16_29_45

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:8.0209140777588 2025-02-20 16_29_45

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0025718212127686 2025-02-20 16_38_09

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0006098747253418 2025-02-20 16_38_09

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        COALESCE (CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END, CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')) AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:12.205429792404 2025-02-20 16_38_09

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.005734920501709 2025-02-20 16_38_50

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0008690357208252 2025-02-20 16_38_50

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        IFNULL(CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END, CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')) AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:12.752501010895 2025-02-20 16_38_50

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0034499168395996 2025-02-20 16_45_20

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0016539096832275 2025-02-20 16_45_20

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        IFNULL(CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END, CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')) AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `s`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:13.258317947388 2025-02-20 16_45_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0022780895233154 2025-02-20 16_52_12

SELECT *
FROM `productos`
WHERE `id` = '1163' 
 Execution Time:0.00086307525634766 2025-02-20 16_52_12

SELECT *
FROM `proveedores`
WHERE `id` = '0' 
 Execution Time:0.00040984153747559 2025-02-20 16_52_12

SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=1163 and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='1163'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden 
 Execution Time:0.02676796913147 2025-02-20 16_52_12

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00078892707824707 2025-02-20 16_52_12

SELECT *
FROM `f_unidades`
WHERE `Clave` = 'H87' 
 Execution Time:0.0047249794006348 2025-02-20 16_52_12

SELECT *
FROM `f_servicios`
WHERE `Clave` = '01010101' 
 Execution Time:0.26860404014587 2025-02-20 16_52_12

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.032222986221313 2025-02-20 16_52_12

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00095105171203613 2025-02-20 16_52_12

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0030419826507568 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0020139217376709 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0013101100921631 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00037312507629395 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00095415115356445 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00049614906311035 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0012810230255127 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00034213066101074 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00071406364440918 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00084590911865234 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.00031518936157227 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00063705444335938 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029182434082031 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00026392936706543 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049901008605957 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00072216987609863 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00073790550231934 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00049209594726562 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00057005882263184 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00059294700622559 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00030398368835449 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00096607208251953 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0011069774627686 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00030279159545898 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00054311752319336 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025796890258789 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00068807601928711 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063490867614746 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027608871459961 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00061678886413574 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00076103210449219 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00040698051452637 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010530948638916 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00087308883666992 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00028777122497559 2025-02-20 16_52_12

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00082802772521973 2025-02-20 16_52_12


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00081110000610352 2025-02-20 16_52_12

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029611587524414 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=8 and disponible=1
            group by id 
 Execution Time:0.0020079612731934 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=8
        group by psl.id 
 Execution Time:0.0027420520782471 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=2 and disponible=1
            group by id 
 Execution Time:0.0022637844085693 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=2
        group by psl.id 
 Execution Time:0.0011289119720459 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=4 and disponible=1
            group by id 
 Execution Time:0.0010528564453125 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=4
        group by psl.id 
 Execution Time:0.0019850730895996 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=5 and disponible=1
            group by id 
 Execution Time:0.00078606605529785 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=5
        group by psl.id 
 Execution Time:0.00070595741271973 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=3 and disponible=1
            group by id 
 Execution Time:0.0011258125305176 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=3
        group by psl.id 
 Execution Time:0.00087809562683105 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=6 and disponible=1
            group by id 
 Execution Time:0.00096392631530762 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=6
        group by psl.id 
 Execution Time:0.0025300979614258 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=7 and disponible=1
            group by id 
 Execution Time:0.0023400783538818 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=7
        group by psl.id 
 Execution Time:0.0027058124542236 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=9 and disponible=1
            group by id 
 Execution Time:0.003432035446167 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=9
        group by psl.id 
 Execution Time:0.0014779567718506 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=10 and disponible=1
            group by id 
 Execution Time:0.00078892707824707 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=10
        group by psl.id 
 Execution Time:0.00068998336791992 2025-02-20 16_52_12

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=1163 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=1163 and sucursalid=11 and disponible=1
            group by id 
 Execution Time:0.00068998336791992 2025-02-20 16_52_12

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=1163 and psl.sucursalid=11
        group by psl.id 
 Execution Time:0.0005950927734375 2025-02-20 16_52_12

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00050497055053711 2025-02-20 16_52_12

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00091791152954102 2025-02-20 16_52_13

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0013160705566406 2025-02-20 16_52_13

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.031444072723389 2025-02-20 16_52_13

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.0015749931335449 2025-02-20 16_52_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0023369789123535 2025-02-20 16_52_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00073385238647461 2025-02-20 16_52_13

SELECT *
FROM `categoria`
WHERE `activo` = 1 
 Execution Time:0.0005650520324707 2025-02-20 16_52_13

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00059914588928223 2025-02-20 16_52_13

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00056600570678711 2025-02-20 16_52_13

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.00041699409484863 2025-02-20 16_52_13

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.00082612037658691 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007779598236084 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00082206726074219 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00027084350585938 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00069212913513184 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00028204917907715 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0007469654083252 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0003960132598877 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00070786476135254 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.09766411781311 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.0003659725189209 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00065493583679199 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027108192443848 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00027918815612793 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00067400932312012 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00084209442138672 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066399574279785 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00035190582275391 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00075578689575195 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061488151550293 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00025105476379395 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00074577331542969 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0010740756988525 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00033807754516602 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00050783157348633 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00033211708068848 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00053596496582031 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00061607360839844 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021100044250488 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00046205520629883 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00063705444335938 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021600723266602 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00045609474182129 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00066089630126953 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.000244140625 2025-02-20 16_52_13

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051999092102051 2025-02-20 16_52_13


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00065493583679199 2025-02-20 16_52_13

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029897689819336 2025-02-20 16_52_13

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00053811073303223 2025-02-20 16_52_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0035879611968994 2025-02-20 16_52_13

SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=1163 
 Execution Time:0.00062704086303711 2025-02-20 16_52_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0037059783935547 2025-02-20 16_52_13

SELECT *
FROM `categoria`
WHERE `activo` = 1
ORDER BY `categoriaId` DESC 
 Execution Time:0.00065517425537109 2025-02-20 16_52_13

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0040469169616699 2025-02-20 16_52_14

SELECT *
FROM `productos_caracteristicas`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.0013821125030518 2025-02-20 16_52_14

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.065582036972046 2025-02-20 16_52_14

SELECT *
FROM `productos_files`
WHERE `activo` = 1
AND `idproductos` = '1163' 
 Execution Time:0.00096011161804199 2025-02-20 16_52_14

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0010859966278076 2025-02-20 16_52_14

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.042728900909424 2025-02-20 16_52_14

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.24540591239929 2025-02-20 16_52_15

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00073409080505371 2025-02-20 16_52_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.017398118972778 2025-02-20 16_52_15

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.10379791259766 2025-02-20 16_52_15

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1 
 Execution Time:0.13181614875793 2025-02-20 16_52_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.003046989440918 2025-02-20 16_52_15

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.10126900672913 2025-02-20 16_52_15

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1 
 Execution Time:0.086220979690552 2025-02-20 16_52_15

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0041129589080811 2025-02-20 16_52_17

SELECT `p`.`id`, `p`.`referencia`, `c`.`categoria`, `p`.`mostrar_pagina`, `p`.`nombre`, `p`.`mas_vendidos`, `p`.`codigoBarras`, `ps`.`precio`, `p`.`idProducto`, `p`.`tipo`, `ps`.`precio_final`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
AND   (
`p`.`id` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`referencia` LIKE '%784512BAR%' ESCAPE '!'
OR  `c`.`categoria` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stock` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stockmin` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stockmax` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`precio_sin_iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`precio_con_iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`mostrar_pagina` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`mas_vendidos` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`codigoBarras` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`idProducto` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`tipo` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`precio` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`incluye_iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`precio_final` LIKE '%784512BAR%' ESCAPE '!'
 )
GROUP BY `p`.`id`
ORDER BY `p`.`idProducto`, `p`.`id` ASC
 LIMIT 10 
 Execution Time:0.096774816513062 2025-02-20 16_52_17

SELECT COUNT(1) as total
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`sucursalid`=2
WHERE `p`.`estatus` = 1
AND   (
`p`.`id` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`referencia` LIKE '%784512BAR%' ESCAPE '!'
OR  `c`.`categoria` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stock` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stockmin` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`stockmax` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`precio_sin_iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`precio_con_iva` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`mostrar_pagina` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`codigoBarras` LIKE '%784512BAR%' ESCAPE '!'
OR  `p`.`idProducto` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`precio` LIKE '%784512BAR%' ESCAPE '!'
OR  `ps`.`precio_final` LIKE '%784512BAR%' ESCAPE '!'
 ) 
 Execution Time:0.24365210533142 2025-02-20 16_52_17

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0054919719696045 2025-02-20 16_52_18

SELECT *
FROM `productos`
WHERE `id` = '4738' 
 Execution Time:0.00082015991210938 2025-02-20 16_52_18

SELECT *
FROM `proveedores`
WHERE `id` = '0' 
 Execution Time:0.00074005126953125 2025-02-20 16_52_18

SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=4738 and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='4738'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden 
 Execution Time:0.024098873138428 2025-02-20 16_52_18

SELECT *
FROM `referencias`
WHERE `activo` = 1 
 Execution Time:0.00037121772766113 2025-02-20 16_52_18

SELECT *
FROM `f_unidades`
WHERE `Clave` = 'H87' 
 Execution Time:0.0026149749755859 2025-02-20 16_52_18

SELECT *
FROM `f_servicios`
WHERE `Clave` = '01010101' 
 Execution Time:0.081080913543701 2025-02-20 16_52_18

SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1 
 Execution Time:0.00076007843017578 2025-02-20 16_52_18

SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0 
 Execution Time:0.001046895980835 2025-02-20 16_52_18

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='1' 
 Execution Time:0.0016410350799561 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0010039806365967 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0024540424346924 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=42 
 Execution Time:0.00031590461730957 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=42 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0013000965118408 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=24 
 Execution Time:0.00028491020202637 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=24 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='2'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00060606002807617 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00023508071899414 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.0022900104522705 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00072908401489258 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=28 
 Execution Time:0.000244140625 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=28 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049805641174316 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0007479190826416 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=6 
 Execution Time:0.00023293495178223 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=6 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='3'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051712989807129 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00091290473937988 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='4'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00068783760070801 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00067496299743652 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00047206878662109 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='5'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00053882598876953 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00022196769714355 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00136399269104 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00090813636779785 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=17 
 Execution Time:0.00025486946105957 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  AND menus.idsubmenu=17 and menus.submenutipo=1  and perfd.PerfilId='1' and menus.MenuId='6'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00049901008605957 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00031304359436035 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00041985511779785 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='7'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00054502487182617 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.0002129077911377 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051188468933105 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='8'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00053119659423828 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00029683113098145 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00051498413085938 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='9'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.0006260871887207 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00027298927307129 2025-02-20 16_52_18

SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId  and menus.submenutipo=0  and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC 
 Execution Time:0.00043296813964844 2025-02-20 16_52_18


            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='1' and menus.MenuId='10'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC 
 Execution Time:0.00057601928710938 2025-02-20 16_52_18

SELECT * FROM menu_sub WHERE MenusubId=0 
 Execution Time:0.00021696090698242 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=8 and disponible=1
            group by id 
 Execution Time:0.00086617469787598 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=8
        group by psl.id 
 Execution Time:0.00063490867614746 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=2 and disponible=1
            group by id 
 Execution Time:0.00090408325195312 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=2
        group by psl.id 
 Execution Time:0.0007779598236084 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=4 and disponible=1
            group by id 
 Execution Time:0.0012519359588623 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=4
        group by psl.id 
 Execution Time:0.019132852554321 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=5 and disponible=1
            group by id 
 Execution Time:0.046385049819946 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=5
        group by psl.id 
 Execution Time:0.0009760856628418 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=3 and disponible=1
            group by id 
 Execution Time:0.00096511840820312 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=3
        group by psl.id 
 Execution Time:0.000640869140625 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=6 and disponible=1
            group by id 
 Execution Time:0.00080609321594238 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=6
        group by psl.id 
 Execution Time:0.00082707405090332 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=7 and disponible=1
            group by id 
 Execution Time:0.00098800659179688 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=7
        group by psl.id 
 Execution Time:0.00072002410888672 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=9 and disponible=1
            group by id 
 Execution Time:0.00078797340393066 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=9
        group by psl.id 
 Execution Time:0.00073099136352539 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=10 and disponible=1
            group by id 
 Execution Time:0.00079894065856934 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=10
        group by psl.id 
 Execution Time:0.00078797340393066 2025-02-20 16_52_18

SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=4738 and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=4738 and sucursalid=11 and disponible=1
            group by id 
 Execution Time:0.00072288513183594 2025-02-20 16_52_18

SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=4738 and psl.sucursalid=11
        group by psl.id 
 Execution Time:0.00069808959960938 2025-02-20 16_52_18

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.00061917304992676 2025-02-20 16_52_18

SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1 
 Execution Time:0.00035691261291504 2025-02-20 16_52_19

SELECT `g`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `garantias` `g`
JOIN `personal` `pe` ON `pe`.`personalId`=`g`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`g`.`id_origen`
WHERE `g`.`estatus` = 1
AND `g`.`activo` = 1
ORDER BY `g`.`id` DESC 
 Execution Time:0.0013179779052734 2025-02-20 16_52_19

SELECT `t`.*, `pe`.`nombre` AS `personal`, `s`.`name_suc` AS `suc_solicita`, `s`.`id_alias`
FROM `traspasos` `t`
JOIN `historial_transpasos` `ht` ON `ht`.`idtranspasos`=`t`.`id`
JOIN `personal` `pe` ON `pe`.`personalId`=`t`.`idpersonal`
JOIN `sucursal` `s` ON `s`.`id`=`ht`.`idsucursal_entra`
WHERE `t`.`status` = 1
AND `ht`.`activo` = 1
AND (`ht`.`idsucursal_sale` = 2 or `ht`.`idsucursal_entra` = 2)
GROUP BY `t`.`id`
ORDER BY `t`.`id` DESC 
 Execution Time:0.026201009750366 2025-02-20 16_52_19

SELECT `r`.*, `c`.`nombre`, `s`.`descripcion`, `pss`.`serie`, `suc`.`name_suc`, `suc`.`id_alias`
FROM `rentas` `r`
JOIN `clientes` `c` ON `c`.`id`=`r`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`r`.`id_servicio`
JOIN `productos_sucursales_serie` `pss` ON `pss`.`id`=`r`.`id_serie`
JOIN `sucursal` `suc` ON `suc`.`id`=`r`.`id_sucursal`
WHERE (`r`.`estatus` =0 or `r`.`pagado` =0)
ORDER BY `r`.`id` DESC 
 Execution Time:0.00066685676574707 2025-02-20 16_52_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0015239715576172 2025-02-20 16_52_19

SELECT *
FROM `productos_files`
WHERE `activo` = 1
AND `idproductos` = '4738' 
 Execution Time:0.00037503242492676 2025-02-20 16_52_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.025738954544067 2025-02-20 16_52_19

SELECT *
FROM `categoria`
WHERE `activo` = 1
ORDER BY `categoriaId` DESC 
 Execution Time:0.0006260871887207 2025-02-20 16_52_19

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0033578872680664 2025-02-20 16_52_20

SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=4738 
 Execution Time:0.00075697898864746 2025-02-20 16_52_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0038681030273438 2025-02-20 16_52_20

SELECT *
FROM `productos_caracteristicas`
WHERE `activo` = 1
AND `idproductos` = '4738' 
 Execution Time:0.0006871223449707 2025-02-20 16_52_20

SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=1 AND MenusubId=3 
 Execution Time:0.0037670135498047 2025-02-20 16_55_10

SELECT *
FROM `sucursal`
WHERE `activo` = 1
ORDER BY `orden` ASC 
 Execution Time:0.0020859241485596 2025-02-20 16_55_10

SELECT `p`.`id`, `p`.`idProducto`, `p`.`nombre`, `p`.`codigoBarras`, `p`.`tipo`, `p`.`costo_compra`, `p`.`unidad_sat`, `p`.`servicioId_sat`, `p`.`incluye_iva_comp`, `p`.`iva_comp`, `p`.`isr`, `p`.`porc_isr`, `pp`.`codigo` as `cod_prov`, `pro`.`nombre` as `proveedor`, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
        COALESCE(CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>') ORDER BY s.orden ASC SEPARATOR ''), CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', `psl`.`cantidad`, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END, CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')) AS stock, /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock, */
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva, CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva, CASE 
            WHEN p.tipo = 0 THEN COALESCE('', '') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'), '')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', `psl`.`lote`, ' / ', psl.caducidad SEPARATOR '<br>'), '')
            ELSE '' 
        END AS detalle, `c`.`categoria`
FROM `productos` `p`
LEFT JOIN `categoria` `c` ON `c`.`categoriaId`=`p`.`categoria`
LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id`
LEFT JOIN `proveedores` `pro` ON `pro`.`id`=`pp`.`idproveedor`
JOIN `sucursal` `s` ON `s`.`activo`=1
JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1
LEFT JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid` = `p`.`id` AND `pss`.`sucursalid`=`ps`.`sucursalid` AND `pss`.`activo` = 1 AND `pss`.`disponible` = 1 AND `pss`.`vendido`=0 AND `p`.`tipo` = 1
LEFT JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid` = `p`.`id` AND `psl`.`sucursalid`=`ps`.`sucursalid` AND `psl`.`activo` = 1 AND `p`.`tipo` = 2
WHERE (`p`.`activo` = 1 OR `p`.`activo` = 'Y')
AND `s`.`activo` = '1'
GROUP BY `p`.`id`
ORDER BY `ps`.`sucursalid` ASC, `s`.`orden` ASC, `p`.`idProducto` ASC 
 Execution Time:11.611126184464 2025-02-20 16_55_10

