<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelSolicitudes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function get_listado($params){
        $columns = array( 
            0=>'r.id',
            1=>'r.nombre',
            2=>'r.correo',
            3=>'r.productos',
            4=>'r.comentarios',
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('requerimientos r');
        $where = array('r.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_listado($params){
        $columns = array( 
            0=>'r.id',
            1=>'r.nombre',
            2=>'r.correo',
            3=>'r.productos',
            4=>'r.comentarios',
        ); 

        $this->db->select('COUNT(1) as total');
        $this->db->from('requerimientos r');
        $where = array('r.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}