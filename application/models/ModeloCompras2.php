<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloCompras extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function obtener_folio_v($sucursal){
        $strq = "SELECT max(v.folio_con)+1 as folio FROM compra_erp as v WHERE v.sucursal=$sucursal";
        $query = $this->db->query($strq);
        $folio=1;
        foreach ($query->result() as $item) {
            if($item->folio>0){
                $folio=$item->folio;
            }
        }
        return $folio;
    }

    function infocomprapre($id){
        $strq = "SELECT c.*,p.nombre,p.codigo,p.contacto
        FROM compra_erp as c 
        LEFT JOIN proveedores AS p ON p.id = c.idproveedor
        WHERE c.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function getcompras($params){
        $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion'
        );

        $columnx = array( 
            0=>'c.id',
            1=>'p.nombre as personal',
            2=>'c.reg',
            3=>'pr.nombre as proveedor', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'IFNULL(re.id,0) as id_re'
        );

        $select="";
        foreach ($columnx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        $this->db->join('bitacora_reverse_entradas re', 're.id_ordencomp=c.id','left');
        $this->db->where(array('c.precompra'=>0));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }      
        if($params['estatus']==1){
            $where = array('c.estatus'=>0);
            $this->db->where($where);
        }else if($params['estatus']==2){
            $where = array('c.estatus'=>1);
            $this->db->where($where);
        }      
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);

        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_getcompras($params){
         $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        
        if($params['estatus']==1){
            $where = array('c.estatus'=>0);
            $this->db->where($where);
        }else if($params['estatus']==2){
            $where = array('c.estatus'=>1);
            $this->db->where($where);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    public function getDataBitacora($id){
        $this->db->select('bit.*, u.Usuario');
        $this->db->from('bitacora_reverse_entradas bit');
        $this->db->join('usuarios u','u.UsuarioID=bit.id_usuario');
        $this->db->where('bit.id_ordencomp',$id);
        $query=$this->db->get();
        return $query->result();
    }

    public function compras_detalles($idcompra,$oc=0){
        $group=""; $select=""; $join="";
        $select=", pss.serie, IFNULL(psl.cantidad,'') as cant_ls, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad";
            $join="LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and pro.tipo=1
                    LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and pro.tipo=2";
        if($oc==1){
            //$select=", pss.serie, IFNULL(psl.cantidad,'') as cant_ls, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad";
            $group="group by cd.idproducto";
            /*$join="LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and pro.tipo=1
                    LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and pro.tipo=2";*/
        }
        $strq = "SELECT pro.idProducto,
                        cd.cantidad,
                        pro.nombre,
                        cd.precio_unitario,
                        cd.idproducto,
                        pro.tipo AS tipo_s,
                        pro.incluye_iva_comp,
                        pro.iva_comp,
                        cd.tipo, r.codigo,r.capacidad,
                        cd.id
                        $select                        
                    FROM compra_erp_detalle as cd 
                    LEFT JOIN productos as pro on pro.id=cd.idproducto and cd.tipo=0
                    LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
                    $join
                    WHERE cd.activo=1 AND cd.idcompra=$idcompra

                    $group
                    ";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function get_productos($id){
        $strq = "SELECT p.idProducto, p.codigoBarras, p.nombre, p.tipo as tipo_prod,p.incluye_iva_comp,p.iva_comp,
                cd.cantidad,cd.precio_unitario,cd.idproducto,cd.tipo,r.id as id_tanque, r.codigo,r.capacidad
                FROM compra_erp_detalle AS cd 
                LEFT JOIN productos AS p ON p.id=cd.idproducto and cd.tipo=0
                /*LEFT JOIN productos_sucursales AS ps ON ps.productoid=cd.idproducto*/
                LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
                WHERE cd.idcompra=$id AND cd.activo=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    /*public function get_productos($id){
        $strq = "SELECT p.idProducto, p.codigoBarras, p.nombre, p.tipo as tipo_prod,
                 IFNULL(ps.incluye_iva,0) as incluye_iva, IFNULL(ps.iva,0) as iva,
                cd.cantidad,cd.precio_unitario,cd.idproducto,cd.tipo,r.id as id_tanque, r.codigo,r.capacidad
                FROM compra_erp_detalle AS cd 
                LEFT JOIN productos AS p ON p.id=cd.idproducto and cd.tipo=0
                LEFT JOIN productos_sucursales AS ps ON ps.productoid=cd.idproducto
                LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
                WHERE cd.idcompra=$id AND cd.activo=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }*/

    function get_compra($id)
    {
        $strq ="SELECT c.*,pr.nombre,pr.direccion,pr.cp,pr.rfc
            FROM compra_erp AS c 
            LEFT JOIN proveedores AS pr ON pr.id=c.idproveedor  
            WHERE c.activo=1 AND c.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_compra_erp_detalle($id){
        $strq = "SELECT p.id,p.idProducto,p.nombre,cd.precio_unitario,cd.cantidad,cd.tipo, r.id as id_tanque,p.incluye_iva_comp,p.iva_comp, 
            r.codigo,IFNULL(r.capacidad,0) as capacidad
            from compra_erp_detalle AS cd 
            LEFT JOIN productos AS p ON p.id=cd.idproducto
            LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
            where cd.id=$id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function sumar_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock+$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
    }

    function resta_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock-$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
    }
    
    function update_distribusion($idcompra,$idproducto){
        $strq = "UPDATE compra_erp_detalle set distribusion=1 where idcompra=$idcompra AND idproducto=$idproducto";
        $query = $this->db->query($strq);
        $this->db->close();
    }

}