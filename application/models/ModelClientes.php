<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelClientes extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.nombre',
            2=>'p.usuario',
            3=>'p.motivo',
            4=>'p.desactivar',
            5=>'p.saldo',
            6=>'s.clave'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes p');
        $this->db->join('sucursal s','s.id=p.sucursal','left');
        if($params['tipocliente']!=0){
            $this->db->where(array('tipo_registro'=>$params['tipocliente']));
        }
        if($params['desactivar']!=2){
            $this->db->where(array('desactivar'=>$params['desactivar']));
        }
        if($params['sucursal']!=0){
            $this->db->where(array('sucursal'=>$params['sucursal']));
        }
        $where = array('p.activo'=>1);
        $this->db->where($where);
        $this->db->where(array('p.id !='=>8255));// el id de publico general en productivo
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.nombre',
            2=>'p.usuario',
            3=>'p.motivo',
            4=>'p.desactivar',
            5=>'p.saldo',
            6=>'s.clave'
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('clientes p');
        $this->db->join('sucursal s','s.id=p.sucursal','left');
        if($params['tipocliente']!=0){
            $this->db->where(array('tipo_registro'=>$params['tipocliente']));
        }
        if($params['desactivar']!=2){
            $this->db->where(array('desactivar'=>$params['desactivar']));
        }
        if($params['sucursal']!=0){
            $this->db->where(array('sucursal'=>$params['sucursal']));
        }
        $where = array('p.activo'=>1);
        $this->db->where($where);
        $this->db->where(array('p.id !='=>8255));// el id de publico general en productivo
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getDatosCPEstado_gruop($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado,ep.mnpio");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $this->db->group_by("ep.codigo");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDatosCPEstado($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $query=$this->db->get();
        return $query->result();
    }

    function getventas($params){

        $columns = array( 
            0=>'v.folio',
            1=>'suc.name_suc',
            2=>'v.reg',
            3=>'clif.razon_social',
            4=>'v.total', 
            5=>'v.id',
            6=>'fac.FacturasId',
            7=>'fac.Estado',
            8=>'fac.rutaXml',
            9=>'v.activo',
            10=>'p.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('venta_erp v');
        $this->db->join('sucursal suc', 'suc.id=v.sucursal');
        $this->db->join('personal p', 'p.personalId=v.personalid');
        $this->db->join('cliente_fiscales clif', 'clif.id =v.id_razonsocial');
        $this->db->join('venta_fk_factura vf', 'vf.idventa=v.id','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId=vf.idfactura','left');
        $this->db->where(array('v.activo'=>1));
        $this->db->where(array('clif.idcliente'=>$params['idcliente']));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_getventas($params){
        $columns = array( 
            0=>'v.folio',
            1=>'suc.name_suc',
            2=>'v.reg',
            3=>'clif.razon_social',
            4=>'v.total',
            5=>'v.id',
            6=>'fac.FacturasId',
            7=>'fac.Estado',
            8=>'fac.rutaXml',
            9=>'p.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('venta_erp v');
        $this->db->join('sucursal suc', 'suc.id=v.sucursal');
        $this->db->join('cliente_fiscales clif', 'clif.id =v.id_razonsocial');
        $this->db->join('venta_fk_factura vf', 'vf.idventa=v.id','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId=vf.idfactura','left');
        
        $this->db->where(array('v.activo'=>1));
        $this->db->where(array('clif.idcliente'=>$params['idcliente']));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    

}