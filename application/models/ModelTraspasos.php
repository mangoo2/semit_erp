<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelTraspasos extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->sucursal=$this->session->userdata('sucursal');
        $this->perfilid=$this->session->userdata('perfilid');
    }

    function get_list($params,$perfilid,$idpersonal){
        $columns = array( 
            0=>'t.id',
            1=>'t.reg',
            2=>'pe.nombre',
            3=>'t.tipo',
            4=>'t.carta_porte',
            5=>'ht.idProducto',
            6=>'fac.FacturasId',
            7=>'fac.rutaXml',
            8=>'fac.Estado',
        );

        $columnsx = array( 
            0=>'t.id',
            1=>'DATE_FORMAT(t.reg, "%d/%m/%Y - %r" ) AS reg',
            2=>'pe.nombre AS personal',
            3=>'suc.name_suc AS suc_solicita',
            4=>'sucy.name_suc AS suc_salida',
            5=>'t.status',
            6=>'t.rechazado',
            7=>'t.tipo',
            8=>'fac.FacturasId',
            9=>'fac.rutaXml',
            10=>'fac.Estado',
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('traspasos t');
        $this->db->join('historial_transpasos ht', 'ht.idtranspasos=t.id and ht.activo');
        $this->db->join('sucursal suc', 'suc.id=ht.idsucursal_entra');
        $this->db->join('sucursal sucy', 'sucy.id=ht.idsucursal_sale');
        $this->db->join('personal pe', 'pe.personalId=t.idpersonal');
        $this->db->join('f_facturas fac', 'fac.FacturasId=t.Idfactura','left');
        if($perfilid==2 || $perfilid==3){
            //$where = array('t.activo'=>1,'t.idpersonal'=>$idpersonal);
        }else{
            //$where = array('t.activo'=>1);
        }
        //$this->db->where($where);
        $this->db->where('t.activo',1);
        if($params["estatus"]==0) { //Apartado
            $this->db->where("t.status",0);
        }
        if($params["estatus"]==1) { //transito
            $this->db->where("t.status",1);
        }
        if($params["estatus"]==2) { //Ingresado
            $this->db->where("t.status",2);
        }
        if($params["estatus"]==3) { //rechazado
            $this->db->where("t.rechazado",1);
        }
 
        //validar si el usuario es almacen o admin, traerle los historial_transpasos de almacen web y almacen rentas tambien
        //validar si idsucursal_entra es la misma del usuario entrante
        if($perfilid==1 || $perfilid==5){ //admin o almacén
            //$this->db->or_where("(idsucursal_entra=6 and t.activo=1 or idsucursal_entra=7 and t.activo=1)");
        }else{
            $suc=$this->sucursal;
            if($this->perfilid==10){
                $suc=7;
            }
            $this->db->where("(idsucursal_entra=".$suc." and t.activo=1)");
        }
        $this->db->group_by("t.id");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params,$perfilid,$idpersonal){
        $columns = array( 
            0=>'t.id',
            1=>'t.reg',
            2=>'pe.nombre',
            3=>'t.tipo',
            4=>'t.carta_porte',
            5=>'ht.idProducto'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('traspasos t');
        $this->db->join('historial_transpasos ht', 'ht.idtranspasos=t.id and ht.activo');
        $this->db->join('personal pe', 'pe.personalId=t.idpersonal');
        $this->db->join('sucursal suc', 'suc.id=ht.idsucursal_entra');
        $this->db->join('sucursal sucy', 'sucy.id=ht.idsucursal_sale');
        if($perfilid==2 || $perfilid==3){
            //$where = array('t.activo'=>1,'t.idpersonal'=>$idpersonal);
        }else{
            //$where = array('t.activo'=>1);
        }
        //$this->db->where($where);
        $this->db->where('t.activo',1);
        if($params["estatus"]==0) { //Apartado
            $this->db->where("t.status",0);
        }
        if($params["estatus"]==1) { //transito
            $this->db->where("t.status",1);
        }
        if($params["estatus"]==2) { //Ingresado
            $this->db->where("t.status",2);
        }
        if($params["estatus"]==3) { //rechazado
            $this->db->where("t.rechazado",1);
        }

        //validar si el usuario es almacen o admin, traerle los historial_transpasos de almacen web y almacen rentas tambien
        //validar si idsucursal_entra es la misma del usuario entrante
        if($perfilid==1 || $perfilid==5){ //admin o almacén
            //$this->db->or_where("(idsucursal_entra=6 and t.activo=1 or idsucursal_entra=7 and t.activo=1)");
        }else{
            $suc=$this->sucursal;
            if($this->perfilid==10){
                $suc=7;
            }
            $this->db->where("(idsucursal_entra=".$suc." and t.activo=1)");
        }

        $this->db->group_by("t.id");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        //return $query->row()->total;
        return $query->num_rows();
    }

    /*function get_list($params){
        $columns = array( 
            0=>'h.id',
            1=>'h.reg',
            2=>'h.cantidad',
            3=>'sx.name_suc',
            4=>'sy.name_suc',
            5=>'p.nombre',
            6=>'pe.nombre',
        );

        $columnsx = array( 
            0=>'h.id',
            1=>'DATE_FORMAT(h.reg, "%d/%m/%Y - %r" ) AS reg',
            2=>'h.cantidad',
            3=>'sx.name_suc AS sucursalx',
            4=>'sy.name_suc AS sucursaly',
            5=>'p.nombre AS producto',
            6=>'pe.nombre AS personal',
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('historial_transpasos h');
        $this->db->join('sucursal sx', 'sx.id=h.idsucursal_sale');
        $this->db->join('sucursal sy', 'sy.id=h.idsucursal_entra');
        $this->db->join('productos p', 'p.id=h.idproducto');
        $this->db->join('personal pe', 'pe.personalId=h.idpersonal');
        $where = array('h.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $columns = array( 
            0=>'h.id',
            1=>'h.reg',
            2=>'h.cantidad',
            3=>'sx.name_suc',
            4=>'sy.name_suc',
            5=>'p.nombre',
            6=>'pe.nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('historial_transpasos h');
        $this->db->join('sucursal sx', 'sx.id=h.idsucursal_sale');
        $this->db->join('sucursal sy', 'sy.id=h.idsucursal_entra');
        $this->db->join('productos p', 'p.id=h.idproducto');
        $this->db->join('personal pe', 'pe.personalId=h.idpersonal');
        $where = array('h.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }*/

    public function get_productos_stock_suc($valor){
        $strq = "SELECT 0 as tipo, p.id,p.nombre,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2) AS stock2,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=3) AS stock3,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=4) AS stock4,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=5) AS stock5,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=6) AS stock6,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=7) AS stock7,(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=8) AS stock8
                FROM productos as p 
                WHERE p.id=$valor";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function sumar_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock+$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function resta_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock-$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function update_tanque_sucu($id,$sucursalid,$can,$opera){
        $where="";
        /*if($opera=="+"){
            $where="and id=".$id;
        }*/
        $strq = "UPDATE recargas set stock=stock $opera $can 
                where sucursal=$sucursalid and tipo=0 $where";
        $query = $this->db->query($strq);
    }

    public function get_traspasos_series_info($id,$id_prod){
        $strq = "SELECT ps.serie
            FROM traspasos_series_historial as t
            INNER JOIN productos_sucursales_serie AS ps ON ps.id=t.idseries and ps.productoid=$id_prod
            WHERE ps.activo=1 and t.idtraspasos=$id and t.activo=1
            group by t.id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }
    
    public function get_traspasos_lotes_info($id,$id_prod){
        $strq = "SELECT ps.lote, t.cantidad
        FROM traspasos_lotes_historial as t
        INNER JOIN historial_transpasos AS ht ON ht.id=t.id_historialt or ht.idtranspasos=t.idtraspasos
        INNER JOIN productos_sucursales_lote AS ps ON ps.id=t.idlotes and ps.productoid=ht.idproducto and ps.activo=1
        WHERE ps.activo=1 and t.idtraspasos=$id and t.activo=1 and ps.productoid=$id_prod
        group by t.id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function sumar_lote($id,$can){
        $strq = "UPDATE productos_sucursales_lote set cantidad=cantidad+$can where id=$id";
        $query = $this->db->query($strq);
    }

    function resta_lote($id,$can){
        $strq = "UPDATE productos_sucursales_lote set cantidad=cantidad-$can where id=$id";
        $query = $this->db->query($strq);
    }

    function getSolicitaDia($dia,$suc){
        $this->db->select('*');
        $this->db->from("solicitud_traspaso st");
        $this->db->join("historial_transpasos ht","ht.idtranspasos=st.id_traspaso and ht.idsucursal_entra=$suc");
        $this->db->where("dia_venta",$dia);
        $query=$this->db->get(); 
        return $query;
    }

    public function get_productos_sucursales_order($id,$id_suc,$perfil){
        $where="AND s.id != $id_suc ";
        if($perfil==5){ //almacen
            $where="";
        }
        $strq = "SELECT ps.*,s.orden, s.name_suc, IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=ps.productoid and ht.activo=1 and ht.idsucursal_sale=ps.sucursalid and ht.tipo=0 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant, ps.sucursalid
        FROM sucursal s
        left join productos_sucursales ps on ps.sucursalid=s.id and ps.activo = 1 and productoid = $id
        WHERE s.activo=1
        $where
        order by s.orden asc";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_all_stock_lote($id,$id_suc){
        $strq = "SELECT p.id,p.nombre,0 as tipo, 
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=$id and sucursalid=$id_suc),0) AS stock_lote,
        IFNULL(tlh.cantidad,0) as tras_stock_lote
        FROM productos as p
        left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$id_suc and psl.cantidad>0
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        where p.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

    public function get_all_stock_serie($id,$id_suc){
        $strq = "SELECT p.id,p.nombre,0 as tipo, count(tsh.id) as tot_tras,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=$id and sucursalid=$id_suc),0) AS stock_serie
        FROM productos as p
        left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$id_suc
        left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
        where p.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

    public function get_all_stock_tanque($suc,$id_prod){
        $strq = "SELECT r.*, 
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=r.id and ht.activo=1 and ht.idsucursal_sale=r.sucursal and ht.tipo=1 and ht.status=1 and ht.rechazado!=1),0) as traslado_stock_cant
        FROM recargas as r
        where sucursal=$suc and tipo=0 and estatus=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getNotificaTraspaso($idsuc){
        /*$this->db->select('ht.*, pe.nombre AS personal, s.name_suc AS suc_solicita');
        $this->db->from("historial_transpasos ht");
        $this->db->join('personal pe', 'pe.personalId=ht.idpersonal');
        $this->db->join('sucursal s', 's.id=ht.idsucursal_entra');
        $this->db->where("ht.status",0); //solicitado - apartado
        $this->db->where("ht.activo",1);
        $this->db->where("ht.idsucursal_sale",$idsuc);*/

        $this->db->select('t.*, pe.nombre AS personal, s.name_suc AS suc_solicita, s.id_alias');
        $this->db->from("traspasos t");
        $this->db->join('historial_transpasos ht', 'ht.idtranspasos=t.id');
        $this->db->join('personal pe', 'pe.personalId=t.idpersonal');
        $this->db->join('sucursal s', 's.id=ht.idsucursal_entra');
        $this->db->where("t.status",1); 
        $this->db->where("ht.activo",1);
        $this->db->where("(ht.idsucursal_sale =".$idsuc." or ht.idsucursal_entra=".$idsuc.")");
        $this->db->group_by("t.id");
        $this->db->order_by("t.id","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getPersonalTraslado($id){
        $this->db->select('ht.*, IFNULL(pe.correo,"") as correo_entra, IFNULL(ps.correo,"") as correo_sale, se.name_suc as suc_solicita,
            IFNULL(palm.correo,"") as correo_alm');
        $this->db->from("historial_transpasos ht");

        $this->db->join('usuarios ualm', 'ualm.perfilid=5 and ualm.estatus=1','left'); //perfil almacen
        $this->db->join('personal palm', 'palm.personalId=ualm.personalId and palm.estatus=1','left');

        $this->db->join('usuarios ue', 'ue.sucursal=ht.idsucursal_entra and ue.estatus=1','left');
        $this->db->join('personal pe', 'pe.personalId=ue.personalId and pe.estatus=1','left');
        $this->db->join('sucursal se', 'se.id=ht.idsucursal_entra');

        $this->db->join('usuarios us', 'us.sucursal=ht.idsucursal_sale and us.estatus=1','left');
        $this->db->join('personal ps', 'ps.personalId=us.personalId and ps.estatus=1','left');
        $this->db->where("ht.idtranspasos",$id);
        //$this->db->group_by("ht.idtranspasos");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getProductoCodigo($cod,$idsuc){
        /*
        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, IFNULL(ps.stock,0) as stock, 0 as id_prod_suc, 0 as id_prod_suc_serie, "" as lote, "" as caducidad, "" as serie, ps.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1 and ps.sucursalid=".$idsuc,"left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("codigoBarras",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query1 = $this->db->get_compiled_select();  

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, IFNULL(psl.cantidad,0) as stock, psl.id as id_prod_suc, 0 as id_prod_suc_serie, psl.lote, psl.caducidad, "" as serie, psl.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_lote psl","psl.productoid=p.id and psl.activo=1 and psl.sucursalid=".$idsuc,"left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("psl.cod_barras",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query2 = $this->db->get_compiled_select();  

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, 1 as stock, 0 as id_prod_suc, pss.id as id_prod_suc_serie, "" as lote, "" as caducidad, pss.serie, pss.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_serie pss","pss.productoid=p.id and pss.activo=1 and pss.sucursalid=".$idsuc,"left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("pss.serie",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query3 = $this->db->get_compiled_select();

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, IFNULL(ps.stock,0) as stock, 0 as id_prod_suc, 0 as id_prod_suc_serie, "" as lote, "" as caducidad, "" as serie, ps.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_proveedores pp","pp.idproducto=p.id and pp.activo=1","left");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1 and ps.sucursalid=".$idsuc,"left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("pp.codigo",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $this->db->group_by("p.id");
        $query4 = $this->db->get_compiled_select();

        $queryunions=$query1." UNION ".$query2." UNION ".$query3." UNION ".$query4;

        return $this->db->query($queryunions)->result();
        */
        $strq="
        SELECT `p`.`id`, `p`.`idProducto`, `p`.`tipo`, `p`.`nombre`, IFNULL(ps.stock, 0) as stock, 0 as `id_prod_suc`, 0 as `id_prod_suc_serie`, '' as `lote`, '' as `caducidad`, '' as `serie`, `ps`.`sucursalid`, IFNULL(fu.nombre, '') as nom_unidad,
            ser.material_peligroso as peligro
            FROM `productos` `p`
            JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`='$idsuc'
            LEFT JOIN `f_unidades` `fu` ON `fu`.`Clave`=`p`.`unidad_sat`
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            WHERE `p`.`estatus` = 1
            AND  (`codigoBarras` LIKE '%$cod%'
            OR  `p`.`idProducto` LIKE '%$cod%') 

            UNION 
            SELECT `p`.`id`, `p`.`idProducto`, `p`.`tipo`, `p`.`nombre`, IFNULL(psl.cantidad, 0) as stock, `psl`.`id` as `id_prod_suc`, 0 as `id_prod_suc_serie`, `psl`.`lote`, `psl`.`caducidad`, '' as `serie`, `psl`.`sucursalid`, IFNULL(fu.nombre, '') as nom_unidad,
            ser.material_peligroso as peligro
            FROM `productos` `p`
            JOIN `productos_sucursales_lote` `psl` ON `psl`.`productoid`=`p`.`id` and `psl`.`activo`=1 and `psl`.`sucursalid`='$idsuc'
            LEFT JOIN `f_unidades` `fu` ON `fu`.`Clave`=`p`.`unidad_sat`
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            WHERE `p`.`estatus` = 1
            AND  (`psl`.`cod_barras` LIKE '%$cod%' 
            OR  `p`.`idProducto` LIKE '%$cod%')  
            UNION 
            SELECT `p`.`id`, `p`.`idProducto`, `p`.`tipo`, `p`.`nombre`, 1 as `stock`, 0 as `id_prod_suc`, `pss`.`id` as `id_prod_suc_serie`, '' as `lote`, '' as `caducidad`, `pss`.`serie`, `pss`.`sucursalid`, IFNULL(fu.nombre, '') as nom_unidad,
            ser.material_peligroso as peligro
            FROM `productos` `p`
            JOIN `productos_sucursales_serie` `pss` ON `pss`.`productoid`=`p`.`id` and `pss`.`activo`=1 and `pss`.`sucursalid`='$idsuc'
            LEFT JOIN `f_unidades` `fu` ON `fu`.`Clave`=`p`.`unidad_sat`
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            WHERE `p`.`estatus` = 1
            AND  (`pss`.`serie` LIKE '%$cod%' 
            OR  `p`.`idProducto` LIKE '%$cod%'  )
            UNION 
            SELECT `p`.`id`, `p`.`idProducto`, `p`.`tipo`, `p`.`nombre`, IFNULL(ps.stock, 0) as stock, 0 as `id_prod_suc`, 0 as `id_prod_suc_serie`, '' as `lote`, '' as `caducidad`, '' as `serie`, `ps`.`sucursalid`, IFNULL(fu.nombre, '') as nom_unidad,
            ser.material_peligroso as peligro
            FROM `productos` `p`
            LEFT JOIN `productos_proveedores` `pp` ON `pp`.`idproducto`=`p`.`id` and `pp`.`activo`=1
            JOIN `productos_sucursales` `ps` ON `ps`.`productoid`=`p`.`id` and `ps`.`activo`=1 and `ps`.`sucursalid`='$idsuc'
            LEFT JOIN `f_unidades` `fu` ON `fu`.`Clave`=`p`.`unidad_sat`
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            WHERE `p`.`estatus` = 1
            AND  (`pp`.`codigo` LIKE '%$cod%' 
            OR  `p`.`idProducto` LIKE '%$cod%' )
            GROUP BY `p`.`id`;
        ";
        return $this->db->query($strq)->result();
    }

    function getProductosOC($id){
        $this->db->select("p.*, ps.stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid, c.cantidad as cant_compra,
        IFNULL(psl.cantidad,0) as cant_lote, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad,
        IFNULL(pss.serie,'') as serie,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8 and p.tipo=2),0) AS lote8, IFNULL(psl.id,'0') as id_prod_suc,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8 and p.tipo=1),0) as serie8, IFNULL(pss.id,'0') as id_prod_suc_serie");
        $this->db->from("compra_erp_detalle c");
        $this->db->join("productos p","p.id=c.idproducto and (p.activo=1 or p.activo='Y')");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1 and ps.sucursalid=8","left");
        $this->db->join("productos_sucursales_lote psl","psl.productoid=p.id and p.tipo=2 and psl.activo=1 and psl.sucursalid=8 and psl.idcompra=$id","left");
        $this->db->join("productos_sucursales_serie pss","pss.productoid=p.id and p.tipo=1 and pss.activo=1 and pss.sucursalid=8 and pss.idcompras=$id","left");
        $this->db->where("c.idcompra",$id);
        $this->db->where("c.tipo",0);
        $this->db->where("c.activo",1);
        $this->db->where("c.distribusion",0);
        //$this->db->group_by("c.idproducto");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getComprasProds(){
        $this->db->select("c.id,c.folio");
        $this->db->from("compra_erp c");
        $this->db->join("compra_erp_detalle cd","cd.idcompra=c.id and cd.activo=1 and cd.distribusion=0 and cd.tipo=0");
        $this->db->where("c.activo",1);
        $this->db->where("c.estatus",1);
        $this->db->where("c.distribusion",0);
        $this->db->group_by("c.id");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getSeries_prod($id,$suc){
        $this->db->select("pss.id, pss.serie, pss.reg, p.idProducto, p.nombre");
        $this->db->from('productos_sucursales_serie pss');
        $this->db->join('productos p', 'p.id=pss.productoid');
        $this->db->where('pss.productoid',$id); 
        $this->db->where('pss.sucursalid',$suc);
        $this->db->where('pss.activo',1); 
        $this->db->where('pss.disponible',1);  
        $query = $this->db->get();
        return $query->result();
    }

    function getLotes_prod($id,$suc){
        $this->db->select("psl.id, psl.cantidad, psl.lote, psl.caducidad, psl.cod_barras, p.idProducto, p.nombre");
        $this->db->from('productos_sucursales_lote psl');
        $this->db->join('productos p', 'p.id=psl.productoid');
        $this->db->where('psl.productoid',$id); 
        $this->db->where('psl.sucursalid',$suc);
        $this->db->where('psl.activo',1); 
        $this->db->order_by('psl.caducidad',"asc"); 
        $query = $this->db->get();
        return $query->result();
    }

    function getSumSerie($id,$id_suc){
        $this->db->select("count(*) as tot_series");
        $this->db->from("productos_sucursales_serie pss");
        $this->db->where("pss.activo",1);
        $this->db->where("pss.disponible",1);
        $this->db->where("pss.vendido",0);
        $this->db->where("pss.productoid",$id);
        $this->db->where("pss.sucursalid",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function getSumLotes($where){
        $this->db->select("IFNULL(sum(psl.cantidad),0) as tot_lotes");
        $this->db->from("productos_sucursales_lote psl");
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->row();
    }

}