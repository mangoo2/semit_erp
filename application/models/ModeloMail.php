<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloMail extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->protocol='smtp';
        $this->smtp_host='mail.semit.mx';
        $this->smtp_user='info@semit.mx';
        $this->smtp_pass='yUs&J=T4*V$J';
        $this->smtp_port='465';
        $this->smtp_crypto='ssl';
    }


    function envio_fac($idfac,$correos){
        //log_message('error','correos: '.$correos);
        $contactosarray = array();
        $array_correos = explode(";", $correos);
        for ($i = 0; $i < count($array_correos); $i++) {
            $contactosarray[] = $array_correos[$i];
        }



        $strqfac="SELECT * FROM f_facturas  WHERE FacturasId='$idfac'";
                $query_fac = $this->db->query($strqfac);
                $query_fac=$query_fac->row();

        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = $this->protocol;
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] =$this->smtp_host; 
             
            //Nuestro usuario
            $config["smtp_user"] = $this->smtp_user;

            //Nuestra contraseña
            $config["smtp_pass"] = $this->smtp_pass;

            //Puerto
            $config["smtp_port"] = $this->smtp_port;

            $config["smtp_crypto"] = $this->smtp_crypto;
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            $asunto='Factura';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@semit.mx',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc('info@semit.mx');
                

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            $this->email->to($contactosarray);


        $message  = '<div style="background: url('.base_url().'public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="'.base_url().'public/img/SEMIT.jpg" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                
                            </div>
                            <div style="width: 30%;float: left;">
                                
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Facturación </b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Facturado:</div>
                            <div style="width: 70%;float: left;">'.$query_fac->Nombre.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Folio:</div>
                            <div style="width: 70%;float: left;">'.$query_fac->serie.''.$query_fac->Folio.'</div>
                        </div>
                        
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            <p>Estimado Cliente:</p>

                            <p>Adjuntamos el archivo de su factura en formatos PDF y XML.</p>

                            <p>Favor no responder este mensaje. Este es un envío automatizado de documentos.</p>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <!--<img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">-->
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);

        $urlfac=FCPATH.'hulesyg/facturaspdf/Factura_'.$query_fac->Folio.'.pdf';
        $this->email->attach($urlfac);
        
        $urlxml=FCPATH.$query_fac->rutaXml;
        $this->email->attach($urlxml);
        
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            //log_message('error', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
            //log_message('error', 'No se a enviado el email');
        }

        //==================
    }
    

}