<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }


    function get_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.puesto',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre as perfil',
            7=>'s.name_suc',
            8=>'u.acceso',
            9=>'u.motivo_desactivar',
        );
        $columnsss = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.puesto',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre',
            7=>'s.name_suc',
            8=>'u.acceso',
            9=>'u.motivo_desactivar',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');
        $this->db->join('sucursal s','s.id=u.sucursal','left');
        $where = array('p.estatus'=>1);
        $this->db->where($where);
        if($params['desactivar']!=2){
            $this->db->where(array('u.acceso'=>$params['desactivar']));
        }
        /*if($params['sucursal']!=0){
            $this->db->where(array('u.sucursal'=>$params['sucursal']));
        }*/
        if($this->session->userdata('perfilid')!=1){ //no es admin
            $this->db->where(array('u.sucursal'=>$params['sucursal']));
        }
        $this->db->where('p.personalId!=1');
        if($params['empleado']!=''){
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.Usuario',$params['empleado']);
            $this->db->or_like('s.name_suc',$params['empleado']);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre as perfil',
            7=>'s.name_suc',
            8=>'u.acceso',
            9=>'u.motivo_desactivar',
        );
        $columnsss = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre',
            7=>'s.name_suc',
            8=>'u.acceso',
            9=>'u.motivo_desactivar',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');
        $this->db->join('sucursal s','s.id=u.sucursal','left');
        $where = array('p.estatus'=>1);
        $this->db->where($where);
        if($params['desactivar']!=2){
            $this->db->where(array('u.acceso'=>$params['desactivar']));
        }
        /*if($params['sucursal']!=0){
            $this->db->where(array('u.sucursal'=>$params['sucursal']));
        }*/
        if($this->session->userdata('perfilid')!=1){ //no es admin
            $this->db->where(array('u.sucursal'=>$params['sucursal']));
        }
        $this->db->where('p.personalId!=1');
        if($params['empleado']!=''){
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.Usuario',$params['empleado']);
            $this->db->or_like('s.name_suc',$params['empleado']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_listado($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.clave',
            2=>'s.name_suc',
            3=>'s.tel',
            4=>'s.domicilio',
            //5=>'s.direccion',
            5=>'s.orden',
            6=>'s.id_alias'
        );  

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sucursal s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_listado($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.clave',
            2=>'s.name_suc',
            3=>'s.tel',
            4=>'s.domicilio',
            //5=>'s.direccion'
            5=>'s.orden',
            6=>'s.id_alias'
        ); 

        $this->db->select('COUNT(1) as total');
        $this->db->from('sucursal s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    

}