<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloCompras extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function obtener_folio_v($sucursal){
        $strq = "SELECT max(v.folio_con)+1 as folio FROM compra_erp as v WHERE v.sucursal=$sucursal";
        $query = $this->db->query($strq);
        $folio=1;
        foreach ($query->result() as $item) {
            if($item->folio>0){
                $folio=$item->folio;
            }
        }
        return $folio;
    }

    function infocomprapre($id){
        $strq = "SELECT c.*,p.nombre,p.codigo,p.contacto
        FROM compra_erp as c 
        LEFT JOIN proveedores AS p ON p.id = c.idproveedor
        WHERE c.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }


    function getcompras($params){
        $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'c.cancelado',
            11=>'c.precompra',
            12=>'IFNULL(c.fecha_ingreso,"") as fecha_ingreso'
        );

        $columnx = array( 
            0=>'c.id',
            1=>'p.nombre as personal',
            2=>'c.reg',
            3=>'pr.nombre as proveedor', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'IFNULL(re.id,0) as id_re',
            11=>'c.cancelado',
            12=>'c.precompra',
            13=>'c.fecha_ingreso'
        );

        $select="";
        foreach ($columnx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        $this->db->join('bitacora_reverse_entradas re', 're.id_ordencomp=c.id','left');
        //$this->db->where(array('c.precompra'=>0));
        $this->db->group_by("c.id");

        if($params['estatus']==0){//todas las compras
            $this->db->where('c.precompra',0);
        }     
        if($params['estatus']==1){
            $where = array('c.estatus'=>0);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }else if($params['estatus']==2){    //ingresada
            $where = array('c.estatus'=>1);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }
        else if($params['estatus']==3){ //no recibida
            $where = array('c.estatus'=>3);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }else if($params['estatus']==4){  //pre compras 
            $where = array('c.estatus'=>0,"precompra"=>1);
            $this->db->where($where);
        }    

        if($params['f1']!='' && $params['f2']!='' && $params['estatus']!=2){
            $where = ' c.reg BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }
        if($params['f1']!='' && $params['f2']!='' && $params['estatus']==2){ //orden ingresada
            $where = ' c.fecha_ingreso BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }

        if(isset($params['code']) && $params['code']!="0"){
            //$this->db->where("c.codigo",$params['code']);
            $this->db->where('concat(c.id,DATE_FORMAT(c.reg, "%Y%m%d%H%i%s")) = '.$params["code"].' ');
        }   

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 

        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);

        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_getcompras($params){
         $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'c.cancelado',
            11=>'c.precompra',
            12=>'c.fecha_ingreso'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        
        if($params['estatus']==0){//todas las compras
            $this->db->where('c.precompra',0);
        }     
        if($params['estatus']==1){
            $where = array('c.estatus'=>0);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }else if($params['estatus']==2){    //ingresada
            $where = array('c.estatus'=>1);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }
        else if($params['estatus']==3){ //no recibida
            $where = array('c.estatus'=>3);
            $this->db->where($where);
            $this->db->where('c.precompra',0);
        }else if($params['estatus']==4){  //pre compras 
            $where = array('c.estatus'=>0,"precompra"=>1);
            $this->db->where($where);
        } 

        if($params['f1']!='' && $params['f2']!='' && $params['estatus']!=2){
            $where = ' c.reg BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }
        if($params['f1']!='' && $params['f2']!='' && $params['estatus']==2){ //orden ingresada
            $where = ' c.fecha_ingreso BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }

        if(isset($params['code']) && $params['code']!="0"){
            //$this->db->where("c.codigo",$params['code']);
            $this->db->where('concat(c.id,DATE_FORMAT(c.reg, "%Y%m%d%H%i%s")) = '.$params["code"].' ');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    public function getDataBitacora($id){
        $this->db->select('bit.*, u.Usuario');
        $this->db->from('bitacora_reverse_entradas bit');
        $this->db->join('usuarios u','u.UsuarioID=bit.id_usuario');
        $this->db->where('bit.id_ordencomp',$id);
        $this->db->order_by('bit.id',"DESC");
        $query=$this->db->get();
        return $query->result();
    }

    public function compras_detalles($idcompra,$oc=0,$band=0){
        $group=""; $select=""; $join=""; $where="";
        $select=", IFNULL(pss.id,0) as id_ps, pss.serie, IFNULL(psl.id,0) as id_pl, IFNULL(psl.cantidad,'') as cant_ls, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad, IFNULL(psl.cod_barras,'') as cod_barras, 
            (select count(pss2.id) as cant_sr from productos_sucursales_serie AS pss2 
            where pss2.idcompras=cd.idcompra and pss2.productoid=cd.idproducto and pss2.activo=1 and cd.tipo=0 and pro.tipo=1) as cant_sr ";
        $join="LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and pro.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and pro.tipo=2";
        //$group="group by serie, lote";
        if($oc==1){
            //$select=", pss.serie, IFNULL(psl.cantidad,'') as cant_ls, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad";
            $group="group by cd.idproducto";
            /*$join="LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and pro.tipo=1
                    LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and pro.tipo=2";*/
        }
        if($band==1){
            $where=" and cd.distribusion=0";
        }
        //$order=" pro.idProducto";
        $order=" pro.tipo asc";
        $strq = "SELECT pro.idProducto, pro.id,
            cd.cantidad,
            pro.nombre,
            cd.precio_unitario,
            cd.idproducto,
            IFNULL(pro.tipo,0) AS tipo_s,
            pro.incluye_iva_comp,
            pro.iva_comp,
            pro.porc_isr,
            cd.tipo, r.codigo,r.capacidad,
            cd.id
            $select                        
        FROM compra_erp_detalle as cd 
        LEFT JOIN productos as pro on pro.id=cd.idproducto and cd.tipo=0
        LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
        $join
        WHERE cd.activo=1 ".$where." AND cd.idcompra=$idcompra
        $group
        order by pro.id asc, $order ,pss.id asc, psl.id asc";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function compra_det_ls($idcompra){
        $strq = "SELECT pro.idProducto, pro.id,cd.cantidad, pro.nombre,pro.tipo as tipo_prod, cd.precio_unitario, cd.idproducto, IFNULL(pro.tipo,0) AS tipo_s, pro.incluye_iva_comp, pro.iva_comp, cd.tipo, cd.id as id_cd,
            pss.serie, IFNULL(psl.cantidad,'') as cant_ls, IFNULL(psl.lote,'') as lote, IFNULL(psl.caducidad,'') as caducidad, IFNULL(psl.cod_barras,'') as cod_barras ,(SELECT COUNT(*) AS total_series FROM productos_sucursales_serie as pss
        WHERE pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1) AS total_series,
        (SELECT COUNT(*) AS total_lote
        FROM productos_sucursales_lote as psl
        WHERE psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1) AS total_lote
        FROM compra_erp_detalle as cd 
        LEFT JOIN productos as pro on pro.id=cd.idproducto and cd.tipo=0
        LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and pro.tipo=1
        LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and pro.tipo=2
        WHERE cd.activo=1 AND cd.idcompra=$idcompra and cd.tipo=0
        and pro.tipo!=0
        group by cd.id
        order by pro.tipo";
        $query = $this->db->query($strq);
        return $query;
    }

    public function lotesCompraProd($idcompra,$idprod){
        $strq = "SELECT psl.id, psl.cantidad, psl.lote, psl.caducidad, psl.cod_barras                        
        FROM productos_sucursales_lote as psl
        WHERE psl.idcompra=".$idcompra." and psl.productoid=".$idprod." and psl.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function seriesCompraProd($idcompra,$idprod){
        $strq = "SELECT pss.id, pss.serie                      
        FROM productos_sucursales_serie as pss
        WHERE pss.idcompras=".$idcompra." and pss.productoid=".$idprod." and pss.activo=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_productos($id){
        $strq = "SELECT cd.id as id_dll, p.idProducto, p.codigoBarras, p.nombre, p.tipo as tipo_prod,p.incluye_iva_comp,p.iva_comp, p.porc_isr,
            cd.cantidad,cd.precio_unitario,cd.idproducto,cd.tipo,r.id as id_tanque, r.codigo,r.capacidad,
            IFNULL(GROUP_CONCAT('Lote: ',' ',psl.lote,'(',psl.cantidad,')' SEPARATOR '<br>'),'') as lote,
            IFNULL(GROUP_CONCAT('Serie: ',' ',pss.serie SEPARATOR '<br>'),'') as serie
            FROM compra_erp_detalle AS cd 
            LEFT JOIN productos AS p ON p.id=cd.idproducto and cd.tipo=0
            /*LEFT JOIN productos_sucursales AS ps ON ps.productoid=cd.idproducto*/
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2
            LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
            WHERE cd.idcompra=$id AND cd.activo=1
            group by cd.id";
        $query = $this->db->query($strq);
        return $query;
    }

    /*public function get_productos($id){
        $strq = "SELECT p.idProducto, p.codigoBarras, p.nombre, p.tipo as tipo_prod,
                 IFNULL(ps.incluye_iva,0) as incluye_iva, IFNULL(ps.iva,0) as iva,
                cd.cantidad,cd.precio_unitario,cd.idproducto,cd.tipo,r.id as id_tanque, r.codigo,r.capacidad
                FROM compra_erp_detalle AS cd 
                LEFT JOIN productos AS p ON p.id=cd.idproducto and cd.tipo=0
                LEFT JOIN productos_sucursales AS ps ON ps.productoid=cd.idproducto
                LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
                WHERE cd.idcompra=$id AND cd.activo=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }*/

    function get_compra($id){
        $strq ="SELECT c.*, pr.codigo, pr.nombre,pr.direccion,pr.cp,pr.rfc
            FROM compra_erp AS c 
            LEFT JOIN proveedores AS pr ON pr.id=c.idproveedor 
            WHERE c.activo=1 AND c.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_compra_erp_detalle($id){
        $strq = "SELECT p.id,p.idProducto,p.nombre,cd.precio_unitario,cd.cantidad,cd.tipo, r.id as id_tanque,p.incluye_iva_comp,p.iva_comp, 
            r.codigo,IFNULL(r.capacidad,0) as capacidad
            from compra_erp_detalle AS cd 
            LEFT JOIN productos AS p ON p.id=cd.idproducto
            LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1
            where cd.id=$id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function sumar_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock+$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
    }

    function resta_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock-$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
    }
    
    function update_distribusion($idcompra,$idproducto){
        $strq = "UPDATE compra_erp_detalle set distribusion=1 where idcompra=$idcompra AND idproducto=$idproducto";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    public function getProds_exist_baja($id,$idsuc=0,$all=0){
        $joinsuc="";
        if($idsuc!=0){
            $joinsuc='
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id AND psl.sucursalid='.$idsuc.' and p.tipo=2),0) AS stock_lote,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 and pss.disponible=1 AND pss.productoid=p.id AND pss.sucursalid='.$idsuc.' and p.tipo=1),0) as stock_serie,
            IFNULL((SELECT ps1.stock FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id AND ps1.sucursalid='.$idsuc.'), 0) AS stok_ps,
            IFNULL((SELECT ps1.id FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id AND ps1.sucursalid='.$idsuc.'), 0) AS idpssub';
        }else{
            $joinsuc='
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id AND psl.sucursalid=8 and p.tipo=2),0) AS stock_lote,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 and pss.disponible=1 AND pss.productoid=p.id and pss.sucursalid=8 AND p.tipo=1),0) as stock_serie,
            IFNULL((SELECT sum(ps1.stock) as stock FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id AND ps1.sucursalid=8), 0) AS stok_ps,
            IFNULL((SELECT ps1.id as idpssub FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id AND ps1.sucursalid=8), 0) AS idpssub';
        }
        $this->db->select('p.id, p.idProducto, p.nombre, p.costo_compra, p.incluye_iva_comp, p.iva_comp, p.tipo, ps.sucursalid, ps.incluye_iva, ps.stock, ps.precio, ps.incluye_iva, ps.iva, ps.stockmin, sum(ps.stockmin) as tot_stockmin,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id and p.tipo=2),0) AS stock_loteall,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 and pss.disponible=1 AND pss.productoid=p.id and p.tipo=1),0) as stock_serieall,
            IFNULL((SELECT sum(ps1.stock) as stock FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id), 0) AS stok_psall,
            '.$joinsuc.'
        ');
        $this->db->from('productos_proveedores pp');
        $this->db->join('productos p','p.id=pp.idproducto and p.estatus=1'); 
        
        if($idsuc>0){
            $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.activo=1','left');
            $this->db->join('sucursal s','s.id=ps.sucursalid and s.activo=1 and s.id='.$idsuc.' ','left');
        }else{
            $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.activo=1','left');
            $this->db->join('sucursal s','s.id=ps.sucursalid and s.activo=1','left');
        }   
        
        if($id>0){
            $this->db->where('(p.idproveedor='.$id.' or pp.idproveedor='.$id.')');
        }
        if($idsuc>0){
            //$this->db->where("ps.sucursalid",$idsuc);
        }
        //$this->db->where("p.estatus",1);
        if($all==0){
            $this->db->where("(ps.stock=0 and ps.stockmin>0 or ps.stock<=ps.stockmin and ps.stockmin>0) or (IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 and pss.disponible=1 AND pss.productoid=p.id and p.tipo=1),0)=0 and p.tipo=1) or (IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id and p.tipo=2),0)=0 and p.tipo=2)");
        }
        $this->db->group_by('p.id');
        //$this->db->order_by('s.orden',"ASC");
        $this->db->order_by('p.nombre',"ASC");
        $query=$this->db->get();
        return $query->result();
    }

    function getProductoCodigoComp($cod){
        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, p.stock, 0 as id_prod_suc, 0 as id_prod_suc_serie, "" as lote, "" as caducidad, "" as serie, ps.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1","left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("codigoBarras",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query1 = $this->db->get_compiled_select();  

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, psl.cantidad as stock, psl.id as id_prod_suc, 0 as id_prod_suc_serie, psl.lote, psl.caducidad, "" as serie, psl.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_lote psl","psl.productoid=p.id and psl.activo=1","left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("psl.cod_barras",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query2 = $this->db->get_compiled_select();  

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, 1 as stock, 0 as id_prod_suc, pss.id as id_prod_suc_serie, "" as lote, "" as caducidad, pss.serie, pss.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_serie pss","pss.productoid=p.id and pss.activo=1","left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("pss.serie",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $query3 = $this->db->get_compiled_select();

        $this->db->select('p.id, p.idProducto, p.tipo, p.nombre, ps.stock, 0 as id_prod_suc, 0 as id_prod_suc_serie, "" as lote, "" as caducidad, "" as serie, ps.sucursalid, IFNULL(fu.nombre,"") as nom_unidad');
        $this->db->from("productos p");
        $this->db->join("productos_proveedores pp","pp.idproducto=p.id and pp.activo=1","left");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1","left");
        $this->db->join("f_unidades fu","fu.Clave=p.unidad_sat","left");
        $this->db->where("p.estatus",1);
        $this->db->like("pp.codigo",$cod);
        $this->db->or_like("p.idProducto",$cod);
        $this->db->group_by("p.id");
        $query4 = $this->db->get_compiled_select();

        $queryunions=$query1." UNION ".$query2." UNION ".$query3." UNION ".$query4;
        return $this->db->query($queryunions)->result();
    }


    function getOC_idCode($code){
        $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre as proveedor', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        $this->db->join('bitacora_reverse_entradas re', 're.id_ordencomp=c.id','left');
        $this->db->where(array('c.precompra'=>0));
        $this->db->group_by("c.id");

        if($code!="0"){
            $this->db->where('concat(c.id,DATE_FORMAT(c.reg, "%Y%m%d%H%i%s")) = '.$code.' ');
        }
        $query=$this->db->get();
        return $query;
    }

    function getTotalCompras($idp,$fechai,$fechaf){
        $this->db->query("SET lc_time_names = 'es_ES'");
        $this->db->select('SUM(c.subtotal) as subtotal, SUM(c.iva) as iva, SUM(c.isr) as isr, SUM(c.total) as total, p.nombre as provee, CONCAT(MONTHNAME(c.reg), " ", YEAR(c.reg)) as mes_anio');
        $this->db->from("compra_erp c");
        $this->db->join('proveedores p', 'p.id=c.idproveedor');
        $this->db->where("c.reg BETWEEN '$fechai 00:00:00' AND '$fechaf 23:59:59'");
        $this->db->where("c.idproveedor",$idp);
        $this->db->where('c.activo',"1");
        $this->db->where('c.cancelado',"0");
        //pendiente o ingresada
        $this->db->where("(c.estatus = 0 or c.estatus=1)");
        $this->db->where('c.precompra',0);  
        $this->db->group_by('DATE_FORMAT(c.reg,"%Y-%m")');  
        $query=$this->db->get();
        return $query->result();
    }

}