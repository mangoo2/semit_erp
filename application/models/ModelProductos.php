<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function get_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'categoriaId',
            1=>'categoria'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_listado($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.referencia',
            2=>'c.categoria',
            3=>'p.stock',
            4=>'p.stockmin',
            5=>'p.stockmax',
            6=>'p.precio_sin_iva',
            7=>'p.precio_con_iva',
            8=>'p.mostrar_pagina',
            9=>'p.nombre',
            10=>'p.mas_vendidos',
            11=>'p.codigoBarras',
            12=>'p.idProducto',
            13=>'p.tipo',
            14=>'ps.precio',
            15=>'ps.incluye_iva',
            16=>'ps.iva',
            17=>'ps.precio_final'
        ); 

        /*$columnsx = array( 
            0=>'p.id',
            1=>'p.referencia',
            2=>'c.categoria',
            3=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2) AS stock2',
            4=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=3) AS stock3',
            5=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=4) AS stock4',
            6=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=5) AS stock5',
            7=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=6) AS stock6',
            8=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=7) AS stock7',
            9=>'(SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=8) AS stock8',
            10=>'p.mostrar_pagina',
            11=>'p.nombre',
            12=>'p.mas_vendidos',
            13=>'p.codigoBarras',
            14=>'(SELECT precio FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2) AS precio',
            15=>'p.idProducto',
            16=>'p.tipo',
            17=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=2) AS serie2',
            18=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=3) AS serie3',
            19=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=4) AS serie4',
            20=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=5) AS serie5',
            21=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=6) AS serie6',
            22=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=7) AS serie7',
            23=>'(SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8) AS serie8',
            24=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=2),0) AS lote2',
            25=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=3),0) AS lote3',
            26=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=4),0) AS lote4',
            27=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=5),0) AS lote5',
            28=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=6),0) AS lote6',
            29=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=7),0) AS lote7',
            30=>'IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8),0) AS lote8',
        );*/
        $columnsx = array( 
            0=>'p.id',
            1=>'p.referencia',
            2=>'c.categoria',
            3=>'p.mostrar_pagina',
            4=>'p.nombre',
            5=>'p.mas_vendidos',
            6=>'p.codigoBarras',
            //7=>'(SELECT precio FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2) AS precio',
            7=>'ps.precio',
            8=>'p.idProducto',
            9=>'p.tipo',
            10=>'ps.precio_final'
        );  

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos p');
        $this->db->join('categoria c','c.categoriaId=p.categoria','left');
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.sucursalid=2','left');
        if($params['idproductos']!=0){
            $where = array('p.estatus'=>1);
        }else{
            $where = array('p.estatus'=>0);
        }
        $this->db->where($where);
        if($params['idcategoria']!=0){
            $where = array('p.categoria'=>$params['idcategoria']);
            $this->db->where($where);
        }
        /*if($params['idproductos']!=0){
            $where = array('p.mostrar_pagina'=>$params['idproductos']);
            $this->db->where($where);
        }*/
        if($params['idmasvendidos']!=0){
            $where = array('p.mas_vendidos'=>$params['idmasvendidos']);
            $this->db->where($where);
        }
        //////////////////
        if($params["perfilid"]==7 || $params["perfilid"]==9){
            $this->db->where("(p.tipo=3 or p.tipo=4)");
        }

        $this->db->order_by("p.idProducto");
        $this->db->group_by("p.id");
        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_listado($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.referencia',
            2=>'c.categoria',
            3=>'p.stock',
            4=>'p.stockmin',
            5=>'p.stockmax',
            6=>'p.precio_sin_iva',
            7=>'p.precio_con_iva',
            8=>'p.mostrar_pagina',
            9=>'p.nombre',
            10=>'p.codigoBarras',
            11=>'p.idProducto',
            12=>'ps.precio',
            13=>'ps.precio_final'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('productos p');
        $this->db->join('categoria c','c.categoriaId=p.categoria','left');
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.sucursalid=2','left');
        if($params['idproductos']!=0){
            $where = array('p.estatus'=>1);
        }else{
            $where = array('p.estatus'=>0);
        }
        $this->db->where($where);
        //////////////////
        if($params['idcategoria']!=0){
            $where = array('p.categoria'=>$params['idcategoria']);
            $this->db->where($where);
        }
        /*if($params['idproductos']!=0){
            $where = array('p.mostrar_pagina'=>$params['idproductos']);
            $this->db->where($where);
        }*/
        if($params['idmasvendidos']!=0){
            $where = array('p.mas_vendidos'=>$params['idmasvendidos']);
            $this->db->where($where);
        }
        if($params["perfilid"]==7 || $params["perfilid"]==9){
            $this->db->where("(p.tipo=3 or p.tipo=4)");
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    function obtenerdatosproducto($codigo){
        $strq = "SELECT * from productos where codigoBarras='$codigo' and (activo=1 or activo='Y')"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerdatosproducto_venta($codigo){
        $strq = "SELECT * from productos where referencia_tipo!=1 AND codigoBarras='$codigo' and (activo=1 or activo='Y')"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerdatosproductoid($id,$sucursal,$tipo,$idps){
        $join=""; $sel="";
        if($tipo==0){
            $sel=" ,IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$sucursal and ht.rechazado!=1),0) as traslado_stock_cant, 0 as lote_stock, 0 as idprod_lote";
        }
        if($tipo==1){ //series
            $sel=" , IFNULL(pss.activo,0) as cant_serie, IFNULL(pss.id,'0') as idprod_serie, IFNULL(concat('SERIE: ',pss.serie),'') as serie, 0 as traslado_stock_cant, 0 as lote_stock, 0 as idprod_lote";
            //$join="join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1";
            $join="join productos_sucursales_serie pss on pss.id=$idps";
        }else if($tipo==2){ //lote y cad.
            $sel=" , (IFNULL(psl.cantidad,0) - IFNULL(tlh.cantidad,0)) as cant_lote, IFNULL(psl.id,'0') as idprod_lote, IFNULL(concat('LOTE: ',psl.lote,' ',psl.caducidad,' (',psl.cantidad,')'),'') as lote, 0 as traslado_stock_cant, psl.cantidad as lote_stock";
            //$join="join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1";  
            $join="join productos_sucursales_lote psl on psl.id=$idps
                left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1";  
        }
        $strq = "SELECT p.id,p.idProducto,p.nombre,p.concentrador, ps.stock,ps.precio,IFNULL(ps.incluye_iva,0) as incluye_iva, IFNULL(ps.iva,0) as iva,p.costo_compra $sel
            from productos AS p
            LEFT JOIN productos_sucursales AS ps ON ps.productoid=p.id
            $join
            WHERE p.id=$id AND ps.sucursalid=$sucursal
            group by p.id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerDatosRecargaVenta($id,$sucursal){
        $strq = "SELECT r.*, r2.stock as stockpadre
                from recargas r
                join recargas as r2 on r2.tipo=0 and r2.estatus=1 and r2.sucursal=$sucursal
                WHERE r.id=$id 
                GROUP BY r.id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerDatosRecarga($id,$sucursal){
        $strq = "SELECT r.*, r2.stock
                from recargas r
                join recargas as r2 on r2.tipo=0 and r2.estatus=1
                WHERE r.id=$id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function getTanquePadreSuc($id){
        $strq = "SELECT r.*, s.name_suc,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=r.id and ht.activo=1 and ht.idsucursal_sale=r.sucursal and ht.tipo=1 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant
        from recargas r
        join sucursal s on s.id=r.sucursal
        WHERE r.id=$id
        order by s.orden asc"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    /*function getTanquePadreSucursales($id){
        $strq = "SELECT r.*, s.name_suc
                from recargas r
                join sucursal s on s.id=r.sucursal
                WHERE r.tipo=0 and r.id!=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }*/

    function getTanquePadreSucursales($id){
        $strq = "SELECT r.*, s.name_suc,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
            join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
            where ht.idproducto=r.id and ht.activo=1 and ht.idsucursal_sale=r.sucursal and ht.tipo=1 and ht.status=1 and ht.rechazado!=1),0) as traslado_stock_cant
        from recargas r
        join sucursal s on s.id=r.sucursal
        WHERE r.tipo=0 and r.estatus=1
        order by orden asc"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function update_productos_sucursales($productoid,$sucursalid,$stock,$stockmin,$stockmax){
        $strq = "UPDATE productos_sucursales SET stock = $stock, stockmin=$stockmin, stockmax=$stockmax where productoid=$productoid and sucursalid=$sucursalid";
        $query = $this->db->query($strq);
    }

    function get_producto($id){
        $strq = "SELECT p.id,p.idProducto,p.nombre,c.categoria,(SELECT precio FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2) AS precio
                from productos AS p
                LEFT JOIN categoria AS c ON c.categoriaId=p.categoria
                WHERE p.id=$id"; 
        $query = $this->db->query($strq);
        return $query;
    }

    public function get_suc_series($id,$id_suc){
        $sql = "SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod=$id and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid=$id and sucursalid=$id_suc and disponible=1
            group by id";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_suc_lotes($id,$id_suc){
        $sql = "SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
        FROM productos_sucursales_lote as psl
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=$id and psl.sucursalid=$id_suc
        group by psl.id";
        $query = $this->db->query($sql);
        return $query;
    }

    function getStockSucursal($id){
        $this->db->select('IFNULL(ps.stock,0) as stock, s.name_suc, s.id as id_suc, s.id_alias,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto='.$id.' and ht.activo=1 and ht.idsucursal_sale=s.id and ht.tipo=0 and ht.status=0 and ht.rechazado!=1), 0) as traslado_stock_cant');
        $this->db->from("sucursal s");
        //$this->db->join("productos p","p.id=".$id);
        $this->db->join("productos_sucursales ps","ps.sucursalid=s.id and ps.activo=1 and ps.productoid=$id","left");
        //$this->db->where("ps.productoid",$id);
        $this->db->where("s.activo",1);
        //$this->db->where("ps.sucursalid","s.id");
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getStockSucursal2($id,$id_suc){
        $this->db->select('s.name_suc, s.id as id_suc, s.id_alias, p.id,p.nombre,0 as tipo, IFNULL(ps.stock,0) as stock,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto='.$id.' and ht.activo=1 and ht.idsucursal_sale=s.id and ht.tipo=0 and ht.status=0 and ht.rechazado!=1), 0) as traslado_stock_cant
            /*, GROUP_CONCAT(concat("<td>",IFNULL(ps.stock,0),"</td>") SEPARATOR "") stock*/');
        $this->db->from("sucursal s");
        $this->db->join("productos p","p.id=".$id."");
        $this->db->join("productos_sucursales ps","ps.sucursalid=s.id and and ps.activo=1","left");
        $this->db->where("productoid",$id);
        $this->db->where("s.activo",1);
        $this->db->where("sucursalid",$id_suc);
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getCategoriaLastProd($id){
        $this->db->select('p.idProducto');
        $this->db->from("productos p");
        $this->db->where("categoria",$id);
        $this->db->where("p.estatus",1);
        $this->db->order_by("id","desc");
        $this->db->limit(1);
        $query=$this->db->get(); 
        return $query;
    }

    function getSerieSucursal($id){
        $this->db->select('ps.idcompras, 
        -- Contamos las series que no tienen compra (idcompras = 0)
        IFNULL(SUM(CASE WHEN ps.idcompras = 0 THEN 1 ELSE 0 END), 0) AS serie, 
        -- Contamos las series que tienen una compra y estatus = 1
        IFNULL(SUM(CASE WHEN ps.idcompras != 0 AND ce.estatus = 1 THEN 1 ELSE 0 END), 0) AS serie_compra, 
        -- Contamos el stock total de productos
        IFNULL(COUNT(ps.id), 0) AS stock, 
        s.name_suc, 
        s.id AS id_suc, 
        s.id_alias, 
        IFNULL(ce.estatus, 0) AS estatus_compra');
        $this->db->from("sucursal s");
        //$this->db->join("productos p","p.id=".$id);
        $this->db->join("productos_sucursales_serie ps","ps.sucursalid=s.id and ps.activo=1 and disponible=1 and vendido=0","left");
        $this->db->join("compra_erp ce","ce.id=ps.idcompras","left");
        $this->db->where("productoid",$id);
        $this->db->where("s.activo",1);
        //$this->db->where("sucursalid",$id_suc);
        $this->db->group_by("s.id");
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getSerieSucursal2($id,$id_suc){
        $this->db->select('IFNULL(count(ps.id),0) as stock, s.name_suc, s.id as id_suc, s.id_alias, p.id,p.nombre,0 as tipo');
        $this->db->from("sucursal s");
        $this->db->join("productos p","p.id=".$id);
        $this->db->join("productos_sucursales_serie ps","ps.sucursalid=s.id and ps.activo=1 and ps.disponible=1 and ps.vendido=0 and ps.sucursalid=".$id_suc."","left");
        $this->db->where("productoid",$id);
        $this->db->where("s.activo",1);
        $this->db->where("sucursalid",$id_suc);
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getLoteSucursal($id){
        $this->db->select('ps.idcompra, 
        -- Contamos las cantidades para lotes sin compra (idcompra = 0)
        IFNULL(SUM(CASE WHEN ps.idcompra = 0 THEN ps.cantidad ELSE 0 END), 0) AS lote, 
        -- Contamos las cantidades para lotes con compra activa (idcompra != 0 y estatus = 1)
        IFNULL(SUM(CASE WHEN ps.idcompra != 0 AND ce.estatus = 1 THEN ps.cantidad ELSE 0 END), 0) AS lote_compra, 
        -- Contamos todas las cantidades disponibles
        IFNULL(SUM(ps.cantidad), 0) AS stock, 
        s.name_suc, 
        s.id AS id_suc, 
        s.id_alias, 
        IFNULL(ce.estatus, 0) AS estatus_compra');
            $this->db->from("sucursal s");
        //$this->db->join("productos p","p.id=".$id);
        $this->db->join("productos_sucursales_lote ps","ps.sucursalid=s.id and ps.activo=1","left");
        $this->db->join("compra_erp ce","ce.id=ps.idcompra","left");
        $this->db->where("productoid",$id);
        $this->db->where("s.activo",1);
        //$this->db->where("sucursalid",$id_suc);
        $this->db->group_by("s.id");
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getLoteSucursal2($id,$id_suc){
        $this->db->select('IFNULL(sum(ps.cantidad),0) as stock, s.name_suc, s.id as id_suc, s.id_alias, p.id,p.nombre,0 as tipo');
        $this->db->from("sucursal s");
        $this->db->join("productos p","p.id=".$id);
        $this->db->join("productos_sucursales_lote ps","ps.sucursalid=s.id and ps.activo=1","left");
        $this->db->where("productoid",$id);
        $this->db->where("s.activo",1);
        $this->db->where("sucursalid",$id_suc);
        $this->db->order_by("orden","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    public function get_proveedores($id){
        $sql = "SELECT p.id,p.idproveedor,pr.nombre,p.codigo
            FROM productos_proveedores as p
            left join proveedores as pr on pr.id=p.idproveedor
            WHERE p.activo=1 and p.idproducto=$id";
        $query = $this->db->query($sql);
        return $query;
    }

    function listProdsExcel(){ //3 td con detalla aparte, por sucursal
        $this->db->select("p.id,p.idProducto, p.nombre, p.codigoBarras, p.tipo, p.costo_compra, p.unidad_sat, p.servicioId_sat, p.incluye_iva_comp, p.iva_comp, p.isr, p.porc_isr, pp.codigo as cod_prov, pro.nombre as proveedor, 
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', ps.stock, '</td>', '<td>', ps.stockmin, '</td>', '<td>', ps.stockmax, '</td>') ORDER BY s.orden ASC SEPARATOR ''), 
                CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>') 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), 
                CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', psl.cantidad, '</td>', '<td>0</td>', '<td>0</td>') 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td></td>', '<td></td>', '<td></td>')
            )
            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
        END AS stock,
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8,
        COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva,
        CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, 
        CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva,
        CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva,
        CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', psl.lote, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle,
        c.categoria");
        $this->db->from("productos p");
        $this->db->join('categoria c','c.categoriaId=p.categoria','left');
        $this->db->join('productos_proveedores pp','pp.idproducto=p.id','left');
        $this->db->join('proveedores pro','pro.id=pp.idproveedor','left');

        //$this->db->join('sucursal s','s.activo=1');
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.activo=1 and ps.sucursalid=8');
        $this->db->join('sucursal s','s.id=ps.sucursalid and s.activo=1');

        $this->db->join('productos_sucursales_serie pss','pss.productoid = p.id AND pss.sucursalid=ps.sucursalid AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND p.tipo = 1','left');
        $this->db->join('productos_sucursales_lote psl','psl.productoid = p.id AND psl.sucursalid=ps.sucursalid AND psl.activo = 1 AND p.tipo = 2','left');
        
        $this->db->where("(p.activo = 1 OR p.activo = 'Y')");
        $this->db->where("s.activo","1");
        $this->db->group_by("p.id");
        $this->db->order_by("ps.sucursalid","asc");
        $this->db->order_by("s.orden","asc");

        $this->db->order_by("p.idProducto","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function listProdsExcel2(){ //3 td y el detalle aparte, con todas las sucursales
        $this->db->select("p.id,p.idProducto, p.nombre, p.codigoBarras, p.tipo, p.costo_compra, p.unidad_sat, p.servicioId_sat, p.incluye_iva_comp, p.iva_comp, p.isr, p.porc_isr, pp.codigo as cod_prov, pro.nombre as proveedor, 
            /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock,
            GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin,
            GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax,*/
        COALESCE(CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', ps.stock, '</td>', '<td>', ps.stockmin, '</td>', '<td>', ps.stockmax, '</td>') ORDER BY s.orden ASC SEPARATOR ''), 
                CONCAT('<td>S/D</td>', '<td>S/D</td>', '<td>S/D</td>')
            )
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td></td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td> sd1</td>', '<td>sd1 </td>', '<td>sd1 </td>')
            )
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', 
                        '<td>', 
                        (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), 
                        '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), 
                CONCAT('<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', psl.cantidad, '</td>', '<td>0</td>', '<td>0</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td>sd 2</td>', '<td>sd 2</td>', '<td>sd 2</td>')
            )
            ELSE CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')
        END, CONCAT('<td>sdAll </td>', '<td>sdAll </td>', '<td>sdAll </td>')) AS stock,
        /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', ps.stock, '</td>', 
                   '<td>', ps.stockmin, '</td>', 
                   '<td>', ps.stockmax, '</td>', 
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), 
            CONCAT('<td></td>', '<td></td>', '<td></td>')
        ) AS stock,*/
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8,
        COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva,
        CASE
            WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
            WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
            WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
            ELSE ''
        END AS iva_txt, 
        CASE
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
            WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
            WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
            ELSE ''
        END AS iva_comp_txt, 
        CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, 
        CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva,
        CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva,
        CASE 
            WHEN p.tipo = 0 THEN COALESCE('','') 
            WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>'),'')
            WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT 'Lote: ', psl.lote, ' / ', psl.caducidad SEPARATOR '<br>'),'')
            ELSE '' 
        END AS detalle,
        c.categoria");
        $this->db->from("productos p");
        $this->db->join('categoria c','c.categoriaId=p.categoria','left');
        $this->db->join('productos_proveedores pp','pp.idproducto=p.id','left');
        $this->db->join('proveedores pro','pro.id=pp.idproveedor','left');

        $this->db->join('sucursal s','s.activo=1');
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.activo=1');
        //$this->db->join('sucursal s','s.id=ps.sucursalid and s.activo=1');

        $this->db->join('productos_sucursales_serie pss','pss.productoid = p.id AND pss.sucursalid=ps.sucursalid AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND p.tipo = 1','left');
        $this->db->join('productos_sucursales_lote psl','psl.productoid = p.id AND psl.sucursalid=ps.sucursalid AND psl.activo = 1 AND p.tipo = 2','left');
        
        $this->db->where("(p.activo = 1 OR p.activo = 'Y')");
        $this->db->where("s.activo","1");
        $this->db->group_by("p.id");
        $this->db->order_by("ps.sucursalid","asc");
        $this->db->order_by("s.orden","asc");

        $this->db->order_by("p.idProducto","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function listProdsExcel3(){
        $this->db->select("p.id,p.idProducto, p.nombre, p.codigoBarras, p.tipo, p.costo_compra, p.unidad_sat, p.servicioId_sat, p.incluye_iva_comp, p.iva_comp, p.isr, p.porc_isr, pp.codigo as cod_prov, pro.nombre as proveedor, 
            /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock,
            GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin,
            GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax,*/
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT CONCAT('<td>', ps.stock, '</td>', '<td>', ps.stockmin, '</td>', '<td>', ps.stockmax, '</td>', 
                    '<td>','</td>') ORDER BY s.orden ASC SEPARATOR ''), 
                CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', 
                        '<td>', IFNULL(concat('Serie: 'pss.serie),''), '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'
                ), 
                CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            -- tarda mucho la consulta así --
            /*WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', 
                        '<td>', 
                        (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                         FROM productos_sucursales_serie pss
                         WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                         AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), 
                        '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )*/
            WHEN p.tipo = 1 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td> </td>', '<td></td>', '<td></td>', 
                        '<td>','Serie: ', pss.serie,'</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR '<br>'), 
                CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )
            WHEN p.tipo = 2 THEN COALESCE(
                GROUP_CONCAT(DISTINCT 
                    CONCAT('<td>', psl.cantidad, '</td>', '<td>0</td>', '<td>0</td>', 
                        '<td>', 
                        (SELECT GROUP_CONCAT(DISTINCT CONCAT('Lote: ', psl.lote, ' / ', psl.caducidad) SEPARATOR '<br>')
                         FROM productos_sucursales_lote psl
                         WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid
                         AND psl.activo = 1), 
                        '</td>'
                    ) 
                    ORDER BY s.orden ASC SEPARATOR ''
                ), 
                CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
            )

            ELSE CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        END AS stock,
        /*COALESCE(
           GROUP_CONCAT(
               DISTINCT CONCAT(
                   '<td>', ps.stock, '</td>', 
                   '<td>', ps.stockmin, '</td>', 
                   '<td>', ps.stockmax, '</td>', 
                   '<td>',
                       CASE 
                           WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
                           WHEN p.tipo = 1 THEN (SELECT GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>') FROM productos_sucursales_serie pss WHEREpss.productoid = p.id AND pss.sucursalid = ps.sucursalid)
                           WHEN p.tipo = 2 THEN (SELECT GROUP_CONCAT(DISTINCT 'Lote: ', psl.lote, ' / ', psl.caducidad SEPARATOR '<br>') FROM productos_sucursales_lotepsl WHERE psl.productoid = p.id AND psl.sucursalid = ps.sucursalid)
                           ELSE ''
                        END,
                    '</td>'
                ) 
                ORDER BY s.orden ASC 
                SEPARATOR ''
            ), 
            CONCAT('<td></td>', '<td></td>', '<td></td>', '<td></td>')
        ) AS stock,*/
        (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8,
        COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva,
        CASE
            WHEN p.tipo = 0 THEN 'Stock'
            WHEN p.tipo = 1 THEN 'Serie'
            WHEN p.tipo = 2 THEN 'Lote y caducidad'
            WHEN p.tipo = 3 THEN 'Insumo'
            WHEN p.tipo = 4 THEN 'Refacciones'
            ELSE ''
        END AS tipo_prod, 
        CASE 
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra * 1.16
            WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra / 1.16
            WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
            ELSE 0
        END AS costo_compra_iva,
        CASE
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
            WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio * 1.16
            WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio / 1.16
            WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
            ELSE 0
        END AS precio_con_iva,
        CASE 
            WHEN p.tipo = 0 THEN ''
            WHEN p.tipo = 1 THEN GROUP_CONCAT(DISTINCT 'Serie: ', pss.serie SEPARATOR '<br>')
            WHEN p.tipo = 2 THEN GROUP_CONCAT(DISTINCT 'Lote: ', psl.lote, ' / ', psl.caducidad SEPARATOR '<br>')
            ELSE '' 
        END AS detalle,
        c.categoria");
        $this->db->from("productos p");
        $this->db->join('categoria c','c.categoriaId=p.categoria','left');
        $this->db->join('productos_proveedores pp','pp.idproducto=p.id','left');
        $this->db->join('proveedores pro','pro.id=pp.idproveedor','left');

        $this->db->join('sucursal s','s.activo=1');
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and ps.activo=1 and s.activo=1 and ps.sucursalid=2');
        //$this->db->join('sucursal s','s.id=ps.sucursalid and s.activo=1');

        $this->db->join('productos_sucursales_serie pss','pss.productoid = p.id AND pss.sucursalid=ps.sucursalid AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND p.tipo = 1','left');
        $this->db->join('productos_sucursales_lote psl','psl.productoid = p.id AND psl.sucursalid=ps.sucursalid AND psl.activo = 1 AND p.tipo = 2','left');
        
        $this->db->where("(p.activo = 1 OR p.activo = 'Y')");
        $this->db->where("s.activo","1");
        $this->db->group_by("p.id");
        $this->db->order_by("ps.sucursalid","asc");
        $this->db->order_by("s.orden","asc");

        $this->db->order_by("p.idProducto","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function listProdsExcel2_p(){
        $strq="SELECT DISTINCT dat.*, GROUP_CONCAT(stock,'<td>', detalle ,'</td>' ORDER BY dat.orden ASC SEPARATOR '') as stock FROM(
            SELECT ps.sucursalid,s.orden,p.id, p.idProducto, p.nombre, p.codigoBarras, p.tipo, p.costo_compra, p.unidad_sat, p.servicioId_sat, p.incluye_iva_comp, p.iva_comp, p.isr,p.porc_isr, pp.codigo as cod_prov, pro.nombre as proveedor, /*GROUP_CONCAT(DISTINCT concat('<br>', ps.stock) ORDER BY s.orden asc SEPARATOR '<br>') as stock, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmin) ORDER BY s.orden asc SEPARATOR '<br>') as stockmin, GROUP_CONCAT(DISTINCT concat('<br>', ps.stockmax) ORDER BY s.orden asc SEPARATOR '<br>') as stockmax, */
                COALESCE(CASE 
                    WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT '<td>', COALESCE(ps.stock, ''), '</td>', '<td>', COALESCE(ps.stockmin, ''), '</td>', '<td>',COALESCE(ps.stockmax, ''),'</td>' SEPARATOR ''), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td></td>', '<td></td>', '<td></td>') 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    -- tarda mucho la consulta así --
                    /*WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td> </td>', '<td></td>', '<td></td>', '<td>', (SELECT GROUP_CONCAT(DISTINCT CONCAT('Serie: ', pss.serie) SEPARATOR '<br>')
                                 FROM productos_sucursales_serie pss
                                 WHERE pss.productoid = p.id AND pss.sucursalid = ps.sucursalid
                                 AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido = 0), '</td>'
                            ) 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )*/
                    /*WHEN p.tipo = 1 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td> </td>', '<td></td>', '<td></td>'
                            ) 
                            ORDER BY s.orden ASC SEPARATOR '<br>'), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )*/
                    WHEN p.tipo = 2 THEN COALESCE(
                        GROUP_CONCAT(DISTINCT 
                            CONCAT('<td></td>', '<td>0</td>', '<td>0</td>') 
                            ORDER BY s.orden ASC SEPARATOR ''
                        ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                    )
                    ELSE CONCAT('<td></td>', '<td></td>', '<td></td>')
                END, CONCAT('<td></td>', '<td></td>', '<td></td>')) AS stock, 
                /*COALESCE(
                   GROUP_CONCAT(
                       DISTINCT CONCAT(
                           '<td>', `ps`.`stock`, '</td>', '<td>', `ps`.`stockmin`, '</td>', '<td>', `ps`.`stockmax`, '</td>', ) 
                        ORDER BY s.orden ASC 
                        SEPARATOR ''
                    ), CONCAT('<td></td>', '<td></td>', '<td></td>')
                ) AS stock, */
                 GROUP_CONCAT(DISTINCT '(',ps.id,', ',ps.sucursalid, ', ',s.orden, ', ',ps.stock, ')' ORDER BY s.orden ASC ) as sucx, (SELECT ps8.precio FROM productos_sucursales ps8 WHERE ps8.productoid = p.id AND ps8.sucursalid = 8 AND ps8.activo = 1 LIMIT 1) AS precio_sucursal_8, COALESCE(ps.iva, 0) as iva, COALESCE(ps.incluye_iva, 0) as incluye_iva, 
                 CASE
                    WHEN ps.iva = 0 and ps.incluye_iva=1 THEN '0'
                    WHEN ps.iva != 0 and ps.incluye_iva=1 THEN '16'
                    WHEN ps.iva = 0 and ps.incluye_iva=0 THEN 'Exento'
                    ELSE ''
                END AS iva_txt, 
                CASE
                    WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=1 THEN '0'
                    WHEN p.incluye_iva_comp != 0 and p.incluye_iva_comp=1 THEN '16'
                    WHEN p.incluye_iva_comp = 0 and p.incluye_iva_comp=0 THEN 'Exento'
                    ELSE ''
                END AS iva_comp_txt, 
                CASE
                    WHEN p.tipo = 0 THEN 'Stock'
                    WHEN p.tipo = 1 THEN 'Serie'
                    WHEN p.tipo = 2 THEN 'Lote y caducidad'
                    WHEN p.tipo = 3 THEN 'Insumo'
                    WHEN p.tipo = 4 THEN 'Refacciones'
                    ELSE ''
                END AS tipo_prod, 
                CASE 
                    WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN p.costo_compra
                    WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 1 THEN round(p.costo_compra * 1.16,2)
                    WHEN COALESCE(p.iva_comp, 0) > 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN round(p.costo_compra / 1.16,2)
                    WHEN COALESCE(p.iva_comp, 0) = 0 AND COALESCE(p.incluye_iva_comp, 0) = 0 THEN p.costo_compra
                    ELSE 0
                END AS costo_compra_iva, 
                CASE
                    WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN ps.precio
                    WHEN COALESCE(ps.iva, 0) != 0 AND COALESCE(ps.incluye_iva, 0) = 1 THEN round(ps.precio * 1.16,2)
                    WHEN COALESCE(ps.iva, 0) > 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN round(ps.precio / 1.16,2)
                    WHEN COALESCE(ps.iva, 0) = 0 AND COALESCE(ps.incluye_iva, 0) = 0 THEN ps.precio
                    ELSE 0
                END AS precio_con_iva, 
                CASE 
                    WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE('', '') 
                    WHEN p.tipo = 1 THEN COALESCE(GROUP_CONCAT(DISTINCT ' Serie: ', pss.serie SEPARATOR '<br>'), '')
                    WHEN p.tipo = 2 THEN COALESCE(GROUP_CONCAT(DISTINCT ' Lote: ', psl.lote, ' / ', psl.caducidad,' (stock: ',psl.cantidad,')' SEPARATOR '<br>'), '')
                END AS detalle, c.categoria
            FROM productos p
            LEFT JOIN categoria c ON c.categoriaId=p.categoria
            LEFT JOIN productos_proveedores pp ON pp.idproducto=p.id
            LEFT JOIN proveedores pro ON pro.id=pp.idproveedor
            JOIN sucursal s ON s.activo=1
            LEFT JOIN productos_sucursales ps ON ps.productoid=p.id and ps.activo=1 and ps.sucursalid=s.id
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND pss.sucursalid=ps.sucursalid AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND p.tipo = 1
            LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND psl.sucursalid=ps.sucursalid AND psl.activo = 1 AND p.tipo = 2 and psl.cantidad > 0
            WHERE (p.activo = 1 OR p.activo = 'Y')
            AND s.activo = '1'
            GROUP BY p.id,s.id
            ORDER BY ps.sucursalid ASC, pss.sucursalid ASC, psl.sucursalid ASC, s.orden ASC, p.idProducto ASC
            ) as dat
            GROUP by id";
        $query = $this->db->query($strq);
        return $query->result();        
    }
}