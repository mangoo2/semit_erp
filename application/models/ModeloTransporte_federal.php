<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloTransporte_federal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
			0=>'o.txtCodigo',
            1=>'o.txtNombre',
            2=>'o.nombre_aseguradora',
			3=>'o.num_poliza_seguro',
            4=>'o.num_permiso_sct',
            5=>'o.configuracion_vhicular',
            6=>'o.placa_vehiculo_motor',
            7=>'o.anio_modelo_vihiculo_motor',
            8=>'o.subtipo_remolque',
            9=>'o.placa_remolque',
            10=>'o.id'
        );

        $columnsx = array( 
            0=>'o.txtCodigo',
            1=>'o.txtNombre',
            2=>'o.nombre_aseguradora',
            3=>'o.num_poliza_seguro',
            4=>'o.num_permiso_sct',
            5=>'o.configuracion_vhicular',
            6=>'o.placa_vehiculo_motor',
            7=>'o.anio_modelo_vihiculo_motor',
            8=>'o.subtipo_remolque',
            9=>'o.placa_remolque',
            10=>'o.id'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('transporte_federal o');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsx[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $columns = array( 
            0=>'o.txtCodigo',
            1=>'o.txtNombre',
            2=>'o.nombre_aseguradora',
            3=>'o.num_poliza_seguro',
            4=>'o.num_permiso_sct',
            5=>'o.configuracion_vhicular',
            6=>'o.placa_vehiculo_motor',
            7=>'o.anio_modelo_vihiculo_motor',
            8=>'o.subtipo_remolque',
            9=>'o.placa_remolque',
            10=>'o.id'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('transporte_federal o');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    public function get_config_transporte_like($search){
        $strq = "SELECT * from f_c_configautotransporte WHERE activo=1 AND clave like '%".$search."%' OR activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_f_c_tipopermiso_like($search){
        $strq = "SELECT * from f_c_tipopermiso WHERE activo=1 AND clave like '%".$search."%' OR activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

}