<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelticket extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function get_listado($params){
        $columns = array( 
            0=>'t.id',
            1=>'p.nombre',
            2=>'t.reg',
            3=>'t.asunto',
            4=>'t.estatus',
            5=>'t.tipoticket',
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ticket t');
        $this->db->join('personal p','p.personalId=t.idpersonal','left');
        if($params['estatus']!=0){
            $this->db->where(array('t.estatus'=>$params['estatus']));
        }
        if($params['tipoticket']!=0){
            $this->db->where(array('t.tipoticket'=>$params['tipoticket']));
        }
        $where = array('t.activo'=>1);
        $this->db->where($where);

        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_listado($params){
        $columns = array( 
            0=>'t.id',
            1=>'p.nombre',
            2=>'t.reg',
            3=>'t.asunto',
            4=>'t.estatus',
            5=>'t.tipoticket',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('ticket t');
        $this->db->join('personal p','p.personalId=t.idpersonal','left');
        if($params['estatus']!=0){
            $this->db->where(array('t.estatus'=>$params['estatus']));
        }
        if($params['tipoticket']!=0){
            $this->db->where(array('t.tipoticket'=>$params['tipoticket']));
        }
        $where = array('t.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    
}