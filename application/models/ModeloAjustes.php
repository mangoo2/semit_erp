<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloAjustes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function searchProductosAjuste($search){
        $strq = "SELECT p.*
            from productos p
            where referencia like '%2%' and (nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            GROUP BY p.id
            order by p.idProducto asc "; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productosAjuste($id,$id_suc){
        $this->db->select('p.id, p.idProducto, p.nombre, ps.stock, ps.id as id_ps, s.name_suc');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.activo=1 and ps.stock>0 and ps.sucursalid=".$id_suc."");
        $this->db->join("sucursal s","s.id=ps.sucursalid");
        $this->db->where("p.id",$id);
        $this->db->order_by("p.idProducto","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_productosAjusteSerie($id,$id_suc){
        $this->db->select('p.id, p.idProducto, p.nombre, pss.serie, pss.id as id_ps, s.name_suc');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_serie pss","pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=".$id_suc."");
        $this->db->join("sucursal s","s.id=pss.sucursalid");
        $this->db->where("p.id",$id);
        $this->db->order_by("pss.serie","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_productosAjusteLote($id,$id_suc){
        $this->db->select('p.id, p.idProducto, p.nombre, psl.cantidad, psl.lote, psl.caducidad, psl.id as id_ps, s.name_suc');
        $this->db->from("productos p");
        $this->db->join("productos_sucursales_lote psl","psl.productoid=p.id and psl.activo=1 and psl.cantidad>0 and psl.sucursalid=".$id_suc."");
        $this->db->join("sucursal s","s.id=psl.sucursalid");
        $this->db->where("p.id",$id);
        $this->db->order_by("psl.caducidad","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_recargaAjuste($id_suc){
        $this->db->select('r.id, r.codigo, r.capacidad, r.stock, r.sucursal, s.name_suc');
        $this->db->from("recargas r");
        $this->db->join("sucursal s","s.id=r.sucursal");
        $this->db->where("r.sucursal",$id_suc);
        $this->db->where("r.tipo","0");
        $this->db->where("r.estatus","1");
        $this->db->order_by("s.orden","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_bitacoraAjuste($id,$id_suc,$tipo){
        $sel=', "" as det_lote';
        if($tipo==1){
            $sel=", concat('<b>Serie:</b> ',ba.num_serie) as det_lote";
        }
        if($tipo==2){
            $sel=", concat(ps.lote,' / ',ps.caducidad) as det_lote";
        }
        $this->db->select('ba.*, p.idProducto, p.nombre, s.name_suc '.$sel.'');
        $this->db->from("bitacora_ajustes ba");
        $this->db->join("productos p","p.id=ba.id_producto");
        if($tipo==0 || $tipo==3 || $tipo==4){
            $this->db->join("productos_sucursales ps","ps.id=ba.id_ps AND ba.tipo_prod IN (0, 4, 5)");
        }
        if($tipo==1){
            $this->db->join("productos_sucursales_serie ps","ps.id=ba.id_ps and ba.tipo_prod=1");
        }
        if($tipo==2){
            $this->db->join("productos_sucursales_lote ps","ps.id=ba.id_ps and ba.tipo_prod=2");
        }
        $this->db->join("sucursal s","s.id=ba.id_sucursal");
        $this->db->where("ba.id_producto",$id);
        $this->db->order_by("ba.id","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_bitacoraAjusteRec($id_suc){
        $this->db->select('ba.*, r.codigo, r.capacidad, s.name_suc');
        $this->db->from("bitacora_ajustes ba");
        $this->db->join("recargas r","r.id=ba.id_producto");
        $this->db->join("sucursal s","s.id=ba.id_sucursal");
        $this->db->where("ba.id_sucursal",$id_suc);
        $this->db->where("ba.tipo_recarga","1");
        $this->db->order_by("ba.id","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

}