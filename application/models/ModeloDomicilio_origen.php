<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloDomicilio_origen extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
            0=>'o.id',
			1=>'o.calle',
			2=>'o.num_ext',
            3=>'o.num_int',
            4=>'col.nombre AS colonia',
            5=>'loc.descripcion AS localidad',
            6=>'o.referencia',
            7=>'mun.descripcion AS municipio',
            8=>'est.descripcion AS estado',
            9=>'pai.descripcion AS pais',
            10=>'o.codigo_postal',
            11=>'o.colonia as col_clave',
            12=>'o.localidad as loc_clave',
            13=>'o.municipio as mun_clave',
            14=>'o.estado as est_clave',
        );

        $columnsy = array( 
            0=>'o.id',
            1=>'o.calle',
            2=>'o.num_ext',
            3=>'o.num_int',
            4=>'col.nombre',
            5=>'loc.descripcion',
            6=>'o.referencia',
            7=>'mun.descripcion',
            8=>'est.descripcion',
            9=>'pai.descripcion',
            10=>'o.codigo_postal',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('domicilio_origen o');
        $this->db->join('f_colonia col', 'col.id=o.colonia','left');
        $this->db->join('f_c_pais pai', 'pai.c_Pais=o.pais','left');
        $this->db->join('f_c_localidad loc', 'loc.id=o.localidad','left');
        $this->db->join('f_c_municipio mun', 'mun.id=o.municipio','left');
        $this->db->join('f_c_estado est', 'est.c_Estado=o.estado','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsy as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsy[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.calle',
            2=>'o.num_ext',
            3=>'o.num_int',
            4=>'col.nombre',
            5=>'loc.descripcion',
            6=>'o.referencia',
            7=>'mun.descripcion',
            8=>'est.descripcion',
            9=>'pai.descripcion',
            10=>'o.codigo_postal',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('domicilio_origen o');
        $this->db->join('f_colonia col', 'col.id=o.colonia','left');
        $this->db->join('f_c_pais pai', 'pai.c_Pais=o.pais','left');
        $this->db->join('f_c_localidad loc', 'loc.id=o.localidad','left');
        $this->db->join('f_c_municipio mun', 'mun.id=o.municipio','left');
        $this->db->join('f_c_estado est', 'est.c_Estado=o.estado','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_colonia_like($search,$cp){
        if(intval($cp)>0){
            $w_cp=" and c_CodigoPostal='$cp' ";
            $w_cp_or="";
        }else{
            $w_cp="";
            $w_cp_or=" OR c_CodigoPostal like '%".$search."%' ";
        }
        $strq = "SELECT * from f_colonia WHERE activo=1 $w_cp AND (nombre like '%".$search."%' $w_cp_or)";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_localidad_like($search,$estado){
        $strq = "SELECT * from f_c_localidad WHERE c_Estado='$estado' AND activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_municipio_like($search,$estado){
        $strq = "SELECT * from f_c_municipio WHERE c_Estado='$estado' AND activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    public function get_pais_like($search){
        $strq = "SELECT * from f_c_pais WHERE activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_estado_like($search,$pais){
        $strq = "SELECT * from f_c_estado WHERE c_Pais='$pais' AND activo=1 AND descripcion like '%".$search."%'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
}