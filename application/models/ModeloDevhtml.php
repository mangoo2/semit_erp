<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloDevhtml extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function tiguras_view($fig){
        $result_estados=$this->getselectwheren('f_c_estado',array('activo'=>1,'c_Pais'=>'MEX'));
        $html='';
                $id_fig='f'.$fig.'_';
              $html.='<div class="row">';
                $html.='<div class="col-md-6" title="RFCFigura">';
                  $html.='<label>RFC/Id</label>';
                  $html.='<input type="text"  id="'.$id_fig.'rfc_del_operador" class="form-control" readonly>';
                $html.='</div>';
                if($fig=='01'){
                $html.='<div class="col-md-6 solooperador" title="NumLicencia">';
                  $html.='<label>NO. LICENCIA</label>';
                  $html.='<input type="text"  id="'.$id_fig.'no_licencia" class="form-control" readonly>';
                $html.='</div>';
                }
                $html.='<div class="col-md-6" title="NombreFigura">';
                  $html.='<label>NOMBRE<span class="required">*</span></label>';
                  $html.='<input type="text"  id="'.$id_fig.'operador" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>NÚMERO DE IDENTIFICACIÓN O REGISTRO FISCAL</label>';
                  $html.='<input type="text"  id="'.$id_fig.'num_identificacion" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6" title="ResidenciaFiscalFigura">';
                  $html.='<label>RESIDENCIA FISCAL</label>';
                  $html.='<select  id="'.$id_fig.'residencia_fiscal" class="form-control" title="Atributo condicional para registrar la clave del país de residencia de la figura de transporte que interviene en el traslado de los bienes y/o mercancías para los efectos fiscales correspondientes">';
                    $html.='<option></option>';
                    $html.='<option value="MEX">Mexico</option>';
                  $html.='</select>';
                $html.='</div>';
              $html.='</div>';
              $html.='<div class="row">';
                $html.='<div class="col-md-6">';
                  $html.='<label>Calle</label>';
                  $html.='<input type="text"  id="'.$id_fig.'calle" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Numero Exterior</label>';
                  $html.='<input type="text"  id="'.$id_fig.'num_ext" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Numero Interior</label>';
                  $html.='<input type="text"  id="'.$id_fig.'num_int" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Colonia</label>';
                  $html.='<input type="text"  id="'.$id_fig.'colo" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Localidad</label>';
                  $html.='<input type="text"  id="'.$id_fig.'loca" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Referencia</label>';
                  $html.='<input type="text"  id="'.$id_fig.'ref" class="form-control" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>Municipio</label>';
                  $html.='<input type="text"  id="'.$id_fig.'muni" class="form-control" readonly>';
                $html.='</div>';
              $html.='</div>';
              $html.='<div class="row">';
                $html.='<div class="col-md-6">';
                  $html.='<label>ESTADO<span class="required">*</span></label>';
                  $html.='<select class="form-control"  id="'.$id_fig.'estado" required>';
                          $html.='<option></option>';
                            foreach ($result_estados->result() as $item) {
                                $html.='<option value="'.$item->c_Estado.'">'.$item->descripcion.'</option>';
                            }
                  $html.='</select>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>PAÍS<span class="required">*</span></label>';
                  $html.='<input type="text"  id="'.$id_fig.'pais" value="MEX" class="form-control" readonly required>';
                $html.='</div>';
                $html.='<div class="col-md-6">';
                  $html.='<label>CÓDIGO POSTAL<span class="required">*</span></label>';
                  $html.='<input type="text"  id="'.$id_fig.'codigo_postal" class="form-control" readonly>';
                $html.='</div>';
              $html.='</div>';
        return $html;
    }

}