<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCotizaciones extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list($params,$perfilid,$idpersonal){
        $columns = array( 
            0=>'c.id',
            1=>'suc.name_suc',
            2=>'clif.razon_social',
            3=>'p.nombre',
            4=>'c.reg',
            5=>'c.id',
            6=>'c.folio',
            7=>"cli.correo"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizacion c');
        $this->db->join('sucursal suc', 'suc.id=c.sucursal');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('cliente_fiscales clif', 'clif.id =c.id_razonsocial');
        $this->db->join('clientes cli', 'cli.id =clif.idcliente');
        if($params['sucursal']!=0){
            $this->db->where(array('c.sucursal'=>$params['sucursal']));
        }
        if($perfilid==2 || $perfilid==3){
            $this->db->where(array('c.personalid'=>$idpersonal));
        }
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params,$perfilid,$idpersonal){
        $columns = array( 
            0=>'c.id',
            1=>'suc.name_suc',
            2=>'clif.razon_social',
            3=>'p.nombre',
            4=>'c.reg',
            5=>'c.id',
            6=>'c.folio'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizacion c');
        $this->db->join('sucursal suc', 'suc.id=c.sucursal');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('cliente_fiscales clif', 'clif.id =c.id_razonsocial');

        if($params['sucursal']!=0){
            $this->db->where(array('c.sucursal'=>$params['sucursal']));
        }
        if($perfilid==2 || $perfilid==3){
            $this->db->where(array('c.personalid'=>$idpersonal));
        }
        $where = array('c.activo'=>1);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_productos($id){
        $strq = "SELECT p.nombre,p.incluye_iva_comp,p.porcentajeiva,p.tipo, cd.tipo as tipo_prodcot, cd.cantidad,cd.precio_unitario, 
        IFNULL(ps.incluye_iva,0) as incluye_iva, IFNULL(ps.iva,0) as iva, IFNULL(cs.iva,0) as iva_serv,
        IFNULL(r.codigo,'') as recarga,IFNULL(r.capacidad,'') as capacidad, IFNULL(concat(cs.numero,' ',cs.descripcion),'') as servicio
        FROM cotizacion_detalle AS cd 
        INNER JOIN cotizacion AS c ON c.id=cd.idcotizacion
        LEFT JOIN productos AS p ON p.id=cd.idproducto and cd.tipo=0
        LEFT JOIN productos_sucursales AS ps ON ps.productoid=p.id and cd.tipo=0 and ps.sucursalid=c.sucursal
        LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1 
        LEFT JOIN cat_servicios AS cs ON cs.id=cd.idproducto and cd.tipo=3
        WHERE cd.idcotizacion=$id AND cd.activo=1
        group by cd.id";
        $query = $this->db->query($strq);
        return $query;
    }

    function get_productos_proveedor($id)
    {
        $strq ="SELECT cd.* 
            FROM cotizacion AS c 
            INNER JOIN cotizacion_clausulas AS cd ON cd.idcotizacion=c.id 
            WHERE cd.activo=1 AND cd.idcotizacion=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_cotizacion($id){
        $strq ="SELECT c.*,clif.razon_social,p.correo,p.celular
            FROM cotizacion AS c 
            LEFT JOIN cliente_fiscales AS clif ON clif.id=c.id_razonsocial
            LEFT JOIN clientes AS cli ON cli.id=clif.idcliente
            LEFT JOIN personal AS p ON p.personalId=c.personalid 
            WHERE c.activo=1 AND c.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }


}