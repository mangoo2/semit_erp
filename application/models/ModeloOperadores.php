<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloOperadores extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $fig = $params['fig'];
        $columns = array( 
            0=>'o.id',
            1=>'o.TipoFigura',
			2=>'o.rfc_del_operador',
			3=>'o.no_licencia',
			4=>'o.operador',
			5=>'o.num_identificacion',
			6=>'o.residencia_fiscal',
			7=>'o.calle',
			8=>'es.descripcion',
			9=>'o.pais',
			10=>'o.codigo_postal',
            11=>'o.estado',
            12=>'o.num_ext',
            13=>'o.num_int',
            14=>'o.colo',
            15=>'o.loca',
            16=>'o.ref',
            17=>'o.muni',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('operadores o');
        $this->db->join('f_c_estado es', 'es.c_Estado=o.estado','left');
        $this->db->where(array('o.activo'=>1));
        $this->db->where(array('o.TipoFigura'=>$fig));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $fig = $params['fig'];
        $columns = array( 
            0=>'o.id',
            1=>'o.TipoFigura',
            2=>'o.rfc_del_operador',
            3=>'o.no_licencia',
            4=>'o.operador',
            5=>'o.num_identificacion',
            6=>'o.residencia_fiscal',
            7=>'o.calle',
            8=>'es.descripcion',
            9=>'o.pais',
            10=>'o.codigo_postal',
            11=>'o.estado',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('operadores o');
        $this->db->where(array('o.activo'=>1));
        $this->db->where(array('o.TipoFigura'=>$fig));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    
}