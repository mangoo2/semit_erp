<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloRentas extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list_rentas($params){
        $columns = array( 
            0=>'r.id',
            1=>'c.nombre',
            2=>'s.descripcion',
            3=>'pss.serie',
            4=>'r.fecha_inicio',
            5=>'r.fecha_fin',
            6=>"DATE_FORMAT(r.hora_inicio,'%H:%i %p') as hora_inicio",
            7=>"DATE_FORMAT(r.hora_fin,'%H:%i %p') as hora_fin",
            8=>'r.estatus',
            9=>'r.renovacion',
            10=>'suc.name_suc',
            11=>'r.id_serie',
            12=>'requiere_factura',
            13=>'r.pagado',
            14=>"(r.costo+r.deposito+r.costo_entrega+r.costo_recolecta) as total",
            15=>"IFNULL(mi.estatus,0) as estatus_mtto",
            16=>"IFNULL(mi.id_renta,0) as id_renta",
            17=>"r.id_prod"
        );
        $columns2 = array( 
            0=>'r.id',
            1=>'c.nombre',
            2=>'s.descripcion',
            3=>'pss.serie',
            4=>'r.fecha_inicio',
            5=>'r.fecha_fin',
            6=>"hora_inicio",
            7=>"hora_fin",
            8=>'r.estatus',
            9=>'r.renovacion',
            10=>'suc.name_suc',
            11=>'r.id_serie',
            12=>"(r.costo+r.deposito+r.costo_entrega+r.costo_recolecta)",
            13=>"r.id_prod"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('mtto_interno mi', 'mi.id_renta=r.id','left');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        //$this->db->join('personal p', 'p.personalId=r.id_personal');
        $this->db->where('r.id_renta_padre','0');

        if($this->session->userdata('tipo_tecnico')){ //finalizado
            $this->db->where('r.renovacion','0'); 
            //$this->db->where('r.estatus','2'); 
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_rentas($params){
        $columns2 = array( 
            0=>'r.id',
            1=>'c.nombre',
            2=>'s.descripcion',
            3=>'pss.serie',
            4=>'r.fecha_inicio',
            5=>'r.fecha_fin',
            6=>"hora_inicio",
            7=>"hora_fin",
            8=>'r.estatus',
            9=>'r.renovacion',
            10=>'suc.name_suc',
            11=>'r.id_serie',
            12=>"(r.costo+r.deposito+r.costo_entrega+r.costo_recolecta)",
            13=>"r.id_prod"
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        $this->db->where('r.id_renta_padre','0');
        if($this->session->userdata('tipo_tecnico')){ //finalizado
            $this->db->where('r.renovacion','0'); 
            //$this->db->where('r.estatus','2'); 
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getRentaId($id){
        $this->db->select("r.*, c.nombre,c.calle, c.colonia, c.cp, c.municipio, c.celular, cf.id as id_razon,
            s.descripcion,pss.serie,pss.productoid, suc.name_suc, IFNULL(p.nombre,'') as equipo, per.nombre as per_rentas, IFNULL(per.correo,'') as correo, 
            cr.en_pagare, el_pagare, cant_pagare, porc_interes, nom_pagare, dir_pagare, pobla_pagare, tel_pagare");
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('cliente_fiscales cf', 'cf.idcliente=c.id');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('venta_erp_detalle vd', 'vd.idventa=r.id_venta and vd.tipo=2 and vd.activo=1','left');
        $this->db->join('productos p', 'p.id=r.id_prod','left');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        //$this->db->join('personal per', 'per.personalId=r.id_personal');
        $this->db->join('usuarios u', 'u.perfilId=7','left'); //rentas
        $this->db->join('personal per', 'per.personalId=u.personalId','left');
        $this->db->join('contrato_renta cr', 'cr.id_renta=r.id','left');
        $this->db->where('r.id',$id);  
        $query = $this->db->get();
        return $query->row();
    }

    function search_servs($search){
        $strq = "SELECT s.*
        from servicios s 
        where (clave like '%$search%' or descripcion like '%$search%') and activo=1
        order by descripcion asc"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function search_series($search,$idsuc){
        $idsuc=7; //solo de rentas
        /*$strq = "SELECT pss.*, p.nombre as producto
        from productos_sucursales_serie pss
        join productos p on pss.productoid=p.id
        where p.nombre like '%$search%' and (p.activo=1 or p.activo='Y')
        and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$idsuc
        order by p.nombre asc";*/ 

        $strq = "SELECT p.nombre as producto, p.id, p.concentrador
        from productos p
        where p.tipo=1 and p.nombre like '%$search%' and (p.activo=1 or p.activo='Y')
        order by p.nombre asc"; 

        $query = $this->db->query($strq);
        return $query;
    }

    function getSerieProd($id,$suc){
        $this->db->select("pss.id, pss.serie, pss.reg, p.idProducto, p.nombre, (select IFNULL(MAX(mi.prox_mtto), 0) as prox_mtto from mtto_interno mi where mi.idprod_ss=pss.id and mi.estatus=2 order by mi.prox_mtto asc) as prox_mtto");
        $this->db->from('productos_sucursales_serie pss');
        $this->db->join('productos p', 'p.id=pss.productoid');
        $this->db->where('pss.productoid',$id); 
        $this->db->where('pss.sucursalid',$suc);
        $this->db->where('pss.activo',1); 
        $this->db->where('pss.disponible',1);
        $this->db->order_by('pss.reg',"asc");
        $this->db->order_by('prox_mtto',"asc");
        $query = $this->db->get();
        return $query->result();
    }

    function getRenovaciones($id){
        $this->db->select("r.*, c.nombre,s.descripcion,pss.serie,suc.name_suc");
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        //$this->db->join('personal p', 'p.personalId=r.id_personal');
        $this->db->where('r.id_renta_padre',$id);    
        $query = $this->db->get();
        return $query->result();
    }

    function getPagosContrato($id){
        $this->db->select("r.*, c.nombre,s.descripcion,pss.serie,suc.name_suc, suc.id_alias");
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        //$this->db->join('pagos_renta pr', 'pr.id_renta=r.id','left');
        $this->db->where('r.id',$id);    
        $query = $this->db->get();
        return $query->result();
    }

    function getDocsRenta($id,$tipo){
        $this->db->select("dc.*");
        $this->db->from('docs_rentas dc');
        $this->db->where('id_renta',$id);
        $this->db->where('tipo',$tipo);
        $this->db->where('estatus',1);
        $this->db->order_by('id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function getRentasPents($id_cli=0,$idsuc=0){
        $this->db->select("r.*, c.nombre,s.descripcion,pss.serie, suc.name_suc, suc.id_alias");
        $this->db->from('rentas r');
        $this->db->join('clientes c', 'c.id=r.id_cliente');
        $this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=r.id_serie');
        $this->db->join('sucursal suc', 'suc.id=r.id_sucursal');
        if($id_cli>0){
            $this->db->where('r.id_cliente',$id_cli); 
        }
        if($id_cli>0 && $idsuc>0){
            $this->db->where('r.id_sucursal',$idsuc); 
        }
        $this->db->where('(r.estatus=0 or r.pagado=0)');  
        $this->db->order_by('r.id','desc');    
        $query = $this->db->get();
        return $query->result();
    }

    function getDetallesPagare($id){
        $this->db->select('cr.*, IFNULL(cr.id,0) as id_cr, r.costo, r.deposito, r.costo_entrega, r.costo_recolecta, c.nombre as cliente, c.calle, c.colonia, c.celular, c.municipio, c.cp');
        $this->db->from("rentas r");
        $this->db->join("contrato_renta cr","cr.id_renta=r.id","left");
        $this->db->join("clientes c","c.id=r.id_cliente");
        $this->db->where("r.id",$id);
        $query=$this->db->get(); 
        return $query->result();
    }


}