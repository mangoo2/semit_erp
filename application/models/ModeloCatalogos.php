<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->hora_actual = date('G:i:s');
        $this->fecha_reciente = date('Y-m-d');
    }

    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }

    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }

    function getselect_tabla($table){
        $this->db->select('*');
        $this->db->from($table);
        $query=$this->db->get(); 
        return $query;
    }

    function getselectwheren_2($select,$table,$where){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function getdeletewheren3($table,$where){
        $this->db->delete($table,$where);
    }

    function getdeletewheren2($table,$where){
        
        $strq = "DELETE from $table WHERE idpago=$where";
        $query = $this->db->query($strq);
    }

    function getselectwherenall($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getselectwherenallOrder($table,$where,$col,$order){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($col,$order);
        $query=$this->db->get(); 
        return $query->result();
    }
    function getselectwherenallOrdern($table,$where,$col,$order){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($col,$order);
        $query=$this->db->get(); 
        return $query;
    }

    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE estatus=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclike2($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE status=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclike3($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function updatestockWhere($tabla,$value,$masmeno,$value2,$where){
        /*$strq = "UPDATE $tabla SET $value = $value $masmeno $value2 where $where ";
        $query = $this->db->query($strq);
        return $id;*/

        $this->db->set($value, "$value $masmeno $value2", false);
        $this->db->where($where);
        $this->db->update($tabla);
    }

    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }

    function updatestock3($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2,$idname3,$id3){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 and $idname3=$id3 ";
        $query = $this->db->query($strq);
        //return $id;
    }

    function updatestock4($Tabla,$value,$masmeno,$value2,$idname1,$id1){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1";
        $query = $this->db->query($strq);
        //return $id;
    }

    function viewprefacturaslis($contrato,$tipo){
        if ($tipo>0) {
            if ($tipo==1) {
                $wheretipo=' and facpre.statuspago=0';
            }else{
                $wheretipo=' and facpre.statuspago=1';
            }
            
        }else{
            $wheretipo='';
        }
        $strq = "SELECT * 
                    FROM factura_prefactura as facpre
                    inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                    WHERE facpre.contratoId=$contrato $wheretipo"; 
        $query = $this->db->query($strq);
        return $query;
    }
    
    function viewprefacturaslis_general($contrato){
        $strq = "SELECT facpre.*,fac.*,facprep.*,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax
                FROM factura_prefactura as facpre
                inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                left join factura_prefactura_pagos as facprep on facprep.facId=facpre.facId 
                WHERE facpre.contratoId=$contrato GROUP by fac.FacturasId

                "; 
        $query = $this->db->query($strq);
        return $query;
    }

    
    function descripcionfactura($factura){
        $strq = "SELECT * FROM f_facturas_servicios where FacturasId=$factura";
        $query = $this->db->query($strq);
        $descripcion='';
        foreach ($query->result() as $item) {
            $descripcion.='<div>'.$item->Descripcion2.'</div>';
        }
        return $descripcion;
    }


    public function asignaciona_p_e($idasig)    {
        $sql = "SELECT e.modelo,r.serie
                FROM asignacion_ser_poliza_a_e AS a
                INNER JOIN polizasCreadas_has_detallesPoliza AS r ON r.id=a.idequipo
                INNER JOIN equipos AS e ON e.id=r.idEquipo
                WHERE a.asignacionId = $idasig";
        $query = $this->db->query($sql);
        return $query;
    }

 
   
    public function doc_cliente_tel_cel($id){
        $sql="SELECT
            ct.tel_local,
            cc.celular
        FROM clientes AS acl
        INNER JOIN cliente_has_telefono AS ct ON ct.idCliente=acl.id
        INNER JOIN cliente_has_celular AS cc ON cc.idCliente=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->db->query($sql);
        return $query->row();
    }
    public function doc_cliente_tel_cel2($id){
        $sql="SELECT
            ct.telefono,
            ct.celular,
            ct.email
        FROM clientes AS acl
        INNER JOIN cliente_datoscontacto AS ct ON ct.clienteId=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->db->query($sql);
        return $query->row();
    }





    /*
    function getlistfoliostatus($cliente){

    */
    //==============================================
   
    //==============================================

    function nombreunidadfacturacion($id){
        $sql="SELECT nombre
                from f_unidades
                WHERE UnidadId='$id'";
        $query=$this->db->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad,
                dt.iva,
                dt.consin_iva
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }

    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId
            ";
        $query=$this->db->query($sql);
        return $query;
    }
  

    function total_facturas($tipo,$finicio, $ffin,$tipof){
        if($tipo==1){
            $estado =' and (Estado = 1 or Estado=0)';
        }else{
            $estado =' and Estado = 0 ';
        }
        if($finicio!=''){
            $w_finicio=" and fechatimbre>='$finicio 00:00:00' ";
        }else{
            $w_finicio='';
        }
        if($ffin!=''){
            $w_ffin=" and fechatimbre<='$ffin 23:59:59' ";
        }else{
            $w_ffin='';
        }
        if($tipof>0){
            $w_tipo='';
            if($tipof==1){//facturas
                //$this->db->where("fac.f_relacion=0 and fac.f_r_tipo='0'");
                $w_tipo=" and f_relacion=0 and (f_r_tipo='0' or f_r_tipo ='01') and cartaporte=0 ";
            }
            if($tipof==2){//notas de credito
                //$this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'01'));
                $w_tipo=" and f_relacion=1 and f_r_tipo='01' ";
            }
            if($tipof==3){
                //$this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'04'));
            }
            if($tipof==4){
                //$this->db->where(array('fac.f_relacion'=>0,'fac.f_r_tipo'=>'07'));
            }
            if($tipof==5){
                //$this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'07','fac.TipoComprobante'=>'I'));
            }
            if($tipof==6){
                //$this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'07','fac.TipoComprobante'=>'E'));
            }
            if($tipof==7){
                //$this->db->where(array('fac.cartaporte'=>1));
                $w_tipo=' and cartaporte=1 ';
            }
        }else{
            $w_tipo='';
        }
        $sql="SELECT count(*) as total FROM f_facturas 
                WHERE activo =1 $estado $w_finicio $w_ffin $w_tipo";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalcomplementos($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' (Estado = 1 or Estado = 0)';
        }else{
            $estado =' Estado = 0 ';
        }
        if($finicio!=''){
            $w_finicio=" and fechatimbre>='$finicio 00:00:00' ";
        }else{
            $w_finicio='';
        }
        if($ffin!=''){
            $w_ffin=" and fechatimbre<='$ffin 23:59:59' ";
        }else{
            $w_ffin='';
        }
        $sql="SELECT count(*) as total FROM f_complementopago 
                WHERE activo=1 and $estado $w_finicio $w_ffin";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function ultimoFolio() {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1";
        $Folio = 0;//si se establese un folio inicial se coloca aqui menos uno
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }

    function topclientesfacturados(){
        $strq = "SELECT clienteId,Nombre,Rfc,sum(total) as total FROM `f_facturas` WHERE Estado=1 and activo=1 GROUP BY clienteId ORDER BY `total` DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query; 
    }
    function sumadelmesfacturados($fini,$ffin){
        $strq = "SELECT sum(total) as total FROM f_facturas WHERE Estado=1 and activo=1 AND fechatimbre BETWEEN '$fini 00:00:00' AND '$ffin 23:59:59'";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }
    function sumapagadocomplemento($factura){
        $strq = "SELECT sum(comd.ImpPagado) as total FROM `f_complementopago_documento` as comd INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId WHERE comd.facturasId=$factura and com.Estado=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }

    public function get_cliente($buscar){
        $strq="SELECT clienteId,razon_social
                FROM clientes 
                WHERE activo = 1 AND razon_social like '%$buscar%'
                UNION
                SELECT clienteId,Nombre AS Nombre
                FROM f_facturas 
                WHERE activo = 1 AND Folio like '%$buscar%'"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function total_facturas_mes_num($anio,$mes){

        $strq = "SELECT COUNT(*) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_ventas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    public function get_cliente_top_10_fecha($anio,$mes){
        $strq="SELECT c.razon_social, COUNT(*) AS reg, SUM(f.total) AS total FROM f_facturas AS f
            INNER JOIN clientes AS c ON c.clienteId=f.clienteId
            WHERE f.activo=1 AND f.Estado=1 AND MONTH(f.fechatimbre)=$mes AND YEAR(f.fechatimbre)=$anio GROUP BY f.clienteId ORDER BY SUM(f.total) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_cliente_top_10(){
        $strq="SELECT c.razon_social, COUNT(*) AS reg, SUM(f.total) AS total FROM f_facturas AS f
            INNER JOIN clientes AS c ON c.clienteId=f.clienteId
            WHERE f.activo=1 AND f.Estado=1 GROUP BY f.clienteId ORDER BY SUM(f.total) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_vacunas_top_10_fecha($anio,$mes){
        $strq="SELECT c.Descripcion2 AS Descripcion, SUM(c.Cantidad) AS reg, SUM(c.Importe) AS total FROM f_facturas AS f
            INNER JOIN f_facturas_servicios AS c ON c.FacturasId=f.FacturasId
            WHERE f.activo=1 AND f.Estado=1 AND MONTH(f.fechatimbre)=$mes AND YEAR(f.fechatimbre)=$anio GROUP BY c.Descripcion2 ORDER BY SUM(c.Importe) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_vacunas_top_10(){
        $strq="SELECT c.Descripcion2 AS Descripcion, SUM(c.Cantidad) AS reg, SUM(c.Importe) AS total FROM f_facturas AS f
            INNER JOIN f_facturas_servicios AS c ON c.FacturasId=f.FacturasId
            WHERE f.activo=1 AND f.Estado=1 GROUP BY c.Descripcion2 ORDER BY SUM(c.Importe) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getclienterazonsociallike($search){     
        $strq = "SELECT clidf.*, cli.id as id_cliente, cli.desactivar,cli.motivo,cli.dias_saldo, cli.nombre, cli.correo, cli.diaspago, cli.saldopago 
        FROM clientes as cli
        INNER JOIN cliente_fiscales as clidf on clidf.idcliente=cli.id
        WHERE cli.activo=1 AND clidf.activo=1 and (clidf.razon_social like '%$search%' or cli.nombre like '%$search%' or cli.id like '%$search%' or clidf.rfc like '%$search%')
        order by clidf.razon_social asc";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    
    function getsearchproductoslike($search){
        $strq = "SELECT p.*, ps.stock,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8 and p.tipo=2),0) AS lote8,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8 and p.tipo=1),0) as serie8 
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid=8 
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by idProducto asc"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function getsearchproductoslike_all($search){
        $strq = "SELECT p.*, ps.stock, ps.incluye_iva, ps.iva,ps.precio,
        COALESCE((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2),0) AS lote8,
        COALESCE((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id and p.tipo=1),0) as serie8, ps.sucursalid
        from productos p 
        join productos_sucursales ps on ps.productoid=p.id and ps.activo=1
        where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
        group by p.id
        order by idProducto asc"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function getsearchproductos_tipo_like($search){
        /*$strq = "SELECT p.id, p.idProducto, p.nombre, p.tipo, 
            COALESCE(ps.stock,0) as stockps, ps.incluye_iva, ps.iva,ps.precio,
            COALESCE(psl.id,'0') as id_prod_suc,
            COALESCE((SELECT sum(psl2.cantidad) FROM productos_sucursales_lote psl2 WHERE psl2.activo=1 AND psl2.productoid=p.id and p.tipo=2 and psl2.sucursalid=8),0) AS cant_lote,
            COALESCE((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8 and p.tipo=1),0) as serie8, 
            COALESCE(pss.id,'0') as id_prod_suc_serie,
            count(tsh.id) as tot_tras,
            COALESCE((SELECT count(tsh2.id) as tot_tras2 FROM traspasos_series_historial tsh2 WHERE p.tipo=1 and tsh2.idseries=pss.id and tsh2.idtraspasos=ht.idtranspasos and tsh2.activo=1 group by ht.id),0) AS tot_tras2,
            COALESCE(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END),0) AS lote8,
            COALESCE(ht.id,0) as id_ht,
            COALESCE(ht.status,2) as status_ht,
            COALESCE(tsh.idseries,0) as id_serht,
            COALESCE(tlh.id,0) as id_traslado_lote,
            COALESCE(t2.status,2) as status_traslt,
            COALESCE((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.idsucursal_sale=8 and ht.rechazado!=1),0) as traslado_stock_cant,
            COALESCE(sum(tlh.cantidad),0) as traslado_lote_cant
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid=8
            left join productos_sucursales_lote psl on psl.productoid=p.id and p.tipo=2 and psl.activo=1 and psl.sucursalid=8
            left join productos_sucursales_serie pss on pss.productoid=p.id and p.tipo=1 and pss.activo=1 and pss.disponible=1 and pss.sucursalid=8
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1 and ht.idsucursal_sale=8
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.idtraspasos=ht.idtranspasos and tsh.activo=1 
            left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt 
            left join traspasos t2 on t2.id=tlh.idtraspasos
            where (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            group by p.id
            order by idProducto asc
        "; */
        $strq="SELECT p.id, p.idProducto, p.nombre, p.tipo, p.unidad_sat, COALESCE(ps.stock, 0) AS stockps, ps.incluye_iva, ps.iva, ps.precio,
        COALESCE(psl.id, '0') AS id_prod_suc,
        COALESCE(SUM(CASE WHEN psl.activo = 1 AND p.tipo = 2 AND psl.sucursalid = 8 THEN psl.cantidad ELSE 0 END), 0) AS cant_lote,
        COALESCE(COUNT(DISTINCT CASE WHEN pss.activo = 1 AND pss.disponible = 1 AND pss.sucursalid = 8 AND p.tipo = 1 THEN pss.id ELSE NULL END), 0) AS serie8,
        COALESCE(pss.id, '0') AS id_prod_suc_serie,
        COALESCE(COUNT(DISTINCT tsh.id), 0) AS tot_tras,
        COALESCE(SUM(CASE WHEN p.tipo = 1 AND tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos THEN 1 ELSE 0 END), 0) AS tot_tras2,
        COALESCE(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END), 0) AS lote8,
        COALESCE(MAX(ht.id), 0) AS id_ht,  
        COALESCE(ht.status, 2) AS status_ht,
        COALESCE(tsh.idseries, 0) AS id_serht,
        COALESCE(tlh.id, 0) AS id_traslado_lote,
        COALESCE(t2.status, 2) AS status_traslt,
        COALESCE(SUM(CASE WHEN ht.activo = 1 AND ht.tipo = 0 AND (ht.status = 0 or ht.status=1) AND t.tipo = 0 AND ht.idsucursal_sale = 8 THEN ht.cantidad ELSE 0 END), 0) AS traslado_stock_cant,
        COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant
        FROM productos p 
        JOIN productos_sucursales ps ON ps.productoid = p.id AND ps.sucursalid = 8
        LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND p.tipo = 2 AND psl.activo = 1 AND psl.sucursalid = 8
        LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND p.tipo = 1 AND pss.activo = 1 AND pss.disponible = 1 AND pss.sucursalid = 8
        LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id AND ht.tipo = 0 AND ht.activo = 1 AND ht.status != 2 AND ht.rechazado != 1 AND ht.idsucursal_sale = 8
        LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos AND tsh.activo = 1
        LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt
        LEFT JOIN traspasos t2 ON t2.id = tlh.idtraspasos
        LEFT JOIN traspasos t ON t.id = ht.idtranspasos 
        WHERE (p.nombre LIKE '%$search%' OR p.idProducto LIKE '%$search%')
          AND (p.activo = 1 OR p.activo = 'Y')
        GROUP BY p.id
        ORDER BY p.idProducto ASC"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function getCantidadTraslado($id,$suc){
        $strq="SELECT
        COALESCE(COUNT(DISTINCT tsh.id), 0) AS tot_tras,
        COALESCE(SUM(CASE WHEN p.tipo = 2 AND tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos AND tsh.activo = 1 THEN 1 ELSE 0 END), 0) AS tot_tras2,
        COALESCE(SUM(CASE WHEN ht.activo = 1 AND ht.tipo = 0 AND (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) AND ht.idsucursal_sale = $suc THEN ht.cantidad ELSE 0 END), 0) AS traslado_stock_cant,
        COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant
        FROM productos p 
        LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND p.tipo = 2 AND psl.activo = 1 AND psl.sucursalid = $suc
        LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND p.tipo = 1 AND pss.activo = 1 AND pss.disponible = 1 and pss.vendido=0 AND pss.sucursalid = $suc
        LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id AND ht.tipo = 0 AND ht.activo = 1 AND ht.status != 2 AND ht.rechazado != 1 AND ht.idsucursal_sale = $suc
        LEFT JOIN traspasos t ON t.id = ht.idtranspasos 
        LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos AND tsh.activo = 1
        LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt
        WHERE p.id=$id ";

        $query = $this->db->query($strq);
        return $query;
    }

    function searchproductos_tipo_likeAsigna($search,$suc){
        $strq="SELECT p.id, p.idProducto, p.nombre, p.tipo, p.unidad_sat, COALESCE(ps.stock, 0) AS stockps, ps.incluye_iva, ps.iva, ps.precio,
        COALESCE(psl.id, '0') AS id_prod_suc,
        COALESCE(SUM(DISTINCT CASE WHEN psl.activo = 1 AND p.tipo = 2 AND psl.sucursalid = 8 THEN psl.cantidad ELSE 0 END), 0) AS cant_lote,
        COALESCE(COUNT(DISTINCT CASE WHEN pss.productoid = p.id AND p.tipo = 1 AND pss.activo = 1 AND pss.disponible = 1 AND pss.vendido=0 AND pss.sucursalid = 8 THEN pss.id ELSE NULL END), 0) AS serie8,
        COALESCE(pss.id, '0') AS id_prod_suc_serie,
        COALESCE(COUNT(DISTINCT CASE WHEN (ht.status=0 and t3.tipo=0 or ht.status=1 and t3.tipo=1) THEN tsh.id ELSE 0 END), 0) AS tot_tras,
        COALESCE(SUM(CASE WHEN p.tipo = 1 AND tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos THEN 1 ELSE 0 END), 0) AS tot_tras2,
        COALESCE(SUM(DISTINCT CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END), 0) AS lote8,
        COALESCE(MAX(ht.id), 0) AS id_ht,  
        COALESCE(ht.status, 2) AS status_ht,
        COALESCE(tsh.idseries, 0) AS id_serht,
        COALESCE(tlh.id, 0) AS id_traslado_lote,
        COALESCE(t2.status, 2) AS status_traslt,
        COALESCE(SUM(CASE WHEN p.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) THEN ht.cantidad ELSE 0 END), 0) AS traslado_stock_cant,
        COALESCE(SUM(CASE WHEN p.tipo=2 THEN tlh.cantidad ELSE 0 END), 0) AS traslado_lote_cant,
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2
            THEN (
                COALESCE((SELECT ps_ent.stock 
                    FROM productos_sucursales ps_ent
                    WHERE ps_ent.productoid = p.id
                    AND ps_ent.activo = 1
                    AND ps_ent.sucursalid = $suc),0)
            )
            ELSE 0
        END as stockps_ent,
        CASE 
            WHEN p.tipo =2 
            THEN (
                COALESCE((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2 and sucursalid=$suc),0)
            ) 
            ELSE 0 
        END as stock_lotes_ent,
        CASE 
            WHEN p.tipo =1 
            THEN (
                COALESCE((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND vendido=0 AND productoid=p.id AND sucursalid=$suc),0)
            ) 
            ELSE 0 
        END as stock_series_ent
        FROM productos p 
        JOIN productos_sucursales ps ON ps.productoid = p.id AND ps.sucursalid = 8
        LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND p.tipo = 2 AND psl.activo = 1 AND psl.sucursalid = 8
        LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND p.tipo = 1 AND pss.activo = 1 AND pss.vendido=0 AND pss.disponible = 1 AND pss.sucursalid = 8
        LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id AND ht.tipo = 0 AND ht.activo = 1 AND ht.status != 2 AND ht.rechazado != 1 AND ht.idsucursal_sale = 8
        LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos AND tsh.activo = 1
        LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt
        LEFT JOIN traspasos t2 ON t2.id = tlh.id_historialt and (ht.status=0 and t2.tipo=0 or ht.status=1 and t2.tipo=1)
        LEFT JOIN traspasos t ON t.id = ht.idtranspasos and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1)
        LEFT JOIN traspasos t3 ON t.id = tsh.idtraspasos and (ht.status=0 and t3.tipo=0 or ht.status=1 and t3.tipo=1)
        WHERE (p.nombre LIKE '%$search%' OR p.idProducto LIKE '%$search%')
          AND (p.activo = 1 OR p.activo = 'Y')
        GROUP BY p.id
        ORDER BY p.idProducto ASC"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function getsearchproductos_tipo_likePV($search,$id_sucursal_salida){
        $strq = "SELECT p.*, ps.stock, ps.incluye_iva, ps.iva,ps.precio,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=$id_sucursal_salida and p.tipo=2),0) AS lote8,  IFNULL(psl.id,'0') as id_prod_suc,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=$id_sucursal_salida and p.tipo=1),0) as serie8, IFNULL(pss.id,'0') as id_prod_suc_serie
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid=$id_sucursal_salida
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$id_sucursal_salida and p.tipo=2
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$id_sucursal_salida and p.tipo=1
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            group by p.id 
            order by idProducto asc"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function get_unidad_producto($idprod){
        $strq = "SELECT p.id, IFNULL(fu.nombre,'') as nom_unidad
        from productos p 
        left join f_unidades fu on fu.Clave=p.unidad_sat
        where p.id=$idprod";
        $query = $this->db->query($strq);
        return $query->row();
    }

    function getsearchproductos_tipo_deta_like($search,$idsuc){
        /*=================consulta original========================*/
            /*$strq = "SELECT p.id, p.idProducto, p.nombre, p.tipo, COALESCE(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid,
            COALESCE(psl.cantidad,0) as cant_lote, 
            COALESCE(psl.lote,'') as lote, 
            COALESCE(psl.caducidad,'') as caducidad,
            COALESCE(pss.serie,'') as serie,
            COALESCE(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END),0) AS lote8,
            COALESCE(psl.id,'0') as id_prod_suc,
            COALESCE(pss.id,'0') as id_prod_suc_serie,
            COALESCE(ht.id,0) as id_ht,
            COALESCE(ht.status,2) as status_ht,
            COALESCE(tsh.idseries,0) as id_serht,
            COALESCE(tlh.id,0) as id_traslado_lote,
            COALESCE(t2.status,2) as status_traslt,
            COALESCE((
                SELECT SUM(ht.cantidad)
                FROM historial_transpasos ht
                JOIN traspasos t ON t.id = ht.idtranspasos
                WHERE ht.idproducto = p.id
                  AND ht.idsucursal_sale = $idsuc
                  AND ht.activo = 1
                  AND ht.tipo = 0
                  AND ht.rechazado != 1
                  AND (ht.status = 0 and t.tipo=0 or ht.status=1 and t.tipo=1)
                  AND t.activo = 1
                  AND t.status = 1
                  AND t.rechazado = 0
            ), 0) AS traslado_stock_cant,
            COALESCE(sum(tlh.cantidad),0) as traslado_lote_cant,
            '' as peligro,
            CASE 
                WHEN ps.iva = 0 and ps.incluye_iva = 1 THEN ps.precio
                WHEN ps.iva != 0 and ps.incluye_iva = 1 THEN ps.precio * 1.16
                
                WHEN ps.iva > 0 and ps.incluye_iva = 0 THEN ps.precio / 1.16
                WHEN ps.iva = 0 and ps.incluye_iva = 0 THEN ps.precio 
                ELSE 0 
            END AS precio_con_ivax,
            CASE 
                WHEN p.tipo = 0 THEN 
                    CONCAT(p.idProducto, ' ', p.nombre, ' / Cantidad: ', p.stock, ' / $',
                        CASE 
                            WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                            WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                            WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                            WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                            ELSE 0 
                        END
                    )
                WHEN p.tipo = 1 THEN 
                    CONCAT(pss.serie, ' ', p.nombre, ' / Cantidad: ', COALESCE(COUNT(DISTINCT pss.id), 0), ' / $',
                        CASE 
                            WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                            WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                            WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                            WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                            ELSE 0 
                        END
                    )
                WHEN p.tipo = 2 THEN 
                    CONCAT(psl.lote, ' ', p.nombre, ' / Cantidad: ', COALESCE(psl.cantidad,0), ' / $',
                        CASE 
                            WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                            WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                            WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                            WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                            ELSE 0 
                        END
                    )
                ELSE '' 
            END AS nom_text,
            CASE 
                WHEN p.tipo = 0 THEN ''
                WHEN p.tipo = 1 THEN pss.serie
                WHEN p.tipo = 2 THEN psl.lote
                ELSE '' 
            END AS detalle,
            CASE 
                WHEN p.tipo = 0 THEN ''
                WHEN p.tipo = 1 THEN ''
                WHEN p.tipo = 2 THEN psl.caducidad
                ELSE '' 
            END AS detalle2
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid='$idsuc'
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid='$idsuc' and p.tipo=2
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid='$idsuc' and p.tipo=1
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.idtraspasos=ht.idtranspasos and tsh.activo=1 and p.tipo=1
            left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt and p.tipo=2
            left join traspasos t2 on t2.id=tlh.idtraspasos
            where (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo='1' or p.activo='Y')
            GROUP BY p.id, pss.id, psl.id
            order by p.idProducto asc, psl.caducidad asc"; 
        */
        $strq="SELECT p.id, p.idProducto, p.nombre, p.tipo, 
        COALESCE(ps.stock, 0) AS stock, ps.incluye_iva, ps.iva, ps.precio, ps.sucursalid,
        COALESCE(psl.cantidad, 0) AS cant_lote, 
        COALESCE(psl.lote, '') AS lote, 
        COALESCE(psl.caducidad, '') AS caducidad,
        COALESCE(pss.serie, '') AS serie,
        COALESCE(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END), 0) AS lote8,
        COALESCE(psl.id, '0') AS id_prod_suc,
        COALESCE(pss.id, '0') AS id_prod_suc_serie,
        COALESCE(ht.id, 0) AS id_ht,
        COALESCE(ht.status, 2) AS status_ht,
        COALESCE(tsh.idseries, 0) AS id_serht,
        COALESCE(tlh.id, 0) AS id_traslado_lote,
        COALESCE(t2.status, 2) AS status_traslt,
        COALESCE(traslado_stock.traslado_stock_cant, 0) AS traslado_stock_cant,
        COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant,
        '' AS peligro,
        CASE 
            WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
            WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
            WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
            WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio 
            ELSE 0 
        END AS precio_con_ivax,
        CASE 
            WHEN p.tipo != 1 and p.tipo != 2 THEN 
                CONCAT(p.idProducto, ' ', p.nombre, ' / Cantidad: ', p.stock, ' / $',
                    CASE 
                        WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                        WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                        WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                        WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                        ELSE 0 
                    END
                )
            WHEN p.tipo = 1 THEN 
                CONCAT(pss.serie, ' ', p.nombre, ' / Cantidad: ', COALESCE(COUNT(DISTINCT pss.id), 0), ' / $',
                    CASE 
                        WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                        WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                        WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                        WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                        ELSE 0 
                    END
                )
            WHEN p.tipo = 2 THEN 
                CONCAT(psl.lote, ' ', p.nombre, ' / Cantidad: ', COALESCE(psl.cantidad, 0), ' / $',
                    CASE 
                        WHEN ps.iva = 0 AND ps.incluye_iva = 1 THEN ps.precio
                        WHEN ps.iva != 0 AND ps.incluye_iva = 1 THEN ps.precio * 1.16
                        WHEN ps.iva > 0 AND ps.incluye_iva = 0 THEN ps.precio / 1.16
                        WHEN ps.iva = 0 AND ps.incluye_iva = 0 THEN ps.precio
                        ELSE 0 
                    END
                )
            ELSE '' 
        END AS nom_text,
        CASE 
            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
            WHEN p.tipo = 1 THEN pss.serie
            WHEN p.tipo = 2 THEN psl.lote
            ELSE '' 
        END AS detalle,
        CASE 
            WHEN p.tipo = p.tipo != 1 and p.tipo != 2 THEN ''
            WHEN p.tipo = 1 THEN ''
            WHEN p.tipo = 2 THEN psl.caducidad
            ELSE '' 
        END AS detalle2
        FROM productos p 
        JOIN productos_sucursales ps ON ps.productoid = p.id AND ps.sucursalid = $idsuc
        LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND psl.activo = 1 AND psl.sucursalid = $idsuc AND p.tipo = 2
        LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND pss.activo = 1 AND pss.disponible = 1 AND pss.sucursalid = $idsuc AND p.tipo = 1
        LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id AND ht.tipo = 0 AND ht.activo = 1 AND ht.status != 2 AND ht.rechazado != 1
        LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.idtraspasos = ht.idtranspasos AND tsh.activo = 1 AND p.tipo = 1
        LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt AND p.tipo = 2
        LEFT JOIN traspasos t2 ON t2.id = tlh.idtraspasos
        LEFT JOIN (
            SELECT 
                ht.idproducto, 
                SUM(ht.cantidad) AS traslado_stock_cant
            FROM 
                historial_transpasos ht
            JOIN 
                traspasos t ON t.id = ht.idtranspasos
            WHERE 
                ht.idsucursal_sale = $idsuc
                AND ht.activo = 1
                AND ht.tipo = 0
                AND ht.rechazado != 1
                AND (ht.status = 0 AND t.tipo = 0 OR ht.status = 1 AND t.tipo = 1)
                AND t.activo = 1
                AND t.status = 1
                AND t.rechazado = 0
            GROUP BY 
                ht.idproducto
        ) AS traslado_stock ON traslado_stock.idproducto = p.id
        WHERE (p.nombre LIKE '%$search%' OR p.idProducto LIKE '%$search%') 
            AND (p.activo = '1' OR p.activo = 'Y')
        GROUP BY 
            p.id, pss.id, psl.id
        ORDER BY 
            p.idProducto ASC, psl.caducidad ASC";

        /*==========================================*/
        /*
        $strq = "";
        $strq .= "SELECT p.*, IFNULL(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid,
        0 as cant_lote, 
        '' as lote, 
        '' as caducidad,
        '' as serie,
        0 AS lote8,
        '0' as id_prod_suc,
        0 as serie8,
        0 as id_prod_suc_serie,
        IFNULL(ht.id,0) as id_ht,
        IFNULL(ht.status,2) as status_ht,
        0 as id_serht,
        0 as id_traslado_lote,
        2 as status_traslt,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
            join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
            where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$idsuc and ht.rechazado!=1),0) as traslado_stock_cant,
        0 as traslado_lote_cant,
        ser.material_peligroso as peligro,
        p.id as pid,
        0 as pssid,
        0 as pslid
        from productos p 
        join productos_sucursales ps on ps.productoid=p.id and sucursalid='$idsuc'
        left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
        left join f_servicios as ser on ser.Clave=p.servicioId_sat
        where p.tipo=0 and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo='1' or p.activo='Y')
        GROUP BY p.id";
        $strq .= " union ";
        $strq .= "SELECT p.*, IFNULL(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid,
            0 as cant_lote, 
            '' as lote, 
            '' as caducidad,
            IFNULL(pss.serie,'') as serie,
            0 AS lote8,
            0 as id_prod_suc,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid='$idsuc' and p.tipo=1),0) as serie8,
            IFNULL(pss.id,'0') as id_prod_suc_serie,
            IFNULL(ht.id,0) as id_ht,
            IFNULL(ht.status,2) as status_ht,
            IFNULL(tsh.idseries,0) as id_serht,
            IFNULL(tlh.id,0) as id_traslado_lote,
            IFNULL(t2.status,2) as status_traslt,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$idsuc and ht.rechazado!=1),0) as traslado_stock_cant,
            IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
            ser.material_peligroso as peligro,
            p.id as pid, 
            pss.id as pssid, 
            0 as pslid
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid='$idsuc'
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid='$idsuc'
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.idtraspasos=ht.idtranspasos and tsh.activo=1
            left join traspasos t2 on t2.id=tlh.idtraspasos
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            where p.tipo=1 and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo='1' or p.activo='Y')
            GROUP BY p.id, pss.id
            ";
            $strq .= " union ";

            $strq .= "SELECT p.*, IFNULL(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid,
            IFNULL(psl.cantidad,0) as cant_lote, 
            IFNULL(psl.lote,'') as lote, 
            IFNULL(psl.caducidad,'') as caducidad,
            '' as serie,
            IFNULL(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END),0) AS lote8,
            IFNULL(psl.id,'0') as id_prod_suc,
            0 as serie8,
            0 as id_prod_suc_serie,
            IFNULL(ht.id,0) as id_ht,
            IFNULL(ht.status,2) as status_ht,
            0 as id_serht,
            IFNULL(tlh.id,0) as id_traslado_lote,
            IFNULL(t2.status,2) as status_traslt,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$idsuc and ht.rechazado!=1),0) as traslado_stock_cant,
            IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
            ser.material_peligroso as peligro,
            p.id as pid, 
            0 as pssid, 
            psl.id as pslid
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid='$idsuc'
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid='$idsuc'
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
            left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt
            left join traspasos t2 on t2.id=tlh.idtraspasos
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            where p.tipo=2 and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo='1' or p.activo='Y')
            GROUP BY p.id,  psl.id";

            $strq .= " union ";

            $strq .= "SELECT p.*, IFNULL(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio, ps.sucursalid,
            IFNULL(psl.cantidad,0) as cant_lote, 
            IFNULL(psl.lote,'') as lote, 
            IFNULL(psl.caducidad,'') as caducidad,
            IFNULL(pss.serie,'') as serie,
            IFNULL(SUM(CASE WHEN p.tipo = 2 THEN psl.cantidad ELSE 0 END),0) AS lote8,
            IFNULL(psl.id,'0') as id_prod_suc,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid='$idsuc' and p.tipo=1),0) as serie8,
            IFNULL(pss.id,'0') as id_prod_suc_serie,
            IFNULL(ht.id,0) as id_ht,
            IFNULL(ht.status,2) as status_ht,
            IFNULL(tsh.idseries,0) as id_serht,
            IFNULL(tlh.id,0) as id_traslado_lote,
            IFNULL(t2.status,2) as status_traslt,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$idsuc and ht.rechazado!=1),0) as traslado_stock_cant,
            IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
            ser.material_peligroso as peligro,
            p.id as pid, 
            pss.id as pssid, 
            psl.id as pslid
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid='$idsuc'
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid='$idsuc'
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid='$idsuc'
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.idtraspasos=ht.idtranspasos and tsh.activo=1
            left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt
            left join traspasos t2 on t2.id=tlh.idtraspasos
            left join f_servicios as ser on ser.Clave=p.servicioId_sat
            where p.tipo>2 and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo='1' or p.activo='Y')
            GROUP BY p.id, pss.id, psl.id ";

        $strq .= " order by idProducto asc, caducidad asc ";
        log_message('error','getsearchproductos_tipo_deta_like '.$strq);
        */
        $query = $this->db->query($strq);
        return $query;
    }

    function getsearchproductos_tipo_like_all($search){
        $strq = "SELECT p.*, IFNULL(ps.stock,0) as stock, ps.incluye_iva, ps.iva,ps.precio,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2),0) AS lote8,  IFNULL(psl.id,'0') as id_prod_suc,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id and p.tipo=1),0) as serie8, IFNULL(pss.id,'0') as id_prod_suc_serie
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            group by p.id 
            order by idProducto asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductoslike_compras($search){
        $strq = "SELECT * from productos where (nombre like '%$search%' or idProducto like '%$search%') and (activo=1 or activo='Y' or activo='N') and referencia like '%3%' order by idProducto asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function getsearchservicios_ventas_like($search){
        $strq = "SELECT * from cat_servicios where (numero like '%$search%' or descripcion like '%$search%') order by descripcion asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_ventas_like($search,$suc){
        /*
        $strq = "SELECT p.*, IFNULL(p.stock,0) as stock, IFNULL(concat('Lote: ',psl.lote,' ','Caducidad: ',psl.caducidad,' (','',psl.cantidad-IFNULL(sum(tlh.cantidad),0),')'),'') as lote, IFNULL(psl.id,'0') as id_prod_suc, psl.caducidad,
            (select ps.id from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as id_ps,
            (select ps.incluye_iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as incluye_iva,
            (select ps.precio from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as precio,
            (select ps.iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as iva,
            (select ps.stock from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as stock_suc,
            IFNULL(concat('Serie: ',pss.serie),'') as serie, IFNULL(pss.id,'0') as id_prod_suc_serie,
            IFNULL(tsh.id,0) as id_traslado_serie,
            IFNULL(t.status,2) as status_trasr,
            IFNULL(tlh.id,0) as id_traslado_lote,
            IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
            IFNULL(t2.status,2) as status_traslt,
            IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.idsucursal_sale=$suc and ht.rechazado!=1),0) as traslado_stock_cant,
            IFNULL(psl.cantidad,0) AS lote8,
            IFNULL(1,0) as serie8 
            from productos p
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$suc and psl.cantidad>0
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$suc
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
            left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt
            left join traspasos t2 on t2.id=tlh.idtraspasos
            where referencia like '%2%' and (nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            GROUP BY p.id, pss.id, psl.id
            order by psl.caducidad asc, p.idProducto asc "; 
            */
        $strq = "
            SELECT * from ((
                SELECT p.*, IFNULL(p.stock,0) as stock0, IFNULL(concat('Lote: ',psl.lote,' ','Caducidad: ',psl.caducidad,' (','',psl.cantidad-IFNULL(sum(tlh.cantidad),0),')'),'') as lote, IFNULL(psl.id,'0') as id_prod_suc, psl.caducidad,
                (select ps.id from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as id_ps,
                (select ps.incluye_iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as incluye_iva,
                (select ps.precio from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as precio,
                (select ps.iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as iva,
                (select ps.stock from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as stock_suc,
                IFNULL(concat('Serie: ',pss.serie),'') as serie, IFNULL(pss.id,'0') as id_prod_suc_serie,
                IFNULL(tsh.id,0) as id_traslado_serie,
                IFNULL(t.status,2) as status_trasr,
                IFNULL(tlh.id,0) as id_traslado_lote,
                IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
                IFNULL(t2.status,2) as status_traslt,
                IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                    join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                    where ht.idproducto=p.id and p.tipo != 1 and p.tipo != 2 and ht.activo=1 and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.idsucursal_sale=$suc and ht.rechazado!=1),0) as traslado_stock_cant,
                IFNULL(psl.cantidad,0) AS lote8,
                IFNULL(1,0) as serie8,
                IFNULL(c.estatus,0) as estatus_compra, IFNULL(psl.idcompra,0) as id_compra
                from productos p
                left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$suc and psl.cantidad>0 and p.tipo=2
                left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid='$suc' and p.tipo=1
                left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1 and p.tipo=1
                left join traspasos t on t.id=tsh.idtraspasos
                left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
                left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt and p.tipo=2
                left join traspasos t2 on t2.id=tlh.idtraspasos
                left join compra_erp c on c.id=psl.idcompra and c.estatus=1
                where referencia like '%2%' and (nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
                and pss.serie is null
                GROUP BY p.id, pss.id, psl.id
            )
            union
            (
                SELECT p.*, IFNULL(p.stock,0) as stock0, IFNULL(concat('Lote: ',psl.lote,' ','Caducidad: ',psl.caducidad,' (','',psl.cantidad-IFNULL(sum(tlh.cantidad),0),')'),'') as lote, IFNULL(psl.id,'0') as id_prod_suc, psl.caducidad,
                (select ps.id from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as id_ps,
                (select ps.incluye_iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as incluye_iva,
                (select ps.precio from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as precio,
                (select ps.iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as iva,
                (select ps.stock from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as stock_suc,
                IFNULL(concat('Serie: ',pss.serie),'') as serie, IFNULL(pss.id,'0') as id_prod_suc_serie,
                IFNULL(tsh.id,0) as id_traslado_serie,
                IFNULL(t.status,2) as status_trasr,
                IFNULL(tlh.id,0) as id_traslado_lote,
                IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
                IFNULL(t2.status,2) as status_traslt,
                IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                    join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                    where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.idsucursal_sale=$suc and ht.rechazado!=1),0) as traslado_stock_cant,
                IFNULL(psl.cantidad,0) AS lote8,
                IFNULL(1,0) as serie8,
                IFNULL(c.estatus,0) as estatus_compra, IFNULL(psl.idcompra,0) as id_compra
                from productos p
                left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$suc and psl.cantidad>0 and p.tipo=2
                INNER join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid='$suc' and p.tipo=1
                left join compra_erp c on c.id=pss.idcompras and c.estatus=1
                left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1 and p.tipo=1
                left join traspasos t on t.id=tsh.idtraspasos
                left join historial_transpasos ht on ht.idproducto=p.id and ht.tipo=0 and ht.activo=1 and ht.status!=2 and ht.rechazado!=1
                left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and ht.id=tlh.id_historialt and p.tipo=2
                left join traspasos t2 on t2.id=tlh.idtraspasos
                where referencia like '%2%' and (nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
                and pss.serie is not null
                and ((pss.idcompras!=0 AND c.estatus=1) OR (pss.idcompras=0)) 
                GROUP BY p.id, pss.id, psl.id
            )) as datos 
            order by caducidad asc, idProducto asc
        ";  
        $strq="SELECT * FROM (
            SELECT p.id, p.nombre, p.idProducto, p.concentrador, p.tanque, p.capacidad, p.tipo,
                COALESCE(p.stock, 0) AS stock0,
                COALESCE(CONCAT('Lote: ', psl.lote, ' ', 'Caducidad: ', psl.caducidad, ' (', psl.cantidad - COALESCE(SUM(tlh.cantidad), 0), ')'), '') AS lote,
                COALESCE(psl.id, '0') AS id_prod_suc,
                psl.caducidad, ps.id AS id_ps, ps.incluye_iva, ps.precio, ps.iva, ps.stock AS stock_suc,
                COALESCE(CONCAT('Serie: ', pss.serie), '') AS serie,
                COALESCE(pss.id, '0') AS id_prod_suc_serie,
                COALESCE(tsh.id, 0) AS id_traslado_serie,
                COALESCE(t.status, 2) AS status_trasr,
                COALESCE(tlh.id, 0) AS id_traslado_lote,
                COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant,
                COALESCE(t2.status, 2) AS status_traslt,
                COALESCE(SUM(ht.cantidad), 0) AS traslado_stock_cant,
                COALESCE(psl.cantidad, 0) AS lote8,
                COALESCE(1, 0) AS serie8,
                COALESCE(c.estatus, 0) AS estatus_compra,
                COALESCE(psl.idcompra, 0) AS id_compra
            FROM productos p
            LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id 
                AND psl.activo = 1 
                AND psl.sucursalid = $suc
                AND psl.cantidad > 0 
                AND p.tipo = 2
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id 
                AND pss.activo = 1 
                AND pss.disponible = 1 
                AND pss.sucursalid = $suc 
                AND pss.vendido = 0
                AND p.tipo = 1
            LEFT JOIN productos_sucursales ps ON ps.productoid = p.id 
                AND ps.sucursalid = $suc
                AND ps.activo = 1
            LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.activo = 1 AND p.tipo = 1
            LEFT JOIN traspasos t ON t.id = tsh.idtraspasos
            LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id 
                AND ht.tipo = 0 
                AND ht.activo = 1 
                AND ht.status != 2 
                AND ht.rechazado != 1
                AND ht.idsucursal_sale = $suc
            LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt AND p.tipo = 2
            LEFT JOIN traspasos t2 ON t2.id = tlh.idtraspasos
            LEFT JOIN compra_erp c ON c.id = psl.idcompra AND c.estatus = 1 and p.tipo=2
            WHERE referencia LIKE '%2%' 
                AND (nombre LIKE '%$search%' OR p.idProducto LIKE '%$search%')  
                AND (p.activo = 1 OR p.activo = 'Y') 
                AND pss.serie IS NULL
            GROUP BY p.id, psl.id
            
            UNION
            SELECT p.id, p.nombre, p.idProducto, p.concentrador, p.tanque, p.capacidad, p.tipo,
                COALESCE(p.stock, 0) AS stock0,
                COALESCE(CONCAT('Lote: ', psl.lote, ' ', 'Caducidad: ', psl.caducidad, ' (', psl.cantidad - COALESCE(SUM(tlh.cantidad), 0), ')'), '') AS lote,
                COALESCE(psl.id, '0') AS id_prod_suc,
                psl.caducidad, ps.id AS id_ps, ps.incluye_iva, ps.precio, ps.iva, ps.stock AS stock_suc,
                COALESCE(CONCAT('Serie: ', pss.serie), '') AS serie,
                COALESCE(pss.id, '0') AS id_prod_suc_serie,
                COALESCE(tsh.id, 0) AS id_traslado_serie,
                COALESCE(t.status, 2) AS status_trasr,
                COALESCE(tlh.id, 0) AS id_traslado_lote,
                COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant,
                COALESCE(t2.status, 2) AS status_traslt,
                COALESCE(SUM(ht.cantidad), 0) AS traslado_stock_cant,
                COALESCE(psl.cantidad, 0) AS lote8,
                COALESCE(1, 0) AS serie8,
                COALESCE(c.estatus, 0) AS estatus_compra,
                COALESCE(pss.idcompras, 0) AS id_compra
            FROM productos p
            LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id 
                AND psl.activo = 1 
                AND psl.sucursalid = $suc
                AND psl.cantidad > 0 
                AND p.tipo = 2
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id 
                AND pss.activo = 1 
                AND pss.disponible = 1 
                AND pss.vendido = 0
                AND pss.sucursalid = $suc 
                AND p.tipo = 1
            LEFT JOIN productos_sucursales ps ON ps.productoid = p.id 
                AND ps.sucursalid = $suc
                AND ps.activo = 1
            LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.activo = 1 AND p.tipo = 1
            LEFT JOIN traspasos t ON t.id = tsh.idtraspasos
            LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id 
                AND ht.tipo = 0 
                AND ht.activo = 1 
                AND ht.status != 2 
                AND ht.rechazado != 1
                AND ht.idsucursal_sale = $suc
            LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 AND ht.id = tlh.id_historialt AND p.tipo = 2
            LEFT JOIN traspasos t2 ON t2.id = tlh.idtraspasos
            LEFT JOIN compra_erp c ON c.id = pss.idcompras AND c.estatus = 1 and p.tipo=1
            WHERE referencia LIKE '%2%' 
                AND (nombre LIKE '%$search%' OR p.idProducto LIKE '%$search%')  
                AND (p.activo = 1 OR p.activo = 'Y') 
                AND ((pss.idcompras!=0 AND c.estatus=1) OR (pss.idcompras=0)) 
                AND pss.serie IS NOT NULL
            GROUP BY p.id, pss.id, psl.id
        ) AS datos
        ORDER BY caducidad ASC, idProducto ASC";
        $query = $this->db->query($strq);
        return $query;
    }

    function searchInsumo_ventas_like($search,$suc){
        $strq = "SELECT p.*, IFNULL(p.stock,0) as stock,
        (select ps.id from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as id_ps,
        (select ps.incluye_iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as incluye_iva,
        (select ps.precio from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as precio,
        (select ps.iva from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as iva,
        (select ps.stock from productos_sucursales ps where ps.productoid=p.id and ps.sucursalid=$suc and ps.activo=1) as stock_suc
        from productos p
        where (p.tipo=3 or  p.tipo=4) and (nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
        GROUP BY p.id
        order by p.idProducto asc "; 
        $query = $this->db->query($strq);
        return $query;
    }

    function get_prod_suc($id_prod,$id_suc){
        $strq = "SELECT pss.*, IFNULL(tsh.id,0) as id_traslado_serie
            from productos_sucursales_serie pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            where pss.activo=1 and productoid=$id_prod and sucursalid=$id_suc and disponible=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getsearchproductos_solicita_like($search){
        $strq = "SELECT p.*
            from productos p
            where referencia like '%2%' and (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_ventas_series_like($search){
        $strq = "SELECT * from productos where tipo=1 and (nombre like '%$search%' or idProducto like '%$search%') and (activo=1 or activo='Y') order by idProducto asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function searchRecargas_like($search){
        $strq = "SELECT * from recargas where codigo like '%$search%' AND tipo=1 and estatus=1"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function searchRentas_like($search){
        $strq = "SELECT * from servicios where clave like '%$search%' AND activo=1 or descripcion like '%$search%' AND activo=1"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerformapagoventa($idventa){

        //$strq = "SELECT * FROM `venta_erp_formaspagos` WHERE idventa=$idventa ORDER BY monto DESC LIMIT 1";
        $strq = "SELECT vfp.* 
                    FROM venta_erp_formaspagos as vfp
                    WHERE 
                        vfp.idventa='$idventa' AND 
                        monto=(SELECT max(vfp_m.monto) FROM venta_erp_formaspagos as vfp_m WHERE vfp_m.activo=1 AND vfp.idventa=vfp_m.idventa)  
                    ORDER BY `vfp`.`formapago`  ASC LIMIT 1";
        $query = $this->db->query($strq);
        $formapago='';
        foreach ($query->result() as $row) {
            $formapago=$row->formapago;
        }


        return $formapago; 
    }

    function infoventapre($id){
        $strq = "SELECT v.*,clif.razon_social, c.nombre as nom_cli, clif.idcliente
        FROM venta_erp as v 
        INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial 
        INNER JOIN clientes as c on c.id=clif.idcliente
        WHERE v.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function infocotizacion($id){
        $strq = "SELECT v.*,clif.razon_social 
        FROM cotizacion as v 
        INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial 
        WHERE v.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclikeunidadsat($search){
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search){
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclikeproveedor($search){
        $strq = "SELECT * from proveedores WHERE activo=1 and (nombre like '%".$search."%' or codigo like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function get_alertas_traspasos(){
        $strq="SELECT h.id,h.tipo,p.nombre,sx.name_suc AS name_sucx, sx.id_alias, sy.name_suc AS name_sucy, sy.id_alias as id_alias2
            FROM historial_alerta AS h
            LEFT JOIN sucursal AS sx ON sx.id=h.sucursaldestino
            LEFT JOIN sucursal AS sy ON sy.id=h.sucursalorigen
            LEFT JOIN personal AS p ON p.personalId=h.idpersonal
            WHERE h.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_alertas_total(){
        $strq="SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function genSelect($tabla){
        $query = $this->db->get($tabla);
        return $query->result();
    }
    function generararqueosinfo($suc,$fecha){
        $strq="SELECT MAX(ar.folio) as folio FROM arqueos as ar WHERE ar.sucursal=$suc AND ar.activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function generararqueosql($emple,$suc,$fechainicio,$hora_ini){
        if($emple>0){
            $where_emple=" and ven.personalid='$emple' ";
        }else{
            $where_emple="";
        }
        $strq="SELECT fmp.clave as clavefm, fmp.formapago_alias,ban.id as bancoid, ban.ban_clave,IFNULL(ban.name_ban,'') as name_ban, 
        /*CASE 
            WHEN vfmp.formapago != '01' THEN (SUM(vfmp.monto) - (SUM(vfmp.monto) - SUM(ven.total)) )
            ELSE (SUM(vfmp.monto) - (SUM(vfmp.monto) - SUM(ven.total)) ) 
        END AS monto */
        SUM(vfmp.monto) as monto
        FROM f_formapago as fmp
        INNER JOIN venta_erp_formaspagos as vfmp ON vfmp.formapago=fmp.clave
        INNER JOIN venta_erp as ven on ven.id=vfmp.idventa AND ven.sucursal=$suc $where_emple AND ven.reg BETWEEN '$fechainicio $hora_ini' AND '$fechainicio 23:59:59'
        LEFT JOIN bancos as ban on ban.id=vfmp.banco 
        WHERE ven.activo=1
        GROUP BY fmp.clave,ban.id;"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function info_arqueo_dll($idarqueo){
        $strq="SELECT arqdll.id,fform.formapago_alias,IFNULL(ban.name_ban,'') as banco,arqdll.monto,arqdll.monto_confirm,arqdll.clavefm,arqdll.bancoid
            FROM arqueos_dll arqdll
            INNER JOIN f_formapago fform ON fform.clave=arqdll.clavefm
            LEFT JOIN bancos as ban on ban.id=arqdll.bancoid
            WHERE arqdll.activo=1 AND arqdll.idarqueo=$idarqueo"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function fomapago($clave){
        $strq="SELECT *
                FROM f_formapago
            
                WHERE clave='$clave' "; 
        $query = $this->db->query($strq);
        $query=$query->row();
        return $query;
    }
    function getbanco($clave){
        $strq="SELECT *
                FROM bancos
            
                WHERE id='$clave' "; 
        $query = $this->db->query($strq);
        
        return $query;
    }
    function generareportexz($idarqueo,$tipo,$view){
        //http://localhost/semit_erp/Cortecaja?cortexz=1&idarqueo=5#tab21
        $result_ar =  $this->getselectwheren('arqueos',array('id'=>$idarqueo));
        $result_ar= $result_ar->row();
        $folioarqueo=$result_ar->folio;
        $fecha_arqueo=$result_ar->fecha;
        $ar_idsucursal=$result_ar->sucursal;
        $ar_reg=$result_ar->reg;
        $id_turno =$result_ar->id_turno;
            //===================================
                $res_turno =  $this->getselectwheren('turnos',array('id'=>$id_turno));
                $horat_inicio='00:00:00';
                $horat_fin='23:59:59';
                foreach ($res_turno->result() as $item_t) {
                    $horat_inicio= $item_t->hora;
                    if($item_t->hora_c!='' && $item_t->hora_c!=null && $item_t->hora_c!='null'){
                       $horat_fin=$item_t->hora_c; 
                    }
                    
                }
            //===================================
        //============================================================================================
        $result_suc =  $this->getselectwheren('sucursal',array('id'=>$result_ar->sucursal));
        $result_suc=$result_suc->row();
        $sucursal=$result_suc->name_suc;
        $domicilio_suc=$result_suc->domicilio;
        //============================================================================================
        $result_per =  $this->getselectwheren('personal',array('personalId'=>$result_ar->personal));
        $result_per=$result_per->row();
        $personal=$result_per->nombre;
        //============================================================================================
        $result_dll =  $this->info_arqueo_dll($idarqueo);
        $result_dll = $result_dll->result();
        $apartura_de_caja=0;//generar la consul para traer los cortes de las cajas del personal que pertenescan a una determinada sucursal (tabla 'turnos')
        //==========================================
            $this->updateCatalogo('turnos',array('estatus'=>2,'idpersonal_c'=>$result_ar->personal,'fecha_c'=>$fecha_arqueo,'hora_c'=>$this->hora_actual),array('estatus'=>1,'fecha'=>$fecha_arqueo,'sucursal'=>$ar_idsucursal));
            $this->limpiartabla();
        //==========================================
            $apartura_de_caja=$this->obtenersaldoencaja($ar_idsucursal,$fecha_arqueo,$id_turno);
            $apartura_de_caja=floatval($apartura_de_caja);
        //==========================================
            $monto_credito=$this->obtenermontoscredito($ar_idsucursal,$fecha_arqueo,$horat_inicio,$horat_fin);
        //==========================================
        $html='<style type="text/css">
        .c_blue{color:#152342;}
        .c_green{color:#c6d420;}
        .t_c{text-align:center;}
        .t_bold{font-weight:bold;}
        .t_size_12{font-size:12px;}
        .table_report{width: 100%;}
        .det_arti{font-size: 10px;}
        .texc{text-align: center;}
        .texe{text-align: end;}
        .sbordertop{border-top: 1px solid #152342;        }
        .sborderbottom{border-bottom: 1px solid #152342;        }
       </style>';
        if($tipo==0){
            $html.='<table border="0" aling="center" class="table_report  c_blue">
                        <tr><td class="texc"><b>Cierre de caja z</b></td></tr>
                        <tr><td class="texc"><b>SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</b></td></tr>
                        <tr><td class="texc"><b>'.$sucursal.'</b></td></tr>
                        <tr><td class="texc"><b>'.$domicilio_suc.'</b></td></tr>
                        <tr><td class="texc"><b>Cierre de cajas Z de la Sucursal '.$sucursal.'</b></td></tr>
                        <tr><td>Fecha de cierre: '.$ar_reg.'</td></tr>
                    </table>';
            $html.='<table border="0" class="table_report"><tbody><tr><td class="sborderbottom "><b>MOVIMIENTOS DE CAJAS</b></td></tr></tbody></table>';
            $html.='<table border="0" class="table_report">
                     <tr>
                        <td colspan="4">Estacion de trabajo: '.$sucursal.'</td>
                     </tr>
                     <tr>
                        <td colspan="4">Apertura de caja</td>
                     </tr>
                     <tr>
                        <td>EF</td>
                        <td>Efectivo</td>
                        <td></td>
                        <td>$'.number_format(($apartura_de_caja),2,'.',',').'</td>
                     </tr>
                     <tr>
                        <td></td>
                        <td></td>
                        <td>Total de concepto</td>
                        <td>$'.number_format(($apartura_de_caja),2,'.',',').'</td>
                     </tr>
                     <tr>
                        <td colspan="4"></td>
                     </tr>
                     <tr>
                        <td colspan="4">Venta</td>
                     </tr>
                     ';
                     $total_concepto=0;
                     foreach ($result_dll as $item) {
                        if($item->clavefm=='01'){
                            $monto_MV=$item->monto-$apartura_de_caja;
                        }else{
                            $monto_MV=$item->monto;
                        }
                        if($item->clavefm=='00'){

                        }else{
                            $total_concepto=$total_concepto+$monto_MV;
                        }
                        
                        $html.='<tr><td class="sborderbottom ">'.$item->formapago_alias.' '.$item->banco.'</td><td class="sborderbottom "></td><td class="sborderbottom ">$ '.number_format(($monto_MV),2,'.',',').'</td></tr>';
                     }
                     if($monto_credito>0){
                        $html.='<tr><td>Credito</td><td></td><td>$ '.number_format(($monto_credito),2,'.',',').'</td></tr>';
                     }
                     $html.='<tr><td></td><td class="sborderbottom ">Total Concepto: </td><td class="sborderbottom ">$ '.number_format($total_concepto,2,'.',',').'</td></tr>';
                     $html.='<tr><td></td><td class="sborderbottom ">Total estacion: </td><td class="sborderbottom ">$ '.number_format(($total_concepto+$apartura_de_caja),2,'.',',').'</td></tr>';
                     $html.='<tr><td></td><td class="sborderbottom ">GRAN TOTAL: </td><td class="sborderbottom ">$ '.number_format(($total_concepto+$apartura_de_caja),2,'.',',').'</td></tr>';

                     $html.='<tr><td></td><td></td><td></td></tr>';
                     $html.='<tr><td></td><td>Total sin apertura: </td><td>$ '.number_format($total_concepto,2,'.',',').'</td></tr>';
            $html.='</table>';
        }
        if($tipo==1){$html.='<table border="0" aling="center" class="table_report  c_blue">
                        <tr><td class="texc"><b>Corte de caja</b></td></tr>
                        <tr><td class="texc"><b>SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</b></td></tr>
                        <tr><td class="texc"><b>Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</b></td></tr>
                        <tr><td><b>No. corte: <span class="c_green">'.$folioarqueo.'</span></b></td></tr>
                        <tr><td><b>Empleado: <span class="c_green">'.$personal.'</span></b></td></tr>
                        <tr><td><b>Sucursal: <span class="c_green">'.$sucursal.'</span></b></td></tr>
                    </table>';

            $html.='<table border="0" class="table_report">
                        <tr><td colspan="2">Arqueo de caja</td><td></td></tr>
                        <tr><td width="20%"></td><td width="60%">';
                        $html.='<table border="0" class="table_report">';
                        $montog=0;
                        $montoxz=0;
                        foreach ($result_dll as $item) {
                            if($item->clavefm=='00'){

                            }else{
                                $montog=$montog+$item->monto;
                                $montoxz=$montoxz+$item->monto_confirm;
                            }
                            

                            $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($item->monto),2,'.',',').'</td></tr>';
                        }
                        $html.='<tr><td class="texe">Total con apertura de caja: </td><td>$ '.number_format(($montog),2,'.',',').'</td></tr>';
                        $html.='<tr><td class="texe">Total corte XZ: </td><td>$ '.number_format(($montoxz),2,'.',',').'</td></tr>';

                        $html.='<tr><td class="texe sbordertop">Dif. Arqueo vs corte: </td><td class="sbordertop">$ '.number_format(($montog-$montoxz),2,'.',',').'</td></tr>';
                        $html.='</table>';

                        $html.='</td width="20%"><td></td></tr>
                    </table>';

            $html.='<table border="0" class="table_report">
                        <tr><td colspan="2">Ingresado</td><td></td></tr>
                        <tr><td width="20%"></td><td width="60%">';
                        $html.='<table border="0" class="table_report">';
                        $total_corte_ciego=0;
                        foreach ($result_dll as $item) {
                            if($item->clavefm=='00'){

                            }else{
                                $total_corte_ciego=$total_corte_ciego+$item->monto_confirm;
                            }

                            $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($item->monto_confirm),2,'.',',').'</td></tr>';
                        }
                        $html.='<tr><td class="texe ">Total corte ciego: </td><td class="">$ '.number_format(($total_corte_ciego),2,'.',',').'</td></tr>';
                        $html.='</table>';

                        $html.='</td width="20%"><td></td></tr>
                    </table>';

            $html.='<table border="0" class="table_report">
                <tr><td colspan="2">Movimientos</td><td></td></tr>
                <tr><td width="20%"></td><td width="60%">';
                $html.='<table border="0" class="table_report">';
                $html.='<tr><td colspan="2"></td></tr>';
                //log_message('error','apartura_de_caja: '.$apartura_de_caja);
                $html.='<tr><td class="sborderbottom">APERTURA DE CAJA</td><td class="sborderbottom">$ '.number_format(($apartura_de_caja),2,'.',',').'</td></tr>';
                
                $html.='<tr><td colspan="2"></td></tr>';
                $html.='<tr><td colspan="2" class="sborderbottom">VENTAS</td></tr>';
                foreach ($result_dll as $item) {
                    
                    if($item->clavefm=='01'){
                        $monto_sin_caja=$item->monto-$apartura_de_caja;
                    }else{
                        $monto_sin_caja=$item->monto;
                    }
                    $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($monto_sin_caja),2,'.',',').'</td></tr>';
                }
                    $html.='<tr><td colspan="2" class="sborderbottom"></td></tr>';

                    $html.='<tr><td class="texe">Total sin apertura de caja: </td><td>$ '.number_format(($montog-$apartura_de_caja),2,'.',',').'</td></tr>';//AGREGA EL DESCUENTO DEL CORTE DE CAJA
                    $html.='<tr><td class="texe">Total corte XZ: </td><td>$ '.number_format(($montoxz),2,'.',',').'</td></tr>';
                    $html.='<tr><td class="texe sbordertop">Dif. Arqueo vs corte: </td><td class="sbordertop">$ '.number_format(($montoxz-($montog-$apartura_de_caja)),2,'.',',').'</td></tr>';

                $html.='</table>';

                $html.='</td width="20%"><td></td></tr>
            </table>';
                
            $html.='<table border="0" class="table_report"><tr><td class="sborderbottom "><b>DETALLES DE FORMA DE PAGO</b></td></tr></table>';

            $gran_total=0; $montototal=0; $montototal_fomp=0;
            $res_formp=$this->formaspagosvgroupby();
            foreach ($res_formp->result() as $itemfp) {
                $name_formapago=$itemfp->formapago_alias.' '.$itemfp->name_ban;
                $id_fompago=$itemfp->formapago;
                $id_banco=$itemfp->banco;
                $resul_pagos = $this->optenciondepagosventas($ar_idsucursal,$fecha_arqueo,$id_fompago,$id_banco,$horat_inicio,$horat_fin);
                $montototal_fomp=0;
                if($resul_pagos->num_rows()>0){
                    $html.='<table border="0" class="table_report"><tr><td class="sborderbottom "><b>'.$name_formapago.'</b></td></tr></table>';
                    $html.='<table border="0" class="table_report">';
                        if($id_fompago=='01'){
                            $html.='<tr><td>Efectivo</td><td></td><td></td><td>$ '.number_format($apartura_de_caja,2,'.',',').'</td></tr>';
                        }
                        foreach ($resul_pagos->result() as $itemfp_table) {
                            $montototal_fomp=$montototal_fomp+$itemfp_table->monto;
                            $montototal=$montototal+$itemfp_table->monto;
                            //log_message("error","montototal: ".$montototal);
                            $html.='<tr><td>'.$itemfp_table->tipo.'</td><td>'.$itemfp_table->razon_social.'</td><td>'.$itemfp_table->folio.'</td><td>$ '.number_format($itemfp_table->monto,2,'.',',').'</td></tr>';
                        }
                        $html.='<tr><td></td><td colspan="2" class="texe"><b>'.$name_formapago.'</b></td><td>$ '.number_format($montototal_fomp,2,'.',',').'</td></tr>
                        <!--<tr><td></td><td colspan="2" class="texe"><b>GRAN TOTAL</b></td><td>$ '.number_format($gran_total,2,'.',',').'</td></tr>-->';
                    $html.='</table>';
                    $gran_total=$gran_total+$montototal;
                }
            }
            //log_message("error","montototal_fomp: ".$montototal_fomp);
            $html.='<table border="0" class="table_report">
                    <tr>
                        <td ></td><td colspan="2" class="texe"><b>GRAN TOTAL</b></td><td>$ '.number_format($montototal,2,'.',',').'</td>
                    </tr>
                </table><br>';
            
            $html.='<table border="0" class="table_report det_arti"><tr><td class="sborderbottom "><b>DETALLES DE ARTÍCULOS</b></td></tr></table>';
            $resul_productos = $this->obtenciondeproductosventa($ar_idsucursal,$fecha_arqueo,$horat_inicio,$horat_fin);
            $html.='<table border="0" class="table_report">
                <thead>
                    <tr>
                        <th width="10%">Código</th>
                        <th width="45%">Descripción</th>
                        <th width="10%">Cant.</th>
                        <th width="15%">Monto</th>
                        <th width="20%">NombreGrupo</th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($resul_productos->result() as $itempr) {
                    $txt_pc="";
                    $precio_unitario=$itempr->precio_unitario;
                    if($itempr->incluye_iva==1 && $itempr->tipo!=1){ //iva diferente de recargas
                        $precio_con_iva =$itempr->precio_unitario*1.16;
                    }if($itempr->incluye_iva==0 && $itempr->tipo!=1){ //iva diferente de recargas
                        $precio_con_iva =$itempr->precio_unitario;
                    }

                    if($itempr->tipo==1){
                        $precio_con_iva =$itempr->precio_unitario;
                    }
                    
                    if($itempr->tipo_prod==0) { $catego=$itempr->categoria; $idProducto=$itempr->idProducto; $nombre=$itempr->nombre; }
                    if($itempr->tipo_prod==1) { $catego=$itempr->categoria; $idProducto=$itempr->idProducto; $nombre=$itempr->nombre." SERIE: ".$itempr->serie; }
                    if($itempr->tipo_prod==2) { $catego=$itempr->categoria; $idProducto=$itempr->idProducto; $nombre=$itempr->nombre." LOTE: ".$itempr->lote; }
                    if($itempr->tipo==1) { $catego="OXÍGENO"; $idProducto=$itempr->codigo; $nombre="Recarga de cilindro de oxígeno ".$itempr->codigo." de ".$itempr->capacidad." L"; }

                    if($itempr->tipo==3){
                        $idProducto=$itempr->numerocss;
                        $nombre=$itempr->descripcioncss;
                        $catego='';
                        if($itempr->tipocss==1){
                            $catego='Mantenimiento';
                        }
                        if($itempr->tipocss==2){
                            $catego='Corectivo';
                        }
                        if($itempr->tipocss==3){
                            $catego='Logística';
                        }
                    }
                    if($itempr->concentrador=="1"){
                        $txt_pc="<br><i style='font-size: 11px'>Incluye: ".$itempr->prod_concentra."</i>";
                    }
                    //se agregan rentas
                    if($itempr->tipo=="2"){
                        $txt_pc=$itempr->nombre_renta;
                        $catego="CONTRATO RENTA";
                        $idProducto = $itempr->cod_renta;
                        if($itempr->prod_concentra_renta!=""){
                            $txt_pc.="<br><i style='font-size: 11px'>Incluye: ".$itempr->prod_concentra_renta."</i>";
                        }
                    }

                    $html.='<tr>
                        <td width="10%">'.$idProducto.'</td>
                        <td width="45%">'.$nombre.' '.$txt_pc.'</td>
                        <td width="10%">'.$itempr->cantidad.'</td>
                        <td width="15%">$ '.number_format($precio_con_iva*$itempr->cantidad,2,'.',',').'</td>
                        <td width="20%">'.$catego.'</td>
                    </tr>';
                }
            $html.='</tbody></table>';
            $html.='';
            
        }
        if($view==0){
            echo $html;
        }else{
            return $html;
        }
        
    }
    function limpiartabla(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();

        unset($_SESSION['costr']);
        $_SESSION['costr']=array();
        unset($_SESSION['despr']);
        $_SESSION['despr']=array();

        unset($_SESSION['desr']);
        $_SESSION['desr']=array();
        unset($_SESSION['cantr']);
        $_SESSION['cantr']=array();
        unset($_SESSION['rentr']);
        $_SESSION['rentr']=array();

        unset($_SESSION['tipo']);
        $_SESSION['tipo']=array();
        unset($_SESSION['tipo_prod']);
        $_SESSION['tipo_prod']=array();
        unset($_SESSION['capac']);
        $_SESSION['capac']=array();
        unset($_SESSION['id_prod_suc']);
        $_SESSION['id_prod_suc']=array();
        unset($_SESSION['idprod_lote']);
        $_SESSION['idprod_lote']=array();
        unset($_SESSION['idprod_serie']);
        $_SESSION['idprod_serie']=array();

        //=============================
        unset($_SESSION['ser']);
        $_SESSION['ser']=array();
        unset($_SESSION['can_ser']);
        $_SESSION['can_ser']=array();
        unset($_SESSION['des_ser']);
        $_SESSION['des_ser']=array();
        unset($_SESSION['precio_ser']);
        $_SESSION['precio_ser']=array();
    }
    public function formaspagosvgroupby(){
        $strq = "SELECT vfp.formapago,vfp.banco,ffp.formapago_alias,IFNULL(ban.name_ban,'')as name_ban
                FROM venta_erp_formaspagos as vfp
                INNER JOIN f_formapago as ffp on ffp.clave=vfp.formapago
                LEFT JOIN bancos as ban on ban.id=vfp.banco
                GROUP BY vfp.formapago,vfp.banco";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function optenciondepagosventas($suc,$fecha,$fomp,$ban,$horat_inicio,$horat_fin){
        $strq = "SELECT 'venta' as tipo,clif.razon_social,v.folio,vfp.monto,vfp.formapago,vfp.banco
                FROM venta_erp as v
                INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial
                INNER JOIN venta_erp_formaspagos as vfp on vfp.idventa=v.id
                WHERE v.activo=1 AND v.sucursal='$suc' AND v.reg>='$fecha $horat_inicio' AND v.reg<='$fecha $horat_fin' AND vfp.formapago='$fomp' AND vfp.banco='$ban'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function obtenciondeproductosventa($suc,$fecha,$horat_inicio,$horat_fin){
        $strq="SELECT pro.idProducto,pro.nombre, pro.concentrador,vd.incluye_iva, vd.cantidad,vd.precio_unitario,cat.categoria, IFNULL(pro.tipo,0) as tipo_prod, vd.tipo, 
        IFNULL(r.codigo,'') as codigo, IFNULL(r.capacidad,0) as capacidad, 
        IFNULL(psl.lote,'') as lote, IFNULL(pss.serie,'') as serie,
        IFNULL(css.numero,'') as numerocss,
        IFNULL(css.descripcion,'') as descripcioncss,
        IFNULL(css.tipo,'') as tipocss,
        IFNULL(GROUP_CONCAT('Cant: ',pc.cantidad,' / ',pc.codigo_prod,' - ',p2.nombre SEPARATOR '<br>'),'') as prod_concentra,
        IFNULL(GROUP_CONCAT('Cant: ',pc2.cantidad,' / ',pc2.codigo_prod,' - ',p3.nombre SEPARATOR '<br>'),'') as prod_concentra_renta,
        IFNULL(sv.clave,'') as cod_renta,
        IFNULL(concat(sv.clave,' - ',sv.descripcion),'') as nombre_renta
        FROM venta_erp as v
        INNER JOIN venta_erp_detalle AS vd on vd.idventa=v.id
        LEFT JOIN productos as pro on pro.id=vd.idproducto and vd.tipo=0
        LEFT JOIN recargas AS r ON r.id=vd.idproducto and vd.tipo=1
        LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.activo=1 
        LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.activo=1
        LEFT JOIN productos_concentrador AS pc on pc.estatus=1 and pro.concentrador=1
        LEFT JOIN productos as p2 on p2.idProducto=pc.codigo_prod
        LEFT JOIN categoria as cat on cat.categoriaId=pro.categoria
        LEFT JOIN servicios as sv on sv.id=vd.id_serv and vd.tipo=2
        LEFT JOIN cat_servicios as css on css.id=vd.idproducto and vd.tipo=3

        LEFT JOIN rentas AS rt on rt.id_venta=v.id and rt.estatus!=3
        LEFT JOIN productos as prorta on prorta.id=rt.id_prod
        LEFT JOIN productos_concentrador AS pc2 on pc2.estatus=1 and prorta.concentrador=1
        LEFT JOIN productos as p3 on p3.idProducto=pc2.codigo_prod

        WHERE v.activo=1 AND vd.activo=1 AND v.sucursal='$suc' AND v.reg>='$fecha $horat_inicio' AND v.reg<='$fecha $horat_fin'
        GROUP by vd.id
            ";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function get_usuarios(){
         $strq="SELECT p.personalId,p.nombre,u.Usuario,p.puesto,p.fechabaja,p.motivo,per.nombre as perfil,s.name_suc,u.acceso,u.motivo_desactivar
            FROM personal AS p
            INNER JOIN usuarios AS u ON u.personalId=p.personalId
            INNER JOIN perfiles AS per ON per.perfilId=u.perfilId
            INNER JOIN sucursal AS s ON s.id=u.sucursal
            WHERE p.estatus=1 AND p.personalId!=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_clientes(){
         $strq="SELECT c.*, s.clave, e.estado as edo_name, cl.razon_social, cl.rfc, cl.cp as cp_fiscal, concat(rf.clave,' ',rf.descripcion) as reg_fiscal_txt, uf.uso_cfdi_text
            FROM clientes AS c
            INNER JOIN sucursal AS s ON s.id=c.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=c.id
            LEFT JOIN estado AS e ON e.id=c.estado
            LEFT JOIN f_regimenfiscal AS rf ON rf.clave=cl.RegimenFiscalReceptor
            left join f_uso_cfdi uf on uf.uso_cfdi=cl.uso_cfdi
            WHERE c.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    function obtenersaldoencaja($suc,$fecha,$id_turno){
        if($id_turno>0){
            $id_turno_w=" and id='$id_turno' ";
        }else{
            $id_turno_w='';
        }
        /*
        if($per>0){
            $where_per=" and t.idpersonal='$per' ";
        }else{
            $where_per="";
        }
        $strq = "SELECT t.monto,t.fecha, usu.sucursal 
                FROM turnos as t 
                INNER JOIN personal as per on per.personalId=t.idpersonal
                INNER JOIN usuarios as usu on usu.personalId=per.personalId
                WHERE usu.sucursal='$suc'  $where_per AND t.fecha='$fecha';";
        */
                //$strq="SELECT * FROM `turnos` WHERE sucursal='$suc' AND fecha='$fecha' and estatus=1"
        $strq="SELECT * FROM `turnos` WHERE sucursal='$suc' AND fecha='$fecha' $id_turno_w";
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->monto>0){
                $monto=$item->monto;
            }
        }

        return $monto;
    }
    function listacotizaciones($suc,$fechaactual){
        $strq="SELECT *,DATE_ADD(reg, INTERVAL 7 DAY) as finvigencia
                FROM `cotizacion` 
                WHERE sucursal='$suc' AND preventa=0 AND activo=1 AND '$fechaactual' < DATE_ADD(reg, INTERVAL 7 DAY)"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenermontosdevoluciones($suc,$fecha){
        $strq="SELECT SUM(monto) as total FROM bitacora_devoluciones WHERE idsucursal='$suc' AND fecha='$fecha'";
        //log_message('error',$strq);
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->total>0){
                $monto=$item->total;
            }
            // code...
        }
        return $monto;
    }
    function obtenermontoscredito($suc,$fecha,$h_ini,$h_fin){
        $strq="SELECT sum(total) as total FROM venta_erp as v 
                WHERE v.tipo_venta=1 AND v.activo=1 AND sucursal='$suc' AND reg BETWEEN '$fecha $h_ini' AND '$fecha $h_fin'";
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->total>0){
                $monto=$item->total;
            }
            // code...
        }
        return $monto;
    }
    function obtenerdatossucursalfacturaventa($idfactura){
        $strq="SELECT s.* FROM venta_fk_factura as vf 
            INNER JOIN venta_erp as v on v.id=vf.idventa
            INNER JOIN sucursal as s on s.id=v.sucursal
            WHERE vf.idfactura='$idfactura'";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenersucursalesfactura(){
        $strq="SELECT * FROM sucursal WHERE id=4 OR id=5 or id=3 or id=2";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenerultimosdigitosventa($idfactura){
        $strq="SELECT vfm.* 
        FROM venta_erp_formaspagos as vfm
        INNER JOIN venta_fk_factura as vfac on vfac.idventa=vfm.idventa
        WHERE vfm.ultimosdigitos!='' AND vfac.idfactura='$idfactura'";
        $query = $this->db->query($strq);
        return $query;
    }
    /*function searchallpros($search){
        $query=array(); $query_0=array(); $query_1=array(); $query_2=array(); $query_3=array();
        $strq0="SELECT p.id,p.idProducto as codigo,p.tipo, p.nombre, 0 as tipop, ps.precio, ps.incluye_iva, ps.iva, 
        (select sum(ps.stock) as stock from productos_sucursales as ps where ps.productoid=p.id and ps.activo=1) as stock,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=p.id and p.tipo=0 and ht.activo=1 and ht.tipo=0 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2),0) AS lote8,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id and p.tipo=1),0) as serie8,
        IFNULL(count(tsh.id),0) as traslado_serie_cant,
        IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
        IFNULL(t.status,2) as status_trasr,
        IFNULL(t2.status,2) as status_traslt
        from productos p
        left join productos_sucursales as ps on ps.productoid=p.id and ps.activo=1
        left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1
        left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1
        left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        left join traspasos t on t.id=tsh.idtraspasos
        left join traspasos t2 on t2.id=tlh.idtraspasos
        where p.referencia like '%2%' and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
        group by p.id
        order by idProducto asc";
        $query0 = $this->db->query($strq0);
        if($query0->num_rows()>0){
            $query_0=$query0->result_array();
        }

        $strq1="SELECT re.id,re.codigo,re.capacidad as nombre, 0 as tipo, 1 as tipop, re.preciov as precio, 0 as incluye_iva, 0 as iva,
        (select sum(r.stock) as stock from recargas as r where r.tipo=0 and r.estatus=1) as stock
        from recargas as re where re.codigo like '%$search%' AND re.tipo=1 and re.estatus=1";
        $query1 = $this->db->query($strq1);
        if($query1->num_rows()>0){
            $query_1=$query1->result_array();
        }
        
        $strq2="SELECT ser.id,ser.clave as codigo,ser.descripcion as nombre, 0 as tipo, 2 as tipop, 0 as incluye_iva, 1 as stock, precios1 as precio,0 as iva
        from servicios as ser where ser.clave like '%$search%' AND ser.activo=1 or ser.descripcion like '%$search%' AND ser.activo=1";
        $query2 = $this->db->query($strq2);
        if($query2->num_rows()>0){
            $query_2=$query2->result_array();
        }

        $strq3="SELECT cser.id,cser.numero as codigo,cser.descripcion as nombre, 0 as tipo, 3 as tipop, 0 as incluye_iva, 1 as stock, precio_siva as precio, iva
        from cat_servicios as cser where (cser.numero like '%$search%' or cser.descripcion like '%$search%')";
        $query3 = $this->db->query($strq3);
        if($query3->num_rows()>0){
            $query_3=$query3->result_array();
        }

        //$query = $this->db->query($strq);
        //$this->db->close();
        $final_results = array_merge($query_0, $query_1, $query_2, $query_3);
        //log_message("error", "final: ".json_encode($final_results));
        return json_encode($final_results);
    }*/

    function searchallpros($search){
        $strq = "select * from (SELECT p.id, p.idProducto AS codigo, p.tipo, p.nombre, 0 AS tipop, ps.precio, ps.incluye_iva, ps.iva,
            /*SUM(ps.stock) AS stock, */
            CASE 
                WHEN p.tipo != 1 and p.tipo != 2 THEN COALESCE(SUM(ps.stock), 0)
                ELSE 0 
            END AS stock,
            COALESCE(SUM(CASE WHEN ht.activo = 1 AND ht.tipo = 0 AND ht.status = 0 AND t.tipo = 0 AND ht.idsucursal_sale = ps.sucursalid THEN ht.cantidad ELSE 0 END), 0) AS traslado_stock_cant,
            CASE 
                WHEN p.tipo = 2 THEN COALESCE(SUM(psl.cantidad), 0)
                ELSE 0 
            END AS lote8,
            CASE 
                WHEN p.tipo = 1 THEN COALESCE(COUNT(pss.id), 0)
                ELSE 0 
            END AS serie8,
            COALESCE(COUNT(tsh.id), 0) AS traslado_serie_cant,
            COALESCE(SUM(tlh.cantidad), 0) AS traslado_lote_cant,
            COALESCE(t.status, 2) AS status_trasr,
            COALESCE(t2.status, 2) AS status_traslt,
            CASE 
                WHEN ps.iva = 0 and ps.incluye_iva = 1 THEN ps.precio
                WHEN ps.iva != 0 and ps.incluye_iva = 1 THEN ps.precio * 1.16
                
                WHEN ps.iva > 0 and ps.incluye_iva = 0 THEN ps.precio / 1.16
                WHEN ps.iva = 0 and ps.incluye_iva = 0 THEN ps.precio 
                ELSE 0 
            END AS precio_con_ivax
            FROM productos p
            LEFT JOIN productos_sucursales ps ON ps.productoid = p.id AND ps.activo = 1 and ps.sucursalid=8
            LEFT JOIN productos_sucursales_lote psl ON psl.productoid = p.id AND psl.activo = 1 AND p.tipo=2 and psl.sucursalid=8
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid = p.id AND pss.activo = 1 AND pss.disponible = 1 AND p.tipo=1 and psl.sucursalid=8
            LEFT JOIN traspasos_series_historial tsh ON tsh.idseries = pss.id AND tsh.activo = 1 and p. tipo=1
            LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes = psl.id AND tlh.activo = 1 and p.tipo=2
            LEFT JOIN traspasos t ON t.id = tsh.idtraspasos
            LEFT JOIN traspasos t2 ON t2.id = tlh.idtraspasos
            LEFT JOIN historial_transpasos ht ON ht.idproducto = p.id AND ht.activo = 1 AND ht.tipo = 0 AND (ht.status=0 AND t.tipo=0 OR ht.status=1 AND t.tipo=1) AND ht.rechazado != 1 and ht.idsucursal_sale = ps.sucursalid 
            where p.referencia like '%2%' and (p.nombre like '%$search%' or p.idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            GROUP BY p.id
        
        union
        SELECT re.id, re.codigo COLLATE utf8_general_ci, 0 as tipo, re.capacidad as nombre, 1 as tipop, re.preciov as precio, 0 as incluye_iva, 0 as iva,
        (select sum(r.stock) as stock from recargas as r where r.tipo=0 and r.estatus=1) as stock,
        0 AS traslado_stock_cant,
        0 AS lote8,
        0 AS serie8,
        0 AS traslado_serie_cant,
        0 AS traslado_lote_cant,
        2 AS status_trasr,
        2 AS status_traslt,
        re.preciov as precio_con_ivax
        from recargas as re 
        where re.codigo like '%$search%' AND re.tipo=1 and re.estatus=1

        union
        SELECT ser.id, ser.clave as codigo, 0 as tipo, ser.descripcion as nombre, 2 as tipop, precios1 as precio, 0 as incluye_iva, 0 as iva,
        1 as stock,
        0 AS traslado_stock_cant,
        0 AS lote8,
        0 AS serie8,
        0 AS traslado_serie_cant,
        0 AS traslado_lote_cant,
        2 AS status_trasr,
        2 AS status_traslt,
        precios1 as precio_con_ivax
        from servicios as ser 
        where ser.clave like '%$search%' AND ser.activo=1 or ser.descripcion like '%$search%' AND ser.activo=1

        union
        SELECT cser.id, cser.numero as codigo, 0 as tipo, cser.descripcion as nombre, 3 as tipop, precio_siva as precio, 0 as incluye_iva, iva, 1 as stock,
        0 AS traslado_stock_cant,
        0 AS lote8,
        0 AS serie8,
        0 AS traslado_serie_cant,
        0 AS traslado_lote_cant,
        2 AS status_trasr,
        2 AS status_traslt,
        CASE 
            WHEN iva = 1 THEN precio_siva / 1.16
            WHEN iva = 0 THEN precio_siva
            ELSE 0 
        END AS precio_con_ivax
        from cat_servicios as cser 
        where (cser.numero like '%$search%' or cser.descripcion like '%$search%')
    ) as datos order by codigo asc";

        $query = $this->db->query($strq);
        $final_results = $query->result_array();
        //log_message("error", "final: ".json_encode($final_results));
        return json_encode($final_results);
    }


    function info_ubicaciones_cartaporte($idfactura,$tipo){
        $strq="SELECT suc.id_alias,suc.clave,suc.name_suc,suc.domicilio,facu.TipoUbicacion,facu.NombreRFC,facu.RFCRemitenteDestinatario,facu.FechaHoraSalidaLlegada,facu.DistanciaRecorrida,facu.Calle,facu.NumeroExterior,facu.Localidad,facu.Municipio,facu.Estado,facu.CodigoPostal 
            FROM f_facturas_ubicaciones as facu
            LEFT JOIN sucursal as suc on suc.id=facu.id_ubicacion
            WHERE facu.FacturasId='$idfactura' and facu.TipoUbicacion='$tipo'";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtener_stock_series($pro,$sucu){
        $cant_inicial=0;
        $strq="SELECT pss.* 
                FROM productos_sucursales_serie as pss 
                left join compra_erp c on c.id=pss.idcompras and c.estatus=1 
                WHERE 
                    pss.activo=1 and 
                    pss.productoid='$pro' and 
                    pss.sucursalid='$sucu' and 
                    pss.disponible=1 AND 
                    ((pss.idcompras!=0 AND c.estatus=1) OR (pss.idcompras=0)) 
                group by id";
                $res_info1 = $this->db->query($strq);
                if ($res_info1->num_rows()>0) {
                    $cant_inicial=$res_info1->num_rows();
                }
        return $cant_inicial;
    }
    function obtener_stock_lotes($pro,$sucu){
        $cant_inicial=0;
        //$res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATAl[$i]->idproducto,'sucursalid'=>8));
        // la siguiente consulta es casi la misma misma que la que esta en ModelPoductos->get_suc_lotes, solo que se agrego directamente la conficion que se hace en php
        $strq="SELECT IFNULL(SUM(cantidad),0) as cantidad from(
                SELECT psl.*
                    FROM productos_sucursales_lote as psl
                    left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
                    left join traspasos t on t.id=tlh.idtraspasos and t.status=1
                    left join compra_erp c on c.id=psl.idcompra and c.estatus=1
                    WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid='$pro' and psl.sucursalid='$sucu' AND
                    ((psl.idcompra!=0 AND c.estatus=1) OR  psl.idcompra=0)
                    GROUP BY psl.id
            )as datos";
                //log_message('error','ingresar_inventario lotes:'.$strq);
        $res_info1 = $this->db->query($strq);
        foreach ($res_info1->result() as $itemi) {
            if($itemi->cantidad>0){
                $cant_inicial=$itemi->cantidad;
            }else{
                $cant_inicial=0;
            }
        }
        return $cant_inicial;
    }
    function obtener_stock_normal($pro,$sucu){
        $cant_inicial=0;
        $strq="SELECT 
                suc.id as idsuc, 
                suc.id_alias, 
                suc.name_suc,
                psuc.*, IFNULL(psuc.stock,0) as stock_ref,
                IFNULL(tsh.idseries,0) as id_serie_tras, 
                IFNULL(ht.cantidad,0) as cant_traslado,
                IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto='$pro' and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status = 0 AND t.tipo = 0 OR ht.status = 1 AND t.tipo = 1) and ht.rechazado!=1),0) as traslado_stock_cant
                FROM sucursal as suc
                LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='$pro'
                left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
                left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
                WHERE suc.activo=1 AND psuc.sucursalid='$sucu'";
                //log_message('error','ingresar_inventario lotes:'.$strq);
        $res_info1 = $this->db->query($strq);
        foreach ($res_info1->result() as $itemi) {
            if($itemi->stock_ref>0){
                $cant_inicial=intval($itemi->stock_ref)-$itemi->traslado_stock_cant;
            }else{
                $cant_inicial=0;
            }
        }
        return $cant_inicial;
    }
    function mes_letra($mes){
        $mes_l='';
        switch ($mes) {
            case '01':
                $mes_l='Ene';
                break;
            case '02':
                $mes_l='Feb';
                break;
            case '03':
                $mes_l='Mar';
                break;
            case '04':
                $mes_l='Abr';
                break;
            case '05':
                $mes_l='May';
                break;
            case '06':
                $mes_l='Jun';
                break;
            case '07':
                $mes_l='Jul';
                break;
            case '08':
                $mes_l='Ago';
                break;
            case '09':
                $mes_l='Sep';
                break;
            case '10':
                $mes_l='Oct';
                break;
            case '11':
                $mes_l='Nov';
                break;
            case '12':
                $mes_l='Dic';
                break;

            
            default:
                // code...
                break;
        }
        return $mes_l;
    }
    function factura_sercivios_updatepro($idfac){
        $strq="SELECT facd.sserviciosId,facd.Unidad,facd.servicioId,facd.Descripcion,facd.idproducto,pro.unidad_sat,pro.servicioId_sat
            FROM f_facturas_servicios as facd
            INNER JOIN productos as pro on pro.id=facd.idproducto
            WHERE facd.servicioId!=pro.servicioId_sat AND  facd.FacturasId='$idfac'";
        $query = $this->db->query($strq);
        return $query;
    }
    function lis_sol_descu($idsol){
        if($idsol>0){
            $w_sol=" and sd.id='$idsol' ";
        }else{
            $w_sol="";
        }
        $strq="SELECT sd.*,suc.name_suc,suc.id_alias,per.nombre
                FROM sol_des as sd
                INNER JOIN sucursal as suc on suc.id=sd.idsucursal
                INNER JOIN personal as per on per.personalId=sd.idpersonal
                WHERE sd.status=0 AND sd.activo=1 AND sd.reg>='$this->fecha_reciente 00:00:00' $w_sol";
        $query = $this->db->query($strq);
        return $query;
    }
}