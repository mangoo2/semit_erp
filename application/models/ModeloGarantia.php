<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloGarantia extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listGarantia($params){
        $columns = array( 
            0=>'g.id',
            1=>'g.reg',
            2=>'pe.nombre',
            3=>'g.motivo',
            4=>'s.name_suc',
            5=>'g.estatus',
            6=>'g.id_venta'
        );

        $columnsx = array( 
            0=>'g.id',
            1=>'DATE_FORMAT(g.reg, "%d/%m/%Y - %r" ) AS reg',
            2=>'pe.nombre AS personal',
            3=>'g.motivo',
            4=>'s.name_suc AS suc_solicita',
            5=>'g.estatus',
            6=>'s.activo',
            7=>'g.id_venta'
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('garantias g');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->join('personal pe', 'pe.personalId=g.idpersonal'); 
        $this->db->where("g.activo",1);
        
        if($params["perfilid"]==1 || $params["perfilid"]==4 || $params["perfilid"]==5){ //admin, compras o almacen
           
        }else{
            $this->db->where("id_origen",$params["sucursal"]);
        } 
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    function total_list_garatia($params){
        $columns = array( 
            0=>'g.id',
            1=>'g.reg',
            2=>'pe.nombre',
            3=>'g.motivo',
            4=>'s.name_suc',
            5=>'g.estatus',
            6=>'g.id_venta'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('garantias g');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->join('personal pe', 'pe.personalId=g.idpersonal'); 
        $this->db->where("g.activo",1);
        
        if($params["perfilid"]==1 || $params["perfilid"]==4 || $params["perfilid"]==5){ //admin o almacen
           
        }else{
            $this->db->where("id_origen",$params["sucursal"]);
        } 

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_detalle_solicitud($id){
        $this->db->select('gd.*, g.id_venta, g.motivo as motivo_all, g.motivo_rechazo as motivo_rechazo_all, g.estatus as estatus_garantia,g.reg, g.id_origen,
            pe.nombre AS personal, s.name_suc AS suc_solicita, pss.id as id_ps_ser, IFNULL(ps.id,0) as id_ps,
        p.tipo, concat(p.idProducto," / ",p.nombre) as producto, IFNULL(pss.serie,"") as serie, IFNULL(psl.id,0) as id_psl, IFNULL(psl.lote,"") as lote,
        v.reg as fecha_venta, c.nombre as cliente, concat(c.calle," C.P ",c.cp) as direccion, c.colonia, c.celular, c.correo, cf.rfc,
        IFNULL(og.id,0) as id_ord, IFNULL(og.fecha,"") as fecha_orden, IFNULL(og.observ,"") as observ, og.nom_solicita, og.dir_solicita, og.col_solicita, og.cel_solicita, og.tel_solicita, og.mail_solicita, og.rfc_solicita, og.nom_contacto, og.tel_contacto');
        $this->db->from("garantias_detalle gd");
        $this->db->join("garantias g","g.id=gd.id_garantia");
        $this->db->join("orden_garantia og","og.id_garantia=gd.id_garantia","left");
        $this->db->join('personal pe', 'pe.personalId=g.idpersonal');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->join("productos p","p.id=gd.idprod");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and gd.tipo=0 and ps.sucursalid=gd.id_origen","left");
        $this->db->join("productos_sucursales_serie pss","pss.id=gd.id_ps_ser and gd.tipo=1 and pss.sucursalid=gd.id_origen","left");
        $this->db->join("productos_sucursales_lote psl","psl.id=gd.id_ps_lot and gd.tipo=2 and psl.sucursalid=gd.id_origen","left");
        //$this->db->join("venta_erp_detalle vd","vd.id=g.id_venta and g.id_venta>0","left");
        $this->db->join("venta_erp v","v.id=g.id_venta and g.id_venta>0","left");
        $this->db->join("clientes c","c.id=v.id_cliente","left");
        $this->db->join("cliente_fiscales cf","cf.id=v.id_razonsocial","left");
        $this->db->where("gd.id_garantia",$id);
        $this->db->where("gd.activo",1);
        $this->db->group_by("gd.id");
        $query=$this->db->get(); 
        return $query->result();
    }

    /* **************************************/
    function get_listProdsGarantia($params){
        $columns = array( 
            0=>'gd.id_garantia',
            1=>'gd.cant',
            2=>'p.idProducto',
            3=>'p.nombre',
            4=>'pss.serie',
            5=>'psl.lote',
            6=>'s.name_suc',
            7=>'gd.estatus',
            8=>'gd.id'
        );

        $columnsx = array( 
            0=>'gd.id_garantia',
            1=>'gd.cant',
            2=>'p.idProducto',
            3=>'p.nombre',
            4=>'pss.serie',
            5=>'psl.lote',
            6=>'s.name_suc as suc_solicita',
            7=>'gd.estatus',
            8=>'gd.id',
            9=>'gd.tipo',
            10=>'gd.retorno',
            11=>'id_ps_ser',
            12=>'g.id_venta',
            13=>'gd.carta_provee'
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select." ,IFNULL(bg.tipo_mov,0) as tipo_mov, bg.id_traslado");
        $this->db->from('garantias_detalle gd');
        $this->db->join('garantias g', 'g.id=gd.id_garantia');
        $this->db->join('productos p', 'p.id=gd.idprod');
        $this->db->join('sucursal s', 's.id=gd.id_origen');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=gd.id_ps_ser and gd.tipo=1 and pss.sucursalid=gd.id_origen','left');
        $this->db->join('productos_sucursales_lote psl', 'psl.id=gd.id_ps_lot and gd.tipo=2 and psl.sucursalid=gd.id_origen','left');
        $this->db->join('bitacora_garantias bg', 'bg.id_gd=gd.id','left');
        $this->db->where("gd.activo",1);
        $this->db->where("gd.estatus",2); //aceptado

        if($params["perfilid"]==1 || $params["perfilid"]==4 || $params["perfilid"]==5){ //admin o almacen
           
        }else{
            $this->db->where("id_origen",$params["sucursal"]);
        } 
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    function total_list_Prodsgaratia($params){
        $columns = array( 
            0=>'gd.id_garantia',
            1=>'gd.cant',
            2=>'p.idProducto',
            3=>'p.nombre',
            4=>'pss.serie',
            5=>'psl.lote',
            6=>'s.name_suc',
            7=>'gd.estatus',
            8=>'gd.id'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('garantias_detalle gd');
        $this->db->join('garantias g', 'g.id=gd.id_garantia');
        $this->db->join('productos p', 'p.id=gd.idprod');
        $this->db->join('sucursal s', 's.id=gd.id_origen');
        $this->db->join('productos_sucursales_serie pss', 'pss.id=gd.id_ps_ser and gd.tipo=1 and pss.sucursalid=gd.id_origen','left');
        $this->db->join('productos_sucursales_lote psl', 'psl.id=gd.id_ps_lot and gd.tipo=2 and psl.sucursalid=gd.id_origen','left');
        $this->db->where("gd.activo",1);
        $this->db->where("gd.estatus",2); //aceptado
        
        if($params["perfilid"]==1 || $params["perfilid"]==4 || $params["perfilid"]==5){ //admin o almacen
           
        }else{
            $this->db->where("id_origen",$params["sucursal"]);
        } 

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getNotificaGarantia(){
        $this->db->select('g.*, pe.nombre AS personal, s.name_suc AS suc_solicita, s.id_alias');
        $this->db->from("garantias g");
        $this->db->join('personal pe', 'pe.personalId=g.idpersonal');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->where("g.estatus",1);
        $this->db->where("g.activo",1);
        $this->db->order_by("g.id","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEvideGarantiaVenta($id){
        $this->db->select('eg.*');
        $this->db->from("evidencia_garantia eg");
        $this->db->where("eg.estatus",1);
        $this->db->where("eg.id=".$id."");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEvideGarantia($id,$idventa){
        $this->db->select('eg.*');
        $this->db->from("evidencia_garantia eg");
        $this->db->where("eg.estatus",1);
        //$this->db->where("(eg.id_garantia=".$id." or eg.id_venta=".$idventa.")");
        $this->db->where("eg.id_garantia=".$id."");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getGarantiaOrden($id){
        $this->db->select('g.*, IFNULL(og.id,0) as id_og, og.fecha, og.observ, og.nom_solicita, og.dir_solicita, og.col_solicita, og.cel_solicita, og.tel_solicita, og.mail_solicita, og.rfc_solicita, og.nom_contacto, og.tel_contacto, s.name_suc AS suc_solicita, s.tel, s.domicilio');
        $this->db->from("garantias g");
        $this->db->join('orden_garantia og', 'og.id_garantia=g.id','left');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->where("g.id",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getPersonalGarantia($id){
        $this->db->select('IFNULL(pc.correo,"") as correo_comp, IFNULL(pa.correo,"") as correo_adm, s.name_suc, g.id, g.reg');
        $this->db->from("garantias g");
        $this->db->join('usuarios uc', 'uc.estatus=1 and uc.perfilid=4','left');
        $this->db->join('personal pc', 'pc.personalId=uc.personalId and pc.estatus=1','left');
        $this->db->join('sucursal s', 's.id=g.id_origen');
        $this->db->join('usuarios ua', 'ua.estatus=1 and ua.perfilId=1','left');    
        $this->db->join('personal pa', 'pa.personalId=ua.personalId and pa.estatus=1','left');
        $this->db->where('g.id',$id);
        $query=$this->db->get(); 
        return $query->result();
    }

}