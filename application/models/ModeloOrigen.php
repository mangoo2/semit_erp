<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloOrigen extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listado($params){
        $columns = array( 
            0=>'o.id',
			1=>'o.idorigen',
			2=>'est.descripcion AS num_estacion',
            3=>'est.clave_identificacion',
        );

        $columnsy = array( 
            0=>'o.id',
            1=>'o.idorigen',
            2=>'est.descripcion',
            3=>'est.clave_identificacion',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('origen o');
        $this->db->join('f_c_estaciones est', 'est.clave_identificacion=o.num_estacion','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsy as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsy[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_total($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.idorigen',
            2=>'est.descripcion',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('origen o');
        $this->db->join('f_c_estaciones est', 'est.clave_identificacion=o.num_estacion','left');
        $this->db->where(array('o.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }    

   

    public function get_estaciones_like($search){
        $strq = "SELECT * from f_c_estaciones WHERE activo=1 and (descripcion like '%".$search."%' OR clave_identificacion like '%".$search."%' )";
        $query = $this->db->query($strq);
        return $query->result();
    }
}