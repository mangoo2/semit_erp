<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloKardex extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->perfilid=$this->session->userdata('perfilid');
    }

    function search_all_productos($search){
        $strq = "SELECT p.*
            from productos p 
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y')
            group by p.id 
            order by idProducto asc"; 
        $query = $this->db->query($strq);
        return $query;
    }

    public function get_productos_kardex($params){
        $idpro=$params['id_producto'];
        $suc=$params["id_suc"];
        $fechi=$params["fechai"];
        $fechf=$params["fechaf"];
        $strq = "";
        $strq .= "select * from (";
        if($this->perfilid==1 || $this->perfilid==4){
            $strq .= "SELECT c.id, c.reg, c.fecha, c.distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, GROUP_CONCAT(IFNULL(pss.serie,'') SEPARATOR '<br>') as serie, GROUP_CONCAT(IFNULL(psl.lote,'') SEPARATOR '<br>') as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                p.incluye_iva_comp,p.iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=0 and cd.activo=1
            JOIN productos as p on p.id=cd.idproducto and cd.tipo=0
            JOIN productos_sucursales as prosuc on prosuc.productoid=cd.idproducto and prosuc.sucursalid=c.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.idcompras=cd.idcompra and pss.productoid=cd.idproducto and pss.activo=1 and cd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=cd.idcompra and psl.productoid=cd.idproducto and psl.activo=1 and cd.tipo=0 and p.tipo=2 
            JOIN sucursal as s on s.id=c.sucursal
            where c.activo=1 and cd.idproducto=".$params['id_producto']." and c.precompra=0
            /*and c.sucursal=".$params["id_suc"]."*/
            AND c.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            group by c.id ";
            $strq .= " union ";
        }
        /* $strq .= " union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, ht.cantidad, 'Dispersión' as tipo_mov, c.factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            'x' as cant_inicial, 'x' as cant_final, 0 as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            1 as consulta
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=0 and ht.activo=1 and ht.idsucursal_entra=".$params["id_suc"]."
            JOIN compra_erp as c on c.id=ht.id_compra and c.distribusion=1 and ht.id_compra>0
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.idcompra=ht.idproducto_lote and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            where t.activo=1 and ht.idproducto=".$params['id_producto']."
            AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59' ";*/
        
        $strq .= "SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva,vd.cantidad, 'Venta' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp,
            CASE 
                WHEN vd.incluye_iva = 1 THEN 16 
                ELSE 0
            END AS iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
            FROM venta_erp v
            JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=0
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as s on s.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where (v.activo=1 or v.activo=0) and vd.idproducto=".$params['id_producto']."
            and v.sucursal=".$params["id_suc"]."
            AND v.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59' and v.tipo_venta=0 

        union
            SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Solicitud Traslado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Solicitud de traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=".$params["id_suc"]." or ht.idsucursal_entra=".$params["id_suc"].")
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            JOIN sucursal as ss on ss.id=ht.idsucursal_sale
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_entra and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=1 and ht.idproducto=".$params['id_producto']." and ht.status=1 
            AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            group by ht.id
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=".$params["id_suc"]." or ht.idsucursal_entra=".$params["id_suc"].")
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and ht.idproducto_lote!=0 and psl.productoid=ht.idproducto and psl.activo=1 and ht.tipo=0 and p.tipo=2
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=1 and t.status=2 and ht.idproducto=".$params['id_producto']." and ht.status=2
            AND ht.fechaingreso BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            group by ht.id

        -- traslado por solicitud de venta --
        union
            SELECT t.id, ht.fechaingreso as reg, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Entrada a ',s.name_suc) as destino,
            concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '4_2' as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            s.name_suc
            FROM traspasos t
            JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.activo=1 and (ht.idsucursal_sale=".$params["id_suc"]." or ht.idsucursal_entra=".$params["id_suc"].")
            JOIN productos as p on p.id=ht.idproducto and ht.tipo=0
            JOIN sucursal as s on s.id=ht.idsucursal_entra
            left join traspasos_lotes_historial tlh on tlh.idtraspasos=t.id and tlh.activo=1
            left join traspasos_series_historial tsh on tsh.idtraspasos=t.id and tsh.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and ht.idproducto_series!=0 and pss.productoid=ht.idproducto and pss.activo=1 and ht.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=tlh.idlotes
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=ht.idproducto and prosuc.sucursalid=ht.idsucursal_sale and prosuc.activo=1
            where t.activo=1 and t.tipo=0 and t.status=2 and ht.idproducto=".$params['id_producto']." and ht.status=2
            AND ht.fechaingreso BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            group by ht.id

        union
            SELECT g.id, g.reg, DATE_FORMAT(g.reg,'%d-%m-%Y') as fecha, '0' as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, gd.cant as cantidad, 'Garantia' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            gd.cant_ini as cant_inicial, (gd.cant_ini-gd.cant) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            5 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM garantias g
            JOIN garantias_detalle as gd on gd.id_garantia=g.id and gd.tipo>=0 and gd.activo=1 and gd.estatus=2
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales as prosuc on prosuc.productoid=gd.idprod and prosuc.sucursalid=gd.id_origen and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.productoid=gd.idprod and pss.id=gd.id_ps_ser and pss.activo=1 and gd.tipo=1 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.productoid=gd.idprod and psl.id=gd.id_ps_lot and psl.activo=1 and gd.tipo=2 and p.tipo=2
            where g.activo=1 and g.id_venta=0 and gd.idprod=".$params['id_producto']."
            and g.id_origen=".$params["id_suc"]."
            AND g.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'

        union
            SELECT bg.id, bg.fecha as reg, DATE_FORMAT(bg.fecha,'%d-%m-%Y') as fecha, gd.cant as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, gd.cant as cantidad, 'Retorno de garantía' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Garantía de ',ss.name_suc) as destino,
            (gd.cant_ini-gd.cant) as cant_inicial, (gd.cant_ini) as cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            '6_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_garantias bg
            JOIN garantias_detalle as gd on gd.id=bg.id_gd and gd.activo=1 and gd.estatus=2 and gd.retorno=1
            JOIN productos as p on p.id=gd.idprod
            JOIN sucursal as ss on ss.id=gd.id_origen
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=gd.id_ps_ser and gd.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=gd.id_ps_lot and gd.tipo=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=gd.id_origen and pros.productoid=gd.idprod and gd.tipo=0
            where gd.idprod=".$params['id_producto']."
            and gd.id_origen=".$params["id_suc"]."
            AND bg.fecha BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'

        union
            SELECT ba.id, ba.fecha_reg as reg, DATE_FORMAT(ba.fecha_reg,'%d-%m-%Y') as fecha, ba.cantidad as distribusion, p.idProducto, p.tipo, p.nombre, pros.incluye_iva, pros.iva, ba.cantidad_ajuste as cantidad, 'Ajuste de producto' as tipo_mov, '' as factura, concat('<b>De serie:</b> ',ba.num_serie,' <b>por serie:</b> ',ba.serie_ajuste) as serie, IFNULL(psl.lote,'') as lote, concat('Producto de ',ss.name_suc) as destino,
            ba.cantidad as cant_inicial, ba.cant_final, '' as cant_inicial_d, '' as cant_final_d, pros.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            6 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_ajustes ba
            JOIN productos as p on p.id=ba.id_producto
            JOIN sucursal as ss on ss.id=ba.id_sucursal
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ba.id_ps and ba.tipo_prod=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ba.id_ps and ba.tipo_prod=2
            LEFT JOIN productos_sucursales as pros on pros.sucursalid=ba.id_sucursal and pros.productoid=ba.id_producto
            where ba.id_producto=".$params['id_producto']."
            and ba.id_sucursal=".$params["id_suc"]."
            and ba.tipo_recarga=0
            AND ba.fecha_reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59' 
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Venta insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                7 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal and prosuc.activo=1
                WHERE v.activo=1 AND v.sucursal='$suc' and pro.id='$idpro' AND v.reg BETWEEN '$fechi 00:00:00' AND '$fechf 23:59:59'
            union
                SELECT 
                bpc.id,
                v.reg,
                DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha,
                v.folio as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Venta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini+bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_2' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN venta_erp as v on v.id=bpc.id_venta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=v.sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=v.sucursal
                WHERE v.activo=0 AND v.sucursal='$suc' and pro.id='$idpro' AND v.delete_reg BETWEEN '$fechi 00:00:00' AND '$fechf 23:59:59'

            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Renta concentrador (insumo)' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus != 3 AND r.id_sucursal='$suc' and pro.id='$idpro' AND r.fecha_reg BETWEEN '$fechi 00:00:00' AND '$fechf 23:59:59'
            union
                SELECT 
                bpc.id,
                r.fecha_reg as reg,
                DATE_FORMAT(r.fecha_reg,'%d-%m-%Y') as fecha,
                bpc.cantidad as distribusion,
                pro.idProducto, 
                pro.tipo,
                pro.nombre, prosuc.incluye_iva, prosuc.iva,
                bpc.cantidad,
                'Devolución insumos concentrador' as tipo_mov,
                '' as factura,
                '' as serie,
                '' as lote,
                concat('Renta de ',s.name_suc) as destino,
                bpc.cant_ini as cant_inicial,
                (bpc.cant_ini-bpc.cantidad) as cant_final,'' as cant_inicial_d, '' as cant_final_d,
                pro.costo_compra as precio_unitario,
                pro.incluye_iva_comp,
                pro.iva_comp,
                '7_3_1' as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
                FROM bitacora_prods_conce as bpc
                INNER JOIN rentas as r on r.id=bpc.id_renta
                INNER JOIN productos as pro on pro.id=bpc.id_prod
                JOIN sucursal as s on s.id=r.id_sucursal
                JOIN productos_sucursales as prosuc on prosuc.productoid=bpc.id_prod and prosuc.sucursalid=r.id_sucursal and prosuc.activo=1
                WHERE r.estatus = 3 AND r.id_sucursal='$suc' and pro.id='$idpro' AND r.fecha_reg BETWEEN '$fechi 00:00:00' AND '$fechf 23:59:59'
                ";

        $strq.="union
            SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=0 and vd.activo=1
            JOIN productos as p on p.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            JOIN productos_sucursales as prosuc on prosuc.productoid=vd.idproducto and prosuc.sucursalid=v.sucursal and prosuc.activo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.productoid=vd.idproducto and pss.activo=1 and vd.tipo=0 and p.tipo=1
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.productoid=vd.idproducto and psl.activo=1 and vd.tipo=0 and p.tipo=2
            where vd.idproducto=".$params['id_producto']."
            and v.sucursal=".$params["id_suc"]."
            AND b.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'

            union
            SELECT m.id, bi.fecha_reg as reg, DATE_FORMAT(bi.fecha_reg,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Interno' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. interno de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            9 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM mtto_interno m
            JOIN bitacora_insumos_mttosint as bi on bi.id_mtto=m.id and bi.estatus=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=".$params['id_producto']."
            and m.id_sucursal=".$params["id_suc"]."
            AND bi.fecha_reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'

            union
            SELECT m.id, m.fecha_serv as reg, DATE_FORMAT(m.fecha_serv,'%d-%m-%Y') as fecha, 0 as distribusion, p.idProducto, p.tipo, p.nombre, prosuc.incluye_iva, prosuc.iva, bi.cantidad, 'Mtto. Externo' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Mtto. externo de ',ss.name_suc) as destino,
            bi.cant_ini as cant_inicial, (bi.cant_ini-bi.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, prosuc.precio as precio_unitario,
            p.incluye_iva_comp,p.iva_comp,
            10 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM mtto_externo m
            JOIN bitacora_ins_servs_ventas as bi on bi.id_mtto=m.id and bi.asignado=1
            JOIN venta_erp as v on v.id=m.id_venta and v.activo=1
            JOIN productos as p on p.id=bi.id_insumo
            JOIN productos_sucursales as prosuc on prosuc.id=bi.id_prod_suc
            JOIN sucursal as ss on ss.id=m.id_sucursal
            where m.activo=1
            and bi.id_insumo=".$params['id_producto']."
            and m.id_sucursal=".$params["id_suc"]."
            AND m.fecha_serv BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
        ) as datos ORDER by reg asc";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_oxigeno_kardex($params){ //NO SUBIR, AUN EN PROCESO, MARCARÁ ERROR EL KARDEX SI SE SUBE
        $strq = "";
        $strq .= "select * from (";
        if($this->perfilid==1 || $this->perfilid==4){
            $strq .= "SELECT c.id, c.reg, c.fecha, c.distribusion, r.codigo as idProducto, 'r' as tipo, r.capacidad as nombre, 1 as incluye_iva, 0 as iva, cd.cantidad, 'Compra' as tipo_mov, c.factura, '' as serie, '' as lote, 'Entrada a almacén general' as destino,
                cd.cant_inicial, (cd.cant_inicial+cd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, cd.precio_unitario,
                1 as incluye_iva_comp, 0 as iva_comp,
                0 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
                s.name_suc
            FROM compra_erp c
            JOIN compra_erp_detalle as cd on cd.idcompra=c.id and cd.tipo=1 and cd.activo=1
            JOIN recargas as r on r.id=cd.idproducto
            JOIN sucursal as s on s.id=c.sucursal
            where c.activo=1 and cd.idproducto=12 and c.precompra=0
            AND c.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
            group by c.id ";
            $strq .= " union ";
        }
        $strq .= "
        /*SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, ht.cantidad, 'Dispersión' as tipo_mov, c.factura, '' as serie, '' as lote, concat('Entrada a ',s.name_suc) as destino,
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.idsucursal_entra=".$params["id_suc"]."
        JOIN compra_erp as c on c.id=ht.id_compra and c.distribusion=1 and ht.id_compra>0
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        where t.activo=1 and ht.idproducto=12
        AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
        union */

        SELECT v.folio as id, v.reg, DATE_FORMAT(v.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, vd.cantidad, 'Venta' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Venta de ',s.name_suc) as destino,
            vd.cant_inicial, vd.cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            vd.incluye_iva as incluye_iva_comp, 0 as iva_comp,
            2 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            s.name_suc
        FROM venta_erp v
        JOIN venta_erp_detalle as vd on vd.idventa=v.id and vd.activo=1 and vd.tipo=1
        JOIN recargas as r on r.id=vd.idproducto 
        JOIN sucursal as s on s.id=v.sucursal
        where v.activo=1 and vd.tipo=1
        and v.sucursal=".$params["id_suc"]."
        AND v.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59' and v.tipo_venta=0 

        union
        SELECT t.id, t.reg, DATE_FORMAT(t.reg,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Traslado de ',ss.name_suc, ' a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.precioc as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            3 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and ht.status=1 and (ht.idsucursal_sale=".$params["id_suc"]." or ht.idsucursal_entra=".$params["id_suc"].")
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=r.id and t.status=1 and ht.status=1 
        AND t.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
        group by ht.id

        union
        SELECT t.id, ht.fechaingreso, DATE_FORMAT(ht.fechaingreso,'%d-%m-%Y') as fecha, '0' as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, ht.cantidad, 'Traslado Aceptado' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Entrada a ',s.name_suc) as destino,
        concat('Origen: ',ht.cant_ini) as cant_inicial, concat('Origen: ',ht.cant_ini-ht.cantidad) as cant_final,
            concat('Destino: ',ht.cant_ini_d) as cant_inicial_d, concat('Destino: ',ht.cant_ini_d + ht.cantidad) as cant_final_d, r.precioc as precio_unitario,
            1 incluye_iva_comp, 0 as iva_comp,
            4 as consulta, ht.idsucursal_sale, ht.idsucursal_entra,
            ss.name_suc
        FROM traspasos t
        JOIN historial_transpasos as ht on ht.idtranspasos=t.id and ht.tipo=1 and ht.activo=1 and ht.id_compra=0 and ht.status=2 and (ht.idsucursal_sale=".$params["id_suc"]." or ht.idsucursal_entra=".$params["id_suc"].") 
        JOIN recargas as r on r.id=ht.idproducto and ht.tipo=1
        JOIN sucursal as s on s.id=ht.idsucursal_entra
        JOIN sucursal as ss on ss.id=ht.idsucursal_sale
        where t.activo=1 and t.tipo=1 and ht.idproducto=r.id and t.status=2 and ht.status=2
        AND ht.fechaingreso BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'
        group by ht.id

        -- devolucion de oxigeno, acá me quedo para integrar al kardex el reporte de devoluciones
        union
        SELECT b.id, b.reg, DATE_FORMAT(b.reg,'%d-%m-%Y') as fecha, v.folio as distribusion, r.codigo as idProducto, 'r' as tipo, concat(r.capacidad,' L') as nombre, 1 as incluye_iva, 0 as iva, bd.cantidad, 'Devolución' as tipo_mov, '' as factura, '' as serie, '' as lote, concat('Devolución venta #',v.folio, ' de ',ss.name_suc) as destino,
            bd.cant_ini as cant_inicial, (bd.cant_ini+bd.cantidad) as cant_final, '' as cant_inicial_d, '' as cant_final_d, vd.precio_unitario,
            1 as incluye_iva_comp, 0 as iva_comp,
            8 as consulta, 0 idsucursal_sale, 0 idsucursal_entra,
            ss.name_suc
            FROM bitacora_devoluciones b
            JOIN bitacora_devoluciones_detalles as bd on bd.id_bitacora=b.id
            JOIN venta_erp as v on v.id=b.idventa 
            JOIN venta_erp_detalle as vd on vd.id=bd.id_venta_det and vd.tipo=1 and vd.activo=1
            JOIN recargas as r on r.id=vd.idproducto
            JOIN sucursal as ss on ss.id=v.sucursal
            where vd.tipo=1
            and v.sucursal=".$params["id_suc"]."
            AND b.reg BETWEEN '".$params["fechai"]." 00:00:00' AND '".$params["fechaf"]." 23:59:59'

        ) as datos group by id ORDER by reg asc";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_all_stock_lote($id,$id_suc){
        $strq = "SELECT p.id,p.nombre,0 as tipo, 
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=$id and sucursalid=$id_suc),0) AS stock_lote,
        IFNULL(tlh.cantidad,0) as tras_stock_lote
        FROM productos as p
        left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$id_suc and psl.cantidad>0
        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
        where p.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

    public function get_all_stock_serie($id,$id_suc){
        $strq = "SELECT p.id,p.nombre,0 as tipo, count(tsh.id) as tot_tras,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=$id and sucursalid=$id_suc),0) AS stock_serie
        FROM productos as p
        left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$id_suc
        left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
        where p.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

    public function ventas_detalles($idventa){
        $strq = "SELECT
            pro.id as id_prod,
            pro.idProducto,
            vd.cantidad,
            pro.nombre,
            pro.tipo as tipo_prod,
            IFNULL(pro2.id,0) as id_recarga,
            IFNULL(pro2.codigo,'') as codigo,
            IFNULL(pro2.capacidad,'') as capacidad,
            vd.precio_unitario,
            vd.descuento,
            vd.tipo,
            IFNULL(psl.lote,'') as lote,
            IFNULL(pss.serie,'') as serie,
            IFNULL(psl.id,'') as id_ps_lot,
            IFNULL(pss.id,'') as id_ps_ser,
            vd.id as id_vd,
            vd.id_ps_lote,
            vd.tipo,
            vd.id_ps_serie,
            vd.solicita_garantia,
            IFNULL(g.motivo,'') as motivo,
            IFNULL(s.clave,'') as clave,
            IFNULL(s.descripcion,'') as desc_serv,
            og.nom_solicita, og.dir_solicita, og.col_solicita, og.cel_solicita, og.tel_solicita, og.mail_solicita, og.rfc_solicita, og.nom_contacto, og.tel_contacto
        FROM venta_erp_detalle as vd 
        left JOIN productos as pro on pro.id=vd.idproducto
        left join productos_sucursales_lote psl on psl.id=vd.id_ps_lote
        left join productos_sucursales_serie pss on pss.id=vd.id_ps_serie
        left JOIN recargas as pro2 on pro2.id=vd.idproducto and vd.tipo=1
        left JOIN garantias as g on g.id_venta=vd.idventa and g.activo=1
        left JOIN orden_garantia og on og.id_garantia=g.id
        left JOIN servicios as s on s.id=vd.id_serv and vd.tipo=2
        WHERE vd.idventa=$idventa and (vd.tipo=0 or vd.tipo=1 or vd.tipo=2)";
        $query = $this->db->query($strq);
        return $query;
    }
    public function ventas_detalles_servicio($idventa){
        $strq = "SELECT vd.id,vd.cantidad,cats.descripcion,vd.precio_unitario,vd.incluye_iva,vd.descuento,cats.numero,cats.Unidad,cats.servicioId
        FROM venta_erp_detalle as vd
        INNER JOIN cat_servicios as cats on cats.id=vd.idproducto
        WHERE vd.activo=1 AND vd.tipo=3 AND vd.idventa='$idventa';";
        $query = $this->db->query($strq);
        return $query;
    }


    public function ventas_detalles_renta($idventa){
        $strq = "SELECT vd.id,vd.cantidad,s.clave,s.descripcion,vd.precio_unitario,vd.incluye_iva,vd.descuento,vd.tipo,vd.periodo
            FROM venta_erp_detalle as vd
            INNER JOIN rentas as r on r.id_venta=vd.idventa
            INNER JOIN servicios as s on s.id=vd.id_serv and vd.tipo=2
            WHERE vd.activo=1 AND vd.tipo=2 AND vd.idventa='$idventa';";
        $query = $this->db->query($strq);
        return $query;
    }

    public function ventas_formaspagos($idventa){
        $strq = "SELECT 
                        ffp.clave,
                        ffp.formapago_text,
                        vfp.monto,
                        vfp.ultimosdigitos
                FROM venta_erp_formaspagos as vfp 
                INNER JOIN f_formapago as ffp on ffp.clave=vfp.formapago 
                WHERE vfp.idventa=$idventa and vfp.activo=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
}