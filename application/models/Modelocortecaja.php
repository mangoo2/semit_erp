<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Modelocortecaja extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    //SELECT ar.folio, suc.name_suc, ar.folio,ar.fecha FROM arqueos as ar INNER JOIN sucursal as suc on suc.id=ar.sucursal WHERE ar.activo AND ar.sucursal=2

    function get_list($params){
        $suc=$params['suc'];
        $columns = array( 
            0=>'ar.folio',
            1=>'suc.name_suc',
            2=>"DATE_FORMAT(ar.reg, '%Y-%m-%d') reg",
            3=>'tu.idpersonal',
            4=>'tu.hora',
            5=>'tu.idpersonal_c',
            6=>'tu.hora_c',
            7=>'ar.id',
            8=>'pera.nombre nom_a',
            9=>'perc.nombre nom_c'
        );
        $columns_a = array( 
            0=>'ar.folio',
            1=>'suc.name_suc',
            2=>'ar.reg',
            3=>'tu.idpersonal',
            4=>'tu.hora',
            5=>'tu.idpersonal_c',
            6=>'tu.hora_c',
            7=>'ar.id',
            8=>'pera.nombre',
            9=>'perc.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('arqueos ar');
        $this->db->join('sucursal suc','suc.id=ar.sucursal');
        $this->db->join('turnos tu','tu.id=ar.id_turno','left');
        $this->db->join('personal pera','pera.personalId=tu.idpersonal','left');
        $this->db->join('personal perc','perc.personalId=tu.idpersonal_c','left');
        if($suc>0){
            $this->db->where(array('ar.sucursal'=>$suc));
        }
        
        $this->db->where(array('ar.activo'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_a as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_a[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $suc=$params['suc'];
        $columns = array( 
            0=>'ar.folio',
            1=>'suc.name_suc',
            2=>'ar.reg',
            3=>'tu.idpersonal',
            4=>'tu.hora',
            5=>'tu.idpersonal_c',
            6=>'tu.hora_c',
            7=>'ar.id',
            8=>'pera.nombre',
            9=>'perc.nombre'
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('arqueos ar');
        $this->db->join('sucursal suc','suc.id=ar.sucursal');
        $this->db->join('turnos tu','tu.id=ar.id_turno','left');
        $this->db->join('personal pera','pera.personalId=tu.idpersonal','left');
        $this->db->join('personal perc','perc.personalId=tu.idpersonal_c','left');
        if($suc>0){
            $this->db->where(array('ar.sucursal'=>$suc));
        }
        
        $this->db->where(array('ar.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    

}