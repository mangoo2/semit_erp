<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturass extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getfacturas($params){
        //log_message('error', json_encode($params));
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $metodopago=$params['metodopago'];
        $tcfdi=$params['tcfdi'];
        $columns = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.Rfc',
            4=>'fac.total',
            5=>'fac.Estado',
            6=>'fac.fechatimbre',
            7=>'fac.FormaPago',
            8=>'fac.rutaXml',
            9=>'fac.rutaAcuseCancelacion',
            10=>'fac.cadenaoriginal',
            11=>'per.nombre',
            12=>'fac.uuid',
            14=>'(SELECT COUNT(*) FROM f_facturas as facn WHERE facn.f_r_uuid=fac.uuid AND facn.f_r_tipo="01" AND facn.Estado=1) as totalnc',
            15=>'fac.f_relacion',
            16=>'fac.f_r_tipo',
            17=>'fac.TipoComprobante',
            18=>'fac.doc_acuse_cancelacion',
            19=>'vent.idventa',
            20=>'fac.Nombre_alias',
            21=>'fac.cartaporte',
            22=>'fac.tim',
            23=>"CASE 
                    WHEN fac.cartaporte > 0 THEN (
                        SELECT tra.id FROM traspasos as tra WHERE tra.Idfactura=fac.FacturasId
                    )
                    ELSE ''
                END AS id_tras"
        );
        $columnsss = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.Rfc',
            4=>'fac.total',
            5=>'fac.Estado',
            6=>'fac.fechatimbre',
            7=>'fac.FormaPago',
            8=>'per.nombre',
            10=>'fac.ordenCompra'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_facturas fac');
        $this->db->join('personal per', 'per.personalId = fac.usuario_id','left');
        //$this->db->join('f_complementopago_documento comp', 'comp.facturasId = fac.FacturasId','left');
        $this->db->join('clientes cl', 'cl.id = fac.clienteId','left');
        $this->db->join('venta_fk_factura vent', 'vent.idfactura = fac.FacturasId','left');

        if($personal>0){
            $this->db->where(array('fac.creada_sesionId'=>$personal));
        }
        if($cliente>0){
            $this->db->where(array('clienteId'=>$cliente));
            /*
            $this->db->group_start();
            $this->db->or_where(array('fac.Clientes_ClientesId'=>$cliente));
            $list_rfcs=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$cliente,'activo'=>1));
            foreach ($list_rfcs->result() as $item) {
                if($item->rfc!='XAXX010101000'){
                    $this->db->or_like(array('fac.Rfc'=>$item->rfc));
                }
            }
            $this->db->group_end();
            */
        }
        if($finicio!=''){
            $this->db->where("(fac.fechatimbre >='$finicio 00:00:00' or fac.fechatimbre='0000-00-00 00:00:00')");
        }
        if($ffin!=''){
            $this->db->where(array('fac.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        
        if($metodopago>0){
            if($metodopago==1){
                $this->db->where(array('fac.FormaPago'=>'PUE'));
            }
            if($metodopago==2){
                $this->db->where(array('fac.FormaPago'=>'PPD'));
            }
        }
        if($tcfdi>0){
            if($tcfdi==1){
                $this->db->where("fac.f_relacion=0 and fac.f_r_tipo='0'");
                $this->db->where(array('fac.cartaporte'=>0));
            }
            if($tcfdi==2){
                $this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'01'));
            }
            if($tcfdi==3){
                $this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'04'));
            }
            if($tcfdi==4){
                $this->db->where(array('fac.f_relacion'=>0,'fac.f_r_tipo'=>'07'));
            }
            if($tcfdi==5){
                $this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'07','fac.TipoComprobante'=>'I'));
            }
            if($tcfdi==6){
                $this->db->where(array('fac.f_relacion'=>1,'fac.f_r_tipo'=>'07','fac.TipoComprobante'=>'E'));
            }
            if($tcfdi==7){
                $this->db->where(array('fac.cartaporte'=>1));
            }
        }
        $this->db->where(array('fac.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        $this->db->group_by("fac.FacturasId");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $metodopago=$params['metodopago'];
        $tcfdi=$params['tcfdi'];
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'FormaPago',
            8=>'rutaXml',
            9=>'ordenCompra',
            10=>'Nombre_alias'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('*');
        $this->db->from('f_facturas');
        if($personal>0){
            $this->db->where(array('creada_sesionId'=>$personal));
        }
        if($cliente>0){
            $this->db->where(array('clienteId'=>$cliente));
            /*
            $this->db->group_start();
            $this->db->or_where(array('Clientes_ClientesId'=>$cliente));
            $list_rfcs=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$cliente,'activo'=>1));
            foreach ($list_rfcs->result() as $item) {
                if($item->rfc!='XAXX010101000'){
                    $this->db->or_like(array('Rfc'=>$item->rfc));
                }
            }
            $this->db->group_end();
            */
        }
        if($finicio!=''){
            $this->db->where("(fechatimbre >='$finicio 00:00:00' or fechatimbre='0000-00-00 00:00:00')");
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        
        $this->db->where(array('activo'=>1));
        if($tcfdi>0){
            if($tcfdi==1){
                $this->db->where("f_relacion=0 and f_r_tipo='0' ");
                $this->db->where(array('cartaporte'=>0));
            }
            if($tcfdi==2){
                $this->db->where(array('f_relacion'=>1,'f_r_tipo'=>'01'));
            }
            if($tcfdi==3){
                $this->db->where(array('f_relacion'=>1,'f_r_tipo'=>'04'));
            }
            if($tcfdi==4){
                $this->db->where(array('f_relacion'=>0,'f_r_tipo'=>'07'));
            }
            if($tcfdi==5){
                $this->db->where(array('f_relacion'=>1,'f_r_tipo'=>'07','TipoComprobante'=>'I'));
            }
            if($tcfdi==6){
                $this->db->where(array('f_relacion'=>1,'f_r_tipo'=>'07','TipoComprobante'=>'E'));
            }
            if($tcfdi==7){
                $this->db->where(array('cartaporte'=>1));
            }
        }
        $this->db->group_by("FacturasId");
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.FormaPago,
                    fac.iva,
                    fac.ivaretenido,
                    fac.subtotal,
                    fac.serie
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }

    function getlistpendientesv($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.id',
            1=>'cli.empresa',
            2=>'concat(COALESCE(per.nombre,"")," ",COALESCE(per.apellido_paterno,"")," ",COALESCE(per.apellido_materno,"")) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_venta fac', 'fac.ventaId=v.id and fac.combinada=0','left');
        $this->db->where('fac.facId IS NULL');
        $this->db->where(array('v.combinada'=>0));
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistpendientesvt($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.id',
            1=>'cli.empresa',
            2=>'concat(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('ventas v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_venta fac', 'fac.ventaId=v.id and fac.combinada=0','left');
        $this->db->where('fac.facId IS NULL');
        $this->db->where(array('v.combinada'=>0));
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    function getlistpendientesvc($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.combinadaId',
            1=>'cli.empresa',
            2=>'concat(COALESCE(per.nombre,"")," ",COALESCE(per.apellido_paterno,"")," ",COALESCE(per.apellido_materno,"")) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventacombinada v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_venta fac', 'fac.ventaId=v.combinadaId and fac.combinada=1','left');
        $this->db->where('fac.facId IS NULL');
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistpendientesvct($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.combinadaId',
            1=>'cli.empresa',
            2=>'concat(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('ventacombinada v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_venta fac', 'fac.ventaId=v.combinadaId and fac.combinada=0','left');
        $this->db->where('fac.facId IS NULL');
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getlistpendientesp($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.id',
            1=>'cli.empresa',
            2=>'concat(COALESCE(per.nombre,"")," ",COALESCE(per.apellido_paterno,"")," ",COALESCE(per.apellido_materno,"")) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('polizasCreadas v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_poliza fac', 'fac.polizaId=v.id','left');
        $this->db->where(array('v.activo'=>1));
        $this->db->where('fac.facId IS NULL');
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistpendientespt($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'v.id',
            1=>'cli.empresa',
            2=>'concat(per.nombre," ",per.apellido_paterno," ",per.apellido_materno) as vendedor',
            3=>'v.reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('polizasCreadas v');
        $this->db->join('clientes cli', 'cli.id=v.idCliente');
        $this->db->join('personal per', 'per.personalId=v.id_personal');
        $this->db->join('factura_poliza fac', 'fac.polizaId=v.id','left');
        $this->db->where(array('v.activo'=>1));
        $this->db->where('fac.facId IS NULL');
        if($personal>0){
            $this->db->where(array('v.id_personal'=>$personal));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getlistpendientesr($params){
        $personal=$params['personals'];
        /*
        SELECT pre.prefId ,pre.periodo_inicial,pre.periodo_final,cont.idcontrato, cont.arrendataria, per.nombre,per.apellido_paterno,per.apellido_materno 
        FROM alta_rentas_prefactura as pre 
        LEFT join factura_prefactura as fac on fac.prefacturaId=pre.prefId 
        LEFT join alta_rentas as ren on ren.id_renta=pre.id_renta 
        LEFT join contrato as cont on cont.idcontrato=ren.idcontrato 
        inner JOIN personal as per on per.personalId=cont.ejecutivo 
        WHERE fac.facturaId is null
        
        */

        $columns = array( 
            0=>'pre.prefId',
            1=>'pre.periodo_inicial',
            2=>'pre.periodo_final',
            3=>'cont.idcontrato', 
            4=>'cont.arrendataria', 
            5=>'per.nombre',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('alta_rentas_prefactura pre');
        $this->db->join('factura_prefactura fac', 'fac.prefacturaId=pre.prefId','left');
        $this->db->join('alta_rentas ren', 'ren.id_renta=pre.id_renta','left');
        $this->db->join('contrato cont', 'cont.idcontrato=ren.idcontrato','left');
        $this->db->join('personal per', 'per.personalId=cont.ejecutivo','left');
        $this->db->where('fac.facturaId is null');
        if($personal>0){
            $this->db->where(array('per.personalId'=>$personal));
        }

        
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistpendientesrt($params){
        $personal=$params['personals'];
        
        $columns = array( 
             0=>'pre.prefId',
            1=>'pre.periodo_inicial',
            2=>'pre.periodo_final',
            3=>'cont.idcontrato', 
            4=>'cont.arrendataria', 
            5=>'per.nombre',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('alta_rentas_prefactura pre');
        $this->db->join('factura_prefactura fac', 'fac.prefacturaId=pre.prefId','left');
        $this->db->join('alta_rentas ren', 'ren.id_renta=pre.id_renta','left');
        $this->db->join('contrato cont', 'cont.idcontrato=ren.idcontrato','left');
        $this->db->join('personal per', 'per.personalId=cont.ejecutivo','left');
        $this->db->where('fac.facturaId is null');
        if($personal>0){
            $this->db->where(array('per.personalId'=>$personal));
        }


        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function regimenf($numero){
        switch (intval($numero)) {            
            case 601:
                $regimen="601 General de Ley Personas Morales";
                break;
            case 603:
                $regimen="603 Personas Morales con Fines no Lucrativos";
                break;
            case 605:
                $regimen="605 Sueldos y Salarios e Ingresos Asimilados a Salarios";
                break;
            case 606:
                $regimen="606 Arrendamiento";
                break;
            case 607:
                $regimen="607 Régimen de Enajenación o Adquisición de Bienes";
                break;
            case 608:
                $regimen="608 Demás ingresos";
                break;
            case 609:
                $regimen="609 Consolidación";
                break;
            case 610:
                $regimen="610 Residentes en el Extranjero sin Establecimiento Permanente en México";
                break;
            case 611:
                $regimen="611 Ingresos por Dividendos (socios y accionistas)";
                break;
            case 612:
                $regimen="612 Personas Físicas con Actividades Empresariales y Profesionales";
                break;
            case 614:
                $regimen="614 Ingresos por intereses";
                break;
            case 615:
                $regimen="615 Régimen de los ingresos por obtención de premios";
                break;
            case 616:
                $regimen="616 Sin obligaciones fiscales";
                break;
            case 620:
                $regimen="620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos";
                break;
            case 621:
                $regimen="621 Incorporación Fiscal";
                break;
            case 622:
                $regimen="622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras";
                break;
            case 623:
                $regimen="623 Opcional para Grupos de Sociedades";
                break;
            case 624:
                $regimen="624 Coordinados";
                break;
            case 625:
                $regimen="625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas";
                break;
            case 626:
                $regimen="626 Régimen Simplificado de Confianza";
                break;
            case 628:
                $regimen="628 Hidrocarburos";
                break;
            case 629:
                $regimen="629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales";
                break;
            case 630:
                $regimen="630 Enajenación de acciones en bolsa de valores";
                break;
            default:
                $regimen="";
                break;
        }
        return $regimen;
    }
    public function getfacturasexport($params){
        $fechaini=$params['finicio'];
        $fechafin=$params['ffin'];
        $tiporeporte=$params['tiporeporte'];

        if($fechaini!=''){
            $wfechaini=" fac.fechatimbre >= '$fechaini 00:00:00' and ";
        }else{
            $wfechaini='';
        }
        if($fechafin!=''){
            $wfechafin=" fac.fechatimbre <= '$fechafin 23:59:59' and ";
        }else{
            $wfechafin="";
        }
        if($tiporeporte!=0){
            if($tiporeporte==1){ //Ventas
                $innerjoin=" INNER JOIN factura_venta as facven ON facven.facturaId=fac.FacturasId and facven.combinada=1 ";
            }elseif($tiporeporte==2){ //Ventas conbinadas
                $innerjoin=" INNER JOIN factura_venta as facven ON facven.facturaId=fac.FacturasId and facven.combinada=0 ";
            }elseif($tiporeporte==3){ //Polizas
                $innerjoin=" INNER JOIN factura_poliza as facpol ON facpol.facturaId=fac.FacturasId ";
            }elseif($tiporeporte==4){ //Servicio
                $innerjoin=" INNER JOIN factura_servicio as facser ON facser.facturaId=fac.FacturasId ";
            }elseif($tiporeporte==5){ //Rentas
                $innerjoin=" INNER JOIN factura_prefactura as facpre ON facpre.facturaId=fac.FacturasId ";
            }
        }else{
            $innerjoin="";
        }

        $sql = "SELECT 
                    fac.FacturasId, 
                    fac.Folio, 
                    fac.Nombre, 
                    fac.Rfc, 
                    fac.total, 
                    fac.Estado, 
                    fac.fechatimbre, 
                    fac.rutaXml, 
                    fac.rutaAcuseCancelacion, 
                    fac.cadenaoriginal, 
                    fac.FormaPago, 
                    per.nombre, 
                    per.apellido_paterno, 
                    per.apellido_materno, 
                    fac.uuid,
                    compl.uuid as uuid_compl,
                    compl.fechatimbre as fechatimbre_compl,
                    compldoc.ImpPagado
                FROM f_facturas as fac
                LEFT JOIN personal as per ON per.personalId = fac.usuario_id
                LEFT JOIN f_complementopago_documento as compldoc on compldoc.facturasId=fac.FacturasId
                LEFT JOIN f_complementopago as compl on compl.complementoId=compldoc.complementoId AND compl.Estado=1 
                $innerjoin
                WHERE 
                    $wfechaini
                    $wfechafin
                    fac.activo = 1

                    GROUP BY fac.FacturasId
                    ORDER BY fac.Folio DESC";
        $query = $this->db->query($sql);
        return $query;
    }
    function getfacturasexportc($params){
        $fechaini=$params['finicio'];
        $fechafin=$params['ffin'];
        if($fechaini!=''){
            $wfechaini=" and fac.fechatimbre >= '$fechaini 00:00:00'  ";
        }else{
            $wfechaini='';
        }
        if($fechafin!=''){
            $wfechafin=" and fac.fechatimbre <= '$fechafin 23:59:59'  ";
        }else{
            $wfechafin="";
        }

        $sql="SELECT * FROM(    (
                    SELECT 
                        fac.serie,fac.Folio,fac.Rfc,fac.Nombre,fac.fechatimbre,
                        0 as tipo, 
                        fven.ventaId,
                        clif.rfc as rfcv, 
                        clif.razon_social  
                    FROM f_facturas as fac
                    INNER JOIN factura_venta as fven on fven.facturaId=fac.FacturasId AND fven.combinada=0
                    INNER JOIN prefactura as pre on pre.ventaId=fven.ventaId AND pre.tipo=0
                    INNER JOIN cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id  
                    WHERE fac.Estado=1 $wfechaini $wfechafin
                )
                union
                (
                    SELECT 
                        fac.serie,fac.Folio,fac.Rfc,fac.Nombre,fac.fechatimbre,
                        3 as tipo,
                        IF(vc.equipo>0,vc.equipo,IF(vc.consumibles>0,vc.consumibles,IF(vc.refacciones>0,vc.refacciones,0))) as ventaId, 
                        clif.rfc as rfcv, 
                        clif.razon_social  
                    FROM f_facturas as fac
                    INNER JOIN factura_venta as fven on fven.facturaId=fac.FacturasId AND fven.combinada=1
                    INNER JOIN prefactura as pre on pre.ventaId=fven.ventaId AND pre.tipo=3
                    INNER JOIN cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id  
                    INNER JOIN ventacombinada as vc on vc.combinadaId=fven.ventaId
                    WHERE fac.Estado=1 $wfechaini $wfechafin
                )
                union
                (
                    SELECT 
                        fac.serie,fac.Folio,fac.Rfc,fac.Nombre,fac.fechatimbre,
                        1 as tipo,
                        facp.polizaId as ventaId,
                        clif.rfc as rfcv, 
                        clif.razon_social
                    FROM f_facturas as fac
                    INNER JOIN factura_poliza as facp on facp.facturaId=fac.FacturasId
                    INNER JOIN prefactura as pre on pre.ventaId=facp.polizaId AND pre.tipo=1
                    INNER JOIN cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id 
                    WHERE fac.Estado=1 $wfechaini $wfechafin
                )
                union
                (
                    SELECT fac.serie,fac.Folio,fac.Rfc,fac.Nombre,fac.fechatimbre,2 as tipo,con.idcontrato as ventaId,clif.rfc as rfcv, clif.razon_social 
                    FROM f_facturas as fac 
                    INNER JOIN factura_prefactura as facp on facp.facturaId=fac.FacturasId 
                    INNER JOIN contrato as con on con.idcontrato=facp.contratoId 
                    INNER JOIN cliente_has_datos_fiscales as clif on clif.id=con.rfc 
                    WHERE fac.Estado=1 $wfechaini $wfechafin
                )
            ) as datos ORDER BY fechatimbre DESC";
        $query = $this->db->query($sql);
        return $query;
    }
    function getfacturas_pp($params){
        //log_message('error', json_encode($params));
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'razonsocial',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
            8=>'rutaAcuseCancelacion',
            9=>'FormaPago',
            10=>'personal',
            11=>'uuid',
            12=>'empresa',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        //==============================================
            if($finicio!=''){
                $finicio_w=" and fac.fechatimbre >='$finicio 00:00:00'";
            }else{
                $finicio_w="";
            }
            if($ffin!=''){
                $ffin_w=" and fac.fechatimbre <='$ffin 23:59:59' ";
            }else{
                $ffin_w="";
            }
            if($personal>0){
                $personal_w=" and fac.creada_sesionId='$personal' ";
            }else{
                $personal_w="";
            }
            if($cliente>0){
                $cliente_w=" and fac.clienteId='$cliente' ";
            }else{
                $cliente_w="";
            }
            
        //==============================================
        $subconsulta="(
                        SELECT 
                            fac.FacturasId,fac.serie,fac.Folio,fac.clienteId, 
                            fac.Nombre as razonsocial,fac.Rfc,fac.total,fac.Estado,fac.fechatimbre,per.nombre as personal,fac.usuario_id,cli.nombre as empresa,fac.rutaXml,fac.rutaAcuseCancelacion,fac.FormaPago,fac.uuid,
                            fac.creada_sesionId,
                            IFNULL(
                                    (
                                        SELECT SUM(compd.ImpPagado)
                                        FROM f_complementopago AS comp
                                        inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                                        where comp.Estado=1 and compd.facturasId=fac.FacturasId),0) as complementos
                            FROM f_facturas as fac 
                            INNER JOIN personal as per on per.personalId=fac.usuario_id
                            inner join clientes as cli on cli.id=fac.clienteId
                            WHERE fac.Estado=1 $finicio_w $ffin_w $personal_w $cliente_w 
                        ) as datos";
        $this->db->from($subconsulta);
        //$this->db->where('total>complementos');

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas_pp($params){
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'razonsocial',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        //==============================================
            if($finicio!=''){
                $finicio_w=" and fac.fechatimbre >='$finicio 00:00:00'";
            }else{
                $finicio_w="";
            }
            if($ffin!=''){
                $ffin_w=" and fac.fechatimbre <='$ffin 23:59:59' ";
            }else{
                $ffin_w="";
            }
            if($personal>0){
                $personal_w=" and fac.creada_sesionId='$personal' ";
            }else{
                $personal_w="";
            }
            if($cliente>0){
                $cliente_w=" and fac.clienteId='$cliente' ";
            }else{
                $cliente_w="";
            }
            
        //==============================================
        $subconsulta="(
                        SELECT 
                            fac.FacturasId,fac.serie,fac.Folio,fac.clienteId, 
                            fac.Nombre as razonsocial,fac.Rfc,fac.total,fac.Estado,fac.fechatimbre,per.nombre as personal,fac.usuario_id, cli.nombre empresa,fac.rutaXml,fac.rutaAcuseCancelacion,fac.FormaPago,fac.uuid,
                            fac.creada_sesionId,
                            IFNULL(
                                    (
                                        SELECT SUM(compd.ImpPagado)
                                        FROM f_complementopago AS comp
                                        inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                                        where comp.Estado=1 and compd.facturasId=fac.FacturasId),0) as complementos
                            FROM f_facturas as fac 
                            INNER JOIN personal as per on per.personalId=fac.usuario_id
                            inner join clientes as cli on cli.id=fac.clienteId
                            WHERE fac.Estado=1  $finicio_w $ffin_w $personal_w $cliente_w 
                        ) as datos";
        $this->db->from($subconsulta);
        //$this->db->where('total>complementos');
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getcomplementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $personal = $params['personal'];
        $idcliente = $params['cliente'];
        $columns = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio) as folios',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre',
            9=>'comp.uuid',
            10=>'comp.doc_acuse_cancelacion',
            11=>'comp.clienteId',
            12=>'fac.envio_m_d',
            13=>'fac.envio_m_p',
            14=>'perem.nombre per_envio'
        );
        $columnsss = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio)',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_complementopago as comp');
        $this->db->join('f_complementopago_documento compd', 'compd.complementoId = comp.complementoId','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId = compd.facturasId','left');
        $this->db->join('personal per', 'per.personalId = comp.personalcreo','left');
        $this->db->join('personal perem', 'perem.personalId = fac.envio_m_p','left');

        if($finicio!=''){
            //$this->db->where(array('comp.fechatimbre >='=>$finicio.' 00:00:00'));
            $this->db->where("(comp.fechatimbre >='$finicio 00:00:00' or comp.fechatimbre='0000-00-00 00:00:00' or comp.fechatimbre IS NULL)");
        }
        if($ffin!=''){
            $this->db->where(array('comp.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($personal>0) {
            $this->db->where(array('comp.personalcreo'=>$personal));
        }
        if ($idcliente>0) {
            $this->db->where(array('comp.clienteId'=>$idcliente));
        }
        $this->db->where(array('comp.activo'=>1));
        $this->db->group_by("comp.complementoId");

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_complementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $personal = $params['personal'];
        $idcliente = $params['cliente'];
        $columns = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio)',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_complementopago as comp');
        $this->db->join('f_complementopago_documento compd', 'compd.complementoId = comp.complementoId','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId = compd.facturasId','left');
        $this->db->join('personal per', 'per.personalId = comp.personalcreo','left');
        
        if($finicio!=''){
            //$this->db->where(array('comp.fechatimbre >='=>$finicio.' 00:00:00'));
            $this->db->where("(comp.fechatimbre >='$finicio 00:00:00' or comp.fechatimbre='0000-00-00 00:00:00' or comp.fechatimbre IS NULL)");
        }
        if($ffin!=''){
            $this->db->where(array('comp.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($personal>0) {
            $this->db->where(array('comp.personalcreo'=>$personal));
        }
        if ($idcliente>0) {
            $this->db->where(array('comp.clienteId'=>$idcliente));
        }
        $this->db->where(array('comp.activo'=>1));
        $this->db->group_by("comp.complementoId");
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }

}