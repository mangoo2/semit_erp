<?php

class ConfiguracionDocumentos_Model extends CI_Model 
{
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function actualizaDatosConfiguracionCotizacion($data, $id) 
    {
	    $this->db->set($data);
	    $this->db->where('id', $id);
	    return $this->db->update('configuracionCotizaciones');
    }

    public function getConfiguracion($idConfiguracion){
        /*
        $sql = "SELECT * FROM configuracionCotizaciones WHERE id=".$idConfiguracion;
        $query = $this->DB11->query($sql);
        return $query->row();
        */

        //if(isset($_SESSION['session_configcot'])){
        //        $session_configcot=$_SESSION['session_configcot'];
        //}else{
            $sql = "SELECT * FROM configuracionCotizaciones WHERE id=".$idConfiguracion;
            $query = $this->db->query($sql);
            $session_configcot=$query->row();
          //  $_SESSION['session_configcot']=$session_configcot;
        //}
        return $session_configcot;
    }
}

?>