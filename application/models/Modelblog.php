<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelblog extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function get_categoria($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('blog_categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_categoria($params){
        $columns = array( 
            0=>'id',
            1=>'nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('blog_categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_listado($params){
        $columns = array( 
            0=>'b.id',
            1=>'b.titulo',
            2=>'b.articulo',
            3=>'c.nombre'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('blog b');
        $this->db->join('blog_categoria c','c.id=b.categoria','left');
        $where = array('b.activo'=>1);
        $this->db->where($where);

        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_listado($params){
        $columns = array( 
            0=>'b.id',
            1=>'b.titulo',
            2=>'b.articulo',
            3=>'c.nombre'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('blog b');
        $this->db->join('blog_categoria c','c.id=b.categoria','left');
        $where = array('b.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    
}