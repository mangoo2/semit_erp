<?php
class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function login($usuario){
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.nombre AS empleado, perf.nombre AS perfil,per.foto,usu.sucursal,suc.clave as succlave, perf.tipo_venta, perf.tipo_tecnico, suc.con_insumos, suc.name_suc
        FROM usuarios as usu 
        INNER JOIN personal as per on per.personalId=usu.personalId
        INNER JOIN perfiles as perf on perf.perfilId=usu.perfilId
        LEFT JOIN sucursal as suc on suc.id=usu.sucursal
        where per.estatus = 1 and usu.acceso = 1 AND usu.Usuario ='".$usuario."'";
        //log_message('error', 'sql: '.$strq);
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getMenus($perfil)
    {
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.estatus=1 and perfd.PerfilId='$perfil'";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu,$menusub){
        $wheretipo='';
        if($menusub==0){
            $wheretipo=" and menus.submenutipo=0 ";
        }else{
            $wheretipo=" AND menus.idsubmenu=$menusub and menus.submenutipo=1 ";
        }
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo 
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId $wheretipo and perfd.PerfilId='$perfil' and menus.MenuId='$menu'
            and perfd.estatus=1 
            ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }


    function submenus_group($perfil,$menu){

        $strq ="
            SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon, menus.tipo ,menus.idsubmenu,menus.MenuId
            from menu_sub as menus, perfiles_detalles as perfd 
            WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu'
            and perfd.estatus=1 
            GROUP BY menus.idsubmenu
            ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_record($table,$col,$id){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function get_data($table,$col,$id){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    ////////// Alertas 
    public function validar_productos_almacen($fecha_mayor){
        $sql = "SELECT pa.*, p.producto FROM productos_almacen AS pa
                LEFT JOIN productos AS p ON p.idproducto=pa.idproducto 
                WHERE pa.activo=1 AND pa.alerta=1 AND pa.fecha_caducidad <='".$fecha_mayor."'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function permisoadmin(){
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function alerta_entrada()
    {
        $sql="SELECT COUNT(*) AS total
            FROM compra_erp AS c
            WHERE c.activo=1 AND c.precompra=0 AND c.estatus!=1";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function alerta_traspaso()
    {
        $sql="SELECT COUNT(*) AS total
            FROM traspasos AS t
            WHERE t.activo=1 AND t.status=0";
        $query = $this->db->query($sql);
        return $query->row();
    }
}