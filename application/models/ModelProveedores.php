<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelProveedores extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_productos_numero(){
        $strq = "SELECT count(*) AS total from proveedores"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_listado($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'p.tel1',
            4=>'p.contacto',
            5=>'p.estatus',
            //6=>'(select sum(c.subtotal) as subtotal from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0) as subtotal',
            //7=>'(select sum(c.iva) as iva from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0) as iva',
            6=>'(select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras',
            7=>'(select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras'
        ); 

        $columns2 = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'p.tel1',
            4=>'p.contacto',
            5=>'p.estatus',
            //6=>'(select sum(c.subtotal) as subtotal from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0)',
            //7=>'(select sum(c.iva) as iva from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0)',
            6=>'(select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0)',
            7=>'(select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras'
        );  

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proveedores p');
        
        //if($params['estatus']!=0){
            $where = array('p.estatus'=>$params['estatus']);
            $this->db->where($where);
        //}
        
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if(!empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_listado($params){
        $columns2 = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'p.tel1',
            4=>'p.contacto',
            5=>'p.estatus',
            //6=>'(select sum(c.subtotal) as subtotal from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0)',
            //7=>'(select sum(c.iva) as iva from compra_erp as c where c.idproveedor=p.id and c.cancelado=0 and c.estatus=0 and c.precompra=0)',
            6=>'(select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0)',
            7=>'(select sum(c.isr) as isr_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as isr_compras'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('proveedores p');
        
        //if($params['estatus']!=0){
            $where = array('p.estatus'=>$params['estatus']);
            $this->db->where($where);
        //}
        
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_productos_provee($id){ //traer la suma del stock disponible en todas las sucus de acuerdo al tipo de producto
        $this->db->select('pp.codigo as cod_provee, p.idProducto, p.nombre, p.tipo, pro.nombre as provee, pro.codigo as codigo_pro,
            CASE 
                WHEN p.incluye_iva_comp = 1 and iva_comp=16 THEN (p.costo_compra*1.16) 
                ELSE p.costo_compra
            END AS costo_compra,
            CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio,
            CASE 
                WHEN ps.incluye_iva = 1 and ps.iva = 16 THEN (ps.precio * 1.16) 
                ELSE COALESCE(ps.precio, 0)
            END AS precio,
            CASE 
                WHEN p.tipo =2 
                THEN (
                    IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND psl.productoid=p.id),0)
                ) 
                ELSE 0 
            END as stock_lotes,
            CASE 
                WHEN p.tipo =1 
                THEN (
                    IFNULL((SELECT count(*) FROM productos_sucursales_serie pss WHERE pss.activo=1 AND pss.disponible=1 AND pss.productoid=p.id and pss.vendido=0),0)
                ) 
                ELSE 0 
            END as stock_series,
            CASE 
                WHEN p.tipo = 0
                THEN (
                    IFNULL((SELECT sum(stock) FROM productos_sucursales ps1 WHERE ps1.activo=1 AND ps1.productoid=p.id),0)
                ) 
                ELSE 0 
            END as stock_ps');
        $this->db->from("productos_proveedores pp");
        $this->db->join("proveedores pro","pro.id=pp.idproveedor");
        $this->db->join("productos p","p.id=pp.idproducto and p.estatus=1");
        $this->db->join("productos_sucursales ps","ps.productoid=p.id and ps.sucursalid=2 and ps.activo=1","left");
        $this->db->where("pp.idproveedor",$id);
        $this->db->where("pp.activo",1);
        $this->db->order_by("p.idProducto","asc");
        $this->db->group_by("p.id");
        $query=$this->db->get(); 
        return $query->result();
    }

    public function get_kardex_compras($params){
        $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'c.cancelado',
            11=>'c.precompra'
        );
        $columnx = array( 
            0=>'c.id',
            1=>'p.nombre as personal',
            2=>'c.reg',
            3=>'pr.nombre as proveedor', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'IFNULL(re.id,0) as id_re',
            11=>'c.cancelado',
            12=>'c.precompra'
        );
        $select="";
        foreach ($columnx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        $this->db->join('bitacora_reverse_entradas re', 're.id_ordencomp=c.id','left');
        $this->db->where("c.idproveedor",$params["id"]);
        $this->db->where('c.activo',"1");
        //pendiente o ingresada
        $this->db->where("(c.estatus = 0 or c.estatus=1)");
        $this->db->where('c.precompra',0);   

        if($params['f1']!='' && $params['f2']!=''){
            $where = ' c.reg BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }
        $this->db->group_by("c.id");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 

        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);

        $query=$this->db->get();
        return $query;
    }

    public function total_kardex_compras($params){
         $columns = array( 
            0=>'c.id',
            1=>'p.nombre',
            2=>'c.reg',
            3=>'pr.nombre', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'c.cancelado',
            11=>'c.precompra'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        
        $this->db->where("c.idproveedor",$params["id"]);
        $this->db->where('c.activo',"1");
        //pendiente o ingresada
        $this->db->where("(c.estatus = 0 or c.estatus=1)");
        $this->db->where('c.precompra',0);   

        if($params['f1']!='' && $params['f2']!=''){
            $where = ' c.reg BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    public function get_excelk_compras($params){
        $columnx = array( 
            0=>'c.id',
            1=>'p.nombre as personal',
            2=>'c.reg',
            3=>'pr.nombre as proveedor', 
            4=>'pr.codigo',
            5=>'c.total',
            6=>'c.estatus',
            7=>'c.activo',
            8=>'c.factura',
            9=>'c.distribusion',
            10=>'IFNULL(re.id,0) as id_re',
            11=>'c.cancelado',
            12=>'c.precompra'
        );
        $select="";
        foreach ($columnx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_erp c');
        $this->db->join('personal p', 'p.personalId=c.personalid');
        $this->db->join('proveedores pr', 'pr.id=c.idproveedor','left');
        $this->db->join('bitacora_reverse_entradas re', 're.id_ordencomp=c.id','left');
        $this->db->where("c.idproveedor",$params["id"]);
        $this->db->where('c.activo',"1");
        //pendiente o ingresada
        $this->db->where("(c.estatus = 0 or c.estatus=1)");
        $this->db->where('c.precompra',0);   

        if($params['f1']!='0' && $params['f2']!='0'){
            $where = ' c.reg BETWEEN "'.$params['f1'].' 00:00:00" AND "'.$params['f2'].' 23:59:59"';
            $this->db->where($where);
        }
        $this->db->group_by("c.id");
        $this->db->order_by("c.id","desc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function listExcelProve(){
        $this->db->select("p.*, (select sum(c.total) as total_compras from compra_erp as c where c.idproveedor=p.id and c.activo and c.cancelado=0 and (c.estatus=0 or c.estatus=1) and c.precompra=0) as total_compras");
        $this->db->from('proveedores p');
        //$this->db->where("estatus",1);
        $this->db->where("p.activo",1);
        $query=$this->db->get();
        return $query->result();
    }

 }