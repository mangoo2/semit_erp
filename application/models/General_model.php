<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_select($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function get_table_active($table){
    	$sql = "SELECT * FROM $table WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_record($col,$id,$table){
    	$sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_records_condition($condition,$table){
    	$sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        $this->db->update($table);
        return $id;
    }
    public function edit_recordw($where,$data,$table){
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);
    }

    public function delete_records($condition,$table){
    	$sql = "DELETE FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhereall_array($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->result_array();
    }

    public function getSelectCol($col,$tables,$values){
        $this->db->select($col);
        $this->db->from($tables);
        $this->db->where($values);
        $query=$this->db->get();
        return $query->result();
    }

    public function getSelectColOrder($col,$tables,$values,$title){
        $this->db->select($col);
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhereall2($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectlike($tables,$values,$search){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->or_like($search);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhere_orden_asc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectwhere_orden_asc2($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query;
    }

    public function getselectwhere_group($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->group_by($title);
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhere_orden_desc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 
    public function horacioslaborales($dia){
        switch ($dia) {
            case 1:
                $dial='l';
                break;
            case 2:
                $dial='m';
                break;
            case 3:
                $dial='mi';
                break;
            case 4:
                $dial='j';
                break;
            case 5:
                $dial='v';
                break;
            case 6:
                $dial='s';
                break;
            case 7:
                $dial='d';
                break;
        }
        $sql = "SELECT hl.horafin AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT hl.horainicio AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT h_inicio AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                UNION
                SELECT h_fin AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                ORDER BY `horas` ASC";
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_info_random($table, $cantidad){
        $sql = "SELECT * FROM $table WHERE activo=1 ORDER BY RAND() LIMIT $cantidad";
        $query = $this->db->query($sql);
        return $query;
    }

    public function delete_detalle_perfil($id){
        $sql = "DELETE FROM perfiles_detalles WHERE Perfil_detalleId=".$id;
        $query = $this->db->query($sql);
    }
    /*
    public function get_records_menu($id){
        $sql = "SELECT m.Pagina FROM config_general AS c
                INNER JOIN menu_sub AS m ON m.MenusubId = c.MenusubId
                WHERE c.perfilId=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclike2($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and estatus=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function ultimafechadesession(){
        $strq = "SELECT max(reg) as fecha FROM `historial_session` WHERE activo=1";
        $query = $this->db->query($strq);
        $fecha='';
        foreach ($query->result() as $row) {
            $fecha=$row->fecha;
        }
        return $fecha; 
    }
    public function formaspagosv(){
        $strq = "SELECT * FROM f_formapago WHERE activo=1 AND (clave='01' or clave='03' or clave='04' or clave='28' or clave='02' or clave='29')";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function turnos_limit1($id,$fecha){
        $sql = "SELECT * FROM turnos WHERE estatus=1 AND sucursal=$id and fecha ='$fecha' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_productos_files($id){
        $sql = "SELECT * FROM productos_files WHERE activo=1 AND idproductos=$id LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_clientes($id){
        $sql = "SELECT c.nombre,c.correo,cf.razon_social,cf.rfc,cf.cp,cf.RegimenFiscalReceptor,cf.uso_cfdi,cf.direccion FROM clientes AS c 
        LEFT JOIN cliente_fiscales AS cf ON  cf.idcliente=c.id
        WHERE c.id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_suc_prod($id){
        $sql = "SELECT suc.id as idsuc, suc.id_alias, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(ht.cantidad,0) as cant_traslado,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=$id and ht.activo=1 and ht.idsucursal_sale=psuc.sucursalid and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant
        FROM sucursal as suc
        LEFT JOIN productos_sucursales as psuc on psuc.sucursalid=suc.id AND psuc.productoid='$id'
        left JOIN historial_transpasos AS ht ON ht.idproducto=psuc.productoid and ht.tipo=1 and ht.status=1 and idsucursal_sale=psuc.sucursalid
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        WHERE suc.activo=1
        order by suc.orden";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_suc_prod_serie($id,$sucu){
        $sql = "SELECT suc.id as idsuc, suc.name_suc,psuc.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(c.estatus,0) as estatus_compra, IFNULL(t.status,2) as status_tras
        FROM sucursal as suc
        LEFT JOIN productos_sucursales_serie as psuc on psuc.sucursalid=suc.id AND psuc.productoid='$id' and psuc.disponible=1 and psuc.vendido=0
        left join traspasos_series_historial tsh on tsh.idseries=psuc.id and tsh.activo=1
        left join traspasos t on t.id=tsh.idtraspasos
        left join compra_erp c on c.id=psuc.idcompras and c.estatus=1
        WHERE suc.activo=1 AND  psuc.activo=1 and psuc.disponible=1 AND psuc.sucursalid=$sucu AND psuc.productoid=$id";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_suc_prod_lote($id,$sucu){
        $sql = "SELECT suc.id as idsuc, suc.name_suc,psuc.*, IFNULL(tlh.cantidad,0) as cantidad_tlh
            FROM sucursal as suc
            INNER JOIN historial_transpasos AS ht ON ht.idproducto=$id and idsucursal_sale=$suc and ht.status=1
            LEFT JOIN productos_sucursales_lote as psuc on psuc.sucursalid=suc.id AND psuc.productoid='$id'
            left join traspasos_lotes_historial tlh on tlh.id_historialt=ht.id and ht.tipo=0 and ht.idlotes=psuc.id
            WHERE suc.activo=1 AND  psuc.activo=1 and psuc.disponible=1 AND psuc.sucursalid=$sucu AND psuc.productoid=$id";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_productos_traspasos($id,$suc=0,$pdf=0,$deta=0,$solicita=0,$asigna=0,$sat_s=0){
        $suc=$this->session->userdata('sucursal');
        $perfil=$this->session->userdata('perfilid');
        $innerjoin='';
        $columns='';

        if($perfil==1 || $perfil==5 || $perfil==10){  //admin o almacen
            $where="WHERE ht.activo=1 AND t.id=$id";
        }else{
            $where="WHERE ht.activo=1 AND t.id=$id and (ht.idsucursal_entra=$suc or ht.idsucursal_sale=$suc)";
        }
        if($pdf==0){ //no es para pdf
            //$group="ht.idproducto";
            $group="ht.id";
        }if($pdf!=0){
            $group="ht.id";
        }if($deta!=0){
            $group="ht.idproducto";
        }if($solicita!=0){
            $group="ht.id";
        }
        $order="";
        if($asigna!=0){
            $order="order by p.tipo desc";
        }
        if($pdf!=0){
            $order.=" ,p.idProducto asc";
        }

        if($sat_s==1){
            /*
            $innerjoin='left join f_unidades fu on fu.Clave=p.unidad_sat 
            LEFT join f_servicios fs on fs.Clave=p.servicioId_sat';
                $columns="IFNULL(fu.nombre,'') as nom_unidad,
            IFNULL(fu.Clave,'') as clave_unidad,
            IFNULL(fs.nombre,'') as nom_servico,
            IFNULL(fs.Clave,'') as clave_servicio,
            IFNULL(fs.material_peligroso,0) as mpeligro,";
            */
        }

        $sql = "SELECT t.id as id_tras, t.tipo as tipo_tras ,ht.reg,ht.fechaingreso, ht.status, ht.id as id_ht,p.idProducto, p.stock, p.nombre,p.tipo as tipo_prod,ht.cantidad,sx.name_suc AS name_sucx,sy.name_suc AS name_sucxy,ht.idsucursal_sale,ht.idsucursal_entra,ht.idproducto,t.reg,pe.nombre AS personal,ht.tipo,ht.id_compra, ht.idtranspasos, ps.stock as stockps,
        r.codigo,r.capacidad, COALESCE(r.tipo,0) as tipo_rec, r.precioc, COALESCE(r.stock,0) as stock_recarga,
        CASE 
            WHEN ht.idproducto_series > 0 and p.tipo != 1 and p.tipo != 2
            THEN (
                COALESCE((SELECT serie FROM productos_sucursales_serie as pss2 WHERE pss2.id=ht.idproducto_series and pss2.sucursalid=ht.idsucursal_sale),'')
            ) 
            ELSE '' 
        END as serie,
        CASE 
            WHEN ht.idproducto_lote > 0 and p.tipo=2
            THEN (
                COALESCE((SELECT lote FROM productos_sucursales_lote as psl2 WHERE psl2.id=ht.idproducto_lote and psl2.sucursalid=ht.idsucursal_sale),'')
            ) 
            ELSE '' 
        END as lote,
        ht.idproducto_series, ht.idproducto_lote, ht.rechazado,ht.motivo_rechazo,
        /*ps.precio, */
        CASE 
            WHEN p.tipo =2 
            THEN (
                COALESCE((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2 and sucursalid=ht.idsucursal_sale),0)
            ) 
            ELSE 0 
        END as stock_lotes,
        CASE 
            WHEN p.tipo =1 
            THEN (
                COALESCE((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=ht.idsucursal_sale),0)
            ) 
            ELSE 0 
        END as stock_series,
        COALESCE((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
            join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
            where ht.idproducto=p.id and ht.activo=1 and ht.idsucursal_sale=sx.id and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant,
        CASE 
            WHEN r.id >0 and ht.tipo=1
            THEN (
                COALESCE((select sum(ht.cantidad) as traslado_tanque_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=r.id and ht.activo=1 and ht.idsucursal_sale=r.sucursal and ht.tipo=1 and ht.status=1 and t.tipo=1 and ht.rechazado!=1),0)
            ) 
            ELSE 0 
        END as traslado_tanque_cant,
        CASE 
            WHEN pss.id >0 and p.tipo=1
            THEN (
                select count(tsh.id) as tot_tras from traspasos_series_historial tsh where tsh.idseries=pss.id and tsh.activo=1
            ) 
            ELSE '' 
        END as tot_tras,
        COALESCE(r_ent.stock,0) as stock_recarga_ent,
        CASE 
            WHEN p.tipo =2 
            THEN (
                COALESCE((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id and p.tipo=2 and sucursalid=ht.idsucursal_entra),0)
            ) 
            ELSE 0 
        END as stock_lotes_ent,
        CASE 
            WHEN p.tipo =1 
            THEN (
                COALESCE((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=ht.idsucursal_entra),0)
            ) 
            ELSE 0 
        END as stock_series_ent,
        COALESCE((select sum(ht.cantidad) as traslado_stock_cant_ent from historial_transpasos as ht
            join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
            where ht.idproducto=p.id and ht.activo=1 and ht.idsucursal_entra=sy.id and ht.tipo=0 and (ht.status=0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant_ent,
        CASE 
            WHEN r_ent.id>0 and ht.tipo=1
            THEN (
                COALESCE((select sum(ht.cantidad) as traslado_tanque_cant_ent from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=r_ent.id and ht.activo=1 and ht.idsucursal_entra=r_ent.sucursal and ht.tipo=1 and ht.status=1 and t.tipo=1 and ht.rechazado!=1),0)
            ) 
            ELSE 0 
        END as traslado_tanque_cant_ent,
        COALESCE((
            SELECT tlh.cantidad 
            FROM traspasos_lotes_historial tlh
            WHERE tlh.idlotes=psl.id 
              AND tlh.activo=1 
              AND p.tipo=2
            LIMIT 1), 0) as tras_stock_lote,
        $columns
        COALESCE(p.peso,0) as peso,
        COALESCE(ps.precio,0) as precio,
        COALESCE(ps.iva,0) as iva,
        /*ps.stock, */
        p.unidad_sat,
        p.servicioId_sat,
        (SELECT ps_ent.stock 
            FROM productos_sucursales ps_ent
            WHERE ps_ent.productoid = p.id
            AND ps_ent.activo = 1
            AND ps_ent.sucursalid = ht.idsucursal_entra) as stockps_ent
        FROM traspasos AS t  
        INNER JOIN historial_transpasos AS ht ON ht.idtranspasos=t.id and ht.activo=1
        INNER JOIN sucursal AS sx ON sx.id=ht.idsucursal_sale
        INNER JOIN sucursal AS sy ON sy.id=ht.idsucursal_entra
        INNER JOIN personal AS pe ON pe.personalId=t.idpersonal
        LEFT JOIN productos AS p ON p.id=ht.idproducto and ht.tipo=0
        LEFT JOIN recargas AS r ON r.id=ht.idproducto and ht.tipo=1 and ht.idsucursal_sale=r.sucursal
        LEFT JOIN recargas AS r_ent ON r_ent.id=ht.idproducto and ht.tipo=1 and ht.idsucursal_entra=r_ent.sucursal
        left join productos_sucursales ps on ps.productoid=p.id and ps.activo=1 and ps.sucursalid=ht.idsucursal_sale
        /*left join productos_sucursales ps_ent on ps_ent.productoid=p.id and ps_ent.activo=1 and ps_ent.sucursalid=ht.idsucursal_entra */
        LEFT join productos_sucursales_serie as pss on pss.activo=1 and pss.disponible=1 and pss.vendido=0 AND pss.productoid=p.id AND pss.sucursalid=sx.id and p.tipo=1
        LEFT join productos_sucursales_lote as psl on psl.activo=1 AND psl.productoid=p.id AND psl.sucursalid=sx.id and psl.cantidad>0 and p.tipo=2
        /*left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1 and p.tipo=2 */
        /*left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1 */
        $where
        group by ".$group."
        ".$order."
        /*WHERE ht.activo=1 AND t.id=$id and (ht.idsucursal_entra=$suc or ht.idsucursal_sale=$suc)*/";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_productos_traspasos2($id){
        $sql = "SELECT t.id as id_tras, ht.id as id_ht, p.nombre,p.tipo as tipo_prod,ht.cantidad,sx.name_suc AS name_sucx,sy.name_suc AS name_sucxy,ht.idsucursal_sale,ht.idsucursal_entra,ht.idproducto,t.reg,pe.nombre AS personal,ht.tipo, ht.id_compra,
            r.codigo,r.capacidad, IFNULL(r.tipo,0) as tipo_rec,
            IFNULL(pss.serie,'') as serie, IFNULL(psl.lote,'') as lote, ht.idproducto_series,psl.id as id_lote,
            IFNULL(tlh.cantidad,0) as cantidad_tlh
        FROM traspasos AS t  
        INNER JOIN historial_transpasos AS ht ON ht.idtranspasos=t.id
        LEFT JOIN productos AS p ON p.id=ht.idproducto and ht.tipo=0
        LEFT JOIN recargas AS r ON r.id=ht.idproducto and ht.tipo=1
        left join traspasos_lotes_historial tlh on tlh.id_historialt=ht.id and ht.tipo=0 and p.tipo=2
        LEFT JOIN productos_sucursales_serie AS pss ON pss.id=ht.idproducto_series and pss.activo=1 and p.tipo=1
        LEFT JOIN productos_sucursales_lote AS psl ON psl.id=ht.idproducto_lote and psl.activo=1  and p.tipo=2
        INNER JOIN sucursal AS sx ON sx.id=ht.idsucursal_sale
        INNER JOIN sucursal AS sy ON sy.id=ht.idsucursal_entra
        INNER JOIN personal AS pe ON pe.personalId=t.idpersonal
        WHERE ht.activo=1 AND ht.id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    /*
        SELECT usu.UsuarioID,usu.Usuario,pef.nombre,suc.name_suc,usu.suc_date
        FROM usuarios as usu
        INNER JOIN personal as per on per.personalId=usu.personalId
        INNER JOIN perfiles as pef on pef.perfilId=usu.perfilId
        INNER JOIN sucursal as suc on suc.id=usu.sucursal
        WHERE usu.estatus=1 AND usu.acceso=1 AND per.estatus=1 AND usu.perfilId=3
    */
    function get_list_cg_u($params){
        $columns = array( 
            0=>'usu.UsuarioID',
            1=>'usu.Usuario',
            2=>'pef.nombre',
            3=>'suc.name_suc',
            4=>'usu.suc_date'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('usuarios usu');
        $this->db->join('personal per','per.personalId=usu.personalId');
        $this->db->join('perfiles pef','pef.perfilId=usu.perfilId');
        $this->db->join('sucursal suc','suc.id=usu.sucursal');
        $this->db->where(array('usu.estatus'=>1,'usu.acceso'=>1,'per.estatus'=>1,'usu.perfilId'=>3));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function get_list_cg_u_total($params){
        $columns = array( 
            0=>'usu.UsuarioID',
            1=>'usu.Usuario',
            2=>'pef.nombre',
            3=>'suc.name_suc',
            4=>'usu.suc_date'
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('usuarios usu');
        $this->db->join('personal per','per.personalId=usu.personalId');
        $this->db->join('perfiles pef','pef.perfilId=usu.perfilId');
        $this->db->join('sucursal suc','suc.id=usu.sucursal');
        $this->db->where(array('usu.estatus'=>1,'usu.acceso'=>1,'per.estatus'=>1,'usu.perfilId'=>3));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_menus(){
        $strq ="SELECT *
            from menu
            where MenuId > 1
            ORDER BY orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_submenus($menu,$perfil){
        $strq ="SELECT ms.*, IFNULL(pd.Perfil_detalleId,0) as Perfil_detalleId, IFNULL(pd.MenusubId,0) as MenusubIdpd
            from menu_sub ms
            left join perfiles_detalles pd on pd.MenusubId=ms.MenusubId and perfilId=$perfil and pd.estatus=1
            WHERE tipo=0 and MenuId=$menu
            ORDER BY orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_productos_traspaso_series($traspasoid){
        $strq="SELECT 
                pro.servicioId_sat as clave_servicio, 
                ser.nombre as nom_servico,
                ser.material_peligroso,
                ser.material_peligroso as mpeligro,
                 1 as cantidad,
                 pro.nombre as producto, 
                ps.serie,
                pro.unidad_sat as clave_unidad,
                uni.nombre as unidad, 
                pro.peso,
                pros.precio,
                pros.iva,
                ROUND(
                    CASE 
                        WHEN pros.iva > 0 THEN pros.precio * 1.16 
                        ELSE pros.precio 
                    END,2) AS precio_final
            FROM traspasos_series_historial as t
            INNER JOIN productos_sucursales_serie AS ps ON ps.id=t.idseries
            INNER JOIN productos as pro on pro.id=ps.productoid
            INNER join f_servicios as ser on ser.Clave=pro.servicioId_sat
            INNER join f_unidades as uni on uni.Clave=pro.unidad_sat
            INNER join productos_sucursales as pros on pros.productoid=pro.id
            WHERE ps.activo=1 and t.idtraspasos='$traspasoid' and t.activo=1
            group by t.id";
            //log_message('error','get_productos_traspaso_series :'.$strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function get_productos_traspaso_lotes($traspasoid){
        $strq="SELECT 
                pro.servicioId_sat as clave_servicio,  
                ser.material_peligroso,
                ser.material_peligroso as mpeligro,
                ser.nombre as nom_servico,
                trasl.cantidad,
                pro.nombre as producto,
                prosl.lote,
                prosl.caducidad,
                '' as serie,
                pro.unidad_sat as clave_unidad,
                uni.nombre as unidad,
                pro.peso,
                pros.precio,
                pros.iva,
                ROUND(
                                CASE 
                                    WHEN pros.iva > 0 THEN pros.precio * 1.16 
                                    ELSE pros.precio 
                                END,2) AS precio_final,
                trasl.*
            FROM traspasos_lotes_historial as trasl
            INNER JOIN historial_transpasos as htra on htra.id=trasl.id_historialt
            INNER JOIN productos as pro on pro.id=htra.idproducto
            INNER join f_servicios as ser on ser.Clave=pro.servicioId_sat
            INNER join f_unidades as uni on uni.Clave=pro.unidad_sat
            INNER join productos_sucursales as pros on pros.productoid=pro.id
            INNER JOIN productos_sucursales_lote as prosl on prosl.id=trasl.idlotes
            WHERE trasl.idtraspasos='$traspasoid'
            GROUP BY trasl.id";
        $query = $this->db->query($strq);
        return $query;
    }
    function stock_producto_lotes($pro,$suc){
        // es una copia de la funcion $this->ModelProductos->get_suc_lotes solo modificada un poco
        $strq="SELECT SUM(cantidad) as cantidad 
                    FROM(
                        SELECT psl.*, IFNULL(tlh.idlotes,0) as id_lote_tras, IFNULL(tlh.cantidad,0) as cant_lote_tras, IFNULL(t.status,0) as status, IFNULL(c.estatus,0) as estatus_compra
                        FROM productos_sucursales_lote as psl
                        left join traspasos_lotes_historial tlh on tlh.idlotes=psl.id and tlh.activo=1
                        left join traspasos t on t.id=tlh.idtraspasos and t.status=1
                        left join compra_erp c on c.id=psl.idcompra and c.estatus=1
                        WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid=$pro and psl.sucursalid=$suc AND
                            ((psl.idcompra!=0 AND c.estatus=1) or psl.idcompra=0)
                    group by psl.id
                )datos";
        $query = $this->db->query($strq);
        $stock=0;
        foreach ($query->result() as $item) {
            if($item->cantidad>0){
                $stock = $item->cantidad;
            }
        }
        return $stock;
    }
    function stock_producto_series($pro,$suc){
        // es una copia de la funcion $this->ModelProductos->get_suc_series solo modificada un poco
        $strq="SELECT pss.*, IFNULL(tsh.idseries,0) as id_serie_tras, IFNULL(gd.id,0) as id_gd, IFNULL(t.status,2) as status_tras, IFNULL(c.estatus,0) as estatus_compra
            FROM productos_sucursales_serie as pss
            left join traspasos_series_historial tsh on tsh.idseries=pss.id and tsh.activo=1
            left join traspasos t on t.id=tsh.idtraspasos
            left join compra_erp c on c.id=pss.idcompras and c.estatus=1
            left join garantias_detalle gd on gd.idprod='$pro' and gd.id_ps_ser=pss.id and gd.estatus=2 and gd.activo=1 and gd.id_origen=pss.sucursalid
            WHERE pss.activo=1 and productoid='$pro' and sucursalid='$suc' and disponible=1 
            and ((pss.idcompras!=0 AND c.estatus=1) or pss.idcompras=0)
            
            group by id;";
        $query = $this->db->query($strq);
        $stock=0;
        if($query->num_rows()>0){
            $stock=$query->num_rows();
        }
        return $stock;
    }
    function lis_suc_turnos($fecha){
        $strq="SELECT suc.id,suc.clave,suc.id_alias,suc.name_suc,tur.id as turid 
                FROM sucursal as suc 
                LEFT JOIN turnos as tur on tur.sucursal=suc.id AND tur.estatus=1 AND tur.fecha='$fecha' 
                WHERE suc.activo=1 ORDER BY suc.orden ASC";
        $query = $this->db->query($strq);
        return $query; 
    }
    function info_turno_vigente($suc,$fecha){
        $strq ="SELECT * FROM turnos WHERE fecha='$fecha' AND sucursal='$suc' AND estatus=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function nom_unidad($clave){
        $result=$this->get_select('f_unidades',array('Clave'=>$clave));
        $nombre='';
        foreach ($result->result() as $item) {
            $nombre = $item->nombre;
        }
        return $nombre;
    }
    function nom_servicio($clave){
        $result=$this->get_select('f_servicios',array('Clave'=>$clave));
        $nombre='';$material_peligroso=0;
        foreach ($result->result() as $item) {
            $nombre = $item->nombre;
            $material_peligroso=$item->material_peligroso;
        }
        $array=array(
            'nom'=>$nombre,
            'mpeligro'=>$material_peligroso
        );
        return $array;
    }

}