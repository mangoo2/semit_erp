<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloMttos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_listMtto($params){
        $columns = array( 
            0=>'me.id',
            1=>'me.reg',
            2=>'me.descrip',
            3=>'me.marca',
            4=>'me.modelo',
            5=>'me.serie',
            6=>'s.name_suc',
            7=>'me.estatus',
            8=>'s.activo',
            9=>'c.nombre',
            10=>'me.id_venta'
        );

        $columnsx = array( 
            0=>'me.id',
            1=>'DATE_FORMAT(me.reg, "%d/%m/%Y - %r" ) AS reg',
            2=>'me.descrip',
            3=>'me.marca',
            4=>'me.modelo',
            5=>'me.serie',
            6=>'s.name_suc AS suc_solicita',
            7=>'me.estatus',
            8=>'s.activo',
            9=>'c.nombre',
            10=>'me.id_venta'
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('mtto_externo me');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->join('personal pe', 'pe.personalId=me.id_personal');
        $this->db->join("clientes c","c.id=me.id_cliente");
        $this->db->where("me.activo",1);
                
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    function total_list_mtto($params){
        $columns = array( 
            0=>'me.id',
            1=>'me.reg',
            2=>'me.descrip',
            3=>'me.marca',
            4=>'me.modelo',
            5=>'me.serie',
            6=>'s.name_suc',
            7=>'me.estatus',
            8=>'s.activo',
            9=>'c.nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('mtto_externo me');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->join('personal pe', 'pe.personalId=me.id_personal'); 
        $this->db->join("clientes c","c.id=me.id_cliente");
        $this->db->where("me.activo",1);
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_mtto($id){
        $this->db->select('m.*, c.nombre, cf.id as id_razon, IFNULL(om.id,0) as id_ord');
        $this->db->from("mtto_externo m");
        $this->db->join("clientes c","c.id=m.id_cliente");
        $this->db->join("orden_trabajo om","om.id_mtto=m.id","left");
        $this->db->join("cliente_fiscales cf","cf.idcliente=c.id");
        $this->db->where("m.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function getInsumos($id,$id_venta){
        $this->db->select("b.*, p.idProducto, p.nombre, IFNULL(cs.numero,'') as numero, IFNULL(concat(cs.descripcion,' $',' ', CASE
            WHEN cs.iva=1 THEN round(cs.precio_siva*1.16,2)
            WHEN cs.iva=2 THEN round(cs.precio_siva,2)
        END),'') as descripcion");
        $this->db->from("bitacora_ins_servs_ventas b");
        $this->db->join("productos p","p.id=b.id_insumo");
        if($id_venta==0){
            $this->db->join("cat_servicios cs","cs.id=b.id_serv","left");
        }else{
            $this->db->join("venta_erp_detalle vd","vd.idventa=$id_venta and vd.tipo=3 and vd.activo=1");
            $this->db->join("cat_servicios cs","cs.id=vd.idproducto");
        }
        $this->db->where("b.id_mtto", $id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getMttosPents($id){
        $this->db->select('m.*, c.nombre, cf.id as id_razon');
        $this->db->from("mtto_externo m");
        $this->db->join("clientes c","c.id=m.id_cliente");
        $this->db->join("cliente_fiscales cf","cf.idcliente=c.id");
        $this->db->where("m.id_cliente", $id);
        $this->db->where("m.id_venta", 0);
        $this->db->where("(m.estatus=1 or m.estatus=2)");
        $query=$this->db->get(); 
        return $query->result();
    }


    function get_detalle_orden($id){
        $this->db->select('ot.*, s.name_suc AS suc_solicita, ot.nom_solicita, ot.dir_solicita, ot.col_solicita, ot.cel_solicita, ot.mail_solicita, ot.tel_solicita, ot.rfc_solicita, ot.nom_contacto, ot.tel_contacto, me.cantidad, me.descrip, me.marca, me.modelo, me.serie, me.a_cuenta, pe.nombre AS personal');
        $this->db->from("orden_trabajo ot");
        $this->db->join("mtto_externo me","me.id=ot.id_mtto");
        $this->db->join('personal pe', 'pe.personalId=me.id_personal');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->join("clientes c","c.id=me.id_cliente","left");
        $this->db->where("me.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }   

    function getMttosEvide($id){
        $this->db->select('e.*');
        $this->db->from("evidencia_mtto e");
        $this->db->where("id_mtto", $id);
        $this->db->where("estatus", 1);
        $query=$this->db->get(); 
        return $query;
    }

    /* **************************************/
    function getOrdenTrabajo($id){
        $this->db->select('me.*, IFNULL(ot.id,0) as id_ot, ot.fecha, ot.observ, ot.nom_solicita, ot.dir_solicita, ot.col_solicita, ot.cel_solicita, ot.tel_solicita, ot.mail_solicita, ot.rfc_solicita, ot.nom_contacto, ot.tel_contacto, s.name_suc AS suc_solicita, s.tel, s.domicilio, c.nombre as cliente, c.calle, c.colonia, c.celular, c.correo');
        $this->db->from("mtto_externo me");
        $this->db->join('orden_trabajo ot', 'ot.id_mtto=me.id','left');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->join("clientes c","c.id=me.id_cliente");
        $this->db->where("me.id",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getPagoServicio($id){
        $this->db->select("me.*, c.nombre");
        $this->db->from('mtto_externo me');
        $this->db->join('clientes c', 'c.id=me.id_cliente');
        //$this->db->join('servicios s', 's.id=r.id_servicio');
        $this->db->where('me.id',$id);    
        $query = $this->db->get();
        return $query->result();
    }

    /* ***************************************** */
    function getEquiposSeries($search,$idsuc){
        $strq = "SELECT pss.*, p.id AS id_prod, p.nombre AS producto, p.idProducto
        FROM productos_sucursales_serie pss
        JOIN productos p ON pss.productoid = p.id
        WHERE (
            (p.nombre LIKE '%$search%' OR pss.serie LIKE '%$search%')
            AND (p.activo = 1 OR p.activo = 'Y')
            AND pss.activo = 1
            AND pss.vendido = 0
            AND (pss.disponible= 1 or pss.disponible= 0)
            AND pss.sucursalid = $idsuc
        )
        ORDER BY p.nombre ASC";
        $query = $this->db->query($strq);
        return $query;
    }

    function get_listMttoInt($params){
        $columns = array( 
            0=>'mi.id',
            1=>'mi.fecha_reg',
            2=>'mi.observ',
            3=>'p.nombre',
            4=>'ps.serie',
            5=>'mi.fecha_termino',
            6=>'mi.prox_mtto',
            7=>'mi.tipo',
            8=>'mi.estatus',
            9=>'mi.activo'
        );

        $columnsx = array( 
            0=>'mi.id',
            1=>'DATE_FORMAT(mi.fecha_reg, "%d/%m/%Y - %r" ) AS fecha_reg',
            2=>'mi.observ',
            3=>'p.nombre as equipo',
            4=>'ps.serie',
            5=>'mi.fecha_termino',
            6=>'mi.prox_mtto',
            7=>'mi.tipo',
            8=>'mi.estatus',
            9=>'mi.activo'
        );

        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('mtto_interno mi');
        $this->db->join('productos p', 'p.id=mi.id_equipo');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=mi.idprod_ss');
        $this->db->where("mi.activo",1);
                
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    function total_list_mttoInt($params){
        $columns = array( 
            0=>'mi.id',
            1=>'mi.fecha_reg',
            2=>'mi.observ',
            3=>'p.nombre',
            4=>'ps.serie',
            5=>'mi.fecha_termino',
            6=>'mi.prox_mtto',
            7=>'mi.tipo',
            8=>'mi.estatus',
            9=>'mi.activo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('mtto_interno mi');
        $this->db->join('productos p', 'p.id=mi.id_equipo');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=mi.idprod_ss');
        $this->db->where("mi.activo",1);
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_mttoInt($id){
        $this->db->select('m.*, p.nombre, ps.serie');
        $this->db->from("mtto_interno m");
        $this->db->join('productos p', 'p.id=m.id_equipo');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=m.idprod_ss');
        $this->db->where("m.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function set_rentaMtto($id){
        $this->db->select('r.*, p.nombre, ps.serie');
        $this->db->from("rentas r");
        $this->db->join('productos p', 'p.id=r.id_prod');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=r.id_serie');
        $this->db->where("r.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function get_mttoIntInsumos($id){
        $this->db->select('bi.*, p.nombre');
        $this->db->from("bitacora_insumos_mttosint bi");
        $this->db->join('productos p', 'p.id=bi.id_insumo');
        $this->db->where("bi.id_mtto",$id);
        $this->db->where("bi.estatus",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getOrdenTrabajo_int($id){
        $this->db->select('me.*, IFNULL(ot.id,0) as id_ot, ot.fecha, ot.observ as observ_ot, s.name_suc AS suc_solicita, s.tel, s.domicilio');
        $this->db->from("mtto_interno me");
        $this->db->join('orden_trabajo_int ot', 'ot.id_mtto=me.id','left');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->where("me.id",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function get_detalle_orden_int($id){
        $this->db->select('ot.*, s.name_suc AS suc_solicita, s.domicilio, s.tel, me.observ as observ_mtto, p.nombre as equipo, ps.serie, pe.nombre AS personal');
        $this->db->from("orden_trabajo_int ot");
        $this->db->join("mtto_interno me","me.id=ot.id_mtto");
        $this->db->join('productos p', 'p.id=me.id_equipo');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=me.idprod_ss');
        $this->db->join('personal pe', 'pe.personalId=me.id_personal');
        $this->db->join('sucursal s', 's.id=me.id_sucursal');
        $this->db->where("me.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function get_equipo_kardex($params){
        $this->db->select('m.*, p.idProducto, p.nombre as equipo, ps.serie');
        $this->db->from("mtto_interno m");
        $this->db->join('productos p', 'p.id=m.id_equipo');
        $this->db->join('productos_sucursales_serie ps', 'ps.id=m.idprod_ss');
        $this->db->where("m.id_equipo", $params["id_equipo"]);
        $this->db->where("m.idprod_ss", $params["id_serie"]);
        $this->db->where("m.activo",1);
        $this->db->where("m.fecha_termino BETWEEN '".$params['fechai']."' AND '".$params['fechaf']."'");
        $query=$this->db->get(); 
        return $query->result();
    }

}