<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloComplementos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getcomplementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $personal = $params['personal'];
        $idcliente = $params['idcliente'];
        $rs_foliosc=$params['rs_foliosc'];
        $columns = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio) as folios',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno',
            11=>'comp.uuid',
            12=>'comp.doc_acuse_cancelacion'
        );
        $columnsss = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio)',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_complementopago as comp');
        $this->db->join('f_complementopago_documento compd', 'compd.complementoId = comp.complementoId','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId = compd.facturasId','left');
        $this->db->join('personal per', 'per.personalId = comp.personalcreo','left');

        if($finicio!=''){
            //$this->db->where(array('comp.fechatimbre >='=>$finicio.' 00:00:00'));
            $this->db->where("(comp.fechatimbre >='$finicio 00:00:00' or comp.fechatimbre='0000-00-00 00:00:00' or comp.fechatimbre IS NULL)");
        }
        if($ffin!=''){
            $this->db->where(array('comp.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($personal>0) {
            $this->db->where(array('comp.personalcreo'=>$personal));
        }
        if ($idcliente>0) {
            $this->db->where(array('comp.clienteId'=>$idcliente));
        }
        $this->db->where(array('comp.activo'=>1,'comp.Serie'=>$rs_foliosc));
        $this->db->group_by("comp.complementoId");

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_complementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $personal = $params['personal'];
        $idcliente = $params['idcliente'];
        $rs_foliosc=$params['rs_foliosc'];
        $columns = array( 
            0=>'comp.complementoId',
            1=>'comp.R_nombre',
            2=>'group_concat(fac.Folio)',
            3=>'comp.Monto',
            4=>'comp.Estado',
            5=>'comp.fechatimbre',
            6=>'comp.rutaXml',
            7=>'comp.rutaAcuseCancelacion',
            8=>'per.nombre',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_complementopago as comp');
        $this->db->join('f_complementopago_documento compd', 'compd.complementoId = comp.complementoId','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId = compd.facturasId','left');
        $this->db->join('personal per', 'per.personalId = comp.personalcreo','left');
        
        if($finicio!=''){
            //$this->db->where(array('comp.fechatimbre >='=>$finicio.' 00:00:00'));
            $this->db->where("(comp.fechatimbre >='$finicio 00:00:00' or comp.fechatimbre='0000-00-00 00:00:00' or comp.fechatimbre IS NULL)");
        }
        if($ffin!=''){
            $this->db->where(array('comp.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($personal>0) {
            $this->db->where(array('comp.personalcreo'=>$personal));
        }
        if ($idcliente>0) {
            $this->db->where(array('comp.clienteId'=>$idcliente));
        }
        $this->db->where(array('comp.activo'=>1,'comp.Serie'=>$rs_foliosc));
        $this->db->group_by("comp.complementoId");
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }

    
    function saldocomplemento($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            //log_message('error', 'validar saldo0: '.$item->ImpPagado);
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        //log_message('error', 'validar saldo1: '.$saldo);
        return $saldo; 
    }
    function saldocomplementonum($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        return $numcomplem; 
    }
    function complementofactura($factura){
        $strq = "SELECT com.complementoId,com.fechatimbre,per.nombre, comd.NumParcialidad,comd.ImpPagado,com.Estado,com.rutaXml, com.rutaAcuseCancelacion
                FROM f_complementopago as com
                INNER JOIN personal as per on per.personalId=com.personalcreo
                INNER JOIN f_complementopago_documento as comd on comd.complementoId=com.complementoId
                WHERE comd.facturasId=$factura ";
        $datoscop = $this->db->query($strq);
        return $datoscop;
    }


}