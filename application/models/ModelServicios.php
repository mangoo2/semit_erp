<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelServicios extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }


    function get_list($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.clave',
            2=>'s.descripcion'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicios s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.clave',
            2=>'s.descripcion'
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('servicios s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    function get_listcs($params){
        $columns = array( 
            0=>'s.numero',
            1=>'s.descripcion',
            2=>'s.tipo',
            3=>'s.precio_siva',
            4=>'s.iva',
            5=>'s.id',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cat_servicios s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_listcs($params){
        $columns = array( 
            0=>'s.numero',
            1=>'s.descripcion',
            2=>'s.tipo',
            3=>'s.precio_siva',
            4=>'s.iva',
            5=>'s.id',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('cat_servicios s');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}