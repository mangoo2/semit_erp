<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getfacturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        $columns = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.Rfc',
            4=>'fac.total',
            5=>'fac.fechatimbre',
            6=>'per.nombre',
            7=>'fac.favorito',
            8=>'fac.favorito',
            9=>'fac.Estado',
            
            10=>'fac.rutaXml',
            11=>'fac.rutaAcuseCancelacion',
            12=>'fac.cadenaoriginal',
            13=>'fac.FormaPago',
            14=>'fac.clienteId',
            15=>'fac.correoenviado',
            16=>'fac.estatus_correo',
            17=>'fac.uuid',
            18=>'fac.Nombre_alias'
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_facturas fac');
        $this->db->join('personal per', 'per.personalId = fac.usuario_id','left');
        
        if($cliente>0){
            $this->db->where(array('fac.clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fac.fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fac.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('fac.favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('fac.Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('fac.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
            8=>'Nombre_alias'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_facturas');
       
        if($cliente>0){
            $this->db->where(array('clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.serie,
                    fac.FormaPago,
                    fac.iva,
                    fac.ivaretenido,
                    fac.subtotal,
                    fac.serie
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturadetalle($facturaId){
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join f_unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=".$facturaId;
        $query = $this->db->query($strq);
        return $query; 
    }
    function regimenf($numero){
        switch (intval($numero)) {            
            case 601:
                $regimen="601 General de Ley Personas Morales";
                break;
            case 603:
                $regimen="603 Personas Morales con Fines no Lucrativos";
                break;
            case 605:
                $regimen="605 Sueldos y Salarios e Ingresos Asimilados a Salarios";
                break;
            case 606:
                $regimen="606 Arrendamiento";
                break;
            case 607:
                $regimen="607 Régimen de Enajenación o Adquisición de Bienes";
                break;
            case 608:
                $regimen="608 Demás ingresos";
                break;
            case 609:
                $regimen="609 Consolidación";
                break;
            case 610:
                $regimen="610 Residentes en el Extranjero sin Establecimiento Permanente en México";
                break;
            case 611:
                $regimen="611 Ingresos por Dividendos (socios y accionistas)";
                break;
            case 612:
                $regimen="612 Personas Físicas con Actividades Empresariales y Profesionales";
                break;
            case 614:
                $regimen="614 Ingresos por intereses";
                break;
            case 615:
                $regimen="615 Régimen de los ingresos por obtención de premios";
                break;
            case 616:
                $regimen="616 Sin obligaciones fiscales";
                break;
            case 620:
                $regimen="620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos";
                break;
            case 621:
                $regimen="621 Incorporación Fiscal";
                break;
            case 622:
                $regimen="622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras";
                break;
            case 623:
                $regimen="623 Opcional para Grupos de Sociedades";
                break;
            case 624:
                $regimen="624 Coordinados";
                break;
            case 625:
                $regimen="625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas";
                break;
            case 626:
                $regimen="626 Régimen Simplificado de Confianza";
                break;
            case 628:
                $regimen="628 Hidrocarburos";
                break;
            case 629:
                $regimen="629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales";
                break;
            case 630:
                $regimen="630 Enajenación de acciones en bolsa de valores";
                break;
            default:
                $regimen="";
                break;
        }
        return $regimen;
    }
    public function getseleclikeunidadsat($search){
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search){
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function total_facturas_mes_num($anio,$mes){

        $strq = "SELECT COUNT(*) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes or Estado=0 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }
    function total_facturas_ventas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    



}