<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloPcataporte extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*
    SELECT pcp.txtNumDocumento,pcp.txtIdentificador,pcp.selectTipoComprobante,pcp.txtFechaInicio,pcp.hora_salida,pcp.txtFechaFin,pcp.hora_llegada,tf.txtNombre,pcp.id

FROM plantilla_carta_porte as pcp
INNER JOIN transporte_federal as tf on tf.id=pcp.selectVehiculos
WHERE pcp.activo=1;
    */
    function get_listado_plantilla($params){
        $columns = array( 
            0=>'pcp.txtNumDocumento',
            1=>'pcp.txtIdentificador',
            2=>'pcp.selectTipoComprobante',
            3=>'pcp.txtFechaInicio',
            4=>'pcp.hora_salida',
            5=>'pcp.txtFechaFin',
            6=>'pcp.hora_llegada',
            7=>'tf.txtNombre',
            8=>'pcp.id',
            9=>'pcp.id'
        );


        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('plantilla_carta_porte pcp');
        $this->db->join('transporte_federal tf', 'tf.id=pcp.selectVehiculos');
        $this->db->where(array('pcp.activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function get_listado_plantilla_total($params){
        $columns = array( 
            0=>'pcp.txtNumDocumento',
            1=>'pcp.txtIdentificador',
            2=>'pcp.selectTipoComprobante',
            3=>'pcp.txtFechaInicio',
            4=>'pcp.hora_salida',
            5=>'pcp.txtFechaFin',
            6=>'pcp.hora_llegada',
            7=>'tf.txtNombre',
            8=>'pcp.id',
            9=>'pcp.id'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('plantilla_carta_porte pcp');
        $this->db->join('transporte_federal tf', 'tf.id=pcp.selectVehiculos');
        $this->db->where(array('pcp.activo'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }
    function view_figuras_selected($idpcp,$fig){
        $strq="SELECT pcpf.id,op.*
            FROM plantilla_carta_porte_figuras as pcpf
            INNER JOIN operadores as op on op.id=pcpf.id_fig
            WHERE pcpf.id_pcp='$idpcp' AND pcpf.activo=1 AND op.TipoFigura='$fig'";
        $query = $this->db->query($strq);
        return $query;
    }
    
   

}