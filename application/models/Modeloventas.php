<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloventas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getventas($params){
        $sucursal=$params['sucursal'];
        $tipoventa=$params['tipoventa'];
        $activo=$params['activo'];
        $columns = array( 
            0=>'v.folio',
            1=>'suc.name_suc',
            2=>'p.nombre',
            3=>'v.reg',
            5=>'clif.razon_social',
            6=>'v.total', 
            7=>'v.id',
            8=>'fac.FacturasId',
            9=>'fac.Estado',
            10=>'fac.rutaXml',
            11=>'v.activo',
            12=>'v.tipo_venta',
            13=>'fac.FormaPago',
            14=>'(SELECT sum(vp.pago) from venta_erp_credito_pagos as vp where vp.ventaId=v.id) as pagocre',
            15=>'(SELECT sum(comd.ImpPagado) FROM f_complementopago as com INNER JOIN f_complementopago_documento as comd on comd.complementoId=com.complementoId WHERE com.Estado=1 AND comd.facturasId=fac.FacturasId) as pagocom',
            16=>'fac.faccliente',
            17=>'fac.pg_global',
            18=>'fac.fechatimbre',
            19=>'cli.correo',
            20=>'cli.nombre as name_cli',
            21=>'cli.calle',
            22=>'cli.colonia',
            23=>'cli.telefono',
            24=>'clif.rfc',
            25=>'og.nom_contacto',
            26=>'og.tel_contacto',
            27=>'v.id_razonsocial_new',
            28=>'clif2.razon_social as razon_social2',
        );
        $columnsss = array( 
            0=>'v.folio',
            1=>'suc.name_suc',
            2=>'p.nombre',
            3=>'v.reg',
            4=>'clif.razon_social',
            5=>'v.total', 
            6=>'v.id',
            7=>'fac.FacturasId',
            8=>'fac.Estado',
            9=>'fac.rutaXml',
            10=>'v.activo',
            11=>'v.tipo_venta',
            12=>'fac.FormaPago',
            13=>'cli.correo',
            14=>'cli.nombre',
            15=>'clif2.razon_social',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select. ", IFNULL(g.id,0) as id_garantia, (select count(tipo) as tot_servs from venta_erp_detalle vd where vd.tipo=3 and vd.activo=1 and vd.idventa=v.id) as tot_servs, (select count(tipo) as tot_servs from venta_erp_detalle vd where vd.tipo=0 and vd.activo=1 and vd.idventa=v.id) as tot_prods");
        $this->db->from('venta_erp v');
        $this->db->join('sucursal suc', 'suc.id=v.sucursal');
        $this->db->join('personal p', 'p.personalId=v.personalid');
        //$this->db->join('cliente_fiscales clif', 'clif.id =v.id_razonsocial');
        $this->db->join('cliente_fiscales clif', 'clif.id =v.id_razonsocial','left');
        $this->db->join('venta_fk_factura vf', 'vf.idventa=v.id and vf.activo=1','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId=vf.idfactura','left');
        $this->db->join('clientes cli', 'cli.id=v.id_cliente','left');
        $this->db->join('garantias g', 'g.id_venta=v.id and g.activo=1','left');
        $this->db->join('orden_garantia og', 'og.id_garantia=g.id','left');
        $this->db->join('cliente_fiscales clif2', 'clif2.id =v.id_razonsocial_new','left');
        $this->db->where(array('v.activo'=>$activo,'v.tipo_venta'=>$tipoventa));
        
        if($params['sucursal']!=0){
            $this->db->where(array('v.sucursal'=>$params['sucursal']));
        }else{
            $this->db->where(array('v.sucursal'=>$params['sucursal']));
        }
        
        if($params['idvendedor']!=0){
            $this->db->where(array('v.personalid'=>$params['idvendedor']));
        }
        
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_getventas($params){
        $sucursal=$params['sucursal'];
        $tipoventa=$params['tipoventa'];
        $activo=$params['activo'];
        $columns = array( 
            0=>'v.folio',
            1=>'suc.name_suc',
            2=>'p.nombre',
            3=>'v.reg',
            4=>'clif.razon_social',
            5=>'v.total', 
            6=>'v.id',
            7=>'fac.FacturasId',
            8=>'fac.Estado',
            9=>'fac.rutaXml',
            10=>'v.activo',
            11=>'v.tipo_venta',
            12=>'fac.FormaPago',
            13=>'cli.nombre',
            14=>'clif2.razon_social',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('venta_erp v');
        $this->db->join('sucursal suc', 'suc.id=v.sucursal');
        $this->db->join('personal p', 'p.personalId=v.personalid');
        $this->db->join('cliente_fiscales clif', 'clif.id =v.id_razonsocial','left');
        $this->db->join('venta_fk_factura vf', 'vf.idventa=v.id','left');
        $this->db->join('f_facturas fac', 'fac.FacturasId=vf.idfactura','left');
        $this->db->join('clientes cli', 'cli.id=v.id_cliente','left');
        $this->db->join('cliente_fiscales clif2', 'clif2.id =v.id_razonsocial_new','left');
        $this->db->where(array('v.activo'=>$activo,'v.tipo_venta'=>$tipoventa));
        
        if($sucursal>0){
            $this->db->where(array('v.sucursal'=>$sucursal));
        }else{
            $this->db->where(array('v.sucursal'=>$params['sucursal']));
        }    

        if($params['idvendedor']!=0){
            $this->db->where(array('v.personalid'=>$params['idvendedor']));
        }
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    public function ventas_detalles($idventa){
        $strq = "SELECT
            pro.id as id_prod,
            pro.idProducto,
            vd.cantidad,
            pro.nombre,
            pro.tipo as tipo_prod,
            IFNULL(pro2.id,0) as id_recarga,
            IFNULL(pro2.codigo,'') as codigo,
            IFNULL(pro2.capacidad,'') as capacidad,
            vd.precio_unitario,
            vd.descuento,
            vd.tipo,
            IFNULL(psl.lote,'') as lote,
            IFNULL(pss.serie,'') as serie,
            IFNULL(psl.id,'') as id_ps_lot,
            IFNULL(pss.id,'') as id_ps_ser,
            vd.id as id_vd,
            vd.id_ps_lote,
            vd.tipo,
            vd.id_ps_serie,
            vd.solicita_garantia,
            IFNULL(g.motivo,'') as motivo,
            IFNULL(s.clave,'') as clave,
            IFNULL(s.descripcion,'') as desc_serv,
            og.nom_solicita, og.dir_solicita, og.col_solicita, og.cel_solicita, og.tel_solicita, og.mail_solicita, og.rfc_solicita, og.nom_contacto, og.tel_contacto
        FROM venta_erp_detalle as vd 
        left JOIN productos as pro on pro.id=vd.idproducto
        left join productos_sucursales_lote psl on psl.id=vd.id_ps_lote
        left join productos_sucursales_serie pss on pss.id=vd.id_ps_serie
        left JOIN recargas as pro2 on pro2.id=vd.idproducto and vd.tipo=1
        left JOIN garantias as g on g.id_venta=vd.idventa and g.activo=1
        left JOIN orden_garantia og on og.id_garantia=g.id
        left JOIN servicios as s on s.id=vd.id_serv and vd.tipo=2
        WHERE vd.idventa=$idventa and (vd.tipo=0 or vd.tipo=1 or vd.tipo=2)";
        $query = $this->db->query($strq);
        return $query;
    }
    public function ventas_detalles_servicio($idventa){
        $strq = "SELECT vd.id,vd.cantidad,cats.descripcion,vd.precio_unitario,vd.incluye_iva,vd.descuento,cats.numero,cats.Unidad,cats.servicioId
        FROM venta_erp_detalle as vd
        INNER JOIN cat_servicios as cats on cats.id=vd.idproducto
        WHERE vd.activo=1 AND vd.tipo=3 AND vd.idventa='$idventa';";
        $query = $this->db->query($strq);
        return $query;
    }


    public function ventas_detalles_renta($idventa){
        $strq = "SELECT vd.id,vd.cantidad,s.clave,s.descripcion,vd.precio_unitario,vd.incluye_iva,vd.descuento,vd.tipo,vd.periodo, pss.serie
            FROM venta_erp_detalle as vd
            INNER JOIN rentas as r on r.id_venta=vd.idventa
            INNER JOIN productos_sucursales_serie as pss on pss.id=r.id_serie
            INNER JOIN servicios as s on s.id=vd.id_serv and vd.tipo=2
            WHERE vd.activo=1 AND vd.tipo=2 AND vd.idventa='$idventa';";
        $query = $this->db->query($strq);
        return $query;
    }

    public function ventas_formaspagos($idventa){
        $strq = "SELECT 
                        ffp.clave,
                        ffp.formapago_text,
                        vfp.monto,
                        vfp.ultimosdigitos
                FROM venta_erp_formaspagos as vfp 
                INNER JOIN f_formapago as ffp on ffp.clave=vfp.formapago 
                WHERE vfp.idventa=$idventa and vfp.activo=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function obtener_folio_v($sucursal){
        $strq = "SELECT max(v.folio_con)+1 as folio FROM venta_erp as v WHERE v.sucursal=$sucursal";
        $query = $this->db->query($strq);
        $folio=1;
        foreach ($query->result() as $item) {
            if($item->folio>0){
                $folio=$item->folio;
            }
        }
        return $folio;
    }

    public function obtener_folio_cotizacion($sucursal){
        $strq = "SELECT max(v.folio_con)+1 as folio FROM cotizacion as v WHERE v.sucursal=$sucursal";
        $query = $this->db->query($strq);
        $folio=1;
        foreach ($query->result() as $item) {
            if($item->folio>0){
                $folio=$item->folio;
            }
        }
        return $folio;
    }

    public function get_productos_stock_suc($valor,$tipo){
        $where='';
        if($tipo==1){
            $where='WHERE p.codigoBarras='.$valor;
        }else{
            $where='WHERE p.id='.$valor;
        }
        $strq = "SELECT p.id,p.nombre,0 as tipo, 
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=2),0) AS stock2,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=3),0) AS stock3,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=4),0) AS stock4,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=5),0) AS stock5,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=6),0) AS stock6,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=7),0) AS stock7,
        IFNULL((SELECT stock FROM productos_sucursales WHERE productoid=p.id AND sucursalid=8),0) AS stock8
        FROM productos as p 
        $where";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_productos_stock_lote($valor,$tipo){
        $where='';
        if($tipo==1){
            $where='WHERE p.codigoBarras='.$valor;
        }else{
            $where='WHERE p.id='.$valor;
        }
        $strq = "SELECT p.id,p.nombre,0 as tipo, 
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=2),0) AS stock2,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=3),0) AS stock3,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=4),0) AS stock4,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=5),0) AS stock5,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=6),0) AS stock6,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=7),0) AS stock7,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8),0) AS stock8
        FROM productos as p 
        $where";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_productos_stock_serie($valor,$tipo){
        $where='';
        if($tipo==1){
            $where='WHERE p.codigoBarras='.$valor;
        }else{
            $where='WHERE p.id='.$valor;
        }
        $strq = "SELECT p.id,p.nombre,0 as tipo, 
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=2),0) AS stock2,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=3),0) AS stock3,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=4),0) AS stock4,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=5),0) AS stock5,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=6),0) AS stock6,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=7),0) AS stock7,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 AND disponible=1 AND productoid=p.id AND sucursalid=8),0) AS stock8
            FROM productos as p 
            $where";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_tanques_stock_suc(){
        $strq = "SELECT r.id, r.codigo, r.tipo,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=2 and estatus=1),0) AS stock2,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=3 and estatus=1),0) AS stock3,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=4 and estatus=1),0) AS stock4,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=5 and estatus=1),0) AS stock5,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=6 and estatus=1),0) AS stock6,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=7 and estatus=1),0) AS stock7,
            IFNULL((SELECT stock FROM recargas WHERE tipo=0 AND sucursal=8 and estatus=1),0)  AS stock8
            FROM recargas as r 
            where tipo=0 AND estatus=1
            group by r.codigo";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_tanques_stock_suc2($id){
        $strq = "SELECT r.id, r.codigo, r.tipo, '".$id."' as id_suc, stock,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=r.id and ht.activo=1 and ht.idsucursal_sale=r.sucursal and ht.tipo=1 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant
        FROM recargas as r 
        where tipo=0 AND estatus=1 and sucursal=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function sumar_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock+$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function resta_sucursal($id,$sucursalid,$can){
        $strq = "UPDATE productos_sucursales set stock=stock-$can where productoid=$id AND sucursalid=$sucursalid";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function pagos_ventas($id){
        $strq = "SELECT vpago.id,vpago.fecha,ffmp.formapago_text,vpago.pago,vpago.comentario,per.nombre,vpago.reg
                FROM venta_erp_credito_pagos as vpago
                INNER JOIN f_formapago as ffmp on ffmp.clave=vpago.formapago
                LEFT JOIN personal as per on per.personalId=vpago.personalId
                WHERE vpago.activo=1 and vpago.ventaId ='$id'";
        $query = $this->db->query($strq);
        return $query; 
    }

    public function get_vendedores(){
        $strq = "SELECT p.nombre,p.personalId
                FROM usuarios as u 
                INNER JOIN personal as p on p.personalId=u.personalId 
                WHERE u.perfilId=3 AND p.estatus=1 and u.estatus=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function get_resumenventa($sucursal,$f1,$f2){
        $strq = "SELECT p.nombre,v.folio,v.folio,v.folio,c.razon_social AS cliente,ff.formapago,v.total,ban.name_ban,GROUP_CONCAT(ff.formapago,' / ',IFNULL(ban.name_ban,'') SEPARATOR '<br>') bancosrow
                FROM venta_erp as v 
                LEFT JOIN personal as p on p.personalId=v.personalid
                LEFT JOIN cliente_fiscales as c on c.id=v.id_razonsocial
                LEFT JOIN venta_erp_formaspagos as vf on vf.idventa=v.id 
                LEFT JOIN f_formapago as ff on ff.clave=vf.formapago 
                LEFT JOIN bancos as ban on ban.id=vf.banco
                WHERE v.activo=1 and v.sucursal=$sucursal AND v.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59' GROUP BY v.id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }

    public function get_resumenventa_tipopago($sucursal,$f1,$f2){
        $strq = "SELECT ff.formapago,SUM(v.total) AS total
                FROM venta_erp as v 
                LEFT JOIN personal as p on p.personalId=v.personalid
                LEFT JOIN cliente_fiscales as c on c.id=v.id_razonsocial
                LEFT JOIN venta_erp_formaspagos as vf on vf.idventa=v.id 
                LEFT JOIN f_formapago as ff on ff.clave=vf.formapago 
                WHERE v.activo=1 and v.sucursal=$sucursal AND v.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59' GROUP BY vf.formapago";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }

    public function get_resumenventa_total($sucursal,$f1,$f2){
        $strq = "SELECT SUM(v.total) AS total
                FROM venta_erp as v 
                WHERE v.activo=1 and v.sucursal=$sucursal AND v.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }

    function sumar_cliente_saldo($id,$saldo){
        $strq = "UPDATE clientes set saldo=saldo+$saldo where id=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function get_clausula()
    {
        $strq ="SELECT c.* 
            FROM cotizacion AS c 
            WHERE c.activo=1 ORDER BY c.id DESC LIMIT 1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productos_proveedor($id)
    {
        $strq ="SELECT cd.* 
            FROM cotizacion AS c 
            INNER JOIN cotizacion_clausulas AS cd ON cd.idcotizacion=c.id 
            WHERE cd.activo=1 AND cd.idcotizacion=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }


    function getVentasDetallePre($id){
        $strq = "SELECT vd.*, p.tipo as tipo_prod, IFNULL(ps.id,0) as id_ps, IFNULL(ren.id,0) as id_renta,
        IFNULL(pss.activo,0) as cant_serie, IFNULL(pss.id,'0') as idprod_serie, IFNULL(concat('SERIE: ',pss.serie),'') as serie,
        IFNULL(psl.cantidad,0) as cant_lote, IFNULL(psl.id,'0') as idprod_lote, IFNULL(concat('LOTE: ',psl.lote,' ',psl.caducidad,' (',psl.cantidad,')'),'') as lote
        from venta_erp_detalle vd
        join venta_erp v on v.id=vd.idventa
        left join productos p on p.id=vd.idproducto and vd.tipo=0
        left join productos_sucursales ps on ps.productoid=p.id and ps.sucursalid=v.sucursal
        LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote
        LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie
        left join recargas r on r.id = vd.idproducto and  vd.tipo = 1
        left join rentas ren on ren.id_venta=vd.idventa and vd.tipo = 2
        left join cat_servicios cs on cs.id=vd.idproducto and vd.tipo = 3
        WHERE vd.idventa=$id and vd.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    /*public function getProdsVentas($dia,$suc){
        $this->db->select('sum(vd.cantidad) as tot_prods, sum(bpc.cantidad) as tot_prods_bpc, sum(vd.cantidad + bpc.cantidad) as total_all_prods, p.id, p.nombre,ps.stock, vd.id_ps_lote,vd.id_ps_serie,
        IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8),0) AS lote8,
        IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8),0) as serie8,
        IFNULL(r.codigo,"") as cod_recarga, 
        IFNULL(count(tsh.id),0) as traslado_serie_cant,
        IFNULL(sum(tlh.cantidad),0) as traslado_lote_cant,
        IFNULL(t.status,2) as status_trasr, IFNULL(t2.status,2) as status_traslt,
        IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
        join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
        where ht.idproducto=p.id and ht.activo=1 and ht.idsucursal_sale=8 and ht.tipo=0 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant');
        $this->db->from('venta_erp v');
        $this->db->join('venta_erp_detalle vd','vd.idventa=v.id and vd.tipo=0 and vd.activo=1');
        $this->db->join('bitacora_prods_conce bpc','bpc.id_venta=v.id and vd.tipo=0 and vd.activo=1 and vd.id_ps_serie !=0','left');
        $this->db->join('productos p','p.id=vd.idproducto and vd.tipo=0 or p.id=bpc.id_prod');
        //$this->db->join('productos p2','p2.id=bpc.id_prod');
        //$this->db->join('productos_sucursales ps','ps.productoid=p.id and sucursalid='.$suc);
        $this->db->join('productos_sucursales ps','ps.productoid=p.id and sucursalid=8');
        $this->db->join('recargas r','r.id=vd.idproducto and vd.tipo=1',"left");
        //$this->db->join('productos_sucursales_lote psl', 'psl.productoid=p.id and psl.activo=1 and psl.sucursalid=8 and psl.cantidad>0','left');
        //$this->db->join('productos_sucursales_serie pss', 'pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=8','left');
        $this->db->join('productos_sucursales_lote psl','psl.id=vd.id_ps_lote and psl.activo=1 and psl.sucursalid=8',"left");
        $this->db->join('productos_sucursales_serie pss','pss.id=vd.id_ps_serie and pss.activo=1 and pss.disponible=1 and pss.sucursalid=8',"left");
        
        //$this->db->join('historial_transpasos ht','ht.idsucursal_sale=8 and ht.idproducto=vd.idproducto and ht.activo=1 and ht.tipo=0 and ht.status!=2 and ht.id_compra=0',"left");
        $this->db->join('traspasos_series_historial tsh','tsh.idseries=pss.id and tsh.activo=1',"left");
        $this->db->join('traspasos_lotes_historial tlh','tlh.idlotes=psl.id and tlh.activo=1',"left");
        $this->db->join('traspasos t','t.id=tsh.idtraspasos and t.status!=2',"left");
        $this->db->join('traspasos t2','t2.id=tlh.idtraspasos and t2.status!=2',"left");

        $this->db->where('v.activo','1');
        $this->db->where('v.sucursal',$suc);
        $this->db->where("v.reg BETWEEN '$dia 00:00:00' AND '$dia 23:59:59'");
        //$this->db->group_by("vd.idproducto");
        $this->db->order_by("p.tipo");
        $this->db->group_by("p.id");
        $query=$this->db->get();
        return $query->result();
    } */

    function getProdsVentas($dia,$suc){
        $strq ="select *,  sum(tot_prods) + sum(total_concen_prods) as gg from (SELECT sum(vd.cantidad) as tot_prods, 0 as total_concen_prods, p.id, p.idProducto, p.nombre, ps.stock, vd.id_ps_lote, vd.id_ps_serie, IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND productoid=p.id AND sucursalid=8), 0) AS lote8, 
            IFNULL((SELECT count(*) FROM productos_sucursales_serie pss2 
            join compra_erp c on c.id = pss2.idcompras and c.estatus=1
            WHERE pss2.activo=1 and pss2.productoid=vd.idproducto and pss2.disponible=1 and pss2.vendido=0 AND sucursalid=8), 0) as serie8, 
            IFNULL(r.codigo, '') as cod_recarga, /*IFNULL(sum(ht.cantidad), 0) as tot_trasht, */ IFNULL(count(tsh.id), 0) as traslado_serie_cant, IFNULL(sum(tlh.cantidad), 0) as traslado_lote_cant, IFNULL(t.status, 2) as status_trasr, IFNULL(t2.status, 2) as status_traslt, IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0 where ht.idproducto=p.id and ht.activo=1 and ht.idsucursal_sale=8 and ht.tipo=0 and ht.status=0 and ht.rechazado!=1), 0) as traslado_stock_cant, 0 as id_bpc, p.tipo
            FROM venta_erp v 
            JOIN venta_erp_detalle vd ON vd.idventa=v.id and vd.tipo=0 and vd.activo=1  
            JOIN productos p ON p.id=vd.idproducto and vd.tipo=0 
            JOIN productos_sucursales ps ON ps.productoid=p.id and sucursalid=8 
            LEFT JOIN recargas r ON r.id=vd.idproducto and vd.tipo=1 
            LEFT JOIN productos_sucursales_lote psl ON psl.id=vd.id_ps_lote and psl.activo=1 and psl.sucursalid=8 
            LEFT JOIN productos_sucursales_serie pss ON pss.productoid=vd.idproducto and pss.id=vd.id_ps_serie and pss.activo=1 and pss.disponible=1 and pss.sucursalid=8 
            LEFT JOIN traspasos_series_historial tsh ON tsh.idseries=pss.id and tsh.activo=1 
            LEFT JOIN traspasos_lotes_historial tlh ON tlh.idlotes=psl.id and tlh.activo=1 
            LEFT JOIN traspasos t ON t.id=tsh.idtraspasos and t.status!=2 
            LEFT JOIN traspasos t2 ON t2.id=tlh.idtraspasos and t2.status!=2 
            WHERE v.activo = '1' AND v.sucursal = $suc AND v.reg BETWEEN '$dia 00:00:00' AND '$dia 23:59:59' 
            GROUP BY vd.idproducto
            union
            SELECT 0 as tot_prods, sum(bpc.cantidad) as total_concen_prods, p.id, p.idProducto, p.nombre, ps.stock, 0 as id_ps_lote, 0 as id_ps_serie, IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote psl WHERE psl.activo=1 AND productoid=p.id AND sucursalid=8), 0) AS lote8, 
            IFNULL((SELECT count(*) FROM productos_sucursales_serie pser
            join compra_erp c on c.id = pser.idcompras and c.estatus=1
            WHERE pser.activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8), 0) as serie8, '' as cod_recarga, /*IFNULL(sum(ht.cantidad), 0) as tot_trasht, */  0 as traslado_serie_cant, 0 as traslado_lote_cant, 2 as status_trasr, 2 as status_traslt, IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0 where ht.idproducto=p.id and ht.activo=1 and ht.idsucursal_sale=8 and ht.tipo=0 and ht.status=0 and ht.rechazado!=1), 0) as traslado_stock_cant, bpc.id as id_bpc, p.tipo
            FROM venta_erp v 
            JOIN venta_erp_detalle vd ON vd.idventa=v.id and vd.tipo=0 and vd.activo=1 
            left join bitacora_prods_conce bpc on bpc.id_venta=v.id and vd.tipo=0 and vd.activo=1 and vd.id_ps_serie !=0
            JOIN productos p ON p.id=vd.idproducto and vd.id_ps_serie=0 and vd.tipo=0 or bpc.id_prod=p.id and p.tipo=0
            JOIN productos_sucursales ps ON ps.productoid=p.id and sucursalid=8 
            WHERE v.activo = '1' AND v.sucursal = $suc AND v.reg BETWEEN '$dia 00:00:00' AND '$dia 23:59:59' 
            GROUP BY p.id) as datos GROUP BY id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function generarCorte($suc,$fechainicio,$forma){
        $strq="SELECT fmp.clave as clavefm, fmp.formapago_alias,ban.id as bancoid, ban.ban_clave,IFNULL(ban.name_ban,'') as name_ban, SUM(vfmp.monto) as monto
                FROM f_formapago as fmp
                INNER JOIN venta_erp_formaspagos as vfmp ON vfmp.formapago=fmp.clave
                INNER JOIN venta_erp as ven on ven.id=vfmp.idventa AND ven.sucursal=$suc AND ven.reg BETWEEN '$fechainicio 00:00:00' AND '$fechainicio 23:59:59'
                LEFT JOIN bancos as ban on ban.id=vfmp.banco 
                WHERE ven.activo=1 and fmp.clave=$forma
                GROUP BY fmp.clave"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function getVentasDetalleCotiza($id){
        $strq = "SELECT cd.*, p.tipo as tipo_prod, IFNULL(ps.incluye_iva,0) as incluye_iva, IFNULL(ps.iva,0) as iva, IFNULL(cs.iva,0) as iva_serv,
        IFNULL(pss.activo,0) as cant_serie, IFNULL(pss.id,'0') as idprod_serie, IFNULL(concat('SERIE: ',pss.serie),'') as serie,
        IFNULL(psl.cantidad,0) as cant_lote, IFNULL(psl.id,'0') as idprod_lote, IFNULL(concat('LOTE: ',psl.lote,' ',psl.caducidad,' (',psl.cantidad,')'),'') as lote
        from cotizacion_detalle cd
        left join productos p on p.id=cd.idproducto and cd.tipo=0
        LEFT JOIN productos_sucursales AS ps ON ps.productoid=p.id
        LEFT JOIN productos_sucursales_lote AS psl ON psl.id=cd.id_ps_lote and cd.tipo=0
        LEFT JOIN productos_sucursales_serie AS pss ON pss.id=cd.id_ps_serie and cd.tipo=0
        LEFT JOIN recargas AS r ON r.id=cd.idproducto and cd.tipo=1 
        LEFT JOIN cat_servicios AS cs ON cs.id=cd.idproducto and cd.tipo=3
        WHERE cd.idcotizacion=$id and cd.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getPersonalSucus($id){
        $this->db->select('IFNULL(pc.correo,"") as correo_comp, IFNULL(pa.correo,"") as correo_adm, s.name_suc, v.folio');
        $this->db->from("usuarios uc");
        $this->db->join('personal pc', 'pc.personalId=uc.personalId and pc.estatus=1','left');
        $this->db->join('venta_erp v', 'v.id='.$id.'');
        $this->db->join('sucursal s', 's.id=v.sucursal');
        $this->db->join('usuarios ua', 'ua.estatus=1 and ua.perfilId=1','left');    
        $this->db->join('personal pa', 'pa.personalId=ua.personalId and pa.estatus=1','left');
        $this->db->where('uc.perfilId',"4");
        $this->db->where('uc.estatus',"1");
        $query=$this->db->get(); 
        return $query->result();
    }

    function restaInsumo_sucursal($id,$cant){
        $strq = "UPDATE productos_sucursales set stock=stock-$cant where id=$id";
        $query = $this->db->query($strq);
    }

    function sumaInsumo_sucursal($id,$cant){
        $strq = "UPDATE productos_sucursales set stock=stock+$cant where id=$id";
        $query = $this->db->query($strq);
    }

    function getVentasCancel($id,$idsuc,$fecha,$tipo){
        if($tipo==1){ //servicios
            $andjoin=" AND (vd.tipo = 2 OR vd.tipo = 3)";
        }
        if($tipo==2){ //prods
            $andjoin=" AND (vd.tipo = 0 OR vd.tipo = 1)";
        }
        //$this->db->select('v.folio, v.subtotal, v.iva, v.total, v.reg, v.delete_reg, delete_mot, IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_cancel');
        $this->db->select('v.folio, v.subtotal, v.iva, v.total, v.reg, v.delete_reg, delete_mot');
        $this->db->from("venta_erp v");
        $this->db->join('venta_erp_detalle vd','vd.idventa = v.id AND vd.activo = 1 '.$andjoin.'');
        if($id>0){
            $this->db->where('v.personalid',$id); 
        }
        if($idsuc>0){
            $this->db->where('v.sucursal',$idsuc);
        }
        $this->db->where($fecha);
        $this->db->where('v.activo',"0");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getAllVendedoresTop($w_mes,$w_eje,$w_suc,$limit){
        $limitsql="";
        if($limit>0){
            $limitsql="LIMIT 10";
        }
        $strq ="SELECT nombre, personalId, sucursal, name_suc, SUM(subtotal) AS subtotal, SUM(iva) AS iva, SUM(total) AS total, comision, comision_tecnico, tipo, tipo_tecnico
            FROM(
                /*SELECT 
                p.nombre,
                    p.personalId,
                    u.sucursal,
                    suc.name_suc,
                    p.comision,
                    p.comision_tecnico,
                    vd.tipo,
                    pf.tipo_tecnico,
                    SUM(vd.cantidad) AS sub_num_servs,
                    IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal,
                    IFNULL(SUM(CASE 
                        WHEN cs.iva = 1 THEN vd.cantidad * vd.precio_unitario * 0.16 
                        ELSE 0 
                    END),0) AS iva,
                    IFNULL(SUM(CASE 
                        WHEN cs.iva > 0 THEN vd.cantidad * vd.precio_unitario * 1.16 
                        ELSE vd.cantidad * vd.precio_unitario 
                    END),0) AS total
                    FROM usuarios AS u 
                    join perfiles pf on pf.perfilId=u.perfilId and pf.tipo_tecnico=1
                    INNER JOIN personal AS p ON p.personalId = u.personalId 
                    INNER JOIN sucursal AS suc ON suc.id = u.sucursal
                    join mtto_externo as mtt on mtt.id_venta!=0 and mtt.activo=1
                    join venta_erp as verp on verp.sucursal=u.sucursal and verp.id=mtt.id_venta and verp.activo=1 $w_mes
                    INNER JOIN 
                        venta_erp_detalle vd ON vd.idventa = verp.id 
                        AND vd.activo = 1 
                        AND (vd.tipo = 2 OR vd.tipo = 3)
                    INNER JOIN cat_servicios cs ON cs.id = vd.idproducto
                    WHERE p.estatus=1 and u.estatus=1 and pf.tipo_tecnico=1 $w_eje $w_suc
                    GROUP BY p.personalId, u.sucursal

            UNION ALL */
                SELECT 
                p.nombre,
                p.personalId,
                u.sucursal,
                suc.name_suc,
                p.comision,
                p.comision_tecnico,
                vd.tipo,
                0 as tipo_tecnico,
                0 AS sub_num_servs,
                IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 0.16
                    ELSE 0
                END),0) AS iva,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
                    ELSE vd.cantidad * vd.precio_unitario
                END),0) AS total
                FROM usuarios AS u 
                INNER JOIN personal AS p on p.personalId=u.personalId 
                INNER JOIN sucursal AS suc on suc.id=u.sucursal
                INNER join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                INNER join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
                INNER join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal and ps.activo=1
                WHERE p.estatus=1 and u.estatus=1 $w_eje $w_suc
                GROUP BY p.personalId, u.sucursal

            UNION ALL

                SELECT   /* recargas */
                p.nombre,
                p.personalId,
                u.sucursal,
                suc.name_suc,
                p.comision,
                p.comision_tecnico,
                vd2.tipo,
                0 as tipo_tecnico,
                0 AS sub_num_servs,
                IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal,
                0 AS iva,
                IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total
                FROM usuarios AS u 
                INNER JOIN personal AS p on p.personalId=u.personalId 
                INNER JOIN sucursal AS suc on suc.id=u.sucursal
                inner join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                inner join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
                WHERE p.estatus=1 and u.estatus=1 $w_eje $w_suc
                GROUP BY p.personalId, u.sucursal
            ) as datos 
            GROUP BY personalId, sucursal
            ORDER BY subtotal DESC
            ".$limitsql." ";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getVentasProds($id,$idsuc,$fecha){
        $this->db->select('v.reg, p.idProducto as codigo, p.nombre, sum(vd.cantidad) as sub_vendido, vd.precio_unitario');
        $this->db->from("venta_erp v");
        $this->db->join('venta_erp_detalle vd','vd.idventa = v.id AND vd.activo = 1 AND vd.tipo = 0');
        $this->db->join('productos p','p.id=vd.idproducto');
        if($id>0){
            $this->db->where('v.personalid',$id); 
        }        
        if($idsuc>0){
            $this->db->where('v.sucursal',$idsuc);
        }
        $this->db->where($fecha);
        $this->db->where('v.activo',"1");
        //$this->db->order_by('v.reg',"desc");
        $this->db->group_by("vd.idproducto");
        $query1 = $this->db->get_compiled_select();

        $this->db->select('v.reg, r.codigo, concat("Recarga de oxígeno de ", r.capacidad," l") as nombre, sum(vd.cantidad) as sub_vendido, vd.precio_unitario');
        $this->db->from("venta_erp v");
        $this->db->join('venta_erp_detalle vd','vd.idventa = v.id AND vd.activo = 1 AND vd.tipo = 1');
        $this->db->join('recargas r','p.id=vd.idproducto');
        if($id>0){
            $this->db->where('v.personalid',$id); 
        }
        if($idsuc>0){
            $this->db->where('v.sucursal',$idsuc);
        }
        $this->db->where($fecha);
        $this->db->where('v.activo',"1");
        //$this->db->order_by('v.reg',"desc");
        $this->db->group_by("vd.idproducto");
        $query2 = $this->db->get_compiled_select();

        $this->db->select('v.reg, cs.numero as codigo, cs.descripcion as nombre, sum(vd.cantidad) as sub_vendido, vd.precio_unitario');
        $this->db->from("venta_erp v");
        $this->db->join('venta_erp_detalle vd','vd.idventa = v.id AND vd.activo = 1 AND (vd.tipo = 2 OR vd.tipo = 3)');
        $this->db->join('cat_servicios cs','cs.id=vd.idproducto');
        if($id>0){
            $this->db->where('v.personalid',$id); 
        }
        if($idsuc>0){
            $this->db->where('v.sucursal',$idsuc);
        }
        $this->db->where($fecha);
        $this->db->where('v.activo',"1");
        //$this->db->order_by('v.reg',"desc");
        $this->db->group_by("vd.idproducto");
        $query2 = $this->db->get_compiled_select();

        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }

    function getVentasComisServs($w_eje,$w_suc,$mes){
        $strq ="SELECT  p.nombre, p.personalId, u.sucursal, suc.name_suc, p.comision_tecnico,
        SUM(vd.cantidad) AS sub_num_servs,
        SUM(vd.cantidad * vd.precio_unitario) AS subtotal_servs,
        SUM(CASE 
            WHEN cs.iva = 1 THEN vd.cantidad * vd.precio_unitario * 0.16 
            ELSE 0 
        END) AS iva_servs,
        SUM(CASE 
            WHEN cs.iva > 0 THEN vd.cantidad * vd.precio_unitario * 1.16 
            ELSE vd.cantidad * vd.precio_unitario 
        END) AS total_servs
        FROM usuarios AS u 
        join perfiles pf on pf.perfilId=u.perfilId and pf.tipo_tecnico=1
        INNER JOIN personal AS p ON p.personalId = u.personalId 
        INNER JOIN sucursal AS suc ON suc.id = u.sucursal
        join mtto_externo as mtt on mtt.id_venta!=0 and mtt.activo=1
        join venta_erp as verp on verp.sucursal=u.sucursal /*and verp.personalid=p.personalId */ and verp.id=mtt.id_venta and verp.activo=1 $mes
        JOIN venta_erp_detalle vd ON vd.idventa = verp.id 
            AND vd.activo = 1 
            AND vd.tipo = 3
        LEFT JOIN cat_servicios cs ON cs.id = vd.idproducto
        WHERE p.estatus=1 and u.estatus=1 and pf.tipo_tecnico=1 $w_eje $w_suc
        GROUP BY p.personalId, u.sucursal
        ORDER BY sub_num_servs DESC";

        $query = $this->db->query($strq);
        return $query->result();
    }
         
    function getVentasComisProds($w_eje,$w_suc,$mes){
        /*$strq ="SELECT p.nombre, p.personalId, u.sucursal, suc.name_suc, ps.incluye_iva, p.comision,
        sum(verp.subtotal) subtotal_general,
        IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_prods,
        IFNULL(SUM(CASE
            WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 0.16
            ELSE 0
        END),0) AS iva_prods,
        IFNULL(SUM(CASE
            WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
            ELSE vd.cantidad * vd.precio_unitario
        END),0) AS total_prods,

        IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal_recarga,
        0 AS iva_recarga,
        IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total_recarga
        FROM usuarios AS u 
        INNER JOIN personal AS p on p.personalId=u.personalId 
        INNER JOIN sucursal AS suc on suc.id=u.sucursal
        inner join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $mes
        left join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
        left join productos prod on prod.id=vd.idproducto
        left join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal

        left join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
        WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
        group by p.personalId,u.sucursal
        ORDER BY (IFNULL(SUM(vd.cantidad * vd.precio_unitario), 0) + IFNULL(SUM(vd2.cantidad * vd2.precio_unitario), 0)) DESC";*/
        $strq="SELECT nombre, personalId, comision, sucursal, name_suc, incluye_iva, SUM(subtotal_general) AS subtotal_general, SUM(subtotal_prods) AS subtotal_prods, SUM(iva_prods) AS iva_prods, SUM(total_prods) AS total_prods, sum(subtotal_recarga) as subtotal_recarga, sum(iva_recarga) as iva_recarga, sum(total_recarga) as total_recarga 
            FROM(
            SELECT
                p.nombre,
                p.personalId,
                p.comision,
                u.sucursal,
                suc.name_suc,
                ps.incluye_iva,
                sum(verp.subtotal) subtotal_general,
                IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_prods,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 and vd.tipo=0 THEN vd.cantidad * vd.precio_unitario * 0.16
                    ELSE 0
                END),0) AS iva_prods,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
                    ELSE vd.cantidad * vd.precio_unitario
                END),0) AS total_prods,
                0 as subtotal_recarga,
                0 as iva_recarga,
                0 as total_recarga
                FROM usuarios AS u 
                INNER JOIN personal AS p on p.personalId=u.personalId 
                INNER JOIN sucursal AS suc on suc.id=u.sucursal
                left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $mes
                left join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
                left join productos prod on prod.id=vd.idproducto
                left join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal
                WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
                group by p.personalId,u.sucursal
            union 
                SELECT
                    p.nombre,
                    p.personalId,
                    p.comision,
                    u.sucursal,
                    suc.name_suc,
                    0 as incluye_iva,
                    0 as subtotal_general,
                    0 as subtotal_prods,
                    0 as iva_prods,
                    0 as total_prods,
                    IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal_recarga,
                    0 AS iva_recarga,
                    IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total_recarga
                    FROM usuarios AS u 
                    INNER JOIN personal AS p on p.personalId=u.personalId 
                    INNER JOIN sucursal AS suc on suc.id=u.sucursal
                    left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $mes
                    left join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
                    WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
                    group by p.personalId,u.sucursal
            ) as datos 
            GROUP BY personalId, sucursal
            ORDER BY (subtotal_prods + subtotal_recarga) DESC";
        $query = $this->db->query($strq);
        return $query->result();
    }

    /* ************************************************** */
    function getTotalVentas($idsuc,$fechai,$fechaf){
        $this->db->select('SUM(v.subtotal) as subtotal, SUM(v.iva) as iva, SUM(v.total) as total, s.name_suc');
        $this->db->from("venta_erp v");
        $this->db->join('sucursal s', 's.id=v.sucursal');

        $this->db->where("v.reg BETWEEN '$fechai 00:00:00' AND '$fechaf 23:59:59'");
        if($idsuc>0){
            $this->db->where("v.sucursal",$idsuc);
        }
        $this->db->where('v.activo',"1");
        $this->db->group_by('v.sucursal');
        $this->db->order_by('s.orden',"ASC");
        $query=$this->db->get();
        return $query->result();
    }
    
    function getTotalUtilidad($idsuc,$fechai,$fechaf){
        $this->db->select('SUM(DISTINCT v.subtotal) AS subtotal, 
            SUM(DISTINCT v.total) AS total, 
            s.name_suc, 
            SUM(DISTINCT p.costo_compra) AS sub_compra, 
            SUM(DISTINCT CASE
                WHEN p.incluye_iva_comp > 0 AND p.iva_comp > 0 THEN p.costo_compra * 1.16
                ELSE p.costo_compra
            END) AS sub_compra_iva');
        $this->db->from("venta_erp v");
        $this->db->join('venta_erp_detalle vd', 'vd.idventa=v.id and vd.activo=1 and vd.tipo=0');
        $this->db->join('productos p', 'p.id=vd.idproducto');
        $this->db->join('sucursal s', 's.id=v.sucursal');

        $this->db->where("v.reg BETWEEN '$fechai 00:00:00' AND '$fechaf 23:59:59'");
        if($idsuc>0){
            $this->db->where("v.sucursal",$idsuc);
        }
        $this->db->where('v.activo',"1");
        $this->db->group_by('v.sucursal');
        $this->db->order_by('s.orden',"ASC");
        $query=$this->db->get();
        return $query->result();
    }

    function getProductosConcen(){
        $this->db->select('pc.*, p.id as id_prod');
        $this->db->from("productos_concentrador pc");
        $this->db->join('productos p', 'p.idProducto=pc.codigo_prod');
        $this->db->where('pc.estatus',"1");
        $query=$this->db->get();
        return $query->result();
    }

    function getInsumosServ($id,$idventa,$suc){
        $this->db->select('bi.*,p.nombre as name_ins, p.tipo, ps.stock, cs.descripcion, cs.precio_siva');
        $this->db->from("bitacora_ins_servs_ventas bi");
        $this->db->join('productos p', 'p.id=bi.id_insumo');
        $this->db->join('productos_sucursales ps', 'ps.productoid=p.id and ps.sucursalid='.$suc.'');
        $this->db->join('cat_servicios cs', 'cs.id=bi.id_serv');
        $this->db->where('bi.cantidad > 0');
        $this->db->where('bi.asignado',"0");
        $this->db->where('bi.id_venta',$idventa);
        $this->db->where('bi.id_serv',$id);
        $query=$this->db->get();
        return $query->result();
    }

    function getProdVentaId($id,$id_venta,$tipo,$id_prod_suc,$tipo_prod,$idprod_det){
        $this->db->select('vd.cantidad');
        $this->db->from("venta_erp_detalle vd");
        $this->db->where('vd.idproducto',$id);
        $this->db->where('vd.idventa',$id_venta);
        $this->db->where('vd.tipo',$tipo);
        if($tipo_prod==1){
            $where_getc = array("id_ps_serie"=>$idprod_det);
            $this->db->where($where_getc);
        }
        if($tipo_prod==2){
            $where_getc = array("id_ps_lote"=>$idprod_det);
            $this->db->where($where_getc);
        }
        
        $query=$this->db->get();
        return $query;
    }


}