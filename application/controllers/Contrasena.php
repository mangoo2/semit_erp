<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contrasena extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloventas');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,22);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=22;
        $data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        $data['get_sucuarsales']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1) );
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('contrasena/index');
        $this->load->view('templates/footer');
        $this->load->view('contrasena/indexjs');
    }

    public function generarcontrasena(){
        $id=$this->input->post('id');
        $data['activo']=0;
        $this->General_model->edit_record('id',$id,$data,'historial_alerta');
        $codigo=$this->generarCodigo();
        $datos['codigo']=$codigo;
        $datos['idpersonal']=$this->idpersonal;
        $this->General_model->add_record('contrasena',$datos);  
        echo $codigo;
    }

    function generarCodigo(){
        $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitud = 5;
        return substr(str_shuffle($caracteres_permitidos), 0, $longitud);
    }
}
