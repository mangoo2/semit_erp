<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelProductos');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechacomp = date('Y-m-d H:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->tipo_tecnico=$this->session->userdata('tipo_tecnico');
            $this->con_insumos=$this->session->userdata('con_insumos');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,3);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        
        $data['perfilid']=$this->perfilid;
        $data['sucs']=$this->General_model->getselectwhere_orden_asc('sucursal',array('activo'=>1),"orden");
        $data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        $data['refe']=$this->ModeloCatalogos->getselectwheren('referencias',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('productos/index');
        $this->load->view('templates/footer');
        $this->load->view('productos/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        $data["perfilid"]=$this->perfilid;
        if($id==0){
            $data['id'] = 0;
            $data['idProducto']='';
            $data['resumen']='';
            $data['descripcion'] = '';
            $data['referencia']='';
            $data["concentrador"]=0;
            $data["tanque"]=0;
            $data["capacidad"]="";
            $data['categoria']=0;

            $data['mostrar_pagina']='';
            $data['stock']='';
            $data['stockmin']='';
            $data['stockmax']='';
            $data['ubicacion']='';
            $data['ancho']='';
            $data['alto']='';
            $data['largo']='';
            $data['peso']='';
            $data['tiempo_carga']='';
            $data['costo_envio']='';
            $data['envio_gratis']='';
            $data['precio_sin_iva']='';
            $data['precio_con_iva']='';
            $data['descuento']='';
            $data['tipo_descuento']='';
            $data['nombre']='';
            $data['codigoBarras']='';
            $data['activo']='Y';
            $data['unidadMedida']='';
            $data['comision']='';
            $unidad_sat='';
            $servicioId_sat='';
            $data['idproveedor']='';
            $data['proveedor']='';
            $data['costo_compra']='';
            $data['incluye_iva_comp']='';
            $data['iva_comp']='';

            $data['tipo']="";
            $data['observaciones']='';
            $data['isr']=0;
            $data['porc_isr']="";
            $data['ieps']=0;
            $data['porc_ieps']="";
        }else{
            $resul=$this->General_model->getselectwhere('productos','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['idProducto']=$item->idProducto;
                $data['resumen']=$item->resumen;
                $data['descripcion']=$item->descripcion;
                $data['referencia']=$item->referencia;      
                $data['categoria']=$item->categoria;
                $data["concentrador"]=$item->concentrador;
                $data["tanque"]=$item->tanque;
                $data["capacidad"]=$item->capacidad;
                
                $data['mostrar_pagina']=$item->mostrar_pagina;
                $data['stock']=$item->stock;
                $data['stockmin']=$item->stockmin;
                $data['stockmax']=$item->stockmax;
                $data['ubicacion']=$item->ubicacion;
                $data['ancho']=$item->ancho;
                $data['alto']=$item->alto;
                $data['largo']=$item->largo;
                $data['peso']=$item->peso;
                $data['tiempo_carga']=$item->tiempo_carga;
                $data['costo_envio']=$item->costo_envio;
                $data['envio_gratis']=$item->envio_gratis;
                $data['precio_sin_iva']=$item->precio_sin_iva;
                $data['precio_con_iva']=$item->precio_con_iva;
                $data['descuento']=$item->descuento;
                $data['tipo_descuento']=$item->tipo_descuento;
                $data['nombre']=$item->nombre;
                $data['codigoBarras']=$item->codigoBarras;
                $data['activo']=$item->activo;
                $data['unidadMedida']=$item->unidadMedida;
                $data['comision']=$item->comision;
                $unidad_sat=$item->unidad_sat;
                $data['costo_compra']=$item->costo_compra;
                $data['incluye_iva_comp']=$item->incluye_iva_comp;
                $data['iva_comp']=$item->iva_comp;
                $data['tipo']=$item->tipo;
                $servicioId_sat=$item->servicioId_sat;
                $resulp=$this->General_model->getselectwhere('proveedores','id',$item->idproveedor);
                $data['idproveedor']=0;
                $data['proveedor']='';
                $data['observaciones']=$item->observaciones;
                foreach ($resulp as $x) {
                    $data['idproveedor']=$x->id;
                    $data['proveedor']=$x->nombre;
                }
                $data['isr']=$item->isr;
                $data['porc_isr']=$item->porc_isr;
                //log_message("error","porc_isr: ".$item->porc_isr);
                $data['ieps']=$item->ieps;
                $data['porc_ieps']=$item->porc_ieps;
            }

        }   
        //log_message("error","tipo: ".$data["tipo"]);
        $data['suc_pro']=$this->General_model->get_suc_prod($id);
        $data['refe']=$this->ModeloCatalogos->getselectwheren('referencias',array('activo'=>1));

        $data['f_u']=$this->ModeloCatalogos->getselectwheren('f_unidades',array('Clave'=>$unidad_sat));
        $data['f_s']=$this->ModeloCatalogos->getselectwheren('f_servicios',array('Clave'=>$servicioId_sat));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('productos/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/formjs');
    }
    
    public function registrar_datos_ab(){
        $data=$this->input->post();
        $nombre=$data['nombre'];
        //log_message('error',json_encode($data));
        $id=$data['id'];
        $data['personalid']=$this->idpersonal;
        if($this->session->userdata('perfilid')!=1){
            unset($data["mostrar_pagina"]);
        }
         
        $resulpro=$this->General_model->getselectwhere('productos','nombre',$nombre);
        $nombrepro='';
        foreach ($resulpro as $pro) {
            $nombrepro=$pro->nombre;
        }
        $validar=0;
        if($nombre==$nombrepro){
            $validar=1;
        }else{
            $validar=0;
        }

        if($id!=0){
            $validar=0;
        }
        
        if(isset($data['mostrar_pagina'])){
            //$data['mostrar_pagina']=1;
        }else{
            //$data['mostrar_pagina']=0;
        }
        if(isset($data['activo'])){
            $data['activo']='N';
        }else{
            $data['activo']='Y';
        }
        if(isset($data['referencia'])){
            $data['referencia']=implode(',',$data['referencia']);
        }else{
            $data['referencia']='';
        }
        if($id==0){
            $id=$this->General_model->add_record('productos',$data);

            $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>1,'id_item'=>$id,'table_name'=>'productos','descripcion'=>'Se inserto '.$data['idProducto'].' '.$data['nombre'],'personal'=>$this->idpersonal));
        }else{
            $this->General_model->edit_record('id',$id,$data,'productos');
            $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>2,'id_item'=>$id,'table_name'=>'productos','descripcion'=>'Se actualizo '.$data['idProducto'].' '.$data['nombre'],'personal'=>$this->idpersonal));
        }
        $resul=$this->General_model->getselectwhereall('productos_files',array('idproductos'=>0,'activo'=>1,'idpersonal'=>$this->idpersonal));
        foreach ($resul as $pro) {
            $datafile['idproductos']=$id;
            $this->General_model->edit_record('id',$pro->id,$datafile,'productos_files');
        }

        $info = array('validar'=>$validar,'id'=>$id); 
        echo json_encode($info);
    }
    function registrar_datos_web(){
        $data=$this->input->post();
        $id=$data['idreg'];
        unset($data['idreg']);
        $this->General_model->edit_record('id',$id,$data,'productos');

    }
    function registrar_datos_sat(){
        $data=$this->input->post();
        $id=$data['idreg'];
        unset($data['idreg']);
        $this->General_model->edit_record('id',$id,$data,'productos');
    }

    function registrar_datos_compra(){
        $data=$this->input->post();
        $id=$data['idreg'];
        unset($data['idreg']);
        $this->General_model->edit_record('id',$id,$data,'productos');
    }

    public function registrar_datos_ca(){
        $data=$this->input->post();
        $idpro=$data['idpro'];
        $cantidades=$data['cantidades'];
        $DATAc = json_decode($cantidades);
        for ($i=0;$i<count($DATAc);$i++) {
            $idrow = $DATAc[$i]->idrow;
            if($idrow>0){
                $data_array=array(
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                );
                //$this->ModelProductos->update_productos_sucursales($DATAc[$i]->suc,$idpro,$DATAc[$i]->stock,$DATAc[$i]->stockmi,$DATAc[$i]->stockma);
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrow));

                $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>2,'id_item'=>$idrow,'table_name'=>'productos_sucursales','descripcion'=>'Se actualizo los stock ','personal'=>$this->idpersonal));
            }else{
                $result_c=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
                if($result_c->num_rows()>0){
                    $result_c->row();
                    $idrowc=$result_c->id;
                    $data_array=array(
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                    );
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrowc));
                    $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>2,'id_item'=>$idrowc,'table_name'=>'productos_sucursales','descripcion'=>'Se actualizo los stock ','personal'=>$this->idpersonal));
                }else{
                    $data_array=array(
                                'productoid'=>$idpro,
                                'sucursalid'=>$DATAc[$i]->suc,
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                    );
                    $this->ModeloCatalogos->Insert('productos_sucursales',$data_array);
                }
            }
        }
    }

    public function registrar_datos_ca2(){
        $data=$this->input->post();
        $idpro=$data['idpro'];
        $cantidades=$data['cantidades'];
        $DATAc = json_decode($cantidades);
        for ($i=0;$i<count($DATAc);$i++) {
            //$idrow = $DATAc[$i]->idrow;
            //if($idrow>0){
                $data_array=array(
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                );
                $data_array_insert=array(
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                'sucursalid'=>$DATAc[$i]->suc,
                                'productoid'=>$idpro

                );
            //$this->ModelProductos->update_productos_sucursales($idpro,$DATAc[$i]->suc,$DATAc[$i]->stock,$DATAc[$i]->stockmi,$DATAc[$i]->stockma);
            if($DATAc[$i]->idrow!=0){
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
            }else{
                $this->ModeloCatalogos->Insert('productos_sucursales',$data_array_insert);
            }

            $DATAcd=$DATAc[$i]->serie_nume;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data_array_serie=array(
                    'productoid'=>$idpro,
                    'sucursalid'=>$DATAcd[$j]->sucursal,
                    'serie'=>$DATAcd[$j]->serie,
                    'reg'=>$this->fechacomp,
                );
                
                if($DATAcd[$j]->id>0){
                    if($DATAcd[$j]->editar_x==1){
                        $data_array_serie=array(
                            'productoid'=>$idpro,
                            'sucursalid'=>$DATAcd[$j]->sucursal,
                            'serie'=>$DATAcd[$j]->seriey,
                            //'reg'=>$this->fechacomp,
                        );
                        $data_array_serie_edit=array(
                            'idproductos_sucursales_serie'=>$DATAcd[$j]->id,
                            'seria_a'=>$DATAcd[$j]->serie,
                            'serie_d'=>$DATAcd[$j]->seriey,
                            'idpersonal'=>$this->idpersonal,
                        );
                        $this->ModeloCatalogos->Insert('productos_sucursales_serie_historial',$data_array_serie_edit);
                    }
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',$data_array_serie,array('id'=>$DATAcd[$j]->id));
                    $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>2,'id_item'=>$DATAcd[$j]->id,'table_name'=>'productos_sucursales_serie','descripcion'=>'Se actualizo las series','personal'=>$this->idpersonal));
                }else{
                    $this->ModeloCatalogos->Insert('productos_sucursales_serie',$data_array_serie);
                }
            }


            $DATAlt=$DATAc[$i]->lote_nume;
            for ($j=0;$j<count($DATAlt);$j++) {
                $data_array_lote=array(
                    'productoid'=>$idpro,
                    'sucursalid'=>$DATAlt[$j]->sucursal,
                    'cantidad'=>$DATAlt[$j]->cantidad,
                    'lote'=>$DATAlt[$j]->lote,
                    'caducidad'=>$DATAlt[$j]->caducidad,
                    'cod_barras'=>$DATAlt[$j]->cod_barras
                );
                if($DATAlt[$j]->id>0){
                    if($data_array_lote["cod_barras"]==""){
                        /*$this->load->library('barcode_manager');
                        $code = $this->obtener_codigo_desde_bd($idpro);
                        $center_x = 220;
                        $center_y = 50;
                        $height = 100;
                        $width = 440;
                        $bars_height = 50;
                        $bars_width = 2;
                        $this->barcode_manager->create_barcode($code, $center_x, $center_y, $width, $height, $bars_width, $bars_height);*/
                    }
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_lote',$data_array_lote,array('id'=>$DATAlt[$j]->id));
                    $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>2,'id_item'=>$DATAlt[$j]->id,'table_name'=>'productos_sucursales_lote','descripcion'=>'Se actualizo el lote','personal'=>$this->idpersonal));
                }else{
                    $this->ModeloCatalogos->Insert('productos_sucursales_lote',$data_array_lote);
                }
            }
            /*}else{
                $result_c=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
                if($result_c->num_rows()>0){
                    $result_c->row();
                    $idrowc=$result_c->id;
                    $data_array=array(
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                    );
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrowc));
                }else{
                    $data_array=array(
                                'productoid'=>$idpro,
                                'sucursalid'=>$DATAc[$i]->suc,
                                'stock'=>$DATAc[$i]->stock,
                                'stockmin'=>$DATAc[$i]->stockmi,
                                'stockmax'=>$DATAc[$i]->stockma,
                                //'ubicacion'=>$DATAc[$i]->ubi
                    );
                    $this->ModeloCatalogos->Insert('productos_sucursales',$data_array);
                }
            }*/
        }
        /*$DATAs = json_decode($serie_nume);
        for ($y=0;$y<count($DATAs);$y++) {
            $data_array_serie=array(
                'productoid'=>$idpro,
                'sucursalid'=>$DATAs[$y]->sucursalid,
                'serie'=>$DATAs[$y]->serie,
            );
            if($DATAs[$y]->id!=0){
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',$data_array_serie,array('id'=>$DATAc[$i]->id));
            }else{
                $this->ModeloCatalogos->Insert('productos_sucursales',$data_array_serie);
            }

        }*/
    }

    /*public function registrar_datos_pre(){
        $data=$this->input->post();
        $idpro=$data['idpro'];
        $precios=$data['precios'];
        $DATAc = json_decode($precios);

        for ($i=0;$i<count($DATAc);$i++) {
            $idrow = $DATAc[$i]->idrow;
            if($idrow>0){
                $data_array=array(
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des
                );
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrow));
            }else{
                $result_c=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
                if($result_c->num_rows()>0){
                    $result_c->row();
                    $idrowc=$result_c->id;
                    $data_array=array(
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des
                    );
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrowc));
                }else{
                    $data_array=array(
                                'productoid'=>$idpro,
                                'sucursalid'=>$DATAc[$i]->suc,
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des
                    );
                    $this->ModeloCatalogos->Insert('productos_sucursales',$data_array);
                }
            }
        }
    }*/

    public function registrar_datos_pre(){
        $data=$this->input->post();
        $idpro=$data['idpro'];
        $precios=$data['precios'];
        $DATAc = json_decode($precios);

        for ($i=0;$i<count($DATAc);$i++) {
            $idrow = $DATAc[$i]->idrow;
            if($idrow>0){
                $data_array=array(
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des,
                                'precio_final'=>$DATAc[$i]->precioff
                );
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
            }else{
                $data_array=array(
                            'productoid'=>$idpro,
                            'sucursalid'=>$DATAc[$i]->suc,
                            'precio'=>$DATAc[$i]->precio,
                            'incluye_iva'=>$DATAc[$i]->iiva,
                            'iva'=>$DATAc[$i]->iva,
                            'desc_monto'=>$DATAc[$i]->desc,
                            'desc_tipo_descuento'=>$DATAc[$i]->ti_des,
                            'precio_final'=>$DATAc[$i]->precioff
                );
                $this->ModeloCatalogos->Insert('productos_sucursales',$data_array);
            }


            /*}else{
                $result_c=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idpro,'sucursalid'=>$DATAc[$i]->suc));
                if($result_c->num_rows()>0){
                    $result_c->row();
                    $idrowc=$result_c->id;
                    $data_array=array(
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des
                    );
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data_array,array('id'=>$idrowc));
                }else{
                    $data_array=array(
                                'productoid'=>$idpro,
                                'sucursalid'=>$DATAc[$i]->suc,
                                'precio'=>$DATAc[$i]->precio,
                                'incluye_iva'=>$DATAc[$i]->iiva,
                                'iva'=>$DATAc[$i]->iva,
                                'desc_monto'=>$DATAc[$i]->desc,
                                'desc_tipo_descuento'=>$DATAc[$i]->ti_des
                    );
                    $this->ModeloCatalogos->Insert('productos_sucursales',$data_array);
                }
            }*/
        }
    }

    public function registrar_datos_lg(){
        $data=$this->input->post();
        $id=$data['id'];
        $data['personalid']=$this->idpersonal;

        if(isset($data['envio_gratis'])){
            $data['envio_gratis']=1;
        }else{
            $data['envio_gratis']=0;
        }
        
        if($id==0){
            $id=$this->General_model->add_record('productos',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'productos');
        }
        echo $id;
    }

    function baner_imagenes_multiple(){
        $idproducto=$_POST['idproducto'];
        $data = [];
        $count = count($_FILES['files']['name']);
        $output = [];
        for($i=0;$i<$count;$i++){
            if(!empty($_FILES['files']['name'][$i])){
              $_FILES['file']['name'] = $_FILES['files']['name'][$i];
              $_FILES['file']['type'] = $_FILES['files']['type'][$i];
              $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
              $_FILES['file']['error'] = $_FILES['files']['error'][$i];
              $_FILES['file']['size'] = $_FILES['files']['size'][$i];
              $DIR_SUC=FCPATH.'uploads/productos';

              $config['upload_path'] = $DIR_SUC; 
              //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
              $config['allowed_types'] = '*';

              //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
              $config['max_size'] = 5000;
              $file_names=date('Ymd_His').'_'.rand(0, 500);
              //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
              $config['file_name'] = $file_names;
              //$config['file_name'] = $file_names;
       
              $this->load->library('upload',$config); 
        
              if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $filenametype = $uploadData['file_ext'];
       
                $data['totalFiles'][] = $filename;
                $this->ModeloCatalogos->Insert('productos_files',array('file'=>$filename,'idproductos'=>$idproducto,'idpersonal'=>$this->idpersonal));
              }else{
                $data = array('error' => $this->upload->display_errors());
                log_message('error', json_encode($data)); 
              }
            }
   
        }
        $array = array('idproductos'=>$idproducto);
        echo json_encode($array);
    }
    
    function viewimages(){
        $params = $this->input->post();
        $id=$params['id'];
        $resultados=$this->ModeloCatalogos->getselectwheren('productos_files',array('activo'=>1,'idproductos'=>$id));
        $html='<section class="regular slider">';
            $cont=1; 
            foreach ($resultados->result() as $item) {
                $html.='<div><a class="btn" onclick="deleteimg('.$item->id.')"><i class="fa fa-trash " style="font-size: 30px;"></i></a>
                  <img style="width: 270px;" src="'.base_url().'uploads/productos/'.$item->file.'?text='.$cont.'">

                </div>';
                $cont++;
            }
        $html.='</section>';

        /*$html='<section class="regular slider">
            <div>
              <img src="https://via.placeholder.com/350x300?text=1">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=2">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=3">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=4">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=5">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=6">
            </div>
          </section>';*/  
        echo $html;
    }
    
    function deleteimgbaner(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('productos_files',array('activo'=>0),array('id'=>$id));
    }

    public function addcategoria(){
        $params = $this->input->post();
        $categoriaId = $params['categoriaId'];
        unset($params['categoriaId']);
        if ($categoriaId>0) {
            $this->ModeloCatalogos->updateCatalogo('categoria',$params,array('categoriaId'=>$categoriaId));
            $id=$categoriaId;
        }else{
            $id=$this->ModeloCatalogos->Insert('categoria',$params);
        }
        echo $id;
    }

    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModelProductos->get_categoria($params);
        $totaldata= $this->ModelProductos->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_categoria(){
        $params = $this->input->post();
        $categoriaId=$params['categoriaId'];
        $this->ModeloCatalogos->updateCatalogo('categoria',array('activo'=>0),array('categoriaId'=>$categoriaId));
        $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>0,'id_item'=>$categoriaId,'table_name'=>'categoria','descripcion'=>'eliminacion de categoria','personal'=>$this->idpersonal));
    }

    public function select_datos_cat_get(){
        $id=$this->input->post('id');
        $html='<label class="form-label">Categoría<span style="color: red">*</span></label>
                <select class="form-select" name="categoria" id="id_categoria" onchange="getCodigoProd()">
                    <option selected disabled value="0">Selecciona una opción</option>';
                    $result=$this->General_model->getselectwhere_orden_desc('categoria',array('activo'=>1),'categoriaId');
                    foreach ($result as $x){
                        $sel="";
                        if($id==$x->categoriaId)
                            $sel="selected";
                        $html.='<option '.$sel.' value="'.$x->categoriaId.'" data-clave="'.$x->clave.'">'.$x->clave.' - '.$x->categoria.'</option>';
                    }    
        $html.='</select>';
        echo $html;
    }

    public function getLastCatProd(){
        $id=$this->input->post('id_cat');
        $clave=$this->input->post('clave');
        $id_prod=0;
        $get_prodcat=$this->ModelProductos->getCategoriaLastProd($id);
        if($get_prodcat->num_rows()>0){
            $get_prodcat=$get_prodcat->row();
            $id_prod = $get_prodcat->idProducto;
        }
        echo $this->generarNuevoCodigo($id_prod,$clave);
    }

    public function generarNuevoCodigo($codigo,$clave){
        //log_message("error", "clave: ".$clave);
        if (empty($clave)) {
            preg_match('/([A-Za-z]+)/', $codigo, $matches);
            $clave = isset($matches[0]) ? $matches[0] : '';
        }
        preg_match('/(\d+)/', $codigo, $matches);
        if (isset($matches[0])) {
            $numero = $matches[0];
        } else {
            $numero = "00000";
        }
        $nuevoNumero = str_pad((int)$numero + 1, strlen($numero), '0', STR_PAD_LEFT);
        $nuevoCodigo = $clave . $nuevoNumero;
        return $nuevoCodigo;
    }

    public function getlistado(){
        $params = $this->input->post();
        $params["con_insumos"]=$this->con_insumos;
        $params["perfilid"]=$this->perfilid;
        $getdata = $this->ModelProductos->get_listado($params);
        $totaldata= $this->ModelProductos->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function viewimages_list(){
        $params = $this->input->post();
        $id=$params['id'];
        $resultados=$this->ModeloCatalogos->getselectwheren('productos_files',array('activo'=>1,'idproductos'=>$id));
        $html='<section class="regular slider">';
            $cont=1; 
            foreach ($resultados->result() as $item) {
                $html.='<div>
                  <img src="'.base_url().'uploads/productos/'.$item->file.'?text='.$cont.'">
                </div>';
                $cont++;
            }
        $html.='</section>';
        echo $html;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('id',$id,$data,'productos');

        $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>0,'id_item'=>$id,'table_name'=>'productos','descripcion'=>'eliminacion de producto','personal'=>$this->idpersonal));
    }

    public function estatus_record(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $data = array('mas_vendidos'=>$tipo);
        $this->General_model->edit_record('id',$id,$data,'productos');
    }

    public function update_pagina(){
        $id=$this->input->post('id');
        $mostrar=$this->input->post('mostrar'); 
        $data = array('mostrar_pagina'=>$mostrar);
        $this->General_model->edit_record('id',$id,$data,'productos');
    }


    function delete_caracteristica(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('productos_caracteristicas',array('activo'=>0),array('id'=>$id));

        $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>0,'id_item'=>$id,'table_name'=>'productos_caracteristicas','descripcion'=>'eliminacion de productos_caracteristicas','personal'=>$this->idpersonal));
    }

    function get_tabla_caracteristcas()
    {
        $id = $this->input->post('id');
        $results=$this->ModeloCatalogos->getselectwheren('productos_caracteristicas',array('activo'=>1,'idproductos'=>$id));
        echo json_encode($results->result());
    }

    function registro_caracteristicas(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idproductos']=$DATA[$i]->idproducto;    
            $data['titulo']=$DATA[$i]->titulo;    
            $data['descripcion']=$DATA[$i]->descripcion; 
            if($DATA[$i]->id==0){
                $this->General_model->add_record('productos_caracteristicas',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'productos_caracteristicas');
            }
        }
    }

    function searchunidadsat(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclikeunidadsat($search);
        echo json_encode($results->result());
    }
    function searchconceptosat(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclikeconceptosa($search);
        echo json_encode($results->result());
    }

    function searchproveedor(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclikeproveedor($search);
        echo json_encode($results->result());
    }
    
    function viewprecios_list(){
        $id = $this->input->post('id');
        $suc_pro=$this->General_model->get_suc_prod($id);
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">Sucursal</th>
                    <th scope="col">Precio</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($suc_pro->result() as $x){
                $html.='<tr>
                <td>'.$x->name_suc.'</td>
                <td>$ '.number_format($x->precio,2,'.',',') .'</td>
                </tr>';
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

    function viewseries_list(){
        $id = $this->input->post('id');
        $sucursal = $this->input->post('sucursal');
        $suc_pro=$this->General_model->get_suc_prod_serie($id,$sucursal);
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">Sucursal</th>
                    <th scope="col">Número de serie</th>
                    <th scope="col">Estatus</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($suc_pro->result() as $x){
                if($x->id_serie_tras==$x->id && $x->status_tras!=2){
                    $estatus="En tránsito";
                    $class="btn-warning";
                }else{
                    $estatus="Disponible";
                    $class="btn-success";
                }
                if($x->idcompras!=0 && $x->estatus_compra=="1" || $x->idcompras==0){
                    $html.='<tr>
                        <td>'.$x->name_suc.'</td>
                        <td> '.$x->serie.'</td>
                        <td><span class="btn '.$class.'">'.$estatus.'</span></td>
                    </tr>';
                }
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

    function viewlotes_list(){
        $id = $this->input->post('id');
        $sucursal = $this->input->post('sucursal');
        //$suc_pro=$this->General_model->getselectwhereall('productos_sucursales_lote',array('productoid'=>$id,'cantidad >'=>0,"sucursalid"=>$sucursal,"activo"=>1));
        //$suc_pro=$this->General_model->get_suc_prod_lote($id,$sucursal);
        $suc_pro=$this->ModelProductos->get_suc_lotes($id,$sucursal);
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">Lote</th>
                    <th scope="col">Caducidad</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>';
            foreach ($suc_pro->result() as $x){
                $style="";
                if($x->cant_lote_tras>0){
                    $style='style="background-color:rgb(255, 255, 143);"';
                }
                if($x->status>0){
                    $txt_tras='(En tránsito: '.$x->cant_lote_tras.')';
                }else{
                    $txt_tras="";
                    $style="";
                }
                if($x->idcompra!=0 && $x->estatus_compra=="1" || $x->idcompra==0){
                    $html.='<tr>
                        <td> '.$x->lote.'</td>
                        <td> '.$x->caducidad.'</td>
                        <td> '.$x->cantidad.'</td>
                        <td> '.$txt_tras.'</td>
                    </tr>';
                }
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

    public function validar_codigo()
    {   
        $id=$this->input->post('reg');
        $codigox=$this->input->post('codigo');
        $result=$this->General_model->getselectwhere('contrasena','activo',1);
        $codigo=0;
        $id=0;
        foreach ($result as $x) {
            $codigo=$x->codigo;
            $id=$x->id;
        }
        $validarc=0;
        if($codigo==$codigox){
            $data = array('activo'=>0);
            $this->General_model->edit_record('id',$id,$data,'contrasena');
            $validarc=1;
        }else{
            $validarc=0;
        }
        echo $validarc;
        
    }
    
    function validar_codigobarras(){
        $codigo = $this->input->post('codigo');
        $result=$this->General_model->getselectwhere('productos','idProducto',$codigo);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function delete_record_serie(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'productos_sucursales_serie');

        $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>0,'id_item'=>$id,'table_name'=>'productos_sucursales_serie','descripcion'=>'eliminacion de serie','personal'=>$this->idpersonal));
    }

    public function delete_record_lote(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'productos_sucursales_lote');
        $this->ModeloCatalogos->Insert('bitacora_general',array('tipo'=>0,'id_item'=>$id,'table_name'=>'productos_sucursales_lote','descripcion'=>'eliminacion de lote','personal'=>$this->idpersonal));
    }
    
    public function get_validar_serie(){
        $serie=$this->input->post('serie');
        $result=$this->General_model->getselectwhereall('productos_sucursales_serie',array('serie'=>$serie,'activo'=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function getStockSucs(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $html="";
        if($tipo==0 || $tipo==3 || $tipo==4){
            $result=$this->ModelProductos->getStockSucursal($id);
            foreach ($result as $r) {
                $html.= "<div class='row col-md-12'><div class='col-md-9 tal'>".$r->id_alias." - ".$r->name_suc. ":</div><div class='col-md-3 tar'>".($r->stock-$r->traslado_stock_cant)."</div></div>";
            }
        }else if($tipo==1){
            $result=$this->ModelProductos->getSerieSucursal($id);
            foreach ($result as $r) {
                $html.="<div class='row col-md-12'><div class='col-md-8 tal'>".$r->id_alias." - ".$r->name_suc. ":</div><div class='col-md-4 tar'>".($r->serie+$r->serie_compra);
                $html.='<a class="btn" onclick="modal_serie('.$id.','.$r->id_suc.')"><i style="width:5px !important" class="fa fa-eye"></i></a></div></div>';
            }
            
        }else if($tipo==2){
            $result=$this->ModelProductos->getLoteSucursal($id);
            foreach ($result as $r) {
                $html.="<div class='row col-md-12'><div class='col-md-8 tal'>".$r->id_alias." - ".$r->name_suc. ":</div><div class='col-md-4 tar'>".($r->lote+$r->lote_compra)." <button type='button' class='btn' onclick='modal_lote(".$id.",".$r->id_suc.")'><i class='fa fa-eye'></i></button></div>
                </div>";
                //$html.='<a class="btn" onclick="modal_lote('.$id.','.$r->id_suc.')"><i class="fa fa-eye"></i></a>';
            }
        }      
        echo $html;
    }

    public function printCodeBarLote(){
        $id=$this->input->get('id');
        $data['getproducto']=$this->General_model->getselectwhereall('productos_sucursales_lote',array('id'=>$id));
        $this->load->view('reportes/etiqueta_codeLote',$data);           
    }

    public function crearCodigoLote(){ //aca me quedo para crear codigo y pasarlo al js y ese al input
        $idsuc=$this->input->post('idsuc');
        $idprod=$this->input->post('idprod');
        $lote=$this->input->post('lote');
        $cad=$this->input->post('cad');
        
        $codigo=$idprod.$idsuc.intval(preg_replace('/[^0-9]+/', '', $lote), 10).date("Ymd",strtotime($cad));
        echo $codigo;        
    }

    function delete_proveedor(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('productos_proveedores',array('activo'=>0),array('id'=>$id));
    }

    function get_tabla_proveedor(){
        $id = $this->input->post('id');
        $results=$this->ModelProductos->get_proveedores($id);
        echo json_encode($results->result());
    }

    function registro_proveedores(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idproveedor']=$DATA[$i]->idproveedor;    
            $data['idproducto']=$DATA[$i]->idproducto;  
            $data['codigo']=$DATA[$i]->codigo; 
            $data['idpersonal']=$this->idpersonal;
            if($DATA[$i]->id==0){
                $this->General_model->add_record('productos_proveedores',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'productos_proveedores');
            }
        }
    }

     function validar_cod_prove(){
        $codigo = $this->input->post('codigo');
        $result=$this->General_model->getselectwhereall('productos_proveedores',array('codigo'=>$codigo,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function exportExcelProds(){
        $data['sucs']=$this->General_model->getselectwhere_orden_asc('sucursal',array('activo'=>1),"orden");

        /*$where= ""; $cont=0;
        foreach($data["sucs"] as $s){
            //$where.="or ps.productoid=p.id and ps.activo=1 and ps.sucursalid = ".$s->id." ";
            $where.="(SELECT ps".$s->id.".stock from productos_sucursales ps".$s->id." where ps.productoid=p.id and ps.activo=1 and ps.sucursalid=$s->id LIMIT 1) as stock_".$s->id.", 
            ";
        }*/
        //$data["prods"]=$this->ModelProductos->listProdsExcel2();
          $data["prods"]=$this->ModelProductos->listProdsExcel2_p();
        $this->load->view('productos/excelProds',$data);
    }

}
