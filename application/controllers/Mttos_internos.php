<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mttos_internos extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloMttos');
        $this->load->model('Modeloventas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,47);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=9;
        $data['btn_active_sub']=47;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/index_int',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/index_intjs');
    }

    public function registro($id=0){
        $data['btn_active']=9;
        $data['btn_active_sub']=47;
        //$data2[""]=array();
        if($id>0){
            $data["mtto"] = $this->ModeloMttos->get_mttoInt($id);
            $data["ins"] = $this->ModeloMttos->get_mttoIntInsumos($id);
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/form_int',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/form_intjs',$data);
    }

    public function registrarMtto($id){
        $data['btn_active']=9;
        $data['btn_active_sub']=47;
        $data["renta"] = $this->ModeloMttos->set_rentaMtto($id);

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/form_int',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/form_intjs',$data);
    }

    function searchEquipos(){
        $pro = $this->input->get('search');
        //$id_sucursal = $this->input->get('id_sucursal');
        $id_sucursal = 7;
        //$id_sucursal = $this->sucursal;
        $results=$this->ModeloMttos->getEquiposSeries($pro,$id_sucursal);
        echo json_encode($results->result());
    }

    function searchInsumo(){
        $pro = $this->input->get('search');
        //$id_suc=$this->sucursal;
        $id_suc=7;
        $results=$this->ModeloCatalogos->searchInsumo_ventas_like($pro,$id_suc);
        //echo $results;
        echo json_encode($results->result());
    }

    public function save_mtto(){
        $data=$this->input->post();
        $insumos = $data['insumos'];
        unset($data["insumos"]);
        $DATAins = json_decode($insumos);
        $id=$data['id'];
        if($id==0){
            $data["id_sucursal"]=$this->sucursal;
            $data["id_personal"]=$this->idpersonal;
            $data["fecha_reg"]=$this->fecha_hora_actual;
            //$data["estatus"]=1;
            $id=$this->General_model->add_record('mtto_interno',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'mtto_interno');
            $id=$id;
        }
        if($data["estatus"]==1 || $data["estatus"]==3){
            $this->General_model->edit_record('id',$data["idprod_ss"],array("disponible"=>0),'productos_sucursales_serie');
        }if($data["estatus"]==2){
            $this->General_model->edit_record('id',$data["idprod_ss"],array("disponible"=>1),'productos_sucursales_serie');
        }
        ///////////////////insumos del mtto/////////////////////////
        //if($id==0){
            $data_array_ins=array();
            for ($l=0;$l<count($DATAins);$l++) {
                $insert_bita_dll=array(
                    'id_insumo'=>$DATAins[$l]->id_ins,
                    'id_prod_suc'=>$DATAins[$l]->id_ps,
                    'cantidad'=>$DATAins[$l]->cant_ins,
                    'id_mtto'=>$id,
                    'fecha_reg'=>$this->fecha_hora_actual,
                    'id_personal'=>$this->idpersonal,
                    'cant_ini'=>$DATAins[$l]->stock
                );
                $data_array_ins[]=$insert_bita_dll;
                //editar insumo utlizado en servicios
                if($DATAins[$l]->id==0){
                    $this->Modeloventas->restaInsumo_sucursal($DATAins[$l]->id_ps,$DATAins[$l]->cant_ins);
                    $this->General_model->add_record('bitacora_insumos_mttosint',$insert_bita_dll);
                }
            }
            /*if(count($data_array_ins)>0){
                $this->ModeloCatalogos->insert_batch('bitacora_insumos_mttosint',$data_array_ins);
            }*/
        //}
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'mtto_interno');
    }

    public function delete_insumo_mtto(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0,"fecha_del"=>$this->fecha_hora_actual);
        $this->General_model->edit_record('id',$id,$data,'bitacora_insumos_mttosint');
        //editar insumo utlizado en servicios
        $getbi=$this->General_model->get_record("id",$id,"bitacora_insumos_mttosint");
        $this->Modeloventas->sumaInsumo_sucursal($getbi->id_prod_suc,$getbi->cantidad);
    }

    public function get_data_list(){
        $params = $this->input->post();
        $params["perfilid"]=$this->perfilid;
        $params["sucursal"]=$this->sucursal;
        $getdata = $this->ModeloMttos->get_listMttoInt($params);
        $totaldata= $this->ModeloMttos->total_list_mttoInt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function update_estatus(){
        $id=$this->input->post('id');
        $estatus=$this->input->post('estatus');
        if($estatus==2){ //reparacion
            $fecha_serv="fecha_serv";
        }if($estatus==3){ //reparado 
            $fecha_serv="fecha_repara";
        }if($estatus==3){ //reparado 
            $fecha_serv="fecha_sin_repara";
        } 
        $data = array('estatus'=>$estatus,$fecha_serv=>$this->fecha_hora_ac);
        $this->General_model->edit_record('id',$id,$data,'mtto_externo');
    }

    public function pdf_garantia($id){
        $data['id']=$id;
        $data['det']= $this->ModeloGarantia->get_detalle_solicitud($id);
        $data['evide']= $this->ModeloGarantia->getEvideGarantia($id,$data["det"][0]->id_venta);
        $this->load->view('reportes/solicitud_garantia',$data);
    }

    public function ordenTrabajo($id){
        $data['id']=$id;
        $data['det']= $this->ModeloMttos->get_detalle_orden_int($id);
        $this->load->view('reportes/orden_trabajo_int',$data);
    }

    public function getDetOrdTrabajo(){
        $id=$this->input->post('id');
        $fecha="";
        $observ="";
        $get=$this->ModeloMttos->getOrdenTrabajo_int($id);
        $id=0;
        foreach($get as $o){
            $id=$o->id_ot;
            $fecha=$o->fecha;
            $observ=$o->observ_ot;
        }
        
        echo json_encode(array("fecha"=>$fecha,"observ"=>$observ,"id"=>$id));
    }

    public function save_orden(){
        $id=$this->input->post('id');
        $data["id_mtto"]=$this->input->post('id_mtto');
        $data["fecha"]=$this->input->post('fecha');
        $data["observ"]=$this->input->post('observ');

        if($id>0){
            unset($data["id_mtto"]);
            $this->General_model->edit_record('id',$id,$data,'orden_trabajo_int');
        }else{
            $this->ModeloCatalogos->Insert('orden_trabajo_int',$data);
        }
    }

    function saveDetaInsumos(){
        $params=$this->input->post();
        $insumos = $params['array_ins'];
        $DATAins = json_decode($insumos);

        ///////////////////insumos del mtto/////////////////////////
        $data_array_ins=array();
        for ($l=0;$l<count($DATAins);$l++) {
            $insert_bita_dll=array(
                'id_venta'=>$DATAins[$l]->id_venta,
                'id_insumo'=>$DATAins[$l]->id_ins,
                'id_prod_suc'=>$DATAins[$l]->id_ps,
                'cantidad'=>$DATAins[$l]->cant_ins,
                'id_mtto'=>$DATAins[$l]->id_mtto
            );
            $data_array_ins[]=$insert_bita_dll;
            //editar insumo utlizado en servicios
            $this->Modeloventas->restaInsumo_sucursal($DATAins[$l]->id_ps,$DATAins[$l]->cant_ins);
        }

        if(count($data_array_ins)>0){
            $this->ModeloCatalogos->insert_batch('bitacora_ins_servs_ventas',$data_array_ins);
        }
    }

    public function getInsumosMtto(){
        $id=$this->input->post("id");
        $id_venta=$this->input->post("id_venta");
        $html=""; $numero=""; $descripcion="";
        $get=$this->ModeloMttos->getInsumos($id,$id_venta);
        foreach($get as $i){
            $numero=$i->numero;
            $descripcion=$i->descripcion;
            $html.="<tr>
                <td>".$i->cantidad."</td>
                <td>".$i->idProducto." / ".$i->nombre."</td>
            </tr>";
        }
        echo json_encode(array("html"=>$html,"numero"=>$numero,"descripcion"=>$descripcion));
    }

    /* ***********************************/
    public function kardex(){
        $data['btn_active']=9;
        $data['btn_active_sub']=46;
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/kardex');
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/kardexjs');
    }

    public function getDataKardex_equipos() {
        $params = $this->input->post();
        $result = $this->ModeloMttos->get_equipo_kardex($params);
        $json_data = array("data" => $result);
        echo json_encode($json_data);
    }

    public function exportKardexEquipo($id_equipo,$id_serie,$fechai,$fechaf) {
        $params["id_equipo"] = $id_equipo;
        $params["id_serie"] = $id_serie;
        $params["fechai"] = $fechai;
        $params["fechaf"] = $fechaf;
        $data["id_equipo"]  = $id_equipo;
        $data["mttos"] = $this->ModeloMttos->get_equipo_kardex($params);
        $this->load->view('mantenimiento/kardex_mtto',$data);
    }

}