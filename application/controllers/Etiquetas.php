<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Etiquetas extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModelProductos');
    }
	public function index(){
		$id=$this->input->get('id');
		$data['getproducto']=$this->ModelProductos->get_producto($id);
		$this->load->view('reportes/etiqueta',$data);
	        
	}
}