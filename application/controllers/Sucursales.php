<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sucursales extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,26);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=26;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('sucursal/index');
        $this->load->view('templates/footer');
        $this->load->view('sucursal/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=26;
        if($id==0){
            $data['id'] = 0;
            $data['id_alias']='';
            $data['clave']='';
            $data['name_suc'] = '';
            $data['tel']='';
            $data['domicilio']='';
            $data['direccion']='';
            $data['orden']='';
            $data['con_insumos']='';
        }else{
            $resul=$this->General_model->getselectwhere('sucursal','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['id_alias']=$item->id_alias;
                $data['clave']=$item->clave;
                $data['name_suc']=$item->name_suc;
                $data['tel']=$item->tel;
                $data['domicilio']=$item->domicilio;
                $data['direccion']=$item->direccion;  
                $data['orden']=$item->orden; 
                $data["con_insumos"]=$item->con_insumos;
            }
        }      

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('sucursal/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('sucursal/formjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_listado($params);
        $totaldata= $this->ModelCatalogos->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id>0){
            $this->General_model->edit_record('id',$id,$data,'sucursal');
        }else{
            $id=$this->General_model->add_record('sucursal',$data);
        }
        echo $id;
    }

    public function get_validar_num_sucursal()
    {   
        $num=$this->input->post('num');
        $resul=$this->General_model->getselectwhereall('sucursal',array('id_alias'=>$num,'activo'=>1));
        $validar=0;
        foreach ($resul as $item){
            $validar=1;
        }
        echo $validar;
    }

}