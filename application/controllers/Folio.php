<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folio extends CI_Controller {
	  function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        //$this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->model('ModeloComplementos');
        $this->load->model('ModeloMail');
        $this->idpersonal=$this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            //$this->idpersonal=$this->session->userdata('idpersonal');
            //$this->perfilid=$this->session->userdata('perfilid');
            //$permiso=$this->Login_model->getviewpermiso($this->perfilid,7);// perfil y id del submenu
            //if ($permiso==0) {
            //    redirect('Login');
            //}
        }else{
            redirect('Inicio');
        }
    }

	  public function index(){
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/listado');
        $this->load->view('templates/footer');
        $this->load->view('folio/listadojs');
    }
    public function registro($id=0){
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/vista');
        $this->load->view('templates/footer');
        $this->load->view('folio/vistajs');
    }
    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getfacturas($params);
        $totaldata= $this->Modelofacturas->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function facturapdf($FacturasId){
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      if($datosconfiguracion->logotipo!=''){
        $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
      }else{
        $logotipo = base_url().'public/img/SEMIT.jpg';
      }
      $data["logotipo"]=$logotipo;
        
      $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
      $data['rrfc']=$datosconfiguracion->Rfc;
      $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Noexterior.'  interior '.$datosconfiguracion->Nointerior.' '.$datosconfiguracion->Colonia.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
      $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;
      $data['LugarExpedicion_suc']=$datosconfiguracion->CodigoPostal;
      //================================================
        $datossucursal=$this->ModeloCatalogos->obtenerdatossucursalfacturaventa($FacturasId);
        foreach ($datossucursal->result() as $items) {
          $data['rdireccion']=$items->domicilio;
          $data['LugarExpedicion_suc']=$items->cp_cp;
        }
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();
        $data['sucu']= $this->ModeloCatalogos->obtenersucursalesfactura();
      //================================================

      $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);
      $factura = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
      foreach ($factura->result() as $item) {
        $MetodoPago=$item->MetodoPago;
        $FormaPago=$item->FormaPago;
        $observaciones=$item->observaciones;
        //$Folio=str_pad($item->Folio, 4, "0", STR_PAD_LEFT);
        $Folio=$item->Folio;
        $isr=$item->isr;
        $uuid=$item->uuid;
        $nocertificadosat=$item->nocertificadosat;
        $certificado=$item->nocertificado;
        $fechatimbre=$item->fechatimbre;
        $uso_cfdi=$item->uso_cfdi;
        $cliente=$item->Nombre;
        $clirfc=$item->Rfc;
        $clidireccion=$item->Direccion;
        $numproveedor=$item->numproveedor;
        $numordencompra=$item->numordencompra;
        $Caducidad=$item->Caducidad;
        $Lote=$item->Lote;
        $moneda=$item->moneda;
        $subtotal  = $item->subtotal;
        $iva = $item->iva;
        $total = $item->total;
        $ivaretenido = $item->ivaretenido;
        $cedular = $item->cedular;
        $selloemisor = $item->sellocadena;
        $sellosat = $item->sellosat;
        $cadenaoriginal = $item->cadenaoriginal;
        $tarjeta=$item->tarjeta;
        $tipoComprobante=$item->TipoComprobante;
        $cp=$item->Cp;
        
        $isr = $item->isr; 
        $ivaretenido = $item->ivaretenido; 
        $cincoalmillarval = $item->cincoalmillarval; 
        $outsourcing = $item->outsourcing; 
        $clienteId=$item->clienteId;
        $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
        $f_relacion=$item->f_relacion;
        $f_r_tipo=$item->f_r_tipo;
        $f_r_uuid=$item->f_r_uuid;
        $cartaporte=$item->cartaporte;
        $usuario_id = $item->usuario_id;
        $pg_global=$item->pg_global;
        if($pg_global==1){
          if($item->LugarExpedicion!=null and $item->LugarExpedicion!='null' and $item->LugarExpedicion!=''){
            $cp=$item->LugarExpedicion;
          }
        }
      }
      //===================================
        $row_personal = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$usuario_id));
        $data['nom_personal']='';
        foreach ($row_personal->result() as $item_p) {
          $data['nom_personal']=$item_p->nombre;
        }
        $strq_fv="SELECT per.nombre,ven.folio 
                    FROM f_facturas as fac
                    INNER JOIN venta_fk_factura as venf on venf.idfactura=fac.FacturasId
                    INNER JOIN venta_erp as ven on ven.id=venf.idventa
                    INNER JOIN personal as per on per.personalId=ven.personalid
                    WHERE fac.FacturasId='$FacturasId'";
        $query_fv = $this->db->query($strq_fv);
        $data['ticket']='';
        foreach ($query_fv->result() as $item_vf) {
          if($pg_global!=1){
            $data['nom_personal']=$item_vf->nombre;
            $data['ticket']=$item_vf->folio;
          }
        }
      //==================================
        $datosdigitos=$this->ModeloCatalogos->obtenerultimosdigitosventa($FacturasId);
        foreach ($datosdigitos->result() as $item) {
          $tarjeta.=' '.$item->ultimosdigitos.' ';
        }
      //==================================
      $facturadetalles = $this->Modelofacturas->facturadetalle($FacturasId);
      $data['cp']=$cp;
      //=============
      $data['FormaPago']=$MetodoPago;
      //log_message('error', 'FormaPago c: '.$MetodoPago);
      $data['FormaPagol']=$this->gf_FormaPago($MetodoPago);
      //$data['FormaPagol']='vv';
      //=================
      $data['MetodoPago']=$FormaPago;
      $data['MetodoPagol']=$this->gt_MetodoPago($FormaPago);
      //$data['MetodoPagol']='cc';
      //=================
      $data['observaciones']=$observaciones;
      $data['Folio']=$Folio;
      //=================
      $data["isr"]=$isr;
      //=================
      $data['folio_fiscal']=$uuid;
      $data['nocertificadosat']=$nocertificadosat;
      $data['certificado']=$certificado;
      $data['fechatimbre']=$fechatimbre;
      $data['cfdi']=$this->gt_uso_cfdi($uso_cfdi);
      $data['cliente']=$cliente;
      $data['clirfc']=$clirfc;
      $data['clidireccion']=$clidireccion;
      $data['RegimenFiscalReceptor']   =   $this->gt_regimenfiscal($RegimenFiscalReceptor);
      $data['numproveedor']=$numproveedor;
      $data['numordencompra']=$numordencompra;
      $data['moneda']=$moneda;
      $data['subtotal']=$subtotal;
      $data['iva']=$iva;

      $data['isr']=$isr;
      $data['ivaretenido']=$ivaretenido;
      $data['cincoalmillarval']=$cincoalmillarval;
      $data['outsourcing']=$outsourcing;

      $data['total']=$total;
      $data['ivaretenido']=$ivaretenido;
      $data['cedular']=$cedular;
      $data['selloemisor']=$selloemisor;
      $data['sellosat']=$sellosat;
      $data['cadenaoriginal']=$cadenaoriginal;
      $data['tarjeta']=$tarjeta;
      $data['facturadetalles']=$facturadetalles->result();
      $data['tipoComprobante']=$tipoComprobante;
      $data['idfactura']=$FacturasId;
      //$data['detalleperidofactura']= $this->Rentas_model->detalleperidofactura($FacturasId);
      //$data['detalleconsumiblesfolios']= $this->Rentas_model->detalleconsumiblesfolios($FacturasId);
      //$data['detallecimages']= $this->Rentas_model->detalleimagesRenta($FacturasId);
      $data['Caducidad']      =   $Caducidad;
      $data['Lote']           =   $Lote;
      $data['f_relacion'] = $f_relacion;
      $data['f_r_tipo'] = $f_r_tipo;
      $data['f_r_uuid'] = $f_r_uuid;
      $data['clienteId'] = $clienteId;
      
      $resut_cli= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$clienteId));
      $data['correo']='';
      foreach ($resut_cli->result() as $itemc) {
        $data['correo']=$itemc->correo;
      }

      if($cartaporte==1){
        $info_ftra= $this->ModeloCatalogos->getselectwheren('traspasos',array('Idfactura'=>$FacturasId));
        $data['no_traspaso']='';
        $data['fecha_traspaso']='';
        $data['suc_s']='';
        $data['suc_e']='';
        $id_traspaso=0;
        foreach ($info_ftra->result() as $item_tr) {
          $id_traspaso=$item_tr->id;
          $data['no_traspaso']=$id_traspaso;

          $timestamp = strtotime($item_tr->reg);
          $anio = date('Y', $timestamp);
          $mes = date('m', $timestamp);
          $mes_l= $this->ModeloCatalogos->mes_letra($mes);
          $dia = date('d', $timestamp);

          $data['fecha_traspaso']=$dia.'-'.$mes_l.'-'.$anio;
        }
        $strq_s="SELECT 
                    sucs.id as id_s,sucs.clave as clave_s,sucs.id_alias as id_alias_s,sucs.name_suc as name_suc_s,sucs.cp_cp,
                      suce.id as id_e,suce.clave as clave_e,suce.id_alias as id_alias_e,suce.name_suc as name_suc_e
                  FROM historial_transpasos as ht 
                  INNER JOIN sucursal as sucs on sucs.id=ht.idsucursal_sale
                  INNER JOIN sucursal as suce on suce.id=ht.idsucursal_entra
                  WHERE ht.idtranspasos='$id_traspaso'
                  GROUP BY sucs.id,suce.id";
        $query_s = $this->db->query($strq_s);
        foreach ($query_s->result() as $item_suc) {
          $data['LugarExpedicion_suc']=$item_suc->cp_cp;
          $data['suc_s']=$item_suc->clave_s;
          $data['suc_e']=$item_suc->clave_e;
        }


        $this->load->view('reportes/factura_cp',$data);
      }else{
        $this->load->view('reportes/factura',$data);
      } 
    }

    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Otros';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }
    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }elseif ($text=='S01') {
            $textl=' S01 Sin efectos fiscales';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gt_regimenfiscal($text){
          if($text=="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text=="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text=="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text=="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text=="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text=="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text=="609"){ 
            $textl='609 Consolidación';

          }elseif($text=="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text=="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text=="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text=="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text=="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text=="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text=="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text=="621"){ 
            $textl='621 Incorporación Fiscal';

          }elseif($text=="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text=="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text=="624"){ 
            $textl='624 Coordinados';

          }elseif($text=="625"){ 
            $textl='625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas';

          }elseif($text=="626"){ 
            $textl='626 Régimen Simplificado de Confianza';

          }elseif($text=="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text=="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text=="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function favorito(){
        $params = $this->input->post();
        $factura = $params['factura'];
        $status = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('favorito'=>$status),array('FacturasId'=>$factura));
    }
    function deletefactura(){
        $params = $this->input->post();
        $factura = $params['factura'];
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('activo'=>0),array('FacturasId'=>$factura));
    }
    function listasdecomplementos(){
        $factura=$this->input->post('facturas');
        //$resultado=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('FacturasId'=>$factura,'Estado'=>1));
        $resultado=$this->ModeloCatalogos->complementofacturas($factura);
        $html='<table class="table" id="tableliscomplementos">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Numero de parcialidad</th>
                        <th>Monto</th>
                        <th>Archivo</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($resultado->result() as $item) {
            $html.='<tr>
                        <td>'.$item->complementoId.'</td>
                        <td>'.$item->FechaPago.'</td>
                        <td>'.$item->NumParcialidad.'</td>
                        <td>'.$item->ImpPagado.'</td>
                        <td>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609"><i class="far fa-file-code fa-2x"></i></a>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().'Facturaslis/complementodoc/'.$item->complementoId.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML"  data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609">
                                <i class="fas fa-file-pdf fa-2x"></i>
                                </a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function correoscliente(){
      $params=$this->input->post();
      $clienteId=$params['cliente'];
      $factura = $this->ModeloCatalogos->getselectwheren('cliente_mail',array('clienteId'=>$clienteId,'activo'=>1));
      $option='';
      foreach ($factura->result() as $item) {
        $option.='<tr class="climailid_'.$item->climailId.'"><td><input value="'.$item->mail.'" id="mailcli" class="form-control" readonly style="border:0px;"></td>';
        $option.='<td><a type="button"
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" onclick="deletemailcli('.$item->climailId.')"
                          data-position="top" data-delay="50" data-tooltip="Elimiar" id="refacturar">
                          <span class="fa-stack">
                          <i class="icon-xl far fa-trash-alt"></i>
                          </span>
                        </a></td></tr>';
      }
      echo $option; 

    }
    function estatusdecomplementos(){
      $factura=$this->input->post('factura');
      $total=$this->ModeloCatalogos->sumapagadocomplemento($factura);
      echo $total;
    }
    function complementos($facturaid){
        $data['facturaid']=$facturaid;
        $data['btn_active']=6;
        $data['btn_active_sub']=19;
        $factura = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaid));
        $data['fac'] = $factura->row();
        $data['faccomp'] = $this->ModeloComplementos->complementofactura($facturaid);
        $data['perfilid']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/complementoaddv');
        $this->load->view('templates/footer');
        $this->load->view('folio/complementoaddvjs');
    }
    function complementoadd($idfactura){
        $data['facturaid']=$idfactura;
        $data['btn_active']=6;
        $data['btn_active_sub']=19;
        $data['cfdi']=$this->ModeloCatalogos->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloCatalogos->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloCatalogos->genSelect('f_formapago'); 
        $idconfi=1;
        //======================================================================
            $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
            $data['fac']=$datosfactura->row();
            $datosfactura=$datosfactura->result();
            $datosfactura=$datosfactura[0];
            if($datosfactura->serie=='D'){
                $idconfi=2;
            }
            $data['serie']=$datosfactura->serie;
        //======================================================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfi));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['rFCEmisor']=$datosconfiguracion->Rfc;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;

        

        $direccion=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('rfc'=>$datosfactura->Rfc,'idcliente'=>$datosfactura->clienteId,'activo'=>1));
        foreach ($direccion->result() as $item) {
            $razon_social = $item->razon_social;
            $cp = $item->cp;
            
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
        }
        $data['razonsocialreceptor']=$razon_social;
        $data['rfcreceptor']=$datosfactura->Rfc;
        $data['Fecha']=date('Y-m-d').'T'.date('H:i:s');
        $data['uuid']=$datosfactura->uuid;
        $data['Folio']=$datosfactura->Folio;
        $data['FacturasId']=$datosfactura->FacturasId;
        $data['Fechafa']=date('Y-m-d',strtotime($datosfactura->fechatimbre)).'T'.date('H:i:s',strtotime($datosfactura->fechatimbre));
        $data['formapago']=$datosfactura->MetodoPago;

        $saldo=$this->ModeloComplementos->saldocomplemento($idfactura);
        $datoscopnum=$this->ModeloComplementos->saldocomplementonum($idfactura);

        $data['copnum']=$datoscopnum+1;
        //log_message('error', 'validar saldo: '.$datosfactura->total.'-'.$saldo);
        $data['saldoanterior']=$datosfactura->total-$saldo;
        $data['cliente']=$datosfactura->clienteId;
        $data['MetodoDePagoDR']=$datosfactura->FormaPago;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/complementoadd');
        $this->load->view('templates/footer');
        $this->load->view('folio/complementoaddjs');
    }
    function obtenerfacturascomplemento(){
        $datos = $this->input->post();
        $cli=$datos['cli'];
        $fact=$datos['fact'];
        $serie=$datos['serie'];
        $rowsfacturas=$this->ModeloCatalogos->getselectwheren('f_facturas',array('Estado'=>1,'clienteId'=>$cli,'Folio !='=>$fact,'FormaPago'=>'PPD','serie'=>$serie));
        $html='<table id="facturascli" class="responsive-table display no-footer dataTable">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>IdDocumento(uuid)</th>
                        <th>Monto</th>
                        <th>Fecha Timbre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
        ';
        foreach ($rowsfacturas->result() as $item) {
            $html.='<tr>
                        <td>'.$item->Folio.'</td>
                        <td class="facturasuuid">'.$item->uuid.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->fechatimbre.'</td>
                        <td>
                            <a class="btn-bmz waves-effect waves-light cyan" 
                                onclick="adddoc('.$item->FacturasId.')"
                                >Agregar</a></td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function documentoadd(){
        $datos = $this->input->post();
        $idfactura=$datos['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];

        //$datoscop=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('FacturasId'=>$idfactura,'Estado'=>1));
        $datoscop=$this->ModeloCatalogos->consultarcomplementosfactura($idfactura);
        $datoscopnum=$datoscop->num_rows();
        $saldo=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
        }
        $NumParcialidad=$datoscopnum+1;
        $array = array(
            'idfactura'=>$idfactura,
            'MetodoDePagoDR'=>$datosfactura->FormaPago,
            'IdDocumento'=>$datosfactura->uuid,
            'NumParcialidad'=>$NumParcialidad,
            'ImpSaldoAnt'=>$datosfactura->total-$saldo
        );
        echo json_encode($array);
    }
    function complementodoc($id){
        $resultscp=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$id));
        $resultscp=$resultscp->row();
        //===================================
            $idconfig=1;
            if($resultscp->Serie=='D'){
                $idconfig=2;
            }
        //===================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfig));
        $datosconfiguracion=$datosconfiguracion->row();
        $data['confif']=$datosconfiguracion;
        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['regimenf']='601 General de Ley Personas Morales';
        //==========================================
            //$data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($idconfig); 

        //==========================================
            $data['idcomplemento']=$id;
            
            $data['resultscpd']=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$id));
            
            $FacturasId=$resultscp->FacturasId;
            $data['uuid']=$resultscp->uuid;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['LugarExpedicion']=$resultscp->LugarExpedicion;
            $data['fechatimbre']=$resultscp->fechatimbre; 
            $data['fechapago']=$resultscp->FechaPago;
            $data['monto']=$resultscp->Monto;
            $data['IdDocumento']=$resultscp->IdDocumento;
            //$data['Folio']=$resultscp->Folio;
            $data['ImpSaldoAnt']=$resultscp->ImpSaldoAnt;
            $data['NumParcialidad']=$resultscp->NumParcialidad;
            $data['ImpSaldoInsoluto']=$resultscp->ImpSaldoInsoluto;
            $data['Sello']=$resultscp->Sello;
            $data['sellosat']=$resultscp->sellosat;
            $data['cadenaoriginal']=$resultscp->cadenaoriginal;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['rfcreceptor']=$resultscp->R_rfc;
            $data['nombrereceptorr']=$resultscp->R_nombre;
            $data['NumOperacion']=$resultscp->NumOperacion;
            $docrelac = $this->Modelofacturas->documentorelacionado($id);
            $data['docrelac']=$docrelac->result();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$resultscp->FormaDePagoP));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
            //$resultsfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
            //$resultsfc=$resultsfc->row();
            /*
            if ($resultsfc->moneda=='pesos') {
                $data['moneda']='Peso Mexicano';
            }else{
                $data['moneda']='Dolar';
            }
            */
        //====================================================================
            //$resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$resultsfc->FormaPago));
            //$resultsmp=$resultsmp->row();
            //$data['metodopago']=$resultsmp->metodopago_text;
        //==========================================
        $this->load->view('reportes/complemento',$data);
    }

}    