<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BannersSliders extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,12);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=12;

        $data['id'] = 0;
        $data['resumen']='';
        $data['descripcion'] = '';
        $data['referencia']='';
        $data['categoria']=0;

        $data['mostrar_pagina']='';
        $data['stock']='';
        $data['stockmin']='';
        $data['stockmax']='';
        $data['ubicacion']='';
        $data['ancho']='';
        $data['alto']='';
        $data['largo']='';
        $data['peso']='';
        $data['tiempo_carga']='';
        $data['costo_envio']='';
        $data['envio_gratis']='';
        $data['precio_sin_iva']='';
        $data['precio_con_iva']='';
        $data['descuento']='';
        $data['tipo_descuento']='';
        $data['nombre']='';

            
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('bannerssliders/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('bannerssliders/formjs');
    }

    public function registrar_datos(){
        $data = array('idbannerssliders'=>1,'activo'=>1);
        $id=$this->General_model->add_record('bannerssliders_pestana',$data);
        echo $id;
    }

    function get_tabla_detalles()
    {
        $results=$this->ModeloCatalogos->getselectwheren('bannerssliders_pestana',array('activo'=>1,'idbannerssliders'=>1));
        echo json_encode($results->result());
    }

    public function get_datos_pestana()
    {
        $id=$this->input->post('id');
        $results=$this->ModeloCatalogos->getselectwheren('bannerssliders_pestana',array('id'=>$id));
        echo json_encode($results->result());
    }

    public function registrar_datos_pestana(){
        $data=$this->input->post();
        $id=$data['id'];
        if(isset($data['mostrar_boton'])){
            $data['mostrar_boton']=1;
        }else{
            $data['mostrar_boton']=0;
        }
        $this->General_model->edit_record('id',$id,$data,'bannerssliders_pestana');
        echo $id;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'bannerssliders_pestana');
    }

    function baner_imagenes(){
        $idreg=$_POST['idreg'];
        $data = [];
        $count = count($_FILES['files']['name']);
        $output = [];
        /*for($i=0;$i<$count;$i++){*/
            if(!empty($_FILES['files']['name'])){
              $_FILES['file']['name'] = $_FILES['files']['name'];
              $_FILES['file']['type'] = $_FILES['files']['type'];
              $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
              $_FILES['file']['error'] = $_FILES['files']['error'];
              $_FILES['file']['size'] = $_FILES['files']['size'];
              $DIR_SUC=FCPATH.'uploads/banners';

              $config['upload_path'] = $DIR_SUC; 
              //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
              $config['allowed_types'] = '*';

              //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
              $config['max_size'] = 5000;
              $file_names=date('Ymd_His').'_'.rand(0, 500);
              //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
              $config['file_name'] = $file_names;
              //$config['file_name'] = $file_names;
       
              $this->load->library('upload',$config); 
        
              if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $filenametype = $uploadData['file_ext'];
       
                $data['totalFiles'][] = $filename;
                $this->General_model->edit_record('id',$idreg,array('file'=>$filename),'bannerssliders_pestana');
              }else{
                $data = array('error' => $this->upload->display_errors());
                log_message('error', json_encode($data)); 
              }
            }
   
        //}
        echo json_encode($output);
    }

    function viewimages(){
        $params = $this->input->post();
        $id=$params['id'];
        $resultados=$this->ModeloCatalogos->getselectwheren('bannerssliders_pestana',array('activo'=>1,'file!='=>'','id'=>$id));
        $html='<section class="regular_'.$id.' slider">';
            $cont=1; 
            foreach ($resultados->result() as $item) {
                $html.='<div><a class="btn" onclick="deleteimg('.$item->id.')"><i class="fa fa-trash" style="font-size: 30px;"></i></a>
                  <img style="width:100%" src="'.base_url().'uploads/banners/'.$item->file.'?text='.$cont.'">
                </div>';
                $cont++;
            }
        $html.='</section>';

        /*$html='<section class="regular slider">
            <div>
              <img src="https://via.placeholder.com/350x300?text=1">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=2">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=3">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=4">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=5">
            </div>
            <div>
              <img src="https://via.placeholder.com/350x300?text=6">
            </div>
          </section>';*/  
        echo $html;
    }

    function deleteimgbaner(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('bannerssliders_pestana',array('file'=>''),array('id'=>$id));
    }

}