<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGarantia');
        $this->load->model('ModelTraspasos');
        $this->load->model('ModeloRentas');
        date_default_timezone_set("America/Mexico_City");
        $this->mes_act=date('n');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->sucursal=$this->session->userdata('sucursal');
    }
    public function index(){
        $data['btn_active']=1;
        $data['btn_active_sub']=1;
        $data['mes']=$this->mes_act;
        $usuarioid = $this->session->userdata('usuarioid');
        $get_usu=$this->General_model->getselectwhere('usuarios','UsuarioID',$usuarioid);
        $new_perf = $get_usu[0]->perfilId; 
        $this->session->set_userdata('perfilid', $new_perf);

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('inicio');
        $this->load->view('templates/footer');
        $this->load->view('iniciojs');
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductoslike($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function get_tabla_verificador(){    
        $codigo=$this->input->post('codigo');
        $tipo=$this->input->post('tipo');
        if($tipo==1){
            $get_productos=$this->General_model->getselectwhere('productos','codigoBarras',$codigo);
        }else{
            $get_productos=$this->General_model->getselectwhere('productos','id',$codigo);
        }
        $html='<table class="table table-sm" id="table_datos">
                    <thead>
                        <tr style="background: #009bdb;">
                            <th scope="col" style="color: white !important;  font-size: 15px;"></th>
                            <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>
                            <th scope="col" style="color: white; font-size: 15px;">Stock sucursal</th>
                            <th scope="col" style="color: white; font-size: 15px;">Precio</th>
                        </tr>
                    </thead>
                    <tbody>';
                        foreach($get_productos as $x){
                            $result=$this->General_model->get_productos_files($x->id);
                            $img=''; 
                            foreach ($result as $z){
                                $img='<img style="width: 100px" src="'.base_url().'uploads/productos/'.$z->file.'"';    
                            }
                            $html.='<tr>
                                <td>'.$img.'</td>
                                <td>'.$x->nombre.'</td>
                                <td>'.$x->stock.'</td>
                                <td>'.$x->precio_con_iva.'</td>
                            </tr>';
                        }
                    $html.='</tbody>
                </table>';
        echo $html;
    }
    
    function get_alertas()
    {
        $result=$this->ModeloCatalogos->get_alertas_traspasos();
        $html='';
        foreach ($result as $x){
            if($x->tipo==1){
                $html.='<li class="noti-primary">';
                    $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/notifi.png"></span>';
                      $html.='<div class="media-body">';
                        $html.='<p>Solicitud de traspasos</p>';
                      $html.='</div>';
                    $html.='</div>';
                    $html.='<div>';
                      $html.='<h5><b style="color: #019cde;">Usuario:</b><b> '.$x->nombre.'</b></h5>';
                      $html.='<h5><b style="color: #019cde;">Sucursal destino:</b> <b>'.$x->id_alias.' - '.$x->name_sucx.'</b></h5>';
                      $html.='<h5><b style="color: #019cde;">Sucursal origen:</b> <b>'.$x->id_alias2.' - '.$x->name_sucy.'</b></h5>';
                      $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Contrasena?valor='.$x->id.'">Generar contraseña</a>';
                    $html.='</div>';
                  $html.='</li>';
            }else{
                $html.='<li class="noti-primary">';
                    $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/notifi2.png"></span>';
                      $html.='<div class="media-body">';
                        $html.='<p>Solicitud de descuento</p>';
                      $html.='</div>';
                    $html.='</div>';
                    $html.='<div>';
                      $html.='<h5><b style="color: #019cde;">Usuario:</b><b> '.$x->nombre.'</b></h5>';
                      $html.='<h5><b style="color: #019cde;">Sucursal:</b><b> '.$x->id_alias2.' - '.$x->name_sucy.'</b></h5>';
                      $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Contrasena?valor='.$x->id.'">Generar contraseña</a>';
                    $html.='</div>';
                  $html.='</li>';
            }
        }
        echo $html;
    }

    function get_alertas_total()
    {
        $result=$this->ModeloCatalogos->get_alertas_total();
        $total=0;
        foreach ($result as $x){
            $total=$x->total;
        }
        echo $total;
    }

    function add_cantidad_sucursal()
    {
        $get_productos=$this->General_model->get_table('productos');
        foreach ($get_productos as $x){
            $idproductos=$x->id;
            $get_sucursal=$this->General_model->get_table('sucursal');
            foreach ($get_sucursal as $s){
                $productos_sucursales=$this->General_model->getselectwhereall('productos_sucursales',array('productoid'=>$idproductos,'sucursalid'=>$s->id));
                $validar=0;
                foreach ($productos_sucursales as $ps){
                    $validar=1;
                }
                if($validar==0){
                    $data['productoid']=$idproductos;
                    $data['sucursalid']=$s->id;                    
                    $data['stock']=0;
                    $data['stockmin']=0;
                    $data['stockmax']=0;
                    $data['precio']=0;
                    $data['incluye_iva']=0;
                    $data['ubicacion']='';
                    $data['desc_monto']=0;
                    $data['desc_tipo_descuento']=0;
                    $data['iva']=0;
                    $this->General_model->add_record('productos_sucursales',$data);
                }
            }
        }
    }

    public function get_alertasGarantias(){
        $get=$this->ModeloGarantia->getNotificaGarantia();
        $html='';
        $cont=0;
        foreach ($get as $g){
            $cont++;
            $html.='<li class="noti-primary">';
                $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/resumen.svg"></span>';
                    $html.='<div class="media-body">';
                        $html.='<p>Solicitud de garantía ('.$g->id.')</p>';
                    $html.='</div>';
                $html.='</div>';
                $html.='<div>';
                    $html.='<h5><b style="color: #019cde;">Personal:</b> <b> '.$g->personal.'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Sucursal:</b> <b>'.$g->id_alias.' - '.$g->suc_solicita.'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Fecha:</b> <b>'.date("H:i a d-m-Y", strtotime($g->reg)).'</b></h5>';
                    $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Garantias">Ver Solicitud</a>';
                $html.='</div>';
            $html.='</li>';
        }
        echo json_encode(array("html"=>$html,"cont"=>$cont));
    }

    public function get_alertasTras(){
        $get=$this->ModelTraspasos->getNotificaTraspaso($this->sucursal);
        $html='';
        $cont=0;
        foreach ($get as $g){
            $cont++;
            $html.='<li class="noti-primary">';
                $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/notifi.png"></span>';
                    $html.='<div class="media-body">';
                        $html.='<p>Solicitud de traspaso ('.$g->id.')</p>';
                    $html.='</div>';
                $html.='</div>';
                $html.='<div>';
                    $html.='<h5><b style="color: #019cde;">Personal:</b> <b>'.$g->personal.'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Sucursal Solicita:</b> <b>'.$g->id_alias.' - '.$g->suc_solicita.'</b></h5>';
                    $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Traspasos">Ver Solicitud</a>';
                $html.='</div>';
            $html.='</li>';
        }
        echo json_encode(array("html"=>$html,"cont"=>$cont));
    }

    public function get_alertaRenta(){
        $get=$this->ModeloRentas->getRentasPents();
        $html='';
        $cont=0;
        foreach ($get as $g){
            $cont++;
            $html.='<li class="noti-primary">';
                $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 30px;" src="'.base_url().'public/img/sillaRuedas.png"></span>';
                    $html.='<div class="media-body">';
                        $html.='<p>Solicitud de renta ('.date("H:i a d-m-Y", strtotime($g->fecha_reg)).')</p>';
                    $html.='</div>';
                $html.='</div>';
                $html.='<div>';
                    $html.='<h5><b style="color: #019cde;">Servicio:</b> <b>'.$g->descripcion.'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Serie:</b> <b>'.$g->serie.'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Total:</b> <b>$'.number_format($g->costo+$g->deposito+$g->costo_entrega+$g->costo_recolecta,2,'.',',').'</b></h5>';
                    $html.='<h5><b style="color: #019cde;">Sucursal Solicita:</b> <b>'.$g->id_alias.' - '.$g->name_suc.'</b></h5>';
                    $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Rentas/alta/'.$g->id.'">Ver Solicitud</a>';
                $html.='</div>';
            $html.='</li>';
        }
        echo json_encode(array("html"=>$html,"cont"=>$cont));
    }
    public function get_alerta_sol_des(){
        $get=$this->ModeloCatalogos->lis_sol_descu(0);
        $html='';
        $cont=0;
        foreach ($get->result() as $g){
            $cont++;
            $html.='<li class="noti-primary">';
                $html.='<div class="media"><span class="notification-bg bg-light-primary"><img style="width: 30px;" src="'.base_url().'public/img/resumen.svg"></span>';
                    $html.='<div class="media-body">';
                        $html.='<p>Solicitud de descuento ('.date("H:i a d-m-Y", strtotime($g->reg)).')</p>';
                    $html.='</div>';
                $html.='</div>';
                $html.='<div>';
                    //$html.='<h5><b style="color: #019cde;">Servicio:</b> <b>'.$g->descripcion.'</b></h5>';
                    //$html.='<h5><b style="color: #019cde;">Serie:</b> <b>'.$g->serie.'</b></h5>';
                    //$html.='<h5><b style="color: #019cde;">Total:</b> <b>$'.number_format($g->costo+$g->deposito+$g->costo_entrega+$g->costo_recolecta,2,'.',',').'</b></h5>';
                    $html.='<h5 style="font-size: 14px;"><b style="color: #019cde;">Sucursal Solicita:</b> <b>'.$g->id_alias.' - '.$g->name_suc.'</b></h5>';
                    $html.='<h5 style="font-size: 14px;"><b style="color: #019cde;">Vendedor:</b> <b>'.$g->nombre.'</b></h5>';
                    $html.='<a type="button" class="btn realizarventa btn-sm" style="width: 100%;" onclick="ver_sol_der('.$g->id.')">Ver Solicitud</a>';
                $html.='</div>';
            $html.='</li>';
        }
        echo json_encode(array("html"=>$html,"cont"=>$cont));
    }
    function ver_sol_der(){
        $params = $this->input->post();
        $idsol = $params['idsol'];
        $get=$this->ModeloCatalogos->lis_sol_descu($idsol);

        echo json_encode($get->result());
    }

}