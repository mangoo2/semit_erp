<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelClientes');
        $this->load->model('Modeloventas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['perfil']=$this->perfilid;
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('cliente/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        if($id==0){
            $data['id'] = 0;
            $data['nombre']='';
            $data['usuario']='';
            $data['correo']='';
            $data['contrasena']='';
            $data['contrasena2']='';
            $data['direccion']='';

            $data['calle']='';
            $data['cp']='';
            $data['colonia']='';

            $data['razon_social']='';
            $data['rfc']='';
            $data['cp']='';
            $data['RegimenFiscalReceptor']='';
            $data['uso_cfdi']='';
            $data['cpy']='';
            $data['saldo']='';
            $data['desactivar']=0;
            $data['motivo']='';
            $data['estado'] = '';
            $data['municipio'] = '';
            $data['diascredito']='';
            $data['dias_saldo']='';
            $data['diaspago']='';
            $data['saldopago']='';
            $data['activar_datos_fiscales']=0; 
            $data['clave']='';
            $data['celular']='';
        }else{
            $resul=$this->General_model->getselectwhere('clientes','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['nombre']=$item->nombre;
                $data['usuario']=$item->usuario;
                $data['correo']=$item->correo;
                $data['contrasena']='x1f3[5]7w78{';     
                $data['contrasena2']='x1f3[5]7w78{';

                $data['calle']=$item->calle;
                $data['cp']=$item->cp;
                $data['colonia']=$item->colonia;  
                $data['saldo']=$item->saldo;  
                $data['desactivar']=$item->desactivar;  
                $data['motivo']=$item->motivo;  
                $data['estado'] = $item->estado;
                $data['municipio'] = $item->municipio;
                $data['diascredito'] = $item->diascredito;
                $data['dias_saldo'] = $item->dias_saldo;
                $data['diaspago'] = $item->diaspago;
                $data['saldopago'] = $item->saldopago;
                $data['celular'] = $item->celular;
                $data['clave']='';
                $resuls=$this->General_model->getselectwhere('sucursal','id',$item->sucursal);
                foreach ($resuls as $s){
                    $data['clave']=$s->clave;
                }
                
            }
          
            $data['razon_social']='';
            $data['rfc']='';
            $data['cpy']='';
            $data['RegimenFiscalReceptor']='';
            $data['uso_cfdi']='';
            $data['activar_datos_fiscales']=0; 
            $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
            foreach ($resulf as $itemf) {
                $data['direccion']=$itemf->direccion;

                $data['razon_social']=$itemf->razon_social;
                $data['rfc']=$itemf->rfc;
                $data['cpy']=$itemf->cp;
                $data['RegimenFiscalReceptor']=$itemf->RegimenFiscalReceptor;
                $data['uso_cfdi']=$itemf->uso_cfdi;
                $data['activar_datos_fiscales']=$itemf->activar_datos_fiscales;
            }

        }   
        $data['estadorow']=$this->ModeloCatalogos->getselect_tabla('estado');
        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $data['get_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('cliente/formjs');
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];

        $pss_verifica = $data['contrasena'];
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($data['contrasena']);
        }

        unset($data['contrasena2']);
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'clientes');
            $id=$id;
        }
        echo $id;
    }

    public function registro_datos_pago(){
        $data=$this->input->post();
        $id=$data['id'];
        $this->General_model->edit_record('id',$id,$data,'clientes');
        echo $id;
    }

    public function registro_datos_cliente(){
        $data=$this->input->post();
        $id=$data['id'];
        //$direccion=$data['direccion'];
        $nombre=$data['nombre'];
        $resulcli=$this->General_model->getselectwhere('clientes','nombre',$nombre);
        $nombrecli='';
        foreach ($resulcli as $cli) {
            $nombrecli=$cli->nombre;
        }
        $validar=0;
        if($nombre==$nombrecli){
            $validar=1;
        }else{
            $validar=0;
        }

        if($id!=0){
            $validar=0;
        }
        //unset($data['direccion']);
        if(isset($data['desactivar'])){
            $data['desactivar']=1;
        }else{
            $data['desactivar']=0;
        }
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
            //$datosf = array('direccion'=>$direccion,'idcliente'=>$id);
            //$this->General_model->add_record('cliente_fiscales',$datosf);
        }else{
            $this->General_model->edit_record('id',$id,$data,'clientes');
            $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
            $idf=0;
            foreach ($resulf as $itemf) {
                $idf=$itemf->id;
            }
            /*$datosf = array('direccion'=>$direccion,'idcliente'=>$id);
            if($idf==0){
                $this->General_model->add_record('cliente_fiscales',$datosf);
            }else{
                $this->General_model->edit_record('id',$idf,$datosf,'cliente_fiscales');
            }*/
            $id=$id;
        }
        $info = array('validar'=>$validar,'id'=>$id); 
        echo json_encode($info);
    }

    public function registro_datos_fiscales(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        $idf=0;
        foreach ($resulf as $itemf) {
            $idf=$itemf->id;
        }
        $data['razon_social']=rtrim($data['razon_social']); // Elimina espacios al final
        if(isset($data['activar_datos_fiscales'])){
            $data['activar_datos_fiscales']=1;
        }else{
            $data['activar_datos_fiscales']=0;
        }
        if($idf==0){
            $this->General_model->add_record('cliente_fiscales',$data);
        }else{
            $this->General_model->edit_record('id',$idf,$data,'cliente_fiscales');
        }
        $id=$id;
        echo $id;
    }


    function validar_correo(){
        $correo = $this->input->post('correo');
        $result=$this->General_model->getselectwhereall('clientes',array('correo'=>$correo,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function validar(){
        $usuario = $this->input->post('usuario');
        //$result=$this->General_model->getselectwhere('clientes','usuario',$usuario);
        $result=$this->General_model->getselectwhereall('clientes',array('usuario'=>$usuario,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelClientes->get_list($params);
        $totaldata= $this->ModelClientes->total_list($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'clientes');
        $this->General_model->edit_record('idcliente',$id,$data,'cliente_fiscales');
    }

    function get_validar_razon_social(){
        $razon_social = $this->input->post('razon_social');
        //$result=$this->General_model->getselectwhere('cliente_fiscales','razon_social',$razon_social);
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('razon_social'=>$razon_social,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function get_validar_rfc(){
        $rfc = $this->input->post('rfc');
        $id = $this->input->post('id');
        //$result=$this->General_model->getselectwhere('cliente_fiscales','rfc',$rfc);
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('rfc'=>$rfc,"activo"=>1,"idcliente != "=>$id));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    
    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    function get_cliente_fiscales()
    {    
        $id=$this->input->post('id');
        $html='<table class="table table-sm" id="table_datos">
                <tbody>';
                $result=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
                foreach($result as $x){ 
                    $regimen='';
                    $resultrf=$this->General_model->getselectwhere('f_regimenfiscal','clave',$x->RegimenFiscalReceptor);
                    foreach($resultrf as $rf){ 
                        $regimen=$rf->descripcion;
                    }
                    $uso_cfdi='';
                    $resultrf=$this->General_model->getselectwhere('f_uso_cfdi','uso_cfdi',$x->uso_cfdi);
                    foreach($resultrf as $rf){ 
                        $uso_cfdi=$rf->uso_cfdi_text;
                    }
                    $html.='<tr>
                        <td>Razón social:</td>
                        <td>'.$x->razon_social.'</td>
                    </tr>
                    <tr>
                        <td>RFC:</td>
                        <td>'.$x->rfc.'</td>
                    </tr>
                    <tr>
                        <td>CP:</td>
                        <td>'.$x->cp.'</td>
                    </tr>
                    <tr>
                        <td>Régimen fiscal:</td>
                        <td>'.$regimen.'</td>
                    </tr>
                    <tr>
                        <td>Uso de CFDI:</td>
                        <td>'.$uso_cfdi.'</td>
                    </tr>';
                }
                $html.='</tbody>
            </table>';
        echo $html;
    }

    public function cliente_saldo($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;

        $data['idcliente']=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/saldo',$data);
        $this->load->view('templates/footer');
        $this->load->view('cliente/saldojs');
    }

    public function getlistventas() {
        $params = $this->input->post();     
        $getdata = $this->ModelClientes->getventas($params);
        $totaldata= $this->ModelClientes->total_getventas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    function view_productos(){
        $params = $this->input->post();
        $ventaid=$params['ventaid'];
        $result= $this->Modeloventas->ventas_detalles($ventaid);
        $html='';
        foreach ($result->result() as $item) {
            $importe=($item->cantidad*$item->precio_unitario)-$item->descuento;
            $html.='<tr>
                            <td>'.$item->cantidad.'</td>
                            <td>'.$item->nombre.'</td>
                            <td>'.$item->precio_unitario.'</td>
                            <td>'.$item->descuento.'</td>
                            <td>'.$importe.'</td>
                        </tr>';
        }
        echo $html;
    }

    function view_formapagos(){
        $params = $this->input->post();
        $ventaid=$params['ventaid'];
        $total = $params['total'];
        $html='<div class="row"><div class="col-md-12"><h4>Total: '.$total.'<h4></div></div>';
        $html.='<table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Últimos 4 dígitos</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>';
        $montog=0;
        $result= $this->Modeloventas->ventas_formaspagos($ventaid);
        foreach ($result->result() as $item) {
            $montog=$montog+$item->monto;
            $html.='<tr>
                            <td>'.$item->formapago_text.'</td>
                            <td>'.$item->ultimosdigitos.'</td>
                            <td>'.$item->monto.'</td>
                        </tr>';
        }
        $cambio=$montog-$total;
        $html.='</tbody>';
        if($cambio>0){
            $html.='<tfoot><tr><td>Cambio</td><td>'.$cambio.'</td></tr></tfoot>';
        }
        
        $html.='</table>';
        echo $html;
    }

    function reporte_excel()
    {  
      $data['anio']=date('d_m_Y');   
      $data['get_clientes']=$this->ModeloCatalogos->get_clientes();
      $this->load->view('cliente/excel',$data);
    }
}