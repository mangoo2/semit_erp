<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class OCS extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCompras'); 
        $this->load->model('ModelProductos');
        

        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoy_normal = date('d/m/Y');
        $this->horahoy_normal = date('H:i:s');
        $this->fecha_larga = date('Y-m-d H:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechacod = date('ymdHis');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,30);// perfil y id del submenu
            //$this->sucusal=1;//cuando ya esten las sucursales
            //$this->folio=$this->sucusal.'-'.$this->fechacod;
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }

    }

    public function index(){
        $data['btn_active']=3;
        $data['btn_active_sub']=30;
        $this->limpiartabla();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('compras/index');
        $this->load->view('templates/footer');
        $this->load->view('compras/indexjs');
    }

    function limpiartabla(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();

        unset($_SESSION['cost']);
        $_SESSION['cost']=array();

        unset($_SESSION['tipo']);
        $_SESSION['tipo']=array();
        unset($_SESSION['capac']);
        $_SESSION['capac']=array();
    }


    public function getlistcompras() {
        $params = $this->input->post();      
        $getdata = $this->ModeloCompras->getcompras($params);
        $totaldata= $this->ModeloCompras->total_getcompras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function view_productos(){
        $params = $this->input->post();
        $idcompras=$params['idcompras'];
        $result= $this->ModeloCompras->compras_detalles($idcompras,1);
        $html='';
        foreach ($result->result() as $item) {
            $importe=($item->cantidad*$item->precio_unitario);
            if($item->tipo==0){
                $nombre=$item->nombre;
            }else{
                $nombre=$item->codigo." - Recarga de oxígeno de ".$item->capacidad."L";
            }
            $incluye_iva = $item->incluye_iva_comp;
            $iva = $item->iva_comp;
            //log_message('error','iva: '.$iva);
            //log_message('error','incluye_iva: '.$incluye_iva);
            $sub_iva=0; $sub_isr=0;
            if($item->tipo==0){
                $precio_con_ivax=0;
                if($incluye_iva==1){
                    $precio_con_ivax=round($item->precio_unitario,6);
                    if($iva==0){
                        $incluye_iva=0;
                        $precio_con_ivax=round($item->precio_unitario,6);
                    }else{
                        $precio_con_ivax=round($item->precio_unitario,6);
                        //$precio_con_ivax=round($item->precio_unitario/1.16,2);
                        $sub_iva=round($item->precio_unitario*.16,6);
                        //log_message('error','sub_iva: '.$sub_iva);
                    }
                    if($item->porc_isr>0){
                        $sub_importe=($item->cantidad*$precio_con_ivax);
                        $sub_isr= $sub_importe*($item->porc_isr/100);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $precio_con_ivax=round($item->precio_unitario/$siva,6);
                    }else{
                        $precio_con_ivax=round($item->precio_unitario,6);  
                    }
                    $sub_isr=0;
                }
                $importe=($item->cantidad*$precio_con_ivax)+$sub_iva-$sub_isr;
            }else{
                $cant_litros = $item->cantidad * 9500;
                $importe=($cant_litros*$item->precio_unitario);
                $precio_con_ivax=$item->precio_unitario;
            }
            $html.='<tr>
                <td>'.$nombre.'</td>
                    <td>'.$item->cantidad.'</td>
                    <td>$'.number_format($precio_con_ivax,6,'.',',').'</td>
                    <td>$'.number_format($sub_iva,4,'.',',').'</td>
                    <td>$'.number_format($sub_isr,4,'.',',').'</td>
                    <td>$'.number_format($importe,4,'.',',').'</td>
                </tr>';
        }
        echo $html;
    }

    function view_prods_lote_serie(){
        $idcompras = $this->input->post("idcompras");
        $result= $this->ModeloCompras->compra_det_ls($idcompras);
        /*$html='';
        foreach ($result->result() as $item) {
            $html.='<tr>
                    <td>'.$item->idProducto.'</td>
                    <td>'.$item->nombre.'</td>
                    <td>'.$item->cantidad.'</td>
                    <td><button type="button" onclick="setLoteSerie('.$item->id_cd.')" class="btn btn-cli-edit" style="width: 50px;"></button></td>
                </tr>';
        }
        echo $html;*/
        echo json_encode(array("data"=>$result->result()));
    }

    public function get_data_lotes(){
        $idprod = $this->input->post('idprod');
        $idcomp = $this->input->post('idcomp');
        $results=$this->ModeloCompras->lotesCompraProd($idcomp,$idprod);
        echo json_encode($results);
    }

    public function get_validar_serie(){
        $serie=$this->input->post('serie');
        $result=$this->General_model->getselectwhereall('productos_sucursales_serie',array('serie'=>$serie,'activo'=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function crearCodigoLote($idsuc,$idprod,$lote,$cad){
        $codigo=$idprod.$idsuc.intval(preg_replace('/[^0-9]+/', '', $lote), 10).date("Ymd",strtotime($cad));
        return $codigo;        
    }

    function ingresar_lotes(){   
        $data=$this->input->post();
        $lotes=$data['lotes'];
        unset($data["lotes"]);
        //ingresa lotes
        $DATAl = json_decode($lotes);
        for ($i=0;$i<count($DATAl);$i++) {
            $data_arrayc=array(
            'sucursalid'=>8,
            'productoid'=>$DATAl[$i]->idproducto,
            'lote'=>$DATAl[$i]->lote,
            'caducidad'=>$DATAl[$i]->caducidad,
            'cantidad'=>$DATAl[$i]->cantidad,
            'cod_barras'=>$this->crearCodigoLote(8,$DATAl[$i]->idproducto,$DATAl[$i]->lote,$DATAl[$i]->caducidad),
            'idcompra'=>$DATAl[$i]->idcompra,
            'reg'=>$this->fecha_larga
            );
            //log_message('error','data_arrayc: '.json_encode($data_arrayc));
            if($DATAl[$i]->id==0){
                $cant_inicial=$this->ModeloCatalogos->obtener_stock_lotes($DATAl[$i]->idproducto,8);
                $cant_final=$cant_inicial+$DATAl[$i]->cantidad;
                //falta validar que exista el prod con lote y caducidad, se sume si existe o se ingrese si no.
                $this->ModeloCatalogos->Insert('productos_sucursales_lote',$data_arrayc);log_message('error','productos_sucursales_lote ingresar_lotes: OCS: ');
                
                $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('idcompra'=>$DATAl[$i]->idcompra,'idproducto'=>$DATAl[$i]->idproducto));
            }else{
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_lote',$data_arrayc,array('id'=>$DATAl[$i]->id));
            }
        }
    }

    public function get_data_series(){
        $idprod = $this->input->post('idprod');
        $idcomp = $this->input->post('idcomp');
        $results=$this->ModeloCompras->seriesCompraProd($idcomp,$idprod);
        echo json_encode($results);
    }

    function ingresar_series(){   
        $data=$this->input->post();
        $series=$data['series'];
        unset($data["series"]);

        $DATAs = json_decode($series);




        $productoid=0;
        $cant_inicial=0;
        $cant=1;
        for ($i=0;$i<count($DATAs);$i++) {
            if($DATAs[$i]->idproducto!=$productoid){
                $productoid=$DATAs[$i]->idproducto;
                $cant_inicial=$this->ModeloCatalogos->obtener_stock_series($productoid,8);
            }
            $cant_final=$cant_inicial+$cant;

            $data_array=array(
            'sucursalid'=>8,
            'productoid'=>$DATAs[$i]->idproducto,
            'serie'=>$DATAs[$i]->serie,
            'idcompras'=>$DATAs[$i]->idcompra,
            'reg'=>$this->fecha_larga
            );
            if($DATAs[$i]->id==0){

                $this->ModeloCatalogos->Insert('productos_sucursales_serie',$data_array);
                $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('idcompra'=>$DATAs[$i]->idcompra,'idproducto'=>$DATAs[$i]->idproducto));
                $cant++;
            }else{
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',$data_array,array('id'=>$DATAs[$i]->id));
            }
        }
    }

    public function delete_detalle_prod(){
        $id=$this->input->post('id');
        $table=$this->input->post('table');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,$table);
    }

    public function validar_codigo()
    {   
        $id=$this->input->post('reg');
        $codigox=$this->input->post('codigo');
        $result=$this->General_model->getselectwhere('contrasena','activo',1);
        $codigo=0;
        $id=0;
        foreach ($result as $x) {
            $codigo=$x->codigo;
            $id=$x->id;
        }
        $validarc=0;
        if($codigo==$codigox){
            $data = array('activo'=>0);
            $this->General_model->edit_record('id',$id,$data,'contrasena');
            $validarc=1;
        }else{
            $validarc=0;
        }
        echo $validarc;
        
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'compra_erp');
    }

    public function imprimir_compra($id){
        $data['idcompra']=$id;
        $data['result_productos']=$this->ModeloCompras->get_productos($id);
        $data['result_compra']=$this->ModeloCatalogos->getselectwheren('compra_erp',array('id'=>$id));
        $result_compra=$this->ModeloCompras->get_compra($id);
        $data['folio']='';
        $data['reg']='';
        $data['razon_social']='';
        $data['codigo']="";
        foreach ($result_compra as $y) {
            $data['reg']=date('d/m/Y',strtotime($y->reg));
            $data['reg2']=$y->reg;
            if($y->fecha_ingreso!="" && $y->fecha_ingreso!=null && $y->fecha_ingreso!="null"){
                $data['fecha_entrega']=date('d/m/Y',strtotime($y->fecha_ingreso));
                $data['hora_entrega']=date('H:s a',strtotime($y->fecha_ingreso));
            }else{
                $data['fecha_entrega']="";
                $data['hora_entrega']="";
            }
            $data['proveedor']=$y->nombre;
            $data['codigo']=$y->codigo;
            $data['proveedor_direccion']=$y->nombre;
            $data['observaciones']=$y->observaciones;
            $data['direccion']=$y->direccion;
            $data['cp']=$y->cp;
            $data['rfc']=$y->rfc;
        }
        $data['direccion_entrega']='';
        $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>8));
        foreach ($sucrow->result() as $s){
            $data['direccion_entrega']=$s->domicilio;
        }
        $this->load->view('reportes/compra',$data);
    }

    public function cancelarPre(){
        $id=$this->input->post('id');
        $data = array('cancelado'=>1,"fecha_cancelado"=>$this->fecha_larga);
        $this->General_model->edit_record('id',$id,$data,'compra_erp');
    }

}