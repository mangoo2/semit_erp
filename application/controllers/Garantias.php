<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Garantias extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGarantia');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){

            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,38);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=9;
        $data['btn_active_sub']=38;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('garantias/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('garantias/indexjs');
    }

    public function solicitud($id=0){
        $data['btn_active']=9;
        $data['btn_active_sub']=38;
        $data2['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('garantias/form',$data2);
        $this->load->view('templates/footer');
        $this->load->view('garantias/formjs',$data);
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $id_suc = $this->input->get('id_suc');
        //$results=$this->ModeloCatalogos->getsearchproductos_ventas_like($pro,$this->sucursal);
        $results=$this->ModeloCatalogos->getsearchproductos_ventas_like($pro,$id_suc);
        //echo $results;
        echo json_encode($results->result());
    }

    function save_solicitud(){
        $datos = $this->input->post();
        $motivo=$datos['motivo'];
        unset($datos["motivo"]);
        $prods=$datos['array_prod'];
        unset($datos["array_prod"]);

        $id_origen=$this->sucursal;
        $DATA = json_decode($prods);
        for ($i=0;$i<count($DATA);$i++) {   
            $id_origen=$DATA[$i]->id_origen;
        }

        $datag["motivo"]=$motivo;
        $datag['idpersonal']=$this->idpersonal;
        $datag['id_origen']=$id_origen;
        $datag['reg']=$this->fecha_hora_actual;
        //log_message('error','motivo: '.$motivo);
        $id=$this->General_model->add_record('garantias',$datag);

        for ($i=0;$i<count($DATA);$i++) {   
            $data['idprod']=$DATA[$i]->idprod;
            $data['id_ps_lot']=$DATA[$i]->id_ps_lot; 
            $data['id_ps_ser']=$DATA[$i]->id_ps_ser;
            $data['tipo']=$DATA[$i]->tipo;
            $data['id_origen']=$DATA[$i]->id_origen;
            $data['cant']=$DATA[$i]->cant;
            $data['id_garantia']=$id;
            $data['cant_ini']=$DATA[$i]->stock;
            //log_message('error','idprod: '.$DATA[$i]->idprod);
            $this->General_model->add_record('garantias_detalle',$data);
            /*if($DATA[$i]->id_ps_ser>0){
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array("disponible"=>0),array('id'=>$DATA[$i]->id_ps_ser));
            }*/
        }
        echo $id;
    }

    public function get_data_list(){
        $params = $this->input->post();
        $params["perfilid"]=$this->perfilid;
        $params["sucursal"]=$this->sucursal;
        $getdata = $this->ModeloGarantia->get_listGarantia($params);
        $totaldata= $this->ModeloGarantia->total_list_garatia($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function view_detalles(){
        $id = $this->input->post("id");
        $tipo = $this->input->post("tipo");
        $result= $this->ModeloGarantia->get_detalle_solicitud($id);
        $html='';

        foreach ($result as $s) {
            $txt=""; $txt_vta="";
            if($s->id_venta>0){
                $txt_vta="<br>Venta: ".$s->id_venta."<br>Fecha: ".date("d-m-Y H:i a", strtotime($s->fecha_venta));
            }
            if($s->tipo==2){
                $txt=" Lote: ".$s->lote;
            }
            if($s->tipo==1){
                $txt=" Serie: ".$s->serie;
            }
            /*$incluye_iva = $item->incluye_iva_comp;
            $iva = $item->iva_comp;
            $sub_iva=0;
            if($item->tipo==0){
                $precio_con_ivax=0;
                if($incluye_iva==1){
                    $precio_con_ivax=round($item->precio_unitario,2);
                    if($iva==0){
                        $incluye_iva=0;
                        $precio_con_ivax=round($item->precio_unitario,2);
                    }else{
                        $precio_con_ivax=round($item->precio_unitario,2);
                        //$precio_con_ivax=round($item->precio_unitario/1.16,2);
                        $sub_iva=round($item->precio_unitario*.16,2);
                        //log_message('error','sub_iva: '.$sub_iva);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $precio_con_ivax=round($item->precio_unitario/$siva,2);
                    }else{
                        $precio_con_ivax=round($item->precio_unitario,2);  
                    }
                }
                $importe=($item->cantidad*$precio_con_ivax)+$sub_iva;
            }else{
                $importe=($item->cantidad*$item->precio_unitario);
                $precio_con_ivax=$item->precio_unitario;
            }*/
            //falta validar si es estatus rechazado pintar columna de rojo con btn disabled y mostrar debajo motivo (en tipo 0)
            $sty=""; $block="none"; $dis="";
            if($tipo==0 && $s->estatus==2){
                $sty="style='background-color:rgb(147,186,31,0.7)'";
                $block="none";
            }
            if($tipo==0 && $s->estatus==3 || $tipo==0 && $s->estatus_garantia==3){
                $sty="style='background-color:rgb(255,64,64,0.7)'";
                $block="block";
                $dis="disabled";
            }if($tipo==0 && $s->estatus==1 && $s->estatus_garantia!=3){
                $sty="";
                $block="none";
                $dis="";
            }
            if($tipo==0 && $s->estatus_garantia==3){
                $sty="style='background-color:rgb(255,64,64,0.7)'";
                $block="none";
                $dis="disabled";
            }
            $txt_ext=""; $disbtn="";
            if($tipo==1){
                if($s->estatus_garantia!=1){
                    $disbtn="disabled";
                }
                $txt_ext="<button ".$disbtn." type='button' title='Rechazar' class='btn btn-danger' onclick='rechaza_prod(".$s->id.")'> <i class='fa fa-ban' aria-hidden='true'> </i></button>
                <button ".$disbtn." type='button' title='Aceptar' class='btn btn-success' onclick='acepta_prod(".$s->id.")'> <i class='fa fa-check-square' aria-hidden='true'> </i></button>";
            }
            $html.='<tr class="rowprod_'.$s->id.'" '.$sty.'>
                    <td><input type="hidden" id="rechazado_'.$s->id.'" value="'.$s->estatus.'">
                        <input type="hidden" id="id" value="'.$s->id.'">
                        <input type="hidden" id="id_garantia" value="'.$s->id_garantia.'">
                        <input type="hidden" id="id_ps" value="'.$s->id_ps.'">
                        <input type="hidden" id="id_ps_ser" value="'.$s->id_ps_ser.'">
                        <input type="hidden" id="id_psl" value="'.$s->id_psl.'">
                        <input type="hidden" id="id_venta" value="'.$s->id_venta.'">
                        <input type="hidden" id="tipo_prod" value="'.$s->tipo.'">
                        <input type="hidden" id="cant" value="'.$s->cant.'">
                        <input type="hidden" id="id_origen" value="'.$s->id_origen.'">
                        '.$s->producto.'
                    </td>
                    <td>'.$txt.' '.$txt_vta.'</td>
                    <td>'.$s->cant.'</td>
                    <td>'.$txt_ext.'
                        <!--<br><div style="display:none" class="cont_mot_'.$s->id.'"><textarea class="form-control" id="motivo"></textarea>-->
                    </td>
                </tr>
                <tr><td colspan="4"><div style="display:'.$block.'" class="cont_mot_'.$s->id.'"><label>Motivo rechazo:</label><textarea class="form-control motivo_'.$s->id.'" id="motivo" '.$dis.'>'.$s->motivo_rechazo.'</textarea></td></tr>';
            if($s->estatus_garantia==3){
                $html.='<tr><td colspan="4"><div style="display:block"><label>Motivo rechazo:</label><textarea class="form-control" id="motivo_all"disabled>'.$s->motivo_rechazo_all.'</textarea></td></tr>';
            }
        }
        echo $html;
    }

    public function rechazar_producto(){
        $id=$this->input->post('id');
        $motivo=$this->input->post('motivo');
        $data = array('estatus'=>3,"motivo_rechazo"=>$motivo);
        $this->General_model->edit_record('id',$id,$data,'garantias_detalle');  
    }

    function editDetalles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->id;
            $data['motivo_rechazo']=$DATA[$i]->motivo;
            $data['estatus']=$DATA[$i]->rechazado; 
            $id_garantia=$DATA[$i]->id_garantia; 
            $this->General_model->edit_record('id',$id,$data,'garantias_detalle');
            if($DATA[$i]->rechazado==2 && $DATA[$i]->id_venta==0){ //aceptado y de stock interno
                //$this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array("disponible"=>0),array('id'=>$DATA[$i]->id_ps_ser));

                if($DATA[$i]->tipo_prod==0){ //de stock normal
                    $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','-',$DATA[$i]->cant,'id',$DATA[$i]->id_ps,'sucursalid',$DATA[$i]->id_origen,'activo',1);
                }
                if($DATA[$i]->tipo_prod==2){ //producto de lote y caducidad
                    $this->ModeloCatalogos->updatestock('productos_sucursales_lote','cantidad','-',$DATA[$i]->cant,'id',$DATA[$i]->id_psl);
                }
                if($DATA[$i]->tipo_prod==1){ //producto de serie
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array('disponible'=>0),array('id'=>$DATA[$i]->id_ps_ser));
                }
            }
        }
        $this->General_model->edit_record('id',$id_garantia,array("estatus"=>2),'garantias');
    }

    public function rechaza_record(){
        $id=$this->input->post('id');
        $motivo=$this->input->post('motivo');
        $data = array('estatus'=>3,"motivo_rechazado"=>$motivo);
        $this->General_model->edit_record('id',$id,$data,'garantias');
    }

    public function pdf_garantia($id_venta,$id){
        $data['id']=$id;
        $data['det']= $this->ModeloGarantia->get_detalle_solicitud($id);
        if($id_venta>0){
            $data['evide']= $this->ModeloGarantia->getEvideGarantiaVenta($id,$id_venta);
        }else{
            $data['evide']= $this->ModeloGarantia->getEvideGarantia($id,$data["det"][0]->id_venta);
        }
        $this->load->view('reportes/solicitud_garantia',$data);
    }

    public function pdf_garantia_orden($id){
        $data['id']=$id;
        $data['det']= $this->ModeloGarantia->get_detalle_solicitud($id);
        $this->load->view('reportes/orden_garantia',$data);
    }

    public function getDetOrdenG(){
        $id=$this->input->post('id');
        $id_venta=$this->input->post('id_venta');
        $fecha="";
        $observ="";
        $motivo=""; $cli=""; $dir=""; $col=""; $cel=""; $tel=""; $mail=""; $rfc=""; $nom_contacto=""; $tel_contacto="";
        //validar si es de venta traer datos de orden garantia, si es prod interno traer datos de sucursal origen y motivo de garantia
        $get=$this->ModeloGarantia->getGarantiaOrden($id);
        $id=0;
        foreach($get as $o){
            $id=$o->id_og;
            $fecha=$o->fecha;
            $observ=$o->observ;
            $motivo=$o->motivo;
            if($id_venta!=0){
                $cli=$o->nom_solicita;
                $dir=$o->dir_solicita;
                $col=$o->col_solicita;
                $cel=$o->cel_solicita;
                $tel=$o->tel_solicita;
                $mail=$o->mail_solicita; 
                $rfc=$o->rfc_solicita;
                $nom_contacto=$o->nom_contacto; $tel_contacto=$o->tel_contacto;
            }else{
                $cli=$o->suc_solicita;
                $dir=$o->domicilio;
                $col="";
                $cel="";
                $tel=$o->tel;
                $mail=""; 
                $rfc="";
                $nom_contacto=""; $tel_contacto="";
            }
        }
        
        echo json_encode(array("fecha"=>$fecha,"observ"=>$observ,"id"=>$id,"cli"=>$cli,"dir"=>$dir,"col"=>$col,"cel"=>$cel,"tel"=>$tel,"mail"=>$mail,"rfc"=>$rfc,"motivo"=>$motivo,"nom_contacto"=>$nom_contacto,"tel_contacto"=>$tel_contacto));
    }

    public function save_orden(){
        $id=$this->input->post('id');
        $data["id_garantia"]=$this->input->post('id_garantia');
        $data["fecha"]=$this->input->post('fecha');
        $data["observ"]=$this->input->post('observ');
        $data["nom_solicita"]=$this->input->post('nom_solicita');
        $data["dir_solicita"]=$this->input->post('dir_solicita');
        $data["col_solicita"]=$this->input->post('col_solicita');
        $data["cel_solicita"]=$this->input->post('cel_solicita');
        $data["tel_solicita"]=$this->input->post('tel_solicita');
        $data["mail_solicita"]=$this->input->post('mail_solicita');
        $data["rfc_solicita"]=$this->input->post('rfc_solicita');

        if($id>0){
            unset($data["id_garantia"]);
            $this->General_model->edit_record('id',$id,$data,'orden_garantia');
        }else{
            $this->ModeloCatalogos->Insert('orden_garantia',$data);
        }
    }

    /* ***********************************/
    public function productos(){
        $data['btn_active']=9;
        $data['btn_active_sub']=38;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('garantias/index_prods',$data);
        $this->load->view('templates/footer');
        $this->load->view('garantias/index_prodsjs');
    }

    public function get_data_prodslist(){
        $params = $this->input->post();
        $params["perfilid"]=$this->perfilid;
        $params["sucursal"]=$this->sucursal;
        $getdata = $this->ModeloGarantia->get_listProdsGarantia($params);
        $totaldata= $this->ModeloGarantia->total_list_Prodsgaratia($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function retorna_equipo(){
        $id=$this->input->post('id');
        $comentarios=$this->input->post('comentarios');
        $data = array('retorno'=>1,"coment_retorno"=>$comentarios);
        $this->General_model->edit_record('id',$id,$data,'garantias_detalle');
        $idtranspaso=0;
        //registra y manda a traslado para que origen lo acepte de nuevo
        //$idtranspaso=$this->General_model->add_record('traspasos',array("idpersonal"=>$this->idpersonal,"status"=>1,"tipo"=>1,"reg"=>$this->fecha_hora_actual));

        $get_gd=$this->ModeloCatalogos->getselectwheren('garantias_detalle',array("id"=>$id));
        $get_gd=$get_gd->row();
        /*$datat['idsucursal_sale']=$get_gd->id_origen;
        $datat['idsucursal_entra']=$get_gd->id_origen; 
        $datat['cantidad']=$get_gd->cant;
        $datat['id_compra']=0;
        $datat['idproducto']=$get_gd->idprod;
        $datat['idpersonal']=$this->idpersonal;
        $datat['idtranspasos']=$idtranspaso;
        $datat['tipo']=0; //productos
        $this->General_model->add_record('historial_transpasos',$datat);*/
        if($get_gd->tipo!=1 && $get_gd->tipo!=2){
            $this->ModeloCatalogos->updatestockWhere('productos_sucursales','stock','+',$get_gd->cant,array('productoid'=>$get_gd->idprod,"sucursalid"=>$get_gd->id_origen));
        }
        if($get_gd->tipo==1){
            /*$data_d['idtraspasos']=$idtranspaso;        
            $data_d['idseries']=$get_gd->id_ps_ser;
            $this->General_model->add_record('traspasos_series_historial',$data_d); */
            $this->General_model->edit_record('id',$get_gd->id_ps_ser,array("disponible"=>"1"),'productos_sucursales_serie');
        }
        
        if($get_gd->tipo==2){
            /*$data_dl['idtraspasos']=$idtranspaso;        
            $data_dl['idlotes']=$get_gd->id_ps_lot;
            $data_dl['cantidad']=$get_gd->cant;
            $this->General_model->add_record('traspasos_lotes_historial',$data_dl); */
            $this->ModeloCatalogos->updatestock('productos_sucursales_lote','cantidad','+',$get_gd->cant,'id',$get_gd->id_ps_lot);
        }
        ////////////////////////
        $this->General_model->add_record('bitacora_garantias',array("id_gd"=>$id,"tipo_mov"=>1,"fecha"=>date("Y-m-d H:i:s"),"idpersonal"=>$this->idpersonal,"comentarios"=>$comentarios,"id_traslado"=>$idtranspaso));
    }

    public function changeSerie(){
        $id=$this->input->post('id');
        $serie=$this->input->post('serie');
        $comentarios=$this->input->post('comentarios');
        $id_ps_ser=$this->input->post('id_ps_ser');
        $this->General_model->add_record('bitacora_garantias',array("id_gd"=>$id,"tipo_mov"=>2,"fecha"=>date("Y-m-d H:i:s"),"idpersonal"=>$this->idpersonal,"serie"=>$serie,"comentarios"=>$comentarios));
    }

    public function repair_equipo(){
        $id=$this->input->post('id');
        $comentarios=$this->input->post('comentarios');
        $estatus=$this->input->post('estatus');
        $this->General_model->add_record('bitacora_garantias',array("id_gd"=>$id,"tipo_mov"=>$estatus,"fecha"=>date("Y-m-d H:i:s"),"idpersonal"=>$this->idpersonal,"comentarios"=>$comentarios));
    }

    public function devolver_equipo(){
        $id=$this->input->post('id');
        $comentarios=$this->input->post('comentarios');
        $nota_credito=$this->input->post('nota_credito');
        $this->General_model->add_record('bitacora_garantias',array("id_gd"=>$id,"tipo_mov"=>4,"fecha"=>date("Y-m-d H:i:s"),"idpersonal"=>$this->idpersonal,"comentarios"=>$comentarios,"nota_credito"=>$nota_credito));
    }

    public function detalleBitacora(){
        $id=$this->input->post('id');
        $html="";
        $get=$this->ModeloCatalogos->getselectwheren('bitacora_garantias',array('id_gd'=>$id));
        foreach($get->result() as $d){
            $detalle="";
            if($d->tipo_mov==1){
                $detalle="Retornado a stock, mediante traslado id ".$d->id_traslado;
            }if($d->tipo_mov==2){
                $detalle="Cambio de serie por: ".$d->serie;
            }if($d->tipo_mov==3){
                $detalle="Reparado";
            }if($d->tipo_mov==4){
                $detalle="Equipo devuelto";
            }if($d->tipo_mov==5){
                $detalle="No aplica garantía";
            }
            $html="<tr>
                <td>".date('d-m-Y H:i a', strtotime($d->fecha))."</td>
                <td>".$detalle."</td>
                <td>".$d->comentarios."</td>
            </tr>";
        }
        echo $html;
    }

    public function cargaimagen(){
        $id_garantia=$_POST['id_garantia'];
        //log_message('error', "id_garantia: ".$id_garantia); 
        $DIR_SUC=FCPATH.'uploads/garantia';
        $config['upload_path']    = $DIR_SUC;
        $config['allowed_types']  = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']       = 5000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload("file")){
            $data = array('error' => $this->upload->display_errors());
            //log_message('error', json_encode($data));                    
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $this->ModeloCatalogos->Insert('evidencia_garantia',array('id_garantia'=>$id_garantia,'id_venta'=>0,"imagen"=>$file_name,"id_personal"=>$this->idpersonal,"fecha_reg"=>$this->fecha_hora_actual));
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function cargaCartaProvee(){
        $id_garantiad=$_POST['id_garantiad'];
        //log_message('error', "id_garantia: ".$id_garantia); 
        $DIR_SUC=FCPATH.'uploads/garantia/carta';
        $config['upload_path']    = $DIR_SUC;
        $config['allowed_types']  = 'pdf';
        $config['max_size']       = 5000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload("file")){
            $data = array('error' => $this->upload->display_errors());
            //log_message('error', json_encode($data));                    
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            //$this->ModeloCatalogos->Insert('evidencia_garantia',array('id_garantia'=>$id_garantia,'id_venta'=>0,"imagen"=>$file_name,"id_personal"=>$this->idpersonal,"fecha_reg"=>$this->fecha_hora_actual));
            $this->ModeloCatalogos->updateCatalogo("garantias_detalle",array("carta_provee"=>$file_name),array('id'=>$id_garantiad));
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function deleteImg($id){
        $data = array('estatus' => 0);
        $result = $this->ModeloCatalogos->updateCatalogo("evidencia_garantia",$data,array('id'=>$id));
        echo $result;
    }

    public function getEvidencias(){
        $id_venta = $this->input->post("id_venta");
        $id = $this->input->post("id");
        $i=0; 
        if($id_venta>0){
            $getdat=$this->ModeloCatalogos->getselectwheren('evidencia_garantia',array('estatus'=>1,'id_venta'=>$id_venta));
        }else{
            $getdat=$this->ModeloCatalogos->getselectwheren('evidencia_garantia',array('estatus'=>1,'id_garantia'=>$id));
        }
        foreach ($getdat->result() as $k) {
            $img=""; $imgdet=""; $imgp="";
            $i++;
            $img = "<img src='".base_url()."uploads/garantia/".$k->imagen."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '', caption: '".$k->imagen."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/garantia/".$k->imagen."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false";  
            echo '<div class="row_'.$k->id.'">
                <div class="col-md-8">
                    <label class="col-md-12"> Imagen: '.$k->imagen.'</label>
                    <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                    <hr style="background-color: blue;">
                </div>
            </div>
            <script> 
                $("#inputFile'.$i.'").fileinput({
                  overwriteInitial: false,
                  showClose: false,
                  showCaption: false,
                  showUploadedThumbs: false,
                  showBrowse: false,
                  showRemove: false,
                  removeTitle: "Cancel or reset changes",
                  elErrorContainer: "#kv-avatar-errors-1",
                  msgErrorClass: "alert alert-block alert-danger",
                  defaultPreviewContent: "'.$img.'",
                  layoutTemplates: {main2: "{preview} {remove}"},
                  allowedFileExtensions: ["jpg", "png", "jpeg","webp"],
                  initialPreview: [
                  "'.$imgp.'"
                  ],
                  initialPreviewAsData: '.$typePDF.',
                  initialPreviewFileType: "image",
                  initialPreviewConfig: [
                      '.$imgdet.'
                  ]
                });
            </script>';
        }     
    }

    public function getCartaProvee(){
        $id = $this->input->post("id");
        $i=0; 

        $getdat=$this->ModeloCatalogos->getselectwheren('garantias_detalle',array('id'=>$id));
        foreach ($getdat->result() as $k) {
            $img=""; $imgdet=""; $imgp="";
            $i++;
            $img="";
            /*$img = "<img src='".base_url()."uploads/garantia/carta/".$k->carta_provee."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '', caption: '".$k->carta_provee."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/garantia/carta/".$k->carta_provee."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false"; */ 

            $imgp = "".base_url()."uploads/garantia/carta/".$k->carta_provee." ";
            $imgdet = "{type: 'pdf', url: '".base_url()."Garantias/file_delete/".$k->id."', caption: '".$k->carta_provee."', key:".$i."}";
            $typePDF = "true";

            echo '<div class="row_'.$k->id.'">
                <div class="col-md-8">
                    <label class="col-md-12"> Archivo: '.$k->carta_provee.'</label>
                    <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                    <hr style="background-color: blue;">
                </div>
            </div>
            <script> 
                $("#inputFile'.$i.'").fileinput({
                  overwriteInitial: false,
                  showClose: false,
                  showCaption: false,
                  showUploadedThumbs: false,
                  showBrowse: false,
                  showRemove: false,
                  removeTitle: "Cancel or reset changes",
                  elErrorContainer: "#kv-avatar-errors-1",
                  msgErrorClass: "alert alert-block alert-danger",
                  defaultPreviewContent: "'.$img.'",
                  layoutTemplates: {main2: "{preview} {remove}"},
                  allowedFileExtensions: ["pdf"],
                  initialPreview: [
                  "'.$imgp.'"
                  ],
                  initialPreviewAsData: '.$typePDF.',
                  initialPreviewFileType: "image",
                  initialPreviewConfig: [
                      '.$imgdet.'
                  ]
                });
            </script>';
        }     
    }

    function mail_solicitudGaran(){ 
        $id = $this->input->post('id');
        $getpers=$this->ModeloGarantia->getPersonalGarantia($id);
        $array_com = array();
        $array_adm = array();
        $suc_solicita=""; $cont_com=0; $cont_adm=0; $reg=""; 
        foreach ($getpers as $p) {
            $suc_solicita=$p->name_suc;
            $reg=$p->reg;
            if ($p->correo_comp!='') {
                array_push($array_com,$p->correo_comp);
                $cont_com++;  
            }if ($p->correo_adm!='') {
                array_push($array_adm,$p->correo_adm);
                $cont_adm++;
            }
        }
  
        $array_com=array_unique($array_com);
        $array_adm=array_unique($array_adm);

        log_message('error','array_com unique: '.json_encode($array_com));
        log_message('error','array_adm unique: '.json_encode($array_adm));

        $asunto='Solicitud de garantíaa';
        $this->load->library('email');
        //$this->load->library('email', NULL, 'ci_email');

        $config['protocol'] = 'smtp';
        $config["smtp_host"] ='mail.semit.mx'; 
        $config["smtp_user"] = 'noreply@semit.mx';
        $config["smtp_pass"] = 'l)sT^Iwk,e7n';
        $config["smtp_port"] = '465';
        $config["smtp_crypto"] = 'ssl';
        $config['charset'] = 'utf-8'; 
        $config['wordwrap'] = TRUE; 
        $config['validate'] = true;
        $config['mailtype'] = 'html';
         
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@semit.mx','Semit');

        //$this->email->to($correo,'Solicitud de traspaso');
        if($cont_com>0){
            $this->email->to($array_com);
        }
        if($cont_adm>0){
            $this->email->cc($array_adm);
        }
        $this->email->subject($asunto);
        
        /*$message =utf8_encode("Solicitud de garantía: ".$suc_solicita ." registra solicitud de garantía de producto semit.
        <br>Con id: ".$id."
        <br>Con fecha: ".date("H:i a d-m-Y", strtotime($reg)));*/
        $message ="Solicitud de garantía: ".$suc_solicita ." registra solicitud de garantía de producto semit.
        <br>Con id: ".$id."
        <br>Con fecha: ".date("H:i a d-m-Y", strtotime($reg));
        $this->email->message($message);
        $this->email->send();
    }

}