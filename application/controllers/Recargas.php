<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recargas extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    $this->load->model('General_model');
	    $this->load->model('ModelProductos');
	    if (!$this->session->userdata('logeado')){
	     	redirect('/Login');
	  	}else{
	    	$this->sucursal=$this->session->userdata('sucursal');
	        $this->idpersonal=$this->session->userdata('idpersonal');
	        $this->perfilid=$this->session->userdata('perfilid');
	      
	      	$this->load->model('Login_model');
	      	$permiso=$this->Login_model->getviewpermiso($this->perfilid,35);
	      	if ($permiso==0) {
	      		redirect('/Login');
	      	}
	  	}
  		date_default_timezone_set('America/Mexico_City');
  		$this->fecha=date("Y-m-d H:i:s");
  		$this->fecha2=date("d-m-Y");
   }

	public function index(){
		$data['btn_active']=2;
        $data['btn_active_sub']=35;
		$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('recargas/index');
        $this->load->view('templates/footer');
        $this->load->view('recargas/indexjs');
	}

	public function alta($id=0){
		$data['btn_active']=2;
        $data['btn_active_sub']=35;
        $data["band_tp"]=0;
        //$get_tanque=$this->General_model->get_select("recargas",array("tipo"=>0,"estatus"=>1,"sucursal"=>$this->sucursal));
        $get_tanque=$this->General_model->get_select("recargas",array("tipo"=>0,"estatus"=>1));

        $get_tipo=$this->General_model->get_select("recargas",array("id"=>$id));
        if($get_tanque->num_rows()>0){
        	//$get=$get_tanque->row();
        	$data["band_tp"]=1;
        }
        $data["tipo"]="";
        if($id>0){
        	$get_tipo=$get_tipo->row();
        	$data["tipo"]=$get_tipo->tipo;
        	//$data['rec']=$this->ModelProductos->getTanquePadreSuc($id);
        	if($get_tipo->tipo==1){ //recarga normal
        		$data['rec']=$this->ModelProductos->getTanquePadreSuc($id);
        	}else{ //registro de tanque padre de 9500l
        		$data['recargas']=$this->ModelProductos->getTanquePadreSucursales($id);
        	}
        }
		$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('recargas/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('recargas/formjs');
	}

	public function getDataTable() {
	    $params = $this->input->post();
	    $datas = $this->General_model->getselectwhere_group("recargas",array("estatus"=>1),"codigo");
	    $json_data = array("data" => $datas);
        echo json_encode($json_data);
    } 

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data['fecha_reg']=$this->fecha;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('recargas',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'recargas');
            $id=$id;
        }
        echo $id;
    }

    public function registro_datos2(){
	    $datos = $this->input->post('data');
	    $DATA = json_decode($datos);
	    for ($i=0;$i<count($DATA);$i++) {   
	    	if(isset($DATA[$i]->codigo)){
			    $data['sucursal']=$DATA[$i]->sucursal;        
			    $data['codigo']=$DATA[$i]->codigo;
			    $data['capacidad']=$DATA[$i]->capacidad;
			    $data['precioc']=$DATA[$i]->precioc;
			    $data['tipo']=$DATA[$i]->tipo;     
	      		if($DATA[$i]->id==0){
		        	$this->General_model->add_record('recargas',$data);
		      	}else{
		       		$this->General_model->edit_record('id',$DATA[$i]->id,$data,'recargas');
		      	}
		    }
	    }
	}

}
