<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traspasos extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelTraspasos');
        $this->load->model('Modeloventas');
        $this->load->model('ModelCotizaciones');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d H:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('H:i:s');
        if ($this->session->userdata('logeado')){
            //$this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,23);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        $data['perfil']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/indexjs');
    }

    public function form(){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        $data['tanques']=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>$this->sucursal,'tipo'=>0,'estatus'=>1));
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/formjs');
    }

    public function solicitudReco(){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3 || $this->session->userdata('perfilid')==4) {

        }else{
            redirect('Traspasos');
        }

        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/form_solicita',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/form_solicitajs');
    }

    public function solicitudManual(){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        //$data['tanques']=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>8,'tipo'=>0,'estatus'=>1));
        $data['tanques']=$this->ModelTraspasos->get_all_stock_tanque(8,12);
        //$data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
        $data['sucrow']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1,'id!='=>8),"orden");
        
        //$data['suc_surte']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>$this->sucursal));
        $data['suc_surte']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1,'id!='=>$this->sucursal),"orden");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/form_solicita_manual',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/form_solicita_manualjs');
    }

    public function soliciManualAlmacen(){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        if($this->session->userdata('perfilid')==1 || $this->session->userdata('perfilid')==2 || $this->session->userdata('perfilid')==3 || $this->session->userdata('perfilid')==5) {

        }else{
            redirect('Traspasos');
        }
        $data['tanques']=$this->ModelTraspasos->get_all_stock_tanque(8,12);
        //$data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
        $data['sucrow']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
    
        //$data['suc_surte']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>$this->sucursal));
        $data['suc_surte']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
        //$data['ocs']=$this->ModeloCatalogos->getselectwheren('compra_erp',array('activo'=>1,'estatus='=>1,'distribusion'=>0));
        $data['ocs']=$this->ModelTraspasos->getComprasProds();
        $data["perfilid"]=$this->perfilid;
        $data['catp_salida']=$this->fecha_hora_actual;
        $data['catp_llegada']= date('Y-m-d H:i:s', strtotime($this->fecha_hora_actual . ' +2 hours'));
        $data['rest_op']=$this->ModeloCatalogos->getselectwheren('operadores',array('activo'=>1,'TipoFigura'=>'01'));
        $data['rest_veh']=$this->ModeloCatalogos->getselectwheren('transporte_federal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/form_solicita_manualalm',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/form_solicita_manualalmjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelTraspasos->get_list($params,$this->perfilid,$this->idpersonal);
        $totaldata= $this->ModelTraspasos->total_list($params,$this->perfilid,$this->idpersonal); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductos_tipo_like($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchproductosAsigna(){
        $pro = $this->input->get('search');
        $suc = $this->input->get('destino');
        $results=$this->ModeloCatalogos->searchproductos_tipo_likeAsigna($pro,$suc);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchproductosPV(){
        $pro = $this->input->get('search');
        $id_sucursal_salida = $this->input->get('id_sucursal_salida');
        $results=$this->ModeloCatalogos->getsearchproductos_tipo_likePV($pro,$id_sucursal_salida);
        //echo $results;
        echo json_encode($results->result());
    }

    function getCantidadTras(){
        $id = $this->input->post('id');
        $id_suc = $this->input->post('id_suc');
        $get=$this->ModeloCatalogos->getCantidadTraslado($id,$id_suc);
        $tot_tras=0;
        $tot_tras2=0;
        $traslado_stock_cant=0;
        $traslado_lote_cant=0;
        //$tot_tras_all =0;
        if($get->num_rows()>0){
            $get = $get->row();
            $tot_tras = $get->tot_tras; 
            $tot_tras2=$get->tot_tras2;
            $traslado_stock_cant=$get->traslado_stock_cant;
            $traslado_lote_cant=$get->traslado_lote_cant;
            //$tot_tras_all =$get->tot_tras_all;
        }
        echo json_encode(array("tot_tras"=>$tot_tras,"tot_tras2"=>$tot_tras2,"traslado_stock_cant"=>$traslado_stock_cant,"traslado_lote_cant"=>$traslado_lote_cant));
        //echo json_encode(array("tot_tras_all "=>$tot_tras_all ));
    }

    function searchproductosDeta(){
        $pro = $this->input->get('search');
        $idsuc = $this->input->get('idsuc');
        //log_message('error','idsuc : '.$idsuc);
        $results=$this->ModeloCatalogos->getsearchproductos_tipo_deta_like($pro,$idsuc);
        //echo $results;
        echo json_encode($results->result());
    }
    function varificar_material_peligroso(){
        $params = $this->input->post();
        $id = $params['id'];
        $strq="SELECT pro.id,pro.idProducto, pro.nombre, pro.servicioId_sat,ser.nombre as servicio, ser.material_peligroso FROM productos as pro INNER JOIN f_servicios as ser on ser.Clave=pro.servicioId_sat WHERE pro.id='$id'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());
    }

    function search_all_productos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductoslike_all($pro);
        echo json_encode($results->result());
    }

    function get_unidad_prod(){
        $id = $this->input->post('id');
        $get=$this->ModeloCatalogos->get_unidad_producto($id);
        echo $get->nom_unidad;
    }

    function getLitrosSuc(){
        $id = $this->input->post('origen');
        $id_prod = $this->input->post('id_prod');
        //$results=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>$id,"tipo"=>0));
        $results=$this->ModelTraspasos->get_all_stock_tanque($id,$id_prod);
        //$get=$results->row();
        if(isset($results[0]->stock)){
            echo json_encode(array("codigo"=>$results[0]->codigo,"stock"=>$results[0]->stock,"id"=>$results[0]->id,"traslado"=>$results[0]->traslado_stock_cant));
        }else{
            echo json_encode(array("codigo"=>"","stock"=>"0","id"=>"0"));
        }
    }

    function get_productos_sucursales(){
        $id_prod = $this->input->post('id_prod');
        $tipo = $this->input->post('tipo');
        $html='';
        
        /*$result=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('activo'=>1,'productoid'=>$id_prod,"sucursalid!="=>$this->sucursal));*/
        $result=$this->ModelTraspasos->get_productos_sucursales_order($id_prod,$this->sucursal,$this->perfilid);
        //$arry_suc =array("","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General");
        foreach ($result as $p){
            //log_message('error','sucursalid : '.$p->sucursalid);
            if($p->sucursalid!="" && $p->sucursalid!=0){
                if($tipo==0){
                    $stock=$p->stock-$p->traslado_stock_cant;
                }if($tipo==1){ //series
                    $gets=$this->ModelTraspasos->get_all_stock_serie($id_prod,$p->sucursalid);
                    $stock=$gets->stock_serie-$gets->tot_tras;
                }if($tipo==2){ //lote
                    $gets=$this->ModelTraspasos->get_all_stock_lote($id_prod,$p->sucursalid);
                    $stock=$gets->stock_lote-$gets->tras_stock_lote;
                }
                if($stock<=0){
                    $dis="disabled";
                }else{
                    $dis="";
                }
                $html.='<tr>
                        <td>'.$p->name_suc.'</td>
                        <td>'.number_format($p->precio,2).'</td>
                        <td>'.$stock.'</td>
                        <td><button '.$dis.' type="button" class="btn btn-success" onclick="aceptarPS('.$p->productoid.','.$p->sucursalid.','.$stock.')"> <i class="fa fa-check-square" aria-hidden="true"></i></button></td>
                    </tr>';
                }
        }              
        echo $html;
    }

    function validar_dia_solicita(){
        $dia = $this->input->post('dia');
        $sucursal = $this->input->post('sucursal');
        //$getdest=$this->ModeloCatalogos->getselectwheren('solicitud_traspaso',array('dia_venta'=>$dia));
        //$getdest=$this->ModelTraspasos->getSolicitaDia($dia,$this->sucursal);
        $getdest=$this->ModelTraspasos->getSolicitaDia($dia,$sucursal);

        if($getdest->num_rows()>0){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    function getlistaProds(){
        $dia = $this->input->post('dia');
        $sucursal = $this->input->post('sucursal');
        //$results=$this->Modeloventas->getProdsVentas($dia,$this->sucursal);
        $results=$this->Modeloventas->getProdsVentas($dia,$sucursal);
        echo json_encode($results);
    }

    function registro_solicitud(){
        $datatra['idpersonal']=$this->idpersonal;
        $datatra['status']=0;
        $datatra['tipo']=0; //tipo sol. por ventas
        $idtranspaso=$this->General_model->add_record('traspasos',$datatra);

        $data = $this->input->post();
        $suc = $data['suc'];
        $prods=$data['prods'];
        unset($data["prods"]);
        unset($data["suc"]);

        $dia_venta=$data['dia_venta'];
        unset($data["dia_venta"]);

        $this->General_model->add_record('solicitud_traspaso',array("id_traspaso"=>$idtranspaso,"id_usuario"=>$this->idpersonal,"dia_venta"=>$dia_venta,"fecha_reg"=>date("Y-m-d H:i:s")));
        //$datos = $this->input->post('data');
        $DATA = json_decode($prods);
        $idcompra=0;
        for ($i=0;$i<count($DATA);$i++) {   
            if($DATA[$i]->cantidad!='' && $DATA[$i]->cantidad>0){
                $datainfo['idsucursal_sale']=8;
                $datainfo['idsucursal_entra']=$suc; 
                $datainfo['cantidad']=$DATA[$i]->cantidad;
                $datainfo['idproducto']=$DATA[$i]->idprod;
                $datainfo['idpersonal']=$this->idpersonal;
                $datainfo['idtranspasos']=$idtranspaso;
                $datainfo['cant_ini']=$DATA[$i]->cantidad_ini;
                $datainfo['tipo']=0; //prod normal
                $this->General_model->add_record('historial_transpasos',$datainfo);
            }   
        }
        echo $idtranspaso;
        //disparar mail de notificacion
        //$this->mail_solicitud($idtranspaso); //
    }

    function get_tabla_producto(){    
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        /*if($tipo==0){
            $get_productos=$this->ModelTraspasos->get_productos_stock_suc($id);
        }else{
            $get_productos=$this->Modeloventas->get_tanques_stock_suc();
        }*/

        $html='<hr><table class="table table-sm" id="table_datos">
                <thead>
                    <tr style="background: #009bdb;">
                        <th scope="col" style="color: white !important;  font-size: 15px;"></th>
                        <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>';
                        $cont_suc=0; $tr_sucs=""; $tr_sucsol=""; $tr_suc_dest="";
                        $sucs=$this->General_model->getselectwhere_orden_asc('sucursal',array('activo'=>1),"orden");
                        foreach($sucs as $s){
                            $html.='<th scope="col" style="color: white; font-size: 15px;">'.$s->name_suc.'</th>';
                            ${'sucu'.intval($s->id)}="";
                            if($s->id==$this->sucursal){
                                ${'sucu'.intval($s->id)}='style="background:#93ba1f;"'; 
                                $transsuc=$s->name_suc;
                            }
                            if($tipo!=0){ //recarga
                                $get_productos=$this->Modeloventas->get_tanques_stock_suc2($s->id);
                            }
                            else{ //stock
                                $get_productos=$this->ModelProductos->getStockSucursal2($id,$s->id);
                            }
                            foreach($get_productos as $x){
                                $img=''; 
                                if($tipo==0){
                                    $result=$this->General_model->get_productos_files($x->id);
                                    foreach ($result as $z){
                                        $img='<img style="width: 100px" src="'.base_url().'uploads/productos/'.$z->file.'"';    
                                    }
                                }
                                if($x->stock==0){
                                    ${'stocktxt'.intval($x->id_suc)}='<span class="badge m-l-10" style="background:red; border-radius: 15px; font-size: 18px;"><b >0</b></span>';
                                    ${'dis'.intval($x->id_suc)}="disabled";
                                    $tr_sucs.='<td></td>';
                                }else{
                                    ${'stocktxt'.intval($x->id_suc)}=$x->stock;
                                    $tr_sucs.='<td>'.${'stocktxt'.intval($x->id_suc)}.'</td>';
                                }
                                $tr_sucsol.='<span class="switch switch-icon">
                                            <label>
                                                <input '.${'dis'.intval($x->id_suc)}.' type="radio" class="check_pago" name="check_pago" value="'.$x->id_suc.'" onclick="check_sy_btn('.$x->id_suc.','.$x->stock.')">
                                                <span style="border: 2px solid #009bdb;"></span>
                                            </label>
                                        </span>';
                                $tr_suc_dest.='<span class="switch switch-icon">
                                        <label>
                                            <input type="radio" class="check_pagoy" name="check_pagoy" value="2" onclick="check_sz_btn('.$x->id_suc.')">
                                            <span style="border: 2px solid #009bdb;"></span>
                                        </label>
                                    </span>';
                                if($tipo==0){
                                    $nombre=$x->nombre;
                                    $esp="";
                                }else{
                                    $nombre=$x->codigo."- Recarga de oxígeno";
                                    $esp="L";
                                }
                            }
                        }
                            
                    $html.='</tr>
                </thead>
                <tbody>';
                    $html.='<tr>
                        <td>'.$img.'</td>
                        <td>'.$nombre.' '.$esp.'</td>
                        '.$tr_sucs.'
                    </tr>';
                    $html.='<tr>
                        <td></td>
                        <td>Sucursal solicitante</td>
                        '.$tr_sucsol.'
                    </tr>';
                    $html.='<tr>
                        <td></td>
                        <td>Sucursal destino</td>
                        '.$tr_suc_dest.'
                    </tr>';
                        
                $html.='</tbody>
                </table><hr>
                <div align="right">
                    <a type="button" class="btn estilo_btn btn-sm btn_registro" onclick="guardar_registro('.$tipo.')">Agregar producto</a>
                </div>';
        echo $html;
    }

    function registro_transferir(){
        $id=$this->input->post('id');
        if($id==0){
            $data['idpersonal']=$this->idpersonal;
            $id=$this->General_model->add_record('traspasos',$data);
        }        
        echo $id;
    }

    function transferir_cantidad_sucursal(){
        $idsucursal = $this->input->post('sucursaly');
        $idsucursal_transferir = $this->input->post('sucursalzn');
        $cantidad = $this->input->post('cantidad');
        $idproducto = $this->input->post('idproducto');
        $idtranspaso = $this->input->post('idtranspaso');
        $tipo = $this->input->post('tipo'); //0=prod, 1=recarga

        $datainfo['idsucursal_sale']=$idsucursal;
        $datainfo['idsucursal_entra']=$idsucursal_transferir;
        $datainfo['cantidad']=$cantidad;
        $datainfo['idproducto']=$idproducto;
        $datainfo['idpersonal']=$this->idpersonal;
        $datainfo['idtranspasos']=$idtranspaso;
        $datainfo['tipo']=$tipo;
        //var_dump($datainfo);
        
        $this->db->trans_start();
        $id=$this->General_model->add_record('historial_transpasos',$datainfo);
        if($tipo==0){ // es tipo de traslado, 0=prod, 1=recargas
            $this->ModelTraspasos->sumar_sucursal($idproducto,$idsucursal_transferir,$cantidad);
            $this->ModelTraspasos->resta_sucursal($idproducto,$idsucursal,$cantidad);
        }else{ 
            $getdest=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>0,"estatus"=>1,"sucursal"=>$idsucursal_transferir));
            if($getdest->num_rows()>0){
                $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal_transferir,$cantidad,"+");
            }else{
                //$getorig=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>0,"estatus"=>1,"sucursal"=>$idsucursal));
                $getorig=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$idproducto));
                $getorig=$getorig->row();
                $this->ModeloCatalogos->Insert('recargas',array("codigo"=>"TO9500","capacidad"=>9500,"preciov"=>0,"precioc"=>$getorig->precioc,"tipo"=>0,"stock"=>$cantidad,"fecha_reg"=>date("Y-m-d H:i:s"),"sucursal"=>$idsucursal_transferir));   
            }
            $data = array('status'=>2);
            $datah = array('status'=>2,'idpersonalingreso'=>$this->idpersonal,'fechaingreso'=>$this->fecha_hora_actual,'cantidadingreso'=>$cantidad);
            $this->General_model->edit_record('id',$idtranspaso,$data,'traspasos'); 
            $this->General_model->edit_record('idtranspasos',$id,$datah,'historial_transpasos');
            $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal,$cantidad,"-");
        }
        $this->db->trans_complete();
    }
     
    function get_transpasos(){
       $id = $this->input->post('id');
       $tipo = $this->input->post('tipo'); 
       $html='<table class="table table-sm" id="table_datos" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">Producto</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Sucursal salida</th>
                    <th scope="col">Sucursal solicita</th>';
                    if($tipo==1){
                        $html.='<th scope="col"></th>';
                    }
                $html.='</tr>
            </thead>
            <tbody>';
                $result=$this->General_model->get_productos_traspasos($id,$this->sucursal);
                foreach ($result as $x){
                    if($x->rechazado==1){
                        $style="style='background-color:rgb(255,64,64,0.7)'";
                    }else{
                        $style="";
                    }
                    if($x->tipo==0){
                        $txt="";
                        if($x->serie!=""){
                            $txt="<br>Serie: ".$x->serie;
                        }
                        if($x->lote!=""){
                            $txt="<br>Lote: ".$x->lote;
                        }
                        $cantidad=$x->cantidad;
                        $nombre=$x->idProducto ." / ".$x->nombre.$txt;
                        $precio=number_format($x->precio,4);
                    }else{
                        $nombre=$x->codigo." - Recarga de oxígeno";
                        $cantidad= ($x->cantidad/9500)." Tanques.<br>(".$x->cantidad." L)";
                        $precio=number_format($x->precioc,6);
                    }
                    $html.='<tr '.$style.'>
                        <td>'.$nombre.'</td>
                        <td>'.$cantidad.'</td>
                        <td>'.$precio.'</td>
                        <td>'.$x->name_sucx.'</td>
                        <td>'.$x->name_sucxy.'</td>
                        ';
                        if($tipo==1){
                            $html.='<td><a onclick="eliminar_registro('.$x->id.')" class="btn"><i class="fa fa-trash" style="color:red"></i></a></td>';
                        }
                    $html.='</tr>';
                }
            $html.='</tbody>
        </table>';
       echo $html;
    }

    public function delete_record_transpaso(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'traspasos');
        /*$resul=$this->General_model->getselectwhere('historial_transpasos','id',$id);
        foreach ($resul as $x) {
            $this->ModelTraspasos->resta_sucursal($x->idproducto,$x->idsucursal_entra,$x->cantidad);
            $this->ModelTraspasos->sumar_sucursal($x->idproducto,$x->idsucursal_sale,$x->cantidad);
        }*/    
    }

    public function rechazar_traspaso(){
        $id=$this->input->post('id');
        $motivo=$this->input->post('motivo');
        $data = array('rechazado'=>1,"motivo_rechazo"=>$motivo);
        $this->General_model->edit_record('id',$id,$data,'traspasos');  
    }

    public function get_motivo(){
        $id=$this->input->post('id');
        $getm=$this->ModeloCatalogos->getselectwheren('traspasos',array('id'=>$id));
        $getm=$getm->row();
        echo $getm->motivo_rechazo;
    }

    public function delete_prod_solicitud(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'historial_transpasos');    
    }

    public function rechaza_prod_solicitud(){
        $id=$this->input->post('id');
        $motivo=$this->input->post('motivo');
        $data = array('rechazado'=>1,"motivo_rechazo"=>$motivo);
        $this->General_model->edit_record('id',$id,$data,'historial_transpasos');    
    }

    public function get_series_tras(){
        $id=$this->input->post('id');
        $id_suc=$this->input->post('id_suc');
        $opt="";
        $result_serie=$this->ModeloCatalogos->get_prod_suc($id,$id_suc);
        foreach ($result_serie as $se){
            $opt.='<option value="'.$se->id.'">'.$se->serie.'</option>';
        }     
        echo json_encode(array("html"=>$opt));
    }

    public function get_lotes_tras(){
        $id=$this->input->post('id');
        $id_suc=$this->input->post('id_suc');
        $opt="";
        $result_lote=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$id,'sucursalid'=>$id_suc,"cantidad > "=>0));
        foreach ($result_lote->result() as $lo){
            $opt.='<option value="'.$lo->id.'" data-cantidad="'.$lo->cantidad.'" data-lotetxt="'.$lo->lote.'">'.$lo->lote.' / Cantidad:'.$lo->cantidad.'</option>';
        }      
        echo json_encode(array("html"=>$opt));
    }

    function asignaProductosTras($id){
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        $data['perfil']=$this->perfilid;
        $data["det"]=$this->General_model->get_productos_traspasos($id,0,0,0,0,1,0);
        $data["id"]=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('traspasos/asignaProdSolicitud',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/asignaProdSolicitudjs');
    }

    function get_transpasos_apartado(){
        $id = $this->input->post('id');
        $html='';
        $cont_y=0;
        $result=$this->General_model->get_productos_traspasos($id);
        foreach ($result as $x){
            $html.='<div class="row g-3 tr_fath_'.$x->id_ht.'">
                  <div class="col-md-12">';
                    //if($cont_y==0){
                        $html.='<h3><span style="color:#152342">Sucursal solicitante: </span><span style="color:#93ba1f">'.$x->name_sucxy.'</span></h3>
                        <h3><span style="color:#152342">Sucursal de salida: </span><span style="color:#93ba1f">'.$x->name_sucx.'</span></h3>';
                    //}
                    $html.='</div>
                </div>';
                $html.='<table class="table table-sm" id="table_datos" style="width:100%;">
                <thead>
                    <tr class="tr_fath_'.$x->id_ht.'">
                        <th style="color:#152342" scope="col">Producto</th>
                        <th style="color:#152342" scope="col">Cantidad</th>
                        <th style="color:#152342" scope="col">Eliminar / Rechazar</th>';
                    $html.='</tr>
                </thead>
                <tbody>';
                if($x->tipo==0){
                    $nombre=$x->nombre;
                    if($x->tipo_prod==0){
                        $stock=$x->stockps-$x->traslado_stock_cant;
                    }
                    if($x->tipo_prod==1){
                        $nombre = $x->nombre;//." Serie: ".$x->serie;
                        $stock=$x->stock_series-$x->tot_tras;
                    }if($x->tipo_prod==2){
                        $nombre = $x->nombre;//." Lote: ".$x->lote;
                        $stock=$x->stock_lotes-$x->tras_stock_lote;
                    }
                }else{
                    $nombre=$x->codigo." - Recarga de oxígeno de ".$x->capacidad."L";
                    $stock=$x->stock_recarga-$x->traslado_tanque_cant;
                }
                
                if($x->tipo_prod==0 || $x->tipo_prod==1 || $x->tipo_prod==2){
                    $tipo_prod=$x->tipo_prod;
                } 
                if($x->tipo==1){
                    $tipo_prod=0;
                } 
                if($x->rechazado==1){
                    $disp="block";
                    $disabled="disabled";
                }else{
                    $disp="none";
                    $disabled="";
                }

                $html.='<tr class="rowprod_'.$x->id_ht.'">
                    <td style="color:#152342"><input type="hidden" id="id_ht" value="'.$x->id_ht.'">'.$nombre.'</td>
                    <td style="color:#152342">
                        <input '.$disabled.' type="number" class="form-control cant_'.$x->id_ht.' cantidad_serie_'.$cont_y.'" id="cantidad_serie" value="'.$x->cantidad.'" onchange="changeCant('.$x->tipo.','.$stock.','.round($x->cantidad,0).','.$cont_y.')"></td>
                    <td style="color:#152342"><button '.$disabled.' class="btn_del_'.$x->id_ht.' btn btn-danger btn_delete_conc_'.$cont_y.'" type="button" onclick="deleteProd('.$x->id_ht.','.$tipo_prod.')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        <button '.$disabled.' title="Rechazar concepto" class="btn_recha_'.$x->id_ht.' btn btn-danger btn_delete_conc_'.$cont_y.'" type="button" onclick="rechazaProd('.$x->id_ht.','.$tipo_prod.')"><i class="fa fa-close" aria-hidden="true"></i></button>
                    </td>';
                $html.='</tr>';
                
                $html.='</tbody>';
                if($x->tipo_prod==0){
                    $html.='
                    <tfoot>
                        <tr>
                            <td><div style="display:'.$disp.'" class="row col-lg-12 cont_rechazo_'.$x->id_ht.'">
                                <label>Motivo de rechazo</label>
                                <textarea class="form-control motivo_concep_'.$x->id_ht.'" id="motivo_rechazo_conc">'.$x->motivo_rechazo.'</textarea>
                            </div></td>
                        </tr>  
                    </tfoot>';
                }
                $html.='</table>';
                if($x->tipo_prod==1){
                    $html.='<table class="table table-sm" style="width:100%;">
                    <tbody>';

                    $html.='<tr class="rowserie_'.$x->id_ht.'">
                        <td style="color:#152342">';
                            $html.='<select '.$disabled.' class="form-control selec_'.$x->id_ht.'" id="series_x">';
                            $html.='<option value="0" selected>Selecciona una opción</option>';
                            //$result_serie=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('activo'=>1,'productoid'=>$x->idproducto,'sucursalid'=>$x->idsucursal_sale,"disponible"=>1));
                            $result_serie=$this->ModeloCatalogos->get_prod_suc($x->idproducto,$x->idsucursal_sale);
                            foreach ($result_serie as $se){
                                if($se->id_traslado_serie==0){
                                    $html.='<option value="'.$se->id.'">'.$se->serie.'</option>';
                                }
                            }              
                        $html.='</select></td>
                        <td style="color:#152342"><button '.$disabled.' class="btn_add_'.$x->id_ht.' btn btn-pill btn-primary" type="button" onclick="add_traspasos('.$cont_y.')"><i class="fa fa-plus"></i></button>
                        </td>';
                    $html.='</tr>';
                    $html.='</tbody>
                        <tfoot>
                            <tr>
                                <td><div style="display:'.$disp.'" class="row col-lg-12 cont_rechazo_'.$x->id_ht.'">
                                    <label>Motivo de rechazo</label>
                                    <textarea class="form-control motivo_concep_'.$x->id_ht.'" id="motivo_rechazo_conc">'.$x->motivo_rechazo.'</textarea>
                                </div></td>
                            </tr>  
                        </tfoot>
                    </table>';
                    $html.='<table class="table table-sm" id="table_datos_traspasos" style="width:100%;">
                    <tbody>';
                    $html.='</tbody>
                    </table>';
                }else if($x->tipo_prod==2){
                    $html.='<table class="table table-sm" style="width:100%;">
                    <tbody>';
                    $html.='<tr class="rowlote_'.$x->id_ht.'">
                        <td style="color:#152342">';
                            $html.='<select '.$disabled.' class="form-control selec_'.$x->id_ht.'" id="lote_x">';
                            $html.='<option value="0" selected>Selecciona una opción</option>';
                            $result_lote=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$x->idproducto,'sucursalid'=>$x->idsucursal_sale));
                            foreach ($result_lote->result() as $lo){
                                $html.='<option value="'.$lo->id.'" data-cantidad="'.$lo->cantidad.'" data-lotetxt="'.$lo->lote.'">Lote: '.$lo->lote.' / Cantidad:'.$lo->cantidad.'</option>';
                            }              
                        $html.='</select></td>
                        <td><input '.$disabled.'class="form-control" type="number" id="cantidad_lo"></td>
                        <td style="color:#152342"><button '.$disabled.' class="btn_add_'.$x->id_ht.' btn btn-pill btn-primary" type="button" onclick="add_traspasos_lotes('.$cont_y.')"><i class="fa fa-plus"></i></button></td>';
                    $html.='</tr>';
                    $html.='</tbody>
                        <tfoot>
                            <tr>
                                <td><div style="display:'.$disp.'" class="row col-lg-12 cont_rechazo_'.$x->id_ht.'">
                                    <label>Motivo de rechazo</label>
                                    <textarea class="form-control motivo_concep_'.$x->id_ht.'" id="motivo_rechazo_conc">'.$x->motivo_rechazo.'</textarea>
                                </div></td>
                            </tr>  
                        </tfoot>
                    </table>';
                    $html.='<table class="table table-sm" id="table_datos_traspasos_lotes" style="width:100%;">
                    <tbody>';
                    $html.='</tbody>
                    </table>';
                }
                
            $html.='<div class="row g-3 btn_add_tras_'.$x->id_ht.'">
                    <div class="col-md-6">
                        <button type="button" '.$disabled.' class="btn_addall_'.$x->id_ht.' btn realizarventa btn-sm btn_transito btn_transito_'.$cont_y.'" style="width: 100%;" onclick="update_transito('.$id.','.$tipo_prod.','.$cont_y.')">Agregar traspaso</button>
                    </div>
                    <div class="col-md-6">
                                 
                    </div>
                    <hr>
                </div>';
            $cont_y++;
        }  
       echo $html;
    }

    public function update_record_transpaso_transito(){
        $id=$this->input->post('id');
        $data = array('status'=>1);
        $this->General_model->edit_record('id',$id,$data,'traspasos');    
    }

    function get_transpasos_transito(){
        $id = $this->input->post('id');
        $html=''; $tipo_rec=0; $cont_det=0; $tipo_tras=0;
        $result=$this->General_model->get_productos_traspasos($id,0,0,1);
        foreach ($result as $x){
            $cont_det++;
            $tipo_tras=$x->tipo_tras;
            $name_sucxy=$x->name_sucxy;
            $name_sucx=$x->name_sucx;
            if($name_sucxy==$name_sucx){
                $name_sucx="Garantías";
            }
            $html.='<div class="row g-3">
                  <div class="col-md-12">';
                    if($cont_det==1){
                        $html.='<h3><span style="color:#152342">Sucursal solicitante: </span><span style="color:#93ba1f">'.$name_sucxy.'</span></h3>
                            <h3><span style="color:#152342">Sucursal de salida: </span><span style="color:#93ba1f">'.$name_sucx.'</span></h3>';
                    }
                  $html.='</div>
                </div>';
                $html.='<table class="table table-sm table_det_tras" id="table_datos" style="width:100%;">
                <thead>
                    <tr>
                        <th style="color:#152342" scope="col">Producto</th>
                        <th style="color:#152342" scope="col">Cantidad</th>';
                    $html.='</tr>
                </thead>
                <tbody>';
                    if($x->serie!=""){
                        $serie="<br>Serie: ".$x->serie;
                    }else{
                        $serie="";    
                    }
                    if($x->lote!=""){
                        $lote="<br>Lote: ".$x->lote;
                    }else{
                        $lote="";    
                    }
                    if($x->rechazado==1){
                        $style="style='background-color:rgb(255,64,64,0.7)'";
                    }else{
                        $style="";
                    }
                    
                    if($x->tipo==0){
                        $nombre=$x->nombre;
                        if($x->tipo_prod==1){
                            //$nombre = $x->nombre. $serie; // se comento por que la serie que muestra es erronea a la que se selecciono
                            $nombre = $x->nombre;
                        }else if($x->tipo_prod==2){
                            $nombre = $x->nombre. $lote;
                        }
                    }else{
                        $nombre=$x->codigo." - Recarga de oxígeno de ".$x->capacidad."L";
                    } 
                    $html.='<tr '.$style.'>
                        <td style="color:#152342">'.$nombre.'</td>
                        <td style="color:#152342"><input type="hidden" id="cantidad_comp_'.$x->id_ht.'" value="'.$x->cantidad.'">'.$x->cantidad.'</td>';
                    $html.='</tr>';
                    if($x->tipo_prod==1){
                        $html.='<tr>
                            <td style="color:#152342"><br></td>';
                        $html.='</tr>';
                        if($x->rechazado==0){
                            $html.='<tr>
                                <td style="color:#152342"><b>Resumen</b></td>';
                            $html.='</tr>';
                            $result_s=$this->ModelTraspasos->get_traspasos_series_info($id,$x->idproducto);
                            foreach ($result_s as $ps){
                                $html.='<tr>
                                    <td style="color:#152342">Serie: '.$ps->serie.'</td>';
                                $html.='</tr>';
                            } 
                        }else{
                            $html.='<tr '.$style.'>
                                <td colspan="2"><b>Motivo de rechazo:</b> '.$x->motivo_rechazo.'</td>';
                            $html.='</tr>';
                        }
                    }else if($x->tipo_prod==2){
                        $html.='<tr>
                            <td style="color:#152342"><br></td><td style="color:#152342"><br></td>';
                        $html.='</tr>';
                        if($x->rechazado==0){
                            $html.='<tr>
                                <td style="color:#152342"><b>Resumen</b></td><td style="color:#152342"></td>';
                            $html.='</tr>';
                            $result_s=$this->ModelTraspasos->get_traspasos_lotes_info($id,$x->idproducto);
                            foreach ($result_s as $ps){
                                $html.='<tr>
                                    <td style="color:#152342">Lote: '.$ps->lote.'</td><td style="color:#152342">'.$ps->cantidad.'</td>';
                                $html.='</tr>';
                            } 
                        }
                    }
                    /*
                    <!--<td style="color:#152342">
                            <input class="form-control" type="hidden" id="id_prod" value="'.$x->idproducto.'">
                            <input class="form-control" type="hidden" id="id_ht" value="'.$x->id_ht.'">
                            <input class="form-control cantidad_real_'.$x->id_ht.'" onchange="validaCant(this.value,'.$x->id_ht.')" type="number" id="cantidad_real" value="'.$x->cantidad.'">
                        </td>-->
                        <!--<td><button type="button" id="btn_tra_'.$x->id_ht.'" class="btn btn-primary btn-sm btn_tra_'.$x->id_ht.'" onclick="update_entrada('.$id.','.$x->tipo_rec.','.$x->id_ht.',$(this))"><i class="fa fa-check-square" aria-hidden="true"></i></button></td>-->
                    */    
                $html.='</tbody>
                </table>';

                $tipo_rec=$x->tipo_rec;
        }
        $html.='<div class="row g-3">
            <div class="col-md-6">
                <button type="button" class="btn realizarventa btn-sm" style="width: 100%;" onclick="cerrar_entrada('.$id.','.$tipo_rec.','.$tipo_tras.')">Generar entrada</button>
            </div>
            <div class="col-md-6">
                <button class="btn realizarventa btn-sm" style="width: 100%;" type="button" data-bs-dismiss="modal">Regresar</button>         
            </div>
        </div>';
       echo $html;
    }

    public function crearCodigoLote($idsuc,$idprod,$lote,$cad){ //aca me quedo para crear codigo y pasarlo al js y ese al input        
        $codigo=$idprod.$idsuc.intval(preg_replace('/[^0-9]+/', '', $lote), 10).date("Ymd",strtotime($cad));
        return $codigo;        
    }

    function transferir_cantidad_sucursal_transito()
    {
        $id = $this->input->post('id');
        $cantidad = $this->input->post('cantidad');
        $tipo=$this->input->post('tipo');
        $id_ht=$this->input->post('id_ht');
        $result=$this->General_model->get_productos_traspasos2($id_ht);
        $idproducto=0;
        $idsucursal_transferir=0;
        $idsucursal=0;
        foreach ($result as $x){
            $idproducto=$x->idproducto;
            $idsucursal_transferir=$x->idsucursal_entra;
            $idsucursal=$x->idsucursal_sale;
            $tipo=$x->tipo;
            $tipo_prod=$x->tipo_prod;
            $idproducto_series=$x->idproducto_series;
            $serie=$x->serie;
        }

        $this->db->trans_start();
        $datah=array('status'=>2,'idpersonalingreso'=>$this->idpersonal,'fechaingreso'=>$this->fecha_hora_actual,'cantidadingreso'=>$cantidad);
        $this->General_model->edit_record('idtranspasos',$id,$datah,'historial_transpasos');
        if($tipo==0){ // es tipo de traslado, 0=prod, 1=recargas
            $this->ModelTraspasos->sumar_sucursal($idproducto,$idsucursal_transferir,$cantidad);
            $this->ModelTraspasos->resta_sucursal($idproducto,$idsucursal,$cantidad);
            if($tipo_prod==1){ //prod con numero de serie
                //CACHAR U OBTENER EL ID DE PROD SUCURSAL, PARA ESA SERIE CAMBIARLA A LA NVA SUCURSAL ENTRADA Y QUITARLA AL ALM. GENERAL
                $this->General_model->edit_recordw(array("id"=>$idproducto_series),array("sucursalid"=>$idsucursal_transferir),"productos_sucursales_serie");   
            }
        }else{   
            $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal_transferir,$cantidad,"+");
            $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal,$cantidad,"-");
        }
        $this->db->trans_complete();
    }

    function cerrar_traslado(){
        $id = $this->input->post('id');
        //$tipo = $this->input->post('tipo');
        ///////////////////////////////////
        $this->db->trans_start();
        $resultx=$this->General_model->get_productos_traspasos($id);
        foreach ($resultx as $tr){
            $id_ht=$tr->id_ht;
            $result=$this->General_model->get_productos_traspasos2($tr->id_ht);
            $idproducto=0;
            $idsucursal_transferir=0;
            $idsucursal=0;
            $cantidad=0;
            $id_compra=$tr->id_compra;
            foreach ($result as $x){
                $idproducto=$x->idproducto;
                $idsucursal_transferir=$x->idsucursal_entra;
                $idsucursal=$x->idsucursal_sale;
                $tipo=$x->tipo;
                $tipo_prod=$x->tipo_prod;
                $idproducto_series=$x->idproducto_series;
                $id_lote=$x->id_lote;
                $serie=$x->serie;
                $cantidad=$x->cantidad;
            }
            //$this->db->trans_start();
            $datah=array('status'=>2,'idpersonalingreso'=>$this->idpersonal,'fechaingreso'=>$this->fecha_hora_actual,'cantidadingreso'=>$cantidad);
            $this->General_model->edit_record('id',$id_ht,$datah,'historial_transpasos');
            if($tipo==0){ // es tipo de traslado, 0=prod, 1=recargas
                //validar que exista el prod en la sucursal destino
                $getnvasuc=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idproducto,'activo'=>1,'sucursalid'=>$idsucursal_transferir));
                if($getnvasuc->num_rows()>0){
                    foreach ($getnvasuc->result() as $se){ 
                        $cant_inicial=$se->stock;
                        $cant_final =$se->stock+$cantidad;
                        
                        $array_bit=array(
                                        'id_producto'=>$idproducto,
                                        'id_ps'=>0,
                                        'id_sucursal'=>$idsucursal_transferir,
                                        'tipo_prod'=>0,//0 stock 1 serie 2 lote 3 insumis 4 herramientas
                                        'cantidad'=>$cant_inicial,//cantidad inicial, asi se tiene en su tabla
                                        'cantidad_ajuste '=>$cantidad,//cantidad del movimiento
                                        'cant_final'=>$cant_final,
                                        'num_serie'=>'',
                                        'serie_ajuste'=>'',
                                        'id_personal'=>$this->idpersonal,
                                        'fecha_reg'=>$this->fecha_hora_actual
                                        );
                        //$this->ModeloCatalogos->Insert('bitacora_ajustes',$array_bit);

                        $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini_d'=>$cant_inicial),array('id'=>$id_ht));
                        $this->ModelTraspasos->sumar_sucursal($idproducto,$idsucursal_transferir,$cantidad);   

                    }
                }else{
                    $getprod=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idproducto,"sucursalid"=>$idsucursal));
                    $getprod=$getprod->row();
                    $array = array('productoid'=>$idproducto,'sucursalid'=>$idsucursal_transferir,'stock'=>$cantidad,'precio'=>$getprod->precio,'incluye_iva'=>$getprod->incluye_iva,'iva'=>$getprod->iva,"desc_tipo_descuento"=>$getprod->desc_tipo_descuento);
                    $this->General_model->add_record('productos_sucursales',$array);
                }
                $get_info_suc_o=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$idproducto,'activo'=>1,'sucursalid'=>$idsucursal));
                if($tipo_prod==0){// solo para tipo stock
                    $cant_ini_o=0;
                    foreach ($get_info_suc_o->result() as $item_i_o) {
                        $cant_ini_o=$item_i_o->stock;
                    }
                    $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o),array('id'=>$id_ht));
                }
                $this->ModelTraspasos->resta_sucursal($idproducto,$idsucursal,$cantidad);
                if($tipo_prod==1){ //prod con numero de serie
                    //CACHAR U OBTENER EL ID DE PROD SUCURSAL, PARA ESA SERIE CAMBIARLA A LA NVA SUCURSAL ENTRADA Y QUITARLA AL ALM. GENERAL
                    if($id_compra==0){
                        //$result_serie=$this->ModeloCatalogos->getselectwheren('traspasos_series_historial',array('activo'=>1,'idtraspasos'=>$id));
                        $strq="SELECT tr_ser.*, ser.serie, ser.productoid
                            FROM traspasos_series_historial as tr_ser
                            INNER JOIN productos_sucursales_serie as ser on ser.id=tr_ser.idseries
                            WHERE tr_ser.idtraspasos='$id' AND tr_ser.idseries='$idproducto_series' and tr_ser.activo=1";
                        $result_serie = $this->db->query($strq);
                        foreach ($result_serie->result() as $se){  
                            //$cant_ini_o=$this->ModeloCatalogos->obtener_stock_series($se->productoid,$idsucursal);
                            //$cant_ini_d=$this->ModeloCatalogos->obtener_stock_series($se->productoid,$idsucursal_transferir);
                            $cant_ini_o=1;
                            $cant_ini_d=0;

                            $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));

                            $this->General_model->edit_recordw(array("id"=>$se->idseries),array("sucursalid"=>$idsucursal_transferir,"disponible"=>"1"),"productos_sucursales_serie");   
                        }
                    }else{
                        $this->General_model->edit_recordw(array("id"=>$idproducto_series),array("sucursalid"=>$idsucursal_transferir,"disponible"=>"1"),"productos_sucursales_serie");   
                    }
                }if($tipo_prod==2){ //prod con lote
                    //PARA CAMBIARLA A LA NVA SUCURSAL ENTRADA Y QUITARLA AL ALM. GENERAL
                    if($id_compra==0){
                        $result_lote=$this->ModeloCatalogos->getselectwheren('traspasos_lotes_historial',array('activo'=>1,'idtraspasos'=>$id,'id_historialt'=>$id_ht));
                        foreach ($result_lote->result() as $lot){ 
                            $idlote_v=$lot->idlotes;
                            $cantidad_v=$lot->cantidad;
                            $result_lote_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('id'=>$lot->idlotes));
                            $idlote_reg=0;
                            $productoidl=0;
                            $sucursalidl=0;
                            $cantidadl=0;
                            $lotel='';
                            $caducidadl='';
                            $idcompral=0;
                            foreach ($result_lote_suc->result() as $lot_s){
                                $productoidl=$lot_s->productoid;
                                $sucursalidl=$lot_s->sucursalid;
                                $lotel=$lot_s->lote;
                                $caducidadl=$lot_s->caducidad;
                                $idcompral=$lot_s->idcompra;
                                $result_lote_suc_entra=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('productoid'=>$lot_s->productoid,'sucursalid'=>$idsucursal_transferir,'lote'=>$lot_s->lote,"caducidad"=>$caducidadl));
                                foreach ($result_lote_suc_entra->result() as $lot_s2){
                                    $idlote_reg=$lot_s2->id;
                                }
                            }
                            //var_dump($idlote_reg);die;
                            /// validar que exista a la nueva sucursal
                            if($idlote_reg==0){
                                $cant_ini_o=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal);
                                $cant_ini_d=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal_transferir);

                                $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));

                                $this->ModelTraspasos->resta_lote($idlote_v,$cantidad_v);
                                $arraylote = array('productoid'=>$productoidl,'sucursalid'=>$idsucursal_transferir,'cantidad'=>$cantidad_v,'lote'=>$lotel,'caducidad'=>$caducidadl,'idcompra'=>$idcompral,"cod_barras"=>$this->crearCodigoLote($idsucursal_transferir,$productoidl,$lotel,$caducidadl));
                                $this->General_model->add_record('productos_sucursales_lote',$arraylote);

                                //$this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));
                            }else{
                                $cant_ini_o=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal);
                                $cant_ini_d=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal_transferir);

                                if($idsucursal_transferir!=$idsucursal){
                                    $this->ModelTraspasos->resta_lote($idlote_v,$cantidad_v);
                                }
                                $this->ModelTraspasos->sumar_lote($idlote_reg,$cantidad_v);
                                
                                $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));
                            }
                        }
                    }else{
                        $this->General_model->edit_recordw(array("id"=>$id_lote),array("sucursalid"=>$idsucursal_transferir),"productos_sucursales_lote");
                    }
                }
            }else{   
                $getrec=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>$idsucursal_transferir,'tipo'=>0,'estatus'=>1));
                if($getrec->num_rows()>0){
                    $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal_transferir,$cantidad,"+"); 
                }else{
                   $this->ModeloCatalogos->Insert('recargas',array("codigo"=>"TO9500","capacidad"=>9500,"preciov"=>0,"precioc"=>0,"tipo"=>0,"stock"=>floatval($cantidad),"fecha_reg"=>date("Y-m-d H:i:s"),"sucursal"=>$idsucursal_transferir));     
                }
                $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal,$cantidad,"-");
            }
        }
        /////////////////////////////////// 

        $data = array('status'=>2);
        $this->General_model->edit_record('id',$id,$data,'traspasos');
        $this->db->trans_complete();
    }

    function cerrar_trasladoSolicitud(){
        $id = $this->input->post('id');
        //$tipo = $this->input->post('tipo');
        ///////////////////////////////////
        $resultx=$this->General_model->get_productos_traspasos($id,0,0,1,1);
        foreach ($resultx as $tr){
            $id_ht=$tr->id_ht;
            $result=$this->General_model->get_productos_traspasos2($tr->id_ht);
            $idproducto=0;
            $idsucursal_transferir=0;
            $idsucursal=0;
            $cantidad=0;
            $id_compra=$tr->id_compra;
            $cant_inicial=$tr->stock;
            foreach ($result as $x){
                $idproducto=$x->idproducto;
                $idsucursal_transferir=$x->idsucursal_entra;
                $idsucursal=$x->idsucursal_sale;
                $tipo=$x->tipo;
                $tipo_prod=$x->tipo_prod;
                $idproducto_series=$x->idproducto_series;
                $id_lote=$x->id_lote;
                $serie=$x->serie;
                $cantidad=$x->cantidad;
                if($x->tipo_prod==2){ //lote
                    $cantidad=$x->cantidad_tlh;
                }
                //log_message('error','tipo_prod : '.$tipo_prod);
                //log_message('error','cantidad : '.$cantidad);
            }
            //$this->db->trans_start();
            $datah=array('status'=>2,'idpersonalingreso'=>$this->idpersonal,'fechaingreso'=>$this->fecha_hora_actual,'cantidadingreso'=>$cantidad);
            $this->General_model->edit_record('id',$id_ht,$datah,'historial_transpasos');
            if($tipo==0){ // es tipo de traslado, 0=prod, 1=recargas
                
                if($tipo_prod==0){
                    $cant_ini_o=$this->ModeloCatalogos->obtener_stock_normal($idproducto,$idsucursal);
                    $cant_ini_d=$this->ModeloCatalogos->obtener_stock_normal($idproducto,$idsucursal_transferir);

                    $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));
                    $this->ModelTraspasos->sumar_sucursal($idproducto,$idsucursal_transferir,$cantidad);
                    $this->ModelTraspasos->resta_sucursal($idproducto,$idsucursal,$cantidad);
                }
                if($tipo_prod==1){ //prod con numero de serie
                    //CACHAR U OBTENER EL ID DE PROD SUCURSAL, PARA ESA SERIE CAMBIARLA A LA NVA SUCURSAL ENTRADA Y QUITARLA AL ALM. GENERAL
                    if($id_compra==0){
                        //$result_serie=$this->ModeloCatalogos->getselectwheren('traspasos_series_historial',array('activo'=>1,'idtraspasos'=>$id));
                        $strq="SELECT tr_ser.*, ser.serie, ser.productoid
                            FROM traspasos_series_historial as tr_ser
                            INNER JOIN productos_sucursales_serie as ser on ser.id=tr_ser.idseries
                            WHERE tr_ser.idtraspasos='$id' AND tr_ser.idseries='$idproducto_series' and tr_ser.activo=1";
                        $result_serie = $this->db->query($strq);
                        foreach ($result_serie->result() as $se){  
                            //$cant_ini_o=$this->ModeloCatalogos->obtener_stock_series($se->productoid,$idsucursal);
                            //$cant_ini_d=$this->ModeloCatalogos->obtener_stock_series($se->productoid,$idsucursal_transferir);
                            $cant_ini_o=1;
                            $cant_ini_d=0;

                            $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));

                            $this->General_model->edit_recordw(array("id"=>$se->idseries),array("sucursalid"=>$idsucursal_transferir),"productos_sucursales_serie");   
                        }
                    }else{
                        $this->General_model->edit_recordw(array("id"=>$idproducto_series),array("sucursalid"=>$idsucursal_transferir),"productos_sucursales_serie");   
                    }
                }if($tipo_prod==2){ //prod con lote
                    //PARA CAMBIARLA A LA NVA SUCURSAL ENTRADA Y QUITARLA AL ALM. GENERAL
                    if($id_compra==0){
                        $result_lote=$this->ModeloCatalogos->getselectwheren('traspasos_lotes_historial',array('activo'=>1,'idtraspasos'=>$id,"id_historialt"=>$tr->id_ht));
                        foreach ($result_lote->result() as $lot){ 
                            $idlote_v=$lot->idlotes;
                            $cantidad_v=$lot->cantidad;
                            $result_lote_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('id'=>$lot->idlotes));
                            $idlote_reg=0;
                            $productoidl=0;
                            $sucursalidl=0;
                            $cantidadl=0;
                            $lotel='';
                            $caducidadl='';
                            $idcompral=0;
                            foreach ($result_lote_suc->result() as $lot_s){
                                $productoidl=$lot_s->productoid;
                                $sucursalidl=$lot_s->sucursalid;
                                $lotel=$lot_s->lote;
                                $caducidadl=$lot_s->caducidad;
                                $idcompral=$lot_s->idcompra;
                                $result_lote_suc_entra=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('productoid'=>$lot_s->productoid,'sucursalid'=>$idsucursal_transferir,'lote'=>$lot_s->lote));
                                foreach ($result_lote_suc_entra->result() as $lot_s2){
                                    $idlote_reg=$lot_s2->id;
                                }
                            }
                            //var_dump($idlote_reg);die;
                            /// validar que exista a la nueva sucursal
                            if($idlote_reg==0){
                                //log_message('error','update stock 0');
                                $cant_ini_o=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal);
                                $cant_ini_d=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal_transferir);

                                $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));

                                $this->ModelTraspasos->resta_lote($idlote_v,$cantidad_v);
                                $arraylote = array('productoid'=>$productoidl,'sucursalid'=>$idsucursal_transferir,'cantidad'=>$cantidad_v,'lote'=>$lotel,'caducidad'=>$caducidadl,'idcompra'=>$idcompral,"cod_barras"=>$this->crearCodigoLote($idsucursal_transferir,$productoidl,$lotel,$caducidadl));
                                $this->General_model->add_record('productos_sucursales_lote',$arraylote);
                            }else{
                                //log_message('error','update stock 1');
                                $cant_ini_o=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal);
                                $cant_ini_d=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal_transferir);

                                $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));

                                $this->ModelTraspasos->resta_lote($idlote_v,$cantidad_v);
                                $this->ModelTraspasos->sumar_lote($idlote_reg,$cantidad_v);
                            }
                        }
                    }else{
                        log_message('error','update stock 2');
                        $cant_ini_o=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal);
                        $cant_ini_d=$this->ModeloCatalogos->obtener_stock_lotes($productoidl,$idsucursal_transferir);
                        
                        $this->General_model->edit_recordw(array("id"=>$id_lote),array("sucursalid"=>$idsucursal_transferir),"productos_sucursales_lote");

                        $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array('cant_ini'=>$cant_ini_o,'cant_ini_d'=>$cant_ini_d),array('id'=>$id_ht));
                    }
                }
            }else{   
                $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal_transferir,$cantidad,"+");
                $this->ModelTraspasos->update_tanque_sucu($idproducto,$idsucursal,$cantidad,"-");
            }
        }
        /////////////////////////////////// 
        $this->db->trans_start();
        $data = array('status'=>2);
        $this->General_model->edit_record('id',$id,$data,'traspasos');
        $this->db->trans_complete();
    }

    public function imprimir_traspaso($id){
        $data['result_tra']=$this->General_model->get_productos_traspasos($id,1,1,1);
        $data['status']="";
        $data['reg']="";
        $data['fechaingreso']="";
        $data['idtras']=$id;
        foreach($data['result_tra'] as $t){
            $data['status']=$t->status;
            $data['reg']=$t->reg;
            $data['fechaingreso']=$t->fechaingreso;
        }
        $this->load->view('reportes/traspaso',$data);
    }
    
    public function registro_series_traspasos(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idtraspasos']=$DATA[$i]->idtraspasos;        
            $data['idseries']=$DATA[$i]->idseries;
            $this->General_model->add_record('traspasos_series_historial',$data);
        }
    }
    
    public function registro_series_traspasos_lotes(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idtraspasos']=$DATA[$i]->idtraspasos;        
            $data['idlotes']=$DATA[$i]->idlotes;
            $data['cantidad']=$DATA[$i]->cantidad;
            $this->General_model->add_record('traspasos_lotes_historial',$data);
        }
    }

    function get_series_producto(){
        $id = $this->input->post('idproducto');
        $id_suc = $this->input->post('id_suc');
        $html="";
        $results=$this->ModelTraspasos->getSeries_prod($id,$id_suc);
        foreach($results as $s){
            $html.='<tr class="tr_series">
                    <td>'.$s->nombre.'</td>
                    <td>'.$s->serie.'</td>
                    <td>1</td>
                    <td><div class="col-lg-1">
                        <input type="hidden" id="sserie" value="'.$s->serie.'">
                        <input type="hidden" id="idserie" value="'.$s->id.'">
                        <span class="switch switch-icon">
                            <label>
                                <input type="radio" onclick="asignaSerie('.$s->id.',\''.$s->serie.'\','.$id_suc.')" name="add_serie" id="add_serie add_serie_'.$s->id.'" class="switch_ser" value="'.$s->id.'">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div></td>
                </tr>';
        }
        echo $html;
    }

    function get_lotes_producto(){
        $id = $this->input->post('idproducto');
        $id_suc = $this->input->post('id_suc');
        $cant = $this->input->post('cant');
        $html="";
        $results=$this->ModelTraspasos->getLotes_prod($id,$id_suc);
        foreach($results as $s){
            if($cant>$s->cantidad){
                $dis="disabled";
            }else{
                $dis="";
            }
            $html.='<tr class="tr_lotes">
                    <td>'.$s->nombre.'</td>
                    <td>'.$s->lote.' Cad: '.$s->caducidad.'</td>
                    <td>'.$s->cantidad.'</td>
                    <td><div class="col-lg-1">
                        <input type="hidden" id="llote" value="'.$s->lote.'">
                        <input type="hidden" id="idlote" value="'.$s->id.'">
                        <span class="switch switch-icon">
                            <label>
                                <input '.$dis.' type="radio" onchange="asignaLote('.$s->id.',\''.$s->lote.'\',\''.$s->caducidad.'\','.$id_suc.','.$s->cantidad.')" name="add_serie" id="add_lote add_lote_'.$s->id.'" class="switch_lot" value="'.$s->id.'">
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </div></td>
                </tr>';
        }
        echo $html;
    }

    function get_productos_series(){
        $idproducto = $this->input->post('idproducto');
        $cont = $this->input->post('cont');
        $html='<select class="form-control" id="series_s_'.$cont.'">';
        $html.='<option value="0" selected>Selecciona una opción</option>';
        $result_serie=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('activo'=>1,'productoid'=>$idproducto,'sucursalid'=>8,"disponible"=>1));
        foreach ($result_serie->result() as $se){
            $html.='<option value="'.$se->id.'">'.$se->serie.'</option>';
        }              
        $html.='</select>';
        echo $html;
    }

    function get_productos_lote(){
        $idproducto = $this->input->post('idproducto');
        $cont = $this->input->post('cont');
        $html='<select class="form-control" id="lote_l_'.$cont.'">';
            $html.='<option value="0" selected>Selecciona una opción</option>';
            $result_lote=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$idproducto,'sucursalid'=>8));
            foreach ($result_lote->result() as $lo){
                $html.='<option value="'.$lo->id.'" data-cantidad="'.$lo->cantidad.'" data-lotetxt="'.$lo->lote.'">Lote: '.$lo->lote.' / Cantidad:'.$lo->cantidad.'</option>';
            }              
        $html.='</select>';
        echo $html;
    }

    function registro_solicitud_traspaso(){
        /// add traspasos
        $datatra['idpersonal']=$this->idpersonal;
        $datatra['status']=0;
        $idtranspaso=$this->General_model->add_record('traspasos',$datatra);
        echo $idtranspaso;
        //disparar mail de notificacion
        //$this->mail_solicitud($idtranspaso);
    }

    function reg_soli_tras_almacen(){
        /// add traspasos
        $datatra = $this->input->post();
        $datatra['idpersonal']=$this->idpersonal;
        $datatra['status']=1;
        $datatra['tipo']=1;
        $idtranspaso=$this->General_model->add_record('traspasos',$datatra);
        echo $idtranspaso;
        //disparar mail de notificacion
        //$this->mail_solicitud($idtranspaso);
    }

    function editar_cant_solicitud(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++){
            $cantidad=$DATA[$i]->cantidad; 
            $this->General_model->edit_record('id',$DATA[$i]->id_ht,array("cantidad"=>$cantidad),'historial_transpasos');
        }
    }

    function guardar_soliciutd_recarga(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            if(isset($DATA[$i]->id_origen)){
                $data['idsucursal_sale']=$DATA[$i]->id_origen;
            }else{
                $data['idsucursal_sale']=8;
            }
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=1;
            $this->General_model->add_record('historial_transpasos',$data);
        }
    }

    function registro_solicitud_traspaso_producto(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['id_compra']=$DATA[$i]->id_ordenc;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=0;
            $this->General_model->add_record('historial_transpasos',$data);
            if($DATA[$i]->id_ordenc!=0){
                $id=$this->General_model->edit_recordw(array("idproducto"=>$DATA[$i]->idproductos,"idcompra"=>$DATA[$i]->id_ordenc),array("distribusion"=>1),'compra_erp_detalle');
            }
        }
    }

    function registro_solicitud_traspaso_producto_series(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=0;
            $idtranspasos=$DATA[$i]->idtraspasos;
            $this->General_model->add_record('historial_transpasos',$data);
            /*$DATAcd=$DATA[$i]->serie_detalles;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data_d['idtraspasos']=$idtranspasos;        
                $data_d['idseries']=$DATAcd[$j]->idseries;
                $this->General_model->add_record('traspasos_series_historial',$data_d);
            }*/
        }
    }

    function registro_solicitud_traspaso_producto_series_alm(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['id_compra']=$DATA[$i]->id_ordenc;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=0;
            $idtranspasos=$DATA[$i]->idtraspasos;
            $this->General_model->add_record('historial_transpasos',$data);
            if($DATA[$i]->id_ordenc!=0){
                $id=$this->General_model->edit_recordw(array("idproducto"=>$DATA[$i]->idproductos,"idcompra"=>$DATA[$i]->id_ordenc),array("distribusion"=>1),'compra_erp_detalle');
            }
            $DATAcd=$DATA[$i]->serie_detalles;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data_d['idtraspasos']=$idtranspasos;        
                $data_d['idseries']=$DATAcd[$j]->idseries;
                $this->General_model->add_record('traspasos_series_historial',$data_d);
            }
        }
    }

    function registro_solicitud_traspaso_producto_lote(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=0;
            $idtranspasos=$DATA[$i]->idtraspasos;
            $this->General_model->add_record('historial_transpasos',$data);
            /*$DATAcd=$DATA[$i]->lote_detalles;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data_d['idtraspasos']=$idtranspasos;        
                $data_d['idlotes']=$DATAcd[$j]->idlote;
                $data_d['cantidad']=$DATAcd[$j]->cantidad;
                $this->General_model->add_record('traspasos_lotes_historial',$data_d);
            }*/
        }
    }

    function registro_solicitud_traspaso_producto_lote_alm(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['id_compra']=$DATA[$i]->id_ordenc;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $data['tipo']=0;
            $idtranspasos=$DATA[$i]->idtraspasos;
            $this->General_model->add_record('historial_transpasos',$data);
            if($DATA[$i]->id_ordenc!=0){
                $id=$this->General_model->edit_recordw(array("idproducto"=>$DATA[$i]->idproductos,"idcompra"=>$DATA[$i]->id_ordenc),array("distribusion"=>1),'compra_erp_detalle');
            }
            $DATAcd=$DATA[$i]->lote_detalles;
            for ($j=0;$j<count($DATAcd);$j++) {
                $data_d['idtraspasos']=$idtranspasos;        
                $data_d['idlotes']=$DATAcd[$j]->idproducto_lote;
                $data_d['cantidad']=$DATAcd[$j]->cantidad;
                $this->General_model->add_record('traspasos_lotes_historial',$data_d);
            }
        }
    }

    function registro_solicitud_traspaso_all_producto_alm(){ //guarda traslado solicitud manual almacen
        //log_message('error','registro_solicitud_traspaso_all_producto_alm');

        $this->db->trans_start();
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $cant_ini=0;
            $data['idsucursal_sale']=$DATA[$i]->id_origen;
            $data['idsucursal_entra']=$DATA[$i]->sucursal; 
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['id_compra']=$DATA[$i]->id_ordenc;
            $data['idproducto']=$DATA[$i]->idproductos;
            $data['idpersonal']=$this->idpersonal;
            $data['idtranspasos']=$DATA[$i]->idtraspasos;
            $cant_ini=$DATA[$i]->cant_ini;
            
            $data['status']=1;
            //hay que validar que la sucursalid sea la del formulario elegida (está 8 por default)
            if($DATA[$i]->tipo==3){
                $data['tipo']=1; //recarga
                //$getoxi=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>"0","sucursal"=>8,"estatus"=>1));
                //$geto=$getoxi->row();
                //$cant_ini=$geto->stock;
                $getoxi=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>"0","sucursal"=>$DATA[$i]->sucursal,"estatus"=>1));
                if($getoxi->num_rows()>0){
                    $geto=$getoxi->row();
                    $cant_ini_dest=$geto->stock;
                }else{
                    $cant_ini_dest=0;
                }
            }if($DATA[$i]->tipo!=3){
                $data['tipo']=0; //productos
                
                //aca me quedo -- OJOOOOO
                $getpro=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$DATA[$i]->idproductos));
                $getp=$getpro->row();
                if($getp->tipo!="1" && $getp->tipo!="2"){ //de stock, insumo y refaccion
                    $getpro_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATA[$i]->idproductos,"sucursalid"=>$DATA[$i]->sucursal,"activo"=>1));
                    if($getpro_suc->num_rows()>0){
                        $getpsuc=$getpro_suc->row();
                        $cant_ini_dest=$getpsuc->stock;
                    }else{
                        $cant_ini_dest=0;
                    }
                }//if($getp->tipo=="1" || $getp->tipo=="2"){ //de serie o lote
                    //$cant_ini=0; //obtener la cantidad que tenga ese lote en sucursal origen - mantener el 0
                    if($DATA[$i]->tipo==1){
                        $DATAcd=$DATA[$i]->serie_detalles;
                        /*for ($j=0;$j<count($DATAcd);$j++) {
                            $getpro_ser=$this->ModelTraspasos->getSumSerie($DATA[$i]->idproductos,$DATA[$i]->sucursal);  
                            $cant_ini_dest=$getpro_ser->tot_series;
                        }*/
                        $cant_ini=1;
                        $cant_ini_dest=0;
                    }
                    if($DATA[$i]->tipo==2){
                        $DATAdl=$DATA[$i]->lote_detalles;
                        for ($j=0;$j<count($DATAdl);$j++) {      
                            $getpro_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('id'=>$DATAdl[$j]->idproducto_lote,"sucursalid"=>$DATA[$i]->sucursal));
                            if($getpro_suc->num_rows()>0){
                                $getpsuc=$getpro_suc->row();
                                $getpro_lot=$this->ModelTraspasos->getSumLotes(array("lote"=>$getpsuc->lote,"caducidad"=>$getpsuc->caducidad,"activo"=>1,"sucursalid"=>$DATA[$i]->sucursal));
                                $cant_ini_dest=$getpro_lot->tot_lotes;
                            }else{
                                $cant_ini_dest=0;
                            }
                        }
                    }
                //}
            }
            $data['cant_ini'] = $cant_ini;
            $data['cant_ini_d'] = $cant_ini_dest;
            //$data['cant_final']=$cant_ini-$DATA[$i]->cantidad;
            $idtranspasos=$DATA[$i]->idtraspasos;
            $id_historialt=$this->General_model->add_record('historial_transpasos',$data);
            /*if($DATA[$i]->id_ordenc!=0){
                $id=$this->General_model->edit_recordw(array("idproducto"=>$DATA[$i]->idproductos,"idcompra"=>$DATA[$i]->id_ordenc),array("distribusion"=>1),'compra_erp_detalle');
            }*/
            if($DATA[$i]->tipo==1){
                $DATAcd=$DATA[$i]->serie_detalles;
                for ($j=0;$j<count($DATAcd);$j++) {
                    $data_d['idtraspasos']=$idtranspasos;        
                    $data_d['idseries']=$DATAcd[$j]->idseries;
                    $this->General_model->add_record('traspasos_series_historial',$data_d);
                    $this->General_model->edit_recordw(array("id"=>$id_historialt),array("idproducto_series"=>$DATAcd[$j]->idseries),'historial_transpasos');
                }
            }
            if($DATA[$i]->tipo==2){
                $DATAdl=$DATA[$i]->lote_detalles;
                for ($j=0;$j<count($DATAdl);$j++) {
                    $data_dl['idtraspasos']=$idtranspasos; 
                    $data_dl['id_historialt']=$id_historialt;       
                    $data_dl['idlotes']=$DATAdl[$j]->idproducto_lote;
                    $data_dl['cantidad']=$DATAdl[$j]->cantidad;
                    $this->General_model->add_record('traspasos_lotes_historial',$data_dl);
                    $this->General_model->edit_recordw(array("id"=>$id_historialt),array("idproducto_lote"=>$DATAdl[$j]->idproducto_lote),'historial_transpasos');
                }
            }
        }
        $this->db->trans_complete();
    }

    function save_asigna_prods_solicitud(){
        $datos = $this->input->post('data');
        $idtraspaso=0; $id_ant=0;
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $idtraspaso=$DATA[$i]->idtraspaso;
            $id=$DATA[$i]->id;
            //log_message('error','id : '.$id);
            //log_message('error','id_ant : '.$id_ant);
            if($id==0 || $id_ant==$id){
                $data['idsucursal_sale']=$DATA[$i]->idsucursal_sale;
                $data['idsucursal_entra']=$DATA[$i]->idsucursal_entra; 
                $data['cantidad']=$DATA[$i]->cantidad;
                $data['reg']=$this->fecha_hora_actual;
                $data['idpersonal']=$this->idpersonal;
                $data['id_compra']=0;
                $data['idproducto']=$DATA[$i]->idproducto;
                $data['tipo']=$DATA[$i]->tipo;
                $data['idtranspasos']=$DATA[$i]->idtraspaso;
                $data['status']=1;
                $data['idproducto_series']=$DATA[$i]->idproducto_series;
                $data['cant_ini_d']=$DATA[$i]->cant_ini;
                $id_ht=$this->General_model->add_record('historial_transpasos',$data);
                if($DATA[$i]->tipo_prod==1 && $DATA[$i]->rechazado=="0"){
                    $data_ds['idtraspasos']=$idtraspaso;        
                    $data_ds['idseries']=$DATA[$i]->idproducto_series;
                    $this->General_model->add_record('traspasos_series_historial',$data_ds);
                }
                if($DATA[$i]->tipo_prod==2){
                    $data_dl['idtraspasos']=$idtraspaso;
                    $data_dl['id_historialt']=$id_ht;             
                    $data_dl['idlotes']=$DATA[$i]->id_lote;
                    $data_dl['cantidad']=$DATA[$i]->cantidad;
                    $this->General_model->add_record('traspasos_lotes_historial',$data_dl);
                }
            }
            if($id!=0 && $id_ant!=$id){
                if($DATA[$i]->tipo_prod==0 && $DATA[$i]->rechazado=="0"){
                    $cant_ini_dest=$DATA[$i]->cant_ini;
                }
                if($DATA[$i]->tipo_prod==1 && $DATA[$i]->rechazado=="0"){
                    $data_ds['idtraspasos']=$idtraspaso;        
                    $data_ds['idseries']=$DATA[$i]->idproducto_series;
                    $this->General_model->add_record('traspasos_series_historial',$data_ds);
                    //$getpro_ser=$this->ModelTraspasos->getSumSerie($DATA[$i]->idproducto);  
                    //$cant_ini_dest=$getpro_ser->tot_series;
                    $cant_ini_dest=0;
                }
                if($DATA[$i]->tipo_prod==2 && $DATA[$i]->rechazado=="0"){
                    $data_dl['idtraspasos']=$idtraspaso;  
                    $data_dl['id_historialt']=$DATA[$i]->id;        
                    $data_dl['idlotes']=$DATA[$i]->id_lote;
                    $data_dl['cantidad']=$DATA[$i]->cantidad;
                    $this->General_model->add_record('traspasos_lotes_historial',$data_dl);
                    $getpro_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('id'=>$DATA[$i]->id_lote));
                    if($getpro_suc->num_rows()>0){
                        $getpsuc=$getpro_suc->row();
                        $getpro_lot=$this->ModelTraspasos->getSumLotes(array("lote"=>$getpsuc->lote,"caducidad"=>$getpsuc->caducidad,"activo"=>1,"sucursalid"=>$DATA[$i]->idsucursal_entra));
                        $cant_ini_dest=$getpro_lot->tot_lotes;
                    }else{
                        $cant_ini_dest=0;
                    }
                }
                $this->General_model->edit_recordw(array("id"=>$DATA[$i]->id),array("status"=>1,"cantidad"=>$DATA[$i]->cantidad,'idproducto_series'=>$DATA[$i]->idproducto_series,'cant_ini_d'=>$cant_ini_dest),'historial_transpasos');
            }
            $id_ant=$DATA[$i]->id;
        }
        $this->General_model->edit_recordw(array("id"=>$idtraspaso),array("status"=>1),'traspasos');
    }

    function searchBarcode(){
        $codigo = $this->input->post('cod');
        $idsuc = $this->input->post('idsuc');
        $results=$this->ModelTraspasos->getProductoCodigo($codigo,$idsuc);
        echo json_encode($results);
    }

    function searchPorOC(){
        $id = $this->input->post('id');
        $results=$this->ModelTraspasos->getProductosOC($id);
        echo json_encode($results);
    }

    function mail_solicitud(){ 
        $id = $this->input->post('id');
        $getpers=$this->ModelTraspasos->getPersonalTraslado($id);
        $array_ent = array();
        $array_sal = array();
        $array_alm = array();
        $suc_solicita=""; $cont_sale=0; $cont_entra=0; $correo_alm=0;
        foreach ($getpers as $p) {
            $suc_solicita=$p->suc_solicita;
            if ($p->correo_entra!='') {
                array_push($array_ent,$p->correo_entra);
                $cont_entra++;  
            }if ($p->correo_sale!='') {
                array_push($array_sal,$p->correo_sale);
                $cont_sale++;
            }if ($p->correo_alm!='') {
                array_push($array_alm,$p->correo_alm);
                $correo_alm++;
            }
        }
        /*log_message('error','array_ent : '.json_encode($array_ent));
        log_message('error','array_sal : '.json_encode($array_sal));*/
        $array_ent=array_unique($array_ent);
        $array_sal=array_unique($array_sal);
        $array_alm=array_unique($array_alm);

        log_message('error','array_ent unique: '.json_encode($array_ent));
        log_message('error','array_sal unique: '.json_encode($array_sal));
        log_message('error','array_alm unique: '.json_encode($array_alm));

        $asunto='Solicitud de traspaso';
        $this->load->library('email');
        //$this->load->library('email', NULL, 'ci_email');

        $config['protocol'] = 'smtp';
        $config["smtp_host"] ='mail.semit.mx'; 
        $config["smtp_user"] = 'noreply@semit.mx';
        $config["smtp_pass"] = 'l)sT^Iwk,e7n';
        $config["smtp_port"] = '465';
        $config["smtp_crypto"] = 'ssl';
        $config['charset'] = 'utf-8'; 
        $config['wordwrap'] = TRUE; 
        $config['validate'] = true;
        $config['mailtype'] = 'html';
         
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@semit.mx','Semit');

        //$this->email->to($correo,'Solicitud de traspaso');
        if($cont_entra>0){
            $this->email->to($array_ent);
        }
        if($cont_sale>0){
            $this->email->cc($array_sal);
        }
        if($correo_alm>0){
            $this->email->bcc($array_alm);
        }
        $this->email->subject($asunto);
        
        $message ="Solicitud de traslado: ".$suc_solicita ." registra solicitud con No. ".$id;
        $this->email->message($message);
        $this->email->send();
        /*if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }*/
    }
    function validarsucusalesccp(){
        $params = $this->input->post();
        $origen = $params['origen'];
        $destino = $params['destino'];

        $strq="SELECT * FROM(
                  SELECT 
                    plao.id_pcp,
                    plao.id_ubicacion as ubio,
                    plad.id_ubicacion as ubid 
                  FROM plantilla_carta_porte_ubicaciones as plao 
                  INNER JOIN plantilla_carta_porte_ubicaciones as plad on plad.id_pcp=plao.id_pcp AND plad.activo=1 AND plad.TipoUbicacion='Destino' 
                  WHERE plao.activo=1 AND plao.TipoUbicacion='Origen' GROUP BY plao.id_pcp 
                ) as datos WHERE ubio='$origen' AND ubid='$destino'";
        $query = $this->db->query($strq);
        echo json_encode($query->result());
    }
    function consultar_unidades(){
        $datos = $this->input->post('datos');
        $DATA = json_decode($datos);
        $unidadesarray=array();
        for ($i=0;$i<count($DATA);$i++) {
            $uni=$DATA[$i]->uni;
            $result=$this->General_model->get_select('f_unidades',array('Clave'=>$uni));
            $nombre='';
            foreach ($result->result() as $item) {
                $nombre = $item->nombre;
                $unidadesarray[]=array(
                    'uni'=>$uni,
                    'nom'=>$item->nombre
                );
            }
            
        }
        echo json_encode($unidadesarray);
    }
}