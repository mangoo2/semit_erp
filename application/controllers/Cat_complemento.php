<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cat_complemento extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloOperadores');
        $this->load->model('ModeloDomicilio_origen');
        $this->load->model('ModeloDomicilio_destino');

        $this->load->model('ModeloOrigen');
        $this->load->model('ModeloDestino');

        $this->load->model('ModeloTransporte_federal');
        $this->load->model('ModeloPcataporte');
        $this->load->model('ModeloDevhtml');
        

        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoy_normal = date('d/m/Y');
        $this->horahoy_normal = date('H:i:s');
        $this->fecha_larga = date('Y-m-d H:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechacod = date('ymdHis');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,42);// perfil y id del submenu
            //$this->sucusal=1;//cuando ya esten las sucursales
            //$this->folio=$this->sucusal.'-'.$this->fechacod;
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('/Sistema');
        }
    }

   

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=42;
        
        $data['result_estados']=$this->ModeloCatalogos->getselectwheren('f_c_estado',array('activo'=>1,'c_Pais'=>'MEX'));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/form');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/formjs');
    }
    public function get_listado_op() {
        $params = $this->input->post();
        $getdata = $this->ModeloOperadores->get_listado($params);
        $totaldata= $this->ModeloOperadores->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function save_op(){
        $params = $this->input->post();
        $id=$params['id'];
        unset($params['id']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('operadores',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('operadores',$params);
        }
    }
    function save_dir(){
        $params = $this->input->post();
        log_message('error','direccion '.json_encode($params));
        $id=$params['dir_id'];
        $tipo=intval($params['dir_tipo']);
        //unset($params['dir_id']);
        if(isset($params['dir_colonia'])){
            $dir_colonia=$params['dir_colonia'];
        }else{
            $dir_colonia='';
        }
        if(isset($params['dir_localidad'])){
            $dir_localidad=$params['dir_localidad'];
        }else{
            $dir_localidad='';
        }
        if(isset($params['dir_municipio'])){
            $dir_municipio=$params['dir_municipio'];
        }else{
            $dir_municipio='';
        }
        $array = array(
            'calle'=>$params['dir_calle'],
            'num_ext'=>$params['dir_num_ext'],
            'num_int'=>$params['dir_num_int'],
            'codigo_postal'=>$params['dir_codigo_postal'],
            'pais'=>$params['dir_pais'],
            'estado'=>$params['dir_estado'],
            'colonia'=>$dir_colonia,
            'localidad'=>$dir_localidad,
            'municipio'=>$dir_municipio,
            'referencia'=>$params['dir_referencia']
        );
        if ($id>0) {
            if($tipo==0){
                $this->ModeloCatalogos->updateCatalogo('domicilio_origen',$array,array('id'=>$id));
            }else{
                $this->ModeloCatalogos->updateCatalogo('domicilio_destino',$array,array('id'=>$id));
            }
            
        }else{
            if($tipo==0){
                $this->ModeloCatalogos->Insert('domicilio_origen',$array);
            }else{
                $this->ModeloCatalogos->Insert('domicilio_destino',$array);
            }
            
        }
    }
    function save_rut(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        unset($params['id']);
        unset($params['tipo']);
        if ($id>0) {
            if($tipo==0){
                $this->ModeloCatalogos->updateCatalogo('origen',$params,array('id'=>$id));
            }else{
                $this->ModeloCatalogos->updateCatalogo('destino',$params,array('id'=>$id));
            }
            
        }else{
            if($tipo==0){
                $this->ModeloCatalogos->Insert('origen',$params);
            }else{
                $this->ModeloCatalogos->Insert('destino',$params);
            }
            
        }
    }
    function delete_op(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('operadores',array('activo'=>0),array('id'=>$id));
    }
    function delete_dir(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('domicilio_origen',array('activo'=>0),array('id'=>$id));
        }else{
            $this->ModeloCatalogos->updateCatalogo('domicilio_destino',array('activo'=>0),array('id'=>$id));
        }
        
    }
    function delete_rut(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('origen',array('activo'=>0),array('id'=>$id));
        }else{
            $this->ModeloCatalogos->updateCatalogo('destino',array('activo'=>0),array('id'=>$id));
        }
        
    }
    public function get_listado_dir_o() {
        $params = $this->input->post();
        $getdata = $this->ModeloDomicilio_origen->get_listado($params);
        $totaldata= $this->ModeloDomicilio_origen->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function get_listado_dir_d() {
        $params = $this->input->post();
        $getdata = $this->ModeloDomicilio_destino->get_listado($params);
        $totaldata= $this->ModeloDomicilio_destino->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function get_listado_d() {
        $params = $this->input->post();
        $getdata = $this->ModeloDomicilio_destino->get_listado($params);
        $totaldata= $this->ModeloDomicilio_destino->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function search_colonia(){
        $search = $this->input->get('search');
        $cp = $this->input->get('cp');
        $results = $this->ModeloDomicilio_origen->get_colonia_like($search,$cp);
        echo json_encode($results);    
    }

    function search_localidad(){
        $search = $this->input->get('search');
        $estado = $this->input->get('estado');
        $results = $this->ModeloDomicilio_origen->get_localidad_like($search,$estado);
        echo json_encode($results);    
    }

    function search_municipio(){
        $search = $this->input->get('search');
        $estado = $this->input->get('estado');
        $results = $this->ModeloDomicilio_origen->get_municipio_like($search,$estado);
        echo json_encode($results);    
    }
    public function get_listado_rd() {
        $params = $this->input->post();
        $getdata = $this->ModeloDestino->get_listado($params);
        $totaldata= $this->ModeloDestino->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function get_listado_ro() {
        $params = $this->input->post();
        $getdata = $this->ModeloOrigen->get_listado($params);
        $totaldata= $this->ModeloOrigen->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function search_estaciones(){
        $search = $this->input->get('search');
        $results = $this->ModeloDestino->get_estaciones_like($search);
        echo json_encode($results);    
    }
    public function get_listado_trans() {
        $params = $this->input->post();
        $getdata = $this->ModeloTransporte_federal->get_listado($params);
        $totaldata= $this->ModeloTransporte_federal->get_listado_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function search_config_transporte(){
        $search = $this->input->get('search');
        $results = $this->ModeloTransporte_federal->get_config_transporte_like($search);
        echo json_encode($results);    
    }

    function search_f_c_tipopermiso(){
        $search = $this->input->get('search');
        $results = $this->ModeloTransporte_federal->get_f_c_tipopermiso_like($search);
        echo json_encode($results);    
    }
    function vehiculos(){
        $data['btn_active']=2;
        $data['btn_active_sub']=43;
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/vehiculos');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/vehiculosjs');
    }
    function insertupdatevehiculos(){
        $params = $this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('transporte_federal',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('transporte_federal',$params);
        }
    }
    function info_tras_fed(){
        $params=$this->input->post();
        $id=$params['id'];
        $strq="SELECT trfe.*,f_c_tp.descripcion as des_f_c_tp,f_c_ct.descripcion as des_f_c_ct
                from transporte_federal as trfe 
                left JOIN f_c_tipopermiso as f_c_tp on f_c_tp.clave=trfe.tipo_permiso_sct
                left join f_c_configautotransporte as f_c_ct on f_c_ct.clave=trfe.configuracion_vhicular
                where trfe.id='$id'";
        $query = $this->db->query($strq);
        echo json_encode($query->row());
    }
    function cat_carta_porte(){
        $data['btn_active']=2;
        $data['btn_active_sub']=44;
        $data['result_vehiculos']=$this->ModeloCatalogos->getselectwheren('transporte_federal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/p_cartaporte');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/p_cartaportejs');
    }
    function insertaplatillag(){
        $params=$this->input->post();
        $id=$params['id'];
        unset($params['id']);
        $this->ModeloCatalogos->Insert('plantilla_carta_porte',$params);
    }
    function get_listado_plantilla(){
        $params = $this->input->post();
        $getdata = $this->ModeloPcataporte->get_listado_plantilla($params);
        $totaldata= $this->ModeloPcataporte->get_listado_plantilla_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function cat_carta_porte_inf($id){
        $id_pcp = $id;
        $data['btn_active']=2;
        $data['btn_active_sub']=44;
        $data['result_vehiculos']=$this->ModeloCatalogos->getselectwheren('transporte_federal',array('activo'=>1));
        //$data['result_sucu']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['result_sucu']=$this->ModeloCatalogos->getselectwherenallOrdern('sucursal',array('activo'=>1),'id_alias','ASC');

        $result_pcp=$this->ModeloCatalogos->getselectwheren('plantilla_carta_porte',array('id'=>$id));
        $data['r_pcp']=$result_pcp->row();

        $result_pcp_uo=$this->ModeloCatalogos->getselectwheren('plantilla_carta_porte_ubicaciones',array('id_pcp'=>$id,'TipoUbicacion'=>'Origen'));
        $data['uo_id']=0;
        $data['uo_id_ubicacion']=0;
        $data['uo_fechasalida']='';
        foreach ($result_pcp_uo->result() as $item) {
            $data['uo_id']=$item->id;
            $data['uo_id_ubicacion']=$item->id_ubicacion;
            $data['uo_fechasalida']=$item->fechasalida;
        }

        $data['res_fig_01']=$this->ModeloCatalogos->getselectwheren('operadores',array('TipoFigura'=>'01','activo'=>1));
        $data['res_fig_02']=$this->ModeloCatalogos->getselectwheren('operadores',array('TipoFigura'=>'02','activo'=>1));
        $data['res_fig_03']=$this->ModeloCatalogos->getselectwheren('operadores',array('TipoFigura'=>'03','activo'=>1));
        $data['res_fig_04']=$this->ModeloCatalogos->getselectwheren('operadores',array('TipoFigura'=>'04','activo'=>1));
        


        $data['res_fig_select_01']=$this->ModeloPcataporte->view_figuras_selected($id_pcp,'01');
        $data['res_fig_select_02']=$this->ModeloPcataporte->view_figuras_selected($id_pcp,'02');
        $data['res_fig_select_03']=$this->ModeloPcataporte->view_figuras_selected($id_pcp,'03');
        $data['res_fig_select_04']=$this->ModeloPcataporte->view_figuras_selected($id_pcp,'04');
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/p_cartaporte_dll');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/p_cartaporte_dlljs');
    }
    function ubicaciones(){
        $data['btn_active']=2;
        $data['btn_active_sub']=49;
        $data['result_sucu']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['result_estados']=$this->ModeloCatalogos->getselectwheren('f_c_estado',array('activo'=>1,'c_Pais'=>'MEX'));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/ubi');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/ubijs');
    }
    function view_ubicaciones(){
        $id = $this->input->post('id');
        $result=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$id));
        echo json_encode($result->row());
    }
    function save_ubicaciones(){
        $params = $this->input->post();
        $id = $params['id_ubi'];
        unset($params['id_ubi']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('sucursal',$params,array('id'=>$id));
        }
    }
    function figuras(){
        $data['btn_active']=2;
        $data['btn_active_sub']=50;
        $data['result_estados']=$this->ModeloCatalogos->getselectwheren('f_c_estado',array('activo'=>1,'c_Pais'=>'MEX'));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cat_comp/opera');
        $this->load->view('templates/footer');
        $this->load->view('cat_comp/operajs');
    }
    function save_general(){
        $params = $this->input->post();
        log_message('error',json_encode($params));
        $id_pcp = $params['id_pcp'];

        parse_str($params['tableg'], $tableg);
        
        $this->ModeloCatalogos->updateCatalogo('plantilla_carta_porte',$tableg,array('id'=>$id_pcp));

        //=================================================
            parse_str($params['tableuo'], $tableuo);
            if($tableuo['id_ubi_o']>0){
                $this->ModeloCatalogos->updateCatalogo('plantilla_carta_porte_ubicaciones',array('id_ubicacion'=>$tableuo['selectalmacenesorigen'],'fechasalida'=>$tableuo['fechasalida']),array('id'=>$tableuo['id_ubi_o']));
            }else{
                $arrray_in_o =array(
                                    'id_pcp'=>$id_pcp,
                                    'TipoUbicacion'=>'Origen',
                                    'id_ubicacion'=>$tableuo['selectalmacenesorigen'],
                                    'fechasalida'=>$tableuo['fechasalida']
                                    );
                $this->ModeloCatalogos->Insert('plantilla_carta_porte_ubicaciones',$arrray_in_o);
            }
        //================================================
        
            $DATAd = json_decode($params['destino']);
            for ($i=0;$i<count($DATAd);$i++) {
                if($DATAd[$i]->id>0){

                }else{
                    $arrray_in_o =array(
                                    'id_pcp'=>$id_pcp,
                                    'TipoUbicacion'=>'Destino',
                                    'id_ubicacion'=>$DATAd[$i]->des,
                                    'fechasalida'=>$DATAd[$i]->fec,
                                    'DistanciaRecorrida'=>$DATAd[$i]->dis
                                    );
                    $this->ModeloCatalogos->Insert('plantilla_carta_porte_ubicaciones',$arrray_in_o);
                }
            }
        //==================================================
            $DATAfig = json_decode($params['figuras']);
            for ($j=0;$j<count($DATAfig);$j++) {
                if($DATAfig[$j]->idreg>0){

                }else{
                    $array_fig =array(
                                    'id_pcp'=>$id_pcp,
                                    'id_fig'=>$DATAfig[$j]->id_fig
                                    );
                    $this->ModeloCatalogos->Insert('plantilla_carta_porte_figuras',$array_fig);
                }
            }
        //====================================================




        echo $tableg['selectVehiculos'];
        echo 'entra';
    }
    function delete_destino(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('plantilla_carta_porte_ubicaciones',array('activo'=>0),array('id'=>$id));
    }
    function carga_destinos(){
        $params = $this->input->post();
        $id_pcp = $params['id_pcp'];

        $result=$this->ModeloCatalogos->getselectwheren('plantilla_carta_porte_ubicaciones',array('id_pcp'=>$id_pcp,'TipoUbicacion'=>'Destino','activo'=>1));
        echo json_encode($result->result());
    }
    function row_figuras(){
        $params = $this->input->post();
        $id = $params['id'];
        $result=$this->ModeloCatalogos->getselectwheren('operadores',array('id'=>$id));
        echo json_encode($result->row());
    }
    function delete_fig(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('plantilla_carta_porte_figuras',array('activo'=>0),array('id'=>$id));
    }
    function consult_fig_add(){
        $params = $this->input->post();
        $id_pcp=$params['id_pcp'];
        $fig = $params['fig'];
        $result =  $this->ModeloPcataporte->view_figuras_selected($id_pcp,$fig);

        echo json_encode($result->result());

    }


}