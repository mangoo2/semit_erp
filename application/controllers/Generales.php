<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
use PhpOffice\PhpSpreadsheet\IOFactory;

class Generales extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        
    }

    public function index(){
        
    }

    function procesarfilecont($numfile){
        //$fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/Productos_final_31-dic-2024_'.$numfile.'.xlsx';
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();
              $array_pro=array();
              $array_pro_suc=array();
              $array_pro_lotes=array();
              $array_pro_proveedores=array();
              for($indiceFila = 3; $indiceFila<=$numeroFilas; $indiceFila++){

                $p_id           =   $hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $p_id         =   intval(strval($p_id));
                $p_idProducto   =   $hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $p_nombre       =   $hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $p_activo       =   $hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $p_descripcion  =   $hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $p_tipo         =   $hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                //log_message('error','p_tipo a:'.$p_tipo);
                $p_tipo         =   intval(strval($p_tipo));
                //log_message('error','p_tipo b:'.$p_tipo);
                $p_categoria    =   $hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                //8 solo es informativo
                $p_concentrador =   $hojaActual->GetCellByColumnAndRow('9',$indiceFila);
                $p_tanque       =   $hojaActual->GetCellByColumnAndRow('10',$indiceFila);
                $p_tanque       =   intval(strval($p_tanque));
                $p_capacidad    =   $hojaActual->GetCellByColumnAndRow('11',$indiceFila);//Capacidad Litros(en caso de ser tipo tanque)
                $p_capacidad    =   intval(strval($p_capacidad));
                //12 de referencias por calculo
                $p_referencia='';
                if($p_tipo==0 || $p_tipo==1 || $p_tipo==2 || $p_tipo==4){ //stock serie lote refacciones
                  $p_referencia='1,2,3';
                }elseif($p_tipo==3){// insumos
                  $p_referencia='2,3,4';
                }
                $p_idproveedor  =   $hojaActual->GetCellByColumnAndRow('13',$indiceFila);
                $p_idproveedor    =   intval(strval($p_idproveedor));
                //14 no aplica
                //$p_codigo   =   $hojaActual->GetCellByColumnAndRow('14',$indiceFila);//codigo proveedor
                $p_codigo='';
                $p_unidad_sat   =   'H87';
                $p_servicioId_sat=  $hojaActual->GetCellByColumnAndRow('16',$indiceFila);
                $p_costo_compra =   $hojaActual->GetCellByColumnAndRow('17',$indiceFila);
                $p_isr =   $hojaActual->GetCellByColumnAndRow('18',$indiceFila);
                $p_isr = floatval(strval($p_isr));
                if($p_isr>0){
                  $p_isr=1;
                }else{
                  $p_isr=0;
                }


                $ps_precio_final      =   ($hojaActual->GetCellByColumnAndRow('19',$indiceFila));
                $ps_precio_final = strval($ps_precio_final);
                $ps_precio_final = floatval($ps_precio_final);
                $ps_precio_final = round($ps_precio_final);


                $ps_incluye_iva =   $hojaActual->GetCellByColumnAndRow('20',$indiceFila);
                $ps_incluye_iva    =   intval(strval($ps_incluye_iva));
                $ps_info_iva =   $hojaActual->GetCellByColumnAndRow('21',$indiceFila);
                $ps_info_iva = floatval(strval($ps_info_iva));
                if($ps_info_iva>0){
                  $i_iva=16;
                }else{
                  $i_iva=0;
                }
                //$ps_iva=16;
                $ps_precio=floatval($ps_precio_final)/1.16;
                //log_message('error','$ps_precio: '.$ps_precio);

                $ps_stock_8 =   $hojaActual->GetCellByColumnAndRow('23',$indiceFila);//almacen general
                $ps_stock_8 =   intval(strval($ps_stock_8));
                $ps_stock_2 =   $hojaActual->GetCellByColumnAndRow('29',$indiceFila);//Pino Suárez
                $ps_stock_2 =   intval(strval($ps_stock_2));
                $ps_stock_4 =   $hojaActual->GetCellByColumnAndRow('35',$indiceFila);//Jesús Carranza
                $ps_stock_4 =   intval(strval($ps_stock_4));
                $ps_stock_5 =   $hojaActual->GetCellByColumnAndRow('41',$indiceFila);//Metepec
                $ps_stock_5 =   intval(strval($ps_stock_5));
                $ps_stock_3 =   $hojaActual->GetCellByColumnAndRow('47',$indiceFila);//Alfredo del Mazo
                $ps_stock_3 =   intval(strval($ps_stock_3));
                //$ps_stock_6 =   $hojaActual->GetCellByColumnAndRow('53',$indiceFila);//Almacén WEB
                $ps_stock_6 =0;
                $ps_stock_7 =   $hojaActual->GetCellByColumnAndRow('59',$indiceFila);//Almacén Rentas
                $ps_stock_7 =   intval(strval($ps_stock_7));

                if($p_id>0){
                  $array_producto=array(
                                      'idProducto'=>strval($p_idProducto),
                                      'nombre'=>strval($p_nombre),
                                      'activo'=>strval($p_activo),
                                      'descripcion'=>strval($p_descripcion),
                                      'categoria'=>intval(strval($p_categoria)),
                                      'referencia'=>$p_referencia,
                                      'tipo'=>$p_tipo,
                                      'concentrador'=>intval(strval($p_concentrador)),
                                      'unidad_sat'=>strval($p_unidad_sat),
                                      'servicioId_sat'=>strval($p_servicioId_sat),
                                      'costo_compra'=>floatval(strval($p_costo_compra)),
                                      'tanque'=>$p_tanque,
                                      'capacidad'=>$p_capacidad,
                                      'idproveedor'=>$p_idproveedor
                                      );
                  $array_pro[]=$array_producto;
                  //$idproducto=0;
                  //$idproducto = $this->ModeloCatalogos->Insert('productos',$array_producto);
                  if($p_idproveedor>0){
                    $array_pro_proveedores[]=array('idproveedor'=>$p_idproveedor,'idpersonal'=>1,'idproducto'=>$p_id,'codigo'=>$p_codigo);
                  }
                  //$array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>1,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>0);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>2,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_2);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>3,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_3);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>4,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_4);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>5,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_5);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>6,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_6);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>7,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_7);
                  $array_pro_suc[]=array('productoid'=>$p_id,'sucursalid'=>8,'precio'=>$ps_precio,'incluye_iva'=>$ps_incluye_iva,'iva'=>$i_iva,'precio_final'=>$ps_precio_final,'stock'=>$ps_stock_8);
                  if($p_tipo==2){
                    if($ps_stock_2>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>2,'cantidad'=>$ps_stock_2,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_3>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>3,'cantidad'=>$ps_stock_3,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_4>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>4,'cantidad'=>$ps_stock_4,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_5>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>5,'cantidad'=>$ps_stock_5,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_6>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>6,'cantidad'=>$ps_stock_6,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_7>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>7,'cantidad'=>$ps_stock_7,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                    if($ps_stock_8>0){
                      $array_pro_lotes[]=array('productoid'=>$p_id,'sucursalid'=>8,'cantidad'=>$ps_stock_8,'lote'=>'Sin lote','caducidad'=>'','cod_barras'=>'','idcompra'=>0);
                    }
                  }
                }
                
              }
              //var_dump($array_pro_suc);
            if(count($array_pro)>0){
              $this->ModeloCatalogos->insert_batch('productos',$array_pro);
            }
            if(count($array_pro_suc)>0){
              $this->ModeloCatalogos->insert_batch('productos_sucursales',$array_pro_suc);
            }
            if(count($array_pro_lotes)>0){
              $this->ModeloCatalogos->insert_batch('productos_sucursales_lote',$array_pro_lotes);
            }
              
        //}
    }
    function procesarfilecont_provee(){
        //$fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/proveedores20241220.xlsx';
        $documento = IOFactory::load($file);
        $array_pro_suc=array();
        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();

              for($indiceFila = 3; $indiceFila<=$numeroFilas; $indiceFila++){
                $codigo =   $hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $nombre       =   $hojaActual->GetCellByColumnAndRow('2',$indiceFila);

                $tel1       =   $hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $tel2  =   $hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $celular    =   $hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $correo    =   $hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                $contacto    =   $hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                $direccion    =   $hojaActual->GetCellByColumnAndRow('8',$indiceFila);
                $razon_social    =   $hojaActual->GetCellByColumnAndRow('9',$indiceFila);
                $rfc    =   $hojaActual->GetCellByColumnAndRow('10',$indiceFila);
                $cp    =   $hojaActual->GetCellByColumnAndRow('11',$indiceFila);
                $RegimenFiscalReceptor    =   $hojaActual->GetCellByColumnAndRow('12',$indiceFila);
                
                

                $array_proveedores=array(
                                    'codigo'=>$codigo, 
                                    'nombre'=>$nombre, 
                                    'tel1'=>$tel1, 
                                    'tel2'=>$tel2, 
                                    'celular'=>$celular, 
                                    'correo'=>$correo, 
                                    'contacto'=>$contacto, 
                                    'razon_social'=>$razon_social, 
                                    'rfc'=>$rfc, 
                                    'cp'=>$cp, 
                                    'RegimenFiscalReceptor'=>$RegimenFiscalReceptor, 
                                    'estatus'=>1,
                                    'uso_cfdi'=>0, 
                                    'idpersonal'=>0, 
                                    'activar_datos_fiscales'=>0, 
                                    'direccion'=>$direccion
                                    );
                $array_pro_suc[]=$array_proveedores;
                
              }
              if(count($array_pro_suc)>0){
                $this->ModeloCatalogos->insert_batch('proveedores',$array_pro_suc);
              }
              
        //}
    }
    function procesarfilecont_cargastock(){
        /*
        1 Matriz
        2 Pino Suárez
        3 Alfredo del Mazo
        4 Jesús Carranza
        5 Metepec
        6 Almacén WEB
        7 Almacén Rentas
        8 Almacén Genera
        */
        $sucursal=2;
        //$fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/inventario final Pino Suarez 30dic2023.xlsx';
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();

              for($indiceFila = 3; $indiceFila<=$numeroFilas; $indiceFila++){
                $codigo =   $hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $nombre       =   $hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $unidad       =   $hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $stock  =   $hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $precio    =   $hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $moneda   =   $hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                $total   =   $hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                $totalm         =   $hojaActual->GetCellByColumnAndRow('8',$indiceFila); 
                $stock = strval($stock);
                $stock = intval($stock);
                if($stock > 0){
                  $result_p = $this->ModeloCatalogos->getselectwheren('productos',array('idProducto'=>$codigo));
                  if($result_p->num_rows()>0){
                    $result_p =  $result_p->row();
                    $id_pro = $result_p->id;
                    if($id_pro>0){
                      $this->ModeloCatalogos->updateCatalogo('productos_sucursales',array('stock'=>$stock),array('productoid'=>$id_pro,'sucursalid'=>$sucursal));
                    }
                  }
                }
              }
        //}
    }
    function procesar_productos_lotes(){
      $strp="SELECT pro.id,pro.idProducto,pro.nombre, pros.sucursalid,pros.stock FROM productos as pro 
            INNER JOIN productos_sucursales as pros on pros.productoid=pro.id
            WHERE pro.tipo=2 AND pro.estatus=1";
      $query = $this->db->query($strp);

      $dataeqarrayinsert=array();

      foreach ($query->result() as $item) {
          $datai=array(
            'productoid'=>$item->id,
            'sucursalid'=>$item->sucursalid,
            'cantidad'=>$item->stock,
            'lote'=>'Sin Lote',
            'caducidad'=>'2024-12-30',
            'idcompra'=>0
          );
          $dataeqarrayinsert[]=$datai;
      }
      if(count($dataeqarrayinsert)>0){
        $this->ModeloCatalogos->insert_batch('productos_sucursales_lote',$dataeqarrayinsert);
      }
    }
    function procesarcargacatservicios(){
        //$fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/Listado de Rentas, Servicios de Mantenimiento y Reparación 22-ene-2024.xlsx';
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();
              $array_pro_suc=array();
              for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
                $numero =   $hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $descripcion       =   $hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $tipo       =   $hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $precio_siva  =   $hojaActual->GetCellByColumnAndRow('4',$indiceFila);

                $Unidad    =   $hojaActual->GetCellByColumnAndRow('8',$indiceFila);
                $servicioId    =   $hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                $array_pro_suc[]=array(
                  'numero'=>$numero,
                  'descripcion'=>$descripcion,
                  'tipo'=>$tipo,
                  'precio_siva'=>$precio_siva,
                  'iva'=>1,
                  'Unidad'=>$Unidad,
                  'servicioId'=>$servicioId
                );

                
              }
              if(count($array_pro_suc)>0){
                $this->ModeloCatalogos->insert_batch('cat_servicios',$array_pro_suc);
              }
              
        //}
    }
    function procesarfilecont_clientes(){
        //$fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/clientes20241220_6.xlsx';
        $documento = IOFactory::load($file);
        $array_clientes=array();
        $array_clientes_df=array();
        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();

              for($indiceFila = 3; $indiceFila<=$numeroFilas; $indiceFila++){
                $id =   $hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $nombre       =   $hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $correo    =   $hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $calleynumero  =   $hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $cp    =   $hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $col       =   $hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                //$saldo    =   $hojaActual->GetCellByColumnAndRow('7',$saldo);
                $estado    =   $hojaActual->GetCellByColumnAndRow('8',$indiceFila);
                $municipio    =   $hojaActual->GetCellByColumnAndRow('9',$indiceFila);
                //$saldopago    =   $hojaActual->GetCellByColumnAndRow('11',$indiceFila);
                $diaspago    =   $hojaActual->GetCellByColumnAndRow('12',$indiceFila);

                $razon_social    =   $hojaActual->GetCellByColumnAndRow('14',$indiceFila);
                $rfc    =   $hojaActual->GetCellByColumnAndRow('15',$indiceFila);
                $cp_df    =   $hojaActual->GetCellByColumnAndRow('16',$indiceFila);
                $RegimenFiscalReceptor    =   $hojaActual->GetCellByColumnAndRow('17',$indiceFila);
                
                

                $array_cli=array(
                                    'nombre'=>$nombre,
                                    'celular'=>'',
                                    'estatus'=>0,
                                    'correo'=>$correo,
                                    'direccion_predeterminada'=>1,
                                    'calle'=>$calleynumero,
                                    'cp'=>$cp,
                                    'colonia'=>$col,
                                    'telefono'=>'',
                                    'user'=>1,
                                    'tipo_registro'=>1,
                                    'estado'=>$estado,
                                    'municipio'=>$municipio,
                                    'diaspago'=>$diaspago,
                                    'saldopago'=>0,
                                    'sucursal'=>1
                                    );
                $array_cli_df=array(
                  'idcliente'=>$id,
                  'razon_social'=>$razon_social,
                  'rfc'=>$rfc,
                  'cp'=>$cp_df ,
                  'RegimenFiscalReceptor'=>$RegimenFiscalReceptor,
                  'uso_cfdi'=>''
                  );
                $array_clientes[]=$array_cli;
                $array_clientes_df[]=$array_cli_df;
                
              }
              if(count($array_clientes)>0){
                $this->ModeloCatalogos->insert_batch('clientes',$array_clientes);
              }
              if(count($array_clientes_df)>0){
                $this->ModeloCatalogos->insert_batch('cliente_fiscales',$array_clientes_df);
              }
              
        //}
    }

}