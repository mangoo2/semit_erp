<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitudesrentas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelSolicitudes');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,3);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=3;
        $data['btn_active_sub']=15;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('solicitudes/index');
        $this->load->view('templates/footer');
        $this->load->view('solicitudes/indexjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelSolicitudes->get_listado($params);
        $totaldata= $this->ModelSolicitudes->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
}