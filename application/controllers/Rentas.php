<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rentas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloRentas');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelClientes');
        $this->load->model('Modeloventas');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,41);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=41;        
        $data['perfilid']=$this->perfilid;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('rentas/index');
        $this->load->view('templates/footer');
        $this->load->view('rentas/indexjs');
    }

    public function alta($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=41;
        $data["id"]=$id;
        $data['estadorow']=$this->ModeloCatalogos->getselect_tabla('estado');
        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $data['get_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        if($id>0){
            $data['ren']=$this->ModeloRentas->getRentaId($id);
            $data['ine']=$this->ModeloRentas->getDocsRenta($id,1);
            $data['comp']=$this->ModeloRentas->getDocsRenta($id,2);
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('rentas/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('rentas/formjs');
    }
    
    public function searchSeries(){
        $pro = $this->input->get('search');
        $results=$this->ModeloRentas->search_series($pro,$this->sucursal);
        echo json_encode($results->result());
    }

    public function searchSeriesEquipo(){
        $id = $this->input->post('id');
        $results=$this->ModeloRentas->getSerieProd($id,7); //solo de series de renta
        $html=""; $cont_serie=0;
        //solo dejar elegible una serie, elegir la mas vieja en fecha de servicio, si no existe fecha de servicio elegir la ultima obtenida
        foreach($results as $s){
            $dis=""; $color="rgb(53,247,68, 0.6)";
            $cont_serie++;
            /*if($cont_serie>1){ //se comenta de momento para poder sacar series que ya se rentaron
                $dis="disabled";
                $color="rgb(247,59,53, 0.6)";
            } */
            $prox_mtto=$s->prox_mtto;
            if($prox_mtto==0){
                $prox_mtto="";
            }
            $html.='<tr style="background-color:'.$color.'">
                <td>'.$s->idProducto.'</td>
                <td>'.$s->nombre.'</td>
                <td>'.$s->serie.'</td>
                <td>'.$prox_mtto.'</td>
                <td><div class="col-lg-1">
                    <input type="hidden" id="sserie" value="'.$s->serie.'">
                    <input type="hidden" id="idserie" value="'.$s->id.'">
                    <span class="switch switch-icon">
                        <label>
                            <input '.$dis.' type="radio" name="add_serie" id="add_serie add_serie_'.$s->id.'" class="switch_ser" value="'.$s->id.'">
                            <span style="border: 2px solid #009bdb;"></span>
                        </label>
                    </span>
                </div></td>
            </tr>';
        }
        echo $html;
    }

    public function searchServicios(){
        $pro = $this->input->get('search');
        $results=$this->ModeloRentas->search_servs($pro);
        echo json_encode($results->result());
    }

    public function get_periodo_servs(){
        $id = $this->input->post('id');
        $periodo = $this->input->post('periodo');
        $costo=0;
        $depo=0;
        $costo15=0;
        $depo15=0;
        $costo30=0;
        $depo30=0;
        $get=$this->General_model->get_record("id",$id,"servicios");
        if($periodo==1 && $get->check1=="1"){
            $costo= $get->precios1;
            $depo=$get->depositogarantia1;
        }
        if($periodo==2 && $get->check15=="1"){
            $costo15= $get->precios15;
            $depo15=$get->depositogarantia15;
        }
        if($periodo==3 && $get->check30=="1"){
            $costo30= $get->precios30;
            $depo30=$get->depositogarantia30;
        }
        echo json_encode(array("costo"=>$costo,"costo15"=>$costo15,"costo30"=>$costo30,"depo"=>$depo,"depo15"=>$depo15,"depo30"=>$depo30,"check1"=>$get->check1,"check15"=>$get->check15,"check30"=>$get->check30));
    }

    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getclienterazonsociallike($pro);
        echo json_encode($results->result());
    }

    public function getData_rentas(){
        $params = $this->input->post();
        $getdata = $this->ModeloRentas->get_list_rentas($params,$this->sucursal,$this->idpersonal);
        $totaldata= $this->ModeloRentas->total_rentas($params,$this->sucursal,$this->idpersonal); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function save_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        $this->db->trans_start();
        if($id==0){
            $data["id_personal"]=$this->idpersonal;
            $data["fecha_reg"]=$this->fechahoy;
            $data["id_sucursal"]=$this->sucursal;
            $data["estatus"]=0; //creado pero pendiente de pago para estár activo
            $validacon=0;
            $get=$this->General_model->get_record("id",$data["id_prod"],"productos");
            if($get->concentrador==1){
                $getpc=$this->Modeloventas->getProductosConcen();
                foreach ($getpc as $pc) {
                    $resp=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$pc->id_prod,'sucursalid'=>$this->sucursal,'activo'=>1));
                    foreach ($resp->result() as $p) {
                        $cant_disp=$p->stock;
                    }
                    $cant_kit = $pc->cantidad;
                    if($cant_disp < $cant_kit){
                        $validacon++;
                    }
                    //log_message('error','cant_kit: '.$cant_kit);
                    //log_message('error','cant_disp: '.$cant_disp);
                }
            }
            if($validacon==0){
                $id=$this->General_model->add_record('rentas',$data);
                $this->General_model->edit_record('id',$data["id_serie"],array("disponible"=>0),'productos_sucursales_serie');
                if($get->concentrador==1){
                    $getpc=$this->Modeloventas->getProductosConcen();
                    foreach ($getpc as $p) {
                        $cant_inicial=0;
                        $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$p->id_prod,'sucursalid'=>$this->sucursal,'activo'=>1));
                        foreach ($res_info1->result() as $itemi) {
                            $cant_inicial=$itemi->stock;
                        }
                        $array_bit=array(
                                'id_prod'=>$p->id_prod,
                                'id_renta'=>$id,
                                'cantidad'=>$p->cantidad,
                                'cant_ini'=>$cant_inicial
                            );
                        $this->ModeloCatalogos->Insert('bitacora_prods_conce',$array_bit);
                        $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','-',$p->cantidad,'productoid',$p->id_prod,'sucursalid',$this->sucursal,'activo',1);
                    }
                }

                //notifica por mail a perfil rentas
                $this->mail_solicita_renta($id);
            }else{
                $id="NO";
            }
        }else{
            $this->General_model->edit_record('id',$id,$data,'rentas');
            //$this->General_model->edit_record('id',$data["id_serie"],array("disponible"=>0),'productos_sucursales_serie'); //ya no se puede editar la serie por el bloqueo
            //$this->mail_solicita_renta($id); //solo para pruebas
        }
        $this->db->trans_complete();
        echo $id;
    }

    public function updateEstatusContrato(){
        $id=$this->input->post("id");
        $estatus=$this->input->post("estatus");
        $id_serie=$this->input->post("id_serie");
        $id_prod=$this->input->post("id_prod");
        if($estatus==2){ //finalizado
            $this->General_model->edit_record('id',$id,array("estatus"=>$estatus,"id_personal_fin"=>$this->idpersonal,"fecha_finaliza"=>$this->fechahoy),'rentas');
            //$this->General_model->edit_record('id',$id_serie,array("disponible"=>1),'productos_sucursales_serie');
        }if($estatus==3){ //cancelado
            $this->General_model->edit_record('id',$id,array("estatus"=>$estatus,"id_personal_cancel"=>$this->idpersonal,"fecha_cancel"=>$this->fechahoy),'rentas');
            $this->General_model->edit_record('id',$id_serie,array("disponible"=>1),'productos_sucursales_serie');

            $get_ren=$this->General_model->get_record("id",$id,"rentas");
            $get=$this->General_model->get_record("id",$id_prod,"productos");
            if($get->concentrador==1){
                $getpc=$this->Modeloventas->getProductosConcen();
                foreach ($getpc as $p) {
                    $cant_inicial=0;
                    $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$p->id_prod,'sucursalid'=>$get_ren->id_sucursal,'activo'=>1));
                    foreach ($res_info1->result() as $itemi) {
                        $cant_inicial=$itemi->stock;
                    }
                    $array_bit=array(
                            'id_prod'=>$p->id_prod,
                            'id_renta'=>$id,
                            'cantidad'=>$p->cantidad,
                            'cant_ini'=>$cant_inicial
                        );
                    $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','+',$p->cantidad,'productoid',$p->id_prod,'sucursalid',$get_ren->id_sucursal,'activo',1);
                }
            }
        }if($estatus==1){ //renovar
            $get=$this->General_model->getselectwhereall_array("rentas",array("id"=>$id)); //select y duplica registro para hacer renovacion
            $data=$get[0];
            //log_message('error','get: '.json_encode($get));
            unset($data["id"]);
            
            foreach ($get as $row) {
                $periodo=$row["periodo"];
                $fecha_inicio=$row["fecha_inicio"];
                $fecha_fincs=$row["fecha_fin"];
            }
            $dias=$periodo;
            if($dias==1){
                $dias=2;
            }
            if($periodo=="2"){
                $dias=15;
            }if($periodo=="3"){
                $dias=30;
            }
            //log_message('error','dias: '.$dias);
            $fecha_fin = date('Y-m-d', strtotime($fecha_fincs . ' +'.intval($dias).' day'));
            //log_message('error','fecha_fin: '.$fecha_fin);
            $data["fecha_inicio"]=$fecha_fincs;
            $data["hora_inicio"]=date("H:i:s");
            $data["fecha_fin"]= $fecha_fin;
            $data["hora_fin"]=date("H:i:s");

            $data["id_personal"]=$this->idpersonal;
            $data["fecha_reg"]=$this->fechahoy;
            $data["id_renta_padre"]=$id;
            $data["estatus"]=0;
            $data["renovacion"]=0;
            $data["pagado"]=0;
            $data["id_venta"]=0;
            
            $this->General_model->add_record('rentas',$data);
            $this->General_model->edit_record('id',$id,array("renovacion"=>1),'rentas');
            $this->General_model->edit_record('id',$data["id_serie"],array("disponible"=>0),'productos_sucursales_serie');

            //al renovar se cobra aparte el kit
        }
    }

    public function save_datos_pdf(){
        $data=$this->input->post();
        $id=$this->input->post('id');
        if($id>0){
            unset($data["id_renta"]);
            $this->General_model->edit_record('id',$id,$data,'contrato_renta');
        }else{
            $this->ModeloCatalogos->Insert('contrato_renta',$data);
        }
    }

    public function getDetaPDF(){
        $id_renta=$this->input->post('id_renta');
        $en_pagare=""; $el_pagare=""; $cant_pagare=""; $porc_interes="5"; $nom_pagare=""; $dir_pagare=""; $pobla_pagare=""; $tel_pagare=""; $cp="";
        $get=$this->ModeloRentas->getDetallesPagare($id_renta);
        $id=0;
        foreach($get as $o){
            $id=$o->id;
            $en_pagare=$o->en_pagare;
            $el_pagare=$o->el_pagare;
            $cant_pagare=$o->cant_pagare;
            
            $nom_pagare=$o->nom_pagare;
            $dir_pagare=$o->dir_pagare;
            $pobla_pagare=$o->pobla_pagare;
            $tel_pagare=$o->tel_pagare; 

            if($o->id_cr==0){
                $nom_pagare=$o->cliente;
                $dir_pagare=$o->calle." ".$o->colonia;
                $tel_pagare=$o->celular;
                //$pobla_pagare=$o->municipio;
                $pobla_pagare=$o->cp;
                //$cant_pagare=round($o->costo,2)+round($o->deposito,2)+round($o->costo_entrega,2)+round($o->costo_recolecta,2);
                $cant_pagare=round($o->deposito,2);
            }if($o->id_cr!=0){
                $porc_interes=$o->porc_interes;
                $pobla_pagare=$o->pobla_pagare;
                $cant_pagare=$o->cant_pagare;
            }
        }
        echo json_encode(array("en_pagare"=>$en_pagare,"el_pagare"=>$el_pagare,"id"=>$id,"cant_pagare"=>$cant_pagare,"porc_interes"=>$porc_interes,"nom_pagare"=>$nom_pagare,"dir_pagare"=>$dir_pagare,"pobla_pagare"=>$pobla_pagare,"tel_pagare"=>$tel_pagare));
    }

    public function pdf_contrato($id){
        $data['id']=$id;
        $data['det']= $this->ModeloRentas->getRentaId($id);
        $this->load->view('reportes/renta_pdf',$data);
    }

    public function getRenovaciones(){
        $id=$this->input->post("id");
        $data=""; $nombre=""; $descripcion=""; $serie="";
        $renova= $this->ModeloRentas->getRenovaciones($id);
        foreach ($renova as $r) {
            $nombre=$r->nombre; $descripcion=$r->descripcion; $serie=$r->serie;
            $estatus='';
            if($r->estatus==0 || $r->pagado==0){
                $estatus="<span class='btn btn-danger'>Pendiente de pago</span>";
            }if($r->estatus==1){
                $estatus="<span class='btn btn-success'>Activo</span>";
            }if($r->estatus==2){
                $estatus="<span class='btn btn-primary'>Finalizado</span>";
            }if($r->estatus==3){
                $estatus="<span class='btn btn-danger'>Cancelado</span>";
            }
            $btn='';
            if($r->pagado=="1"){
                $btn.='<a title="Generar Contrato" style="cursor:pointer" onclick="get_pdf('.$r->id.')"><img style="width: 32px;" src="'.base_url().'public/img/pdf1.png"></a>';
            }
            $btn.='<a title="Pagos Contrato" style="cursor:pointer" onclick="pay_contra('.$r->id.')"><img style="width: 32px;" src="'.base_url().'public/img/pay.svg"></a>';
            if($r->estatus==1 && $r->pagado==1){
                $btn.='<a title="Finalizar Contrato" onclick="change_estatus('.$r->id.',2)" class="btn"><img style="width: 30px;" src="'.base_url().'public/img/bill-invoice.svg"></a>';
            }if($r->pagado==0){
                $btn.='<a title="Cancelar Contrato" onclick="change_estatus('.$r->id.',3)" class="btn"><img style="width: 30px;" src="'.base_url().'public/img/cancel.svg"></a>';
            }if($r->estatus!=3){
                $btn.='<a href="'.base_url().'Rentas/alta/'.$r->id.'" class="btn"><img style="width: 30px;" src="'.base_url().'public/img/edit.svg"></a>';
            }
            $data.="<tr>
                    <td>".$r->id."</td>
                    <!--<td>".$r->nombre."</td>
                    <td>".$r->descripcion."</td>
                    <td>".$r->serie."</td>-->
                    <td>".date("d-m-Y ",strtotime($r->fecha_inicio)).' '.date("H:i a ",strtotime($r->hora_inicio))."</td>
                    <td>".date("d-m-Y ",strtotime($r->fecha_fin)).' '.date("H:i a ",strtotime($r->hora_fin))."</td>
                    <td>".$r->name_suc."</td>
                    <td>".$estatus."</td>
                    <td>".$btn."</td>
                </tr>";
        }
        echo json_encode(array("data"=>$data,"nombre"=>$nombre,"descripcion"=>$descripcion,"serie"=>$serie));
    }

    public function updateFolio(){
        $id=$this->input->post("id");
        $folio=$this->input->post("folio");
        $this->General_model->edit_record('id',$id,array("folio"=>$folio),'rentas');
    }

    public function getPagos(){
        $id=$this->input->post("id");
        $html=""; $nombre=""; $descripcion=""; $serie=""; $pagado=0; $id_venta=0; $folio=""; $total=0;
        $renova= $this->ModeloRentas->getPagosContrato($id);
        foreach ($renova as $r) {
            $nombre=$r->nombre; $descripcion=$r->descripcion; $serie=$r->serie; $pagado=$r->pagado; $id_venta=$r->id_venta;
            $folio=$r->folio;
            $total=round($r->costo+$r->deposito+$r->costo_entrega+$r->costo_recolecta,2);
            /*$html.="<tr>
                    <td>".$r->id."</td>
                    <!--<td>".$r->nombre."</td>
                    <td>".$r->descripcion."</td>
                    <td>".$r->serie."</td>-->
                    <td>".date("d-m-Y ",strtotime($r->fecha_inicio)).' '.date("H:i a ",strtotime($r->hora_inicio))."</td>
                    <td>".date("d-m-Y ",strtotime($r->fecha_fin)).' '.date("H:i a ",strtotime($r->hora_fin))."</td>
                    <td>".$r->name_suc."</td>
                    <td>".$estatus."</td>
                    <td>".$btn."</td>
                </tr>";*/
        }
        echo json_encode(array("html"=>$html,"total"=>$total,"nombre"=>$nombre,"descripcion"=>$descripcion,"serie"=>$serie,"pagado"=>$pagado,"id_venta"=>$id_venta,"folio"=>$folio));
    }

    public function cargaimagen(){
        $id_renta=$_POST['id_renta'];
        $input_name=$_POST['input_name'];

        if($input_name=="doc_ine"){
            $DIR_SUC=FCPATH.'uploads/ine';
            $tipo="1";
        }else if($input_name=="doc_comp"){
            $DIR_SUC=FCPATH.'uploads/comprobante';
            $tipo="2";
        }
        $config['upload_path']    = $DIR_SUC;
        $config['allowed_types']  = 'gif|jpg|png|jpeg|bmp|pdf';
        $config['max_size']       = 8000;
        $file_names=date('YmdGis')."_".$input_name;
        $config['file_name']=$file_names;       
        $output = [];

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload($input_name)){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $this->General_model->add_record('docs_rentas',array('id_renta'=>$id_renta,"documento"=>$file_name,"tipo"=>$tipo,"fecha_reg"=>$this->fechahoy));
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function deleteDoc($id){
        $res=$this->General_model->edit_record('id',$id,array("estatus"=>0),'docs_rentas');
        echo $res;
    }


    /*****************************************/
    function get_data_cliente(){
        $id=$this->input->post('id');
        $nombre='';
        $usuario='';
        $correo='';
        $contrasena='';
        $contrasena2='';
        $direccion='';

        $calle='';
        $cp='';
        $colonia='';

        $razon_social='';
        $rfc='';
        $cp='';
        $RegimenFiscalReceptor='';
        $uso_cfdi='';
        $cpy='';
        $saldo='';
        $desactivar=0;
        $motivo='';
        $estado = '';
        $municipio = '';
        $diascredito='';
        $dias_saldo='';
        $diaspago='';
        $saldopago='';
        $resul=$this->General_model->getselectwhere('clientes','id',$id);
        foreach ($resul as $item) {
            $nombre=$item->nombre;
            $usuario=$item->usuario;
            $correo=$item->correo;
            $contrasena='x1f3[5]7w78{';     
            $contrasena2='x1f3[5]7w78{';

            $calle=$item->calle;
            $cp=$item->cp;
            $colonia=$item->colonia;  
            $saldo=$item->saldo;  
            $desactivar=$item->desactivar;  
            $motivo=$item->motivo;  
            $estado = $item->estado;
            $municipio = $item->municipio;
            $diascredito = $item->diascredito;
            $dias_saldo = $item->dias_saldo;
            $diaspago = $item->diaspago;
            $saldopago = $item->saldopago;
        }

        $razon_social='';
        $rfc='';
        $cpy='';
        $RegimenFiscalReceptor='';
        $uso_cfdi='';

        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        foreach ($resulf as $itemf) {
            $direccion=$itemf->direccion;
            $razon_social=$itemf->razon_social;
            $rfc=$itemf->rfc;
            $cpy=$itemf->cp;
            $RegimenFiscalReceptor=$itemf->RegimenFiscalReceptor;
            $uso_cfdi=$itemf->uso_cfdi;
        }
        $estadorow=$this->ModeloCatalogos->getselect_tabla('estado');
        $get_regimen=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $get_cfdi=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $html='<ul class="nav nav-tabs" id="icon-tab" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="icon-home-tabx" data-bs-toggle="tab" href="#icon-homex" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>Datos generales</a></li>
              <li class="nav-item"><a class="nav-link" id="profile-icon-tabx" data-bs-toggle="tab" href="#profile-iconx" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Datos fiscales</a></li>
              <li class="nav-item"><a class="nav-link" id="pagos-icon-tabx" data-bs-toggle="tab" href="#pagos-iconx" role="tab" aria-controls="pagos-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Condiciones de pago</a></li>
              <li class="nav-item"><a class="nav-link" id="contact-icon-tabx" data-bs-toggle="tab" href="#contact-iconx" role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Sesión en pagína web</a></li>
            </ul>
            <div class="tab-content" id="icon-tabContent">
              <div class="tab-pane fade show active" id="icon-homex" role="tabpanel" aria-labelledby="icon-home-tabx">
                <br>
                <form id="form_registro_datosx" method="post">
                    <input type="hidden" id="idreg1x" name="id" value="'.$id.'">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nombre del cliente</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="nombre" id="razon_socialx" value="'.$nombre.'">
                                </div>
                            </div>
                        </div>

                    </div>    

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Correo electrónico</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()" value="'.$correo.'">
                                </div>   
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Calle y número</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="calle" value="'.$calle.'">
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Código postal</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="cp" id="codigo_postalx" oninput="cambiaCP2x()" value="'.$cp.'">
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Colonia</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="coloniax" name="colonia" onchange="regimefiscal()" required>
                                        <option value="0" selected disabled>Seleccione</option>';
                                        if($colonia!=''){
                                            $html.='<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Saldo</label>
                                <div class="col-lg-5"><input class="form-control" type="number" name="saldo" id="saldo" value="'.$saldo.'">
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Suspender</label>
                                <div class="col-lg-5">
                                    <span class="switch switch-icon">
                                        <label>';
                                        $desactivartxt='';
                                        if($desactivar==1){
                                            $desactivartxt='checked';
                                        }
                                        $html.='<input type="checkbox" name="desactivar" id="desactivarx" '.$desactivartxt.' onclick="activar_campox()">';
                                        $html.='<span></span>
                                        </label>
                                    </span>
                                </div>   
                            </div>
                        </div>';
                        
                            $motivo_sty='';
                            if($desactivar==1){
                                $motivo_sty='style="display: block;"';
                            }else{
                                $motivo_sty='style="display: none;"';
                            }
                    
                        $html.='<div class="col-lg-12">
                            <div class="motivo_txt" '.$motivo_sty.'>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Motivo</label>
                                    <div class="col-lg-5">
                                        <textarea class="form-control" rows="3" name="motivo">'.$motivo.'</textarea>
                                    </div>   
                                </div>
                            </div>    
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Estado</label>
                                <div class="col-lg-5">
                                    <select name="estado" id="estado" class="form-control">';
                                      foreach ($estadorow->result() as $item){ 
                                        if($estado==$item->id){ 
                                            $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                                        }else{
                                            $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
                                        }
                                      
                                       } 
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Municipio</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="municipio" value="'.$municipio.'">
                                </div>   
                            </div>
                        </div>
                    </div>
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datosx()">Guardar</button>                      
              </div>
              <div class="tab-pane fade" id="profile-iconx" role="tabpanel" aria-labelledby="profile-icon-tabx">
                <br>
                <form id="form_registro_datos_fiscalesx" method="post">
                    <input type="hidden" id="idreg2x" name="id" value="'.$id.'">

                    <div class="row">
                        <div class="col-md-6">
                            <label class="form-label">Activar datos fiscales</label>
                            <span class="switch switch-icon">
                            <label>
                                <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscalesx" >
                                <span></span>
                            </label>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Razón social</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text"  name="razon_social" oninput="validar_razon_socialx()" value="'.$razon_social.'">
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">RFC</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="rfc" id="rfcx" oninput="validar_rfc_clientex()" value="'.$rfc.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">CP</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="cp"  value="'.$cpy.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Régimen fiscal</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="RegimenFiscalReceptorx" name="RegimenFiscalReceptor" onchange="regimefiscalx()">
                                        <option value="0" selected disabled>Seleccione</option>';
                                        foreach ($get_regimen->result() as $item) {
                                            if($item->clave==$RegimenFiscalReceptor){
                                                $html.='<option value="'.$item->clave.'" selected>'.$item->clave.' '.$item->descripcion.'</option>';
                                            }else{
                                                $html.='<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                                            } 
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Uso de CFDI</label>
                                <div class="col-lg-5">
                                    <select class="form-control" data-cliuso="'.$uso_cfdi.'" id="uso_cfdix" name="uso_cfdi">
                                        <option value="0" >Seleccione</option>';
                                        foreach ($get_cfdi->result() as $item) {
                                            if($item->uso_cfdi==$uso_cfdi){
                                                $html.='<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'"  selected>'.$item->uso_cfdi_text.'</option>';
                                            }else{
                                                $html.='<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                                            }
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                    </div>
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos_fiscalesx()">Guardar</button>        
              </div>
              <div class="tab-pane fade" id="pagos-iconx" role="tabpanel" aria-labelledby="pagos-icon-tabx">
                <br>
                <form id="form_registro_pagosx" method="post">
                    <input type="hidden" id="idreg4x" name="id" value="'.$id.'">
                    <div class="row">
                        <!-- <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Número de días de crédito</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="number" id="diascredito" name="diascredito" value="<?php // echo $diascredito ?>">
                                </div>
                            </div>
                        </div> -->';
                    
                            $dias_saldo1='style="display:none"'; 
                            $dias_saldo2='style="display:none"'; 
                            if($dias_saldo==1){
                                $dias_saldo1='style="display:block"'; 
                                $dias_saldo2='style="display:none"'; 
                            }else if($dias_saldo==2){
                                $dias_saldo1='style="display:none"'; 
                                $dias_saldo2='style="display:block"'; 
                            }
                
                        $html.='<div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En días</label>
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>';
                                        $dias_saldotxt='';
                                        if($dias_saldo==1){
                                            $dias_saldotxt='checked';
                                        }
                                            $html.='<input type="radio" name="dias_saldo" id="dias_saldo" value="1" onclick="tipo_pago(1)" '.$dias_saldotxt.'>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>   
                                <div class="col-lg-2 diaspagotxt" '.$dias_saldo1.'>
                                    <select class="form-control" id="diaspago" name="diaspago" required>
                                        <option value="0" selected disabled>Seleccionar días</option>
                                        <option value="15"';if($diaspago==15){$html.='selected';}$html.='>15</option>
                                        <option value="30"';if($diaspago==30){$html.='selected';}$html.='>30</option>
                                    </select>
                                </div> 
                                <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En saldo</label>
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="radio" name="dias_saldo" id="dias_saldo" value="2" onclick="tipo_pago(2)"';if($dias_saldo==2){$html.='checked';}$html.='>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>  
                                <div class="col-lg-2 saldopagotxt" '.$dias_saldo2.'> 
                                    <input class="form-control" type="number" id="saldopago" name="saldopago" value="'.$saldopago.'">
                                </div>  
                            </div>
                        </div>
                    </div>    
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_pagosx()">Guardar</button>        
              </div>
              <div class="tab-pane fade" id="contact-iconx" role="tabpanel" aria-labelledby="contact-icon-tabx">
                <br>
                <form id="form_registro_usuariox" method="post">
                    <input type="hidden" id="idreg3x" name="id" value="'.$id.'">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="text" name="usuario" id="usuariox" autocomplete="nope" oninput="verificar_usuariox()" value="'.$usuario.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Contraseña</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="contrasenax" id="contrasenax" autocomplete="new-password" oninput="" value="'.$contrasena.'">
                                </div>  
                                <div class="col-5">
                                    <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                        <i class="icon-xl fas fa-eye" style="color: white"></i>
                                    </a>
                                </div> 
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="contrasena2x" id="contrasena2x" autocomplete="new-password" value="'.$contrasena2.'">
                                </div>   
                            </div>
                        </div> 
                    </div>    
                </form>
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_usuariox()">Guardar</button>
              </div>  
            </div>';
        echo $html;
    }

    public function registro_datos_cliente(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['direccion']);
        if(isset($data['desactivar'])){
            $data['desactivar']=1;
        }else{
            $data['desactivar']=0;
        }
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
            $datosf = array('idcliente'=>$id);
            $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
        }else{
            $this->General_model->edit_record('id',$id,$data,'clientes');
            $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
            $idf=0;
            foreach ($resulf as $itemf) {
                $idf=$itemf->id;
            }
            $datosf = array('idcliente'=>$id);
            if($idf==0){
                $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
            }else{
                $this->General_model->edit_record('id',$idf,$datosf,'cliente_fiscales');
            }
            $idf=$idf;
        }
        echo json_encode(array("id"=>$id,"idf"=>$idf));
    }

    function validar_correo(){
        $correo = $this->input->post('correo');
        $result=$this->General_model->getselectwhereall('clientes',array('correo'=>$correo,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function registro_datos_fiscales(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        $idf=0;
        foreach ($resulf as $itemf) {
            $idf=$itemf->id;
        }

        if($idf==0){
            $this->General_model->add_record('cliente_fiscales',$data);
        }else{
            $this->General_model->edit_record('id',$idf,$data,'cliente_fiscales');
        }
        $id=$id;
        echo $id;
    }

    public function registro_datos_pago(){
        $data=$this->input->post();
        $id=$data['id'];
        $this->General_model->edit_record('id',$id,$data,'clientes');
        echo $id;
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];

        $pss_verifica = $data['contrasena'];
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($data['contrasena']);
        }

        unset($data['contrasena2']);
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'clientes');
            $id=$id;
        }
        echo $id;
    }

    function get_validar_razon_social(){
        $razon_social = $this->input->post('razon_social');
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('razon_social'=>$razon_social,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function get_validar_rfc(){
        $rfc = $this->input->post('rfc');
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('rfc'=>$rfc,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }
    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    } 

    function mail_solicita_renta($id){ 
        $get=$this->ModeloRentas->getRentaId($id);        
        //log_message('error','enviar mail de renta: '.$id);
        $asunto='Solicitud de renta';
        $this->load->library('email');
        
        $config['protocol'] = 'smtp';
        $config["smtp_host"] ='mail.semit.mx'; 
        $config["smtp_user"] = 'rentas@semit.mx';
        $config["smtp_pass"] = '@Zp5F0nn-;_+';
        $config["smtp_port"] = '465';
        $config["smtp_crypto"] = 'ssl';
        $config['charset'] = 'utf-8'; 
        $config['wordwrap'] = TRUE; 
        $config['validate'] = true;
        $config['mailtype'] = 'html';
         
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('rentas@semit.mx','Rentas Semit');

        if($get->correo!=""){
            $this->email->to($get->correo);
        }
        $this->email->subject($asunto);
        
        $message ="Solicitud de renta: ".$get->name_suc .
        "<p>Registro de renta con No. ".$id."</p>".
        "<p>Fecha: ".date("H:i a d-m-Y", strtotime($get->fecha_reg))." </p>".
        "<p>Servicio: ".utf8_encode($get->descripcion)." </p>".
        "<p>Equipo: ".utf8_encode($get->equipo)." </p>".
        "<p>Serie: ".$get->serie." </p>".
        "<span>Total de renta: $".number_format($get->costo+$get->deposito+$get->costo_entrega+$get->costo_recolecta,2,'.',',')." </span>";
        //log_message('error','message: '.$message);

        $this->email->message($message);
        $this->email->send();

    }

    function validar(){
        $usuario = $this->input->post('usuario');
        $result=$this->General_model->getselectwhere('clientes','usuario',$usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function registro_datosx(){
        $data=$this->input->post();
        $id=$data['id'];

        $pss_verifica = $data['contrasenax'];
        $pass = password_hash($data['contrasenax'], PASSWORD_BCRYPT);
        $data['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($data['contrasena']);
        }
        unset($data['contrasenax']);
        unset($data['contrasena2x']);
        

        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'clientes');
            $id=$id;
        }
        echo $id;
    }
    
}