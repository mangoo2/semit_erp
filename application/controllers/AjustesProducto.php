<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AjustesProducto extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos'); 
        $this->load->model('General_model'); 
        $this->load->model('ModeloAjustes');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,51);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=51;
         $data['sucs']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ajustes/index');
        $this->load->view('templates/footer');
        $this->load->view('ajustes/indexjs');
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        $id=$this->General_model->add_record('bitacora_ajustes',$data);
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloAjustes->searchProductosAjuste($pro);
        echo json_encode($results);
    }

    public function getTableProd(){
        $id_prod = $this->input->post('id_prod');
        $tipo = $this->input->post('tipo');
        $idsuc = $this->input->post('id_suc');
        $recarga = $this->input->post('recarga');
        //log_message("error", "tipo: ".$tipo);
        $html="";
        if($tipo==0){
            $getprod=$this->ModeloAjustes->get_productosAjuste($id_prod,$idsuc);
            foreach($getprod as $p){
                $html.="<tr>
                    <td><input type='hidden' class='form-control' id='idp' value='".$p->id."'><input type='hidden' class='form-control' id='tipo_recarga' value='0'>
                    <input type='hidden' class='form-control tipo_prod' id='tipo' value='".$tipo."'>".$p->idProducto."</td>
                    <td><input type='hidden' class='form-control' id='id_suc' value='".$idsuc."'><input type='hidden' class='form-control' id='id_ps' value='".$p->id_ps."'>".$p->nombre."</td>
                    <td><input type='hidden' class='form-control' id='cantidad' value='".$p->stock."'><input type='number' class='form-control' id='stock_inv' value='".$p->stock."'></td>
                    <td>".$p->name_suc."</td>
                </tr>";
            }
        }
        if($tipo==1){ //serie
            $getprod=$this->ModeloAjustes->get_productosAjusteSerie($id_prod,$idsuc);
            foreach($getprod as $p){
                $html.="<tr>
                    <td><input type='hidden' class='form-control' id='idp' value='".$p->id."'><input type='hidden' class='form-control' id='tipo_recarga' value='0'>
                    <input type='hidden' class='form-control tipo_prod' id='tipo' value='".$tipo."'>".$p->idProducto."</td>
                    <td><input type='hidden' class='form-control' id='id_suc' value='".$idsuc."'><input type='hidden' class='form-control' id='id_ps' value='".$p->id_ps."'>".$p->nombre."</td>
                    <td><input type='hidden' class='form-control' id='num_serie' value='".$p->serie."'><input type='text' class='form-control' id='serie_ajuste' value='".$p->serie."'></td>
                    <td>".$p->name_suc."</td>
                </tr>";
            }
        }
        if($tipo==2){ //lote y caducidad
            $getprod=$this->ModeloAjustes->get_productosAjusteLote($id_prod,$idsuc);
            foreach($getprod as $p){
                $html.="<tr>
                    <td><input type='hidden' class='form-control' id='idp' value='".$p->id."'><input type='hidden' class='form-control' id='tipo_recarga' value='0'>
                    <input type='hidden' class='form-control tipo_prod' id='tipo' value='".$tipo."'>".$p->idProducto."</td>
                    <td><input type='hidden' class='form-control' id='id_suc' value='".$idsuc."'><input type='hidden' class='form-control' id='id_ps' value='".$p->id_ps."'>".$p->nombre."<br> 
                        ".$p->lote." / ".$p->caducidad."</td>
                    <td><input type='hidden' class='form-control' id='cantidad' value='".$p->cantidad."'><input type='number' class='form-control' id='stock_inv' value='".$p->cantidad."'></td>
                    <td>".$p->name_suc."</td>
                </tr>";
            }
        }

        if($recarga==1){ //recarga
            $getprod=$this->ModeloAjustes->get_recargaAjuste($idsuc);
            foreach($getprod as $p){
                $html.="<tr>
                    <td><input type='hidden' class='form-control' id='idp' value='".$p->id."'>
                        <input type='hidden' class='form-control' id='tipo_recarga' value='1'>
                        <input type='hidden' class='form-control tipo_prod' id='tipo' value='0'>".$p->codigo."
                    </td>
                    <td><input type='hidden' class='form-control' id='id_suc' value='".$p->sucursal."'><input type='hidden' class='form-control' id='id_ps' value='0'>".$p->capacidad." L</td>
                    <td><input type='hidden' class='form-control' id='cantidad' value='".$p->stock."'><input type='number' class='form-control' id='stock_inv' value='".$p->stock."'></td>
                    <td>".$p->name_suc."</td>
                </tr>";
            }
        }
        echo $html;
    }

    function guardaAjusteProds(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $band_save=0;  
            $data['id_producto']=$DATA[$i]->idp;
            $data['id_ps']=$DATA[$i]->id_ps;
            $data['id_sucursal']=$DATA[$i]->id_suc;
            $data['tipo_prod']=$DATA[$i]->tipo;
            //log_message("error","tipo: ".$DATA[$i]->tipo);
            $data['tipo_recarga']=$DATA[$i]->tipo_recarga;
            $tipo_recarga=$DATA[$i]->tipo_recarga;

            $data['id_personal']=$this->idpersonal;
            $data['fecha_reg']=$this->fechahoy;
            if($DATA[$i]->tipo_recarga==0){
                if($DATA[$i]->tipo==0 || $DATA[$i]->tipo==3 || $DATA[$i]->tipo==4){
                    $data['cantidad']=$DATA[$i]->cantidad;  
                    $data['cantidad_ajuste']=$DATA[$i]->cantidad_ajuste;
                    if($DATA[$i]->cantidad != $DATA[$i]->cantidad_ajuste){
                        $band_save++;
                        $id=$this->General_model->edit_recordw(array("id"=>$DATA[$i]->id_ps),array("stock"=>$DATA[$i]->cantidad_ajuste),'productos_sucursales');
                    }
                }
                if($DATA[$i]->tipo==1){ //de serie
                    $data['num_serie']=$DATA[$i]->num_serie;  
                    $data['serie_ajuste']=$DATA[$i]->serie_ajuste;
                    if($DATA[$i]->num_serie != $DATA[$i]->serie_ajuste){
                        $band_save++;
                        $id=$this->General_model->edit_recordw(array("id"=>$DATA[$i]->id_ps),array("serie"=>$DATA[$i]->serie_ajuste),'productos_sucursales_serie');
                    }
                }
                if($DATA[$i]->tipo==2){ //de Lote y caducidad
                    $data['cantidad']=$DATA[$i]->cantidad;  
                    $data['cantidad_ajuste']=$DATA[$i]->cantidad_ajuste;
                    if($DATA[$i]->cantidad != $DATA[$i]->cantidad_ajuste){
                        $band_save++;
                        $id=$this->General_model->edit_recordw(array("id"=>$DATA[$i]->id_ps),array("cantidad"=>$DATA[$i]->cantidad_ajuste),'productos_sucursales_lote');
                    }
                }
            }else{
                $band_save++;
                $tipo_recarga=$DATA[$i]->tipo_recarga;
                $data['cantidad']=$DATA[$i]->cantidad;  
                $data['cantidad_ajuste']=$DATA[$i]->cantidad_ajuste;
                $id=$this->General_model->edit_recordw(array("id"=>$DATA[$i]->idp),array("stock"=>$DATA[$i]->cantidad_ajuste),'recargas');
            }
            if($band_save>0){
                $this->General_model->add_record('bitacora_ajustes',$data);
            }
        }
        echo $tipo_recarga;
    }

    public function getTableProdAjustes(){
        $id_prod = $this->input->post('id_prod');
        $tipo = $this->input->post('tipo');
        $idsuc = $this->input->post('id_suc');
        $recarga = $this->input->post('recarga');
        $html="";

        if($recarga==0){
            $getb=$this->ModeloAjustes->get_bitacoraAjuste($id_prod,$idsuc,$tipo);
            foreach($getb as $p){ //acá me quedo
                if($tipo==0 || $tipo==3 || $tipo==4){
                    $det_mov="Cant. inicial: ".$p->cantidad."<br>Cant. Mov: ".$p->cantidad_ajuste;
                }
                if($tipo==1){
                    $det_mov="Serie inicial: ".$p->num_serie."<br>Serie Mov: ".$p->serie_ajuste;
                }
                if($tipo==2){
                    $det_mov="Cant. inicial: ".$p->cantidad."<br>Cant. Mov: ".$p->cantidad_ajuste;
                }
                $html.="<tr>
                    <td>".$p->id."</td>
                    <td>".$p->idProducto." / ".$p->nombre."</td>
                    <td>".date('d-m-Y', strtotime($p->fecha_reg))."</td>
                    <td>".$p->det_lote."</td>
                    <td>".$det_mov."</td>
                </tr>";
            }
        }else{ //de tipo recarga
            $getb=$this->ModeloAjustes->get_bitacoraAjusteRec($idsuc);
            foreach($getb as $p){ //acá me quedo
                $det_mov="Cant. inicial: ".$p->cantidad."<br>Cant. Mov: ".$p->cantidad_ajuste;
                $html.="<tr>
                    <td>".$p->id."</td>
                    <td>".$p->codigo." / ".$p->capacidad." L</td>
                    <td>".date('d-m-Y', strtotime($p->fecha_reg))."</td>
                    <td>Oxígeno medicinal</td>
                    <td>".$det_mov."</td>
                </tr>";
            }
        }
        echo $html;
    }

}