<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloRentas');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelticket');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,54);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=54;        
        $data['perfilid']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ticket/index');
        $this->load->view('templates/footer');
        $this->load->view('ticket/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;

        if($id==0){
            $data['id'] = 0;
            $data['asunto']='';
            $data['mensaje']='';
        }else{
            $resul=$this->General_model->getselectwhere('ticket','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['asunto']=$item->asunto;
                $data['mensaje']=$item->mensaje;
            }
        }   
        $data['key']=uniqid();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ticket/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('ticket/formjs');
    }

    public function form($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['idticket']=$id;
        $resul=$this->General_model->getselectwhere('ticket','id',$id);
        foreach ($resul as $item) {
            $data['id']=$item->id;
            $data['asunto']=$item->asunto;
            $data['mensaje']=$item->mensaje;
        }
        $data['result']=$this->General_model->getselectwhere('ticket_file','idticket',$id);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ticket/ticket',$data);
        $this->load->view('templates/footer');
        $this->load->view('ticket/ticketjs');
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data['idpersonal']=$this->idpersonal;
            $data['reg']=$this->fecha_hora_actual;
            $data['estatus']=1;
            $id=$this->General_model->add_record('ticket',$data);
        }else{
            $data['idpersonal']=$this->idpersonal;
            $this->General_model->edit_record('id',$id,$data,'ticket');
        }
        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->Modelticket->get_listado($params);
        $totaldata= $this->Modelticket->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function baner_imagenes_multiple(){
        $idreg=$_POST['idreg'];
        $key=$_POST['key'];
        $data = [];
        $count = count($_FILES['files']['name']);
        $output = [];
        for($i=0;$i<$count;$i++){
            if(!empty($_FILES['files']['name'][$i])){
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                $DIR_SUC=FCPATH.'uploads/tickets';
                $config['upload_path'] = $DIR_SUC; 
                //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
                $config['allowed_types'] = '*';
                //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
                $config['max_size'] = 5000;
                $file_names=date('Ymd_His').'_'.rand(0, 500);
                //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
                $config['file_name'] = $file_names;
                //$config['file_name'] = $file_names;
                $this->load->library('upload',$config); 
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $filenametype = $uploadData['file_ext'];
                    $data['totalFiles'][] = $filename;
                    $this->ModeloCatalogos->Insert('ticket_file',array('file'=>$filename,'codigo'=>$key));
                }else{
                    $data = array('error' => $this->upload->display_errors());
                    log_message('error', json_encode($data)); 
                }
            }
   
        }
        $array = array('idreg'=>$idreg);
        echo json_encode($array);
    }

    public function update_idticket(){
        $key=$this->input->post('key');
        $id=$this->input->post('id');
        $data = array('idticket'=>$id);
        $this->General_model->edit_record('codigo',$key,$data,'ticket_file');
    }

    public function registro_datos_respuesta(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data['idpersonal']=$this->idpersonal;
            $data['reg']=$this->fecha_hora_actual;
            if($this->idpersonal==1){
                $data['tipo']=1;
            }else{
                $data['tipo']=0;
            }
            
            $id=$this->General_model->add_record('ticket_respuesta',$data);
        }else{
            $data['idpersonal']=$this->idpersonal;
            $this->General_model->edit_record('id',$id,$data,'ticket_respuesta');
        }
        echo $id;
    }

    function baner_imagenes_multiple_respuesta(){
        $key=$_POST['key'];
        $data = [];
        $count = count($_FILES['files']['name']);
        $output = [];
        for($i=0;$i<$count;$i++){
            if(!empty($_FILES['files']['name'][$i])){
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                $DIR_SUC=FCPATH.'uploads/tickets_respuesta';
                $config['upload_path'] = $DIR_SUC; 
                //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
                $config['allowed_types'] = '*';
                //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
                $config['max_size'] = 5000;
                $file_names=date('Ymd_His').'_'.rand(0, 500);
                //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
                $config['file_name'] = $file_names;
                //$config['file_name'] = $file_names;
                $this->load->library('upload',$config); 
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $filenametype = $uploadData['file_ext'];
                    $data['totalFiles'][] = $filename;
                    $this->ModeloCatalogos->Insert('ticket_respuesta_file',array('file'=>$filename,'codigo'=>$key));
                }else{
                    $data = array('error' => $this->upload->display_errors());
                    log_message('error', json_encode($data)); 
                }
            }
   
        }
        $array = array('idreg'=>0);
        echo json_encode($array);
    }
    public function get_key()
    {
        $key=uniqid();
        echo $key; 
    }

    public function update_idticket_respuesta(){
        $key=$this->input->post('key');
        $id=$this->input->post('id');
        $data = array('idticket'=>$id);
        $this->General_model->edit_record('codigo',$key,$data,'ticket_respuesta_file');
    }

}