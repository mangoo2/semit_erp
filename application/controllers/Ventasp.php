<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventasp extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelProductos');
        $this->load->model('Modeloventas');
        $this->load->model('Modelonumeroletra');
        $this->load->model('ModelClientes');
        $this->load->model('ModelCotizaciones'); 
        $this->load->model('ModeloRentas');
        $this->load->model('ModeloMttos'); 
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoy_normal = date('d/m/Y');
        $this->horahoy_normal = date('H:i:s');
        $this->fecha_larga = date('Y-m-d H:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechacod = date('ymdHis');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,18);// perfil y id del submenu
            //$this->sucusal=1;//cuando ya esten las sucursales
            //$this->folio=$this->sucusal.'-'.$this->fechacod;
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
        /*
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();
        */
    }

	public function index(){
        $this->limpiartabla();// se agrego limpiar las session cada ves que entren por que luegos les sale error
        if(isset($_GET['idr'])){
            $data['rta_cli']=$this->ModeloRentas->getRentaId($_GET['idr']);
        }
        if(isset($_GET['idsrv'])){
            $data['serv_cli']=$this->ModeloMttos->get_mtto($_GET['idsrv']);
        }
        $data['btn_active']=6;
        $data['btn_active_sub']=18;
        $data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        if($_SERVER['HTTP_HOST']=='adminsys.semit.mx'){
            $data['get_clientegen']=$this->General_model->get_select('cliente_fiscales',array('id'=>8255));//para productivo tiene que ser el 8255 para pruebas el 2
        }else{
            $data['get_clientegen']=$this->General_model->get_select('cliente_fiscales',array('id'=>2,'activo'=>1)); 
        }
        
        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $data['get_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $data['get_formaspagosv']=$this->General_model->formaspagosv();
        $data['fecha']=$this->fechahoy_normal;

        //$hora=$this->horahoy_normal;
        $horaactualm1=date('H:i:s');
        $horaactualm1 = strtotime ( '-1 hours' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1=date ( 'h:i a' , $horaactualm1 );

        $data['hora']=$horaactualm1;
        //$data['hora']=$this->horahoy_normal_l;
        $data['results_turno']=$this->General_model->turnos_limit1($this->sucursal,$this->fecha_reciente); 

        if($this->session->userdata('perfilid')==3 || $this->session->userdata('tipo_tecnico')==1){
            $preventa=1;
        }else{
            $preventa=0;
        }
        $data['preventa']=$preventa;
        $data['sucursal']=$this->sucursal;
        $data['idpersonal']=$this->idpersonal;
        $data['perfilid']=$this->perfilid;
        $data['get_vendedores']=$this->Modeloventas->get_vendedores();
        //$data['rows_preventas']=$this->ModeloCatalogos->getselectwheren('venta_erp',array('sucursal'=>$this->sucursal,'preventa'=>1,'activo'=>1));
        //$data['rows_cotizacion']=$this->ModeloCatalogos->getselectwheren('cotizacion',array('sucursal'=>$this->sucursal,'preventa'=>0,'activo'=>1));
        $data['rows_cotizacion']=$this->ModeloCatalogos->listacotizaciones($this->sucursal,$this->fecha_reciente);
        $data['estadorow']=$this->ModeloCatalogos->getselect_tabla('estado');
        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $data['get_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $data['get_ban']=$this->General_model->get_select('bancos',array('activo'=>1));

        $data['tanques']=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>$this->sucursal,'tipo'=>0,'estatus'=>1));
        //$data['rentas']=$this->ModeloRentas->getRentasPents($this->sucursal);
        $data['colonia']='';
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ventasp/form');

        $this->load->view('templates/footer');
        $this->load->view('ventasp/formjs');
        //$this->load->view('ventasp/modalp');
    }
    function load_preventas(){
        $pre = $this->input->post('pre');
        //$rows_preventas=$this->ModeloCatalogos->getselectwheren('venta_erp',array('sucursal'=>$this->sucursal,'preventa'=>1,'activo'=>1));
        $strq ="SELECT * FROM `venta_erp` WHERE sucursal='$this->sucursal' AND preventa=1 AND activo=1 AND reg BETWEEN '$this->fecha_reciente 00:00:00' AND '$this->fecha_reciente 23:59:59'";
        $rows_preventas = $this->db->query($strq);
        $html='';
        $html.='<option value="0">Preventas</option>';
            foreach ($rows_preventas->result() as $item_pv) {
                $sel="";
                if($pre==$item_pv->id){
                    $sel="selected";
                }
                $html.='<option '.$sel.' value="'.$item_pv->id.'">'.$item_pv->folio.'</option>';
            }
        echo $html;
    }
    
    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getclienterazonsociallike($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductos_ventas_like($pro,$this->sucursal);
        //echo $results;
        echo json_encode($results->result());
    }
    function searchservicios(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchservicios_ventas_like($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchInsumo(){
        $pro = $this->input->get('search');
        $id_suc = 7; //solo puede traer insumos de almacen rentas el empleado tecnico
        $results=$this->ModeloCatalogos->searchInsumo_ventas_like($pro,$id_suc);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchproductosSolicita(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductos_solicita_like($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function searchRecargas(){
        $pro = $this->input->get('search');
        $pro = mb_strtoupper($pro);
        $results=$this->ModeloCatalogos->searchRecargas_like($pro);
        echo json_encode($results->result());
    }

    function searchRentas(){
        $pro = $this->input->get('search');
        $pro = mb_strtoupper($pro);
        $results=$this->ModeloCatalogos->searchRentas_like($pro);
        echo json_encode($results->result());
    }

    function searchproductos_serie(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductos_ventas_series_like($pro);
        echo json_encode($results->result());
    }

    function getRentas(){
        $id_cli = $this->input->post('id_cli');
        $results=$this->ModeloRentas->getRentasPents($id_cli,$this->sucursal);
        echo json_encode($results);
    }

    function getMttosCli(){
        $id_cli = $this->input->post('id_cli');
        $results=$this->ModeloMttos->getMttosPents($id_cli);
        echo json_encode($results);
    }

    function insercliente(){
        $params=$this->input->post();

        $insercli['nombre']=$params['razon_social'];
        $insercli['celular']='';
        $insercli['codigo']='';
        $insercli['correo']=$params['correo'];
        $insercli['direccion_predeterminada']=0;
        $insercli['calle']='';
        $insercli['cp']=$params['cp'];
        $insercli['colonia']='';
        $insercli['telefono']='';
        $insercli['instrucciones']='';
        $insercli['pais']='';
        $insercli['user']=0;

        $idcliente=$this->ModeloCatalogos->Insert('clientes',$insercli);
        $params['idcliente']=$idcliente;
        $this->ModeloCatalogos->Insert('cliente_fiscales',$params);
        echo $idcliente;
    }

    function obtenerdatosproducto(){
        $params = $this->input->post();
        $bar=$params['barcode'];
        $result = $this->ModelProductos->obtenerdatosproducto_venta($bar);
        $id=0;
        foreach ($result->result() as $item) {
            $id=$item->id;
        }
        echo $id;
    }

    function addproducto(){
        $params = $this->input->post();
        $prod=$params['id'];
        $tipo=$params['tipo'];
        $tipo_prod=$params['tipo_prod']; //1=series, 2=lote y cad.
        $id_prod_suc=$params['id_prod_suc'];
        $id_venta=$params['id_venta'];
        //$cant=1;
        $des=0;
        $desp=0;
        $validar=0;
        //log_message('error','prod: '.$prod);
        //log_message('error','tipo: '.$tipo);
        //log_message('error','tipo_prod: '.$tipo_prod);
        //log_message('error','id_prod_suc: '.$id_prod_suc);
        //log_message('error','id_venta: '.$id_venta);
        
        if($tipo==0){ //producto normal        
            $result = $this->ModelProductos->obtenerdatosproductoid($prod,$this->sucursal,$tipo_prod,$id_prod_suc);
            foreach ($result->result() as $item){
                $id = $item->id;
                $idProducto = $item->idProducto;
                $nombre = $item->nombre;
                $idprod_serie = 0;
                $idprod_lote = 0; 
                $stock = $item->stock;
                $traslado_stock_cant=$item->traslado_stock_cant;
                $precio_con_iva = $item->precio;
                $incluye_iva = $item->incluye_iva;
                $iva = $item->iva;
                //log_message('error','iva: '.$iva);
                //log_message('error','incluye_iva: '.$incluye_iva);
                //log_message('error','traslado_stock_cant: '.$traslado_stock_cant);
                if($item->idprod_lote==0){
                    $stock=$stock-$traslado_stock_cant;
                }else{
                    $stock=$item->lote_stock-$traslado_stock_cant;
                }
                $precio_con_ivax=0;
                if($item->incluye_iva==1){
                    $precio_con_ivax=round($item->precio,2);
                    if($iva==0){
                        $incluye_iva=0;
                        $precio_con_ivax=round($item->precio,2);
                        //log_message('error','precio_con_ivax: '.$precio_con_ivax);
                    }else{
                        $precio_con_ivax=round($item->precio*1.16,2);
                        //$precio_con_ivax=round($item->precio/1.16,2);
                        $sub_iva=round($item->precio*.16,2);
                        //log_message('error','sub_iva: '.$sub_iva);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $precio_con_ivax=round($item->precio/$siva,2);
                    }else{
                        $precio_con_ivax=round($item->precio,2);  
                    }
                    $incluye_iva = 0;
                    $iva =0;
                }
                //log_message('error','precio_con_ivax2: '.$precio_con_ivax);
                if($tipo_prod==1){
                    $nombre = $item->nombre." ".$item->serie;
                    $idprod_serie = intval($item->idprod_serie);
                    $stock=$item->cant_serie;
                }if($tipo_prod==2){
                    $nombre = $item->nombre." ".$item->lote;
                    $idprod_lote = intval($item->idprod_lote); 
                    $stock=$item->cant_lote;
                }
                if($item->idprod_lote==0 ){
                    $stock=$stock-$traslado_stock_cant;
                }else{
                    $stock=$item->lote_stock-$traslado_stock_cant;
                }
                $cant_disp=0;
                $cant_kit=0;
                $no_disp=0;
                //log_message('error','concentrador: '.$item->concentrador);
                if($item->concentrador==1){
                    $getpc=$this->Modeloventas->getProductosConcen();
                    foreach ($getpc as $pc) {
                        $resp=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$pc->id_prod,'sucursalid'=>$this->sucursal,'activo'=>1));
                        foreach ($resp->result() as $p) {
                            $cant_disp=$p->stock;
                        }
                        $cant_kit = $pc->cantidad;
                        if($cant_disp < $cant_kit){
                            $no_disp++;
                        }
                        //log_message('error','cant_kit: '.$cant_kit);
                        //log_message('error','cant_disp: '.$cant_disp);
                    }
                }
                //log_message('error','no_disp: '.$no_disp);
                //log_message('error','id_prod_suc: '.$id_prod_suc);
                //log_message('error','idprod_lote: '.$idprod_lote);
                //log_message('error','idprod_lote: '.$idprod_lote);
                //============================================ quitar el iva a los productos
                if($no_disp>0){
                    $validar=3;
                }
                //log_message('error','cant: '.$cant);
                //log_message('error','id_prod_suc: '.$id_prod_suc);
                if($id_venta>0){
                    $idprod_det=0;
                    if($tipo_prod==1){
                        $idprod_det = $idprod_serie;
                    }
                    if($tipo_prod==2){
                        $idprod_det = $idprod_lote;
                    }
                    $result_vd= $this->Modeloventas->getProdVentaId($prod,$id_venta,$tipo,$id_prod_suc,$tipo_prod,$idprod_det);
                    if($result_vd->num_rows()>0){
                        $result_vd=$result_vd->row();
                        $cant=$result_vd->cantidad;
                    }else{
                        $cant=1;
                    }
                }else{
                    $cant=1;
                }

                //============================================
                $oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'precio_con_iva'=>$precio_con_iva,'stock'=>$stock,'incluye_iva'=>$incluye_iva,'precio_con_ivax'=>$precio_con_ivax,"tipo"=>$tipo,"capac"=>0,"tipo_prod"=>$tipo_prod,"id_prod_suc"=>$id_prod_suc,"idprod_serie"=>$idprod_serie,"idprod_lote"=>$idprod_lote);
                //log_message('error','oProducto: '.json_encode($oProducto));
                if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                    //log_message('error','existe producto');
                    $idx = array_search($oProducto, $_SESSION['pro']);
                    //log_message('error','existe producto idx'.$idx);
                    $cant_pre = $_SESSION['can'][$idx];
                    //log_message('error','cant_pre: '.$cant_pre);
                    //log_message('error','stock: '.$stock);
                    $cant_pre=$cant_pre+1;
                    if(intval($cant_pre)<=intval($stock) && $no_disp==0){
                        $_SESSION['can'][$idx]+=$cant;
                    }    
                }else{ //sino lo agrega
                    //log_message('error','crea nuevo producto');
                    if($no_disp==0){
                        array_push($_SESSION['pro'],$oProducto);
                        array_push($_SESSION['can'],$cant);
                        array_push($_SESSION['des'],$des);
                        array_push($_SESSION['desp'],$desp);
                        if(isset($_SESSION['id_prod_suc'])){
                            array_push($_SESSION['id_prod_suc'],$id_prod_suc);
                        }
                        if(isset($_SESSION['idprod_serie'])){
                            array_push($_SESSION['idprod_serie'],$idprod_serie);
                        }
                        if(isset($_SESSION['idprod_lote'])){
                            array_push($_SESSION['idprod_lote'],$idprod_lote);
                        }
                        if(isset($_SESSION['precio_con_iva'])){
                            array_push($_SESSION['precio_con_iva'],$precio_con_iva);
                            array_push($_SESSION['stock'],$stock);
                            array_push($_SESSION['incluye_iva'],$incluye_iva);
                            array_push($_SESSION['precio_con_ivax'],$precio_con_ivax);
                            array_push($_SESSION['tipo'],$tipo);
                            array_push($_SESSION['tipo_prod'],$tipo_prod);
                            array_push($_SESSION['capac'],0);
                        }
                    }else{
                        $validar=3;
                    }
                }   
                $validar=1;
            }
            if($no_disp>0){
                $validar=3;
            }
        }if($tipo==1){ //recarga de oxígeno
            $result = $this->ModelProductos->obtenerDatosRecargaVenta($prod,$this->sucursal);
            foreach ($result->result() as $item){
                $id = $item->id;
                $idProducto = $item->id;
                $nombre = $item->codigo;
                $stock = $item->stockpadre;
                $precio_con_iva = number_format($item->preciov,2);
                $incluye_iva = 0; //0=no, 1=si
                $capac=$item->capacidad;

                if($capac<=$stock){
                    //$precio_con_ivax=0;
                    $precio_con_ivax=$precio_con_iva;
                    /*if($incluye_iva==1){
                        $precio_con_ivax=round($precio_con_iva,2);
                    }else{
                        $siva=1.16;
                        $precio_con_ivax=round($precio_con_iva/$siva,2);
                    }*/
                    $precio_con_ivax=$precio_con_iva;
                    //============================================ quitar el iva a los productos
                    if($id_venta>0){
                        $idprod_det=0;
                        $result_vd= $this->Modeloventas->getProdVentaId($prod,$id_venta,$tipo,$id_prod_suc,$tipo_prod,$idprod_det);
                        if($result_vd->num_rows()>0){
                            $result_vd=$result_vd->row();
                            $cant=$result_vd->cantidad;
                        }else{
                            $cant=1;
                        }
                    }else{
                        $cant=1;
                    }
                    //============================================
                    $oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'precio_con_iva'=>$precio_con_iva,'stock'=>$stock,'incluye_iva'=>$incluye_iva,'precio_con_ivax'=>$precio_con_ivax,"tipo"=>$tipo,"capac"=>$capac,"tipo_prod"=>0,"id_prod_suc"=>$id_prod_suc,"idprod_serie"=>0,"idprod_lote"=>0);

                    if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                        //log_message('error','existe producto');
                        $idx = array_search($oProducto, $_SESSION['pro']);
                        //log_message('error','existe producto idx'.$idx);
                        $_SESSION['can'][$idx]+=$cant;
                        //$_SESSION['can'][$idx]=1;
                    }else{ //sino lo agrega
                        //log_message('error','crea nuevo producto');
                        array_push($_SESSION['pro'],$oProducto);
                        array_push($_SESSION['can'],$cant);
                        array_push($_SESSION['des'],$des);
                        array_push($_SESSION['desp'],$desp);
                        if(isset($_SESSION['precio_con_iva'])){
                            array_push($_SESSION['precio_con_iva'],$precio_con_iva);
                            array_push($_SESSION['stock'],$stock);
                            array_push($_SESSION['incluye_iva'],$incluye_iva);
                            array_push($_SESSION['precio_con_ivax'],$precio_con_ivax);
                            array_push($_SESSION['tipo'],$tipo);
                            array_push($_SESSION['tipo_prod'],$tipo_prod);
                            array_push($_SESSION['capac'],$capac);
                            array_push($_SESSION['id_prod_suc'],$id_prod_suc);
                            array_push($_SESSION['idprod_serie'],$idprod_serie);
                            array_push($_SESSION['idprod_lote'],$idprod_lote);
                        }
                    } 
                    $validar=1;
                }else{
                    $validar=2;
                }
            }
        }
        echo json_encode(array("validar"=>$validar,"tipo"=>$tipo));
    }
    function addservicios(){
        $params = $this->input->post();
        $id=$params['id'];
        $id_venta=$params['id_venta'];
        $des=0;
        $cant=1;
        $precio=0; $stock=1000;
        //log_message('error','id: '.$id);
        $result=$this->ModeloCatalogos->getselectwheren('cat_servicios',array('id'=>$id));
        foreach ($result->result() as $item) {
            $precio=$item->precio_siva;
            if($precio==0 && $id_venta!=0){
                $getpsrv=$this->ModeloCatalogos->getselectwheren("venta_erp_detalle",array("idproducto"=>$id,"tipo"=>3));
                $getpsrv=$getpsrv->row();
                $precio = $getpsrv->precio_unitario;
            }
            $oServicio=array("id"=>$item->id,"numero"=>$item->numero,"descripcion"=>$item->descripcion,'precio_siva'=>$precio,'incluye_iva'=>$item->iva,"id_prod_suc"=>0,"idprod_serie"=>0,"idprod_lote"=>0);
            //log_message('error','oServicio: '.json_encode($oServicio));
            if(in_array($oServicio, $_SESSION['ser'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                //log_message('error','existe producto');
                $idx = array_search($oServicio, $_SESSION['ser']);
                //log_message('error','existe producto idx'.$idx);
                $cant_pre = $_SESSION['can_ser'][$idx];
                //log_message('error','cant_pre: '.$cant_pre);
                //log_message('error','stock: '.$stock);
                $cant_pre=$cant_pre+1;
                if(intval($cant_pre)<=intval($stock)){
                    $_SESSION['can_ser'][$idx]+=$cant;
                }
            }else{ //sino lo agrega
                //log_message('error','crea nuevo producto');
                array_push($_SESSION['ser'],$oServicio);
                array_push($_SESSION['can_ser'],$cant);
                array_push($_SESSION['des_ser'],$des);
                array_push($_SESSION['precio_ser'],$precio);
            }   
        }
    }
    /* ///////////////////////////////// */
    function addInsumos_servicios(){
        $datos = $this->input->post();
        $prods=$datos['array_ins'];
        unset($datos["array_ins"]);
        $DATA = json_decode($prods);
        for ($i=0;$i<count($DATA);$i++) {   
            $id_ins=$DATA[$i]->id_ins;
            $id_ps=$DATA[$i]->id_ps; 
            $cant_ins=$DATA[$i]->cant_ins;
            $tipo=$DATA[$i]->tipo;
            $id_serv=$DATA[$i]->id_serv;
            $stock=$DATA[$i]->stock;

            //traer descripcion y stock
            $getprod=$this->General_model->getselectwhereall('productos',array('id'=>$id_ins));
            foreach ($getprod as $p) {
                $descrip = $p->nombre;
            }
            $oInsumo=array("id"=>$id_ins,"descripcion"=>$descrip,'precio_siva'=>0,'incluye_iva'=>0,"id_serv"=>$id_serv,"id_prod_suc"=>$id_ps,"stock"=>$stock);
            //log_message('error','oInsumo: '.json_encode($oInsumo));
            if(in_array($oInsumo, $_SESSION['insumo'])){  // si el producto ya se encuentra en la lista suma las cantidades
                $idd = array_search($oInsumo, $_SESSION['insumo']);

                /*unset($_SESSION['insumo'][$idd]);
                $_SESSION['insumo']=array_values($_SESSION['insumo']);

                unset($_SESSION['cant_ins'][$idd]);
                $_SESSION['cant_ins']=array_values($_SESSION['cant_ins']);

                unset($_SESSION['id_serv'][$idd]);
                $_SESSION['id_serv']=array_values($_SESSION['id_serv']);

                unset($_SESSION['precio_ins'][$idd]);
                $_SESSION['precio_ins']=array_values($_SESSION['precio_ins']);*/
            }else{ //sino lo agrega
                //log_message('error','crea nuevo producto');
                array_push($_SESSION['insumo'],$oInsumo);
                array_push($_SESSION['cant_ins'],$cant_ins);
                array_push($_SESSION['id_serv'],$id_serv);
                array_push($_SESSION['precio_ins'],0);
            } 
        }
    }

    function view_insumos_servs(){
        //====================================
        $count = 0;
        $html='';
        foreach ($_SESSION['insumo'] as $fila){
            $Cantidad=$_SESSION['cant_ins'][$count]; 
            $id_serv=$_SESSION['id_serv'][$count];
            $precio_ins=$_SESSION['precio_ins'][$count];

            $id_prod_suc=$fila['id_prod_suc'];
            if($fila['stock']==0){
                $stocktxt='<span class="badge m-l-10" style="background:red; border-radius: 15px; font-size: 18px;"><b >0</b></span>';
            }else{
                $stocktxt=$fila['stock'];
            }
            $html.='<tr class="producto_'.$count.'_'.$fila['tipo_prod'].'">
                        <td>
                            <input type="hidden" name="count" id="count" value="'.$count.'">
                            <input type="hidden" name="vsproid" id="vsproid" value="'.$fila['id'].'">
                            <input type="hidden" name="id_serv" id="id_serv" value="'.$fila['id_serv'].'">
                            <input type="hidden" name="id_prod_suc" id="id_prod_suc" value="'.$fila['id_prod_suc'].'">
                            <input type="hidden" class="vstipo_'.$count.'" name="vstipo" id="vstipo" value="3_1">
                            <input type="number" '.$dis.' name="vscanti" id="vscanti" class="vscanti vscanti_'.$count.'" value="'.$Cantidad.'">
                        </td>
                        <td>'.$Cantidad.'</td>
                        <td>'.$fila['descripcion'].'</td>
                    </tr>';   
            $count++;
        }
        echo $html;
    }
    ////////////////////////////////////////////////////

    function viewproductos(){
        //====================================
        $count = 0;
        $html='';
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            $des=$_SESSION['des'][$count];
            $desp=$_SESSION['desp'][$count];
            $incluye_iva=$fila['incluye_iva'];
            $precio=$fila['precio_con_iva'];
            if($incluye_iva==1){

            }
            $preciox=$fila['precio_con_ivax'];
            $id_prod_suc=$fila['id_prod_suc'];
            $idprod_serie=$fila['idprod_serie'];
            $idprod_lote=$fila['idprod_lote'];   
            if(isset($_SESSION['incluye_iva'][$count])){
                $incluye_iva=$_SESSION['incluye_iva'][$count];
            }if(isset($_SESSION['tipo'][$count])){
                $tipo=$_SESSION['tipo'][$count];
            }if(isset($_SESSION['capac'][$count])){
                $capac=$_SESSION['capac'][$count];
            }if(isset($_SESSION['precio_con_iva'][$count])){
                $precio=$_SESSION['precio_con_iva'][$count];
            }if(isset($_SESSION['precio_con_ivax'][$count])){
                $preciox=$_SESSION['precio_con_ivax'][$count];
            }
            if(isset($_SESSION['tipo_prod'][$count])){
                $tipo_prod=$_SESSION['tipo_prod'][$count];
            }

            $descuento=0;
            if($desp>0){
                $descuento_l=$desp.'%';
                $descuento=round(($Cantidad*$precio)*($desp/100), 2);
            }else{
                $descuento_l='$ '.$des;
                $descuento=$des;
            }
            $cantotal=($Cantidad*$fila['precio_con_ivax'])-$descuento;
            $stocktxt='';
            if($fila['stock']==0){
                $stocktxt='<span class="badge m-l-10" style="background:red; border-radius: 15px; font-size: 18px;"><b >0</b></span>';
            }else{
                $stocktxt=$fila['stock'];
            }
            //log_message('error','tipo producto: '.$fila['tipo_prod']);
            if($fila['tipo_prod']==1){ //series
                $dis="disabled";
            }
            else{
                $dis="";
            }
            //============================================
                $res_p = $this->ModeloCatalogos->getselectwheren('productos',array('id'=>$fila['id']));
                $uni_sat='';
                $ser_sat='';
                $concentrador='';
                $tanque='';
                $capacidad='';
                foreach ($res_p->result() as $itemp) {
                    if($itemp->unidad_sat!=null && $itemp->unidad_sat!='null' ){
                        $uni_sat=$itemp->unidad_sat;
                    }
                    if($itemp->servicioId_sat!=null && $itemp->servicioId_sat!='null' ){
                        $ser_sat=$itemp->servicioId_sat;
                    }
                    $concentrador=$itemp->concentrador;
                    $tanque=$itemp->tanque;
                    $capacidad=$itemp->capacidad;
                }
                if($fila["tipo"]==1){ //recarga
                    $uni_sat="H87"; //VALIDAR SEMIT - LIX XOCHITL - SI LA UNIDAD ES CORRECTA
                    $ser_sat="12141904";
                }
                if($des>0){
                    if($incluye_iva==1){
                        //$fila['precio_con_ivax']=round(($precio-$des)*1.16,2);
                        $precio_iva_des=round(($precio-$des)*1.16,2);
                        $cantotal=($Cantidad*$precio_iva_des);
                    }
                }
            //============================================
            $html.='<tr class="producto_'.$count.'_'.$fila['tipo_prod'].' info_pro_'.$fila['id'].'_'.$fila['tipo_prod'].' validar_sat" data-uni="'.$uni_sat.'" data-ser="'.$ser_sat.'">
                    <td>
                        <input type="hidden" name="count" id="count" value="'.$count.'">
                        <input type="hidden" name="vsproid" id="vsproid" value="'.$fila['id'].'">
                        <input type="hidden" name="vsproid_ps_lot" id="vsproid_ps_lot" value="'.$fila['idprod_lote'].'">
                        <input type="hidden" name="vsproid_ps_ser" id="vsproid_ps_ser" value="'.$fila['idprod_serie'].'">
                        <input type="hidden" class="vstipo_'.$count.'" name="vstipo" id="vstipo" value="'.$fila['tipo'].'">
                        <input type="hidden" id="concentrador" value="'.$concentrador.'">
                        <input type="hidden" id="tanque" value="'.$tanque.'">
                        <input type="hidden" id="capacidad" value="'.$capacidad.'">
                        <input type="hidden" class="vscapac_'.$count.'" name="vscapac" id="vscapac" value="'.$fila['capac'].'">
                        <input type="number" '.$dis.' name="vscanti" id="vscanti" class="vscanti vscanti_'.$count.'" value="'.$_SESSION['can'][$count].'" onchange="editarcantidad('.$count.','.$fila['stock'].')">
                    </td>
                    <td class="name_pros">'.$fila['idProducto'].' / '.$fila['nombre'].'</td>
                    <td><input type="hidden" class="stockn_'.$count.'" name="stockn" id="stockn" value="'.$fila['stock'].'">'.$stocktxt.'</td>
                    <td><div class="btn-group" style="display: flex; align-items: center;">$<input type="hidden" name="incluye_iva" id="incluye_iva" value="'.$fila['incluye_iva'].'" readonly style="background: transparent;border: 0px;width: 100px;"><input type="text" name="vsprecio" id="vsprecio" class="vsprecio"  value="'.$precio.'" readonly style="background: transparent;border: 0px;width: 100px;"></div></td>
                    <td>$'.number_format($fila['precio_con_ivax'],2).'</td>';
                    
                    $html.='<td style="display:none">';
                        $html.='<input type="text" name="vsdescuento" id="vsdescuento" class="vsdescuento" value="'.$descuento.'" readonly style="display:none">';
                            $html.=$descuento_l;
                        $html.='<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="validar_descuento('.$count.',0)">%</a>';
                        
                    $html.='</td>';

                    $html.='<td>';
                        $html.='<div class="btn-group" style="display: flex; align-items: center;float:left">$ ';
                            $html.='<input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px; width: 100px;">';
                        $html.='</div>';
                        $html.='<a class="btn btn-desc" onclick="sol_pro_desc('.$fila['id'].','.$fila['tipo_prod'].')"></a>';
                        $html.='<span clas="mont_des">';
                            if($des>0){
                                $html.='Desc: $'.$des;
                            }
                        $html.='</span>';
                    $html.='</td>';
                    $html.='<td>';
                        $html.='<a onclick="deletepro('.$count.','.$fila['tipo_prod'].')" class="btn"><i class="fa fa-trash"></i></a>';
                    $html.='</td>';
            $html.='</tr>';   
            $count++;
            //<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="adddescuento('.$count.',1)">$</a>
        }
        $html.=$this->viewservicio();
        echo $html;
    }
    
    function deleteproducto(){
        $idd = $this->input->post('id');
        $tipo = $this->input->post('tipo');
        //log_message('error','delete row: '.$idd);
        //log_message('error','tipo row: '.$tipo);
        if($tipo!=3 && $tipo!=4){ //!= de servicios
            unset($_SESSION['pro'][$idd]);
            $_SESSION['pro']=array_values($_SESSION['pro']);

            unset($_SESSION['can'][$idd]);
            $_SESSION['can'] = array_values($_SESSION['can']);
            
            unset($_SESSION['des'][$idd]);
            $_SESSION['des'] = array_values($_SESSION['des']);
            
            unset($_SESSION['desp'][$idd]);
            $_SESSION['desp'] = array_values($_SESSION['desp']); 

            unset($_SESSION['id_prod_suc'][$idd]);
            $_SESSION['id_prod_suc']=array_values($_SESSION['id_prod_suc']);
            unset($_SESSION['idprod_lote'][$idd]);
            $_SESSION['idprod_lote']=array_values($_SESSION['idprod_lote']);
            unset($_SESSION['idprod_serie'][$idd]);
            $_SESSION['idprod_serie']=array_values($_SESSION['idprod_serie']);
        }
        if($tipo==3){ //servicios
            unset($_SESSION['ser'][$idd]);
            $_SESSION['ser']=array_values($_SESSION['ser']);

            unset($_SESSION['can_ser'][$idd]);
            $_SESSION['can_ser']=array_values($_SESSION['can_ser']);

            unset($_SESSION['des_ser'][$idd]);
            $_SESSION['des_ser']=array_values($_SESSION['des_ser']);

            unset($_SESSION['precio_ser'][$idd]);
            $_SESSION['precio_ser']=array_values($_SESSION['precio_ser']);
        }
        if($tipo==4){ //rentas
            unset($_SESSION['costr'][$idd]);
            $_SESSION['costr']=array_values($_SESSION['costr']);

            unset($_SESSION['despr'][$idd]);
            $_SESSION['despr'] = array_values($_SESSION['despr']);
            
            unset($_SESSION['desr'][$idd]);
            $_SESSION['desr'] = array_values($_SESSION['desr']);
            
            unset($_SESSION['cantr'][$idd]);
            $_SESSION['cantr'] = array_values($_SESSION['cantr']); 

            unset($_SESSION['rentr'][$idd]);
            $_SESSION['rentr'] = array_values($_SESSION['rentr']); 
            //log_message('error',json_encode($_SESSION['rentr']));

            unset($_SESSION['id_prod_suc'][$idd]);
            $_SESSION['id_prod_suc']=array_values($_SESSION['id_prod_suc']);
            unset($_SESSION['idprod_lote'][$idd]);
            $_SESSION['idprod_lote']=array_values($_SESSION['idprod_lote']);
            unset($_SESSION['idprod_serie'][$idd]);
            $_SESSION['idprod_serie']=array_values($_SESSION['idprod_serie']);
        
        }
    }/*
    function adddescuento(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        $des=$params['des'];
        if($tipo==0){//porcentaje
            if($des>100){
                $des=100;
            }
            if($des<0){
                $des=0;
            }
            $_SESSION['desp'][$id]=$des;
        }
        if($tipo==1){//monto
            if($des<0){
                $des=0;
            }
            $_SESSION['des'][$id]=$des;
        }
    }*/
    function limpiartabla(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();

        unset($_SESSION['costr']);
        $_SESSION['costr']=array();
        unset($_SESSION['despr']);
        $_SESSION['despr']=array();

        unset($_SESSION['desr']);
        $_SESSION['desr']=array();
        unset($_SESSION['cantr']);
        $_SESSION['cantr']=array();
        unset($_SESSION['rentr']);
        $_SESSION['rentr']=array();

        if(isset($_SESSION['tipo'])){
            unset($_SESSION['tipo']);
        }
        if(isset($_SESSION['tipo_prod'])){
            $_SESSION['tipo_prod']=array();
        }
        if(isset($_SESSION['capac'])){
            unset($_SESSION['capac']);
        }

        unset($_SESSION['id_prod_suc']);
        $_SESSION['id_prod_suc']=array();
        unset($_SESSION['idprod_lote']);
        $_SESSION['idprod_lote']=array();
        unset($_SESSION['idprod_serie']);
        $_SESSION['idprod_serie']=array();

        unset($_SESSION['ser']);
        $_SESSION['ser']=array();
        unset($_SESSION['can_ser']);
        $_SESSION['can_ser']=array();
        unset($_SESSION['des_ser']);
        $_SESSION['des_ser']=array();
        unset($_SESSION['precio_ser']);
        $_SESSION['precio_ser']=array();
    }
    function savefactura(){
        $params=$this->input->post();
        $productos = $params['productos'];
        $productosr = $params['productosr'];
        $montosp = $params['montosp'];
        $vf=$params['vf'];
        $facturar=0;
        $viewventa=$params['viewventa'];
        $preventa=$params['preventa'];
        $total=$params['total'];
        $tipo_venta=$params['tipo_venta'];
        $proserv = $params['proserv'];
        $insumos = $params['insumos'];
        $id_mtto = $params['id_mtto'];
        $id_renta = $params['id_renta'];

        unset($params['productos']);
        unset($params['montosp']);
        unset($params['proserv']);
        unset($params['insumos']);
        unset($params['id_mtto']);
        unset($params['id_renta']);
        
        if($params['validarpin']==1){
            $idvendedor=$params['idvendedor'];
        }else{
            $idvendedor=$this->idpersonal;
        }
        
        $DATApr = json_decode($productosr);
        $DATAp = json_decode($productos);
        $DATAm = json_decode($montosp);
        $DATAps = json_decode($proserv);
        $DATAins = json_decode($insumos);

        $dataeqarrayinsert_p=array();
        $dataeqarrayinsert_m=array();
        $dataeqarrayinsert_s=array();
        $folio_con = $this->Modeloventas->obtener_folio_v($this->sucursal);

        $this->db->trans_start();

        $folio=$this->succlave.str_pad($this->idpersonal, 3, "0", STR_PAD_LEFT).str_pad($folio_con, 5, "0", STR_PAD_LEFT);
        if($viewventa==0){
            $insertventa=array(
                            'sucursal'=>$this->sucursal,
                            'folio'=>$folio,
                            'folio_con'=>$folio_con,
                            'id_razonsocial'=>$params['id_razonsocial'],
                            'id_cliente'=>$params['id_cliente'],
                            'subtotal'=>$params['subtotal'],
                            'descuento'=>$params['descuento'],
                            'iva'=>$params['iva'],
                            'total'=>$params['total'],
                            'observaciones'=>$params['observaciones'],
                            'facturar'=>$params['vf'],
                            'reg'=>$this->fechahoy,
                            'preventa'=>$preventa,
                            'personalid'=>$idvendedor,
                            'tipo_venta'=>$tipo_venta,
                        );
            $idventa=$this->ModeloCatalogos->Insert('venta_erp',$insertventa);
            $resultcli=$this->General_model->getselectwhere('cliente_fiscales','id',$params['id_razonsocial']);
            $idcli=0;
            
            foreach ($resultcli as $x){
                $idcli=$x->idcliente;
            }

            if($idcli!=0){
                $this->Modeloventas->sumar_cliente_saldo($idcli,$total);
            }            
        }else{
            $this->ModeloCatalogos->getdeletewheren('venta_erp_detalle',array('idventa'=>$viewventa));
            $update_venta=array(
                            'id_razonsocial'=>$params['id_razonsocial'],
                            'id_cliente'=>$params['id_cliente'],
                            'subtotal'=>$params['subtotal'],
                            'descuento'=>$params['descuento'],
                            'iva'=>$params['iva'],
                            'total'=>$params['total'],
                            'preventa'=>$preventa,
                            'tipo_venta'=>$params['tipo_venta'],
                            'observaciones'=>$params['observaciones'],
                        );
            $this->ModeloCatalogos->updateCatalogo('venta_erp',$update_venta,array('id'=>$viewventa));
            $idventa=$viewventa;
        }
        for ($i=0;$i<count($DATAp);$i++) { //venta de productos (check venta productos)
            $cant_inicial=0;
            $insert_venta_dll=array(
                'idventa'=>$idventa,
                'idproducto'=>$DATAp[$i]->pro,
                'cantidad'=>$DATAp[$i]->cant,
                'precio_unitario'=>$DATAp[$i]->cunit,
                'descuento'=>$DATAp[$i]->desc,
                'incluye_iva'=>$DATAp[$i]->incluye_iva,
                'tipo'=>$DATAp[$i]->tipo,
                'id_ps_lote'=>$DATAp[$i]->id_ps_lot,
                'id_ps_serie'=>$DATAp[$i]->id_ps_ser
            );
            
            //log_message('error','dataeqarrayinsert_p: '.json_encode($dataeqarrayinsert_p));
            if($preventa==0){ //se vende la nota
                //=============================aqui debera de ir el descuento de inventario de acuerdo a la sucursal cuando ya se tenga ese modulo
                if($DATAp[$i]->tipo==0 && $DATAp[$i]->id_ps_lot==0 && $DATAp[$i]->id_ps_ser==0){ //tipo 0 = producto(no recarga)
                    //=============================================
                        $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATAp[$i]->pro,'sucursalid'=>$this->sucursal,'activo'=>1));
                        foreach ($res_info1->result() as $itemi) {
                            $cant_inicial=$itemi->stock;
                        }
                        $cant_final=$cant_inicial-$DATAp[$i]->cant;
                        $insert_venta_dll['cant_inicial']   = $cant_inicial;
                        $insert_venta_dll['cant_final']     = $cant_final;
                    //=============================================
                    $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','-',$DATAp[$i]->cant,'productoid',$DATAp[$i]->pro,'sucursalid',$this->sucursal,'activo',1);
                }
                if($DATAp[$i]->tipo==0 && $DATAp[$i]->id_ps_lot!=0){ //producto de lote y caducidad
                     //==================================================
                        $cant_inicial=$this->ModeloCatalogos->obtener_stock_lotes($DATAp[$i]->pro,$this->sucursal);
                        
                        $cant_final=$cant_inicial-$DATAp[$i]->cant;
                        $insert_venta_dll['cant_inicial']   = $cant_inicial;
                        $insert_venta_dll['cant_final']     = $cant_final;
                    //==================================================
                    $this->ModeloCatalogos->updatestock('productos_sucursales_lote','cantidad','-',$DATAp[$i]->cant,'id',$DATAp[$i]->id_ps_lot);
                }
                if($DATAp[$i]->tipo==0 && $DATAp[$i]->id_ps_ser!=0){ //producto de serie
                    //==================================================
                        //$cant_inicial=$this->ModeloCatalogos->obtener_stock_series($DATAp[$i]->pro,$this->sucursal);
                        $cant_inicial=1;
                        $cant_final=$cant_inicial-$DATAp[$i]->cant;
                        $insert_venta_dll['cant_inicial']   = $cant_inicial;
                        $insert_venta_dll['cant_final']     = $cant_final;
                    //==================================================
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array('disponible'=>0,"vendido"=>1),array('id'=>$DATAp[$i]->id_ps_ser));
                }
                if($DATAp[$i]->tipo==1){ //tipo 1 = recarga
                    //=====================================================
                        $res_info1=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>0,'sucursal'=>$this->sucursal,'estatus'=>1));
                        foreach ($res_info1->result() as $itemi) {
                            $cant_inicial=$itemi->stock;
                        }
                        $cant_final=$cant_inicial+$DATAp[$i]->capac;
                        $insert_venta_dll['cant_inicial']   = $cant_inicial;
                        $insert_venta_dll['cant_final']     = $cant_final;
                    //=====================================================
                    $this->ModeloCatalogos->updatestock3('recargas','stock','-',$DATAp[$i]->capac,'tipo',0,'sucursal',$this->sucursal,'estatus',1);
                }
                //asignar para los servicios porque cae error de insert Array
                if($DATAp[$i]->tipo==3){
                    $insert_venta_dll['cant_inicial']   = 1;
                    $insert_venta_dll['cant_final']     = 1;
                }

                if(isset($DATAp[$i]->concentrador) && $DATAp[$i]->concentrador=="1"){
                    $getpc=$this->Modeloventas->getProductosConcen();
                    foreach ($getpc as $p) {
                        $cant_inicial=0;
                        $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$p->id_prod,'sucursalid'=>$this->sucursal,'activo'=>1));
                        foreach ($res_info1->result() as $itemi) {
                            $cant_inicial=$itemi->stock;
                        }
                        $array_bit=array(
                                        'id_prod'=>$p->id_prod,
                                        'id_venta'=>$idventa,
                                        'cantidad'=>$p->cantidad,
                                        'cant_ini'=>$cant_inicial
                                    );
                        $this->ModeloCatalogos->Insert('bitacora_prods_conce',$array_bit); //nva tabla de bitacora de prods_concentra_salida
                        /****************************************/
                        $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','-',$p->cantidad,'productoid',$p->id_prod,'sucursalid',$this->sucursal,'activo',1);
                    }
                }
                if(isset($DATAp[$i]->tanque) && $DATAp[$i]->tanque=="1"){
                    //=====================================================
                        $res_info1=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>0,'sucursal'=>$this->sucursal,'estatus'=>1));
                        foreach ($res_info1->result() as $itemi) {
                            $cant_inicial=$itemi->stock;
                            $id_rec=$itemi->id;
                            $suc_rec=$itemi->sucursal;
                        }
                        $array_bit=array(
                                        'id_recarga'=>$id_rec,
                                        'id_venta'=>$idventa,
                                        'cantidad'=>$DATAp[$i]->capacidad,
                                        'cant_ini'=>$cant_inicial
                                    );
                        $this->ModeloCatalogos->Insert('bitacora_oxi_tanque',$array_bit); //nva tabla de bitacora de bitacora_oxi_tanque
                    //=====================================================
                    $this->ModeloCatalogos->updatestock3('recargas','stock','-',$DATAp[$i]->capacidad,'tipo',0,'sucursal',$this->sucursal,'estatus',1);
                }
                //=============================
            }
            $dataeqarrayinsert_p[]=$insert_venta_dll;
        }

        for ($j=0;$j<count($DATApr);$j++) {
            $idserie=$DATApr[$j]->idserie;
            $renta_tab=$DATApr[$j]->id_renta;
            //ya se cambia estatus disponible desde modulo renta al asignar serie
            /*if($idserie>0){
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array('disponible'=>0),array('id'=>$idserie));
            }*/
            if($preventa==1){ //es preventa
                $up_renta=array('id_venta'=>$idventa);
            }else{
                $up_renta=array('id_venta'=>$idventa,"pagado"=>1,"estatus"=>1);
            }
            $this->ModeloCatalogos->updateCatalogo('rentas',$up_renta,array('id'=>$renta_tab));
            $insert_venta_dll=array(
                'idventa'=>$idventa,
                'idproducto'=>$DATApr[$j]->pro,
                'cantidad'=>$DATApr[$j]->cant,
                'precio_unitario'=>round($DATApr[$j]->cunit/1.16,5),
                'descuento'=>0,
                'incluye_iva'=>1,
                'tipo'=>2,
                'id_serv'=>$DATApr[$j]->id_serv,
                'id_ps_serie'=>$idserie,
                'periodo'=>$DATApr[$j]->periodo
            );
            //$dataeqarrayinsert_p[]=$insert_venta_dll;
            $this->ModeloCatalogos->Insert('venta_erp_detalle',$insert_venta_dll);
            //log_message('error','dataeqarrayinsert_p de rentas: '.json_encode($dataeqarrayinsert_p));
        }

        if(isset($id_renta) && $id_renta>0){
            $this->ModeloCatalogos->updateCatalogo('rentas',array('id_venta'=>$idventa),array('id'=>$id_renta));
        }

        for ($l=0;$l<count($DATAps);$l++) { //servicios
            $insert_venta_dll_ser=array(
                'idventa'=>$idventa,
                'idproducto'=>$DATAps[$l]->pro,
                'cantidad'=>$DATAps[$l]->cant,
                'precio_unitario'=>$DATAps[$l]->cunit,
                'descuento'=>$DATAps[$l]->desc,
                'incluye_iva'=>$DATAps[$l]->incluye_iva,
                'tipo'=>3,
                'id_ps_lote'=>0,
                'id_ps_serie'=>0
            );
            $dataeqarrayinsert_p[]=$insert_venta_dll_ser;
        }
        //log_message('error','dataeqarrayinsert_p: '.count($dataeqarrayinsert_p));
        if(count($dataeqarrayinsert_p)>0){
            $this->ModeloCatalogos->insert_batch('venta_erp_detalle',$dataeqarrayinsert_p);
        }

        if(isset($id_mtto) && $id_mtto>0){
            $this->ModeloCatalogos->updateCatalogo('mtto_externo',array('id_venta'=>$idventa),array('id'=>$id_mtto));
        }
        ///////////////////insumos del servicio/////////////////////////
        $data_array_ins=array();
        $asignado=0;
        if($preventa==0){
            $asignado=1;
        }
        for ($l=0;$l<count($DATAins);$l++) {
            $cant_inicial=0;
            $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATAins[$l]->id_ins,'sucursalid'=>$this->sucursal,'activo'=>1));
            foreach ($res_info1->result() as $itemi) {
                $cant_inicial=$itemi->stock;
            }

            $insert_bita_dll=array(
                'id_venta'=>$idventa,
                'id_mtto'=>$id_mtto,
                'id_insumo'=>$DATAins[$l]->id_ins,
                'id_prod_suc'=>$DATAins[$l]->id_ps,
                'cantidad'=>$DATAins[$l]->cant_ins,
                //'tipo'=>$DATAins[$l]->tipo,
                'id_serv'=>$DATAins[$l]->id_serv,
                'asignado'=>$asignado,
                'cant_ini'=>$cant_inicial
            );
            $data_array_ins[]=$insert_bita_dll;
            //editar insumo utlizado en servicios
            if($preventa==0){ //una venta como tal
                $this->Modeloventas->restaInsumo_sucursal($DATAins[$l]->id_ps,$DATAins[$l]->cant_ins);
            }
            if($DATAins[$l]->id==0){//agregar la cantidad inicial
                $this->ModeloCatalogos->Insert('bitacora_ins_servs_ventas',$insert_bita_dll);
            }else{
                unset($insert_bita_dll["cant_ini"]);
                $this->ModeloCatalogos->updateCatalogo('bitacora_ins_servs_ventas',$insert_bita_dll,array('id'=>$DATAins[$l]->id));
            }
        }

        /*if(count($data_array_ins)>0){
            $this->ModeloCatalogos->insert_batch('bitacora_ins_servs_ventas',$data_array_ins);
        }*/
        //////////////////////////////////////////

        for ($j=0;$j<count($DATAm);$j++) {
            if($DATAm[$j]->monto>0){ // solo se debe de insertar si hay un monto mayor a cero
                $insert_venta_dll=array(
                    'idventa'=>$idventa,
                    'formapago '=>$DATAm[$j]->tipo,
                    'monto'=>$DATAm[$j]->monto,
                    'banco'=>$DATAm[$j]->banco,
                    'ultimosdigitos'=>$DATAm[$j]->ultimosdigitos
                );
                $dataeqarrayinsert_m[]=$insert_venta_dll;
            }
        }
        if(count($dataeqarrayinsert_m)>0){
            $this->ModeloCatalogos->insert_batch('venta_erp_formaspagos',$dataeqarrayinsert_m);
        }
        $this->db->trans_complete();

        if(count($DATAm)>0){
            if($vf==1){
                $facturar=1;
            }
        }
        if($tipo_venta==1){
            $facturar=1;
        }
        $this->limpiartabla();
        //========================================= limpia la solicitudes de descuento 
            $this->limpiarsolicitudes_des();
        //=========================================
        //$this->deleteproducto();

        $array = array(
                'idventa' =>$idventa,
                'folio' =>$folio,
                'facturar'=>$facturar
            );
        echo json_encode($array);
    }
    function editarcantidad(){
        $params = $this->input->post();
        $row=$params['row'];
        $cantidad=$params['cantidad'];
        $_SESSION['can'][$row]=$cantidad;
    }
    function get_validar_razon_social(){
        $razon_social = $this->input->post('razon_social');
        //$result=$this->General_model->getselectwhere('cliente_fiscales','razon_social',$razon_social);
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('razon_social'=>$razon_social,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    function get_validar_rfc(){
        $rfc = $this->input->post('rfc');
        $id = $this->input->post('id');
        //$result=$this->General_model->getselectwhere('cliente_fiscales','rfc',$rfc);
        $result=$this->General_model->getselectwhereall('cliente_fiscales',array('rfc'=>$rfc,"activo"=>1,"idcliente != "=>$id));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    function set_abrir_turno(){
        $monto = $this->input->post('monto');
        $validar=0;
        if($this->perfilid==1 || $num_p=$this->perfilid==2){
            //$result=$this->General_model->getselectwhereall('turnos',array('sucursal'=>$this->sucursal,"estatus"=>2,"fecha"=>date("Y-m-d")));
            $resultado=0;
            //foreach ($result as $row) {
              //  $resultado=1;
            //}
            if($resultado==0){
                $validar=1;
                $data = array('idpersonal'=>$this->idpersonal,
                    'fecha'=>$this->fecha_reciente,
                    'hora'=>$this->horahoy_normal,
                    'estatus'=>1,
                    'monto'=>$monto,
                    'sucursal'=>$this->sucursal
                 );
                $idcliente=$this->ModeloCatalogos->Insert('turnos',$data);
            }else{
                $validar=2;
            }
        } 
        echo $validar;
    }
    function set_cerrar_turno()    {   
        $id=0;
        $results_turno=$this->General_model->turnos_limit1($this->idpersonal); 
        foreach ($results_turno as $x) {
            $id=$x->id;
        }
        $data = array('idpersonal_c'=>$this->idpersonal,
            'fecha_c'=>$this->fecha_reciente,
            'hora_c'=>$this->horahoy_normal,
            'estatus'=>2,
        );
        $this->General_model->edit_record('id',$id,$data,'turnos');
        $this->limpiartabla();
    }
    function get_tabla_verificador()    {    
        $codigo=$this->input->post('codigo');
        $tipo=$this->input->post('tipo');
        $prod=$this->input->post('prod');
        $tipo_prod=$this->input->post('tipo_prod');
        //log_message('error','tipo: '.$tipo);
        //log_message('error','prod: '.$prod);
        /*if($tipo==1 && $prod==0 && $tipo_prod==0){
            $get_productos=$this->Modeloventas->get_productos_stock_suc($codigo,1);
        }if($tipo==2 && $prod==0 && $tipo_prod==0){
            $get_productos=$this->Modeloventas->get_productos_stock_suc($codigo,0);
        }
        if($prod==1){
            $get_productos=$this->Modeloventas->get_tanques_stock_suc();
        }
        if($prod==0 && $tipo_prod==1){ //prod de serie
            $get_productos=$this->Modeloventas->get_productos_stock_serie($codigo,0);
        }
        if($prod==0 && $tipo_prod==2){ //prod de lote
            $get_productos=$this->Modeloventas->get_productos_stock_lote($codigo,0);
        }*/
        
        $tr_sucs=""; $btntrans=""; //${'sucu'.intval($this->sucursal)}='style="background:#93ba1f;"';
        $html='<table class="table table-sm" id="table_datos">
                <thead>
                    <tr style="background: #009bdb;">
                        <th scope="col" style="color: white !important;  font-size: 12px;"></th>
                        <th scope="col" style="color: white; font-size: 12px;">Nombre/Descripción</th>';
                        $cont_suc=0;
                        $sucs=$this->General_model->getselectwhere_orden_asc('sucursal',array('activo'=>1),"orden");
                        foreach($sucs as $s){
                            $html.='<th scope="col" style="color: white; font-size: 12px;">'.$s->id_alias.' - '.$s->name_suc.'</th>';
                            ${'sucu'.intval($s->id)}="";
                            if($s->id==$this->sucursal){// almacen general
                                ${'sucu'.intval($s->id)}='style="background:#93ba1f;"'; 
                                $transsuc=$s->name_suc;
                            }
                            if($prod==1){ //recarga
                                $get_productos=$this->Modeloventas->get_tanques_stock_suc2($s->id);
                            }
                            if($prod==0 && $tipo_prod==0){ //stock
                                $get_productos=$this->ModelProductos->getStockSucursal2($codigo,$s->id);
                            }if($prod==0 && $tipo_prod==1){ //serie
                                $get_productos=$this->ModelProductos->getSerieSucursal2($codigo,$s->id);   
                            }if($prod==0 && $tipo_prod==2){ //lote
                                $get_productos=$this->ModelProductos->getLoteSucursal2($codigo,$s->id);
                            }
                            $cont_dsuc=0; $stock=0;
                            foreach($get_productos as $x){
                                $cont_dsuc++;
                                if($x->stock==0){
                                    ${'stocktxt'.intval($x->id_suc)}='<span class="badge m-l-10" style="background:red; border-radius: 15px; font-size: 18px;"><b >0</b></span>';
                                    ${'dis'.intval($x->id_suc)}="disabled";
                                    $tr_sucs.='<td></td>';
                                }else{
                                    $stock=$x->stock;
                                    if($prod==0 && $tipo_prod==0){
                                        $stock=$stock-$x->traslado_stock_cant;
                                    }if($prod==1){ //recarga
                                        $stock=$x->stock-$x->traslado_stock_cant;
                                    }
                                    ${'stocktxt'.intval($x->id_suc)}=$stock;
                                    ${'dis'.intval($x->id_suc)}="";
                                    $tr_sucs.='<td '.${'sucu'.intval($x->id_suc)}.'>'.${'stocktxt'.intval($x->id_suc)}.'</td>';
                                }
                                
                                if($prod==1){ //recarga 
                                    if($this->sucursal!=$x->id_suc && $x->id_suc==8){
                                        $btntrans.='<td><button '.${'dis'.intval($x->id_suc)}.' type="button" class="btn realizarventa btn-sm" onclick="trasnpaso_suc('.$x->id_suc.','.$stock.','.$x->id.','.$prod.')">Solicitar traspaso</button></td>';
                                    }else{
                                        $btntrans.='<td></td>';
                                    } 
                                }else{
                                    if($this->sucursal!=$x->id_suc && $x->id_suc!=6 && $x->id_suc!=7){
                                        $btntrans.='<td><button '.${'dis'.intval($x->id_suc)}.' type="button" class="btn realizarventa btn-sm" onclick="trasnpaso_suc('.$x->id_suc.','.$stock.','.$x->id.','.$prod.')">Solicitar traspaso</button></td>';
                                    }else{
                                        $btntrans.='<td></td>';
                                    } 
                                }
                                if($prod==0){
                                    $nombre=$x->nombre;
                                }else{
                                    $nombre=$x->codigo."- Recarga de oxígeno";
                                }
                                
                            }
                            if($cont_dsuc==0){
                                $tr_sucs.='<td>0</td>'; 
                                $btntrans.='<td></td>';
                            }
                        }
                        $img=''; 
                        if($prod==0){
                            $result=$this->General_model->get_productos_files($x->id);
                            foreach ($result as $z){
                                $img='<img style="width: 100px" src="'.base_url().'uploads/productos/'.$z->file.'"';    
                            }
                        } 
                        $html.='<th scope="col" style="color: white; font-size: 12px;">Precio</th>
                        <!--<th scope="col" style="color: white; font-size: 12px;">Acciones</th>-->
                    </tr>
                </thead>
                <tbody>';                        
                    //foreach($get_productos as $x){
                        //log_message('error','tr_sucs:'.$tr_sucs);
                        /*$img=''; 
                        if($prod==0){
                            $result=$this->General_model->get_productos_files($x->id);
                            foreach ($result as $z){
                                $img='<img style="width: 100px" src="'.base_url().'uploads/productos/'.$z->file.'"';    
                            }
                        } */                                       
                        $html.='<tr>
                            <td>'.$img.'</td>
                            <td>'.$nombre.'</td>
                            '.$tr_sucs.'
                            <td><a class="btn" onclick="modal_precios('.$x->id.')"><i class="fa fa-eye"></i></a></td>
                            <!--<td>
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" id="check_idproducto" onclick="check_producto('.$x->id.')">
                                        <span style="border: 2px solid #009bdb;"></span>
                                    </label>
                                </span>
                            </td>-->
                        </tr>';
                        $html.='<tr>
                            <td></td>
                            <td></td>
                            '.$btntrans.'
                            <td></td>
                            <td></td>
                        </tr>';
                    //}
                $html.='</tbody>
            </table>';
            $html.='<div class="ocultar_transpaso" style="display:none">
            <div class="row">
                <div class="col-lg-12">
                    <label class="form-label">¿Cantidad de piezas que desea traspasar de <span class="suc_tras"></span> a '.$transsuc.'?</label>
                </div>
                <div class="col-lg-4">
                    <input class="form-control" type="number" id="cantidad_trans">
                </div>
                <div class="col-lg-4">
                    <a type="button" class="btn realizarventa btn-sm" onclick="transferir_cantidad()">Solicitar traspaso</a>
                </div>
            </div>';
        echo $html;
    }
    function get_data_cliente(){
        $id=$this->input->post('id');
        $nombre='';
        $usuario='';
        $correo='';
        $contrasena='';
        $contrasena2='';
        $direccion='';
        $celular="";
        $calle='';
        $cp='';
        $colonia='';

        $razon_social='';
        $rfc='';
        $cp='';
        $RegimenFiscalReceptor='';
        $uso_cfdi='';
        $cpy='';
        $saldo='';
        $desactivar=0;
        $motivo='';
        $estado = '';
        $municipio = '';
        $diascredito='';
        $dias_saldo='';
        $diaspago='';
        $saldopago='';
        $clave='';
        $resul=$this->General_model->getselectwhere('clientes','id',$id);
        foreach ($resul as $item) {
            $nombre=$item->nombre;
            $usuario=$item->usuario;
            $correo=$item->correo;
            $contrasena='x1f3[5]7w78{';     
            $contrasena2='x1f3[5]7w78{';

            $calle=$item->calle;
            $cp=$item->cp;
            $colonia=$item->colonia;  
            $saldo=$item->saldo;  
            $desactivar=$item->desactivar;  
            $motivo=$item->motivo;  
            $estado = $item->estado;
            $municipio = $item->municipio;
            $diascredito = $item->diascredito;
            $dias_saldo = $item->dias_saldo;
            $diaspago = $item->diaspago;
            $saldopago = $item->saldopago;
            $celular = $item->celular;
            $resuls=$this->General_model->getselectwhere('sucursal','id',$item->sucursal);
            foreach ($resuls as $s){
                $clave=$s->clave;
            }
        }

        $razon_social='';
        $rfc='';
        $cpy='';
        $RegimenFiscalReceptor='';
        $uso_cfdi='';
        
        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        foreach ($resulf as $itemf) {
            $direccion=$itemf->direccion;
            $razon_social=$itemf->razon_social;
            $rfc=$itemf->rfc;
            $cpy=$itemf->cp;
            $RegimenFiscalReceptor=$itemf->RegimenFiscalReceptor;
            $uso_cfdi=$itemf->uso_cfdi;
        }
        $estadorow=$this->ModeloCatalogos->getselect_tabla('estado');
        $get_regimen=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $get_cfdi=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $html='<ul class="nav nav-tabs" id="icon-tab" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="icon-home-tabx" data-bs-toggle="tab" href="#icon-homex" role="tab" aria-controls="icon-home" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>Datos generales</a></li>
              <li class="nav-item"><a class="nav-link" id="profile-icon-tabx" data-bs-toggle="tab" href="#profile-iconx" role="tab" aria-controls="profile-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Datos fiscales</a></li>
              <li class="nav-item"><a class="nav-link" id="pagos-icon-tabx" data-bs-toggle="tab"'; if($this->session->userdata('perfilid')==1) $html.='href="#pagos-iconx"'; else $html.='href="#checkTab(3)" style="cursor: no-drop;'; $html.='role="tab" aria-controls="pagos-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Condiciones de pago</a></li>
              <li class="nav-item"><a class="nav-link" id="programa-icon-tab" data-bs-toggle="tab"'; if($this->session->userdata('perfilid')==1) $html.='href="#programa-iconx"'; else $html.='href="#checkTab(4)" style="cursor: no-drop; '; $html.='role="tab" aria-controls="programa-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Programa cliente frecuente</a></li>
              <li class="nav-item"><a class="nav-link" id="contact-icon-tabx" data-bs-toggle="tab"'; if($this->session->userdata('perfilid')==1) $html.='href="#contact-iconx"'; else $html.='href="#checkTab(5)" style="cursor: no-drop;'; $html.='role="tab" aria-controls="contact-icon" aria-selected="false"><i class="icofont icofont-contacts"></i>Sesión en pagína web</a></li>
            </ul>
            <div class="tab-content" id="icon-tabContent">
              <div class="tab-pane fade show active" id="icon-homex" role="tabpanel" aria-labelledby="icon-home-tabx">
                <br>
                <form id="form_registro_datosx" method="post">
                    <input type="hidden" id="idreg1x" name="id" value="'.$id.'">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nombre del cliente<span style="color: red">*</span> </label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="nombre" id="razon_socialx" value="'.$nombre.'">
                                </div>
                            </div>
                        </div>

                    </div>    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Teléfono</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="celular" id="celular" value="'.$celular.'">
                                </div>   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Correo electrónico<span style="color: red">*</span> </label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="correo" id="correo" autocomplete="nope" oninput="verificar_correo()" value="'.$correo.'">
                                </div>   
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Calle y número</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="calle" value="'.$calle.'">
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Código postal</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="cp" id="codigo_postalx" oninput="cambiaCP2x()" value="'.$cp.'">
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Colonia<a onclick="modal_alert()"><img style="width: 18px;" src="'.base_url().'images/spotlight-svgrepo-com.svg"></a></label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="coloniax" name="colonia">
                                        <option value="0" selected disabled>Seleccione</option>';
                                        if($colonia!=''){
                                            $html.='<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Saldo</label>
                                <div class="col-lg-5"><input class="form-control" type="number" name="saldo" id="saldo" value="'.$saldo.'">
                                </div>   
                            </div>
                        </div>';
                        
                        $html.='<div class="col-lg-12">';
                            if($this->session->userdata('perfilid')==1){
                                $html.='<div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Suspender</label>
                                    <div class="col-lg-5">
                                        <span class="switch switch-icon">
                                            <label>';
                                            $desactivartxt='';
                                            if($desactivar==1){
                                                $desactivartxt='checked';
                                            }
                                                $html.='<input type="checkbox" name="desactivar" id="desactivarx" '.$desactivartxt.' onclick="activar_campox()">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>   
                                </div>';
                            } else {
                                $html.='<div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Suspender</label>
                                    <div class="col-lg-5">
                                        <span class="switch switch-icon">
                                            <label>
                                                <input disabled type="checkbox" name="desactivar" id="desactivarx">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>   
                                </div>';
                            } 
                        $html.='</div>';

                            $motivo_sty='';
                            if($desactivar==1){
                                $motivo_sty='style="display: block;"';
                            }else{
                                $motivo_sty='style="display: none;"';
                            }
                    
                        $html.='<div class="col-lg-12">
                            <div class="motivo_txt" '.$motivo_sty.'>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Motivo</label>
                                    <div class="col-lg-5">
                                        <textarea class="form-control" rows="3" name="motivo">'.$motivo.'</textarea>
                                    </div>   
                                </div>
                            </div>    
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Estado</label>
                                <div class="col-lg-5">
                                    <select name="estado" id="estado" class="form-control">';
                                      foreach ($estadorow->result() as $item){ 
                                        if($estado==$item->id){ 
                                            $html.='<option value="'.$item->id.'" selected>'.$item->estado.'</option>';
                                        }else{
                                            $html.='<option value="'.$item->id.'">'.$item->estado.'</option>';
                                        }
                                      
                                       } 
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Municipio</label>
                                <div class="col-lg-5"><input class="form-control" type="text" name="municipio" value="'.$municipio.'">
                                </div>   
                            </div>
                        </div>
                    </div>
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datosx()">Guardar</button>                      
              </div>
              <div class="tab-pane fade" id="profile-iconx" role="tabpanel" aria-labelledby="profile-icon-tabx">
                <br>
                <form id="form_registro_datos_fiscalesx" method="post">
                    <input type="hidden" id="idreg2x" name="id" value="'.$id.'">

                    <div class="row">
                        <div class="col-md-6">
                            <label class="form-label">Activar datos fiscales</label>
                            <span class="switch switch-icon">
                            <label>
                                <input type="checkbox" name="activar_datos_fiscales" id="activar_datos_fiscalesx" >
                                <span></span>
                            </label>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Razón social</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text"  name="razon_social" oninput="validar_razon_socialx()" value="'.$razon_social.'">
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">RFC</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="rfc" id="rfcx" oninput="validar_rfc_clientex()" value="'.$rfc.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">CP</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="text" name="cp"  value="'.$cpy.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Régimen fiscal</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="RegimenFiscalReceptorx" name="RegimenFiscalReceptor" onchange="regimefiscalx()">
                                        <option value="0" selected disabled>Seleccione</option>';
                                        foreach ($get_regimen->result() as $item) {
                                            if($item->clave==$RegimenFiscalReceptor){
                                                $html.='<option value="'.$item->clave.'" selected>'.$item->clave.' '.$item->descripcion.'</option>';
                                            }else{
                                                $html.='<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
                                            } 
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Uso de CFDI</label>
                                <div class="col-lg-5">
                                    <select class="form-control" data-cliuso="'.$uso_cfdi.'" id="uso_cfdix" name="uso_cfdi">
                                        <option value="0" >Seleccione</option>';
                                        foreach ($get_cfdi->result() as $item) {
                                            if($item->uso_cfdi==$uso_cfdi){
                                                $html.='<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'"  selected>'.$item->uso_cfdi_text.'</option>';
                                            }else{
                                                $html.='<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" disabled>'.$item->uso_cfdi_text.'</option>'; 
                                            }
                                        }
                                    $html.='</select>
                                </div>   
                            </div>
                        </div>
                    </div>
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_datos_fiscalesx()">Guardar</button>        
              </div>
              <div class="tab-pane fade" id="pagos-iconx" role="tabpanel" aria-labelledby="pagos-icon-tabx">
                <br>
                <form id="form_registro_pagosx" method="post">
                    <input type="hidden" id="idreg4x" name="id" value="'.$id.'">
                    <div class="row">
                        <!-- <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Número de días de crédito</label>
                                <div class="col-lg-5">
                                    <input class="form-control" type="number" id="diascredito" name="diascredito" value="<?php // echo $diascredito ?>">
                                </div>
                            </div>
                        </div> -->';
                    
                            $dias_saldo1='style="display:none"'; 
                            $dias_saldo2='style="display:none"'; 
                            if($dias_saldo==1){
                                $dias_saldo1='style="display:block"'; 
                                $dias_saldo2='style="display:none"'; 
                            }else if($dias_saldo==2){
                                $dias_saldo1='style="display:none"'; 
                                $dias_saldo2='style="display:block"'; 
                            }
                
                        $html.='<div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En días</label>
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>';
                                        $dias_saldotxt='';
                                        if($dias_saldo==1){
                                            $dias_saldotxt='checked';
                                        }
                                            $html.='<input type="radio" name="dias_saldo" id="dias_saldo" value="1" onclick="tipo_pago(1)" '.$dias_saldotxt.'>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>   
                                <div class="col-lg-2 diaspagotxt" '.$dias_saldo1.'>
                                    <select class="form-control" id="diaspago" name="diaspago" required>
                                        <option value="0" selected disabled>Seleccionar días</option>
                                        <option value="15"';if($diaspago==15){$html.='selected';}$html.='>15</option>
                                        <option value="30"';if($diaspago==30){$html.='selected';}$html.='>30</option>
                                    </select>
                                </div> 
                                <label class="col-lg-2 col-form-label" style="color: #019cde;font-weight: bold;">En saldo</label>
                                <div class="col-lg-1">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="radio" name="dias_saldo" id="dias_saldo" value="2" onclick="tipo_pago(2)"';if($dias_saldo==2){$html.='checked';}$html.='>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>  
                                <div class="col-lg-2 saldopagotxt" '.$dias_saldo2.'> 
                                    <input class="form-control" type="number" id="saldopago" name="saldopago" value="'.$saldopago.'">
                                </div>  
                            </div>
                        </div>
                    </div>    
                </form>  
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_pagosx()">Guardar</button>        
              </div>
              <div class="tab-pane fade" id="programa-iconx" role="tabpanel" aria-labelledby="programa-icon-tab">
                <br>
                <form id="form_registro_usuario" method="post">
                    <input type="hidden" id="idreg3" name="id" value="<?php echo $id ?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">ID cliente</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="text" disabled value="'.$clave.$id.'">
                                </div>   
                            </div>
                        </div> 
                    </div>    
                </form>
              </div>  
              <div class="tab-pane fade" id="contact-iconx" role="tabpanel" aria-labelledby="contact-icon-tabx">
                <br>
                <form id="form_registro_usuariox" method="post">
                    <input type="hidden" id="idreg3x" name="id" value="'.$id.'">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Nombre de usuario</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="text" name="usuario" id="usuariox" autocomplete="nope" oninput="verificar_usuariox()" value="'.$usuario.'">
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Contraseña</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="contrasenax" id="contrasenax" autocomplete="new-password" oninput="" value="'.$contrasena.'">
                                </div>  
                                <div class="col-5">
                                    <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                        <i class="icon-xl fas fa-eye" style="color: white"></i>
                                    </a>
                                </div> 
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Confirmar contraseña</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="contrasena2x" id="contrasena2x" autocomplete="new-password" value="'.$contrasena2.'">
                                </div>   
                            </div>
                        </div> 
                    </div>    
                </form>
                <button class="btn btn-primary btn_registro" type="button" onclick="guardar_registro_usuariox()">Guardar</button>
              </div>  
            </div>';
        echo $html;
    }
    /*function get_data_cliente()
    {
        $id=$this->input->post('id');
        $nombre='';
        $correo='';
        $direccion='';
        $razon_social='';
        $rfc='';
        $cp='';
        $clave='';
        $uso_cfdi='';
       
        $get_regimen=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $get_cfdi=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        
        $resultc=$this->General_model->get_clientes($id);
        foreach ($resultc as $x){
            $nombre=$x->nombre;
            $correo=$x->correo;
            $direccion=$x->direccion;
            $razon_social=$x->razon_social;
            $rfc=$x->rfc;
            $cp=$x->cp;
            $clave=$x->RegimenFiscalReceptor;
            $uso_cfdi=$x->uso_cfdi;
        }
        $html='<form class="form" id="form_registro_datos_cliente_fiscales">
            <div class="row">
                <div class="col-md-6">
                    <input class="form-control" type="hidden" name="id" value="'.$id.'">
                    <label class="form-label">Nombre del cliente</label>
                    <input class="form-control" type="text" name="nombre" value="'.$nombre.'">
                </div>
                <div class="col-md-6">
                    <label class="form-label">Correo electrónico</label>
                    <input class="form-control" type="email" name="correo" value="'.$correo.'">
                </div>
                <div class="col-md-12">
                    <label class="form-label">Dirección</label>
                    <textarea class="form-control" name="direccion">'.$direccion.'</textarea>
                </div>
                <div class="col-md-6">
                    <label class="form-label">Razón social</label>
                    <input class="form-control" type="text" name="razon_social" value="'.$razon_social.'">
                </div>
                <div class="col-md-3">
                    <label class="form-label">RFC</label>
                    <input class="form-control" type="text" name="rfc" value="'.$rfc.'">
                </div>
                <div class="col-md-3">
                    <label class="form-label">CP</label>
                    <input class="form-control" type="text" name="cp" value="'.$cp.'">
                </div>
                <div class="col-md-6">
                    <label class="form-label">Régimen fiscal</label>
                    <select class="form-control" id="RegimenFiscalReceptorxx" name="RegimenFiscalReceptor" onchange="regimefiscalcf()" required>
                        <option value="0" selected disabled>Seleccione</option>';
                        foreach ($get_regimen->result() as $item) {
                            if($item->clave==$clave){
                                $html.='<option value="'.$item->clave.'" selected>'.$item->clave.' '.$item->descripcion.'</option>';    
                            }else{
                                $html.='<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';    
                            }
                            
                        }
                    $html.='</select>
                </div>
                <div class="col-md-6">
                    <label class="form-label">Uso de CFDI</label>
                    <select class="form-control" id="uso_cfdixx" name="uso_cfdi" required>
                        <option value="0" selected disabled>Seleccione</option>';
                        foreach ($get_cfdi->result() as $item) {
                            if($item->uso_cfdi==$uso_cfdi){
                                $html.='<option value="'.$item->uso_cfdi.'" selected class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" >'.$item->uso_cfdi_text.'</option>';
                            }else{
                                $html.='<option value="'.$item->uso_cfdi.'" class="option_'.$item->uso_cfdi.' pararf '.str_replace(',',' ',$item->pararf).'" >'.$item->uso_cfdi_text.'</option>';
                            }
                            
                        }
                    $html.='</select>
                </div>';
        $html.='</div><br></form>
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-primary" type="button" onclick="guardar_registro_datos_cliente()">Guardar</button>
            </div>
        </div>';
        echo $html;
    }*/
    public function registro_datos_cliente_edit(){
        $data=$this->input->post();
        $id=$data['id'];
        $datac['nombre']=$data['nombre'];
        $datac['correo']=$data['correo'];

        $dataf['direccion']=$data['direccion'];
        $dataf['razon_social']=rtrim($data['razon_social']);//rtrim para eliminar los espacios al final
        $dataf['rfc']=$data['rfc'];
        $dataf['cp']=$data['cp'];
        $dataf['RegimenFiscalReceptor']=$data['RegimenFiscalReceptor'];
        $dataf['uso_cfdi']=$data['uso_cfdi'];

        $this->General_model->edit_record('id',$id,$datac,'clientes');
        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        $idf=0;
        foreach ($resulf as $itemf) {
            $idf=$itemf->id;
        }
        if($idf==0){
            $this->General_model->add_record('cliente_fiscales',$dataf);
        }else{
            $this->General_model->edit_record('id',$idf,$dataf,'cliente_fiscales');
        }
    }
    function view_presv(){
        $params = $this->input->post();
        $idventa=$params['ventaid'];
        //log_message('error','venta:'.$idventa);
            $result_v= $this->ModeloCatalogos->infoventapre($idventa);
            //$result_vdll= $this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$idventa));
            $result_vdll= $this->Modeloventas->getVentasDetallePre($idventa);
        $array = array(
                        'venta'=>$result_v->row(),
                        'ventadll'=>$result_vdll
                        );
        echo json_encode($array);
    }
    function view_cotiza(){
        $params = $this->input->post();
        $idcotizacion=$params['idcotizacion'];
        //log_message('error','venta:'.$idcotizacion);
            $result_v= $this->ModeloCatalogos->infocotizacion($idcotizacion);
            //$result_vdll= $this->ModeloCatalogos->getselectwheren('cotizacion_detalle',array('idcotizacion'=>$idcotizacion));
            $result_vdll= $this->Modeloventas->getVentasDetalleCotiza($idcotizacion);
        $array = array(
                        'venta'=>$result_v->row(),
                        'ventadll'=>$result_vdll
                        );
        echo json_encode($array);
    }
    function update_cotizacion(){
        $id=$this->input->post('id');
        $data = array('preventa'=>1);
        $this->General_model->edit_record('id',$id,$data,'cotizacion');
    }
    function datosfiscales_view(){
        $params = $this->input->post();
        $id=$params['id'];
            $result_df= $this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$id));
        $array = array(
                        'df'=>$result_df->row()
                        );
        echo json_encode($array);
    }
    function ticket($idventa){
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();

        $configfac= $this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $data['configfac']=$configfac->row();

        $r_v= $this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$idventa));
        $r_v=$r_v->row();
        $data['r_v']=$r_v;

        $r_suc= $this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$r_v->sucursal));
        $r_suc=$r_suc->row();
        $data['r_suc']=$r_suc;

        $r_per= $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$r_v->personalid));
        $r_per=$r_per->row();
        $data['r_per']=$r_per;

        //log_message('error','razon_social: '.$r_v->id_razonsocial);

        $r_cli_f= $this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$r_v->id_razonsocial));
        $r_cli_f=$r_cli_f->row();
        $id_clien = $r_cli_f->idcliente; 
        if($r_cli_f->razon_social==""){
            if($r_v->id_cliente!=0){
                $r_cli_f= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$r_v->id_cliente));
                $r_cli_f=$r_cli_f->row();
                $data['r_cli_f_cliente']=$r_cli_f->nombre;
                $data["idcliente"]=$r_cli_f->id; 
            }else{
                $r_cli_f2= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$id_clien));
                $r_cli_f2=$r_cli_f2->row();
                $data['r_cli_f_cliente']=$r_cli_f2->nombre;
                $data["idcliente"]=$r_cli_f2->id; 
            }
        }else{
            $data['r_cli_f_cliente']=$r_cli_f->razon_social;
            $data["idcliente"]=$r_cli_f->idcliente;
        }
        
        //log_message('error','razon_social: '.$data['r_cli_f']->razon_social);

        //$data['reg'] = $r_v->reg;
        $data['fecha'] = date("d/m/Y", strtotime($r_v->reg));
        //$NuevaFecha = strtotime ( '-1 hour' , strtotime ($r_v->reg)) ; 
        $data['hora'] = date("h:i a", strtotime($r_v->reg));

        $data['r_vd'] = $this->Modeloventas->ventas_detalles($idventa);
        $data['s_vd'] = $this->Modeloventas->ventas_detalles_servicio($idventa);
        $data['renta_vd'] = $this->Modeloventas->ventas_detalles_renta($idventa);
        $data['total_letra'] =$this->Modelonumeroletra->ValorEnLetras($r_v->total);
        $data['r_vfp'] = $this->Modeloventas->ventas_formaspagos($idventa);
        $this->load->view('reportes/ticket',$data);
    }
    function viewprecios_list(){
        $id = $this->input->post('id');
        $suc_pro=$this->General_model->get_suc_prod($id);
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">Sucursal</th>
                    <th scope="col">Precio</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($suc_pro->result() as $x){
                $html.='<tr>
                <td>'.$x->id_alias.' - '.$x->name_suc.'</td>
                <td>$ '.number_format($x->precio,2,'.',',') .'</td>
                </tr>';
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }
    function transferir_cantidad_sucursal()
    {
        $idsucursal = $this->input->post('idsucursal');
        $cantidad = $this->input->post('cantidad');
        $idproducto = $this->input->post('idproducto');
        $tipo = $this->input->post('tipo'); //0=prod, 1=recarga

        $idsucursal_transferir=$this->sucursal;

        $datatra['idpersonal']=$this->idpersonal;
        $idtra=$this->General_model->add_record('traspasos',$datatra);
        $datainfo['idtranspasos']=$idtra;
        $datainfo['idsucursal_sale']=$idsucursal;
        $datainfo['idsucursal_entra']=$idsucursal_transferir;
        $datainfo['cantidad']=$cantidad;
        $datainfo['idproducto']=$idproducto;
        $datainfo['tipo']=$tipo;
        $datainfo['idpersonal']=$this->idpersonal;
        //var_dump($datainfo);
        $this->General_model->add_record('historial_transpasos',$datainfo);

        //$this->Modeloventas->sumar_sucursal($idproducto,$idsucursal_transferir,$cantidad);
        //$this->Modeloventas->resta_sucursal($idproducto,$idsucursal,$cantidad);
    }
    public function validar_codigo(){   
        $id=$this->input->post('reg');
        $codigox=$this->input->post('codigo');
        $result=$this->General_model->getselectwhere('contrasena','activo',1);
        $codigo=0;
        $id=0;
        foreach ($result as $x) {
            $codigo=$x->codigo;
            $id=$x->id;
        }
        $validarc=0;
        if($codigo==$codigox){
            $data = array('activo'=>0);
            $this->General_model->edit_record('id',$id,$data,'contrasena');
            $validarc=1;
        }else{
            $validarc=0;
        }
        echo $validarc; 
    }
    function add_alerta_traspasos(){   
        $sucursaldestino=$this->input->post('idsucursal');
        $tipo=$this->input->post('tipo');
        $data = array('idpersonal'=>$this->idpersonal,
            'sucursaldestino'=>$sucursaldestino,
            'sucursalorigen'=>$this->sucursal,
            'tipo'=>$tipo);
        $id=$this->General_model->add_record('historial_alerta',$data);
    }
    function get_validar_pin()
    {
        $input_pin=$this->input->post('input_pin');
        $idvendedor=$this->input->post('idvendedor');
        $result=$this->General_model->get_select('usuarios',array('personalId'=>$idvendedor));
        $pin_txt='';
        foreach ($result->result() as $x){
            $pin_txt=$x->pin;
        }
        $validar=0;
        if($pin_txt==$input_pin){
            $validar=1;
        }else{
            $validar=0;
        }
        echo $validar;
    }
    /// cliente
    function validar_correo(){
        $correo = $this->input->post('correo');
        $result=$this->General_model->getselectwhereall('clientes',array('correo'=>$correo,"activo"=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }
    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }
    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }
    public function registro_datos_cliente(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['direccion']);
        if(isset($data['desactivar'])){
            $data['desactivar']=1;
        }else{
            $data['desactivar']=0;
        }
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
            $datosf = array('idcliente'=>$id);
            $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
        }else{
            $this->General_model->edit_record('id',$id,$data,'clientes');
            $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
            $idf=0;
            foreach ($resulf as $itemf) {
                $idf=$itemf->id;
            }
            $datosf = array('idcliente'=>$id);
            if($idf==0){
                $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
            }else{
                $this->General_model->edit_record('id',$idf,$datosf,'cliente_fiscales');
            }
            $idf=$idf;
        }
        echo json_encode(array("id"=>$id,"idf"=>$idf));
    }
    function validar(){
        $usuario = $this->input->post('usuario');
        $result=$this->General_model->getselectwhere('clientes','usuario',$usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    public function registro_datos_fiscales(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);
        $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
        $idf=0;
        foreach ($resulf as $itemf) {
            $idf=$itemf->id;
        }

        if($idf==0){
            $this->General_model->add_record('cliente_fiscales',$data);
        }else{
            $this->General_model->edit_record('id',$idf,$data,'cliente_fiscales');
        }
        $id=$id;
        echo $id;
    }
    public function registro_datos_pago(){
        $data=$this->input->post();
        $id=$data['id'];
        $this->General_model->edit_record('id',$id,$data,'clientes');
        echo $id;
    }
    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];

        $pss_verifica = $data['contrasena'];
        $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
        $data['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($data['contrasena']);
        }

        unset($data['contrasena2']);
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'clientes');
            $id=$id;
        }
        echo $id;
    }
    public function registro_datosx(){
        $data=$this->input->post();
        $id=$data['id'];

        $pss_verifica = $data['contrasenax'];
        $pass = password_hash($data['contrasenax'], PASSWORD_BCRYPT);
        $data['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($data['contrasena']);
        }
        unset($data['contrasenax']);
        unset($data['contrasena2x']);
        

        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id,$data,'clientes');
            $id=$id;
        }
        echo $id;
    }
    function get_resumen_ventas_sucursal(){
        $f1=$this->input->post('f1');
        $f2=$this->input->post('f2');
        $get_result=$this->Modeloventas->get_resumenventa($this->sucursal,$f1,$f2);
        $html='<table class="table table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Folio</th>
                                <th scope="col">Vendedor</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Método de pago</th>
                                <th scope="col">Monto</th>
                            </tr>
                        </thead>
                        <tbody>';
                        foreach ($get_result as $x){
                            $html.='<tr>
                               <td>'.$x->folio.'</td>
                               <td>'.$x->nombre.'</td>
                               <td>'.$x->cliente.'</td>
                               <td><!--'.$x->formapago.' / '.$x->name_ban.'-->'.$x->bancosrow.'</td>
                               <td>'.$x->total.'</td>
                            </tr>';
                        }
                        $html.='</tbody>
                    </table>';
        echo $html;
    }
    function get_resumen_ventas_sucursal_tipos_pagos(){
        $f1=$this->input->post('f1');
        $f2=$this->input->post('f2');
        $get_result=$this->Modeloventas->get_resumenventa_tipopago($this->sucursal,$f1,$f2);
        $html='<table class="table table-sm">
                    <tbody>';
                    foreach ($get_result as $x){
                        if($x->formapago!=''){
                            $html.='<tr>
                               <td>'.$x->formapago.'</td>
                               <td><a type="button" class="btn realizarventa btn-sm">$'.number_format($x->total,2,'.',',').'</a></td>
                            </tr>';
                        }
                    }
                    $html.='</tbody>
                </table>';
        echo $html;
    }
    function get_resumen_ventas_sucursal_total(){
        $f1=$this->input->post('f1');
        $f2=$this->input->post('f2');
        $get_result=$this->Modeloventas->get_resumenventa_total($this->sucursal,$f1,$f2);
        $total=0;
        foreach ($get_result as $x){
            $total=$x->total;
        }
        echo $total;
    }
    function get_validar_pin_resumen(){
        $input_pin=$this->input->post('input_pin');
        $result=$this->General_model->get_select('usuarios',array('personalId'=>$this->idpersonal));
        $pin_txt='';
        foreach ($result->result() as $x){
            $pin_txt=$x->pin;
        }
        $validar=0;
        if($pin_txt==$input_pin){
            $validar=1;
        }else{
            $validar=0;
        }
        echo $validar;
    }
    function savecotizacion(){
        $params=$this->input->post();
        $productos = $params['productos'];

        $clausulas = $params['clausulas'];

        $vf=$params['vf'];
        $facturar=0;
        $viewventa=$params['viewventa'];
        $preventa=$params['preventa'];
        $total=$params['total'];
        unset($params['productos']);

        unset($params['productos']);

        if($params['validarpin']==1){
            $idvendedor=$params['idvendedor'];
        }else{
            $idvendedor=$this->idpersonal;
        }
        
        $DATAp = json_decode($productos);

        $DATAcl = json_decode($clausulas);

        $dataeqarrayinsert_p=array();
        $dataeqarrayinsert_m=array();
        $folio_con = $this->Modeloventas->obtener_folio_cotizacion($this->sucursal);

        $folio=$this->succlave.str_pad($this->idpersonal, 3, "0", STR_PAD_LEFT).str_pad($folio_con, 5, "0", STR_PAD_LEFT);
        $insertcla=array(
                        'sucursal'=>$this->sucursal,
                        'folio'=>$folio,
                        'folio_con'=>$folio_con,
                        'id_razonsocial'=>$params['id_razonsocial'],
                        'subtotal'=>$params['subtotal'],
                        'descuento'=>$params['descuento'],
                        'iva'=>$params['iva'],
                        'total'=>$params['total'],
                        'facturar'=>$params['vf'],
                        'reg'=>$this->fechahoy,
                        'preventa'=>$preventa,
                        'personalid'=>$idvendedor,
                        'tipo_venta'=>$params['tipo_venta'],
                        'correo'=>$params['correo_cl'],
                    );
        $idcotizacion=$this->ModeloCatalogos->Insert('cotizacion',$insertcla);   

        for ($i=0;$i<count($DATAp);$i++) {
            $insert_venta_dll=array(
                'idcotizacion'=>$idcotizacion,
                'tipo'=>$DATAp[$i]->tipo,
                'id_ps_lote'=>$DATAp[$i]->id_ps_lote,
                'id_ps_serie'=>$DATAp[$i]->id_ps_serie,
                'idproducto'=>$DATAp[$i]->pro,
                'cantidad'=>$DATAp[$i]->cant,
                'precio_unitario'=>$DATAp[$i]->cunit,
                'descuento'=>$DATAp[$i]->desc,
                'incluye_iva'=>$DATAp[$i]->incluye_iva,
            );
            $dataeqarrayinsert_p[]=$insert_venta_dll;
        }
        if(count($dataeqarrayinsert_p)>0){
            $this->ModeloCatalogos->insert_batch('cotizacion_detalle',$dataeqarrayinsert_p);
        }

        for ($i=0;$i<count($DATAcl);$i++) {
            $insert_clausulas_dll=array(
                'idcotizacion'=>$idcotizacion,
                'clausula'=>$DATAcl
                [$i]->clausula,
            );
            $dataeqarrayinsert_cl[]=$insert_clausulas_dll;
        }
        if(count($dataeqarrayinsert_cl)>0){
            $this->ModeloCatalogos->insert_batch('cotizacion_clausulas',$dataeqarrayinsert_cl);
        }

        $this->limpiartabla();
        echo $idcotizacion;
    }
    function enviar_aviso(){
        
        $idreg=$this->input->post('id');
        $correo=$this->input->post('correo');
        log_message('error','correo: '.$correo);
    
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            $this->load->library('email', NULL, 'ci_email');

            $config['protocol'] = 'smtp';
            $config["smtp_host"] ='mail.semit.mx'; 
            $config["smtp_user"] = 'cotizaciones@semit.mx';
            $config["smtp_pass"] = 'k@+K-cXb$z}0';
            $config["smtp_port"] = '465';
            $config["smtp_crypto"] = 'ssl';
            $config['charset'] = 'utf-8'; 
            $config['wordwrap'] = TRUE; 
            $config['validate'] = true;
            $config['mailtype'] = 'html';
             
            //Establecemos esta configuración
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('cotizaciones@semit.mx','Semit');
             
        //======================
        $this->email->to($correo);
        //$this->email->to('andres@mangoo.mx','Factura');

        $this->email->subject('Cotización    ');
        
/*        $message ="<div>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            <tr>
                              <th rowspan='14' style='background-color: #009bdb; color: #009bdb; -webkit-print-color-adjust: exact;'>..</th>
                              <th rowspan='14' style='background-color: #93ba1f; color: #93ba1f; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                                <th colspan='4' align='center'>
                                  <img src='".base_url()."public/img/SEMIT.jpg' style='width: 250px;'>
                                </th>
                            </tr>
                            <tr>
                              <th colspan='2' align='center'>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: black; -webkit-print-color-adjust: exact;' align='left'>
                                Cotización: ".$idreg."
                              </th>
                            </tr>
                          </thead>
                      </table>    
                      </div>
                  </div>";<span color:white>...</span>
                  */
        $message='<div>
                <div style="border-radius: 30px; background: #93ba1f; padding: 18px 1px 0px 45px; width: 17%; margin-left: auto; margin-right: auto; float: left;  box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                      <div style="background: white; border-top-left-radius: 30px;">
                        <div style="width: 100%; float: left;">
                            <div style="text-align:center">
                                <br>
                                <img src="'.base_url().'public/img/SEMIT.jpg" style="max-height: 100px;">
                                <br>
                                <h5 style="color:#152342; font-family: sans-serif;">¡Semit te envía la cotización solicitada!</h5>
                                <br>
                            </div>
                        </div>

                        <div style="width: 100%; color:white; text-align:center">
                            <h5 style="color:#152342; font-family: sans-serif;">Elevar tu calidad de vida es nuestro<br>compromiso</h5>
                        </div>
                        <div style="width: 100%; color:white; text-align:center">
                            <h5 style="color:#152342; font-family: sans-serif;">¡Muchas gracias por preferencia!<br>30 años cambiando vidas</h5>
                        </div>
                        <div style="width: 100%; text-align: center;">
                            <img src="'.base_url().'images/11.png" style="width: 149px;">
                        </div>
                      </div> 

                        
                    </div></div>';


        $this->email->message($message);
        $urlfac=FCPATH.'doc_cotizaciones/Cotizacion_'.$idreg.'.pdf';
        $this->email->attach($urlfac);

        $this->email->send();
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        /*if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }*/
    }
    function get_clausulas(){
        $results_cl=$this->Modeloventas->get_clausula();
        $id=0;
        foreach ($results_cl as $x){
            $id=$x->id;
        }
        $results=$this->Modeloventas->get_productos_proveedor($id);
        echo json_encode($results);
    }
    public function imprimir_cotizacion($id){
        $data['idcotizacion']=$id;
        $data['result_clausulas']=$this->ModelCotizaciones->get_productos_proveedor($id);
        $data['result_productos']=$this->ModelCotizaciones->get_productos($id);
        $data['result_cotizacion']=$this->ModeloCatalogos->getselectwheren('cotizacion',array('id'=>$id));
        $result_cotizacion=$this->ModelCotizaciones->get_cotizacion($id);
        $data['folio']='';
        $data['reg']='';
        $data['razon_social']='';
        $data['celular']='';
        $data['correo']='';
        foreach ($result_cotizacion as $y) {
            $data['folio']=$y->folio;
            $data['reg']=date('d/m/Y',strtotime($y->reg));
            $data['razon_social']=$y->razon_social;
            $data['celular']=$y->celular;
            $data['correo']=$y->correo;
        }

        $this->load->view('reportes/cotizacion',$data);
    }

    /*  *******************RENTAS********************* */
    function add_rentas(){
        $params = $this->input->post();
        $idreg=$params['id'];
        $cant=1;
        $des=0;
        $desp=0;
        /*$result=$this->General_model->getselectwhere('servicios','id',$idreg);
        foreach ($result as $item) {
            $id = $item->id;
            $clave = $item->clave;
            $descripcion = $item->descripcion;
            $check1 = $item->check1;
            $check15 = $item->check15;
            $check30 = $item->check30;
            //============================================ quitar el iva a los productos
            $orentas=array("id"=>$id,"clave"=>$clave,"descripcion"=>$descripcion,"check1"=>$check1,"check15"=>$check15,"check30"=>$check30);
            if(in_array($orentas, $_SESSION['rentr'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                //log_message('error','existe producto');
                $idx = array_search($orentas, $_SESSION['rentr']);
                //log_message('error','existe producto idx'.$idx);
                $_SESSION['cantr'][$idx]+=$cant;
            }else{ //sino lo agrega
                array_push($_SESSION['rentr'],$orentas);
                array_push($_SESSION['cantr'],1);
                array_push($_SESSION['desr'],$des);
                array_push($_SESSION['despr'],$desp);
                array_push($_SESSION['costr'],$desp);
            }   
        }*/
        $get=$this->ModeloRentas->getRentaId($idreg);
        $id = $get->id;
        $id_servicio = $get->id_servicio;
        $id_serie = $get->id_serie;
        $productoid=$get->productoid;
        $descripcion = $get->descripcion;
        $costo = $get->costo;
        $deposito = $get->deposito;
        $serie = $get->serie;
        $periodo = $get->periodo;
        $inicio = $get->fecha_inicio;
        $entrega = $get->costo_entrega;
        $recolecta = $get->costo_recolecta;
        //============================================ quitar el iva a los productos
        $orentas=array("id"=>$id,"id_servicio"=>$id_servicio,"descripcion"=>$descripcion,"costo"=>$costo,"deposito"=>$deposito,"serie"=>$serie,"periodo"=>$periodo,"inicio"=>$inicio,"id_serie"=>$id_serie,"productoid"=>$productoid,"entrega"=>$entrega,"recolecta"=>$recolecta);
        //log_message('error','orentas '.json_encode($orentas));
        if(in_array($orentas, $_SESSION['rentr'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            //log_message('error','existe producto');
            $idx = array_search($orentas, $_SESSION['rentr']);
            //log_message('error','existe producto idx'.$idx);
            $_SESSION['cantr'][$idx]=$cant;
        }else{ //sino lo agrega
            array_push($_SESSION['rentr'],$orentas);
            array_push($_SESSION['cantr'],1);
            array_push($_SESSION['desr'],$des);
            array_push($_SESSION['despr'],$desp);
            array_push($_SESSION['costr'],$desp);
        }
    }
    function viewrentas(){
        //====================================
        $count = 0;
        $html='';
        //log_message('error','count rentr: '.count($_SESSION['rentr']));
        if(count($_SESSION['rentr'])>0){
            foreach ($_SESSION['rentr'] as $fila){
                $Cantidad=$_SESSION['cantr'][$count]; 
                $des=$_SESSION['desr'][$count];
                $desp=$_SESSION['despr'][$count];
                
                $costo=$fila['costo'];
                $deposito=$fila['deposito'];
                $entrega=$fila['entrega'];
                $recolecta=$fila['recolecta'];
                //$total_renta=round($costo,2)+round($deposito,2)+round($entrega,2)+round($recolecta,2); //cmenta lic xoch quitar costo de deposito
                $total_renta=round($costo,2)+round($entrega,2)+round($recolecta,2);
                $serie=$fila['serie'];
                $periodo=$fila['periodo'];
                $id_servicio=$fila['id_servicio'];
                if($periodo==1){
                    $periodo="1 día";
                }if($periodo==2){
                    $periodo="15 días";
                }if($periodo==3){
                    $periodo="30 días";
                }

                $precio=1;
                $descuento=0;
                if($desp>0){
                    $descuento_l=$desp.'%';
                    $descuento=round(($Cantidad*$precio)*($desp/100), 2);
                }else{
                    $descuento_l='$ '.$des;
                    $descuento=$des;
                }

                $uni_sat='H87';
                $ser_sat='85161505';
                
                /*$res_p = $this->ModeloCatalogos->getselectwheren('servicios',array('id'=>$fila['id']));
                foreach ($res_p->result() as $r) {
                    if($r->Unidad!=null && $r->Unidad!='null'){
                        $uni_sat=$r->Unidad;
                    }
                    if($r->servicioId!=null && $r->servicioId!='null'){
                        $ser_sat=$r->servicioId;
                    }   
                }*/

                $cantotal=($Cantidad*$precio)-$descuento;
                $stocktxt='';
                $html.='<tr class="producto_'.$count.' tr_rentas" data-uni="'.$uni_sat.'" data-ser="'.$ser_sat.'">
                        <td>
                            <input type="hidden" name="count" id="count" value="'.$count.'">
                            <input type="hidden" name="vsproid" id="vsproid" class="vsproid_'.$count.'" value="'.$fila['id_servicio'].'">
                            <input type="hidden" id="id_renta" class="id_renta_'.$count.'" value="'.$fila['id'].'">
                            <input type="hidden" id="incluye_iva" class="incluye_iva_'.$count.'" value="1">
                            <input disabled type="number" name="vscanti" id="vscanti" class="vscanti vscanti_'.$count.'" value="1">
                        </td>
                        <td><input type="hidden" id="id_prod" class="id_prod_'.$count.'" value="'.$fila['productoid'].'">'.$fila['descripcion'].'</td>
                        <td><input type="hidden" id="id_sserie" class="id_sserie_'.$count.'" value="'.$fila['id_serie'].'">'.$serie.'</td>
                        <td><input type="hidden" id="costo_serv" class="costo_serv_'.$count.'" value="'.$fila['costo'].'">'.$costo.'</td>
                        <td><input type="hidden" id="depto_serv" class="depto_serv_'.$count.'" value="'.$fila['deposito'].'">'.$deposito.'</td>
                        <td><input type="hidden" id="periodo" class="periodo_'.$count.'" value="'.$fila['periodo'].'">'.$periodo.'</td>
                        <td>'.date("d-m-Y", strtotime($fila['inicio'])).'</td>
                        <td>'.$entrega.'</td>
                        <td>'.$recolecta.'</td>
                        <td><div class="btn-group" style="display: flex; align-items: center;">$ <input type="text" class="vstotal vstotal_'.$count.'" name="vstotal" id="vstotal" value="'.$total_renta.'" readonly style="background: transparent;border: 0px; width: 100px;"></div></td>
                        <td>
                            <a onclick="deletepro('.$count.',4)" class="btn"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>';   
                $count++;
                //<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="adddescuento('.$count.',1)">$</a>
            }
        }

        echo $html;
    }
    function get_producto_select_series(){   
        $id=$this->input->post('id');
        $sucursal=$this->sucursal;
        $result=$this->General_model->getselectwhereall('productos_sucursales_serie',array('productoid'=>$id,'sucursalid'=>$sucursal));
        echo json_encode($result);
    }
    function get_servicio_venta(){
        $params = $this->input->post();
        $id=$params['id'];
        $per=$params['per'];
        $result=$this->ModeloCatalogos->getselectwheren('servicios',array('id'=>$id));

        $precio=0;
        $depositogarantia=0;

        foreach ($result->result() as $iten) {
            if($per==1){
                $precio=$iten->precios1;
                $depositogarantia=$iten->depositogarantia1;
            }
            if($per==15){
                $precio=$iten->precios15;
                $depositogarantia=$iten->depositogarantia15;
            }
            if($per==30){
                $precio=$iten->precios30;
                $depositogarantia=$iten->depositogarantia30;
            }
        }
        $array=array('precio'=>$precio,
                    'depositogarantia'=>$depositogarantia);
        echo json_encode($array);
    }
    function editardatosfiscales(){
        $params = $this->input->post();
        $id=$params['id'];
        unset($params['id']);
        $this->ModeloCatalogos->updateCatalogo('cliente_fiscales',$params,array('id'=>$id));
    }
    function viewservicio(){
        //log_message('error','function viewservicio: ');
        //====================================
        $count = 0;
        $html='';
        if(isset($_SESSION['ser'])){
            foreach ($_SESSION['ser'] as $fila){
                //log_message('error','servicio: '.$fila['id']);
                $Cantidad=$_SESSION['can_ser'][$count]; 
                $des=$_SESSION['des_ser'][$count];
                //$desp=$_SESSION['despr'][$count];
                $desp=0;
                
                $precio=$_SESSION['precio_ser'][$count];
                if($precio>0){
                    $pre_readonly=' readonly ';
                    $pre_style=' style="background: transparent;border: 0px; width: 100px;" ';
                }else{
                    $pre_readonly='';
                    $pre_style='';
                }
                $descuento=0;
                if($desp>0){
                    $descuento_l=$desp.'%';
                    $descuento=round(($Cantidad*$precio)*($desp/100), 2);
                }else{
                    $descuento_l='$ '.$des;
                    $descuento=$des;
                }

                $cantotal=($Cantidad*$precio)-$descuento;
                $precio_iva=$precio;
                if($fila['incluye_iva']==1){
                    $ivacant=round($cantotal*0.16,5);
                    $cantotal=$cantotal+$ivacant;

                    $precio_iva_i=round($precio*0.16,5);
                    $precio_iva=round($precio_iva+$precio_iva_i,2);
                }

                $cantotal=round($cantotal,2);
                $stocktxt='';
                //============================================ falta especificar esta parte de donde se tomara
                    $uni_sat='';
                    $ser_sat='';
                    
                    $res_p = $this->ModeloCatalogos->getselectwheren('cat_servicios',array('id'=>$fila['id']));
                    foreach ($res_p->result() as $itemp) {
                        if($itemp->Unidad!=null && $itemp->Unidad!='null' ){
                            $uni_sat=$itemp->Unidad;
                        }
                        if($itemp->servicioId!=null && $itemp->servicioId!='null' ){
                            $ser_sat=$itemp->servicioId;
                        }   
                    }
                    
                //============================================
                $html.='<tr class="producto_'.$count.'_3 validar_sat info_pro_'.$fila['id'].'_3" data-uni="'.$uni_sat.'" data-ser="'.$ser_sat.'">
                            <td>
                                <input type="hidden" name="count" id="count" value="'.$count.'">
                                <input type="hidden" name="vsproid" id="vsproid" class="vsproid_'.$count.'" value="'.$fila['id'].'">
                                <input type="hidden" name="vsproid_ps_lot" id="vsproid_ps_lot" value="0">
                                <input type="hidden" name="vsproid_ps_ser" id="vsproid_ps_ser" value="0">
                                <input type="hidden" class="vstipo_'.$count.'" name="vstipo" id="vstipo" value="3">
                                <input type="hidden" class="vscapac_'.$count.'" name="vscapac" id="vscapac" value="0">
                                <input type="number" name="vscanti" id="vscanti" class="vscanti vscanti_'.$count.'" value="'.$Cantidad.'" onchange="editarcantidad_ser('.$count.')">
                                <input type="hidden" name="incluye_iva" id="incluye_iva" class="incluye_iva incluye_iva_'.$count.'" value="'.$fila['incluye_iva'].'">
                            </td>
                            <td class="name_pros">'.$fila['numero'].' '.$fila['descripcion'].'</td>
                            <td><input type="number" class="stockn_'.$count.'" name="stockn" id="stockn" value="'.$Cantidad.'" readonly></td>
                            <td>$<input type="text" class="form-control vsprecio vsprecio_'.$count.'" name="vsprecio" id="vsprecio" value="'.$precio.'" '.$pre_readonly.' '.$pre_style.' onchange="editarprecio('.$count.')"></td>
                            <td>'.$precio_iva.'</td>
                            <td style="display:none"><input type="text" name="vsdescuento" id="vsdescuento" class="vsdescuento" value="'.$descuento.'" readonly>
                                <!--<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="validar_descuento('.$count.',0)">%</a>-->
                            </td>
                            <td>';
                            $html.='<div class="btn-group" style="display: flex; align-items: center;">';
                                $html.='$ <input type="text" class="vstotal vstotal_'.$count.'" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px;    width: 100px;">';
                                $html.='</div>';
                                $html.='<a class="btn btn-desc" onclick="sol_pro_desc('.$fila['id'].',3)"></a>';
                                $html.='<span clas="mont_des">';
                                    if($des>0){
                                        $html.='Desc: $'.$des;
                                    }
                                $html.='</span>';
                            $html.='</td>
                            <td>
                                <a onclick="deletepro('.$count.',3,'.$fila['id'].')" class="btn"><i class="fa fa-trash"></i></a>';
                                if($this->session->userdata('tipo_tecnico')=="1"){
                                    $html.='<a title="Agregar Insumos" data-precioserv="'.number_format($precio_iva,2,".","").'" data-nameserv="'.$fila['descripcion'].'" onclick="modalInsumos('.$fila['id'].')" class="btn add_serv_'.$fila['id'].'"><i class="fa fa-plus"></i></a>';
                                }
                            $html.='</td>
                        </tr>';   
                $count++;
                //<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="adddescuento('.$count.',1)">$</a>
            }
        }

        //echo $html;
        return $html;
    }

    function getInsumosServicio(){
        $id = $this->input->post('id');
        $id_venta = $this->input->post('id_venta');
        $results=$this->Modeloventas->getInsumosServ($id,$id_venta,$this->sucursal);
        echo json_encode($results);
    }

    public function delete_insumo_mttoext(){
        $id=$this->input->post('id');
        //editar insumo utlizado en servicios
        //$getbi=$this->General_model->get_record("id",$id,"bitacora_ins_servs_ventas");
        //$this->Modeloventas->sumaInsumo_sucursal($getbi->id_prod_suc,$getbi->cantidad);

        $data = array('cantidad'=>0);
        $this->General_model->edit_record('id',$id,$data,'bitacora_ins_servs_ventas');   
    }

    function editarcantidad_ser(){
        $params = $this->input->post();
        $row=$params['row'];
        $cantidad=$params['cantidad'];
        $_SESSION['can_ser'][$row]=$cantidad;
    }
    function editarprecio(){
        $params = $this->input->post();
        $row=$params['row'];
        $precio=$params['precio'];
        $_SESSION['precio_ser'][$row]=$precio;
    }
    function searchallpros(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->searchallpros($pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function viewprecios_ptipo(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        $html='';
        if($tipo==0){
            $row=0;
            $suc_pro=$this->General_model->get_suc_prod($id);
            
                foreach ($suc_pro->result() as $x){
                    $row++;
                    if($row==1){
                        $precio=$x->precio;
                        $incluye_iva=$x->incluye_iva;
                        $iva_tex='';
                        $ivacal=0;
                        if($incluye_iva==1){
                            $iva_tex=$x->iva.' %';
                            $ivacal=round($precio*0.16,5);
                        }
                        $precio_final=round($precio+$ivacal,2);
                        $html.='<tr>
                        
                        <td>$ '.number_format($precio,5,'.',',') .'</td>
                        <td>$ '.number_format($precio_final,2,'.',',') .'</td>
                        </tr>';
                    }
                }
                
        }
        if($tipo==1){
            $result=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$id));
            foreach ($result->result() as $item) {
                $html.='<tr>
                    <td>$ '.number_format($item->preciov,5,'.',',') .'</td>
                    <td>$ '.number_format($item->preciov,2,'.',',') .'</td>
                    </tr>';
            }

        }
        if($tipo==2){
            $result=$this->ModeloCatalogos->getselectwheren('servicios',array('id'=>$id));
            foreach ($result->result() as $item) {
                $htmlprecios='';
                $precio1=$item->precios1;
                if($precio1>0){
                    $htmlprecios=number_format($item->precios1,2,'.',',');
                    $html.='<tr>
                    <td>1 dia </td>
                    <td> '.$htmlprecios .'</td>
                    <td></td>
                    <td> '.$htmlprecios .'</td>
                    </tr>';
                }
                $precio2=$item->precios15;
                if($precio2>0){
                    $htmlprecios=number_format($item->precios15,2,'.',',');
                    $html.='<tr>
                    <td>15 dias </td>
                    <td> '.$htmlprecios .'</td>
                    <td></td>
                    <td> '.$htmlprecios .'</td>
                    </tr>';
                }
                $precio3=$item->precios30;
                if($precio3>0){
                    $htmlprecios=number_format($item->precios30,2,'.',',');
                    $html.='<tr>
                    <td>30 dias</td>
                    <td> '.$htmlprecios .'</td>
                    <td></td>
                    <td> '.$htmlprecios .'</td>
                    </tr>';
                }

                
            }

        }
        if($tipo==3){
            $result=$this->ModeloCatalogos->getselectwheren('cat_servicios',array('id'=>$id));
            foreach ($result->result() as $item) {
                    $precio=$item->precio_siva;
                    $incluye_iva=$item->iva;
                    $iva_tex='';
                    $ivacal=0;
                    if($incluye_iva==1){
                        $iva_tex='16 %';
                        $ivacal=round($precio*0.16,5);
                    }
                    $precio_final=round($precio+$ivacal,2);
                $html.='<tr>
                    <td>$ '.number_format($precio,5,'.',',') .'</td>
                    <td>$ '.number_format($precio_final,2,'.',',') .'</td>
                    </tr>';
            }
        }
        echo $html;
    }
    function viewexistencias_ptipo(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        $ext=$params['ext'];
        $html='';
        if($tipo==0){
            $strq="SELECT suc.id as idsuc,pro.id,suc.name_suc,pro.tipo,pros.stock, IFNULL(count(*),0) as serie, IFNULL(sum(prosl.cantidad),0) as lotes,
                IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
                join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
                where ht.idproducto=pro.id and ht.activo=1 and ht.idsucursal_sale=suc.id and ht.tipo=0 and ht.status=0 and ht.rechazado!=1),0) as traslado_stock_cant,
                IFNULL(tlh.cantidad,0) as tras_stock_lote,
                count(tsh.id) as tot_tras_serie,
                IFNULL(t.status,2) as status_trasr,
                IFNULL(t2.status,2) as status_traslt
                FROM productos as pro
                INNER JOIN sucursal as suc ON suc.activo=1
                left join productos_sucursales as pros on pros.productoid=pro.id AND pros.sucursalid=suc.id
                LEFT join productos_sucursales_serie as pross on pross.activo=1 and pross.disponible=1 AND pross.productoid=pro.id AND pross.sucursalid=suc.id
                LEFT join productos_sucursales_lote as prosl on prosl.activo=1 AND prosl.productoid=pro.id AND prosl.sucursalid=suc.id
                left join traspasos_lotes_historial tlh on tlh.idlotes=prosl.id and tlh.activo=1 and pro.tipo=2
                left join traspasos_series_historial tsh on tsh.idseries=pross.id and tsh.activo=1 and pro.tipo=1
                left join traspasos t on t.id=tsh.idtraspasos
                left join traspasos t2 on t2.id=tlh.idtraspasos
                WHERE pro.id='$id'
                group by suc.id,pro.id
                order by suc.orden";
                $query = $this->db->query($strq);
                foreach ($query->result() as $item) {
                    $stock=0;
                    if($item->tipo==0){//normal
                        $stock=$item->stock-$item->traslado_stock_cant;
                    }
                    if($item->tipo==1){//serie
                        if($item->status_trasr!=2){
                            $stock=$item->serie-$item->tot_tras_serie;
                        }else{
                            $stock=$item->serie;
                        }
                    }
                    if($item->tipo==2){//lote
                        if($item->status_traslt!=2){
                            $stock=$item->lotes-$item->tras_stock_lote;
                        }else{
                            $stock=$item->lotes;
                        }
                    }

                    $info_html='<tr>
                    <td>'.$item->idsuc.'</td>
                    <td>'.$item->name_suc.'</td>
                    <td>'.$stock.'</td>
                    </tr>';
                    if($ext==0){
                        $html.=$info_html;
                    }
                    if($ext==1 and $stock>0){
                        $html.=$info_html;
                    }
                }
        }
        if($tipo==1){
            /*
            $result=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$id));
            foreach ($result->result() as $item) {
                $html.='<tr>
                    <td></td>
                    <td></td>
                    <td>'.$item->capacidad.'</td>
                    </tr>';
            }
            */
        }
        if($tipo==2){
            
        }
        if($tipo==3){
            
        }
        echo $html;
    }

    function demo_correo(){
        $this->load->view('ventasp/correo_demo');
    }
    function soldescuento(){
        $params=$this->input->post();
        if(isset($params['id_sol'])){
            $id_sol= $params['id_sol'];
            unset($params['id_sol']);
            
        }else{
            $id_sol = 0;
        }
        if($id_sol>0){
            $aurech = $params['aurech'];
            unset($params['aurech']);
            if($aurech==1){
                $datos = $params;
                $datos['status']=1;
            }else{
                $datos['status']=2;
            }
            $this->ModeloCatalogos->updateCatalogo('sol_des',$datos,array('id'=>$id_sol));
        }else{
            $this->limpiarsolicitudes_des();// limpiar las solicitudes
            $params['idsucursal']=$this->sucursal;
            $params['idpersonal']=$this->idpersonal;
            $this->ModeloCatalogos->Insert('sol_des',$params); 
        }
        
    }
    function soldescuento_verif(){
        $res=$this->ModeloCatalogos->getselectwheren('sol_des',array('idsucursal'=>$this->sucursal,'idpersonal'=>$this->idpersonal,'activo'=>1,'reg >='=>$this->fecha_reciente.' 00:00:00'));
        echo json_encode($res->result()); 
    }
    function limpiarsolicitudes_des(){
        $this->ModeloCatalogos->updateCatalogo('sol_des',array('activo'=>0),array('idsucursal'=>$this->sucursal,'idpersonal'=>$this->idpersonal));
    }
    function adddescuento(){
        $params = $this->input->post();
        $row = $params['row'];
        $des = $params['des'];
        $tipo = $params['tipo'];
        if($tipo==0 || $tipo==1 || $tipo==2){
            $_SESSION['des'][$row]=$des;
        }
        if($tipo==3){
            $_SESSION['des_ser'][$row]=$des;
        }
    }
 
}


