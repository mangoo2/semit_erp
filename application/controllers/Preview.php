<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->helper('url');
        $this->succlave=$this->session->userdata('succlave');
        $this->sucursal=$this->session->userdata('sucursal');
    }

	public function index(){
        
        
	}   
    function capturapreview(){
      $params = $this->input->post();
      $_SESSION['infopreview']=$params;
    }
    public function factura(){
        $ipre=$_SESSION['infopreview'];
        if(isset($ipre['pg_global'])){
          $pg_global=$ipre['pg_global'];
        }else{
          $pg_global=0;
        }
        $cp_cp='';
        //var_dump($ipre);
        //echo $ipre['uso_cfdi'];
       $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/SEMIT.jpg';
        }
        $data["logotipo"]=$logotipo;
        
      $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
      $data['rrfc']=$datosconfiguracion->Rfc;
      $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Noexterior.'  interior '.$datosconfiguracion->Nointerior.' '.$datosconfiguracion->Colonia.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
      $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;
      $data['LugarExpedicion_suc']=$datosconfiguracion->CodigoPostal;
      $cp_cp=$datosconfiguracion->CodigoPostal;
      //===============================================
        $datossucursal= $this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$this->sucursal));
        foreach ($datossucursal->result() as $items) {
          $data['rdireccion']=$items->domicilio;
          $data['LugarExpedicion_suc']=$items->cp_cp;
          $cp_cp=$items->cp_cp;
        }
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();
        $data['sucu']= $this->ModeloCatalogos->obtenersucursalesfactura();
      //===============================================
      
      
      $data['Folio']=0001;
      $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
      $data['nocertificadosat']='00001000000000000000';
      $data['certificado']='00001000000000000000';
      $data['cfdi']=$this->gt_uso_cfdi($ipre['uso_cfdi']);
      $data['fechatimbre']='0000-00-00 00:00:00';
      
      $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
      $data['configticket']=$configticket->row();

      $confif=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $confif=$confif->row();
      $data['confif']=$confif;
      if($confif->logotipo!=''){
        $logotipo = base_url().'public/img/'.$confif->logotipo;
      }else{
        $logotipo = base_url().'public/img/SEMIT.jpg';
      }
      $data['logotipo']=$logotipo;
      $data["Nombrerasonsocial"]=$confif->Nombre;
      $data['rrfc']=$confif->Rfc;
      $data['rdireccion']=$confif->Calle.' '.$confif->Noexterior.'  interior '.$confif->Nointerior.' '.$confif->Colonia.' '.$confif->Municipio.' '.$confif->Estado.' '.$confif->PaisExpedicion.' C.P. '.$confif->CodigoPostal;
      $data['regimenf']=$this->Modelofacturas->regimenf($confif->Regimen);

        $resultcli=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$ipre['rfc'],'activo'=>1));
        $cliente='';
        $clirfc='';
        $clidireccion='';
        foreach ($resultcli->result() as $item) {
            $data['clienteId']=$item->idcliente;
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            
            $cp = $item->cp;

            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;

            if($clirfc=='XAXX010101000'){
              if(isset($ipre['pg_global'])){
                if($ipre['pg_global']==0){
                  if($cliente=='PUBLICO EN GENERAL'){
                    $cliente=$cliente.' '.$this->succlave;
                  }
                }
              }else{
                //$cliente=$empresa;
              }
            }


        }
        if($pg_global==1){
          $cp=$cp_cp;
        }
        
          $clidireccion     =       '' ;
        $tipoComprobante='I-Ingreso';
        if($ipre['f_relacion']==1){
          if($ipre['f_r_tipo']=='01'){
            $tipoComprobante='E-Egreso';
          }
        }                    
        $tipoComprobante=$ipre['TipoComprobante'];
        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        $data['isr']            =   $ipre['visr'];
        $data['numordencompra'] =   $ipre['numordencompra'];
        $data['ivaretenido']    =   $ipre['vriva'];
        $data['cedular']        =   0;
        $data['cincoalmillarval']        =   $ipre['v5millar'];
        $data['outsourcing']        =   $ipre['outsourcing'];
        $data['numproveedor']   =   $ipre['numproveedor'];
        $data['observaciones']  =   $ipre['observaciones'];
        $data['iva']            =   $ipre['iva'];
        $data['subtotal']       =   $ipre['subtotal'];
        $data['total']          =   $ipre['total'];
        $data['moneda']         =   $ipre['moneda'];
        $data['tarjeta']        =   $ipre['tarjeta'];
        $data['FormaPago']      =   $ipre['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($ipre['MetodoPago']);
        $data['MetodoPago']     =   $ipre['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($ipre['FormaPago']);
        $data['tipoComprobante']=$tipoComprobante;
        $data['RegimenFiscalReceptor']=$this->Modelofacturas->regimenf($RegimenFiscalReceptor);
        $data['f_relacion']=$ipre['f_relacion'];
          $data['f_r_tipo']=$ipre['f_r_tipo'];
          $data['f_r_uuid']=$ipre['f_r_uuid'];
          $data['Caducidad']      =   '';
          $data['Lote']           = '';

          $data['nom_personal']           = '';
          $data['ticket']           = '';


        $conseptos=json_decode($ipre['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            if($item->iva>0){
              $consin_iva=1;
            }else{
              $consin_iva=0;
            }
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cant,
                                    'Unidad'=>$item->Uni,
                                    'nombre'=>$this->get_f_undades($item->Uni),
                                    'servicioId'=>$item->serId,
                                    'Descripcion'=>$this->get_servicios($item->serId), //originamnelte $item->Descripcion
                                    'Descripcion2'=>$item->Desc2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->desc,
                                    'iva'=>$item->iva,
                                    'consin_iva'=>$consin_iva
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$ipre['subtotal'].'|MXN|'.$ipre['total'].'|I|'.$ipre['FormaPago'].'|72090|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE
CV|'.$ipre['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('reportes/factura',$data);
    }   
    public function facturar(){
        $data["Nombrerasonsocial"]='Nombrerasonsocial';
        $data['rrfc']='erftgyuio5678';
        $data['rdireccion']='direccion conocida';
        $data['regimenf']='601 General de Ley Personas Morales';
        $data['Folio']=0001;
        $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['cfdi']=$this->gt_uso_cfdi($_GET['uso_cfdi']);
        $data['fechatimbre']='0000-00-00 00:00:00';
        $confic=$this->ModeloCatalogos->db10_getselectwheren('configuracionCotizaciones',array('id'=>1));
        $data['confic']=$confic->row();
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();
        $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $data['confif']=$confif->row();
        $resultcli=$this->ModeloCatalogos->db10_getselectwheren('cliente_has_datos_fiscales',array('Rfc'=>$_GET['rfc'],'activo'=>1));
        $cliente='';
        $clirfc='';
        $clidireccion='';
        foreach ($resultcli->result() as $item) {
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            $localidad=$item->localidad;
            $estado=$item->estado;
            $municipio=$item->municipio;
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;

        }
        /*
        $dcliente=$this->ModeloCatalogos->db10_getselectwheren('clientes',array('id'=>$_GET['idcliente']));
          foreach ($dcliente->result() as $item) {
            $municipio=$item->municipio;
            $idestado=$item->estado;
          }
          /*
          $destado=$this->ModeloCatalogos->db10_getselectwheren('estado',array('EstadoId'=>$idestado));
          foreach ($destado->result() as $item) {
            $estado=$item->Nombre;
          }
          */
          $clidireccion     =       $calle . ' No ' . $num_ext . ' Col: ' . 
                                    $colonia . ' , ' . 
                                    $municipio . ' , '.$localidad.' , ' . $estado . ' . ';

        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        $data['isr']            =   $_GET['visr'];
        $data['numordencompra'] =   $_GET['numordencompra'];
        $data['ivaretenido']    =   $_GET['vriva'];
        $data['cedular']        =   0;
        $data['cincoalmillarval']        =   $_GET['v5millar'];
        $data['outsourcing']        =   $_GET['outsourcing'];
        $data['numproveedor']   =   $_GET['numproveedor'];
        $data['observaciones']  =   $_GET['observaciones'];
        $data['iva']            =   $_GET['iva'];
        $data['subtotal']       =   $_GET['subtotal'];
        $data['total']          =   $_GET['total'];
        $data['moneda']         =   $_GET['moneda'];
        $data['tarjeta']        =   $_GET['tarjeta'];
        $data['FormaPago']      =   $_GET['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($_GET['MetodoPago']);
        $data['MetodoPago']     =   $_GET['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($_GET['FormaPago']);
        $data['tipoComprobante']='I-Ingreso';
        $data['RegimenFiscalReceptor']=$this->Modelofacturas->regimenf($RegimenFiscalReceptor);
        $data['f_relacion']=$_GET['f_relacion'];
          $data['f_r_tipo']=$_GET['f_r_tipo'];
          $data['f_r_uuid']=$_GET['f_r_uuid'];

        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cantidad,
                                    'Unidad'=>$item->Unidad,
                                    'nombre'=>$this->get_f_undades($item->Unidad),
                                    'servicioId'=>$item->servicioId,
                                    'Descripcion'=>$item->Descripcion,
                                    'Descripcion2'=>$item->Descripcion2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->descuento
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$_GET['subtotal'].'|MXN|'.$_GET['total'].'|I|'.$_GET['FormaPago'].'|72090|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE
CV|'.$_GET['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('Reportes/factura',$data);
    } 
    public function complementodoc(){
      
      $idconfig=1;
      if(isset($_GET['Serie'])){
        if($_GET['Serie']==2){
          $idconfig=1;  
        }
        
      }
        //$data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($idconfig);
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfig));
        $datosconfiguracion=$datosconfiguracion->row();
        $data['confif']=$datosconfiguracion;
        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['regimenf']='601 General de Ley Personas Morales';
        $data['idcomplemento']=0;
        $data['uuid']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['rfcreceptor']=$_GET['rfcreceptor'];
        $data['LugarExpedicion']=$_GET['LugarExpedicion'];
        $data['fechatimbre']='0000-00-00 00:00:00';
        $data['nombrereceptorr']=$_GET['razonsocialreceptor'];
        $data['NumOperacion']=$_GET['NumOperacion'];
        $data['moneda']=$_GET['Moneda'];
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$_GET['FormaDePagoP']));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
        $data['fechapago']=$_GET['Fechatimbre'];
        $data['monto']=$_GET['Monto'];
        $data['Sello']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|282|0000-00-00T00:00:00|00001000000000000000|0|XXX|0|P|'.$_GET['LugarExpedicion'].'|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE CV|601|'.$_GET['rfcreceptor'].'|'.$_GET['rfcreceptor'].'|P01|84111506|1|ACT|Pago|0|0|1.0|0000-00-00T00:00:00|03|MXN|0.00|00446880|XX00X000-000X-0X00-0X0X-X0X00XXXX000|U|282|MXN|PPD|1|4060|4060|0||';

        $docrelac=json_decode($_GET['arraydocumento']);
        
        $conseptos=json_decode($_GET['arraydocumento']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $resultscp=$this->ModeloCatalogos->db10_getselectwheren('f_facturas',array('FacturasId'=>$item->idfactura));
            $resultscp=$resultscp->row();
            $arrayconceptos[]=array(
                                    'idfactura'=>$item->idfactura,
                                    'IdDocumento'=>$item->IdDocumento,
                                    'NumParcialidad'=>$item->NumParcialidad,
                                    'ImpSaldoAnt'=>$item->ImpSaldoAnt,
                                    'ImpPagado'=>$item->ImpPagado,
                                    'moneda'=>$resultscp->moneda,
                                    'FormaPago'=>$resultscp->FormaPago,
                                    'Folio'=>$resultscp->Folio,
                                    'ImpSaldoInsoluto'=>$item->ImpSaldoInsoluto
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);




        $data['docrelac']=$arrayconceptos;


      $this->load->view('reportes/complemento',$data);
    }  
    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }
    function get_f_undades($unidad){
        $result=$this->ModeloCatalogos->getselectwheren('f_unidades',array('Clave'=>$unidad));
        $nombre='';
        foreach ($result->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    function get_servicios($servicioId){
      switch ($servicioId) {
        case '14111507':
          $servicioname='Papel para impresora o fotocopiadora';
          break;
        case '80161801':
          $servicioname='Servicio de alquiler o leasing de fotocopiadoras';
          break;
        case '81101707':
          $servicioname='Mantenimiento de equipos de impresion';
          break;
        case '56101501':
          $servicioname='Stands';
          break;
        case '39121635':
          $servicioname='Regulador de voltaje ';
          break;
        case '44101705':
          $servicioname='Bandejas o alimentadores de máquinas de oficina';
          break;
        case '25101503':
          $servicioname='Carros';
          break;
        case '82121500':
          $servicioname='Impresión';
          break;
        case '82121501':
          $servicioname='Planificación y trazados de producciones gráficas';
          break;
        case '82121700':
          $servicioname='Fotocopiado';
          break;
        case '82121701':
          $servicioname='Servicios de copias en blanco y negro o de cotejo';
          break;
        case '82121702':
          $servicioname='Servicios de copias a color o de cotejo';
          break;
        case '43212110':
          $servicioname='Impresoras de multiples funciones';
          break;
        case '81112306':
          $servicioname='Mantenimiento de impresoras';
          break;
        case '44103103':
          $servicioname='Toner para impresoras o fax';
          break;
        case '78102203':
          $servicioname='Servicios de envio, recogida o entrega de correo ';
          break;
        case '44103125':
          $servicioname='Kit de mantenimiento de impresoras';
          break;
        case '84111506':
          $servicioname='Servicios de facturación';
          break;
        case '44101700':
          $servicioname='Accesorios para impresoras, fotocopiadoras y aparatos de fax';
          break;
        case '01010101':
          $servicioname='No existe en el catálogo';
          break;
        
        default:
          $servicioname='';
          break;
      }
      return $servicioname;
    }
    
}
