<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cservicios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');        
        $this->load->model('ModelServicios');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,37);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=36;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('servicios/cindex');
        $this->load->view('templates/footer');
        $this->load->view('servicios/cindexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=37;
        if($id==0){
            $data['id']=0;
            $data['numero']='';
            $data['descripcion']='';
            $data['tipo']=0;
            $data['precio_siva']='';
            $data['iva']=0;
            $Unidad=0;
            $servicioId=0;
        }else{
            $resul=$this->General_model->getselectwhere('cat_servicios','id',$id);
            foreach ($resul as $item){
                $data['id']=$item->id;
                $data['numero']=$item->numero;
                $data['descripcion']=$item->descripcion;
                $data['tipo']=$item->tipo;
                $data['precio_siva']=$item->precio_siva;
                $data['iva']=$item->iva;
                $Unidad=$item->Unidad;
                $servicioId=$item->servicioId;
                
            }
        }      
        $data['f_u']=$this->ModeloCatalogos->getselectwheren('f_unidades',array('Clave'=>$Unidad));
        $data['f_s']=$this->ModeloCatalogos->getselectwheren('f_servicios',array('Clave'=>$servicioId));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('servicios/cform',$data);
        $this->load->view('templates/footer');
        $this->load->view('servicios/cformjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelServicios->get_listcs($params);
        $totaldata= $this->ModelServicios->total_listcs($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];

        if($id==0){
            $id=$this->General_model->add_record('cat_servicios',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'cat_servicios');
            $id=$id;
        }

    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'cat_servicios');
    }

    function viewdetalles_list(){
        $id = $this->input->post('id');
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Precios</th>
                    <th scope="col">Depósito en garantia</th>
                </tr>
            </thead>
            <tbody>';
            $resul=$this->General_model->getselectwhere('servicios','id',$id);
            foreach ($resul as $s){
                if($s->check1==1){
                    $html.='<tr>
                    <td>1 día</td>
                    <td>$'.number_format($s->precios1,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia1,2,'.',',').'</td>
                    </tr>';
                }
                if($s->check15==1){
                    $html.='<tr>
                    <td>15 días</td>
                    <td>$'.number_format($s->precios15,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia15,2,'.',',').'</td>
                    </tr>';
                }
                if($s->check30==1){
                    $html.='<tr>
                    <td>30 días</td>
                    <td>$'.number_format($s->precios30,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia30,2,'.',',').'</td>
                    </tr>';
                }
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

}