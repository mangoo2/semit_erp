<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ConfigGeneral extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloventas');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,34);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=34;
        //$data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        //$data['get_sucuarsales']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1) );
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('config/general');
        $this->load->view('templates/footer');
        $this->load->view('config/generaljs');
    }
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->General_model->get_list_cg_u($params);
        $totaldata= $this->General_model->get_list_cg_u_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function cambiarpermiso(){
        $params=$this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        if($tipo==0){
            $data['suc_date']=null;
        }
        if($tipo==1){
            $data['suc_date']=$this->fecha_reciente;
        }
        $this->ModeloCatalogos->updateCatalogo('usuarios',$data,array('UsuarioID'=>$id));
    }

   
}
