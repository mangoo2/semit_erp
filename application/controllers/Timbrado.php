<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timbrado extends CI_Controller {
	function __construct()    {
        parent::__construct();
        //$this->load->model('Login_model');
        //$this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->model('General_model');
        $this->load->model("Modeloventas");
        
        //$this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
        $this->demoproductivo=0;//0 demo 1 productivo
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            //$this->perfilid=$this->session->userdata('perfilid');
            //$this->idsucursal=1;// esto sera por seccion para que cambien entre cuentas     xxx
            $this->idsucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            
        }else{
            redirect('Inicio');
        }
    }

	public function index(){
      redirect('Inicio');
  }
  function generafacturarventa($idventa){
    $datas = $this->input->post();

    $resut_v=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$idventa));
    $resut_v=$resut_v->row();
    $conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$idventa));
    if(isset($datas['idrs'])){
      if($datas['idrs']>0){
        $id_razonsocial=$datas['idrs'];
        //$this->ModeloCatalogos->updateCatalogo('venta_erp',array('id_razonsocial'=>$id_razonsocial),array('id'=>$idventa));
        $this->ModeloCatalogos->updateCatalogo('venta_erp',array('id_razonsocial_new'=>$id_razonsocial),array('id'=>$idventa));
      }else{
        $id_razonsocial=$resut_v->id_razonsocial;
      }
    }else{
      $id_razonsocial=$resut_v->id_razonsocial;
    }
    $id_sucursal=$resut_v->sucursal;
    $resut_df=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$id_razonsocial));
    $resut_df=$resut_df->row();

    $MetodoPago=$this->ModeloCatalogos->obtenerformapagoventa($idventa);

    $idcliente=$resut_df->idcliente;
    $rfc=str_replace(' ', '', $resut_df->rfc);
    $razon_social=$resut_df->razon_social;
    $cp=str_replace(' ', '', $resut_df->cp);
    $direccion=$resut_df->direccion;
    $idventad = 0;
    $uso_cfdi = $resut_df->uso_cfdi;
    $RegimenFiscalReceptor = $resut_df->RegimenFiscalReceptor;
    
    $pais = 'MEXICO';
    $TipoComprobante='I';
    
    $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
    
    //$conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('idventa'=>$idventa));
    //$conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('id'=>$idventad));
    //$importetotal=0;
    //foreach ($conceptos_result->result() as $itemc) {
    //  $importetotal+=($itemc->cantidad*$itemc->costo);
    //}
    //$subtotal=round($importetotal/1.16,2);
    //$iva=round($subtotal*0.16,2);
      $subtotal=$resut_v->subtotal;
      $iva=$resut_v->iva;
      $importetotal=$resut_v->total;
      if($resut_v->tipo_venta==1){
        $FormaPago='PPD';
        $MetodoPago='99';
      }else{
        $FormaPago='PUE';
      }
      if(isset($datas['editdf'])){
        $cdfrs = $datas['cdfrs'];
        $cdfrfc = $datas['cdfrfc'];
        $cdfcp = $datas['cdfcp'];
        $cdfrf = $datas['cdfrf'];
        $cdfuso = $datas['cdfuso'];
        $idrs = $datas['idrs'];
        $razon_social=$cdfrs;
        $cp=$cdfcp;
        $rfc=$cdfrfc;
        $RegimenFiscalReceptor=$cdfrf;
        $uso_cfdi=$cdfuso;
      }
      if($rfc=='XAXX010101000'){
        $nombre_alias=$razon_social.' '.$this->succlave;
        if($razon_social=='PUBLICO EN GENERAL'){
          $razon_social=$razon_social.' '.$this->succlave;
          $cp='06300';
        }
      }else{
        $nombre_alias='';
      }
      //===========================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $cp_cp=$datosconfiguracion->CodigoPostal;

        if($id_sucursal>0){
          $resut_suc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$id_sucursal));
          foreach ($resut_suc->result() as $item_suc) {
              $cp_cp = $item_suc->cp_cp;
          }  
        }
      //============================================
    $data = array(
              
              "nombre"        => rtrim($razon_social),
              "Nombre_alias"  => $nombre_alias,
              "direccion"     => $direccion, 
              "cp"            => $cp,
              "rfc"           => strtoupper($rfc),
              "folio"         => $ultimoFolioval,
              "PaisReceptor"  =>$pais,
              "clienteId"       => $idcliente,
              "serie" =>'H',
              //"status"        => 1,
              "TipoComprobante"=>$TipoComprobante,
              "usuario_id"       => $this->idpersonal,
              "creada_sesionId"=> $this->idpersonal,
              "rutaXml"           => '',
              "FormaPago"         => $FormaPago,
              "tarjeta"       => '',
              "MetodoPago"        => $MetodoPago,//la forma de pago se obtiene de las diferentes que se tenga y se obtiene el de mayor valor
              "ordenCompra"   => '',//no se ocupa
              "moneda"        => 'MXN',
              "observaciones" => $resut_v->observaciones, //se agregan observacioes de la venta
              "numproveedor"  => '',
              "numordencompra"=> '',
              "Lote"          => '',//no se ocupa
              "Paciente"      => '',//no se ocupa
              "Caducidad"     => '',//no se ocupa
              "uso_cfdi"      => $uso_cfdi,
              "subtotal"      => $subtotal,
              "iva"           => $iva,
              "total"         => $importetotal,
              "honorario"     => $subtotal,
              "ivaretenido"   => 0,
              "isr"           => 0,
              "cincoalmillarval"  => 0,
              "CondicionesDePago" => '',
              "outsourcing"   => 0,
              "facturaabierta"=>1,
              "f_relacion"    => 0,
              "f_r_tipo"      => 0,
              "f_r_uuid"      => '',
              "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
              "relacion_venta_pro"=>$idventad,
              "LugarExpedicion"=>$cp_cp
          );
      if(isset($datas['pg_global'])){
        $data['pg_global']=$datas['pg_global'];
      }
      if(isset($datas['pg_periodicidad'])){
        $data['pg_periodicidad']=$datas['pg_periodicidad'];
      }
      if(isset($datas['pg_meses'])){
            $data['pg_meses']=$datas['pg_meses'];
      }
      if(isset($datas['pg_anio'])){
            $data['pg_anio']=$datas['pg_anio'];
      }    
    $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);

    foreach ($conceptos_result->result() as $itemc) {
      $idproducto=$itemc->idproducto;
      $nombre='';
      $Unidad ='H87';
      $servicioId ='10101501';
      $Descripcion ='Gatos vivos';
      if($itemc->tipo==0){//productos
        $conceptos_prot=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$itemc->idproducto));
      
        foreach ($conceptos_prot->result() as $itemp) { 
          $nombre=$itemp->idProducto." / ".$itemp->nombre; // -- se agrega codigo de producto
          if($itemp->unidad_sat!=''){
            $Unidad =$itemp->unidad_sat;
          }
          if($itemp->servicioId_sat!=''){
            $servicioId =$itemp->servicioId_sat;
            $Descripcion ='x';
            $info_ser=$this->ModeloCatalogos->getselectwheren('f_servicios',array('Clave'=>$servicioId));
            foreach ($info_ser->result() as $items) {
              $Descripcion = $items->nombre;
            }
          }
        }
      }
      if($itemc->tipo==1){//recargas -- se agrega codigo de recarga
        $conceptos_prot=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$itemc->idproducto));
        foreach ($conceptos_prot->result() as $itemp) {
          $nombre=$itemp->codigo. ' / Recarga de tanque de oxígeno de '.$itemp->capacidad.' L'; //se agrega codigo
          $dataco['servicioId']="12141904";
        }
        $servicioId="12141904";
        //recarga
        $Descripcion ='Oxígeno o'; 
        
      }
      if($itemc->tipo==2){//rentas
        $txt_periodo="";
        if($itemc->periodo==1){
          $txt_periodo="1 día";
        }if($itemc->periodo==2){
          $txt_periodo="15 días";
        }if($itemc->periodo==3){
          $txt_periodo="30 días";
        }

        $conceptos_prot=$this->ModeloCatalogos->getselectwheren('servicios',array('id'=>$itemc->id_serv));
        foreach ($conceptos_prot->result() as $itemp) {
          $nombre=$itemp->clave. ' / '.$itemp->descripcion.", con un periodo de ".$txt_periodo; //se agrega codigo
          $dataco['servicioId']="85161505";
        }
        $servicioId="85161505";
        $ivap=round($itemc->precio_unitario*0.16,5); 
        $consin_iva =1;
      }
      if($itemc->tipo==3){//servicios
          $conceptos_prot=$this->ModeloCatalogos->getselectwheren('cat_servicios',array('id'=>$itemc->idproducto));
          foreach ($conceptos_prot->result() as $items) {
            $Unidad=$items->Unidad;
            $servicioId=$items->servicioId;
            $nombre=$items->numero.' / '.$items->descripcion;
          }
          $info_ser=$this->ModeloCatalogos->getselectwheren('f_servicios',array('Clave'=>$servicioId));
          foreach ($info_ser->result() as $items) {
            $Descripcion = $items->nombre;
          }
      }
      
      //$cu=round($itemc->costo/1.16,2);
      $ivap=round($itemc->precio_unitario*0.16,5);
      if($itemc->incluye_iva==1){
        $ivap=round($itemc->precio_unitario*0.16,5);  
        $consin_iva =1;
      }else{
        $ivap=0;
        $consin_iva =0;
      }
      if($itemc->tipo==1){//recargas -- iva al  %0
        $ivap=0;
        $consin_iva=1;
      }
      $strq = "SELECT pros.*, suc.name_suc FROM productos_sucursales as pros INNER JOIN sucursal as suc on suc.id=pros.sucursalid WHERE suc.activo=1 AND pros.activo=1 AND pros.productoid='$itemc->idproducto' LIMIT 1";
      $query = $this->db->query($strq);
      foreach ($query->result() as $itemp) {
        $consin_iva=$itemp->incluye_iva;
      }

      if($itemc->tipo==1){//recargas -- iva al  %0
        $ivap=0;
        $consin_iva=1;
      }
      if($itemc->tipo==2 || $itemc->tipo==3){//rentas, mttos de servicios
        $ivap=round($itemc->precio_unitario*0.16,5); 
        $consin_iva =1;
      }
      $dataco['FacturasId']=$FacturasId;
      $dataco['Cantidad'] =$itemc->cantidad;
      $dataco['Unidad'] =$Unidad;
      $dataco['servicioId'] =$servicioId;
      $dataco['Descripcion'] =$Descripcion;
      $dataco['Descripcion2'] =$nombre;
      $dataco['Cu'] =$itemc->precio_unitario;
      $dataco['descuento'] =$itemc->descuento;;
      $dataco['Importe'] =($itemc->cantidad*$itemc->precio_unitario);
      $dataco['iva'] =$ivap;
      $dataco['consin_iva'] =$consin_iva;
      $dataco['idproducto'] =$idproducto;
      $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
    }
    $this->ModeloCatalogos->Insert('venta_fk_factura',array('idventa'=>$idventa,'idfactura'=>$FacturasId));
    $respuesta=$this->emitirfacturas($FacturasId);    
    echo json_encode($respuesta);
  }

  function generafacturarabierta(){
    $datas = $this->input->post();
    if(isset($datas['conceptos'])){
      $conceptos = $datas['conceptos'];
      unset($datas['conceptos']);
    }
    //$save=$datas['save'];
    //$saveante=$datas['saveante'];
    //log_message('error', 'save: '.$save);
    //log_message('error', 'saveante: '.$saveante);

    $idcliente=$this->idcliente;
    $rfc=str_replace(' ', '', $datas['rfc']);
    $razon_social=$datas['razon_social'];
    $cp=str_replace(' ', '', $datas['cp']);
    $direccion='';
    $idventad = $datas['idventad'];
    $uso_cfdi = $datas['uso_cfdi'];
    $RegimenFiscalReceptor = $datas['RegimenFiscalReceptor'];
    
    $pais = 'MEXICO';
    $TipoComprobante='I';
    
    $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
    
    //$conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('idventa'=>$idventa));
    $conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('id'=>$idventad));
    $importetotal=0;
    foreach ($conceptos_result->result() as $itemc) {
      $importetotal+=($itemc->cantidad*$itemc->costo);
    }
    $subtotal=round($importetotal/1.16,2);
    $iva=round($subtotal*0.16,2);

    if($rfc=='XAXX010101000'){
      $nombre_alias=$razon_social.' '.$this->succlave;
    }else{
      $nombre_alias='';
    }

    $data = array(
              
              "nombre"        => rtrim($razon_social),
              "Nombre_alias"  => $nombre_alias,
              "direccion"     => $direccion, 
              "cp"            => $cp,
              "rfc"           => strtoupper($rfc),
              "folio"         => $ultimoFolioval,
              "PaisReceptor"  =>$pais,
              "clienteId"       => $idcliente,
              "serie" =>'H',
              //"status"        => 1,
              "TipoComprobante"=>$TipoComprobante,
              "usuario_id"       => $this->idcliente,
              "creada_sesionId"=> $this->idcliente,
              "rutaXml"           => '',
              "FormaPago"         => 'PUE',
              "tarjeta"       => '',
              "MetodoPago"        => '04',//04 tarjeta
              "ordenCompra"   => '',//no se ocupa
              "moneda"        => 'MXN',
              "observaciones" => '',
              "numproveedor"  => '',
              "numordencompra"=> '',
              "Lote"          => '',//no se ocupa
              "Paciente"      => '',//no se ocupa
              "Caducidad"     => '',//no se ocupa
              "uso_cfdi"      => $uso_cfdi,
              "subtotal"      => $subtotal,
              "iva"           => $iva,
              "total"         => $importetotal,
              "honorario"     => $subtotal,
              "ivaretenido"   => 0,
              "isr"           => 0,
              "cincoalmillarval"  => 0,
              "CondicionesDePago" => '',
              "outsourcing"   => 0,
              "facturaabierta"=>1,
              "f_relacion"    => 0,
              "f_r_tipo"      => 0,
              "f_r_uuid"      => '',
              "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
              "relacion_venta_pro"=>$idventad
          );
      if(isset($datas['pg_global'])){
        $data['pg_global']=$datas['pg_global'];
      }
      if(isset($datas['pg_periodicidad'])){
        $data['pg_periodicidad']=$datas['pg_periodicidad'];
      }
      if(isset($datas['pg_meses'])){
            $data['pg_meses']=$datas['pg_meses'];
      }
      if(isset($datas['pg_anio'])){
            $data['pg_anio']=$datas['pg_anio'];
      }    
    $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);

    foreach ($conceptos_result->result() as $itemc) {
      $conceptos_prot=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$itemc->producto));
      $nombre='';
      foreach ($conceptos_prot->result() as $itemp) {
          $nombre=$itemp->nombre;
      }
      $cu=round($itemc->costo/1.16,2);
      $ivap=round($cu*0.16,2);

      $dataco['FacturasId']=$FacturasId;
      $dataco['Cantidad'] =$itemc->cantidad;
      $dataco['Unidad'] ='H87';
      $dataco['servicioId'] ='10101501';
      $dataco['Descripcion'] ='Gatos vivos';
      if($itemc->tipo==1){ //recarga
         $dataco['Descripcion'] ='Oxígeno o'; 
      }
      $dataco['Descripcion2'] =$nombre;
      $dataco['Cu'] =$cu;
      $dataco['descuento'] =0;
      $dataco['Importe'] =($itemc->cantidad*$cu);
      $dataco['iva'] =$ivap;
      if($ivap>0){
        $consin_iva=1;
      }else{
        $consin_iva=0;
      }
      if($itemc->tipo==1){//recargas -- iva al  %0
        $ivap=0;
        $consin_iva=1;
      }
      if($itemc->tipo==2 || $itemc->tipo==3){//rentas, mttos de servicios
        $ivap=round($itemc->precio_unitario*0.16,5); 
        $consin_iva =1;
      }
      $dataco['consin_iva'] =$consin_iva;
      $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
    }
    $respuesta=$this->emitirfacturas($FacturasId);
    echo json_encode($respuesta);
  }
  function generafacturarabierta2(){ 
    $datas = $this->input->post();
    $id_sucursal=0;
    if(isset($datas['conceptos'])){
          $conceptos = $datas['conceptos'];
          unset($datas['conceptos']);
    }
    $DATAc = json_decode($conceptos);
    $idcliente=$datas['idcliente'];
    $rfcid=$datas['rfc'];
    
    $resut_cli=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$idcliente));
    foreach ($resut_cli->result() as $itemc) {
        $empresa=$itemc->nombre;
    }

    $resut_df=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$rfcid));
    $resut_df=$resut_df->row();

    $razon_social=$resut_df->razon_social;
    $RegimenFiscalReceptor=$resut_df->RegimenFiscalReceptor;
    $cp = $resut_df->cp;
    $rfc = $resut_df->rfc;
    $clienteid=$resut_df->idcliente;
    if(isset($datas['pg_global'])){
      $pg_global = $datas['pg_global'];
    }else{
      $pg_global=0;
    }
      if($rfc=='XAXX010101000'){
        if(isset($datas['pg_global'])){
          if($datas['pg_global']==0){
            $razon_social=$empresa;
          }
        }else{
          $razon_social=$empresa;
        }
      }
      $pais = 'MEXICO';
      $TipoComprobante='I';
      if($datas['f_r']==1){
        if($datas['f_r_t']=='01'){
          $TipoComprobante='E';
        }
      }
      $TipoComprobante=$datas['TipoComprobante'];
      
      $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
      if(isset($datas['pg_suc'])){
          $pg_suc=$datas['pg_suc'];
      }else{
        $pg_suc='';
      }
      if($rfc=='XAXX010101000'){
        if($pg_suc==''){
          $nombre_alias=$razon_social.' '.$this->succlave;
        }else{
          $nombre_alias=$razon_social.' '.$pg_suc;
        }
        
        $cp='06300';
      }else{
        $nombre_alias='';
      }
      //=================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $cp_cp=$datosconfiguracion->CodigoPostal;

        $id_venta=0;
        for ($j=0;$j<count($DATAc);$j++) {
          if($DATAc[$j]->venta>0){
            $id_venta=$DATAc[$j]->venta;
          }
        }
        $rest_ven=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$id_venta));
        foreach ($rest_ven->result() as $itemv) {
          $id_sucursal=$itemv->sucursal;
        }
        if($id_sucursal>0){
          $resut_suc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$id_sucursal));
          foreach ($resut_suc->result() as $item_suc) {
              $cp_cp = $item_suc->cp_cp;
          }  
        }
        log_message('error','$pg_global:'.$pg_global);
        if($pg_global==1){
          $cp=$item_suc->cp_cp;
        }
      //=================================
      $data = array(
                "serie"         => 'H',
                "nombre"        => rtrim($razon_social),
                "Nombre_alias"  => $nombre_alias,
                "direccion"     => '', 
                "cp"            => $cp,
                "rfc"           => strtoupper($rfc),
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "clienteId"       => $clienteid,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "creada_sesionId"=> $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => $datas['FormaPago'],
                "tarjeta"       => $datas['tarjeta'],
                "MetodoPago"        => $datas['MetodoPago'],
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => $datas['moneda'],
                "observaciones" => $datas['observaciones'],
                "numproveedor"  => $datas['numproveedor'],
                "numordencompra"=> $datas['numordencompra'],
                "Lote"          => '',//no se ocupa
                "Paciente"      => '',//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $datas['uso_cfdi'],
                "subtotal"      => $datas['subtotal'],
                "iva"           => $datas['iva'],
                "total"         => $datas['total'],
                "honorario"     => $datas['subtotal'],
                "ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],
                "CondicionesDePago" => $datas['CondicionesDePago'],
                "outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>1,
                "f_relacion"    => $datas['f_r'],
                "f_r_tipo"      => $datas['f_r_t'],
                "f_r_uuid"      =>str_replace(' ', '', $datas['f_r_uuid']),
                "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
                "LugarExpedicion"=>$cp_cp
            
            );
      if(isset($datas['pg_global'])){
          $data['pg_global']=$datas['pg_global'];
      }
      if(isset($datas['pg_periodicidad'])){
          $data['pg_periodicidad']=$datas['pg_periodicidad'];
      }
      if(isset($datas['pg_meses'])){
          $data['pg_meses']=$datas['pg_meses'];
      }
      if(isset($datas['pg_anio'])){
          $data['pg_anio']=$datas['pg_anio'];
      }
      
      $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
      $datainsertarray=array();
      $datainsertarrayventa=array();

      
      for ($i=0;$i<count($DATAc);$i++) {
        if($DATAc[$i]->venta>0){
          $datacovr['idfactura']=$FacturasId;
          $datacovr['idventa']=$DATAc[$i]->venta;
          $datainsertarrayventa[]=$datacovr;
        }
        $dataco['FacturasId']=$FacturasId;
        $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
        $dataco['Unidad'] =$DATAc[$i]->Unidad;
        $dataco['servicioId'] =$DATAc[$i]->servicioId;
        $dataco['Descripcion'] =rtrim($DATAc[$i]->Descripcion);
        $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
        $dataco['Cu'] =$DATAc[$i]->Cu;
        $dataco['descuento'] =$DATAc[$i]->descuento;
        $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
        $dataco['iva'] =$DATAc[$i]->iva;
        if($DATAc[$i]->iva>0){
          $consin_iva=1;
        }else{
          $consin_iva=0;
        }
        if($DATAc[$i]->venta>0){
          if($consin_iva==0){
            $idventa=$DATAc[$i]->venta;
            //$resut_vd=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$DATAc[$i]->venta,'tipo'=>1));
            $strq="SELECT COUNT(*) as totalp,vd.*, GROUP_CONCAT(CASE WHEN vd.tipo = 1 THEN vd.tipo END) as tipo FROM venta_erp_detalle as vd WHERE vd.idventa='$idventa'";
            $strq="SELECT COUNT(*) as totalp,vd.*, GROUP_CONCAT(DISTINCT vd.tipo) as tipo FROM venta_erp_detalle as vd WHERE vd.idventa='$idventa'";
            $resut_vd = $this->db->query($strq);
            foreach ($resut_vd->result() as $itemvd) {
                //if($itemvd->totalp==1){
                  if($itemvd->tipo=='1'){
                    $dataco['iva']=0;
                    $consin_iva=1;
                  }
                //}
            }
          }
        }
        $dataco['consin_iva'] =$consin_iva;

        $datainsertarray[]=$dataco;
        //$this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
      }
      if(count($datainsertarray)>0){
        $this->ModeloCatalogos->insert_batch('f_facturas_servicios',$datainsertarray);
      }
      if(count($datainsertarrayventa)>0){
        $this->ModeloCatalogos->insert_batch('venta_fk_factura',$datainsertarrayventa);
      }
      
      //=======================================
      /*
      $DATAvv = json_decode($datas['ventaviculada']);
      for ($i=0;$i<count($DATAvv);$i++) {
        
        if ($DATAvv[$i]->tiporelacion>0) {
          if ($DATAvv[$i]->tiporelacion==1) {//venta normal
            $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>0,'ventaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==2) {//venta combinada
            $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>1,'ventaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==3) {//poliza
            $this->ModeloCatalogos->Insert('factura_poliza',array('polizaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==4) {
            $this->ModeloCatalogos->Insert('factura_servicio',array('servicioId'=>$DATAvv[$i]->ventaviculada,'tipo'=>$DATAvv[$i]->ventaviculadatipo,'facturaId'=>$FacturasId));
          }
        }

      }
      */
      //=======================================
      
      $respuesta=$this->emitirfacturas($FacturasId);
      
      echo json_encode($respuesta);
  }
  function emitirfacturas($facturaId){
      $productivo=$this->demoproductivo;//0 demo 1 producccion
      //$this->load->library('Nusoap');
      //require_once APPPATH."/third_party/nusoap/nusoap.php"; 
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //========================
      $datosFacturaa=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFacturaa=$datosFacturaa->result();
      $datosFacturaa=$datosFacturaa[0];
      //============
      $datosFactura = array(
        'carpeta'=>'hulesyg',
        'pwskey'=>'',
        'archivo_key'=>'',
        'archivo_cer'=>'',
        'factura_id'=>$facturaId
      );
      //================================================
        $respuesta=array();
        $passwordLlavePrivada   = $datosFactura['pwskey'];
        $nombreArchivoKey       = base_url() . $datosFactura['archivo_key'];
        $nombreArchivoCer       = base_url() . $datosFactura['archivo_cer'];
        // ---------------- INICIAN COMANDOS OPENSSL --------------------
        //Generar archivo que contendra cadena de certificado (Convertir Certificado .cer a .pem)
        $comando='openssl x509 -inform der -in '.$nombreArchivoCer.' -out ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem';    
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -startdate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/IniciaVigencia.txt';
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -enddate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/FinVigencia.txt';
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -serial > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt';
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -text > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/datos.txt';
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada_pem.txt';
        //exec($comando,$salida_script,$valor_exit);
        $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem';
        //echo '<br><br>' . $comando .'<br><br>';
        //exec($comando,$salida_script,$valor_exit);
      // ---------------- TERMINAN COMANDOS OPENSSL --------------------      
        //GUARDAR NUMERO DE SERIE EN ARCHIVO     
        //$archivo=fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt','r');
        //$numeroCertificado= fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt'));   
        $numeroCertificado=file_get_contents(FCPATH . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
        //fclose($archivo);
        $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
      
        $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
      
        $numeroCertificado="";
        $i=0;  
        foreach ($temporal as $value) {
            
            if(($i%2))
            
                $numeroCertificado .= $value;
        
            $i++;
        }
      
      $numeroCertificado = str_replace('\n','',$numeroCertificado);
      
      $numeroCertificado = trim($numeroCertificado);
      
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('certificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
      //$this->model_mango_invoice->actualizaCampoFactura('certificado',$numeroCertificado,$datosFactura['factura_id']);
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
      //$this->model_mango_invoice->actualizaCampoFactura('nocertificado',$numeroCertificado,$datosFactura['factura_id']);
      
      $respuesta['numeroCertificado']=$numeroCertificado;

      //$archivo = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem', "r");
      
      //$certificado = fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem'));
      $certificado=file_get_contents(FCPATH . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
      
      //fclose($archivo);
      $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
      
      $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
      
      $certificado=  str_replace("\n", "", $certificado);
      $certificado=  str_replace(" ", "", $certificado);
      $certificado=  str_replace("\n", "", $certificado);
      $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
      $certificado= trim($certificado);
      
      $respuesta['certificado']=$certificado;
          
      $arrayDatosFactura=  $this->getArrayParaCadenaOriginal($facturaId);  
      //=================================================================
        $horaactualm1=date('Y-m-d H:i:s');
        //$horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1 = strtotime ( '-1 hour -1 minute' , strtotime ( $horaactualm1 ) ) ;
        //$horaactualm1=date ( 'H:i:s' , $horaactualm1 );
        $datosfacturaadd['fechageneracion']=date ( 'Y-m-d' , $horaactualm1 ).'T'.date ( 'H:i:s' , $horaactualm1 );
      //==================================================================
      $idccp = $this->generateIdCCP();
      $xml=  $this->getXML($facturaId,'',$certificado,$numeroCertificado,$datosfacturaadd,$idccp);
      //echo "contruccion de xml ===========================";  //borrar despues 
      //echo $xml;
      //echo "=====";
      
      log_message('error', 'generaCadenaOriginalReal xml1: '.$xml);
      //log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');
      $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
      log_message('error', 'generaCadenaOriginalReal xml2: '.$str);
      $cadena=$str;
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('cadenaoriginal'=>$str),array('FacturasId'=>$datosFactura['factura_id']));
      //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL',array('soap_version' => SOAP_1_2)); //productivo
      //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL');//productivo
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        //$URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';//solo para pruebas ya que no tengo los accesos al demo
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      
      //$clienteSOAP = new nusoap_client('https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL');//pruebas
      $clienteSOAP = new SoapClient($URL_WS);

      //$clienteSOAP->soap_defencoding='UTF-8';
      
      $referencia=$datosFactura['factura_id']."-C-". $this->idpersonal;
      
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
        //solo para pruebas ya que no tengo los accesos al demo
        //$password= "contRa$3na";
        //$usuario= "COE960119D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
      
      
      $fp = fopen(FCPATH . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r"); 
      
      $priv_key = fread($fp, 8192); 
      
      fclose($fp); 
      
      $pkeyid = openssl_get_privatekey($priv_key);
      
      //openssl_sign($cadena, $sig, $pkeyid);
      //openssl_sign($cadena, $sig, $pkeyid,'sha256');
      openssl_sign(utf8_encode($cadena), $sig, $pkeyid,'sha256');
      
      openssl_free_key($pkeyid);
      
      $sello = base64_encode($sig);
      //echo $sello;
      $respuesta['sello']=$sello;
      
      //$this->model_mango_invoice->actualizaCampoFactura('sellocadena',$sello,$datosFactura['factura_id']);
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('sellocadena'=>$sello),array('FacturasId'=>$datosFactura['factura_id']));
      //Gererar XML 2013-12-05T12:05:56
      $xml=  $this->getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd,$idccp);                
      //$fp = fopen(DIR_FILES . $datosFactura['carpeta'] . '/facturas/preview_'. $datosFactura['rfcEmisor'].'_'. $datosFactura['folio'].'.xml', 'w');//Cadena original 
      file_put_contents($datosFactura['carpeta'].'/facturas/preview_'. $datosFacturaa->Rfc.'_'. $datosFacturaa->Folio.'.xml',$xml);
              
      //Preparar parametros para web service    
      //cadenaXML

      $result = $clienteSOAP->TimbrarCFDI(array(
                  'usuario' => $usuario,
                  'password' => $password,
                  'cadenaXML' => $xml,
                  'referencia' => $referencia));
      file_put_contents($datosFactura['carpeta'] . '/facturaslog/'.'log_facturas_result_'.$this->fechahoyL.'_.txt', json_encode($result));
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Referencia'=>$referencia),array('FacturasId'=>$datosFactura['factura_id']));
  
      if( $result->TimbrarCFDIResult->CodigoRespuesta != '0' ) {                       
          
          $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                        'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                        'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                        'facturaId'=>$facturaId,
                        'info'=>$result,
                        'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                        );
          //$this->session->data['error'] = $resultado['CodigoRespuesta'] . ' ' . $resultado['MensajeError'] . ' ' . $resultado['MensajeErrorDetallado']. ' No se puede timbrar la factura verifique la informacion por favor <BR/> PORFAVOR RETIMBRAR MAS TARDE';
                      
          //$this->load->model("setting/general");  
          
          //$this->model_setting_general->saveLog($this->session->data['id_usuario'],$this->session->data['activo'],$resultado,2);
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
          //$this->model_mango_invoice->actualizaCampoFactura('Estado','2',$datosFactura['factura_id']);
          
          //quirar restriccion de salida de sesion por error 12/02/2014 david garduño
          //$this->redirect($this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL'));
          
          return $resultado;
      
      } else {
        
          try {
         
              $xmlCompleto=str_replace(array('A\u00f1o','A?','A�o'),array('Año','Año','Año'),$result->TimbrarCFDIResult->XMLResultado);//Contiene XML  
              //Guardar en archivo     y la ruta en la bd
              $ruta = $datosFactura['carpeta'] . '/facturas/'.$datosFacturaa->Rfc.'_'.$datosFacturaa->Folio.'.xml';
          
              file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
              $this->ModeloCatalogos->updateCatalogo('f_facturas',array('rutaXml'=>$ruta),array('FacturasId'=>$datosFactura['factura_id']));     

              $xmlCompleto_2=utf8_decode(str_replace(array('A\u00f1o','A?','A�o'),array('Año','Año','Año'),$result->TimbrarCFDIResult->XMLResultado));//Contiene XML   
              $ruta_2 = $datosFactura['carpeta'] . '/facturas/'.$datosFacturaa->Rfc.'_'.$datosFacturaa->Folio.'_2.xml';
              //file_put_contents($ruta_2,"\xEF\xBB\xBF".$xmlCompleto_2);
          
              //$sxe = new SimpleXMLElement($xmlCompleto);
                       
              //$ns = $sxe->getNamespaces(true);
          
              //$sxe->registerXPathNamespace('c', $ns['cfdi']);
          
              //$sxe->registerXPathNamespace('t', $ns['tfd']);

              //$uuid = '';
          
              //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
               
                  //Actualizamos el Folio Fiscal UUID
                  //$uuid = $tfd['UUID'];
                  $updatedatossat=array(
                    'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                    'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                    'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                    'nocertificado'=>$numeroCertificado,
                    'certificado'=>$numeroCertificado,
                    'fechatimbre'=>date('Y-m-d G:i:s'),
                    'Estado'=>1
                  );
                  $this->ModeloCatalogos->updateCatalogo('f_facturas',$updatedatossat,array('FacturasId'=>$datosFactura['factura_id'])); 
              //} 
          
              //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

              $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Generada',
                        'facturaId'=>$facturaId
                        );
          return $resultado;
         }  catch (Exception $e) {
            $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                        'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                        'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                        'facturaId'=>$facturaId,
                        'info'=>$result,
                        'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                        );
           
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
          
          return $resultado;
              
         }
      
      }
      
      return $resultado;

  }
  function getArrayParaCadenaOriginal($facturaId){
    $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
    $datosFactura=$datosFactura->result();
    $datosFactura=$datosFactura[0];

    $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
    $datosconfiguracion=$datosconfiguracion->result();
    $datosconfiguracion=$datosconfiguracion[0];
    //=======================================================================
          $conceptos = array();
          $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
          foreach ($datosconceptos->result() as $item) {
            $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
            $conceptos[]    =   array(
                                     'cantidad' => $item->Cantidad,
                                     'unidad' =>  $unidad,
                                     'descripcion' =>  $item->Descripcion,
                                     'valorUnnitario' =>  $item->Cu,
                                     'importe' =>  $item->Importe
                                  );
          }
    //=======================================================================
    $datos=array(
                'comprobante' => array(
                                          'version' => '3.2',  //Requerido                     
                                          'fecha' => $this->fechahoyc,       //Requerido                
                                          'tipoDeComprobante' => $datosFactura->TipoComprobante,//Requedido                       
                                          'formaDePago' => $datosFactura->FormaPago,                       
                                          'subtotal' => $datosFactura->subtotal,                       
                                          'total' => $datosFactura->total,                       
                                          'metodoDePago' => $datosFactura->MetodoPago,                       
                                          'lugarExpedicion' => $datosconfiguracion->LugarExpedicion                       
                                      ),
                'emisor'    => array(
                                          'rfc' => $datosconfiguracion->Rfc,//Requedido
                                          'domocilioFiscal' => array(
                                                                      'calle' => $datosconfiguracion->Calle,//Requerido
                                                                      'municipio' => $datosconfiguracion->Municipio,//Requerido 
                                                                      'estado' => $datosconfiguracion->Estado,//Requerido 
                                                                      'pais' => $datosconfiguracion->PaisExpedicion,//Requedido
                                                                      'codigoPostal' => $datosconfiguracion->CodigoPostal//Requedido
                                                                      ),
                                          'expedidoEn' => array(
                                                                      'pais' => $datosconfiguracion->PaisExpedicion//Requedido
                                                               ),
                                          'regimenFiscal' => array(
                                                                      'regimen' => $datosconfiguracion->CodigoPostal//Requedido
                                                                  )
                                    ),
                'receptor'    => array(
                                          'rfc' => $datosFactura->Rfc,//Requedido
                                          'nombreCliente' => $datosFactura->Nombre,//Opcional
                                          'domicilioFiscal' => array( 
      
         
                                                                      'calle' => '',//Opcional
                                                                      'noExterior' => '',//Opcional
                                                                      'colonia' => '',//Opcional                   todos son datos del receptor
                                                                      'municipio' => '',//Opcional
                                                                      'estado' => '',//Opcional
                                                                      'pais' => $datosFactura->PaisReceptor //Requedido
                                                                     )
                                    ),
                'conceptos' => $conceptos,
           
                'traslados' => array(
                                      'impuesto' => '002',//iva
                                      'tasa' => 16.00,
                                      'importe' => $datosFactura->subtotal,
                                      'totalImpuestosTrasladados' => $datosFactura->iva
                                    )
                                    
              );
       return $datos;
  }
  public function generaCadenaOriginalReal($xml,$carpeta) {   
      error_reporting(0);
      $paso = new DOMDocument();
      //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
      $paso->loadXML($xml);
      
      $xsl = new DOMDocument();               
      
      //$file= base_url().$carpeta."/xslt33/cadenaoriginal_3_3.xslt";
      $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
      //$file="http://www.sat.gob.mx/sitio_internet/cfd/4/cadenaoriginal_4_0/cadenaoriginal_4_0.xslt";
      
      $xsl->load($file);
      
      $proc = new XSLTProcessor();
      // activar php_xsl.dll
      
      $proc->importStyleSheet($xsl); 
      
      $cadena_original = $proc->transformToXML($paso);
      
      //echo '<br><br>' . $cadena_original . '<br><br>';
      
      $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
 
      //GUARDAR CADENA ORIGINAL EN ARCHIVO  
       
      $sello = utf8_decode($str);
                      
      $sello = str_replace('#13','',$sello);
      
      $sello = str_replace('#10','',$sello);
      
      //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
      /*
      $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
      
      fwrite($fp, $sello);
      
      fclose($fp);
      */
      file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
      return $sello;    
  }
  function generateIdCCP() {
    $uuid = sprintf(
        '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
    $uuid=strtoupper($uuid);
    return 'CCC' . substr($uuid, 3, 33); // Ajustamos a 33 caracteres.
  }
  private function getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd,$idccp) {
    log_message('error', '$sello c: '.$sello);
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //==============================================================================================================

      $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFactura=$datosFactura->result();
      $datosFactura=$datosFactura[0];
      //====================================
      $clienteId=$datosFactura->clienteId;
      $cartaporte=$datosFactura->cartaporte;
      $TipoComprobante=$datosFactura->TipoComprobante;
      //$datoscliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clienteId'=>$clienteId));
      //$datoscliente=$datoscliente->result();
      //$datoscliente=$datoscliente[0];
      //===============================================================================================================
          $xmlConceptos = '';
          $conceptos = array();
          $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
          foreach ($datosconceptos->result() as $item) {
              $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
              $xmlConceptos.= '<cfdi:Concepto cantidad="' . chop(ltrim(trim($item->Cantidad))) . '" ';
              $xmlConceptos.= 'unidad="' . chop(ltrim($unidad)) . '" descripcion="' . chop(ltrim($item->servicioId)) . '"';
              $xmlConceptos.= ' valorUnitario="' . chop(ltrim(trim(round($item->Cu, 2)))) . '" importe="' . round(($item->Importe), 2) . '"/>';

            $conceptos[]    =   array(
                                     'cantidad' => $item->Cantidad,
                                     'unidad' =>  $item->Unidad,
                                     'descripcion' =>  $item->Descripcion,
                                     'valorUnnitario' =>  $item->Cu,
                                     'importe' =>  $item->Importe
                                  );
          }
      //===============================================================================================================
      
      /******************************************************/
      /*          INICIA ASIGNACION DE VARIABLES           */
      /****************************************************/
      //$certificado=str_replace("\n", "", $certificado);
      $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','"');
      $caract2  = array('&amp;','&ntilde;','&Ntilde;','','a','e','i','o','u','A','E','I','O','U','&quot;');
      $caractc   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','"');
      $caract2c  = array('','n','N','','a','e','i','o','u','A','E','I','O','U','');
      $fecha=$this->fechahoyc;
      $calleEmisor=$datosconfiguracion->Calle;
      $NoexteriorEmisor=$datosconfiguracion->Noexterior;
      $coloniaEmisor=$datosconfiguracion->Colonia;
      $localidadEmisor=$datosconfiguracion->localidad;
      $estadoEmisor=$datosconfiguracion->Estado;
      $municipioEmisor=$datosconfiguracion->Municipio;
      $codigoPostalEmisor=$datosconfiguracion->CodigoPostal;
      $formaPago=$datosFactura->FormaPago;
      $subtotal=$datosFactura->subtotal;
      if($subtotal=='0.00'){
        $subtotal=0;
      }
      $total=$datosFactura->total;
      $uso_cfdi=$datosFactura->uso_cfdi;
      $FacturasId=$datosFactura->FacturasId;
      $serie=$datosFactura->serie;
      $f_relacion=$datosFactura->f_relacion;
      $f_r_tipo=$datosFactura->f_r_tipo;
      $f_r_uuid=$datosFactura->f_r_uuid;
      $impuesto='002';
      $tasa=16.00;
      $impuestosTrasladados=$datosFactura->iva;
      if($this->trunquearredondear==0){
        $impuestosTrasladados=round($impuestosTrasladados,5);
      }else{
        $impuestosTrasladados=floor(($impuestosTrasladados*100))/100;
      }
      //$tipoComprobante='Ingreso';
      $metodoPago=$datosFactura->MetodoPago;
      $lugarExpedicion=$datosconfiguracion->LugarExpedicion;
      $rfcEmisor=$datosconfiguracion->Rfc;
      $nombreEmisor=$datosconfiguracion->Nombre;
      $nombreEmisor=strval(str_replace($caract, $caract2, $datosconfiguracion->Nombre));
      $paisEmisor=$datosconfiguracion->Pais;
      $paisExpedicion=$datosconfiguracion->PaisExpedicion;
      $regimenFiscal=$datosconfiguracion->Regimen;
      $rfcReceptor=str_replace(array(' ','&'), array('','&amp;'), $datosFactura->Rfc);
      $paisReceptor=$datosFactura->PaisReceptor;
      
      $localidadReceptor='';
      
      $xmlConceptos=$xmlConceptos;
      $nombreCliente=strval(str_replace($caract, $caract2, $datosFactura->Nombre));
      $CondicionesDePago = strval(str_replace($caractc, $caract2c, $datosFactura->CondicionesDePago));
      //$calleReceptor=$datosFactura['calleReceptor'];
      //$noExteriorReceptor=$datosFactura['noExteriorReceptor'];
      //$municipioReceptor=$datosFactura['municipioReceptor'];
      //$estadoReceptor=$datosFactura['estadoReceptor'];
      //$coloniaReceptor=$datosFactura['coloniaReceptor'];
      $folio=$datosFactura->Folio;

      $isr=$datosFactura->isr;
      $ivaretenido=$datosFactura->ivaretenido;
      $cedular=$datosFactura->cedular;
      $cincoalmillarval=$datosFactura->cincoalmillarval;
      $outsourcing=$datosFactura->outsourcing;
      $totalimpuestosretenido=$isr+$ivaretenido+$outsourcing;
      if($datosFactura->LugarExpedicion!=null and $datosFactura->LugarExpedicion!='null' and $datosFactura->LugarExpedicion!=''){
        $codigoPostalEmisor=$datosFactura->LugarExpedicion;
      }
      if ($totalimpuestosretenido==0) {
          $TotalImpuestosRetenidoss='';
      }else{
          $TotalImpuestosRetenidoss='TotalImpuestosRetenidos="'.round($totalimpuestosretenido,2).'"';
      }
      if ($cedular!=0) {
          $impuestoslocal=' xmlns:implocal="http://www.sat.gob.mx/implocal" ';
          $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd ';
      }else{
          $impuestoslocal=' ';
          $impuestoslocal2=' ';
      }
      if ($cincoalmillarval>0) {
          $impuestoslocal=' xmlns:implocal="http://www.sat.gob.mx/implocal" ';
          $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd ';
      }else{
          $impuestoslocal=' ';
          $impuestoslocal2=' ';
      }

      
      //cambios para poner el acr?nimo de la moneda en el XML
      if ($datosFactura->moneda=="USD") {
          $moneda="USD";
          $TipoCambio='TipoCambio="19.35"';
      }elseif ($datosFactura->moneda=="EUR") {
          $moneda="EUR";
          $TipoCambio='';
      }else{
          $moneda="MXN";
          $TipoCambio='';
      }
      //DATOS DE TIMBRE
      if(isset($datosFactura->uuid)) { 
      
          $uuid = $datosFactura->uuid;
          
      } else {
          
          $uuid= '';   
      }
      if(isset($datosFactura->sellosat)) {
          
          $selloSAT = $datosFactura->sellosat;
      } else {
          
          $selloSAT = '';
      }
      if(isset($datosFactura->certificado)) {   
          $certificadoSAT=$datosFactura->certificado;
      } else {
          $certificadoSAT='';
      }
      /*
      if(!empty($datosFactura['noInteriorReceptor'])) {   
          $noInt = '" noInterior="' . $datosFactura['noInteriorReceptor'] . '';
      } else {
          
          $noInt = '';
      }
      */
      /*  $minuto=date('i');
      $m=$minuto-1;
      $contar=strlen($m);
      if($contar==1)
      {
      $min='0'.$m;
      }else{$min=$m;} */  
      
      $importabasetotal=0;
      
      //.date('Y-m-d').'T'.date('H:i').':'.$segundos.
      
      /******************************************************/
      /*          TERMINA ASIGNACION DE VARIABLES          */
      /****************************************************/   
      /*
      $horaactualm1=date('H:i:s');
      $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
      $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
      */
      $factura_detalle = $this->ModeloCatalogos->traeProductosFactura($FacturasId); 
      $descuentogeneral=0;
      foreach ($factura_detalle->result() as $facturadell) {
        $descuentogeneral=$descuentogeneral+$facturadell->descuento;
      }

      if ($descuentogeneral>0) {
        $xmldescuentogene=' Descuento="'.$descuentogeneral.'" ';
      }else{
        $xmldescuentogene='';
      }
      if($rfcReceptor=='XAXX010101000'){
        //$DomicilioFiscalReceptor=$datosconfiguracion->CodigoPostal;
        $DomicilioFiscalReceptor=$codigoPostalEmisor;
      }else{
        $DomicilioFiscalReceptor=$datosFactura->Cp;
      }
      if($datosFactura->pg_global==1){
        $DomicilioFiscalReceptor=$codigoPostalEmisor;
      }
      if($cartaporte==1){
          $cp20=' xmlns:cartaporte20="http://www.sat.gob.mx/CartaPorte20" ';
          $cp20=' xmlns:cartaporte31="http://www.sat.gob.mx/CartaPorte31" ';
          $cp20_2=' http://www.sat.gob.mx/CartaPorte20 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte20.xsd ';
          $cp20_2=' http://www.sat.gob.mx/CartaPorte31 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte31.xsd';

          $moneda='XXX';
        }else{
          $cp20=' ';
          $cp20_2='';
        }
      $fechageneracion=$datosfacturaadd['fechageneracion'];
      $razon_social_receptor=str_replace(array('&','ñ','"'),array('&amp;','&ntilde;','&quot;'), $datosFactura->Nombre);
      $xml= '<?xml version="1.0" encoding="utf-8"?>';
      $xml.='<cfdi:Comprobante'.$cp20.'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'.$impuestoslocal.'xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd'.$impuestoslocal2.$cp20_2.'" Exportacion="01" ';
      $xml .= 'Version="4.0" Serie="'.$serie.'" Folio="'.$folio.'" Fecha="'.$fechageneracion.'" ';
      if($cartaporte!=1){
        $xml .= 'FormaPago="'.$metodoPago.'" MetodoPago="'.$formaPago.'" ';
      }
      if($CondicionesDePago!=''){
          $xml .= 'CondicionesDePago="'.$CondicionesDePago.'" ';
      }
      //$xml .= 'SubTotal="'.$subtotal.'" Descuento="0.00" Moneda="'.$moneda.'" TipoCambio="1" Total="'.$total.'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
      $xml .= 'SubTotal="'.$subtotal.'" Moneda="'.$moneda.'"'.$xmldescuentogene.''.$TipoCambio.' Total="'.round($total,2).'" TipoDeComprobante="'.$TipoComprobante.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
      $xml .='xmlns:cfdi="http://www.sat.gob.mx/cfd/4" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" Sello="'.$sello.'">';
              if($f_relacion==1){
                  $xml .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                              <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                          </cfdi:CfdiRelacionados>';
              }
              if($rfcReceptor=='XAXX010101000' and $datosFactura->pg_global==1){
                  $xml .= '<cfdi:InformacionGlobal Año="'.$datosFactura->pg_anio.'" Meses="'.$datosFactura->pg_meses.'" Periodicidad="'.$datosFactura->pg_periodicidad.'"/>';
              }
      $xml .= '<cfdi:Emisor Nombre="'.$nombreEmisor.'" RegimenFiscal="'.$regimenFiscal.'" Rfc="'.$rfcEmisor.'"></cfdi:Emisor>';
      $xml .= '<cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razon_social_receptor.'" DomicilioFiscalReceptor="'.$DomicilioFiscalReceptor.'" RegimenFiscalReceptor="'.$datosFactura->RegimenFiscalReceptor.'" UsoCFDI="'.$uso_cfdi.'"></cfdi:Receptor>';
      $xml .='<cfdi:Conceptos>';
      
      //$isr=$datosFactura['isr'];
      //$ivaretenido=$datosFactura['ivaretenido'];
      //$cedular=$datosFactura['cedular'];

      
      $partidaproducto=1;
      $niva=$tasa/100;
      $comparariva = $datosFactura->iva;//colocar
      //log_message('error','$comparariva = '.$comparariva);
      $consin_iva_g=0;
      foreach ($factura_detalle->result() as $facturadell) {
          if($facturadell->consin_iva==1){
            $consin_iva_g=1;
          }
          $valorunitario=$facturadell->Cu;
          if($facturadell->descuento>0){
            $xmldescuento=' Descuento="'.$facturadell->descuento.'" ';
          }else{
            $xmldescuento='';
          }
          if($facturadell->Importe==$facturadell->descuento){
              $ObjetoImp='01';//no objeto de impuesto
          }else{
              $ObjetoImp='02';//si, objeto de impuestos
              log_message('error','$facturadell->iva = '.$facturadell->iva);
              if($facturadell->iva>0){
                
              }else{
                $ObjetoImp='01';//no objeto de impuesto
                if($facturadell->consin_iva==1){
                  $ObjetoImp='02';//si es objeto de impuesto pero en 0%
                }
              }
          }
          if($rfcReceptor=='XAXX010101000'){
              $xml_unidad=' ';
          }else{
              $xml_unidad=' Unidad="'.$facturadell->nombre.'" ';
          }
          
          //$xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell['cunidad'].'" Unidad="'.$facturadell['nombre'].'" ClaveProdServ="'.$facturadell['ClaveProdServ'].'" NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" Cantidad="'.$facturadell['Cantidad'].'" Descripcion="'.$facturadell['Descripcion'].'" ValorUnitario="'.$facturadell['Cu'].'" Importe="'.$facturadell['Importe'].'" Descuento="0.00">';
          $xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell->cunidad.'" ClaveProdServ="'.$facturadell->ClaveProdServ.'"';
           //$xml.='NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" ';
           $xml.=' Cantidad="'.$facturadell->Cantidad.'"'.$xml_unidad.'Descripcion="'.utf8_encode(str_replace($caractc, $caract2c, $facturadell->Descripcion)).'" ValorUnitario="'.$valorunitario.'" Importe="'.round($facturadell->Importe,5).'"'.$xmldescuento.' ObjetoImp="'.$ObjetoImp.'">';
            if($comparariva>0 or $facturadell->iva==0){
              //if($facturadell->iva>0){
                $importabase=$facturadell->Importe-$facturadell->descuento;
                if($this->trunquearredondear==0){
                  $trasladadoimporte=round($importabase*$niva,5);
                }else{
                  $trasladadoimporte=$importabase*$niva;
                  $trasladadoimporte=floor(($trasladadoimporte*100))/100;
                }
                //log_message('error','$importabase = '.$importabase.' $facturadell->iva = '.$facturadell->iva);
              if($comparariva>0 and $ObjetoImp=='02'){
                if($importabase>0 or $facturadell->iva==0){
                  $xml .='<cfdi:Impuestos>';
                      $xml .='<cfdi:Traslados>';
                          $importabasetotal=$importabasetotal+$importabase;
                          if($facturadell->iva>0){
                              $xml .='<cfdi:Traslado Base="'.round($importabase,5).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="'.$niva.'0000" Importe="'.$trasladadoimporte.'"></cfdi:Traslado>';
                          }else{
                              $importabasetotal=$importabasetotal-$importabase;
                              
                              if($facturadell->consin_iva==1){
                                $xml .='<cfdi:Traslado Base="'.round($importabase,5).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.000000" Importe="0"></cfdi:Traslado>';
                              }else{
                                $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Exento" ></cfdi:Traslado>';
                              }
                          }
                      $xml .='</cfdi:Traslados>';
                      //-----------------------------------------------------------------------------------
                      if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                          $xml .='<cfdi:Retenciones>';
                          if ($isr!=0) {
                              $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.100000" Importe="'.round($facturadell->Importe*0.100000,2).'"></cfdi:Retencion>';
                          }
                          if ($ivaretenido!=0) {
                              $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.106666" Importe="'.round($facturadell->Importe*0.106666,2).'"></cfdi:Retencion>'; 
                          }   
                          if ($outsourcing!=0) {
                              $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.060000" Importe="'.round($facturadell->Importe*0.06,2).'"></cfdi:Retencion>'; 
                          }      
                          $xml .='</cfdi:Retenciones>';
                      }
                      //------------------------------------------------------------------------------------- 
                      //$xml .='<cfdi:Retenciones>';
                      //    $xml .='<cfdi:Retencion Base="1000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="0.00" />';      
                      //$xml .='</cfdi:Retenciones>';
                  $xml .='</cfdi:Impuestos>';
                }
              }elseif($facturadell->consin_iva==1){//esta parte es para iva al 0%
                $importabasetotal=$importabasetotal+$importabase;
                $xml .='<cfdi:Impuestos>';
                      $xml .='<cfdi:Traslados>';
                              $xml .='<cfdi:Traslado Base="'.round($importabase,5).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.000000" Importe="0"></cfdi:Traslado>';
                      $xml .='</cfdi:Traslados>';
                  $xml .='</cfdi:Impuestos>';
              }
            }elseif($facturadell->consin_iva==1){
                  //2024-01-10 se agrego por si es unido producto no entra en el anterior
                  $importabasetotal=$importabasetotal+$facturadell->Importe;
                  $xml .='<cfdi:Impuestos>';
                      $xml .='<cfdi:Traslados>';
                              $xml .='<cfdi:Traslado Base="'.round($facturadell->Importe,5).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.000000" Importe="0"></cfdi:Traslado>';
                      $xml .='</cfdi:Traslados>';
                  $xml .='</cfdi:Impuestos>';
            }
          $xml .='</cfdi:Concepto>';
          $partidaproducto++;
      }
      $xml .='</cfdi:Conceptos>';

      if($comparariva>0 or $consin_iva_g==1){
          //$xml .='<cfdi:Impuestos TotalImpuestosRetenidos="0.00" TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
          $xml .='<cfdi:Impuestos '.$TotalImpuestosRetenidoss.' TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
              if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                  $xml .='<cfdi:Retenciones>';
                  if ($isr!=0) {
                      $xml .='<cfdi:Retencion Impuesto="001" Importe="'.number_format(round($isr,2), 2, ".", "").'"></cfdi:Retencion>';
                  }
                  if ($ivaretenido!=0) {
                      $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($ivaretenido,2), 2, ".", "").'"></cfdi:Retencion>'; 
                  } 
                  if ($outsourcing>0) {
                      $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($outsourcing,2), 2, ".", "").'"></cfdi:Retencion>'; 
                  }      
                  $xml .='</cfdi:Retenciones>';
              }
             
              
              
              $xml .='<cfdi:Traslados>';
                  if($comparariva>0){
                    $xml .='<cfdi:Traslado Base="'.round($importabasetotal,2).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.round($impuestosTrasladados,2).'"></cfdi:Traslado>';
                  }elseif($comparariva==0 and $consin_iva_g==1){
                    $xml .='<cfdi:Traslado Base="'.round($importabasetotal,2).'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.000000" Importe="0.00"></cfdi:Traslado>';
                  }
              $xml .='</cfdi:Traslados>';
          $xml .='</cfdi:Impuestos>';
      }
      /*
      if($cedular!=0){
          
      }
      */
      if($cedular!=0||$uuid||$cincoalmillarval>0||$cartaporte==1) {            
          $xml .= '<cfdi:Complemento>';
          if($cartaporte==1){    //true incluye carta porte false la descarta     
            //=========================================================
              $datosFactura_cp=$this->ModeloCatalogos->getselectwheren('f_facturas_carta_porte',array('FacturasId'=>$facturaId));
              $datosFactura_cp=$datosFactura_cp->row();
              $datosFactura_u=$this->ModeloCatalogos->getselectwheren('f_facturas_ubicaciones',array('FacturasId'=>$facturaId));
              $datosFactura_fms=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancias',array('FacturasId'=>$facturaId));
              $datosFactura_fms=$datosFactura_fms->row();
              $datosFactura_fm=$this->ModeloCatalogos->getselectwheren('f_facturas_mercancia',array('FacturasId'=>$facturaId));
              $datosFactura_tf=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_fed',array('FacturasId'=>$facturaId));
              $datosFactura_ta=$this->ModeloCatalogos->getselectwheren('f_facturas_transporte_aereo',array('FacturasId'=>$facturaId));
              $datosFactura_fa=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_arrendatario',array('FacturasId'=>$facturaId));
              $datosFactura_fo=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_operadores',array('FacturasId'=>$facturaId));
              $datosFactura_fp=$this->ModeloCatalogos->getselectwheren('f_facturas_figtrans_propietario',array('FacturasId'=>$facturaId));
              $datosFactura_ft=$this->ModeloCatalogos->getselectwheren('f_facturas_figura_transporte',array('FacturasId'=>$facturaId));
              $datosFactura_ft=$datosFactura_ft->row();
            //==================================================================
                        
                        $xmlcp='<cartaporte31:CartaPorte';
                                    if($datosFactura_cp->TotalDistRec!=''){ $xmlcp.=' TotalDistRec="'.intval($datosFactura_cp->TotalDistRec).'" ';}
                                    //if($datosFactura_cp->EntradaSalidaMerc!=''){ $xmlcp.=' EntradaSalidaMerc="'.$datosFactura_cp->EntradaSalidaMerc.'" ';}
                                    $xmlcp.='TranspInternac="'.$datosFactura_cp->TranspInternac.'" ';
                                    $xmlcp.='Version="3.1" IdCCP="'.$idccp.'">';
                        $xmlcp.='<cartaporte31:Ubicaciones>';
                            foreach ($datosFactura_u->result() as $dF_u_row) {
                                $xmlcp.='<cartaporte31:Ubicacion ';
                                        //if($dF_u_row->TipoEstacion!=''){$xmlcp.='TipoEstacion="'.$dF_u_row->TipoEstacion.'" ';}//
                                        //if($dF_u_row->NumEstacion!=''){ $xmlcp.='NumEstacion="'.$dF_u_row->NumEstacion.'" ';}
                                        //if($dF_u_row->NombreEstacion!=''){$xmlcp.='NombreEstacion="'.$dF_u_row->NombreEstacion.'" ';}
                                        $xmlcp.='FechaHoraSalidaLlegada="'.$dF_u_row->FechaHoraSalidaLlegada.'" ';
                                        //$xmlcp.='IDUbicacion="'.$dF_u_row->IDUbicacion.'" ';
                                        $xmlcp.='RFCRemitenteDestinatario="'.$dF_u_row->RFCRemitenteDestinatario.'" ';
                                        $xmlcp.='NombreRemitenteDestinatario="'.strval(str_replace($caract, $caract2, $dF_u_row->NombreRFC)).'" ';
                                        $xmlcp.='TipoUbicacion="'.$dF_u_row->TipoUbicacion.'" ';
                                        
                                        if($dF_u_row->DistanciaRecorrida>0){
                                            $xmlcp.='DistanciaRecorrida="'.intval($dF_u_row->DistanciaRecorrida).'" ';
                                        }
                                        if($dF_u_row->NumRegIdTrib!=''){
                                            //$xmlcp.='NumRegIdTrib="'.$dF_u_row->NumRegIdTrib.'" ';
                                        }
                                        if($dF_u_row->ResidenciaFiscal!=''){
                                            //$xmlcp.='ResidenciaFiscal="'.$dF_u_row->ResidenciaFiscal.'" ';
                                        }
                                        $xmlcp.='>';

                                    $xmlcp.='<cartaporte31:Domicilio ';
                                                $xmlcp.='Calle="'.$dF_u_row->Calle.'" ';
                                                $xmlcp.='CodigoPostal="'.$dF_u_row->CodigoPostal.'" ';
                                                if($dF_u_row->Colonia!=''){$xmlcp.='Colonia="'.$dF_u_row->Colonia.'" ';}
                                                $xmlcp.='Estado="'.$dF_u_row->Estado.'" ';
                                                if($dF_u_row->Localidad!=''){$xmlcp.='Localidad="'.$dF_u_row->Localidad.'" ';}
                                                if($dF_u_row->Municipio!=''){$xmlcp.='Municipio="'.$dF_u_row->Municipio.'" ';}
                                                if($dF_u_row->NumeroExterior!=''){$xmlcp.='NumeroExterior="'.$dF_u_row->NumeroExterior.'" ';}
                                                if($dF_u_row->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_u_row->NumeroInterior.'" ';}
                                                $xmlcp.='Pais="'.$dF_u_row->Pais.'" ';
                                    $xmlcp.='/>';
                                $xmlcp.='</cartaporte31:Ubicacion>';
                            }
                        $xmlcp.='</cartaporte31:Ubicaciones>';
                        $xmlcp.='<cartaporte31:Mercancias ';
                                $xmlcp.='PesoBrutoTotal="'.$datosFactura_fms->PesoBrutoTotal.'" ';
                                $xmlcp.='UnidadPeso="'.$datosFactura_fms->UnidadPeso.'" ';
                                $xmlcp.='NumTotalMercancias="'.$datosFactura_fms->NumTotalMercancias.'" ';
                                //if($datosFactura_fms->CargoPorTasacion>0){$xmlcp.='CargoPorTasacion="'.$datosFactura_fms->CargoPorTasacion.'" ';}
                        $xmlcp.='>';
                            foreach ($datosFactura_fm->result() as $dF_fm_row) {
                                $xmlcp.='<cartaporte31:Mercancia ';
                                    if($dF_fm_row->BienesTransp!=''){$xmlcp.='BienesTransp="'.$dF_fm_row->BienesTransp.'" ';}
                                    $xmlcp.='Descripcion="'.strval(str_replace($caractc, $caract2c, $dF_fm_row->Descripcion)).'" ';
                                    $xmlcp.='Cantidad="'.$dF_fm_row->Cantidad.'" ';
                                    $xmlcp.='ClaveUnidad="'.$dF_fm_row->ClaveUnidad.'" ';
                                    //if($dF_fm_row->Dimensiones!=''){$xmlcp.='Dimensiones="'.$dF_fm_row->Dimensiones.'" ';}
                                    //if($dF_fm_row->Embalaje!=''){$xmlcp.='Embalaje="'.$dF_fm_row->Embalaje.'" ';}
                                    //if($dF_fm_row->DescripEmbalaje!=''){$xmlcp.='DescripEmbalaje="'.$dF_fm_row->DescripEmbalaje.'" ';}
                                    //if($dF_fm_row->ValorMercancia>0){$xmlcp.='ValorMercancia="'.$dF_fm_row->ValorMercancia.'" ';}
                                    //if($dF_fm_row->Unidad!=''){$xmlcp.='Unidad="'.$dF_fm_row->Unidad.'" ';}
                                    $xmlcp.='PesoEnKg="'.$dF_fm_row->PesoEnKg.'" ';
                                    //$xmlcp.='Moneda="'.$dF_fm_row->Moneda.'" ';
                                    //$xmlcp.='MaterialPeligroso="No" ';
                                $xmlcp.='>';
                                    //$xmlcp.='<cartaporte20:CantidadTransporta Cantidad="'.$dF_fm_row->Cantidad.'" IDOrigen="'.$IDOrigen.'" IDDestino="'.$IDDestino.'" />';
                                $xmlcp.='</cartaporte31:Mercancia>';
                            }
                            foreach ($datosFactura_tf->result() as $dF_tf_row) {
                                $xmlcp.='<cartaporte31:Autotransporte ';
                                        $xmlcp.='PermSCT="'.$dF_tf_row->PermSCT.'" ';
                                        $xmlcp.='NumPermisoSCT="'.$dF_tf_row->NumPermisoSCT.'">';
                                    $xmlcp.='<cartaporte31:IdentificacionVehicular ';
                                        $xmlcp.='ConfigVehicular="'.$dF_tf_row->ConfigVehicular.'" ';
                                        $xmlcp.='PlacaVM="'.$dF_tf_row->PlacaVM.'" ';
                                        $xmlcp.='AnioModeloVM="'.$dF_tf_row->AnioModeloVM.'" PesoBrutoVehicular="2"/>';
                                    $xmlcp.='<cartaporte31:Seguros ';
                                            $xmlcp.='AseguraCarga="'.$dF_tf_row->NombreAseg.'" ';
                                            $xmlcp.='AseguraRespCivil="'.$dF_tf_row->NombreAseg.'" ';
                                            $xmlcp.='PolizaRespCivil="'.$dF_tf_row->NumPolizaSeguro.'" />';
                                
                                //$xmlcp.='<cartaporte20:Remolques><cartaporte20:Remolque Placa="'.$dF_tf_row->Placa.'" SubTipoRem="'.$dF_tf_row->SubTipoRem.'"/></cartaporte20:Remolques>';
                                $xmlcp.='</cartaporte31:Autotransporte>';
                            }
                            /*
                            foreach($datosFactura_ta->result() as $dF_ta_row){
                                $xmlcp.='<cartaporte20:TransporteAereo ';
                                    $xmlcp.='CodigoTransportista="'.$dF_ta_row->CodigoTransportista.'" ';
                                    if($dF_ta_row->LugarContrato!=''){$xmlcp.='LugarContrato="'.$dF_ta_row->LugarContrato.'" ';}
                                    $xmlcp.='MatriculaAeronave="'.$dF_ta_row->MatriculaAeronave.'" ';
                                    $xmlcp.='NombreAseg="'.$dF_ta_row->NombreAseg.'" ';
                                    
                                    $xmlcp.='NumPermisoSCT="'.$dF_ta_row->NumPermisoSCT.'" ';
                                    $xmlcp.='NumPolizaSeguro="'.$dF_ta_row->NumPolizaSeguro.'" ';
                                    $xmlcp.='NumeroGuia="'.$dF_ta_row->NumeroGuia.'" ';
                                    $xmlcp.='PermSCT="'.$dF_ta_row->PermSCT.'" ';

                                    if($dF_ta_row->NumRegIdTribTranspor!=''){$xmlcp.='NumRegIdTribTranspor="'.$dF_ta_row->NumRegIdTribTranspor.'" ';}
                                    //if($dF_ta_row['ResidenciaFiscalTranspor']!=''){$xmlcp.='ResidenciaFiscalTranspor="'.$dF_ta_row['ResidenciaFiscalTranspor'].'" ';}
                                    //if($dF_ta_row['NombreTransportista']!=''){$xmlcp.='NombreTransportista="'.$dF_ta_row['NombreTransportista'].'" ';}
                                    if($dF_ta_row->NumRegIdTribEmbarc!=''){$xmlcp.='NumRegIdTribEmbarc="'.$dF_ta_row->NumRegIdTribEmbarc.'" ';}
                                    if($dF_ta_row->RFCEmbarcador!=''){$xmlcp.='RFCEmbarcador="'.$dF_ta_row->RFCEmbarcador.'" ';}
                                    if($dF_ta_row->ResidenciaFiscalEmbarc!=''){$xmlcp.='ResidenciaFiscalEmbarc="'.$dF_ta_row->ResidenciaFiscalEmbarc.'" ';}
                                    if($dF_ta_row->NombreEmbarcador!=''){$xmlcp.='NombreEmbarcador="'.$dF_ta_row->NombreEmbarcador.'" ';}
                                $xmlcp.='/>';
                            }
                            */
                        $xmlcp.='</cartaporte31:Mercancias>';
                        $FiguraTransporte_row=0;
                        foreach($datosFactura_fa as $dF_fa){
                            $FiguraTransporte_row++;
                        }
                        foreach($datosFactura_fo as $dF_fo){
                            $FiguraTransporte_row++;
                        }
                        foreach($datosFactura_fp as $dF_fp){
                            $FiguraTransporte_row++;
                        }
                        if($FiguraTransporte_row>0){
                            $xmlcp.='<cartaporte31:FiguraTransporte>';
                            foreach($datosFactura_fo->result() as $dF_fo){
                                $xmlcp.='<cartaporte31:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fo->NombreOperador.'" ';
                                    if($dF_fo->NumLicencia!=''){$xmlcp.='NumLicencia="'.$dF_fo->NumLicencia.'" ';}
                                    $xmlcp.='RFCFigura="'.preg_replace("[\n|\r|\n\r]", "",strval(str_replace(' ', '', $dF_fo->RFCOperador))).'" ';
                                    //if($dF_fo->NumRegIdTribOperador!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fo->NumRegIdTribOperador.'" ';}
                                    //if($dF_fo->ResidenciaFiscalOperador!=''){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fo->ResidenciaFiscalOperador.'" ';}
                                    $xmlcp.='TipoFigura="01"/>';
                                    if($dF_fo->adddomicilio==1){
                                        $xmlcp.='<cartaporte31:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fo->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fo->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fo->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fo->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fo->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fo->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fo->NumeroExterior.'" ';
                                            $xmlcp.='NumeroInterior="'.$dF_fo->NumeroInterior.'" ';
                                            $xmlcp.='Pais="'.$dF_fo->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                            }
                            foreach($datosFactura_fa->result() as $dF_fa){
                                $xmlcp.='<cartaporte31:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fa->NombreArrendatario.'" ';
                                    $xmlcp.='RFCFigura="'.strval(str_replace(' ', '', $dF_fa->RFCArrendatario)).'" ';
                                    if($dF_fa->NumRegIdTribArrendatario!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fa->NumRegIdTribArrendatario.'" ';}
                                    if($dF_fa->ResidenciaFiscalOperador!='' and $dF_fa->ResidenciaFiscalOperador!='null'){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fa->ResidenciaFiscalOperador.'" ';}
                                    $xmlcp.='TipoFigura="03"/>';
                                if($dF_fa->adddomicilio==1){
                                        $xmlcp.='<cartaporte31:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fa->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fa->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fa->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fa->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fa->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fa->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fa->NumeroExterior.'" ';
                                            if($dF_fa->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_fa->NumeroInterior.'" ';}
                                            $xmlcp.='Pais="'.$dF_fa->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                            }
                            
                            foreach($datosFactura_fp->result() as $dF_fp){
                                $xmlcp.='<cartaporte31:TiposFigura ';
                                    $xmlcp.='NombreFigura="'.$dF_fp->NombrePropietario.'" ';
                                    $xmlcp.='RFCFigura="'.strval(str_replace(' ', '', $dF_fp->RFCPropietario)).'" ';
                                    if($dF_fp->NumRegIdTribPropietario!=''){$xmlcp.='NumRegIdTribFigura="'.$dF_fp->NumRegIdTribPropietario.'" ';}
                                    if($dF_fp->ResidenciaFiscalPropietario!=''){$xmlcp.='ResidenciaFiscalFigura="'.$dF_fp->ResidenciaFiscalPropietario.'" ';}
                                    $xmlcp.='TipoFigura="02">';
                                if($datosFactura_ft->CveTransporte=='03'){
                                    $xmlcp.='<cartaporte20:PartesTransporte ParteTransporte="PT08"/>';
                                }
                                    if($dF_fp->adddomicilio==1){
                                        $xmlcp.='<cartaporte31:Domicilio ';
                                            $xmlcp.='Calle="'.$dF_fp->Calle.'" ';
                                            $xmlcp.='CodigoPostal="'.$dF_fp->CodigoPostal.'" ';
                                            $xmlcp.='Colonia="'.$dF_fp->Colonia.'" ';
                                            $xmlcp.='Estado="'.$dF_fp->Estado.'" ';
                                            $xmlcp.='Localidad="'.$dF_fp->Localidad.'" ';
                                            $xmlcp.='Municipio="'.$dF_fp->Municipio.'" ';
                                            $xmlcp.='NumeroExterior="'.$dF_fp->NumeroExterior.'" ';
                                            if($dF_fp->NumeroInterior!=''){$xmlcp.='NumeroInterior="'.$dF_fp->NumeroInterior.'" ';}
                                            $xmlcp.='Pais="'.$dF_fp->Pais.'" ';
                                            $xmlcp.='/>';
                                    }
                                $xmlcp.='</cartaporte31:TiposFigura>';
                                
                            }
                                
                            $xmlcp.='</cartaporte31:FiguraTransporte>';
                        }
                        $xmlcp.='</cartaporte31:CartaPorte>';
                        $xml.=$xmlcp;
          }
          if ($cedular!=0) {
              //$xml .= '<cfdi:Complemento xmlns:cfdi="http://www.sat.gob.mx/cfd/3" >';
              $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cedular.'" >';
              $xml .= '<implocal:RetencionesLocales  TasadeRetencion="01.00" ImpLocRetenido="CEDULAR" Importe="'.$cedular.'"/>';
              $xml .= '</implocal:ImpuestosLocales>';
          }
          if ($cincoalmillarval>0) {
              $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cincoalmillarval.'" >';
                $xml .= '<implocal:RetencionesLocales Importe="'.$cincoalmillarval.'" TasadeRetencion="0.50" ImpLocRetenido=".005 Insp y Vig" />';
              $xml .= '</implocal:ImpuestosLocales>';
          }
          if ($uuid) {
              $xml .='<tfd:TimbreFiscalDigital Version="1.1" RfcProvCertif="FLI081010EK2"';
              $xml .='UUID="'.$uuid.'" FechaTimbrado="'.$fecha.'" ';
              $xml .='SelloCFD="'.$sello.'" NoCertificadoSAT="'.$certificadoSAT.'" SelloSAT="'.$selloSAT.'"';
              $xml .='xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"';
              $xml .= 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" />';
          }
          







              
          $xml .= '</cfdi:Complemento>';
      }
      
      $xml .= '</cfdi:Comprobante>';
      
     
      return $xml;
  }
  /*
  function cancelarCfdi(){
    $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    $ruta=$rutainterna.'/hulesyg/temporalsat/';
    $rutaf=$rutainterna.'/hulesyg/facturas/';
    $rutalog=$rutainterna.'/hulesyg/facturaslog/';

    $productivo=1;//0 demo 1 producccion
    $datas = $this->input->post();
    $facturas=$datas['facturas'];
    unset($datas['facturas']);

    $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
    $datosconfiguracion=$datosconfiguracion->result();
    $datosconfiguracion=$datosconfiguracion[0];
    $rFCEmisor=$datosconfiguracion->Rfc;
    $passwordClavePrivada=$datosconfiguracion->paswordkey;

    $datosCancelacion=array();
    $DATAf = json_decode($facturas);
    for ($i=0;$i<count($DATAf);$i++) {
      $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
      $datosfactura=$datosfactura->result();
      $datosfactura=$datosfactura[0];
      $FacturasId=$datosfactura->FacturasId;
      $datosCancelacion[] = array(
                                'Propiedad'=>'cancelacion',
                                'RFCReceptor'=>$datosfactura->Rfc,
                                'Total'=>round($datosfactura->total, 2),
                                'UUID'=>$datosfactura->uuid

                              );
    }
    //===========================================================
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
    //=======================================================
      
      $fp = fopen($ruta.'pfx.pem', "r");
      $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
      fclose($fp);
      
      
      $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
      $rutapass=$rutaarchivos.'passs.txt';
      $passwordClavePrivada = file_get_contents($rutapass);
      
      //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
      
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'rFCEmisor' => $rFCEmisor,
          'listaCFDI' => $datosCancelacion,
          'clavePrivada_Base64'=>$clavePrivada,
          'passwordClavePrivada'=> $passwordClavePrivada  );
      file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
      $clienteSOAP = new SoapClient($URL_WS);

      $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

      if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
        $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Cancelada',
                        'info'=>$result
                        );
        //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
              $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
              $ruta = $rutaf .$fileacuse;
              //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
              file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

              $datosdecancelacion = array(
                                          'Estado'=>0,
                                          'rutaAcuseCancelacion '=>$fileacuse,
                                          'fechacancelacion'=>$this->fechahoyc
                                        );
              $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
        
      }else{
        $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                        'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                        'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                        'info'=>$result
                        );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
      }
      
      echo json_encode($resultado);
  }
  */
  function cancelarCfdi(){
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/semit_erp';//rutalocal
    $rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    $ruta=$rutainterna.'/hulesyg/temporalsat/';
    $rutaf=$rutainterna.'/hulesyg/facturas/';
    $rutalog=$rutainterna.'/hulesyg/facturaslog/';

    $productivo=$this->demoproductivo;//0 demo 1 producccion
    $datas = $this->input->post();
    $facturas=$datas['facturas'];
    $motivo=$datas['motivo'];
    $uuidrelacionado=$datas['uuidrelacionado'];

    unset($datas['facturas']);
    unset($datas['motivo']);
    unset($datas['uuidrelacionado']);

    $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
    $datosconfiguracion=$datosconfiguracion->result();
    $datosconfiguracion=$datosconfiguracion[0];
    $rFCEmisor=$datosconfiguracion->Rfc;
    $passwordClavePrivada=$datosconfiguracion->paswordkey;

    $datosCancelacion=array();
    $DATAf = json_decode($facturas);
    for ($i=0;$i<count($DATAf);$i++) {
      $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
      $datosfactura=$datosfactura->result();
      $datosfactura=$datosfactura[0];
      $FacturasId=$datosfactura->FacturasId;
      $datosCancelacion[] = array(
                                'RFCReceptor'=>$datosfactura->Rfc,
                                'Total'=>round($datosfactura->total, 2),
                                'UUID'=>$datosfactura->uuid,
                                'Motivo'=>$motivo,
                                'FolioSustitucion'=>$uuidrelacionado

                              );
    }
    //===========================================================
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
    //=======================================================
      
      $fp = fopen($ruta.'pfx.pem', "r");
      $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
      fclose($fp);
      
      
      $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
      $rutapass=$rutaarchivos.'passs.txt';
      $passwordClavePrivada = file_get_contents($rutapass);
      
      //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
      
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'rFCEmisor' => $rFCEmisor,
          'listaCFDI' => $datosCancelacion,
          'clavePrivada_Base64'=>$clavePrivada,
          'passwordClavePrivada'=> $passwordClavePrivada  );
      file_put_contents($rutalog.'log_'.$this->fechahoyL.'_cancelacion_.txt', json_encode($parametros));
      $clienteSOAP = new SoapClient($URL_WS);

      $result = $clienteSOAP->CancelarCFDI($parametros);

      $fileacuse1='log_cancelcion2_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
      $ruta1 = $rutalog .$fileacuse1;

      file_put_contents($ruta1,json_encode($result));

      if($result->CancelarCFDIResult->OperacionExitosa) {
        $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Cancelada',
                        'info'=>$result
                        );
        //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
              $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
              $ruta = $rutaf .$fileacuse;
              //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
              file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

              $datosdecancelacion = array(
                                          'Estado'=>0,
                                          'rutaAcuseCancelacion '=>$fileacuse,
                                          'fechacancelacion'=>$this->fechahoyc
                                        );
              $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
        
      }else{
        $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                        'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                        'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                        'info'=>$result
                        );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
      }
      
      echo json_encode($resultado);
  }
  /*
  function cancelarCfdicomplemento(){
    $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    $ruta=$rutainterna.'/hulesyg/temporalsat/';
    $rutaf=$rutainterna.'/hulesyg/facturas/';
    $rutalog=$rutainterna.'/hulesyg/facturaslog/';

    $productivo=1;//0 demo 1 producccion
    $datas = $this->input->post();
    $facturas=$datas['facturas'];
    unset($datas['facturas']);

    $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
    $datosconfiguracion=$datosconfiguracion->result();
    $datosconfiguracion=$datosconfiguracion[0];
    $rFCEmisor=$datosconfiguracion->Rfc;
    $passwordClavePrivada=$datosconfiguracion->paswordkey;

    $datosCancelacion=array();
    //$DATAf = json_decode($facturas);
    //for ($i=0;$i<count($DATAf);$i++) {
      $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
      $datosfactura=$datosfactura->result();
      $datosfactura=$datosfactura[0];
      $FacturasId=$datosfactura->FacturasId;
      $datosCancelacion[] = array(
                                'Propiedad'=>'cancelacion',
                                'RFCReceptor'=>$datosfactura->R_rfc,
                                'Total'=>0,
                                'UUID'=>$datosfactura->uuid

                              );
      //$this->liberarfacturas($DATAf[$i]->FacturasIds);
    //}
    //===========================================================
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
    //=======================================================
      
      $fp = fopen($ruta.'pfx.pem', "r");
      $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
      fclose($fp);
      
      
      $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
      $rutapass=$rutaarchivos.'passs.txt';
      $passwordClavePrivada = file_get_contents($rutapass);
      
      //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
      
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'rFCEmisor' => $rFCEmisor,
          'listaCFDI' => $datosCancelacion,
          'clavePrivada_Base64'=>$clavePrivada,
          'passwordClavePrivada'=> $passwordClavePrivada  );
      file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
      $clienteSOAP = new SoapClient($URL_WS);

      $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

      if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
        $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Cancelada',
                        'info'=>$result
                        );
        //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
              $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
              $ruta = $rutaf .$fileacuse;
              //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
              file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

              $datosdecancelacion = array(
                                          'Estado'=>0,
                                          'rutaAcuseCancelacion '=>$fileacuse,
                                          'fechacancelacion'=>$this->fechahoyc
                                        );
              $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
        
      }else{
        $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                        'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                        'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                        'info'=>$result
                        );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
      }
      
      echo json_encode($resultado);
  }
  */
  function cancelarCfdicomplemento(){
    $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/semit_erp';//rutalocal
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    $ruta=$rutainterna.'/hulesyg/temporalsat/';
    $rutaf=$rutainterna.'/hulesyg/facturas/';
    $rutalog=$rutainterna.'/hulesyg/facturaslog/';

    $productivo=$this->demoproductivo;//0 demo 1 producccion
    $datas = $this->input->post();
    $facturas=$datas['facturas'];
    $motivo=$datas['motivo'];
    $uuidrelacionado=$datas['uuidrelacionado'];
    
    unset($datas['facturas']);
    unset($datas['motivo']);
    unset($datas['uuidrelacionado']);

    $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
    $datosconfiguracion=$datosconfiguracion->result();
    $datosconfiguracion=$datosconfiguracion[0];
    $rFCEmisor=$datosconfiguracion->Rfc;
    $passwordClavePrivada=$datosconfiguracion->paswordkey;

    $datosCancelacion=array();
    //$DATAf = json_decode($facturas);
    //for ($i=0;$i<count($DATAf);$i++) {
      $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
      $datosfactura=$datosfactura->result();
      $datosfactura=$datosfactura[0];
      $FacturasId=$datosfactura->FacturasId;
      $datosCancelacion[] = array(
                                'RFCReceptor'=>$datosfactura->R_rfc,
                                'Total'=>0,
                                'UUID'=>$datosfactura->uuid,
                                'Motivo'=>$motivo,
                                'FolioSustitucion'=>$uuidrelacionado

                              );
      //$this->liberarfacturas($DATAf[$i]->FacturasIds);
    //}
    //===========================================================
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
    //=======================================================
      
      $fp = fopen($ruta.'pfx.pem', "r");
      $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
      fclose($fp);
      
      
      $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
      $rutapass=$rutaarchivos.'passs.txt';
      $passwordClavePrivada = file_get_contents($rutapass);
      
      //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
      
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'rFCEmisor' => $rFCEmisor,
          'listaCFDI' => $datosCancelacion,
          'clavePrivada_Base64'=>$clavePrivada,
          'passwordClavePrivada'=> $passwordClavePrivada  );
      file_put_contents($rutalog.'log_'.$this->fechahoyL.'_cancelacion_complemto_.txt', json_encode($parametros));
      $clienteSOAP = new SoapClient($URL_WS);

      $result = $clienteSOAP->CancelarCFDI($parametros);

      if($result->CancelarCFDIResult->OperacionExitosa) {
        $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Cancelada',
                        'info'=>$result
                        );
        //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
              $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
              $ruta = $rutaf .$fileacuse;
              //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
              file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

              $datosdecancelacion = array(
                                          'Estado'=>0,
                                          'rutaAcuseCancelacion '=>$fileacuse,
                                          'fechacancelacion'=>$this->fechahoyc
                                        );
              $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
        
      }else{
        $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                        'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                        'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                        'info'=>$result
                        );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_result_complemento.txt', json_encode($result));
      }
      
      echo json_encode($resultado);
  }
  function retimbrar(){
    $factura = $this->input->post('factura');
    //=========================================
      $fac_dll=$this->ModeloCatalogos->factura_sercivios_updatepro($factura);
      foreach ($fac_dll->result() as $itemdll) {
        $this->ModeloCatalogos->updateCatalogo('f_facturas_servicios',array('servicioId'=>$itemdll->servicioId_sat),array('sserviciosId'=>$itemdll->sserviciosId));
      }
    //=========================================
    $facturaresult=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
    foreach ($facturaresult->result() as $item) {
      $Folio=$item->Folio;
      $Rfc=strtoupper($item->Rfc);
      $clienteId = $item->clienteId;
      if($Rfc!='XAXX010101000'){
        $datosfiscales=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('idcliente'=>$clienteId,'activo'=>1));
        foreach ($datosfiscales->result() as $itemdf) {
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Nombre'=>rtrim($itemdf->razon_social),'Cp'=>$itemdf->cp,'RegimenFiscalReceptor'=>$itemdf->RegimenFiscalReceptor,'Rfc'=>strtoupper($itemdf->rfc),'uso_cfdi'=>$itemdf->uso_cfdi),array('FacturasId'=>$factura));
        }
      }
      if($Folio==0){
        $newfolio=$this->ModeloCatalogos->ultimoFolio() + 1;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Folio'=>$newfolio),array('FacturasId'=>$factura));
      }
    }
    $respuesta=$this->emitirfacturas($factura);
    echo json_encode($respuesta);
  }
  function addcomplemento(){
    $data = $this->input->post();
      $arraydoc=$data['arraydocumento'];
      unset($data['arraydocumento']);
      //$idfactura=$data['idfactura'];
      //==================================================================
        //$datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        //$datosfactura=$datosfactura->result();
        //$datosfactura=$datosfactura[0];
      //==================================================================
        //$idsucursal=$this->idsucursal;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
      //==================================================================

      //$Sello=$datosfactura->sellosat;
      //$Sello=$datosfactura->sellocadena;
      
      $datacp['Folio']=$data['Folio'];
      $datacp['Fecha']=$data['Fecha'];
      $datacp['Sello']='';
      //$datacp['NoCertificado']=$datosfactura->nocertificado;
      //$datacp['Certificado']=$datosfactura->certificado;
      $datacp['Certificado']='';
      $datacp['LugarExpedicion']=$data['LugarExpedicion'];
      $datacp['E_rfc']=$datosconfiguracion->Rfc;
      $datacp['E_nombre']=$datosconfiguracion->Nombre;
      $datacp['E_regimenfiscal']=$datosconfiguracion->Regimen;
      $datacp['R_rfc']=$data['rfcreceptor'];
      $datacp['R_nombre']=$data['razonsocialreceptor'];
      $datacp['R_regimenfiscal']='';
      $datacp['FechaPago']=$data['Fechatimbre'];
      $datacp['FormaDePagoP']=$data['FormaDePagoP'];
      $datacp['MonedaP']=$data['ModedaP'];
      $datacp['Monto']=$data['Monto'];
      $datacp['NumOperacion']=str_replace(' ', '', $data['NumOperacion']);
      $datacp['CtaBeneficiario']=$data['CtaBeneficiario'];
      
      $datacp['Serie']=$data['Serie'];//xxx
      $datacp['Foliod']=$data['Folio'];
      $datacp['MonedaDR']=$data['ModedaP'];
      $datacp['personalcreo']=$this->idpersonal;

      $datacp['f_relacion']    = $data['f_r'];
      $datacp['f_r_tipo']      = $data['f_r_t'];
      $datacp['f_r_uuid']      = $data['f_r_uuid'];
      $datacp['clienteId']      = $data['clienteId'];
      
      //==============================================================
      //$datacp['FacturasId']=$data['idfactura'];
      //$datacp['IdDocumento']=$data['IdDocumento'];
      //$datacp['NumParcialidad']=$data['NumParcialidad'];
      //$datacp['ImpPagado']=$data['Monto'];
      //$datacp['ImpSaldoAnt']=$data['ImpSaldoAnt'];
      //$datacp['ImpSaldoInsoluto']=$data['ImpSaldoAnt']-$data['Monto'];
      //$datacp['MetodoDePagoDR']=$datosfactura->FormaPago;
      //============================================================
      $datacp['UsoCFDI']='';
      $idcomplemento=$this->ModeloCatalogos->Insert('f_complementopago',$datacp);
      $DATAdoc = json_decode($arraydoc);
      for ($j=0;$j<count($DATAdoc);$j++){
        $datacdoc['complementoId']=$idcomplemento;
        $datacdoc['facturasId']=$DATAdoc[$j]->idfactura;
        $datacdoc['IdDocumento']=$DATAdoc[$j]->IdDocumento;
        //$datacdoc['serie']=$DATAdoc[$j]->serie;
        //$datacdoc['folio']=$DATAdoc[$j]->folio;
        $datacdoc['NumParcialidad']=$DATAdoc[$j]->NumParcialidad;
        $datacdoc['ImpSaldoAnt']=$DATAdoc[$j]->ImpSaldoAnt;
        $datacdoc['ImpPagado']=$DATAdoc[$j]->ImpPagado;
        $datacdoc['ImpSaldoInsoluto']=$DATAdoc[$j]->ImpSaldoInsoluto;
        $datacdoc['MetodoDePagoDR']=$DATAdoc[$j]->MetodoDePagoDR;
        $this->ModeloCatalogos->Insert('f_complementopago_documento',$datacdoc);
      }





      $respuesta=$this->procesarcomplemento($idcomplemento);

      echo json_encode($respuesta);
  }
  function retimbrarcomplemento(){
    $complemento = $this->input->post('complemento');
    $respuesta=$this->procesarcomplemento($complemento);
      echo json_encode($respuesta);
  }
  function procesarcomplemento($idcomplemento){
    //========================================================================
    $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/semit_erp';//rutalocal
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    $ruta=$rutainterna.'/hulesyg/temporalsat/';
    $rutaf=$rutainterna.'/hulesyg/facturas/';
    $rutalog=$rutainterna.'/hulesyg/facturaslog/';
      $datosFactura = array(
        'carpeta'=>'hulesyg',
        'pwskey'=>'',
        'archivo_key'=>'',
        'archivo_cer'=>'',
        'factura_id'=>0
      );
      $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
        //fclose($archivo);
        $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
      
        $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
      
        $numeroCertificado="";
        $i=0;  
        foreach ($temporal as $value) {
            
            if(($i%2))
            
                $numeroCertificado .= $value;
        
            $i++;
        }
      
      $numeroCertificado = str_replace('\n','',$numeroCertificado);
      
      $numeroCertificado = trim($numeroCertificado);

      $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
      
      //fclose($archivo);
      $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
      
      $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
      
      $certificado=  str_replace("\n", "", $certificado);
      $certificado=  str_replace(" ", "", $certificado);
      $certificado=  str_replace("\n", "", $certificado);
      $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
      $certificado= trim($certificado);
    //========================================================================================
      $xml=$this->generaxmlcomplemento('',$certificado,$numeroCertificado,$idcomplemento);
      log_message('error', 'se genera xml2: '.$xml);
      $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
      
      $cadena=$str;
    //===========================================================================================
      $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r"); 
      
      $priv_key = fread($fp, 8192); 
      
      fclose($fp); 
      
      $pkeyid = openssl_get_privatekey($priv_key);
      
      //openssl_sign($cadena, $sig, $pkeyid);
      openssl_sign($cadena, $sig, $pkeyid,'sha256');
      
      openssl_free_key($pkeyid);
      
      $sello = base64_encode($sig);
    //===============accessos al webservice===================================================

      $productivo=$this->demoproductivo;//0 demo 1 productivo
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
      $idsucursal=$this->idsucursal;
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idsucursal));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $clienteSOAP = new SoapClient($URL_WS);

    //==================================================================
        $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
        $datoscomplemento=$datoscomplemento->result();
        $datoscomplemento=$datoscomplemento[0];
      //==================================================================
        
        $xmlcomplemento=$this->generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento);
        //log_message('error', 'se genera xml3: '.$xmlcomplemento);
          file_put_contents('hulesyg/facturas/preview_complemento_'.$idcomplemento.'.xml',$xmlcomplemento);
          
          $referencia=$datoscomplemento->E_rfc.'_'.$idcomplemento;
      //======================================================================================
          
        $result = $clienteSOAP->TimbrarCFDI(array(
                  'usuario' => $usuario,
                  'password' => $password,
                  'cadenaXML' => $xmlcomplemento,
                  'referencia' => $referencia));
        file_put_contents($rutalog.'log_complemento'.$this->fechahoyL.'_.txt', json_encode($result));
        if($result->TimbrarCFDIResult->CodigoRespuesta != '0' ) {                       
          
          $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                        'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                        'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                        'idcomplemento'=>$idcomplemento,
                        'info'=>$result,
                        'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                        );
          
          $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));
                      
          return $resultado;
      
      } else {
        
          try {
         
              $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
          
              //Guardar en archivo     y la ruta en la bd
              $ruta = $datosFactura['carpeta'] . '/facturas/complemento_'.$idcomplemento.'.xml';
          
              file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
              $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('rutaXml'=>$ruta),array('complementoId'=>$idcomplemento));            
          
              $sxe = new SimpleXMLElement($xmlCompleto);
                       
              $ns = $sxe->getNamespaces(true);
          
              $sxe->registerXPathNamespace('c', $ns['cfdi']);
          
              $sxe->registerXPathNamespace('t', $ns['tfd']);

              $uuid = '';
          
              foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
               
                  //Actualizamos el Folio Fiscal UUID
                  //$uuid = $tfd['UUID'];
                  $updatedatossat=array(
                    'uuid'=>$tfd['UUID'],
                    'Sello'=>$tfd['SelloCFD'],
                    'sellosat'=>$tfd['SelloSAT'],
                    'NoCertificado'=>$tfd['NoCertificado'],
                    'nocertificadosat'=>$tfd['NoCertificadoSAT'],
                    'fechatimbre'=>date('Y-m-d G:i:s'),
                    'cadenaoriginal'=>$cadena,
                    'Estado'=>1
                  );
                  $this->ModeloCatalogos->updateCatalogo('f_complementopago',$updatedatossat,array('complementoId'=>$idcomplemento)); 
              } 
          
              //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

              $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Complemento Generado',
                        'idcomplemento'=>$idcomplemento
                        );
          return $resultado;
         }  catch (Exception $e) {
            $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                        'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                        'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                        'idcomplemento'=>$idcomplemento,
                        'info'=>$result,
                        'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                        );
           
          $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));
          
          return $resultado;
              
         }
      
      }
        //echo json_encode($result);
        
        return $resultado;

  }
  function generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento){
      
        $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
        $datoscomplemento=$datoscomplemento->result();
        $datoscomplemento=$datoscomplemento[0];
        $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','"',"'");
        $caract2  = array('&amp','&ntilde','&ntilde','','a','e','i','o','u','A','E','I','O','U','&quot;','&apos;');
        $R_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->R_nombre));

          $f_relacion=$datoscomplemento->f_relacion;
          $f_r_tipo=$datoscomplemento->f_r_tipo;
          $f_r_uuid=$datoscomplemento->f_r_uuid;
      //==================================================================
          $datoscliente=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('rfc'=>$datoscomplemento->R_rfc,'activo'=>1));
          $datoscliente=$datoscliente->result();
          $datoscliente=$datoscliente[0];
      //==================================================================
        $NumOperacion=str_replace(' ', '', $datoscomplemento->NumOperacion);

        $xmlcomplemento='<?xml version="1.0" encoding="utf-8"?>';
        $xmlcomplemento.='
          <cfdi:Comprobante xmlns:pago20="http://www.sat.gob.mx/Pagos20" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/4" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd" Version="4.0" Exportacion="01"
            Fecha="'.date('Y-m-d',strtotime($datoscomplemento->Fecha)).'T'.date('H:i:s',strtotime($datoscomplemento->Fecha)).'" Sello="'.$sello.'" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" SubTotal="'.$datoscomplemento->SubTotal.'" Moneda="'.$datoscomplemento->Moneda.'" Total="'.$datoscomplemento->Total.'" TipoDeComprobante="'.$datoscomplemento->TipoDeComprobante.'" LugarExpedicion="'.$datoscomplemento->LugarExpedicion.'">';
            if($f_relacion==1){
                  $xmlcomplemento .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                              <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                          </cfdi:CfdiRelacionados>';
              }$xmlcomplemento .='
          <cfdi:Emisor Rfc="'.$datoscomplemento->E_rfc.'" Nombre="'.$datoscomplemento->E_nombre.'" RegimenFiscal="'.$datoscomplemento->E_regimenfiscal.'"/>
          <cfdi:Receptor Rfc="'.$datoscomplemento->R_rfc.'" Nombre="'.$R_nombre.'"  DomicilioFiscalReceptor="'.$datoscliente->cp.'" RegimenFiscalReceptor="'.$datoscliente->RegimenFiscalReceptor.'" UsoCFDI="CP01"/>
          <cfdi:Conceptos>
            <cfdi:Concepto ClaveUnidad="'.$datoscomplemento->ClaveUnidad.'" ClaveProdServ="'.$datoscomplemento->ClaveProdServ.'" Cantidad="'.$datoscomplemento->Cantidad.'" Descripcion="'.$datoscomplemento->Descripcion.'" ValorUnitario="'.$datoscomplemento->ValorUnitario.'" Importe="'.$datoscomplemento->Importe.'" ObjetoImp="01"/>
          </cfdi:Conceptos>
          <cfdi:Complemento>';
          $dcompdoc=$this->Modelofacturas->documentorelacionado($idcomplemento);
          $TotalTrasladosBaseIVA16=0;
          $TotalTrasladosImpuestoIVA16=0;
          foreach ($dcompdoc->result() as $itemdoc) {
            if($itemdoc->iva>0){                
              $basedr0=$itemdoc->ImpPagado/1.16;
              $ImporteDR0=$basedr0*0.16;
              //$TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+$basedr0;
              $TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+round($basedr0,2);
              //$TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+$ImporteDR0;
              $TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+round($ImporteDR0,2);
            }
          }
          if($TotalTrasladosBaseIVA16>0){
            $tang_TotalTrasladosBaseIVA16=' TotalTrasladosBaseIVA16="'.number_format(round($TotalTrasladosBaseIVA16,2), 2, ".", "").'"'; 
          }else{
            $tang_TotalTrasladosBaseIVA16='';
          }
          if($TotalTrasladosImpuestoIVA16>0){
            $tang_TotalTrasladosImpuestoIVA16='TotalTrasladosImpuestoIVA16="'.number_format(round($TotalTrasladosImpuestoIVA16,2), 2, ".", "").'"';
          }else{
            $tang_TotalTrasladosImpuestoIVA16='';
          }
          $xmlcomplemento.='<pago20:Pagos Version="2.0">
              <pago20:Totales MontoTotalPagos="'.$datoscomplemento->Monto.'" '.$tang_TotalTrasladosBaseIVA16.' '.$tang_TotalTrasladosImpuestoIVA16.'/>
              <pago20:Pago FechaPago="'.date('Y-m-d',strtotime($datoscomplemento->FechaPago)).'T'.date('H:i:s',strtotime($datoscomplemento->FechaPago)).'" FormaDePagoP="'.$datoscomplemento->FormaDePagoP.'" MonedaP="MXN" Monto="'.$datoscomplemento->Monto.'" TipoCambioP="1"  ';
          $xmlcomplemento.=' NumOperacion="'.$NumOperacion.'" '; 
          //$xmlcomplemento.=' CtaBeneficiario="9680096800" ';
          $xmlcomplemento.=' >';
          //$dcompdoc=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
          $RetencionesP=0;
          $TrasladosP=0;
          $ImporteDRtotal=0;
          $basedrtotal=0;
          $ImporteDRtotal0=0;
          foreach ($dcompdoc->result() as $itemdoc) {
            $iva_r_g=$itemdoc->iva+$itemdoc->ivaretenido;
            if($iva_r_g>0){
              $tang_ObjetoImpDR=' ObjetoImpDR="02" ';
            }else{
              $tang_ObjetoImpDR=' ObjetoImpDR="01" ';
            }
           $xmlcomplemento.='<pago20:DoctoRelacionado IdDocumento="'.$itemdoc->IdDocumento.'" Serie="'.$datoscomplemento->Serie.'" Folio="'.$itemdoc->Folio.'" MonedaDR="'.$datoscomplemento->MonedaDR.'" NumParcialidad="'.$itemdoc->NumParcialidad.'" ImpSaldoAnt="'.$itemdoc->ImpSaldoAnt.'" ImpPagado="'.$itemdoc->ImpPagado.'" ImpSaldoInsoluto="'.$itemdoc->ImpSaldoInsoluto.'" '.$tang_ObjetoImpDR.' EquivalenciaDR="1">';
              $iva_r_g=$itemdoc->iva+$itemdoc->ivaretenido;
              if($iva_r_g>0){
                $xmlcomplemento.='<pago20:ImpuestosDR>';
                                  if($itemdoc->iva>0){
                                    $TrasladosP++;
                                    
                                    $basedr=$itemdoc->ImpPagado/1.16;
                                    $ImporteDR=$basedr*0.16;
                                    //$basedrtotal=$basedrtotal+$basedr;
                                    $basedrtotal=$basedrtotal+round($basedr,2);
                                    //$ImporteDRtotal0=$ImporteDRtotal0+$ImporteDR;
                                    $ImporteDRtotal0=$ImporteDRtotal0+round($ImporteDR,2);
                                    $xmlcomplemento.='<pago20:TrasladosDR>
                                                          <pago20:TrasladoDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                        </pago20:TrasladosDR>';
                                  }
                                  if($itemdoc->ivaretenido>0){
                                    $RetencionesP++;
                                    $basedr=$itemdoc->ImpPagado/1.16;
                                    $ImporteDR=$basedr*0.16;
                                    $ImporteDRtotal=$ImporteDRtotal+$ImporteDR;
                                    $xmlcomplemento.='<pago20:RetencionesDR>
                                                        <pago20:RetencionDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                      </pago20:RetencionesDR>';
                                  }
                $xmlcomplemento.='</pago20:ImpuestosDR>';
              }
                                
           $xmlcomplemento.='</pago20:DoctoRelacionado>';
          }
          $xmlcomplemento.='<pago20:ImpuestosP>';
                            if($RetencionesP>0){
                              $xmlcomplemento.='<pago20:RetencionesP>';
                                  $xmlcomplemento.='<pago20:RetencionP ImporteP="'.number_format(round($ImporteDRtotal,2), 2, ".", "").'" ImpuestoP="002"/>';
                              $xmlcomplemento.='</pago20:RetencionesP>';
                            }
                            if($TrasladosP>0){
                              $xmlcomplemento.='<pago20:TrasladosP>';
                                 $xmlcomplemento.='<pago20:TrasladoP BaseP="'.number_format(round($basedrtotal,2), 2, ".", "").'" ImporteP="'.number_format(round($ImporteDRtotal0,2), 2, ".", "").'" ImpuestoP="002" TasaOCuotaP="0.160000" TipoFactorP="Tasa"/>';
                              $xmlcomplemento.='</pago20:TrasladosP>';
                            }
          $xmlcomplemento.='</pago20:ImpuestosP>';
          $xmlcomplemento.='</pago20:Pago>
          </pago20:Pagos>
          </cfdi:Complemento>
          </cfdi:Comprobante>'; 
          //log_message('error', 'se genera xml: '.$xmlcomplemento);
    return $xmlcomplemento;
  }
  function estatuscancelacionCfdi($uuid){
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
    //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
    //$ruta=$rutainterna.'/kyocera/temporalsat/';
    //$rutaf=$rutainterna.'/kyocera/facturas/';
    //$rutalog=$rutainterna.'/kyocera/facturaslog/';

    $productivo=1;//0 demo 1 producccion
    
      $res_uuid=$uuid;
      $res_facturaid=0;
    
    

    //var_dump($datosfactura->uuid);
    //===========================================================
      if($productivo==0){
        $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
      }elseif ($productivo==1) {
        //Produccion
        $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
      }else{
        $URL_WS='';
      }
      if($productivo==0){
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }elseif ($productivo==1) {
        //Produccion
        $password= "@fuJXsS@";
        $usuario= "HERD890308UAA";
      }else{
        //usuario demo
        $password= "contRa$3na";
        $usuario= "HERD890308D33";
      }
    //=======================================================
      /*
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'UUID' => $datosfactura->uuid);
      */
      $parametros = array(
          'usuario' => $usuario,
          'password' => $password,
          'uUID' => $res_uuid);
      //echo json_encode($parametros);
      $clienteSOAP = new SoapClient($URL_WS);

      $result = $clienteSOAP->ObtenerAcuseCancelacion($parametros);

      //$fileacuse1='log_cancelcion_estatus_'.$res_facturaid.'_'.$res_uuid.'.xml';
      //$ruta1 = $rutalog .$fileacuse1;
      $resultado=$result;
      
      //file_put_contents($ruta1,json_encode($result));
      /*if($result->CancelarCFDIResult->OperacionExitosa) {
        $resultado = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Factura Cancelada',
                        'info'=>$result
                        );
        //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
              $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
              $ruta = $rutaf .$fileacuse;
              //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
              file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

              $datosdecancelacion = array(
                                          'Estado'=>0,
                                          'rutaAcuseCancelacion '=>$fileacuse,
                                          'fechacancelacion'=>$this->fechahoyc
                                        );
              $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
        
      }else{
        $resultado = array(
                        'resultado'=>'error',
                        'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                        'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                        'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                        'info'=>$result
                        );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
      }
      */
      /*
        CodigoRespuesta":"825" //El UUID aún no ha sido cancelado
        {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"825","CreditosRestantes":0,"MensajeError":"El UUID aun no ha sido cancelado.","MensajeErrorDetallado":null,"OperacionExitosa":false,"PDFResultado":null,"Timbre":null,"XMLResultado":null}}


        CodigoRespuesta":"800" //Operación exitosa
        {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"800","CreditosRestantes":0,"MensajeError":"","MensajeErrorDetallado":null,"OperacionExitosa":true,"PDFResultado":null,"Timbre":{"Estado":"Cancelado","FechaTimbrado":"2023-03-23T14:26:13","NumeroCertificadoSAT":"","SelloCFD":"mnWkSb9U8xhqvhB6cQOZr5hgJHRZOcZDoNk4vR7XHMJVZX78p\/jZLeYKH6I8WuD\/lrxEtl0dPLaaDzy6PcsFQibzA1mGLWCOi3Z4vBFvtmgcNGrZdWn5hZG\/EMUvE8m667HGaCZSRLxoIjT73ClvQJ640XG+awGTzd8xhcQvMfjNwxQMorA4yVFdivzbYSHChBDMsDmSnGZoCbCaL\/HsTJVM5vHW+sEV3qDZ1IHcp9TqKOU7+YFqxY\/Xrzu2mOHTGPXnEp2tT2a1oa3zf2xExngkVg72O+w\/E6jk7HuyE2iri3b15GwDiZGU\/tbYm\/kyf+wnR3tiw\/VVollajVnCtQ==","SelloSAT":"","UUID":"F615D95D-5F1D-490A-98AB-903397D60B4F"},"XMLResultado":"\r\n\r\n \r\n F615D95D-5F1D-490A-98AB-903397D60B4F<\/UUID>\r\n 201<\/EstatusUUID>\r\n <\/Folios>\r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n not(ancestor-or-self::*[local-name()='Signature'])<\/XPath>\r\n <\/Transform>\r\n <\/Transforms>\r\n \r\n jxzMoRslxiV3czAkeXpq5qwIC6xb9a4RYzpgPKBkXWJomCcng5lu8xoTUR\/y2RU9LYWmWZv0iKZTO9HPjTlZvA==<\/DigestValue>\r\n <\/Reference>\r\n <\/SignedInfo>\r\n LoEndrN4K7U4FmhFJS\/O\/DpNNDxJWEowmBlg2lrgkAZbwKqYnzgSdEJppkBm5av36q2F\/3c6H\/\/86j3XFy\/vjQ==<\/SignatureValue>\r\n \r\n 00001088888800000038<\/KeyName>\r\n \r\n \r\n qRWzHPVtRHYyDQTnnpPYtOBmb5Raaddb4XZH1DNlIhuhUrh7RKwfvcwh05wEu1lgUnej9BwsLd4u1SeYyawmaF2zIhzssP19yhOQUN9dRBeSg51m8XqCiodxoGilgNm7eDdZQ7j0ZYV0UOkMgGfomw\/3L0z\/O1gTbRDSg8gwM4BMSSu+iSMTcIKqn0oh4C+u77vKRui9NX6WZW2uRnmvEnDybxxtxTQVR3VlM4tdXeYTfkr5WUUrtaFyFW\/B7zs9Mb+FO5Cr\/TbWmu+xJP6LlN4UbZdPVe7+7rrv\/hwzeYe2LeFT0Jtmvuy1NfR\/uUZa35+UMou+RbsDOTm4vfJyuw==<\/Modulus>\r\n AQAB<\/Exponent>\r\n <\/RSAKeyValue>\r\n <\/KeyValue>\r\n <\/KeyInfo>\r\n <\/Signature>\r\n<\/Acuse>"}}

      */
        $MensajeError='';
        if($resultado->ObtenerAcuseCancelacionResult->CodigoRespuesta=='825'){
          $MensajeError=$resultado->ObtenerAcuseCancelacionResult->MensajeError;
        }else{

        }
      
      //echo '['.json_encode($resultado).']';
        return $MensajeError;
  }
  function estatuscancelacionCfdi_all(){
      $datos = $this->input->post('datos');
      //log_message('error', $datos);
      $DATA = json_decode($datos);
      $facturas=array();
      for ($i=0;$i<count($DATA);$i++) {
          $complemento=0;
          $uuid=$DATA[$i]->uuid;
          $resultado=$this->estatuscancelacionCfdi($uuid);

          $facturas[]=array(
                  'uuid'=>$uuid,
                  'resultado'=>$resultado
              );
      }
      echo json_encode($facturas);
  }
  function cancelarventa2(){
    $params = $this->input->post();
    $id = $params['id'];
    $mot = $params['mot'];
    $fac = $params['fac'];
    //==================================================================
    $result_ven=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$id));
    $result_ven=$result_ven->row();

    $sucursalid=$result_ven->sucursal;
    $activo=$result_ven->activo;
    $subtotal_v=$result_ven->subtotal;
    $total=$result_ven->total;
    $iva_v=$result_ven->iva;

    //==================================================================
    if($activo==1){
     $data_up_v= array(
                  'delete_reg'=>$this->fechahoyc, //
                  'delete_mot'=>$mot,
                  'delete_user'=>$this->idpersonal,
                  'activo'=>0
      );
      $id_bita=$this->ModeloCatalogos->Insert('bitacora_devoluciones',array('idventa'=>$id,'idsucursal'=>$sucursalid,'monto'=>$total,'idpersonal'=>$this->idpersonal,'fecha'=>$this->fechahoy,'reg'=>$this->fechahoyc));
      $this->ModeloCatalogos->updateCatalogo('venta_erp',$data_up_v,array('id'=>$id));

      $result_vend=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$id));
      $importes_totales=0;
      foreach ($result_vend->result() as $item){
        $importes_totales+=($item->cantidad*$item->precio_unitario);
        if($item->tipo==0){
          if($item->id_ps_serie==0 && $item->id_ps_lote==0){
            //obtiene cant disp. y genera detalle de bitacora
            $cant_inicial=0;
            $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$item->idproducto,'sucursalid'=>$sucursalid,'activo'=>1));
            foreach ($res_info1->result() as $itemi) {
              $cant_inicial=$itemi->stock;
            }
            $this->ModeloCatalogos->Insert('bitacora_devoluciones_detalles',array('id_bitacora'=>$id_bita,'id_venta_det'=>$item->id,'cant_ini'=>$cant_inicial,'cantidad'=>$item->cantidad));
            $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','+',$item->cantidad,'productoid',$item->idproducto,'sucursalid',$sucursalid,'activo',1);        
            // **************************** //
          }else{
            if($item->id_ps_lote!=0){
              //obtiene cant disp. y genera detalle de bitacora
              $cant_inicial=$this->ModeloCatalogos->obtener_stock_lotes($item->idproducto,$sucursalid);
              $this->ModeloCatalogos->Insert('bitacora_devoluciones_detalles',array('id_bitacora'=>$id_bita,'id_venta_det'=>$item->id,'cant_ini'=>$cant_inicial,'cantidad'=>$item->cantidad));
              // **************************** //
              $this->ModeloCatalogos->updatestock4('productos_sucursales_lote','cantidad','+',$item->cantidad,'id',$item->id_ps_lote);
            }else{
              //obtiene cant disp. y genera detalle de bitacora
              //$cant_inicial=$this->ModeloCatalogos->obtener_stock_series($item->idproducto,$sucursalid);
              $cant_inicial=0;
              $this->ModeloCatalogos->Insert('bitacora_devoluciones_detalles',array('id_bitacora'=>$id_bita,'id_venta_det'=>$item->id,'cant_ini'=>$cant_inicial,'cantidad'=>$item->cantidad));
              /* *************************** */
              $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array('disponible'=>1,'vendido'=>0),array('id'=>$item->id_ps_serie));
            } 
          }
          $res_prod=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$item->idproducto));
          $res_prod=$res_prod->row();
          if($res_prod->concentrador=="1"){
            $getpc=$this->Modeloventas->getProductosConcen();
            foreach ($getpc as $p) {
              $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','+',$p->cantidad,'productoid',$p->id_prod,'sucursalid',$sucursalid,'activo',1);
            }
          }
          if($res_prod->tanque=="1"){
            //obtiene cant disp. y genera detalle de bitacora
            $res_info1=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$item->idproducto));
            foreach ($res_info1->result() as $itemi) {
              $cant_inicial=$itemi->stock;
            }
            $this->ModeloCatalogos->Insert('bitacora_devoluciones_detalles',array('id_bitacora'=>$id_bita,'id_venta_det'=>$item->id,'cant_ini'=>$cant_inicial,'cantidad'=>$item->cantidad));
            /* *************************** */
            $this->ModeloCatalogos->updatestock3('recargas','stock','+',$res_prod->capacidad,'tipo',0,'sucursal',$sucursalid,'estatus',1);
          }
        }else{
          if($item->tipo==1){ //tipo 1 = recarga
            //obtiene cant disp. y genera detalle de bitacora
            $res_info1=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$item->idproducto));
            foreach ($res_info1->result() as $itemi) {
              $cant_inicial=$itemi->stock;
            }
            $this->ModeloCatalogos->Insert('bitacora_devoluciones_detalles',array('id_bitacora'=>$id_bita,'id_venta_det'=>$item->id,'cant_ini'=>$cant_inicial,'cantidad'=>$item->cantidad));
            /* *************************** */

            $res_rec=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$item->idproducto));
            $res_rec=$res_rec->row();
            $capac_vend = $res_rec->capacidad * $item->cantidad;
            $this->ModeloCatalogos->updateCatalogo('recargas',array('capacidad = capacidad +'=>$capac_vend),array('id'=>$item->idproducto));
          } 
        }
        
      } 
      //log_message('error','fac: '.$fac);
      if($fac>0){
        $result_fac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$fac));
        $result_fac=$result_fac->row();
        
        $Rfc=$result_fac->Rfc;

        $uuid=$result_fac->uuid;
        $result_fac->serie='H';
        $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
        $result_fac->Folio=$ultimoFolioval;
        $result_fac->TipoComprobante='E';
        $result_fac->CondicionesDePago='';
        $result_fac->rutaXml='';
        $result_fac->cadenaoriginal='';
        $result_fac->sellocadena='';
        $result_fac->sellosat='';
        $result_fac->uuid='';
        $result_fac->fechatimbre='';
        $result_fac->certificado='';
        $result_fac->nocertificado='';
        $result_fac->nocertificadosat='';
        $result_fac->tarjeta='';
        $result_fac->fechacancelacion='';
        $result_fac->sellocancelacion='';
        $result_fac->comprobado=0;
        $result_fac->uso_cfdi='G02';
        
        $result_fac->numproveedor='';
        $result_fac->numordencompra='';
        $result_fac->honorario=0;
        $result_fac->isr=0;
        $result_fac->ivaretenido=0;
        $result_fac->cedular=0;
        $result_fac->outsourcing=0;
        //$result_fac->5almillar=0;
        $result_fac->cincoalmillarval=0;
        $result_fac->facturaabierta=0;
        $result_fac->f_relacion='1';
        $result_fac->f_r_tipo='01';
        $result_fac->f_r_uuid=$uuid;
        $result_fac->pg_global=0;
        $result_fac->pg_periodicidad=null;
        $result_fac->pg_meses=null;
        $result_fac->pg_anio=null;
        $result_fac->favorito=0;
        $result_fac->correoenviado=0;
        $result_fac->estatus_correo=0;
        $result_fac->relacion_venta_pro=null;
        $result_fac->usuario_id=$this->idpersonal;
        $result_fac->creada_sesionId = $this->idpersonal;
        if($Rfc=='XAXX010101000'){
          $result_fac->uso_cfdi='S01';
          $result_fac->RegimenFiscalReceptor='616';
        }
        if($result_fac->Nombre=='PUBLICO EN GENERAL'){
          $result_fac->Nombre='PUBLICO EN GENERAL '.$this->succlave;
        }
        

        unset($result_fac->FacturasId);
        unset($result_fac->Estado);
        unset($result_fac->rutaAcuseCancelacion);
        $FacturasId_new=$this->ModeloCatalogos->Insert('f_facturas',$result_fac);

        $datainsertarray=array(); /*
        // modificar esta parte para que no se desglose todos los servicios de la factura si no solo el monto total de la venta
        //$result_fac_dll=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$fac));
        //foreach ($result_fac_dll->result() as $itemdll) {
          //unset($itemdll->sserviciosId);
          $itemdll['FacturasId']=$FacturasId_new;
          $itemdll['Cantidad']=1;
          $itemdll['Unidad']='ACT';
          $itemdll['servicioId']='84111506';
          $itemdll['Descripcion']='Servicios de facturacion';
          $itemdll['Descripcion2']='Devolucion';
          $itemdll['Cu']=$importes_totales;
          $itemdll['descuento']=0;
          $itemdll['Importe']=$importes_totales; 
          $itemdll['iva']=$iva_v;
          if($iva_v>0){
            $consin_iva=1;
          }else{
            $consin_iva=0;
          }
          $itemdll['consin_iva'] =$consin_iva;
          $datainsertarray[]=$itemdll;
        //}*/
          $result_facdll=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$fac));
          foreach ($result_facdll->result() as $itemdll) {
            unset($itemdll->sserviciosId);
            $itemdll->FacturasId=$FacturasId_new;
            $datainsertarray[]=$itemdll;
          }
        if(count($datainsertarray)>0){
          $this->ModeloCatalogos->insert_batch('f_facturas_servicios',$datainsertarray);
        }
        $respuesta=$this->emitirfacturas($FacturasId_new);
        echo json_encode($respuesta);
      }
    }
  }

  function cancelarnotaventa(){
    $params = $this->input->post();
    $id = $params['id'];
    $fac = $params['fac'];
    $idventa=$id;
    //==================================================================
    $result_ven=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$id));
    $result_ven_dll=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$id));
    $result_ven=$result_ven->row();

    $sucursalid=$result_ven->sucursal;
    $activo=$result_ven->activo;
    $subtotal_v=$result_ven->subtotal;
    $total=$result_ven->total;
    $iva_v=$result_ven->iva;

    //==================================================================
    if($activo==1){ 
      //log_message('error','fac: '.$fac);
      if($fac>0){
        $result_fac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$fac));
        $result_fac=$result_fac->row();
        
        $Rfc=$result_fac->Rfc;

        $uuid=$result_fac->uuid;
        $result_fac->serie='H';
        $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
        $result_fac->Folio=$ultimoFolioval;
        $result_fac->TipoComprobante='E';
        $result_fac->CondicionesDePago='';
        $result_fac->rutaXml='';
        $result_fac->cadenaoriginal='';
        $result_fac->sellocadena='';
        $result_fac->sellosat='';
        $result_fac->uuid='';
        $result_fac->fechatimbre='';
        $result_fac->certificado='';
        $result_fac->nocertificado='';
        $result_fac->nocertificadosat='';
        $result_fac->tarjeta='';
        $result_fac->fechacancelacion='';
        $result_fac->sellocancelacion='';
        $result_fac->comprobado=0;
        $result_fac->uso_cfdi='G02';
        
        $result_fac->numproveedor='';
        $result_fac->numordencompra='';
        $result_fac->honorario=0;
        $result_fac->isr=0;
        $result_fac->ivaretenido=0;
        $result_fac->cedular=0;
        $result_fac->outsourcing=0;
        //$result_fac->5almillar=0;
        $result_fac->cincoalmillarval=0;
        $result_fac->facturaabierta=0;
        $result_fac->f_relacion='1';
        $result_fac->f_r_tipo='01';
        $result_fac->f_r_uuid=$uuid;
        $result_fac->pg_global=0;
        $result_fac->pg_periodicidad=null;
        $result_fac->pg_meses=null;
        $result_fac->pg_anio=null;
        $result_fac->favorito=0;
        $result_fac->correoenviado=0;
        $result_fac->estatus_correo=0;
        $result_fac->relacion_venta_pro=null;
        $result_fac->usuario_id=$this->idpersonal;
        $result_fac->creada_sesionId = $this->idpersonal;
        $result_fac->subtotal=$subtotal_v;
        $result_fac->iva=$iva_v;
        $result_fac->total=$total;
        if($Rfc=='XAXX010101000'){
          $result_fac->uso_cfdi='S01';
          $result_fac->RegimenFiscalReceptor='616';
        }
        if($result_fac->Nombre=='PUBLICO EN GENERAL'){
          $result_fac->Nombre='PUBLICO EN GENERAL '.$this->succlave;
        }
        

        unset($result_fac->FacturasId);
        unset($result_fac->Estado);
        unset($result_fac->rutaAcuseCancelacion);
        $FacturasId_new=$this->ModeloCatalogos->Insert('f_facturas',$result_fac);

        $datainsertarray=array();
        // modificar esta parte para que no se desglose todos los servicios de la factura si no solo el monto total de la venta
          $result_fac_dll=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$fac));
          
        //foreach ($result_fac_dll->result() as $itemdll) {
          //unset($itemdll->sserviciosId);
          if(($subtotal_v+$iva_v)!=$total){
            $subtotal_v=$total;$iva_v=0;
          }
          if($result_fac_dll->num_rows()==1){
            foreach ($result_fac_dll->result() as $item_dll) {
              if($item_dll->Cantidad==1){
                $subtotal_v=$item_dll->Cu;
                $iva_v=$item_dll->iva;
              }
            }
          }
          if($id==6020){
            $subtotal_v=560.34484;//ajuste temporal
          }
          //===============================================
          // tomar el subtotal directo de las ventas y no tomar el general
          $subtotal_v_dll=0;
          foreach ($result_ven_dll->result() as $item_dll) {
            $subtotal_v_dll=$subtotal_v_dll+($item_dll->cantidad*$item_dll->precio_unitario);
          }
          $subtotal_v=round($subtotal_v_dll,5);
          //==================================================

          $itemdll['FacturasId']=$FacturasId_new;
          $itemdll['Cantidad']=1;
          $itemdll['Unidad']='ACT';
          $itemdll['servicioId']='84111506';
          $itemdll['Descripcion']='Servicios de facturacion';
          $itemdll['Descripcion2']='Devolucion';
          $itemdll['Cu']=$subtotal_v;
          $itemdll['descuento']=0;
          $itemdll['Importe']=$subtotal_v; 
          $itemdll['iva']=$iva_v;
          if($iva_v>0){
            $consin_iva=1;
          }else{
            $consin_iva=0;
          }
          $itemdll['consin_iva'] =$consin_iva;
          $datainsertarray[]=$itemdll;
        //}
        if(count($datainsertarray)>0){
          $this->ModeloCatalogos->insert_batch('f_facturas_servicios',$datainsertarray);
        }
        $respuesta=$this->emitirfacturas($FacturasId_new);
        if($respuesta['resultado']=='correcto'){
          //log_message('error','correcto');
          $this->ModeloCatalogos->updateCatalogo('venta_fk_factura',array('activo'=>0),array('idventa'=>$idventa));
        }
        echo json_encode($respuesta);

      }
    }
  }
  function generafacturarcartaporte($idtraspaso){
    $materialpeligroso=0;
    $TotalDistRec=0;
    $EntradaSalidaMerc='Salida';
    $PesoBrutoTotal=0;//preguntar de onde vendra este valor
    $UnidadPeso='KGM';
    $NumTotalMercancias=0;
    $rempla = array('"');
    $rempla2   = array("");
    $res_transpaso=$this->ModeloCatalogos->getselectwheren('traspasos',array('id'=>$idtraspaso));
    foreach ($res_transpaso->result() as $itemt) {
      $catp_operador = $itemt->catp_operador;
      $catp_vehiculo = $itemt->catp_vehiculo;
      $catp_salida = $itemt->catp_salida;
      $catp_llegada = $itemt->catp_llegada;
    }

    /*
      $datas = $this->input->post();
          $ubicaciones=$datas['ubicaciones'];
          $mercancias=$datas['mercancias'];
          $transportes=$datas['transportes'];
          $aereo=$datas['aereo'];
          $operadores=$datas['operadores'];
          $propietario=$datas['propietario'];
          $arrendatario=$datas['arrendatario'];
          $TranspInternac = $datas['TranspInternac'];
          $TotalDistRec = $datas['TotalDistRec'];
          $EntradaSalidaMerc = $datas['EntradaSalidaMerc'];
          $ViaEntradaSalida = $datas['ViaEntradaSalida'];

          $PesoBrutoTotal = $datas['PesoBrutoTotal'];
          $UnidadPeso = $datas['UnidadPeso'];
          $PesoNetoTotal = $datas['PesoNetoTotal'];
          $NumTotalMercancias = $datas['NumTotalMercancias'];
          $CargoPorTasacion = $datas['CargoPorTasacion'];
          $tipofac = $datas['tipofac'];
          $rid_relacion_cot_folio=$datas['rid_relacion_cot_folio'];
          if(isset($datas['rid_relacion'])){
            $rid_relacion = $datas['rid_relacion'];
          }else{
            $rid_relacion=0;
          }
          

          if(isset($datas['conceptos'])){
              $conceptos = $datas['conceptos'];
              unset($datas['conceptos']);
          }

          $idcliente=$datas['idcliente'];
          $rfc=$datas['rfc'];
          //log_message('error', 'idcliente:'.$idcliente);
          $dcliente=$this->ModeloCatalogos->getselectwheren('cli_facts',array('rid'=>$idcliente));
          foreach ($dcliente->result() as $item) {
              $razon_social=$item->fac_nrs;
              $direccion=$item->fac_dir;
              $cp=$item->fac_cod;
              $rfc=$item->fac_rfc;
              $tel1=$item->fac_te1;
              $email=$item->fac_email;
          }
        
          $pais = 'MEXICO';
          $TipoComprobante=$datas['TipoDeComprobante'];
          if($datas['tipoempresa']>0){
            $tipoempresa=$datas['tipoempresa'];  
          }else{
            $tipoempresa=1;//1 COECSA 2 CADIPA
          }
          if($tipoempresa==1){
            $serie='CO';
            if($datas['sucursal']>0){
              $infosuc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$datas['sucursal'],'conserie'=>1,'activo'=>1));
              foreach ($infosuc->result() as $item) {
                $serie=$item->serie;
              }
            }
          }else{
            $serie='CA';
          }
          if($rfc=='XAXX010101000'){
              //$cp=$codigoPostalEmisor;
              //$RegimenFiscalReceptor=616;
              //$uso_cfdinew='S01';
            $datas['uso_cfdi']='S01';
          }
    */
        $res_config=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        foreach ($res_config->result() as $item_cg) {
          $razon_social=$item_cg->Nombre;
          $direccion=$item_cg->Nombre;
          $cp = $item_cg->CodigoPostal;
          $cp_cp = $item_cg->CodigoPostal;
            $num_ext = $item_cg->Noexterior;
            $num_int = $item_cg->Nointerior;
            $colonia = $item_cg->Colonia;
            $calle = $item_cg->Calle;
            $municipio = $item_cg->Municipio;
            $localidad = $item_cg->localidad;
            $estado = $item_cg->Estado;
            $direccion     =       $calle . ' No ' . $num_ext . ' ' . $num_int . ' Col: ' . 
                                    $colonia . ' , ' . 
                                    $municipio . ' , '.$localidad . ' , '. $estado . ' . ' ;
            $rfc=$item_cg->Rfc;
            $RegimenFiscalReceptor=$item_cg->Regimen;
        }
        $serie='H';
        $pais='MEX';

        $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;//verificar si se tienen series diferentes

        $res_ht=$this->ModeloCatalogos->getselectwheren('historial_transpasos',array('idtranspasos'=>$idtraspaso));
        foreach ($res_ht->result() as $item_ht) {
          $sucursal_o = $item_ht->idsucursal_sale;
          $sucursal_d = $item_ht->idsucursal_entra;
        }
        if($sucursal_o==7 && $sucursal_d==8){
          $materialpeligroso=1;// solo es para que no se timbre si es de general a rentas y viceversa
        }elseif($sucursal_o==8 && $sucursal_d==7){
          $materialpeligroso=1;// solo es para que no se timbre si es de general a rentas y viceversa
        }
        $strq="SELECT * FROM(
                  SELECT 
                    plao.id_pcp,
                    plao.id_ubicacion as ubio,
                    plad.id_ubicacion as ubid 
                  FROM plantilla_carta_porte_ubicaciones as plao 
                  INNER JOIN plantilla_carta_porte_ubicaciones as plad on plad.id_pcp=plao.id_pcp AND plad.activo=1 AND plad.TipoUbicacion='Destino' 
                  WHERE plao.activo=1 AND plao.TipoUbicacion='Origen' GROUP BY plao.id_pcp 
                ) as datos WHERE ubio='$sucursal_o' AND ubid='$sucursal_d'";
        $query = $this->db->query($strq);
        $id_pcp=0;
        foreach ($query->result() as $item) {
          $id_pcp=$item->id_pcp;
        }
        $res_pcp=$this->ModeloCatalogos->getselectwheren('plantilla_carta_porte',array('id'=>$id_pcp));
        foreach ($res_pcp->result() as $item_pcp) {
          $TipoComprobante=$item_pcp->selectTipoComprobante;
          $TranspInternac=$item_pcp->TranspInternac;
          if ($item_pcp->selectTipoFecha=='SA') {
            // code...
          }elseif($item_pcp->selectTipoFecha=='LL'){

          }
        }
        //$result_tra_series=$this->General_model->get_productos_traspaso_series($idtraspaso);
        //$result_tra_lotes=$this->General_model->get_productos_traspaso_lotes($idtraspaso);//agregar igual para f_facturas_servicios y f_facturas_mercancia
        $result_tra=$this->General_model->get_productos_traspasos($idtraspaso,1,1,1,0,0,0);
        $res_pcp=$this->ModeloCatalogos->getselectwheren('plantilla_carta_porte',array('id'=>$id_pcp));

        $strq1="SELECT pcpu.TipoUbicacion,pcpu.DistanciaRecorrida,pcpu.id_ubicacion,suc.*
              FROM plantilla_carta_porte_ubicaciones as pcpu
              INNER JOIN sucursal as suc on suc.id=pcpu.id_ubicacion
              WHERE pcpu.id_pcp='$id_pcp' AND pcpu.activo=1  
              ORDER BY pcpu.TipoUbicacion  DESC";
        $query1 = $this->db->query($strq1);
        foreach ($query1->result() as $item1) {
          if($item1->DistanciaRecorrida>0){
            $TotalDistRec+=$item1->DistanciaRecorrida;
          }
        }
        $strq2="SELECT trans.* 
          FROM plantilla_carta_porte as pcp
          INNER JOIN transporte_federal as trans on trans.id=pcp.selectVehiculos
          WHERE pcp.id='$id_pcp'";
          $query2 = $this->db->query($strq2);
          if($catp_vehiculo>0){
            $query2=$this->ModeloCatalogos->getselectwheren('transporte_federal',array('id'=>$catp_vehiculo));
          }
        $strq3="SELECT ope.* 
          FROM plantilla_carta_porte_figuras as pcpf
          INNER JOIN operadores as ope on ope.id=pcpf.id_fig
          WHERE pcpf.id_pcp='$id_pcp' AND pcpf.activo=1 AND ope.TipoFigura='01'";
        $query3 = $this->db->query($strq3);
        if($catp_operador>0){
          $query3=$this->ModeloCatalogos->getselectwheren('operadores',array('id'=>$catp_operador));
        }
        if($sucursal_o>0){
          $resut_suc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$sucursal_o));
          foreach ($resut_suc->result() as $item_suc) {
              $cp_cp = $item_suc->cp_cp;
          }  
        }

        $data = array(
                
                "nombre"        => $razon_social,
                "direccion"     => $direccion, 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "clienteId"       => 0,
                "serie" =>$serie,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => 'PUE',
                "tarjeta"       => '',
                "MetodoPago"        => 99,
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => 'XXX',
                "observaciones" => '',
                "numproveedor"  => '',
                "numordencompra"=> '',
                "Lote"          => '',
                "Paciente"      => '',//no se ocupa
                //"Caducidad"     => $datas['Caducidad'],//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => 'S01',
                "subtotal"      => 0,
                "iva"           => 0,
                "total"         => 0,
                "honorario"     => 0,
                /*"ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],*/
                "ivaretenido"   => '',
                "isr"           => '',
  
                "CondicionesDePago" => '',
                //"outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>1,
                "f_relacion"    => 0,
                "f_r_tipo"      => 0,
                "f_r_uuid"      => 0,
                "cartaporte"    => 1,
                "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
                "LugarExpedicion"=>$cp_cp
                //'tipoempresa'   => $tipoempresa,
                //'tipofac'       => $datas['tipofac'],
                //'sucursal'      => $datas['sucursal']
            );
        /*
            //$facturainfo="Nombre: $razon_social <br>R.F.C.: $rfc <br>Direccion: $direccion <br>C.P.: $cp <br>Tel: $tel1 <br>eMail: $email";
            //$data['rid_relacion_cot_folio']=$rid_relacion_cot_folio;//descomentar cuanto tengamos acceso a su base
          if(isset($datas['pg_global'])){
            $data['pg_global']=$datas['pg_global'];
          }
          if(isset($datas['pg_periodicidad'])){
              $data['pg_periodicidad']=$datas['pg_periodicidad'];
          }
          if(isset($datas['pg_meses'])){
              $data['pg_meses']=$datas['pg_meses'];
          }
          if(isset($datas['pg_anio'])){
              $data['pg_anio']=$datas['pg_anio'];
          }
        */
        $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
        foreach ($result_tra as $item_tr) {
            $res_ser = $this->General_model->nom_servicio($item_tr->servicioId_sat);
          //if($item_tr->serie==''){
            $serie=$item_tr->serie;
            if($res_ser['mpeligro']=='0'){}else{ $materialpeligroso=1;}
            if($item_tr->peso>0){
              $peso=$item_tr->peso;
            }else{
              $peso=$item_tr->cantidad;
            }
            $PesoBrutoTotal+=$peso;
            $dataco['FacturasId']=$FacturasId;
            //$dataco['NoIdentificacion'] =$DATAc[$i]->NoIdentificacion;
            $dataco['Cantidad'] =$item_tr->cantidad;
            $dataco['Unidad'] =$item_tr->unidad_sat;
            $dataco['servicioId'] =$item_tr->servicioId_sat;
            $dataco['Descripcion'] =str_replace($rempla, $rempla2, rtrim($res_ser['nom']));
            if($item_tr->tipo==0){
              $dataco['Descripcion2'] =$item_tr->nombre.$serie; //log_message('error','Entra tipo 1');
            }else{
              $materialpeligroso=1;
              $dataco['Unidad'] ='H87';
              $dataco['servicioId'] ='12141904';
              $dataco['Descripcion'] ='Oxígeno o';
              $dataco['Descripcion2'] ="Recarga oxígeno ".$item_tr->codigo." / ".$item_tr->capacidad." L"; //log_message('error','Entra diferente de tipo 1');
            }
            $dataco['Cu'] =0;
            $dataco['descuento'] =0;
            $dataco['Importe'] =0;
            $dataco['iva'] =0;
            $dataco['consin_iva'] =0;
            //log_message('error','item_tr '.json_encode($item_tr));
            //log_message('error','Descripcion2 '.$dataco['Descripcion2']);
            if($dataco['Descripcion2']!=""){
              $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
            }
            $NumTotalMercancias++;
          //}
        }
        /*
        foreach ($result_tra_series->result() as $item_trs) {
            if($item_trs->mpeligro=='0'){}else{ $materialpeligroso=1;}
            if($item_trs->peso>0){
              $peso=$item_trs->peso;
            }else{
              $peso=$item_trs->cantidad;
            }
            $PesoBrutoTotal+=$peso;
            $dataco['FacturasId']=$FacturasId;
            //$dataco['NoIdentificacion'] =$DATAc[$i]->NoIdentificacion;
            $dataco['Cantidad'] =$item_trs->cantidad;
            $dataco['Unidad'] =$item_trs->clave_unidad;
            $dataco['servicioId'] =$item_trs->clave_servicio;
            $dataco['Descripcion'] =str_replace($rempla, $rempla2, rtrim($item_trs->nom_servico));
            $dataco['Descripcion2'] =$item_trs->producto.' '.$item_trs->serie;
            $dataco['Cu'] =0;
            $dataco['descuento'] =0;
            $dataco['Importe'] =0;
            $dataco['iva'] =0;
            $dataco['consin_iva'] =0;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
            $NumTotalMercancias++;
        }
        foreach ($result_tra_lotes->result() as $item_lts) {
            if($item_lts->mpeligro=='0'){}else{ $materialpeligroso=1;}
            if($item_lts->peso>0){
              $peso=$item_lts->peso;
            }else{
              $peso=$item_lts->cantidad;
            }
            $PesoBrutoTotal+=$peso;
            $dataco['FacturasId']=$FacturasId;
            //$dataco['NoIdentificacion'] =$DATAc[$i]->NoIdentificacion;
            $dataco['Cantidad'] =$item_lts->cantidad;
            $dataco['Unidad'] =$item_lts->clave_unidad;
            $dataco['servicioId'] =$item_lts->clave_servicio;
            $dataco['Descripcion'] =str_replace($rempla, $rempla2, rtrim($item_lts->nom_servico));
            $dataco['Descripcion2'] =$item_lts->producto.' '.$item_lts->lote;
            $dataco['Cu'] =0;
            $dataco['descuento'] =0;
            $dataco['Importe'] =0;
            $dataco['iva'] =0;
            $dataco['consin_iva'] =0;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
            $NumTotalMercancias++;
        }
        */
        
        
        //==========================================================================
        if(true){
                  $this->ModeloCatalogos->Insert('f_facturas_carta_porte',array('FacturasId'=>$FacturasId,'TranspInternac'=>$TranspInternac,'TotalDistRec'=>$TotalDistRec,'EntradaSalidaMerc'=>$EntradaSalidaMerc,'ViaEntradaSalida'=>''));

                  $this->ModeloCatalogos->Insert('f_facturas_mercancias',array('FacturasId'=>$FacturasId,'PesoBrutoTotal'=>$PesoBrutoTotal,'UnidadPeso'=>$UnidadPeso,'NumTotalMercancias'=>$NumTotalMercancias,'CargoPorTasacion'=>''));
                //=============================Ubicaciones=====================================================
                    foreach ($query1->result() as $item1) {
                        if($item1->TipoUbicacion=='Origen'){

                          
                          $hora1 = date("H:i:s",strtotime($this->fechahoyc.' +60 minutes'));
                          $FechaHoraSalidaLlegada=date('Y-m-d').'T'.$hora1;
                          if($catp_salida!='' && $catp_salida!='null' && $catp_salida!=null){
                            $FechaHoraSalidaLlegada=date("Y-m-d",strtotime($catp_salida)).'T'.date("H:i:s",strtotime($catp_salida));
                            log_message('error','Salida '.$FechaHoraSalidaLlegada);
                          }
                        }else{
                          $hora1 = date("H:i:s",strtotime($this->fechahoyc.' +120 minutes'));
                          $FechaHoraSalidaLlegada=date('Y-m-d').'T'.$hora1;
                          if($catp_llegada!='' && $catp_llegada!='null' && $catp_llegada!=null){
                            $FechaHoraSalidaLlegada=date("Y-m-d",strtotime($catp_llegada)).'T'.date("H:i:s",strtotime($catp_llegada));
                            log_message('error','Llegada '.$FechaHoraSalidaLlegada);
                          }
                        }
                        $datafu['FacturasId']=$FacturasId;
                        $datafu['TipoUbicacion'] = $item1->TipoUbicacion;
                        //$datafu['TipoEstacion'] = $DATAu[$j]->TipoEstacion;
                        //$datafu['IDUbicacion'] = $DATAu[$j]->IDUbicacion;
                        $datafu['RFCRemitenteDestinatario'] = $rfc;
                        $datafu['NombreRFC'] = $razon_social;
                        //$datafu['NumRegIdTrib'] = $DATAu[$j]->NumRegIdTrib;
                        //$datafu['ResidenciaFiscal'] = $DATAu[$j]->ResidenciaFiscal;
                        //$datafu['NumEstacion'] = $DATAu[$j]->NumEstacion;
                        //$datafu['NombreEstacion'] = $DATAu[$j]->NombreEstacion;
                        $datafu['FechaHoraSalidaLlegada'] = $FechaHoraSalidaLlegada;
                        $datafu['DistanciaRecorrida'] = $item1->DistanciaRecorrida;
                        $datafu['Calle'] = $item1->cp_calle;
                        $datafu['NumeroExterior'] = $item1->cp_n_ex;
                        $datafu['NumeroInterior'] = $item1->cp_n_int;
                        $datafu['Colonia'] = $item1->cp_colonia;
                        $datafu['Localidad'] = $item1->cp_localidad;
                        $datafu['Referencia'] = $item1->cp_ref;
                        $datafu['Municipio'] = $item1->cp_minicipio;
                        $datafu['Estado'] = $item1->cp_estado;
                        $datafu['Pais'] = $item1->cp_pais;
                        $datafu['CodigoPostal'] = $item1->cp_cp;
                        $datafu['id_ubicacion'] = $item1->id_ubicacion;

                        $this->ModeloCatalogos->Insert('f_facturas_ubicaciones',$datafu);
                    }
                //=============================mercancias=====================================================
                    //$DATAme = json_decode($mercancias);
                    foreach ($result_tra as $item_tr) {
                      //if($item_tr->serie==''){
                        $serie=$item_tr->serie;
                        $datafm['FacturasId'] = $FacturasId;
                        $datafm['BienesTransp']   = $item_tr->servicioId_sat;
                        $datafm['Descripcion']    = str_replace($rempla, $rempla2, $item_tr->nombre).$serie;
                        $datafm['Cantidad']       = $item_tr->cantidad;
                        $datafm['ClaveUnidad']    = $item_tr->unidad_sat;
                        if($item_tr->tipo==0){

                        }else{
                          $datafm['ClaveUnidad'] ='H87';
                          $datafm['BienesTransp'] ='12141904';
                          $datafm['Descripcion'] ="Recarga oxígeno ".$item_tr->codigo." / ".$item_tr->capacidad." L"; //log_message('error','Entra diferente de tipo 1');
                        }
                        //$datafm['Dimensiones']    = $DATAme[$k]->Dimensiones;
                        /*
                        if($DATAme[$k]->Embalaje=='null'){
                            $datafm['Embalaje']       = '';
                        }else{
                            $datafm['Embalaje']       = $DATAme[$k]->Embalaje;
                        }
                        */
                        
                        //$datafm['DescripEmbalaje'] = $DATAme[$k]->DescripEmbalaje;
                        if($item_tr->peso>0){
                          $peso=$item_tr->peso;
                        }else{
                          $peso=$item_tr->cantidad;
                        }
                        $datafm['PesoEnKg']       = $peso;
                        if($item_tr->iva>0){
                          $valor=$item_tr->precio*1.16;
                        }else{
                          $valor=$item_tr->precio;
                        }
                        $datafm['ValorMercancia'] = $valor;
                        //$datafm['Moneda']         = $DATAme[$k]->Moneda;
                        $datafm['Unidad']         = $item_tr->unidad_sat;
                        
                        $this->ModeloCatalogos->Insert('f_facturas_mercancia',$datafm);
                      //}
                    }
                    /*
                    foreach ($result_tra_series->result() as $item_trs) {
                        $datafm['FacturasId'] = $FacturasId;
                        $datafm['BienesTransp']   = $item_trs->clave_servicio;
                        $datafm['Descripcion']    = str_replace($rempla, $rempla2, $item_trs->producto.' '.$item_trs->serie);
                        $datafm['Cantidad']       = $item_trs->cantidad;
                        $datafm['ClaveUnidad']    = $item_trs->clave_unidad;
                        //$datafm['Dimensiones']    = $DATAme[$k]->Dimensiones;
                        
                        
                        //$datafm['DescripEmbalaje'] = $DATAme[$k]->DescripEmbalaje;
                        if($item_trs->peso>0){
                          $peso=$item_trs->peso;
                        }else{
                          $peso=$item_trs->cantidad;
                        }
                        $datafm['PesoEnKg']       = $peso;
                        if($item_trs->iva>0){
                          $valor=$item_trs->precio*1.16;
                        }else{
                          $valor=$item_trs->precio;
                        }
                        $datafm['ValorMercancia'] = $item_trs->precio_final;
                        //$datafm['Moneda']         = $DATAme[$k]->Moneda;
                        $datafm['Unidad']         = $item_tr->clave_unidad;
                        
                        $this->ModeloCatalogos->Insert('f_facturas_mercancia',$datafm);
                    }
                    foreach ($result_tra_lotes->result() as $item_lts) {
                        $datafm['FacturasId'] = $FacturasId;
                        $datafm['BienesTransp']   = $item_lts->clave_servicio;
                        $datafm['Descripcion']    = str_replace($rempla, $rempla2, $item_lts->producto.' '.$item_lts->lote);
                        $datafm['Cantidad']       = $item_lts->cantidad;
                        $datafm['ClaveUnidad']    = $item_lts->clave_unidad;
                        //$datafm['Dimensiones']    = $DATAme[$k]->Dimensiones;
                        
                        
                        //$datafm['DescripEmbalaje'] = $DATAme[$k]->DescripEmbalaje;
                        if($item_lts->peso>0){
                          $peso=$item_lts->peso;
                        }else{
                          $peso=$item_lts->cantidad;
                        }
                        $datafm['PesoEnKg']       = $peso;
                        if($item_lts->iva>0){
                          $valor=$item_lts->precio*1.16;
                        }else{
                          $valor=$item_lts->precio;
                        }
                        $datafm['ValorMercancia'] = $valor;
                        //$datafm['Moneda']         = $DATAme[$k]->Moneda;
                        $datafm['Unidad']         = $item_lts->unidad;
                        
                        $this->ModeloCatalogos->Insert('f_facturas_mercancia',$datafm);
                    }
                    */
                    
                    
                //=============================transporte=====================================================
                    //$DATAtrans = json_decode($transportes);
                    foreach ($query2->result() as $item2) {
                        $dataft['FacturasId']=$FacturasId;
                        $dataft['PermSCT'] = $item2->tipo_permiso_sct;
                        $dataft['NumPermisoSCT'] = $item2->num_permiso_sct;
                        $dataft['NombreAseg'] = $item2->nombre_aseguradora;
                        $dataft['NumPolizaSeguro'] = $item2->num_poliza_seguro;
                        $dataft['ConfigVehicular'] = $item2->configuracion_vhicular;
                        $dataft['PlacaVM'] = $item2->placa_vehiculo_motor;
                        $dataft['AnioModeloVM'] = $item2->anio_modelo_vihiculo_motor;
                        $dataft['SubTipoRem'] = $item2->subtipo_remolque;
                        $dataft['Placa'] = $item2->placa_remolque;

                        $this->ModeloCatalogos->Insert('f_facturas_transporte_fed',$dataft);
                    }
                //=============================aerero=====================================================
                    /*
                    $DATAtaer = json_decode($aereo);
                    for ($a=0;$a<count($DATAtaer);$a++) {
                        $datafta['FacturasId']=$FacturasId;
                        $datafta['PermSCT'] = $DATAtaer[$a]->PermSCT;
                        $datafta['NumPermisoSCT'] = $DATAtaer[$a]->NumPermisoSCT;
                        $datafta['MatriculaAeronave'] = $DATAtaer[$a]->MatriculaAeronave;
                        $datafta['NombreAseg'] = $DATAtaer[$a]->NombreAseg;
                        $datafta['NumPolizaSeguro'] = $DATAtaer[$a]->NumPolizaSeguro;
                        $datafta['NumeroGuia'] = $DATAtaer[$a]->NumeroGuia;
                        $datafta['LugarContrato'] = $DATAtaer[$a]->LugarContrato;
                        $datafta['RFCTransportista'] = $DATAtaer[$a]->RFCTransportista;
                        $datafta['CodigoTransportista'] = $DATAtaer[$a]->CodigoTransportista;
                        $datafta['NumRegIdTribTranspora'] = $DATAtaer[$a]->NumRegIdTribTranspora;
                        $datafta['ResidenciaFiscalTranspor'] = $DATAtaer[$a]->ResidenciaFiscalTranspor;
                        $datafta['NombreTransportista'] = $DATAtaer[$a]->NombreTransportista;
                        $datafta['RFCEmbarcador'] = $DATAtaer[$a]->RFCEmbarcador;
                        $datafta['NumRegIdTribEmbarc'] = $DATAtaer[$a]->NumRegIdTribEmbarc;
                        $datafta['ResidenciaFiscalEmbarc'] = $DATAtaer[$a]->ResidenciaFiscalEmbarc;
                        $datafta['NombreEmbarcador'] = $DATAtaer[$a]->NombreEmbarcador;

                        $this->ModeloCatalogos->Insert('f_facturas_transporte_aereo',$datafta);
                    }
                    */
                //==================================================================================
                //=============================operadores=====================================================
                    //$DATAtoper = json_decode($operadores);
                    foreach ($query3->result() as $item3) {
                        $dataffo['FacturasId'] = $FacturasId;
                        $dataffo['RFCOperador'] = $item3->rfc_del_operador;
                        $dataffo['NumLicencia'] = $item3->no_licencia;
                        $dataffo['NombreOperador'] = $item3->operador;
                        $dataffo['NumRegIdTribOperador'] = '';
                        $dataffo['ResidenciaFiscalOperador'] ='';
                        
                        $dataffo['adddomicilio'] = 0;//0 no se incluye 1 si se incluye
                        $dataffo['Calle'] = $item3->calle;
                        $dataffo['NumeroExterior'] = $item3->num_ext;
                        $dataffo['NumeroInterior'] = $item3->num_int;
                        $dataffo['Colonia'] = $item3->colo;
                        $dataffo['Localidad'] = $item3->loca;
                        $dataffo['Referencia'] = $item3->ref;
                        $dataffo['Municipio'] = $item3->muni;
                        $dataffo['Estado'] = $item3->estado;
                        $dataffo['Pais'] = $item3->pais;
                        $dataffo['CodigoPostal'] = $item3->codigo_postal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_operadores',$dataffo);
                    }
                //==================================================================================
                //=============================propietario=====================================================
                    /*
                    $DATAprop = json_decode($propietario);
                    for ($r=0;$r<count($DATAprop);$r++) {
                        $dataffp['FacturasId']=$FacturasId;
                        $dataffp['RFCPropietario'] = $DATAprop[$r]->RFCPropietario;
                        $dataffp['NombrePropietario'] = $DATAprop[$r]->NombrePropietario;
                        $dataffp['NumRegIdTribPropietario'] = $DATAprop[$r]->NumRegIdTribPropietario;
                        if($DATAprop[$r]->ResidenciaFiscalPropietario=='null'){
                            $dataffp['ResidenciaFiscalPropietario'] = '';
                        }else{
                            $dataffp['ResidenciaFiscalPropietario'] = $DATAprop[$r]->ResidenciaFiscalPropietario;
                        }
                        
                        $dataffp['adddomicilio'] = $DATAprop[$r]->adddomiciliop;
                        $dataffp['Calle'] = $DATAprop[$r]->Calle;
                        $dataffp['NumeroExterior'] = $DATAprop[$r]->NumeroExterior;
                        $dataffp['NumeroInterior'] = $DATAprop[$r]->NumeroInterior;
                        $dataffp['Colonia'] = $DATAprop[$r]->Colonia;
                        $dataffp['Localidad'] = $DATAprop[$r]->Localidad;
                        $dataffp['Referencia'] = $DATAprop[$r]->Referencia;
                        $dataffp['Municipio'] = $DATAprop[$r]->Municipio;
                        $dataffp['Estado'] = $DATAprop[$r]->Estado;
                        $dataffp['Pais'] = $DATAprop[$r]->Pais;
                        $dataffp['CodigoPostal'] = $DATAprop[$r]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_propietario',$dataffp);
                    }
                    */
                //=============================arrendatario=====================================================
                    /*
                    $DATAarre = json_decode($arrendatario);
                    for ($r=0;$r<count($DATAarre);$r++) {
                        $dataffa['FacturasId']=$FacturasId;
                        $dataffa['RFCArrendatario'] = $DATAarre[$r]->RFCArrendatario;
                        $dataffa['NombreArrendatario'] = $DATAarre[$r]->NombreArrendatario;
                        $dataffa['NumRegIdTribArrendatario'] = $DATAarre[$r]->NumRegIdTribArrendatario;
                        $dataffa['ResidenciaFiscalOperador'] = $DATAarre[$r]->ResidenciaFiscalArrendatario;
                        $dataffa['adddomicilio'] = $DATAarre[$r]->adddomicilioa;
                        $dataffa['Calle'] = $DATAarre[$r]->Calle;
                        $dataffa['NumeroExterior'] = $DATAarre[$r]->NumeroExterior;
                        $dataffa['NumeroInterior'] = $DATAarre[$r]->NumeroInterior;
                        $dataffa['Colonia'] = $DATAarre[$r]->Colonia;
                        $dataffa['Localidad'] = $DATAarre[$r]->Localidad;
                        $dataffa['Referencia'] = $DATAarre[$r]->Referencia;
                        $dataffa['Municipio'] = $DATAarre[$r]->Municipio;
                        $dataffa['Estado'] = $DATAarre[$r]->Estado;
                        $dataffa['Pais'] = $DATAarre[$r]->Pais;
                        $dataffa['CodigoPostal'] = $DATAarre[$r]->CodigoPostal;

                        $this->ModeloCatalogos->Insert('f_facturas_figtrans_arrendatario',$dataffa);
                    }
                    */
                //==================================================================================
                        /*
                        if($datas['cartaporte']==1){
                          $CveTransporte=$datas['CveTransporte'];

                          $this->ModeloCatalogos->Insert('f_facturas_figura_transporte',array('FacturasId'=>$FacturasId,'CveTransporte'=>$CveTransporte));
                        }
                        */
        }       
                //==================================================================================
        //==========================================================================
        $this->ModeloCatalogos->updateCatalogo('traspasos',array('Idfactura'=>$FacturasId),array('id'=>$idtraspaso));
        if($materialpeligroso==0){
          $respuesta=$this->emitirfacturas($FacturasId,0); 
        }else{
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('tim'=>0),array('FacturasId'=>$FacturasId));//1 por defecto si se timbro 0 solo el documento
          $respuesta = array(
                        'resultado'=>'correcto',
                        'Mensajeadicional'=>'Documento Generado',
                        'facturaId'=>$FacturasId
                        );
        }
        
        
        
        echo json_encode($respuesta);
  }


} 