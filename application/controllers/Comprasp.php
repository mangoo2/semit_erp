<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Comprasp extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCompras'); 
        $this->load->model('ModelProductos'); 
        

        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoy_normal = date('d/m/Y');
        $this->horahoy_normal = date('H:i:s');
        $this->fecha_larga = date('Y-m-d H:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechacod = date('ymdHis');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,29);// perfil y id del submenu
            //$this->sucusal=1;//cuando ya esten las sucursales
            //$this->folio=$this->sucusal.'-'.$this->fechacod;
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    private function set_barcode($code){
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $barcode = Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
        //log_message('error','barcode: '.$barcode);
    }

	public function index(){
        $data['btn_active']=6;
        $data['btn_active_sub']=18;
        $data['viewcompra']=0;
        $data['fecha']=$this->fecha_reciente;
        $this->limpiartabla();
        $data['rows_precompras']=$this->ModeloCatalogos->getselectwheren('compra_erp',array('sucursal'=>$this->sucursal,'precompra'=>1,'activo'=>1,"cancelado"=>0));
        $data['tanques']=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>8,'tipo'=>0,'estatus'=>1));
        $data['sucus']=$this->ModeloCatalogos->getselectwherenallOrder('sucursal',array('activo'=>1),"orden","ASC");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('compras/form');
        $this->load->view('templates/footer');
        $this->load->view('compras/formjs');
        $this->load->view('ventasp/modalp');
    }
    function searchproveedor(){
        $pro = $this->input->get('search');
        $results=$this->General_model->getseleclike('proveedores','nombre',$pro);
        echo json_encode($results->result());
    }
    function searchproductos(){
        $pro = $this->input->get('search');
        //$results=$this->ModeloCatalogos->getsearchproductoslike($pro);
        $results=$this->ModeloCatalogos->getsearchproductoslike_compras($pro);
        
        //echo $results;
        echo json_encode($results->result());
    }

    function addproducto(){
        $params = $this->input->post();
        $prod=$params['id'];
        $tipo=$params['tipo'];
        $cantidad=$params['cantidad'];
        //log_message('error','cantidad: '.$cantidad);
        $cant=1;
        if($cantidad>0){
            $cant=$cantidad;
        }
        $des=0;
        $desp=0;
        $costo=0;
        $validar=0;
        $porc_isr=0;
        //$result = $this->ModelProductos->obtenerdatosproductoid($prod,$this->sucursal);
        if($tipo==0){
            $result = $this->ModeloCatalogos->getselectwheren('productos',array('id'=>$prod));
            foreach ($result->result() as $item){
                $id = $item->id;
                $idProducto = $item->idProducto;
                $nombre = $item->nombre;
                $costo=$item->costo_compra;
                $incluye_iva = $item->incluye_iva_comp;
                $iva = $item->iva_comp;
                //log_message('error','iva: '.$iva);
                //log_message('error','incluye_iva: '.$incluye_iva);
                $precio_con_ivax=0;
                if($incluye_iva==1){
                    $precio_con_ivax=round($costo,2);
                    if($iva==0){
                        $incluye_iva=0;
                    }else{
                        $precio_con_ivax=round($costo*1.16,2);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $precio_con_ivax=round($costo/$siva,2);
                    }else{
                        $precio_con_ivax=round($item->costo_compra,2);  
                    }
                }
                $porc_isr = $item->porc_isr;
                //log_message('error','porc_isr: '.$porc_isr);
                //log_message('error','precio_con_ivax: '.$precio_con_ivax);
                //var_dump($costo_compra);die;
                //============================================ quitar el iva a los productos
                    
                //============================================
                //$oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'costo_compra'=>$costo,'stock'=>$stock,'costo_compra'=>$costo);
                $oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'costo_compra'=>$costo,"tipo"=>0,"capac"=>0,"precio_con_ivax"=>$precio_con_ivax,'incluye_iva'=>$incluye_iva,"porc_isr"=>$porc_isr);
                //log_message('error','oProductoe: '.json_encode($oProducto));

                if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                    //log_message('error','existe producto');
                    $idx = array_search($oProducto, $_SESSION['pro']);
                    //log_message('error','existe producto idx'.$idx);
                    $_SESSION['can'][$idx]+=$cant;
                    //log_message('error','nombre: '.$nombre);
                    //log_message('error','idProducto: '.$idProducto);
                    $_SESSION['idProducto'][$idx]=$idProducto;
                    $_SESSION['nombre'][$idx]=$nombre;
                    $_SESSION['costo_compra'][$idx]=$costo;
                    $_SESSION['cost'][$idx]=$costo;
                    $_SESSION['precio_con_ivax'][$idx]=$precio_con_ivax;
                    $_SESSION['incluye_iva'][$idx]=$incluye_iva;
                }else{ //sino lo agrega
                    //log_message('error','crea nuevo producto');
                    //log_message('error','porc_isr en else: '.$porc_isr);
                    array_push($_SESSION['pro'],$oProducto);
                    array_push($_SESSION['can'],$cant);
                    array_push($_SESSION['des'],$des);
                    array_push($_SESSION['desp'],$desp);
                    array_push($_SESSION['cost'],$costo);
                    array_push($_SESSION['incluye_iva'],$incluye_iva);
                    array_push($_SESSION['precio_con_ivax'],$precio_con_ivax);
                    array_push($_SESSION['tipo'],0);
                    array_push($_SESSION['capac'],0);
                    array_push($_SESSION['porc_isr'],$porc_isr);
                }   
                $validar=1;
            }
        }else{
            $result = $this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$prod));
            foreach ($result->result() as $item){
                $id = $item->id;
                $idProducto = $item->id;
                $nombre = $item->codigo;
                $costo=$item->precioc;
                $capac=$item->capacidad;
                $porc_isr=0;
                $oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'costo_compra'=>$costo,"tipo"=>1,"capac"=>$capac,"precio_con_ivax"=>$costo,'incluye_iva'=>0,"porc_isr"=>0);
                if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                    $idx = array_search($oProducto, $_SESSION['pro']);
                    $_SESSION['can'][$idx]+=$cant;
                }else{ //sino lo agrega
                    array_push($_SESSION['pro'],$oProducto);
                    array_push($_SESSION['can'],$cant);
                    array_push($_SESSION['des'],$des);
                    array_push($_SESSION['desp'],$desp);
                    array_push($_SESSION['cost'],$costo);
                    array_push($_SESSION['incluye_iva'],0);
                    array_push($_SESSION['precio_con_ivax'],$costo);
                    array_push($_SESSION['tipo'],1);
                    array_push($_SESSION['capac'],$capac);
                    array_push($_SESSION['porc_isr'],$porc_isr);
                }   
                $validar=1;
            }
        }
        echo $validar;
    }

    function id_compra_producto(){
        //log_message('error','id_compra_producto function: ');
        $params = $this->input->post();
        $idcompra_detalle=$params['id'];
        $cant=1;
        $des=0;
        $desp=0;
        $costo=0;
        $validar=0;
        //$result = $this->ModelProductos->obtenerdatosproductoid($prod,$this->sucursal);
        $result = $this->ModeloCompras->get_compra_erp_detalle($idcompra_detalle);///getselectwheren('compra_erp_detalle',array('id'=>$idcompra_detalle));
        foreach ($result->result() as $item){
            $id = $item->id;
            if($item->tipo==0){
                $nombre = $item->nombre;
                $idProducto = $item->idProducto;
            }else{
                $nombre = $item->codigo;
                $idProducto = $item->id_tanque;
            }
            $costo=$item->precio_unitario;
            $cantidad=$item->cantidad;
            $tipo=$item->tipo;
            $capac=$item->capacidad;
            $incluye_iva = $item->incluye_iva_comp;
            $iva = $item->iva_comp;
            //log_message('error','iva: '.$iva);
            //log_message('error','incluye_iva: '.$incluye_iva);
            $precio_con_ivax=0;
            if($incluye_iva==1){
                $precio_con_ivax=round($costo,2);
                if($iva==0){
                    $incluye_iva=0;
                }else{
                    $precio_con_ivax=round($costo*1.16,2);
                }
            }else{
                if($iva>0){
                    $siva=1.16;
                    $precio_con_ivax=round($costo/$siva,2);
                }
            }

            $oProducto=array("id"=>$id,"idProducto"=>$idProducto,"nombre"=>$nombre,'costo_compra'=>$costo,"tipo"=>$tipo,"capac"=>$capac,"precio_con_ivax"=>$precio_con_ivax,'incluye_iva'=>$incluye_iva);
            if(in_array($oProducto, $_SESSION['pro'])){  
                //log_message('error','existe producto');
                $idx = array_search($oProducto, $_SESSION['pro']);
                //log_message('error','existe producto idx'.$idx);
                $_SESSION['can'][$idx]+=$cant;
            }else{ //sino lo agrega
                //log_message('error','crea nuevo producto');
                array_push($_SESSION['pro'],$oProducto);
                array_push($_SESSION['can'],$cantidad);
                array_push($_SESSION['des'],$des);
                array_push($_SESSION['desp'],$desp);
                array_push($_SESSION['cost'],$costo);
                array_push($_SESSION['incluye_iva'],$incluye_iva);
                array_push($_SESSION['precio_con_ivax'],$precio_con_ivax);
                array_push($_SESSION['tipo'],$tipo);
                array_push($_SESSION['capac'],$capac);
            }   
        }
        echo $validar;
    }

    function viewproductos(){
        //log_message('error','viewproductos function: ');
        $edit=$this->input->post("edit");
        $id=$this->input->post("id");
        $tipo=$this->input->post("tipo");
        $eoce=$this->input->post("eoce");
        //log_message('error','edit: '.$edit);
        //====================================
        $count = 0;
        $html='';

        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            $des=$_SESSION['des'][$count];
            $desp=$_SESSION['desp'][$count];
            $costo_compra=$_SESSION['cost'][$count];
            $incluye_iva=$fila['incluye_iva'];
            $porc_isr=0;
            //if($edit==1 && $tipo==0){ //acá me quedo para validar si editó un producto jalar su nombre, codigo y costo de compra de nuevo
            if($fila['tipo']==0){ //producto
                //$result = $this->ModeloCatalogos->getselectwheren('productos',array('id'=>$id));
                $result = $this->ModeloCatalogos->getselectwheren('productos',array('id'=>$fila["id"]));
                foreach ($result->result() as $p){
                    $id_prod = $p->id;
                    //$costo_compra=$p->costo_compra;
                    $incluye_iva=$p->incluye_iva_comp;
                    $name_edit=$p->nombre;
                    $idProducto=$p->idProducto;
                    $iva = $p->iva_comp;
                    $precio_con_ivax=0;
                    $porc_isr=$p->porc_isr;
                    if($incluye_iva==1){
                        $precio_con_ivax=round($costo_compra,2);
                        if($iva==0){
                            $incluye_iva=0;
                        }else{
                            $precio_con_ivax=round($costo_compra*1.16,2);
                        }
                    }else{
                        if($iva>0){
                            $siva=1.16;
                            $precio_con_ivax=round($costo_compra/$siva,2);
                        }else{
                            $precio_con_ivax=round($p->costo_compra,2);  
                        }
                    }

                    //log_message('error','id prod de session: '.$fila['id']);
                    //if($id == $fila['id']){
                    if($id_prod == $fila['id']){
                        $_SESSION['nombre'][$count]=$name_edit;
                        $_SESSION['idProducto'][$count]=$idProducto;
                        //$_SESSION['cost'][$count]=$costo_compra;
                        $_SESSION['costo_compra'][$count]=$costo_compra;
                        $_SESSION['incluye_iva'][$count]=$incluye_iva;
                        $_SESSION['precio_con_ivax'][$count]=$precio_con_ivax;
                        $_SESSION['tipo'][$count]=0;
                        $_SESSION['capac'][$count]=0;
                        //log_message('error','nombre de session: '.$_SESSION['nombre'][$count]);
                        $cod=$idProducto;
                        $name=$name_edit;
                        $fila['nombre']=$name_edit;
                        $fila['idProducto']=$cod;
                    }
                }
            }if($fila['tipo']==1){ //recargas
                $result = $this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$fila['id']));
                foreach ($result->result() as $item){
                    $id_rec = $item->id;
                    $idProducto = $item->id;
                    $name_edit = $item->codigo;
                    $costo=$item->precioc;
                    $capac=$item->capacidad;
                    $porc_isr=0;
                    if($id_rec == $fila['id']){
                        $cod=$name_edit;
                        $name="Recarga de oxígeno para tanque ".$cod." de ".$capac."L";

                        $_SESSION['nombre'][$count]=$name;
                        $_SESSION['idProducto'][$count]=$idProducto;
                        $_SESSION['cost'][$count]=$costo;
                        $_SESSION['costo_compra'][$count]=$costo;
                        $_SESSION['incluye_iva'][$count]=0;
                        $_SESSION['precio_con_ivax'][$count]=$costo;
                        $_SESSION['tipo'][$count]=1;
                        $_SESSION['capac'][$count]=$capac;
                        //log_message('error','nombre de session: '.$_SESSION['nombre'][$count]);
                        
                        $fila['nombre']=$name_edit;
                        $fila['idProducto']=$cod;
                    }
                }
                $cant_litros = $Cantidad * 9500;
            }
            $descuento=0;
            if($desp>0){
                $descuento_l=$desp.'%';
                $descuento=round(($Cantidad*$costo_compra)*($desp/100), 2);
            }else{
                $descuento_l='$ '.$des;
                $descuento=$des;
            }
           // $cantotal=($Cantidad*$costo_compra)-$descuento;
            if($fila['tipo']==1){ //recargas
                $cantotal=($cant_litros*$costo_compra)-$descuento;
            }else{
                $cantotal=($Cantidad*$costo_compra)-$descuento;
            }
            if($fila['tipo']==0){
                /*if($edit==1){
                    log_message('error','actualiza datos de prod edit: '.$edit);
                    if($id == $fila['id']){
                        log_message('error','mismo id: '.$fila["id"]);
                        $cod=$idProducto;
                        $name=$name_edit;
                    }
                }else{
                    $cod=$fila['idProducto'];
                    $name=$fila['nombre'];
                    $id=$fila["id"]; 
                }*/
                $cod=$fila['idProducto'];
                $name=$fila['nombre'];
                $id=$fila["id"];
            }else{
                $cod=$fila['nombre'];
                $id=$fila["id"];
                $name="Recarga de oxígeno para tanque ".$fila['nombre']." de ".$fila['capac']."L";
            }
            //log_message('error','name: '.$name);
            if($eoce==1){
                $txt_edit="";
            }else{
                $txt_edit='<a onclick="editaProd('.$fila["id"].','.$fila["tipo"].')" class="btn"><i class="fa fa-edit"></i></a>';
            }
            if($Cantidad==0){
                $cant_invalido=' cant_invalido ';
            }else{
                $cant_invalido='';
            }
            //$costo_compra2=$_SESSION['cost'][$count];
            if($costo_compra==0){
                $costo_invalido=' cant_invalido ';
            }else{
                $costo_invalido='';
            }

            $html.='<tr class="producto_'.$count.'">
                    <td>
                        <input type="hidden" name="count" id="count" value="'.$count.'">
                        <input type="hidden" class="vsproid_'.$count.'" name="vsproid" id="vsproid" value="'.$id.'">
                        <input type="hidden" class="vstipo_'.$count.'" name="vstipo" id="vstipo" value="'.$fila['tipo'].'">
                        <input type="hidden" name="incluye_iva" id="incluye_iva" value="'.$incluye_iva.'" readonly>
                        <input type="hidden" class="vscapac_'.$count.'" name="vscapac" id="vscapac" value="'.$fila['capac'].'">
                        <input type="hidden" name="porc_isr" id="porc_isr" value="'.$porc_isr.'" readonly>
                        '.$cod.'
                    </td>
                    <td>'.$name.'</td>
                    <td><input type="number" name="vscanti" id="vscanti" class="form-control vscanti vscanti_'.$count.$cant_invalido.'" value="'.$Cantidad.'" onchange="editarcantidad('.$count.')"></td>
                    <td><input type="number" name="vscosto" id="vscosto" class="form-control vscosto vscosto_'.$count.$costo_invalido.'" value="'.$costo_compra.'" onchange="editarcosto('.$count.')"></td>
                    <!--<td><input type="text" name="vsdescuento" id="vsdescuento" class="vsdescuento" value="'.$descuento.'" readonly style="display:none">'.$descuento_l.'
                        <a type="button" style="cursor: no-drop;" class="btn btn-primary btn-sm descuentos-btn" onclick="validar_descuento('.$count.',0)">%</a>
                    </td>-->
                    <td><div class="btn-group" style="display: flex; align-items: center;">$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'.$cantotal.'" readonly style="background: transparent;border: 0px;    width: 100px;"></div></td>
                    <td><a onclick="deletepro('.$count.')" class="btn"><i class="fa fa-trash"></i></a>
                        '.$txt_edit.'    
                    </td>
                </tr>';   
            $count++;
            //<a type="button" class="btn btn-primary btn-sm descuentos-btn" onclick="adddescuento('.$count.',1)">$</a>
        }

        echo $html;
    }

    function deleteproducto(){
        $idd = $this->input->post('id');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);

        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']);
        
        unset($_SESSION['des'][$idd]);
        $_SESSION['des'] = array_values($_SESSION['des']);
        
        unset($_SESSION['desp'][$idd]);
        $_SESSION['desp'] = array_values($_SESSION['desp']);

        unset($_SESSION['cost'][$idd]);
        $_SESSION['cost'] = array_values($_SESSION['cost']);

        unset($_SESSION['tipo'][$idd]);
        $_SESSION['tipo'] = array_values($_SESSION['tipo']);
        unset($_SESSION['capac'][$idd]);
        $_SESSION['capac'] = array_values($_SESSION['capac']); 

        unset($_SESSION['porc_isr'][$idd]);
        $_SESSION['porc_isr'] = array_values($_SESSION['porc_isr']); 

        unset($_SESSION['incluye_iva'][$idd]);
        $_SESSION['incluye_iva'] = array_values($_SESSION['incluye_iva']);
        unset($_SESSION['precio_con_ivax'][$idd]);
        $_SESSION['precio_con_ivax'] = array_values($_SESSION['precio_con_ivax']); 
    }

    function editarcantidad(){
        $params = $this->input->post();
        $row=$params['row'];
        $cantidad=$params['cantidad'];
        if($cantidad>0){}else{ $cantidad=0;}
        $_SESSION['can'][$row]=$cantidad;
    }

    function editarcosto(){
        $params = $this->input->post();
        $row=$params['row'];
        $costo=$params['costo'];
        $id=$params['id'];
        $tipo=$params['tipo'];
        if($costo>0){}else{
            $costo=0;
        }
        $_SESSION['cost'][$row]=$costo;
        //tambien editar el costo de compra en productos
        //log_message('error','id: '.$id);
        //log_message('error','tipo: '.$tipo);
        if($tipo==0){ //producto
            //$this->ModeloCatalogos->updateCatalogo('productos',array("costo_compra"=>$costo),array('id'=>$id));// ya no se ara directo cuando se edite
        }else{
            //$this->ModeloCatalogos->updateCatalogo('recargas',array("precioc"=>$costo),array('tipo'=>0));// ya no se ara directo cuando se edite
        }
    }

    function limpiartabla(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();

        unset($_SESSION['cost']);
        $_SESSION['cost']=array();

        unset($_SESSION['tipo']);
        $_SESSION['tipo']=array();
        unset($_SESSION['capac']);
        $_SESSION['capac']=array();

        unset($_SESSION['porc_isr']);
        $_SESSION['porc_isr']=array();

        unset($_SESSION['incluye_iva']);
        $_SESSION['incluye_iva']=array();
        unset($_SESSION['precio_con_ivax']);
        $_SESSION['precio_con_ivax']=array();
    }

    function savecompra(){
        $params=$this->input->post();
        $productos = $params['productos'];

        $viewcompra=$params['viewcompra'];
        $precompra=$params['precompra'];
        $total=$params['total'];
        unset($params['productos']);
        
        $DATAp = json_decode($productos);

        $dataeqarrayinsert_p=array();
        $dataeqarrayinsert_m=array();
        $folio_con = $this->ModeloCompras->obtener_folio_v($this->sucursal);
        $folio=$this->succlave.str_pad($this->idpersonal, 3, "0", STR_PAD_LEFT).str_pad($folio_con, 5, "0", STR_PAD_LEFT);
        if($viewcompra==0){
            $insertventa=array(
                'sucursal'=>$this->sucursal,
                'folio'=>$folio,
                'folio_con'=>$folio_con,
                'idproveedor'=>$params['idproveedor'],
                'subtotal'=>$params['subtotal'],
                'descuento'=>$params['descuento'],
                'iva'=>$params['iva'],
                'isr'=>$params['isr'],
                'total'=>$params['total'],
                'reg'=>$this->fechahoy,
                'precompra'=>$precompra,
                'personalid'=>$this->idpersonal,
                'fecha'=>$params['fecha'],
                'observaciones'=>$params['observaciones'],
            );
            $idcompra=$this->ModeloCatalogos->Insert('compra_erp',$insertventa);
            //$cod_oc=$idcompra.date("YmdGis", strtotime($this->fechahoy));
            //$codebar=$this->set_barcode($cod_oc);
            //$this->ModeloCatalogos->updateCatalogo('compra_erp',array("codigo"=>$codebar),array('id'=>$idcompra));
        }else{
            $this->ModeloCatalogos->getdeletewheren('compra_erp_detalle',array('idcompra'=>$viewcompra));
            $update_venta=array(
                'idproveedor'=>$params['idproveedor'],
                'subtotal'=>$params['subtotal'],
                'descuento'=>$params['descuento'],
                'iva'=>$params['iva'],
                'isr'=>$params['isr'],
                'total'=>$params['total'],
                'precompra'=>$precompra,
                'fecha'=>$params['fecha'],
                'observaciones'=>$params['observaciones'],
            );
            $this->ModeloCatalogos->updateCatalogo('compra_erp',$update_venta,array('id'=>$viewcompra));
            $idcompra=$viewcompra;
            //$cod_oc=$idcompra.date("YmdGis", strtotime($this->fechahoy));
            //$codebar=$this->set_barcode($cod_oc);
            //$this->ModeloCatalogos->updateCatalogo('compra_erp',array("codigo"=>$codebar),array('id'=>$idcompra));
        }

        for ($i=0;$i<count($DATAp);$i++) {
            $cant_ini=0;
            if($DATAp[$i]->tipo==0){ //producto
                $getpro=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$DATAp[$i]->pro));
                $getp=$getpro->row();
                if($getp->tipo!="1" && $getp->tipo!="2"){ //de stock, insumo y refaccion
                    $getpro_suc=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATAp[$i]->pro,"sucursalid"=>8,"activo"=>1));
                    $getpsuc=$getpro_suc->row();
                    $cant_ini=$getpsuc->stock;
                }if($getp->tipo=="1" || $getp->tipo=="2"){ //de serie o lote
                    $cant_ini=0; //si quieran validar stock de lote
                }
            }else{ //oxigeno
                $getoxi=$this->ModeloCatalogos->getselectwheren('recargas',array('tipo'=>"0","sucursal"=>8,"estatus"=>1)); //0 es tipo padre osea de 9500l
                $geto=$getoxi->row();
                $cant_ini=$geto->stock;
            }

            $insert_venta_dll=array(
                'idcompra'=>$idcompra,
                'idproducto'=>$DATAp[$i]->pro,
                'cantidad'=>$DATAp[$i]->cant,
                'precio_unitario'=>$DATAp[$i]->cunit,
                //'descuento'=>$DATAp[$i]->desc,
                'descuento'=>0,
                'tipo'=>$DATAp[$i]->tipo,
                "cant_ini"=>$cant_ini,
                "cant_inicial"=>$cant_ini
            );
            $dataeqarrayinsert_p[]=$insert_venta_dll;
            if($DATAp[$i]->tipo==0){ //producto
                $this->ModeloCatalogos->updateCatalogo('productos',array("costo_compra"=>$DATAp[$i]->cunit),array('id'=>$DATAp[$i]->pro));
            }else{
                $this->ModeloCatalogos->updateCatalogo('recargas',array("precioc"=>$DATAp[$i]->cunit),array('estatus'=>1));
            }
            if($precompra==0){
                //=============================aqui debera de ir el descuento de inventario de acuerdo a la sucursal cuando ya se tenga ese modulo
                //=============================
            }
        }
        if(count($dataeqarrayinsert_p)>0){
            $this->ModeloCatalogos->insert_batch('compra_erp_detalle',$dataeqarrayinsert_p);
        }
         
        $this->limpiartabla();
        $array = array(
                'idcompra' =>$idcompra,
            );
        echo $idcompra;//json_encode($array);
    }
    
    function view_presv(){
        $params = $this->input->post();
        $idcompra=$params['idcompra'];
        $result_v= $this->ModeloCompras->infocomprapre($idcompra);
        $result_vdll= $this->ModeloCatalogos->getselectwheren('compra_erp_detalle',array('idcompra'=>$idcompra));
        $array = array(
            'compra'=>$result_v->row(),
            'compradll'=>$result_vdll->result()
        );
        echo json_encode($array);
    }
     
    function obtenerdatosproducto(){
        $params = $this->input->post();
        $bar=$params['barcode'];
        $result = $this->ModelProductos->obtenerdatosproducto($bar);
        $id=0;
        $costo_compra=0;
        foreach ($result->result() as $item) {
            $id=$item->id;
            $costo_compra=$item->costo_compra;
        }
        $info = array('id'=>$id,'costo_compra'=>$costo_compra);
        echo json_encode($info);
    }

    public function imprimir_compra($id){
        $data['idcompra']=$id;
        $data['result_productos']=$this->ModeloCompras->get_productos($id);
        $data['result_compra']=$this->ModeloCatalogos->getselectwheren('compra_erp',array('id'=>$id));
        $result_compra=$this->ModeloCompras->get_compra($id);
        $data['folio']='';
        $data['reg']='';
        $data['razon_social']='';
        $data['codigo']="";
        foreach ($result_compra as $y) {
            $data['reg']=date('d/m/Y',strtotime($y->reg));
            $data['reg2']=$y->reg;
            if($y->fecha_ingreso!="" && $y->fecha_ingreso!=null && $y->fecha_ingreso!="null"){
                $data['fecha_entrega']=date('d/m/Y',strtotime($y->fecha_ingreso));
                $data['hora_entrega']=date('H:s a',strtotime($y->fecha_ingreso));
            }else{
                $data['fecha_entrega']="";
                $data['hora_entrega']="";
            }
            
            $data['proveedor']=$y->nombre;
            $data['codigo']=$y->codigo;
            $data['proveedor_direccion']=$y->nombre;
            $data['observaciones']=$y->observaciones;
            $data['direccion']=$y->direccion;
            $data['cp']=$y->cp;
            $data['rfc']=$y->rfc;
        }
        $data['direccion_entrega']='';
        $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>8));
        foreach ($sucrow->result() as $s){
            $data['direccion_entrega']=$s->domicilio;
        }
        $this->load->view('reportes/compra',$data);
    }

    public function get_productos_existencia(){
        $id = $this->input->post("id");
        $all = $this->input->post("all");
        $html1=""; $html2="";
        $datas = $this->ModeloCompras->getProds_exist_baja($id,0,$all);
        foreach($datas as $d2){
            ${'td_'.intval($d2->id)}="";
            ${'td_medio'.intval($d2->id)}="";
            ${'td_fin'.intval($d2->id)}="";
        }

        //$sucs=$this->ModeloCatalogos->getselectwherenallOrder('sucursal',array('activo'=>1),"orden","ASC");
        //foreach ($sucs as $s){
            $datas = $this->ModeloCompras->getProds_exist_baja($id,0,$all);
            foreach($datas as $d){
                $precio_con_ivax=0;
                $preciov_con_ivax=0;
                $costo=$d->costo_compra;
                $incluye_iva = $d->incluye_iva_comp;
                $iva = $d->iva_comp;
                $precio_con_ivax=0;
                if($incluye_iva==1){
                    $precio_con_ivax=round($costo,2);
                    if($iva==0){
                        $incluye_iva=0;
                    }else{
                        $precio_con_ivax=round($costo*1.16,2);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $precio_con_ivax=round($costo/$siva,2);
                    }else{
                        $precio_con_ivax=round($d->costo_compra,2);  
                    }
                }
                $precio_con_iva = $d->precio;
                $incluye_iva = $d->incluye_iva;
                $iva = $d->iva;
                $preciov_con_ivax=0;
                if($d->incluye_iva==1){
                    $preciov_con_ivax=round($d->precio,2);
                    if($iva==0){
                        $incluye_iva=0;
                        $preciov_con_ivax=round($d->precio,2);
                        //log_message('error','precio_con_ivax: '.$precio_con_ivax);
                    }else{
                        $preciov_con_ivax=round($d->precio*1.16,2);
                        //$precio_con_ivax=round($item->precio/1.16,2);
                        $sub_iva=round($d->precio*.16,2);
                        //log_message('error','sub_iva: '.$sub_iva);
                    }
                }else{
                    if($iva>0){
                        $siva=1.16;
                        $preciov_con_ivax=round($d->precio/$siva,2);
                    }else{
                        $preciov_con_ivax=round($d->precio,2);  
                    }
                }
                ${'idprod_'.intval($d->id).'_'.$d->sucursalid}=$d->id;
                ${'td_'.intval($d->id)}.="<tr class='tr_prods GridviewScrollItem'>
                    <td><input type='hidden' id='idprod' value='".$d->id."'><input type='hidden' id='tipo' value='".$d->tipo."'>".$d->idProducto."</td>
                    <td>".$d->nombre."</td>
                    <td>".$precio_con_ivax."</td>
                    <td>".$preciov_con_ivax."</td>";
                $stock=0;
                if($d->tipo=="0"){
                    if($d->idpssub==0)
                        $stock=$d->stok_psall;
                    else
                        $stock=$d->stok_psall;
                }if($d->tipo=="1"){
                    $stock=$d->stock_serieall;
                }if($d->tipo=="2"){
                    $stock=$d->stock_loteall;
                }
                ${'td_fin'.intval($d->id)}.="<td class='dname'><div class='col-lg-1'>
                            <input type='hidden' id='tot_stock' value='".$stock."'><input type='hidden' id='tot_min' value='".$d->tot_stockmin."'>
                            <span class='switch switch-icon'>
                                <label>
                                    <input type='checkbox' id='add_prod' class='swi_prods check_".$d->id."' value='".$d->id."'>
                                    <span style='border: 2px solid #009bdb;'></span>
                                </label>
                            </span>
                        </div></td>
                    </tr>";
            }
            $sucs=$this->ModeloCatalogos->getselectwherenallOrder('sucursal',array('activo'=>1),"orden","ASC");
            foreach ($sucs as $s){
                $tot_stock=0; $tot_min=0;
                $datas = $this->ModeloCompras->getProds_exist_baja($id,$s->id,$all);
                foreach($datas as $d2){
                    $stock=0;
                    if($d2->tipo=="0"){
                        if($d2->idpssub==0)
                            $stock=$d2->stok_ps;
                        else
                            $stock=$d2->stok_ps;
                    }if($d2->tipo=="1"){
                        $stock=$d2->stock_serie;
                    }if($d2->tipo=="2"){
                        $stock=$d2->stock_lote;
                    }
                    if(isset(${'td_medio'.intval($d2->id)})){
                        ${'td_medio'.intval($d2->id)}.="<td class='dname'><input type='hidden' id='stock' value='".$stock."'>".$stock."</td>";
                    }
                }
            }     

        //}
        $datas = $this->ModeloCompras->getProds_exist_baja($id,0,$all);
        foreach($datas as $d2){
            echo ${'td_'.intval($d2->id)}.${'td_medio'.intval($d2->id)}.${'td_fin'.intval($d2->id)};
        }
        
    }



}