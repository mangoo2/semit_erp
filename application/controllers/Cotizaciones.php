<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelCotizaciones');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,27);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=6;
        $data['btn_active_sub']=27;
        $data['idpersonal']=$this->idpersonal;
        $data['sucursal']=$this->sucursal;
        //$data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['sucrow']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cotizacion/index');
        $this->load->view('templates/footer');
        $this->load->view('cotizacion/indexjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCotizaciones->get_list($params,$this->perfilid,$this->idpersonal);
        $totaldata= $this->ModelCotizaciones->total_list($params,$this->perfilid,$this->idpersonal); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    function get_productos(){
        $id = $this->input->post('id');
        $nombre="";
        $html='<table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>';
        $result= $this->ModelCotizaciones->get_productos($id);
        foreach ($result->result() as $item){
            if($item->tipo_prodcot==0){
                $nombre=$item->nombre;
            }if($item->tipo_prodcot==1){
                $nombre=$item->recarga." / Recarga de oxígeno ".$item->capacidad." L";
            }if($item->tipo_prodcot==3){
                $nombre=$item->servicio;
            }
            $html.='<tr>
                <td>'.$nombre.'</td>
                <td>'.$item->cantidad.'</td>
            </tr>';
        }
        $html.='</tbody>';
        $html.='</table>';
        echo $html;
    }

    function enviar_aviso(){
        $idreg=$this->input->post('id');
        $correo=$this->input->post('correo');

        $asunto='Cotización';
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            $this->load->library('email', NULL, 'ci_email');
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
            $config["smtp_host"] ='mail.semit.mx'; 
            $config["smtp_user"] = 'cotizaciones@semit.mx';
            $config["smtp_pass"] = 'k@+K-cXb$z}0';
            $config["smtp_port"] = '465';
            $config["smtp_crypto"] = 'ssl';
            $config['charset'] = 'utf-8'; 
            $config['wordwrap'] = TRUE; 
            $config['validate'] = true;
            $config['mailtype'] = 'html';
             
            //Establecemos esta configuración
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('cotizaciones@semit.mx','Semit');
        //======================
        $this->email->to($correo,'Cotización');
        $this->email->subject($asunto);
        
        $message ="<div>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            <tr>
                              <th rowspan='14' style='background-color: #009bdb; color: #009bdb; -webkit-print-color-adjust: exact;'>..</th>
                              <th rowspan='14' style='background-color: #93ba1f; color: #93ba1f; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                                <th colspan='4' align='center'>
                                  <img src='".base_url()."public/img/SEMIT.jpg' style='width: 250px;'>
                                </th>
                            </tr>
                            <tr>
                              <th colspan='2' align='center'>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: black; -webkit-print-color-adjust: exact;' align='left'>
                                Cotización: ".$idreg."
                              </th>
                            </tr>
                          </thead>
                      </table>    
                      </div>
                  </div>";
        
        $this->email->message($message);

        $urlfac=FCPATH.'doc_cotizaciones/Cotizacion_'.$idreg.'.pdf';
        $this->email->attach($urlfac);
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
    }
     
    public function imprimir_cotizacion($id){
        $data['idcotizacion']=$id;
        $data['result_clausulas']=$this->ModelCotizaciones->get_productos_proveedor($id);
        $data['result_productos']=$this->ModelCotizaciones->get_productos($id);
        $data['result_cotizacion']=$this->ModeloCatalogos->getselectwheren('cotizacion',array('id'=>$id));
        $result_cotizacion=$this->ModelCotizaciones->get_cotizacion($id);
        $data['folio']='';
        $data['reg']='';
        $data['razon_social']='';
        $data['celular']='';
        $data['correo']='';
        foreach ($result_cotizacion as $y) {
            $data['folio']=$y->folio;
            $data['reg']=date('d/m/Y',strtotime($y->reg));
            $data['razon_social']=$y->razon_social;
            $data['celular']=$y->celular;
            $data['correo']=$y->correo;
        }

        $this->load->view('reportes/cotizacion',$data);
    }
    

}