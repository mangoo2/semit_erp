<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categorias extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,52);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=52;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('categorias/index');
        $this->load->view('templates/footer');
        $this->load->view('categorias/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=52;
        if($id>0){
            $resul=$this->General_model->get_select('categoria',array('categoriaId'=>$id));
            $data["cat"]=$resul->row();
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('categorias/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('categorias/formjs');
    }

    public function getlistado(){
        $getdatos = $this->General_model->getselectwhere('categoria','activo',1);
        $json_data = array("data" => $getdatos);
        echo json_encode($json_data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['categoriaId'];

        if($id==0){
            $id=$this->General_model->add_record('categoria',$data);
        }else{
            $this->General_model->edit_record('categoriaId',$id,$data,'categoria');
            $id=$id;
        }
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('categoriaId',$id,$data,'categoria');
    }

}