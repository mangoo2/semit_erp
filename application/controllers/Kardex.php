<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kardex extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloKardex');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelonumeroletra');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->sucursal=$this->session->userdata('sucursal');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,39);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=39;        
        $data['perfilid']=$this->perfilid;

        if($this->perfilid==1){
            //$data['suc']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
            $data['suc']=$this->ModeloCatalogos->getselectwherenallOrdern('sucursal',array('activo'=>1),'id_alias','ASC');
        }else{
            $data['suc']=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$this->sucursal));
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('kardex/index');
        $this->load->view('templates/footer');
        $this->load->view('kardex/indexjs');
        $this->load->view('ventasp/modalp');
    }
    
    public function search_allproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloKardex->search_all_productos($pro);
        echo json_encode($results->result());
    }    

    public function getDataKardex() {
        $params = $this->input->post();
        if($params["recarga"]==0){
            $result = $this->ModeloKardex->get_productos_kardex($params);
        }else{
            $result = $this->ModeloKardex->get_oxigeno_kardex($params);
        }
        $json_data = array("data" => $result);
        echo json_encode($json_data);
    }

    function ticket($folio){
        $configticket= $this->ModeloCatalogos->getselectwheren('configticket',array('id'=>1));
        $data['configticket']=$configticket->row();

        $configfac= $this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $data['configfac']=$configfac->row();

        $r_v= $this->ModeloCatalogos->getselectwheren('venta_erp',array('folio'=>$folio));
        $r_v=$r_v->row();
        $data['r_v']=$r_v;

        $r_suc= $this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$r_v->sucursal));
        $r_suc=$r_suc->row();
        $data['r_suc']=$r_suc;

        $r_per= $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$r_v->personalid));
        $r_per=$r_per->row();
        $data['r_per']=$r_per;

        //log_message('error','razon_social: '.$r_v->id_razonsocial);

        $r_cli_f= $this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$r_v->id_razonsocial));
        $r_cli_f=$r_cli_f->row();
        if($r_cli_f->razon_social==""){
            $r_cli_f= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$r_v->id_cliente)); 
            $r_cli_f=$r_cli_f->row();
            $data['r_cli_f_cliente']=$r_cli_f->nombre;
            $data["idcliente"]=$r_cli_f->id;
        }else{
            $data['r_cli_f_cliente']=$r_cli_f->razon_social;
            $data["idcliente"]=$r_cli_f->idcliente;
        }
        
        //log_message('error','razon_social: '.$data['r_cli_f']->razon_social);

        //$data['reg'] = $r_v->reg;
        $data['fecha'] = date("d/m/Y", strtotime($r_v->reg));
        //$NuevaFecha = strtotime ( '-1 hour' , strtotime ($r_v->reg)) ; 
        $data['hora'] = date("h:i a", strtotime($r_v->reg));

        $data['r_vd'] = $this->ModeloKardex->ventas_detalles($r_v->id);
        $data['s_vd'] = $this->ModeloKardex->ventas_detalles_servicio($r_v->id);
        $data['renta_vd'] = $this->ModeloKardex->ventas_detalles_renta($r_v->id);
        $data['total_letra'] =$this->Modelonumeroletra->ValorEnLetras($r_v->total);
        $data['r_vfp'] = $this->ModeloKardex->ventas_formaspagos($r_v->id);
        $this->load->view('reportes/ticket',$data);
    }

    function exportExcel($tipo,$id,$fi,$ff,$suc){
        $data["tipo"]=$tipo;
        $params["id_producto"]=$id;
        $params["fechai"]=$fi;
        $params["fechaf"]=$ff;
        $params["id_suc"]=$suc;
        $data["perfil"]=$this->perfilid;
        $data["sucursal"]=$this->sucursal;

        if($tipo==0){
            $data["result"]=$this->ModeloKardex->get_productos_kardex($params);
        }else{
            $data["result"]=$this->ModeloKardex->get_oxigeno_kardex($params);
        }
        $this->load->view('kardex/excelKardex',$data);
    }

}
