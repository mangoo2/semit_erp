<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller {
	  function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        //$this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturass');
        $this->load->model('Modelofacturas');
        
        $this->load->model('ModeloComplementos');
        $this->idpersonal=$this->session->userdata('id');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            //$this->idpersonal=$this->session->userdata('idpersonal');
            //$this->perfilid=$this->session->userdata('perfilid');
            //$permiso=$this->Login_model->getviewpermiso($this->perfilid,21);// perfil y id del submenu
            //if ($permiso==0) {
            //    redirect('Login');
            //}
        }else{
            redirect('Inicio');
        }
    }

	public function index(){
        $permiso=$this->Login_model->getviewpermiso($this->perfilid,21);
        if ($permiso==0) {
            redirect('Sistema');
        }
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        if ($this->perfilid==1) {
            $data['idpersonal']=0;
            $data['vendedor']='block';
        }else{
            $data['vendedor']='none';
            //$data['idpersonal']=$this->idpersonal;
            $data['idpersonal']=0;
        }
        $data['perfilid']=$this->perfilid;
        $data['viewidpersonal']=$this->idpersonal;
        $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1,'usuario'=>1));
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 1 year")); 
        $anioinicialreg =date("Y",strtotime($this->fechaactual."- 1 year"));
        if($anioinicialreg==2024){
            $data['fechainicial_reg']='2025-01-01';
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/facturas');
        $this->load->view('templates/footer');
        $this->load->view('folio/facturasjs');
    }
    function add($facturaId=0){
        $permiso=$this->Login_model->getviewpermiso($this->perfilid,21);
        if ($permiso==0) {
            redirect('Sistema');
        }
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        $data['facturaId']=$facturaId;
        $data['perfilid']=$this->perfilid;
        $data['metodo']=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('activo'=>1));
        $data['forma']=$this->ModeloCatalogos->getselectwheren('f_formapago',array('activo'=>1));
        $data['cfdi']=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('activo'=>1));
        $data['fservicios']=$this->ModeloCatalogos->getselectwheren('f_servicios',array('activo'=>1));

        $data['res_suc']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['succlave']=$this->succlave;

        $data['suc']=$this->sucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/facturasadd');
        $this->load->view('templates/footer');
        $this->load->view('folio/facturasaddjs');
    }
    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturass->getfacturas($params);
        $totaldata= $this->Modelofacturass->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistfacturas_pp() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturass->getfacturas_pp($params);
        $totaldata= $this->Modelofacturass->total_facturas_pp($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistfacturas_com() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturass->getcomplementos($params);
        $totaldata= $this->Modelofacturass->total_complementos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function obtenerrfc(){
        $params = $this->input->post();
        $idcliente = $params['id'];
        $rfc = $params['rfc'];
        $result=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('idcliente'=>$idcliente,'activo'=>1));
        $html='';
        foreach ($result->result() as $item) {
            if($rfc==$item->rfc){
                $rfcselected=' selected';
            }else{
                $rfcselected='';
            }
            $html.='<option value="'.$item->id.'" '.$rfcselected.'>'.$item->rfc.'</option>';
        }
        echo $html;
    }
    function viewdatosfiscalesinfo(){
        $params = $this->input->post();
        $cliente = $params['cliente'];
        $rfc = $params['rfc'];
        
        $razon_social='';
        $cp='';
        $RegimenFiscalReceptor=0;
        $direccion=$this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('id'=>$cliente));
        foreach ($direccion->result() as $item) {
            $razon_social = $item->razon_social;
            $rfc = $item->rfc;
            $cp = $item->cp;
            
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
        }
        $razon_social=str_replace(array(' '), array('-'), $razon_social);
        $razon_social='<span title="los guiones representan los espacios">'.$razon_social.'</span>';
        $datos=array(
                    'nombrefiscal'=>$razon_social,
                    'cp'=>$cp,
                    'regimennumber'=>$RegimenFiscalReceptor,
                    'regimen'=>$this->Modelofacturas->regimenf($RegimenFiscalReceptor),
                    'regimenclave'=>$RegimenFiscalReceptor
                    );
        echo json_encode($datos);
    }
    function searchunidadsat(){
      $search = $this->input->get('search');
      $results=$this->Modelofacturas->getseleclikeunidadsat($search);
        echo json_encode($results->result());
    }
    function searchconceptosat(){
      $search = $this->input->get('search');
      $results=$this->Modelofacturas->getseleclikeconceptosa($search);
        echo json_encode($results->result());
    }
    function obtenerpg(){
        $params=$this->input->post();
        $faci = $params['faci'];
        $facf = $params['facf'];
        $suc  = $params['suc'];
        $html='<div class="col s9"></div><div class="col s3"><button class="btn btn-primary" onclick="import_pg()" >Importar Todos</button></div>';
        $html.='<table class="responsive-table display no-footer table"><thead><tr><th>#</th><th>Cliente</th><th>Formas pagos</th><th></th><th>Fecha</th><th></th></tr></thead><tbody>';
        //===========================================================================================
            
            if($faci!=''){
                $fechaiwhere=" and v.reg>='".$faci." 00:00:00' ";
            }else{
                $fechaiwhere="";
            }
            if($facf!=''){
                $fechafwhere=" and v.reg<='".$facf." 23:59:59' ";
            }else{
                $fechafwhere="";
            }
               
                $select="SELECT v.id,v.folio,clif.razon_social,v.subtotal,v.descuento,v.iva,v.total,v.reg, vfp.formapago,max(vfp.monto) as monto,vfac.idfactura,GROUP_CONCAT(ffp.formapago_alias) as formaspagos
                            FROM venta_erp as v 
                            INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial
                            INNER JOIN venta_erp_formaspagos as vfp on vfp.idventa=v.id 
                            INNER JOIN f_formapago as ffp on ffp.clave=vfp.formapago
                            LEFT JOIN venta_fk_factura as vfac on vfac.idventa=v.id
                            WHERE v.activo=1 AND v.sucursal='$suc' and vfac.idfactura IS null  $fechaiwhere $fechafwhere GROUP by v.id";
                $query=$this->db->query($select);

            
            foreach ($query->result() as $item) {
                
                $html.='<tr  class="totalventa_'.$item->total.' importar_delete_'.$item->id.'">
                            <td>'.$item->folio.'</td>
                            <td>'.$item->razon_social.'</td>
                            <td>'.$item->formaspagos.'</td>
                            <td>'.$item->total.'</td>
                            <td>'.$item->reg.'</td>
                            <td class="tdtablepg">';
                            
                                $html.='<button class="btn btn-primary btn-sm import_'.$item->id.' import_pg import_pg_'.$item->id.'_0" onclick="importarproductospg('.$item->id.')" 
                                        data-id="'.$item->id.'" 
                                        data-subtotal="'.$item->subtotal.'"
                                        data-iva="'.$item->iva.'"
                                        data-formapago="'.$item->formapago.'"
                                        data-fpmonto="'.$item->monto.'"
                                        >Importar</button> ';
                                $html.=' <button class="btn btn-danger btn-sm" onclick="deleteproductospg('.$item->id.')"><i class="fas fa-trash-alt"></i></button>';
                        $html.='</td>
                        </tr>';
                
            }
        
            $html.='</tbody></table>';
            echo $html;
    }
    function obtenerventaspg(){

    }
    function obtenerdatosfactura(){
        $params = $this->input->post();
        $idfactura = $params['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfacturad=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$idfactura));
        $array = array(
            'facturas'=>$datosfactura->result(),
            'facturasd'=>$datosfacturad->result()
        );
        echo json_encode($array);
    }
    
    function enviar_factura(){
        $folio=$this->input->post('folio');
        $correo=$this->input->post('correo');

        $asunto='Factura de compra';
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            $this->load->library('email', NULL, 'ci_email');
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
            $config["smtp_host"] ='mail.semit.mx'; 
            $config["smtp_user"] = 'facturacion@semit.mx';
            $config["smtp_pass"] = ')2W_JxfKqklS';
            $config["smtp_port"] = '465';
            $config["smtp_crypto"] = 'ssl';
            $config['charset'] = 'utf-8'; 
            $config['wordwrap'] = TRUE; 
            $config['validate'] = true;
            $config['mailtype'] = 'html';
             
            //Establecemos esta configuración
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('facturacion@semit.mx','Semit');
        //======================
        $this->email->to($correo,'Te enviamos tu factura de compra');
        $this->email->subject($asunto);
        
        $message ="Se adjunta factura de compra";
        $this->email->message($message);

        $urlfac=FCPATH.'hulesyg/facturas/Factura_'.$Folio.'.pdf';
        $this->email->attach($urlfac);
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
    }

}