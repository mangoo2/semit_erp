<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Entradas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCompras'); 
        $this->load->model('ModelProductos');
        $this->load->model('ModelTraspasos'); 
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoy_normal = date('d/m/Y');
        $this->horahoy_normal = date('H:i:s');
        $this->fecha_larga = date('Y-m-d H:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechacod = date('ymdHis');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->usuarioid=$this->session->userdata('usuarioid');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,31);// perfil y id del submenu
            //$this->sucusal=1;//cuando ya esten las sucursales
            //$this->folio=$this->sucusal.'-'.$this->fechacod;
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=9;
        $data['btn_active_sub']=31;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('entradas/index');
        $this->load->view('templates/footer');
        $this->load->view('entradas/indexjs');
    }

    public function getlistentradas() {
        $params = $this->input->post();      
        $getdata = $this->ModeloCompras->getcompras($params);
        $totaldata= $this->ModeloCompras->total_getcompras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    /*function view_productos(){
        $params = $this->input->post();
        $idcompras=$params['idcompras'];
        $band=$params['band'];
        $dis=""; $oc=0;
        if($band==0){
            $dis="disabled";
            $oc=0;
        }
        $result= $this->ModeloCompras->compras_detalles($idcompras,0);
        $html=''; $conts=0; $contnom=0; $serie=""; $contg=0; $contlot=0;
        foreach ($result->result() as $item) {
            $importe=($item->cantidad*$item->precio_unitario);          
            $contg++;
            if($item->tipo==0){
                $nombre=$item->nombre;
            }else{
                $nombre=$item->codigo." - Recarga de oxígeno de ".$item->capacidad."L";
            }
            $cantidadaux=$item->cantidad;
            if($item->tipo==1){ //recarga
                $html.='<tr>
                    <td colspan="2">'.$nombre.'</td>
                    <td>'.$item->cantidad.'</td>
                    <td colspan="2">'.number_format($item->precio_unitario,2).'</td>
                </tr>';
            }
            if($item->tipo_s==0 && $item->tipo==0){ //normal
                $contnom++;
                //log_message('error','contnom: '.$contnom);
                if($contnom==1){
                    $html.='<tr>
                        <td colspan="2">'.$nombre.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td colspan="2">'.number_format($item->precio_unitario,2).'</td>
                    </tr>';
                }
            }
            if(isset($item->serie) && $item->serie=="" || !isset($item->serie)){
                $sserie="";
            }if(isset($item->serie) && $item->serie!=""){
                $sserie=$item->serie;
            }
            //if($item->tipo_s==1 && isset($sserie) && $sserie==""){
            if($item->tipo_s==1){ //es de serie
                $conts++;
                if($conts==1){
                    $html.='<tr>
                        <td colspan="2">'.$nombre.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td colspan="2">'.number_format($item->precio_unitario,2).'</td>
                    </tr>';
                }
            }
            if($item->tipo_s==1 && $band==0){
                $html.='<tr>
                    <th colspan="2">Producto</th>
                    <th>Serie</th>
                </tr>';
                for ($i = 1; $i <= $cantidadaux; $i++) {
                    $html.='<tr class="tr_s">
                        <td colspan="2">'.$nombre.'</td>
                        <td colspan="2"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input class="form-control" type="text" id="seriex" value="" onchange="validar_serie(this.value,$(this))" '.$dis.'></td>
                    </tr>';
                }
                //$serie=$item->serie;
            }if($band==1 && !isset($item->serie)){                
                $html.='<tr class="tr_s">
                    <td colspan="2">'.$nombre.'</td>
                    <td colspan="2"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input class="form-control" type="text" id="seriex" value="" onchange="validar_serie(this.value,$(this))" '.$dis.'></td>
                </tr>';
            }
            //else if($item->tipo_s==1 && isset($sserie) && $sserie!=""){
            if($item->tipo_s==2){
                $html.='<tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Lote</th>
                    <th>Caducidad</th>
                    <th></th>
                </tr>';
            }
            if($item->tipo_s==2){ //es de lote
                $contlot++;
                if($contlot==1){
                    $html.='<tr>
                        <td colspan="2">'.$nombre.'</td>
                        <td><input type="hidden" id="cantidad" value="'.$item->cantidad.'"> '.$item->cantidad.'</td>
                        <td colspan="2">'.number_format($item->precio_unitario,2).'</td>
                    </tr>';
                }
                //$contlot=0;
            }
            if($item->tipo_s==2 && $band==0 ){
                //for ($i = 1; $i <= $cantidadaux; $i++) {
                    $html.='<tr class="tr_lc tr_lc_'.$contg.'" id="tr_lc">
                        <td>'.$nombre.'</td>
                        <td><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input type="hidden" id="catidadcomplc" value="'.$item->cantidad.'">
                            <input '.$dis.' class="form-control cant cant_'.$item->idproducto.'" type="text" id="cantidadx" value="'.$item->cantidad.'">
                        </td>
                        <td><input '.$dis.' class="form-control lote_'.$item->idproducto.'" onchange="changeLote(this.value,'.$item->idproducto.',this.id,1)" type="text" id="lotex" value="">
                        </td>
                        <td><input '.$dis.' class="form-control caduc_'.$item->idproducto.'" onchange="changeLote(this.value,'.$item->idproducto.',this.id,1)" type="date" id="caducidadx" value=""></td>';
                    if($item->cantidad>1){
                        $html.='<td><button '.$dis.' onclick="addRow('.$item->idproducto.',$(this),'.$contg.')" class="btn btn-primary" type="button"> <i class="fas fa-plus-square" aria-hidden="true"></i></button></td>';
                    }else{
                        $html.='<td></td>';
                    }
                    $html.='</tr>';
                //}
            }
            else if($item->tipo_s==2 && $band==1 ){
                $lcant_ls=$item->cantidad;
                if(isset($item->lote) && $item->lote!=""){
                    $llote=$item->lote;
                    $lcaducidad=$item->caducidad;
                    $lcant_ls=$item->cant_ls;
                }if (!isset($item->lote) || isset($item->lote) && $item->lote==""){
                    $llote="";
                    $lcaducidad="";
                    $lcant_ls=$item->cantidad;
                }
                $html.='<tr class="tr_lc tr_lc_'.$contg.'">
                    <td>'.$nombre.'</td>
                    <td><input type="hidden" id="idproductox" value="'.$item->idproducto.'">
                        <input '.$dis.' class="form-control cant cant_'.$item->idproducto.'" type="text" id="cantidadx" value="'.$lcant_ls.'">
                    </td>
                    <td><input '.$dis.' class="form-control lote_'.$item->idproducto.'" type="text" id="lotex" value="'.$llote.'">
                    </td>
                    <td><input '.$dis.' class="form-control caduc_'.$item->idproducto.'" type="date" id="caducidadx" value="'.$lcaducidad.'"></td>
                    <td></td>
                </tr>';
            }
        }
        echo $html;
    }*/
    function view_productos(){
        $params = $this->input->post();
        $idcompras=$params['idcompras'];
        $band=$params['band'];
        $dis="";
        if($band==0){
            $dis="disabled";
        }
        $html_scr=""; $idprod_ant=0;
        $html_sty='<style type="text/css">';
        $result= $this->ModeloCompras->compras_detalles($idcompras,0,$band);
        $html=''; $cont_serie=0; $cont_lote=0; $cont_stock=0; $cont_tanque=0; $serie=""; $contg=0; $conts=0; $cont_serie_dato=0;
        foreach ($result->result() as $item) {
            $cont_serie=0;
            $importe=($item->cantidad*$item->precio_unitario);
            //$conts++;
            $contg++;
            $nombre=$item->nombre;
            $html_sty.=' .tr_det_accordion_'.$item->idproducto.' { display: none; } ';
            if($item->tipo_s==2 && $item->tipo==0 || $item->tipo_s==0 && $item->tipo==0 || $item->tipo_s==1 && $item->tipo==0 || $item->tipo_s==3 && $item->tipo==0 || $item->tipo_s==4 && $item->tipo==0){
                $nombre=$item->nombre;
            }else if($item->tipo_s==0 && $item->tipo==1){
                $nombre=$item->codigo." - Recarga de oxígeno de ".$item->capacidad."L";
            }
            $cantidadaux=$item->cantidad;
            $cant_sr=$item->cant_sr;
            //log_message('error','tipo_s: '.$item->tipo_s);
            if($item->tipo_s==0 && $item->tipo==1){ //de recarga
                $preciouni = $item->precio_unitario * 9500;
                $cont_tanque++;
                //if($cont_tanque==1){
                    $html.='<tr>
                        <td colspan="2">'.$nombre.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td colspan="1">$'.number_format($preciouni,6,".",",").'</td>
                        <td colspan="1">$'.number_format($preciouni*$item->cantidad,6,".",",").'</td>
                        <td><div style="display:none" id="div_scn_'.$item->idproducto.'" class="fa-2x"><i style="color:green;" class="fa fa-solid fa-spinner fa-spin-pulse"></i></div></td>
                    </tr>';
                //}
            }
            if($item->tipo_s==0 && $item->tipo==0 || $item->tipo_s==3 && $item->tipo==0 || $item->tipo_s==4 && $item->tipo==0){ //de stock, insumo, herramienta
                $cont_stock++;
                //if($cont_stock==1){
                    $html.='<tr class="tr_det_accordion'.$item->idproducto.'">
                        <td colspan="2">'.$nombre.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario,6,".",",").'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario*$item->cantidad,6,".",",").'</td>
                        <td>
                            <div style="display:none" id="div_scn_'.$item->idproducto.'" class="fa-2x"><i style="color:green;" class="fa fa-solid fa-spinner fa-spin-pulse"></i></div>
                        </td>
                    </tr>';
                //}
            }if($item->tipo_s==1){ //es de serie
                $cont_serie++;
                if($cont_serie==1 && $idprod_ant!=$item->idproducto){
                    $html.='<tr style="background-color:rgb(217,217,217);" onclick="showHideTd('.$item->idproducto.')" class="tr_accordion'.$item->idproducto.'">
                        <td colspan="2"><input type="hidden" id="id" value="'.$item->id_ps.'">
                            <span class="expand-button exp-btn_'.$item->idproducto.'"> - </span> '.$nombre.'
                        </td>
                        <td>'.$item->cantidad.'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario,6,".",",").'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario*$item->cantidad,6,".",",").'</td>
                        <td><div style="display:none" id="div_scn_'.$item->idproducto.'" class="fa-2x"><i style="color:green;" class="fa fa-solid fa-spinner fa-spin-pulse"></i></div></td>
                    </tr>';
                    $cont_serie=0;
                }
                if($cantidadaux>=1){
                    //if($idprod_ant!=$item->idproducto){
                        $html_scr.="<script>showHideTdBef(".$item->idproducto.")</script>";
                    //}
                }
            }
            if($item->tipo_s==2){ //es de lote
                $cont_lote++;
                //if($cont_lote==1){
                    $html.='<tr style="background-color:rgb(217,217,217);" onclick="showHideTd('.$item->idproducto.')" class="tr_accordion'.$item->idproducto.'">
                        <td colspan="2"><input type="hidden" id="id" value="'.$item->id_pl.'"><span class="expand-button exp-btn_'.$item->idproducto.'"> - </span> '.$nombre.'</td>
                        <td><input type="hidden" id="cantidad" value="'.$item->cantidad.'"> '.$item->cantidad.'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario,6,".",",").'</td>
                        <td colspan="1">$'.number_format($item->precio_unitario*$item->cantidad,6,".",",").'</td>
                        <td><div style="display:none" id="div_scn_'.$item->idproducto.'" class="fa-2x"><i style="color:green;" class="fa fa-solid fa-spinner fa-spin-pulse"></i></div></td>
                    </tr>';
                //}
                //$conts=0;
                if($cantidadaux>=1){
                    //if($idprod_ant!=$item->idproducto){
                        $html_scr.="<script>showHideTdBef(".$item->idproducto.")</script>";
                    //}
                }
            }
             if($item->tipo_s==1 && $cant_sr < $cantidadaux && $item->idproducto!=$idprod_ant){
                //log_message('error','cant_ls: '.$cant_sr);
                //log_message('error','cantidadaux: '.$cantidadaux);
                $resta = $cantidadaux - $cant_sr;
                //log_message('error','resta: '.$resta);
                for ($i = 1; $i <= $resta; $i++) {
                    /*$html.='<tr class="tr_s tr_det_accordion'.$item->idproducto.'">
                        <td colspan="2"><input type="hidden" id="id" value="0">'.$nombre.'</td>
                        <td colspan="2"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input class="form-control" type="hidden" id="seriex" value="" onchange="validar_serie(this.value,$(this))" '.$dis.'></td>
                    </tr>'; */
                }
            }
            if($item->tipo_s==1 && $item->serie==""){
                $html.='<tr class="tr_det_accordion'.$item->idproducto.'">
                    <td colspan="2">Producto</td>
                    <td>Serie</td>
                </tr>';
                for ($i = 1; $i <= $cantidadaux; $i++) {
                    $html.='<tr class="tr_s tr_det_accordion'.$item->idproducto.'">
                        <td colspan="2"><input type="hidden" id="id" value="'.$item->id_ps.'">'.$nombre.'</td>
                        <td colspan="2"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input class="form-control" type="text" id="seriex" value="" onchange="validar_serie(this.value,$(this))" '.$dis.'></td>
                    </tr>';
                }
                $serie=$item->serie;
            }else if($item->tipo_s==1 && $item->serie!=""){
                $cont_serie_dato++;
                $html.='<tr class="tr_s tr_det_accordion'.$item->idproducto.' tr_serie_det_'.$item->serie.'">
                    <td colspan="2"><input type="hidden" id="id" value="'.$item->id_ps.'">'.$nombre.'</td>
                    <td colspan="2">
                        <input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input class="form-control" type="text" id="seriex" value="'.$item->serie.'" '.$dis.'>
                    </td>
                </tr>';
                $conts=0;
                $serie=$item->serie;
            }
           
            if($item->tipo_s==2){
                $html.='<tr  class="tr_det_accordion'.$item->idproducto.'">
                    <td scope="col">Producto</td>
                    <td scope="col">Cantidad</td>
                    <td scope="col">Lote</td>
                    <td scope="col">Caducidad</td>
                    <td scope="col">Cod. Barras</td>
                    <td></td>
                </tr>';
            }
            if($item->tipo_s==2 && $item->lote==""){
                //for ($i = 1; $i <= $cantidadaux; $i++) {
                    $html.='<tr class="tr_lc tr_lc_'.$contg.' tr_det_accordion'.$item->idproducto.'" id="tr_lc">
                        <td width="30%"><input type="hidden" id="id" value="'.$item->id_pl.'">
                            <span style="display:none" id="div_scn_'.$item->idproducto.'" class="fa-2x"><i style="color:green;" class="fa fa-solid fa-spinner fa-spin-pulse"></i></span>
                            '.$nombre.'
                        </td>
                        <td width="10%"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input type="hidden" id="catidadcomplc" value="'.$item->cantidad.'">
                            <input '.$dis.' class="form-control cant cant_'.$item->idproducto.'" type="text" id="cantidadx" value="'.$item->cantidad.'">
                        </td>
                        <td width="15%"><input '.$dis.' class="form-control lote_'.$item->idproducto.'" onchange="changeLote(this.value,'.$item->idproducto.',this.id,1);createCDL(8,'.$item->idproducto.')" type="text" id="lotex" value="'.$item->lote.'">
                        </td>
                        <td width="15%"><input '.$dis.' class="form-control caduc_'.$item->idproducto.'" onchange="changeLote(this.value,'.$item->idproducto.',this.id,1);createCDL(8,'.$item->idproducto.')" type="date" id="caducidadx" value="'.$item->caducidad.'"></td>
                        <td width="20%"><input disabled class="form-control cod_barras_'.$item->idproducto.'" type="text" id="cod_barrasx"></td>';
                    if($item->cantidad>1){
                        $html.='<td width="10%"><button '.$dis.' onclick="addRow('.$item->idproducto.',$(this),'.$contg.')" class="btn btn-primary" type="button"> <i class="fas fa-plus-square" aria-hidden="true"></i></button></td>';
                    }else{
                        $html.='<td width="10%"></td>';
                    }
                    $html.='</tr>';
                //}
            }
            else if($item->tipo_s==2 && $item->lote!=""){
                $html.='<tr class="tr_lc tr_lc_'.$contg.' tr_det_accordion'.$item->idproducto.'">
                    <td width="30%"><input type="hidden" id="id" value="'.$item->id_pl.'">'.$nombre.'</td>
                    <td width="10%"><input type="hidden" id="idproductox" value="'.$item->idproducto.'"><input type="hidden" id="catidadcomplc" value="'.$item->cantidad.'">
                        <input '.$dis.' class="form-control cant cant_'.$item->idproducto.'" type="text" id="cantidadx" value="'.$item->cant_ls.'">
                    </td>
                    <td width="15%"><input '.$dis.' class="form-control lote_'.$item->idproducto.'" type="text" id="lotex" value="'.$item->lote.'">
                    </td>
                    <td width="15%"><input '.$dis.' class="form-control caduc_'.$item->idproducto.'" type="date" id="caducidadx" value="'.$item->caducidad.'"></td>
                    <td width="20%"><input '.$dis.' class="form-control cod_barras_'.$item->idproducto.'" type="text" id="cod_barrasx" value="'.$item->cod_barras.'"></td>
                    <td width="10%"></td>
                </tr>';
            }
            if($idprod_ant!=$item->idproducto){
                //$html_scr.="<script>showHideTdBef(".$item->idproducto.")</script>";
            }
            $idprod_ant=$item->idproducto;    
        }
        $html_sty.='</style>';
        
        //echo $html;
        echo json_encode(array("html"=>$html,"html_sty"=>$html_sty,"html_scr"=>$html_scr));
    }

    function searchBarcode(){
        $codigo = $this->input->post('cod');
        $results=$this->ModeloCompras->getProductoCodigoComp($codigo);
        echo json_encode($results);
    }

    public function crearCodigoLote(){
        $idsuc=$this->input->post('idsuc');
        $idprod=$this->input->post('idprod');
        $lote=$this->input->post('lote');
        $cad=$this->input->post('cad');
        
        $codigo=$idprod.$idsuc.intval(preg_replace('/[^0-9]+/', '', $lote), 10).date("Ymd",strtotime($cad));
        echo $codigo;        
    }

    public function imprimir_compra($id){
        $data['idcompra']=$id;
        $data['result_productos']=$this->ModeloCompras->get_productos($id);
        $data['result_compra']=$this->ModeloCatalogos->getselectwheren('compra_erp',array('id'=>$id));
        $result_compra=$this->ModeloCompras->get_compra($id);
        $data['folio']='';
        $data['reg']='';
        $data['razon_social']='';
        $data['codigo']="";
        $data['factura']="";
        $data['fechai']="";
        foreach ($result_compra as $y) {
            $data['reg']=date('d/m/Y',strtotime($y->reg));
            $data['reg2']=$y->reg;
            //$data['fecha_entrega']=date('d/m/Y',strtotime($y->fecha));
            if($y->fecha_ingreso!="" && $y->fecha_ingreso!=null && $y->fecha_ingreso!="null"){
                $data['fecha_entrega']=date('d/m/Y',strtotime($y->fecha_ingreso));
                $data['hora_entrega']=date('H:s a',strtotime($y->fecha_ingreso));
            }else{
                $data['fecha_entrega']="";
                $data['hora_entrega']="";
            }
            $data['proveedor']=$y->nombre;
            $data['codigo']=$y->codigo;
            $data['proveedor_direccion']=$y->nombre;
            $data['observaciones']=$y->observaciones;
            $data['direccion']=$y->direccion;
            $data['cp']=$y->cp;
            $data['rfc']=$y->rfc;
            $data['factura']=$y->factura;
            $data['fechai']=$y->fecha_ingreso;
        }
        $data['direccion_entrega']='';
        $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>8));
        foreach ($sucrow->result() as $s){
            $data['direccion_entrega']=$s->domicilio;
        }
        $this->load->view('reportes/compra',$data);
    }

    public function revertirEntrada(){
        $id=$this->input->post("id");
        $motivo=$this->input->post("motivo");
        $arrayinfo = array('factura'=>"",'fecha_ingreso'=>"",'idpersonal_ingreso'=>0,'estatus'=>0);
        $this->db->trans_start();

        $this->General_model->edit_record('id',$id,$arrayinfo,'compra_erp');
        $id_traslado=0;
        $result_productos=$this->ModeloCompras->get_productos($id);
        foreach ($result_productos->result() as $x){  //quita del almacen general (y despues de la dispersion)
            if($x->tipo==0 && $x->tipo_prod==0){ //producto
                $get_hist=$this->ModeloCatalogos->getselectwheren('historial_transpasos',array('id_compra'=>$id,"tipo"=>0,"idproducto_lote"=>0,"idproducto_series"=>0,"activo"=>1/*,"status"=>2*/));
                $cont_prods=0;
                foreach ($get_hist->result() as $h){ 
                    $cont_prods++;
                    if($h->status==0){ //sin dispersar
                        $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array("activo"=>0),array('id'=>$h->id));
                    }else{
                        $this->ModeloCompras->resta_sucursal($x->idproducto,$h->idsucursal_sale,$x->cantidad); //QUITA AL DESTINO
                        $this->ModeloCompras->sumar_sucursal($x->idproducto,8,$x->cantidad); //REGRESA A ALMACEN GRAL
                    }
                }
                if($cont_prods==0){
                    $this->ModeloCompras->resta_sucursal($x->idproducto,8,$x->cantidad); //QUITA A ALMACEN GRAL
                }
            }else if($x->tipo==1){ //recarga
                $get_hist=$this->ModeloCatalogos->getselectwheren('historial_transpasos',array('id_compra'=>$id,"tipo"=>1,"activo"=>1,"status"=>2)); //obtiene recargas
                foreach ($get_hist->result() as $h){ 
                    $id_traslado=$h->idtranspasos;
                    //$cant = $x->capacidad*$h->cantidad;
                    $cant = $h->cantidad;
                    //$this->ModelTraspasos->update_tanque_sucu($h->idproducto,$h->idsucursal_sale,$cant,"+"); //regresa a origen
                    $this->ModelTraspasos->update_tanque_sucu($h->idproducto,$h->idsucursal_entra,$cant,"-"); //resta a destino
                    //$this->ModeloCatalogos->updateCatalogo('historial_transpasos',array("activo"=>0),array('id'=>$h->id));
                }
            }else if($x->tipo==0 && $x->tipo_prod==1){ //series
                //$this->ModeloCompras->resta_sucursal($x->idproducto,8,$x->cantidad);
                $get_series=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('idcompras'=>$id,"activo"=>1));
                foreach ($get_series->result() as $s){ 
                    //$this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array("sucursalid"=>8),array('id'=>$s->id));
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',array("activo"=>0),array('id'=>$s->id));
                }
            }else if($x->tipo==0 && $x->tipo_prod==2){ //lotes
                //$this->ModeloCompras->resta_sucursal($x->idproducto,8,$x->cantidad);
                $get_lotes=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('idcompra'=>$id,"activo"=>1));
                foreach ($get_lotes->result() as $l){ 
                    //$this->ModeloCatalogos->updateCatalogo('productos_sucursales_lote',array("sucursalid"=>8),array('id'=>$l->id));
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales_lote',array("activo"=>0),array('id'=>$l->id));
                }
            }
        }
        $get_hist=$this->ModeloCatalogos->getselectwheren('historial_transpasos',array('id_compra'=>$id,"activo"=>1));
        foreach ($get_hist->result() as $h){ 
            $this->ModeloCatalogos->updateCatalogo('traspasos',array("activo"=>0),array('id'=>$h->idtranspasos));
        }

        $this->ModeloCatalogos->updateCatalogo('historial_transpasos',array("activo"=>0),array('id_compra'=>$id));
        $this->ModeloCatalogos->Insert('bitacora_reverse_entradas',array("id_ordencomp"=>$id,"id_usuario"=>$this->usuarioid,"fecha"=>$this->fecha_larga,"motivo"=>$motivo));

        $this->db->trans_complete();
    }

    function getBitacora(){
        $id = $this->input->post("id");
        
        $html='';
        $result= $this->ModeloCompras->getDataBitacora($id);
        foreach ($result as $b) {
            $html.="<tr>
                    <td>".$b->id."</td>
                    <td>".$id."</td>
                    <td>".$b->fecha."</td>
                    <td>".$b->Usuario."</td>
                    <td>".$b->motivo."</td>
                </tr>";          
        }
        echo $html;
    }

    function ingresar_inventario(){   //guarda en stock el producto ingresada de la compra 
        $data=$this->input->post();
        $idcompras=$data['idcompras'];
        $factura=$data['factura'];
        $series=$data['series'];
        unset($data["series"]);

        $lotes=$data['lotes'];
        unset($data["lotes"]);

        $arrayinfo = array('factura'=>$factura,'fecha_ingreso'=>$this->fechahoy,'idpersonal_ingreso'=>$this->idpersonal,'estatus'=>1);
        $id=$this->General_model->edit_record('id',$idcompras,$arrayinfo,'compra_erp');
        $result_productos=$this->ModeloCompras->get_productos($idcompras);
        foreach ($result_productos->result() as $x){ 
            $id_dll = $x->id_dll;
            $cant_inicial=0;$cant_final=0;
            if($x->tipo==0 && $x->tipo_prod==0 || $x->tipo==0 && $x->tipo_prod==3 || $x->tipo==0 && $x->tipo_prod==4){ //producto, insumo, herramienta
                $res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$x->idproducto,'sucursalid'=>8));
                foreach ($res_info1->result() as $itemi) {
                    $cant_inicial=$itemi->stock;
                }
                $cant_final=$cant_inicial+$x->cantidad;
                $this->ModeloCompras->sumar_sucursal($x->idproducto,8,$x->cantidad);
                $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('id'=>$id_dll));
            }else if($x->tipo==1){ //recargas
                $res_info1=$this->ModeloCatalogos->getselectwheren('recargas',array('sucursal'=>8,'tipo'=>0));
                foreach ($res_info1->result() as $itemi) {
                    $cant_inicial=$itemi->stock;
                }

                $cant = $x->capacidad*$x->cantidad;
                $cant_final=$cant_inicial+$cant;
                $this->ModelTraspasos->update_tanque_sucu($x->idproducto,8,$cant,"+");
                $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('id'=>$id_dll));
            }
            if($x->tipo==0 && $x->tipo_prod==1){ //series
                //$cant_inicial=$this->ModeloCatalogos->obtener_stock_series($x->idproducto,8);
                /*
                $strq="SELECT pss.* FROM productos_sucursales_serie as pss left join compra_erp c on c.id=pss.idcompras and c.estatus=1 WHERE pss.activo=1 and pss.productoid='$x->idproducto' and pss.sucursalid='8' and pss.disponible=1 AND ((pss.idcompras!=0 AND c.estatus=1) OR (pss.idcompras=0)) group by id";
                $res_info1 = $this->db->query($strq);
                if ($res_info1->num_rows()>0) {
                    $cant_inicial=$res_info1->num_rows();
                }
                */
                $cant_final=$cant_inicial+$x->cantidad;
                //$this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('id'=>$id_dll));

            }
            /*else if($x->tipo==0 && $x->tipo_prod==1){ //series
                $this->ModeloCompras->sumar_sucursal($x->idproducto,8,$x->cantidad);
            }else if($x->tipo==0 && $x->tipo_prod==2){ //lotes
                $this->ModeloCompras->sumar_sucursal($x->idproducto,8,$x->cantidad);
            }*/
        }
        //ingresa las series de los equipos
        $productoid=0;
        $cant_inicial=0;
        $cant=1;
        $DATAs = json_decode($series);
        for ($i=0;$i<count($DATAs);$i++) {
            if($DATAs[$i]->idproducto!=$productoid){
                $productoid=$DATAs[$i]->idproducto;
                $cant_inicial=$this->ModeloCatalogos->obtener_stock_series($productoid,8);
            }
            $cant_final=$cant_inicial+$cant;

            $data_array=array(
            'sucursalid'=>8,
            'productoid'=>$DATAs[$i]->idproducto,
            'serie'=>$DATAs[$i]->serie,
            'idcompras'=>$idcompras,
            'reg'=>$this->fecha_larga
            );
            if($DATAs[$i]->id==0){
                $this->ModeloCatalogos->Insert('productos_sucursales_serie',$data_array);
                $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final),array('idcompra'=>$idcompras,'idproducto'=>$DATAs[$i]->idproducto));
                $cant++;

                $this->ModeloCompras->sumar_sucursal($DATAs[$i]->idproducto,8,1);
            }else{
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_serie',$data_array,array('id'=>$DATAs[$i]->id));
            }
        }

        //ingresa lotes
        $DATAl = json_decode($lotes);
        for ($i=0;$i<count($DATAl);$i++) {
            $data_arrayc=array(
            'sucursalid'=>8,
            'productoid'=>$DATAl[$i]->idproducto,
            'lote'=>$DATAl[$i]->lote,
            'caducidad'=>$DATAl[$i]->caducidad,
            'cantidad'=>$DATAl[$i]->cantidad,
            'cod_barras'=>$DATAl[$i]->cod_barras,
            'idcompra'=>$idcompras,
            'reg'=>$this->fecha_larga
            );
            if($DATAl[$i]->id==0){
                //==================================================
                    $cant_inicial=$this->ModeloCatalogos->obtener_stock_lotes($DATAl[$i]->idproducto,8);
                    
                    //$res_info1=$this->ModeloCatalogos->getselectwheren('productos_sucursales',array('productoid'=>$DATAl[$i]->idproducto,'sucursalid'=>8));
                    // la siguiente consulta es casi la misma misma que la que esta en ModelPoductos->get_suc_lotes, solo que se agrego directamente la conficion que se hace en php
                    /*
                    $strq="SELECT SUM(psl.cantidad) as cantidad
                            FROM productos_sucursales_lote as psl
                            left join compra_erp c on c.id=psl.idcompra and c.estatus=1
                            WHERE psl.activo=1 and psl.cantidad>0 and psl.productoid='".$DATAl[$i]->idproducto."' and psl.sucursalid=8 AND((psl.idcompra!=0 AND c.estatus=1) OR  psl.idcompra=0)";
                            //log_message('error','ingresar_inventario lotes:'.$strq);
                    $res_info1 = $this->db->query($strq);
                    foreach ($res_info1->result() as $itemi) {
                        if($itemi->cantidad>0){
                            $cant_inicial=$itemi->cantidad;
                        }else{
                            $cant_inicial=0;
                        }
                        
                    }
                    */
                    //log_message('error','ingresar_inventario lotes: cant_inicial: '.$cant_inicial);
                    $cant_final=$cant_inicial+$DATAl[$i]->cantidad;
                    //log_message('error','ingresar_inventario lotes: cantidad '.$DATAl[$i]->cantidad.' + cant_final '.$cant_final);
                    $this->ModeloCatalogos->updateCatalogo('compra_erp_detalle',array('cant_inicial'=>$cant_inicial,'cant_final'=>$cant_final,'cantidad'=>$DATAl[$i]->cantidad),array('idcompra'=>$idcompras,'idproducto'=>$DATAl[$i]->idproducto));
                //==================================================
                $this->ModeloCatalogos->Insert('productos_sucursales_lote',$data_arrayc);
                
                $this->ModeloCompras->sumar_sucursal($DATAl[$i]->idproducto,8,$DATAl[$i]->cantidad);
            }else{
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales_lote',$data_arrayc,array('id'=>$DATAl[$i]->id));
            }
        }
    }

    function ingresar_inventario2(){   
        $data=$this->input->post();
        $idcompras=$data['idcompras'];
        $series=$data['series'];

        $DATAs = json_decode($series);
        for ($i=0;$i<count($DATAs);$i++) {
            $data_array=array(
            'sucursalid'=>8,
            'productoid'=>$DATAs[$i]->idproducto,
            'serie'=>$DATAs[$i]->serie,
            'idcompras'=>$idcompras
            );
            $this->ModeloCatalogos->Insert('productos_sucursales_serie',$data_array);
        }
    }

    function view_productos_dispersion_r(){
        $params = $this->input->post();
        $idcompras=$params['idcompras'];
        $result= $this->ModeloCompras->compras_detalles($idcompras,1,1);
        $html='';
        $html.='<ul class="nav nav-tabs" id="icon-tab" role="tablist">';
            $contx=0; $contp=0;
            foreach ($result->result() as $item){
                $contp++;
                if($item->tipo==0){
                    $nombre=$item->nombre;
                }else{
                    $nombre=$item->codigo." - Recarga de oxígeno de ".$item->capacidad."L";
                }
                if($contx==0){
                    $html.='<li class="nav-item"><a class="nav-link active" id="icon-'.$contx.'_tab" data-bs-toggle="tab" href="#icon-'.$contx.'" role="tab" aria-controls="icon-'.$contx.'" aria-selected="true"><i class="icofont icofont-man-in-glasses"></i>'.$nombre.'</a></li>';
                }else{
                    $html.='<li class="nav-item"><a class="nav-link" id="icon-'.$contx.'_tab" data-bs-toggle="tab" href="#icon-'.$contx.'" role="tab" aria-controls="icon-'.$contx.'" aria-selected="false"><i class="icofont icofont-contacts"></i>'.$nombre.'</a></li>';
                }
                $contx++;
            }
            if($contp==0){
                $html.='<li class="nav-item"><h1> -- Productos dipersados --</h1></li>';  
            }
        $html.='</ul>';
        echo $html;
    }

    function view_productos_dispersion(){
        $params = $this->input->post();
        $idcompras=$params['idcompras'];
        $result= $this->ModeloCompras->compras_detalles($idcompras,1,1);
        $html=''; $html2='';
        $contx=0; 
        foreach ($result->result() as $item){
            $produc=$item->nombre;
            $idproduc=$item->idproducto;
            $html.=''; $html2.='';
            //var_dump(array('activo'=>1,'productoid'=>$item->idproducto,'idcompras'=>$idcompras));
            $activo='';
            if($contx==0){
                $activo='show active'; 
            }else{
                $activo=''; 
            }
            if($item->tipo==0){ //prod normal
                $nombre=$item->nombre;
            }else{ //recarga de oxigeno
                $nombre=$item->codigo." - Recarga de oxígeno de ".$item->capacidad."L";
            }
            $html.='<div class="tab-pane fade  '.$activo.'" id="icon-'.$contx.'" role="tabpanel" aria-labelledby="icon-'.$contx.'_tab">
                <h4>Resumen</h4>
                <input type="hidden" id="idproducto_'.$contx.'" value="'.$item->cantidad.'">
                <input type="hidden" id="idproducto_padre_'.$contx.'" value="'.$idproduc.'">

                    <table class="table tabla_validar_pro" style="width: 100%" >
                        <thead>
                            <tr>
                                <th><input type="hidden" class="validar_pro_'.$contx.'" id="validar_pro" value="0">Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody class="table_pro_dis_r">';
                            $html.='<tr> 
                                <td>'.$nombre.'</td>
                                <td><input type="hidden" value="'.$item->cantidad.'">'.$item->cantidad.'</td>
                            </tr>';
                            $html.='</tbody>
                    </table>';
                    if($item->tipo_s==0){
                        $html.='<table class="table" id="table_distribusion_'.$contx.'" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody class="table_pro_dis">';
                                $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
                                foreach ($sucrow->result() as $s){
                                    if($s->id!=8){
                                      $html.='<tr class="tr_dis"> 
                                            <td>'.$s->name_suc.'</td>
                                            <td>'.$nombre.'</td>
                                            <td><input type="hidden" id="idsucursalx" value="'.$s->id.'">
                                                <input type="hidden" id="idproductox" value="'.$idproduc.'">
                                                <input type="hidden" id="tipoprodx" value="'.$item->tipo.'">
                                                <input type="number" id="cantidadx" class="form-control cantidadx_'.$contx.'">
                                            </td>
                                        </tr>';
                                    }
                                }
                            $html.='</tbody>
                        </table>';
                    }if($item->tipo_s==1){// prods con serie
                        $html.='<table class="table" id="table_distribusion_serie_'.$contx.'" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Número de serie</th>
                                    <th>Sucursal</th>
                                </tr>
                            </thead>';
                            $resul_row=$this->ModeloCatalogos->getselectwheren('productos_sucursales_serie',array('activo'=>1,'productoid'=>$item->idproducto,'idcompras'=>$idcompras));
                            $html.='<tbody>';
                            foreach ($resul_row->result() as $su){
                                $html_select=''; 
                                $html_select.='<select class="form-control idsucursals idsucursals_'.$contx.'">
                                <option value="0" selected >Seleccione</option>';
                                $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
                                foreach ($sucrow->result() as $x){
                                    $html_select.='<option value="'.$x->id.'">'.$x->name_suc.'</option>'; 
                                }
                                $html_select.='</select>';
                                    $html.='<tr> 
                                    <td><input type="hidden" id="idproducto_series" value="'.$su->id.'">'.$su->serie.'</td>
                                    <td>'.$html_select.'</td>
                                </tr>';
                            }
                        $html.='</tbody>
                        </table>';
                    }if($item->tipo_s==2){ // prods con lotes
                        $html.='<table class="table" id="table_dist_lote_'.$contx.'" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Cantidad Comprada</th>
                                    <th>Cantidad Traslado</th>
                                    <th>Sucursal</th>
                                    <th></th>
                                </tr>
                            </thead>';
                            $html.='<tbody>';
                            $resul_row=$this->ModeloCatalogos->getselectwheren('productos_sucursales_lote',array('activo'=>1,'productoid'=>$item->idproducto,'idcompra'=>$idcompras));
                            foreach ($resul_row->result() as $su){
                                $html_select=''; 
                                $html_select.='<select class="form-control idsucursals idsucursals_'.$contx.'" id="select_fath">
                                <option value="0" selected >Seleccione</option>';
                                $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1,'id!='=>8));
                                foreach ($sucrow->result() as $x){
                                    $html_select.='<option value="'.$x->id.'">'.$x->name_suc.'</option>'; 
                                }
                                $html_select.='</select>';
                                
                                    $html.='<tr class="tr_disp_" id="tr_disp_'.$item->idproducto.'_'.$su->id.'"> 
                                    <td><input type="hidden" id="idproducto_lote" value="'.$su->id.'">'.$su->lote.'</td>
                                    <td>'.$su->caducidad.'</td>
                                    <td><input class="form-control cant_cc_'.$su->id.'" type="hidden" id="cant_comp_'.$contx.'" value="'.$su->cantidad.'">'.$su->cantidad.'</td>';
                                    if($su->cantidad==1) { $cant_tras=1; } else{ $cant_tras=""; }
                                    $html.='<td><input class="form-control" type="number" id="cantidad" value="'.$cant_tras.'"></td>
                                    <td>'.$html_select.'</td>';
                                if($su->cantidad>1){
                                    $html.='<td><button onclick="addRowDisp('.$item->idproducto.','.$su->id.','.$su->cantidad.')" class="btn btn-primary" type="button"> <i class="fas fa-plus-square" aria-hidden="true"></i></button></td>';
                                }else{
                                    $html.='<td></td>';
                                }
                                $html.='</tr>';
                            }
                        $html.='</tbody>
                        </table>';
                    }
                    $html.='<button type="button" class="btn realizarventa btn-sm btn_can_'.$contx.'" onclick="validar_cantidad('.$contx.')">Validar</button>';
            $html.='</div>';

                /*<table class="table" id="table_distribusion_'.$contx.'" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody class="table_pro_dis">';
                            $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
                            foreach ($sucrow->result() as $s){
                                if($s->id!=8){
                                  $html.='<tr> 
                                        <td>'.$s->name_suc.'</td>
                                        <td>'.$produc.'</td>
                                        <td><input type="hidden" id="idsucursalx" value="'.$s->id.'"><input type="hidden" id="idproductox" value="'.$idproduc.'"><input type="number" id="cantidadx" class="form-control cantidadx_'.$contx.'"></td>
                                    </tr>';
                                }
                            }
                        $html.='</tbody>
                    </table>
                    <button type="button" class="btn realizarventa btn-sm btn_can_'.$contx.'" onclick="validar_cantidad('.$contx.')">Validar</button>
                    */
            /*}else{
                $html.='<div class="tab-pane fade" id="icon-'.$contx.'" role="tabpanel" aria-labelledby="icon-'.$contx.'_tab">
                    <h4>Resumen</h4>
                    <input type="hidden" id="idproducto_'.$contx.'" value="'.$item->cantidad.'">
                    <table class="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody class="table_pro_dis_r">';
                                $html.='<tr> 
                                    <td>'.$item->nombre.'</td>
                                    <td>'.$item->cantidad.'</td>
                                </tr>';
                        $html.='</tbody>
                    </table>
                    <table class="table" id="table_distribusion_'.$contx.'" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody class="table_pro_dis">';
                            $sucrow=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
                            foreach ($sucrow->result() as $s){
                                if($s->id!=8){
                                  $html.='<tr> 
                                        <td>'.$s->name_suc.'</td>
                                        <td>'.$produc.'</td>
                                        <td><input type="hidden" id="idsucursalx" value="'.$s->id.'"><input type="hidden" id="idproductox" value="'.$idproduc.'"><input type="number" id="cantidadx" class="form-control cantidadx_'.$contx.'"></td>
                                    </tr>';
                                }
                            }
                        $html.='</tbody>
                    </table>
                    <button type="button" class="btn realizarventa btn-sm btn_can_'.$contx.'" onclick="validar_cantidad('.$contx.')">Validar</button>
                </div>';*/
            //}
            $contx++;
        }        
        echo $html;
    }

    function registro_distribusion(){
        $datatra['idpersonal']=$this->idpersonal;
        $datatra['status']=1;

        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $idcompra=0;
        $idtranspaso=$this->General_model->add_record('traspasos',$datatra);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idcompra']=$DATA[$i]->idcompra;    
            $data['idsucursal']=$DATA[$i]->idsucursal;    
            $data['idproducto']=$DATA[$i]->idproducto; 
            $data['cantidad']=$DATA[$i]->cantidad; 
            $idcompra=$DATA[$i]->idcompra;
            if($DATA[$i]->cantidad!=''){
                //$idtranspaso=$this->General_model->add_record('traspasos',$datatra);
                $datainfo['idsucursal_sale']=8;
                $datainfo['idsucursal_entra']=$DATA[$i]->idsucursal;
                if($DATA[$i]->tipo==0){ //prod normal
                    $cant=$DATA[$i]->cantidad;
                }else{ //recarga de oxigeno
                    $cant = 9500 * $DATA[$i]->cantidad;
                }
                $datainfo['cantidad']=$cant;
                $datainfo['idproducto']=$DATA[$i]->idproducto;
                $datainfo['idpersonal']=$this->idpersonal;
                $datainfo['idtranspasos']=$idtranspaso;
                $datainfo['id_compra']=$idcompra;
                $datainfo['tipo']=$DATA[$i]->tipo;
                $this->General_model->add_record('historial_transpasos',$datainfo);

                //se tiene que formar a solicitud antes de ser aceptado y afectar stocks
                /*if($DATA[$i]->tipo==0){ //prod normal
                    $this->ModeloCompras->sumar_sucursal($DATA[$i]->idproducto,$DATA[$i]->idsucursal,$DATA[$i]->cantidad);
                    $this->ModeloCompras->resta_sucursal($DATA[$i]->idproducto,8,$DATA[$i]->cantidad);
                }else{ //recarga de oxigeno
                    $cant = 9500 * $DATA[$i]->cantidad;
                    $this->ModelTraspasos->update_tanque_sucu($DATA[$i]->idproducto,$DATA[$i]->idsucursal,$cant,"+");
                    $this->ModelTraspasos->update_tanque_sucu($DATA[$i]->idproducto,8,$cant,"-");
                }*/
            }   
        }
    }

    function registro_distribusion_series(){
        $datatra['idpersonal']=$this->idpersonal;
        $datatra['status']=1;

        $data=$this->input->post();
        $series=$data['series'];
        unset($data["series"]);
        $DATA = json_decode($series);

        $lotes=$data['lotes'];
        unset($data["lotes"]);

        $idcompra=0;
        $idtranspaso=$this->General_model->add_record('traspasos',$datatra);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idcompra']=$DATA[$i]->idcompra;    
            $data['idsucursal']=$DATA[$i]->idsucursal;    
            $data['idproducto_series']=$DATA[$i]->idproducto_series; 
            $data['idproducto']=$DATA[$i]->idproducto; 
            $data['cantidad']=1; 
            $idcompra=$DATA[$i]->idcompra;
            if($DATA[$i]->idsucursal!=''){
                //$idtranspaso=$this->General_model->add_record('traspasos',$datatra);
                $datainfo['idsucursal_sale']=8;
                $datainfo['idsucursal_entra']=$DATA[$i]->idsucursal;
                $cant=1;
                
                $datainfo['cantidad']=$cant;
                $datainfo['idproducto_series']=$DATA[$i]->idproducto_series;
                $datainfo['idproducto']=$DATA[$i]->idproducto;
                $datainfo['idpersonal']=$this->idpersonal;
                $datainfo['idtranspasos']=$idtranspaso;
                $datainfo['id_compra']=$idcompra;
                $datainfo['tipo']=0;
                $this->General_model->add_record('historial_transpasos',$datainfo);

                //se tiene que formar a solicitud antes de ser aceptado y afectar stocks
                /*$this->ModeloCompras->sumar_sucursal($DATA[$i]->idproducto,$DATA[$i]->idsucursal,$DATA[$i]->cantidad);
                $this->ModeloCompras->resta_sucursal($DATA[$i]->idproducto,8,$DATA[$i]->cantidad);*/
            }
        }
      
        //ingresa lotes
        $DATAl = json_decode($lotes);
        for ($i=0;$i<count($DATAl);$i++) {
            $data['idcompra']=$DATAl[$i]->idcompra;    
            $data['idsucursal']=$DATAl[$i]->idsucursal;    
            $data['idproducto_lote']=$DATAl[$i]->idproducto_lote; 
            $data['idproducto']=$DATAl[$i]->idproducto; 
            $data['cantidad']=$DATAl[$i]->cantidad; 
            $idcompra=$DATAl[$i]->idcompra;
            if($DATAl[$i]->cantidad!=''){
                //$idtranspaso=$this->General_model->add_record('traspasos',$datatra);
                $datainfo['idsucursal_sale']=8;
                $datainfo['idsucursal_entra']=$DATAl[$i]->idsucursal;
                $cant=$DATAl[$i]->cantidad;
                
                $datainfo['cantidad']=$cant;
                $datainfo['idproducto_lote']=$DATAl[$i]->idproducto_lote;
                $datainfo['idproducto']=$DATAl[$i]->idproducto;
                $datainfo['idpersonal']=$this->idpersonal;
                $datainfo['idtranspasos']=$idtranspaso;
                $datainfo['id_compra']=$idcompra;
                $datainfo['tipo']=0;
                $this->General_model->add_record('historial_transpasos',$datainfo);

                //se tiene que formar a solicitud antes de ser aceptado y afectar stocks
                /*$this->ModeloCompras->sumar_sucursal($DATAl[$i]->idproducto,$DATAl[$i]->idsucursal,$DATAl[$i]->cantidad);
                $this->ModeloCompras->resta_sucursal($DATAl[$i]->idproducto,8,$DATAl[$i]->cantidad);*/
            }
        }
    }

    function registro_distribusion2(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $idcompra=0;
        for ($i=0;$i<count($DATA);$i++) {   
            $datax['idcompra']=$DATA[$i]->idcompra;    
            $datax['idsucursal']=$DATA[$i]->idsucursal;    
            $datax['idproducto']=$DATA[$i]->idproducto; 
            $datax['cantidad']=$DATA[$i]->cantidad; 
            $idcompra=$DATA[$i]->idcompra;
            if($DATA[$i]->cantidad!=""){
                $this->General_model->add_record('compras_distribusion_productos',$datax);
            }
        }
    }

    function registro_distribusion3(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $idcompra=0;
        for ($i=0;$i<count($DATA);$i++) {   
            $datax['idcompra']=$DATA[$i]->idcompra;    
            $datax['idsucursal']=$DATA[$i]->idsucursal;    
            $datax['idproducto']=$DATA[$i]->idproducto; 
            $datax['cantidad']=$DATA[$i]->cantidad; 
            $idcompra=$DATA[$i]->idcompra;
            $this->ModeloCompras->update_distribusion($DATA[$i]->idcompra,$DATA[$i]->idproducto); 
        }
    }

    function update_distribusion()
    {   
        $idcompra=$this->input->post('idcompra');
        $this->General_model->edit_record('id',$idcompra,array('distribusion'=>1),'compra_erp');
    }
    
    public function get_validar_serie(){
        $serie=$this->input->post('serie');
        $result=$this->General_model->getselectwhereall('productos_sucursales_serie',array('serie'=>$serie,'activo'=>1));
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function update_factura(){   
        $id=$this->input->post('id');
        $factura=$this->input->post('factura');
        $this->General_model->edit_record('id',$id,array('factura'=>$factura),'compra_erp');
    }
    
    function cambio_estatus()
    {
        $id=$this->input->post('id');
        $arrayinfo = array('reg_norecibido'=>$this->fechahoy,'idpersonal_norecibido'=>$this->idpersonal,'estatus'=>3);    
        $id=$this->General_model->edit_record('id',$id,$arrayinfo,'compra_erp');
    }

    function get_oc_estatus(){   
        $code=$this->input->post('code');
        $getd=$this->ModeloCompras->getOC_idCode($code);
        echo json_encode($getd->result());

    }
    
        
}