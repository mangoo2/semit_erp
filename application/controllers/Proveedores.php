<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelProveedores');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,25);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=25;
        $data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        $data['refe']=$this->ModeloCatalogos->getselectwheren('referencias',array('activo'=>1));
        $data["perfilid"]=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('proveedores/index');
        $this->load->view('templates/footer');
        $this->load->view('proveedores/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=25;
        if($id==0){
            $serie=0;
            $result=$this->ModelProveedores->get_productos_numero();
            foreach ($result as $x){
                $serie=$x->total;
            }
            $total=$serie+1;
            $codigo='P'.str_pad($total, 4, "0", STR_PAD_LEFT);

            $data['id'] = 0;
            $data['codigo']=$codigo;
            $data['nombre']='';
            $data['tel1'] = '';
            $data['tel2']='';
            $data['celular']='';
            $data['correo']='';
            $data['contacto']='';
            $data['estatus']=0;
            $data['razon_social']='';
            $data['rfc']='';
            $data['cp']='';
            $data['RegimenFiscalReceptor']='';
            $data['uso_cfdi']='';
            $data['activar_datos_fiscales']=0; 
            $data['direccion']='';
        }else{
            $resul=$this->General_model->getselectwhere('proveedores','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['codigo']=$item->codigo;
                $data['nombre']=$item->nombre;
                $data['tel1']=$item->tel1;
                $data['tel2']=$item->tel2;      
                $data['celular']=$item->celular;
                $data['correo']=$item->correo;
                $data['contacto']=$item->contacto;
                $data['estatus']=$item->estatus;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['cp']=$item->cp;
                $data['RegimenFiscalReceptor']=$item->RegimenFiscalReceptor;
                $data['uso_cfdi']=$item->uso_cfdi;
                $data['activar_datos_fiscales']=$item->activar_datos_fiscales;
                $data['direccion']=$item->direccion;
            }
        }      

        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        $data['get_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('proveedores/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/formjs');
    }

    public function registro_generales(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $serie=0;
            $result=$this->ModelProveedores->get_productos_numero();
            foreach ($result as $x){
                $serie=$x->total;
            }
            $total=$serie+1;
            $codigo='P'.str_pad($total, 4, "0", STR_PAD_LEFT);

            if(isset($data['estatus'])){
                $data['estatus']=1;
            }else{
                //$data['estatus']=0;
                $data['estatus']=1;
            }

            $data['serie']=$serie;
            $data['codigo']=$codigo;
            $data['idpersonal']=$this->idpersonal;
            $data['reg']=$this->fechahoy;
            $id=$this->General_model->add_record('proveedores',$data);
        }else{
            if(isset($data['estatus'])){
                $data['estatus']=1;
            }else{
                $data['estatus']=0;
            }
            $this->General_model->edit_record('id',$id,$data,'proveedores');
        }
        echo $id;
    }

    public function registro_fiscales(){
        $data=$this->input->post();
        $id=$data['id'];
        if(isset($data['activar_datos_fiscales'])){
            $data['activar_datos_fiscales']=1;
        }else{
            $data['activar_datos_fiscales']=0;
        }
        if($id==0){
            $data['idpersonal']=$this->idpersonal;
            $data['reg']=$this->fechahoy;
            $id=$this->General_model->add_record('proveedores',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'proveedores');
        }
        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelProveedores->get_listado($params);
        $totaldata= $this->ModelProveedores->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'proveedores');
    }

    function get_datos_fiscales(){
        $id=$this->input->post('id');
        $html='<table class="table table-sm" style="width:100%">
            <tbody>';
            $resul=$this->General_model->getselectwhere('proveedores','id',$id);
            foreach ($resul as $item) {
                $regimen='';
                $resulrf=$this->General_model->getselectwhere('f_regimenfiscal','clave',$item->RegimenFiscalReceptor);
                foreach ($resulrf as $x) {
                    $regimen=$x->descripcion;
                }

                $uso='';
                $resulrfd=$this->General_model->getselectwhere('f_uso_cfdi','uso_cfdi',$item->uso_cfdi);
                foreach ($resulrfd as $x) {
                    $uso=$x->uso_cfdi_text;
                }  

                $html.='<tr>
                   <td>Razón social:</td>
                   <td>'.$item->razon_social.'</td>
                </tr>
                <tr>
                   <td>RFC:</td>
                   <td>'.$item->rfc.'</td>
                </tr>
                <tr>
                   <td>CP: </td>
                   <td>'.$item->cp.'</td>
                </tr>
                <tr>
                   <td>Régimen fiscal:</td>
                   <td>'.$regimen.'</td>
                </tr>
                <tr>
                   <td>Uso de CFDI:</td>
                   <td>'.$uso.'</td>
                </tr>';
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

    public function getDataProds() {
        $id = $this->input->post("id");
        $datas = $this->ModelProveedores->get_productos_provee($id);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    } 

    function exportProdsExcel($id){
        $data["id"]=$id;
        $data["result_productos"]=$this->ModelProveedores->get_productos_provee($id);
        $this->load->view('proveedores/excelProds',$data);
    }

    public function imprimir_pdf($id){
        $data['id']=$id;
        $data['result_productos']=$this->ModelProveedores->get_productos_provee($id);
        $this->load->view('reportes/pdf_prods_provee',$data);
    }

    public function saldoCompras($id=0){   
        $data["id"]=$id;
        $datas['btn_active']=2;
        $datas['btn_active_sub']=25;
        //$data['compras']=$this->ModelProveedores->get_kardex_compras($params);
        $getpro=$this->General_model->getselectwhere('proveedores','id',$id);
        $data["provee"] = $getpro[0]->nombre;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$datas);
        $this->load->view('proveedores/compras_kardex',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/compras_kardexjs');
    }

    public function getComprasProvee() {
        $params = $this->input->post();      
        $getdata = $this->ModelProveedores->get_kardex_compras($params);
        $totaldata= $this->ModelProveedores->total_kardex_compras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function exportExcel($id,$fi,$ff){
        $params["id"]=$id;
        $params["f1"]=$fi;
        $params["f2"]=$ff;
        $data["comp"]=$this->ModelProveedores->get_excelk_compras($params);
        $this->load->view('proveedores/excelCompras',$data);
    }

    function exportExcelPove(){
        $data["prove"]=$this->ModelProveedores->listExcelProve();
        $this->load->view('proveedores/excelProve',$data);
    }

}
