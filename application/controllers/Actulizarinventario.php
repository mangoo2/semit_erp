<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actulizarinventario extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,13);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=14;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('actulizarinventario/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('actulizarinventario/indexjs');
    }

    public function generatoken(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://semit.vertrou.mx:91/inventarios/api/login/authenticate',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"username":"cliente","password":"c0rp0n37"}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        $data = json_decode($response);
        $token = json_encode($data->valor);
        $token =str_replace('"', '',$token);
        //echo $token;
        curl_close($curl);
        return $token;
    }
    public function producto(){
        $token=$this->generatoken();
        log_message('error', 'token: '.$token);
        $url="http://semit.vertrou.mx:91/inventarios/api/producto/SEM0206032U7/000001";

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function productos($pagina){
        $token=$this->generatoken();
        log_message('error', 'token: '.$token);
        $url="http://semit.vertrou.mx:91/inventarios/api/productos/SEM0206032U7/".$pagina;

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        //curl_close($curl);
        return $response;
    }

    public function actulizar_productos()
    {   
      $resultf=$this->General_model->get_select('productos_historial',array('id'=>1));
      $pagina=0;
      foreach ($resultf->result() as $f){
        if($f->num==26){
          $this->General_model->edit_record('id',1,array('num'=>0),'productos_historial');
        }
        $pagina=$f->num+1;
      }
      $this->get_productos($pagina);
      echo $pagina;
    }
    
    public function get_productos($pagina)
    {   
        //echo '__'.$pagina.'__';
        $result=$this->productos($pagina);
        //echo $result;
        $produ=json_decode($result);
        //echo $produ;
        //echo $produ->registros;
        $resultp=$produ->registros;
        $cont=0;

        $resultf=$this->General_model->get_select('productos_historial',array('id'=>1));
        $valor=0;
        foreach ($resultf->result() as $f){
          $valor=$f->num+1;
        }

        //$this->General_model->add_record('productos_historial',array('fecha'=>$this->fecha_reciente));
        //$this->General_model->edit_record_tabla(array('num'=>$valor),'productos_historial'); 
        $this->General_model->edit_record('id',1,array('num'=>$valor),'productos_historial');
        //$this->General_model->edit_record_tabla(array('estatus'=>0),'productos'); 
        foreach ($resultp as $x){
          $data = array('idProducto'=>$x->idProducto,
          'nombre'=>$x->nombreProducto,
          'marca'=>$x->marca,
          'codigoBarras'=>$x->codigoBarras,
          'unidadMedida'=>$x->unidadMedida,
          'activo'=>$x->activo,
          'activoDesde'=>$x->activoDesde,
          'activoHasta'=>$x->activoHasta,
          'mostrar_pagina'=>$x->pagina,
          'cpaginas'=>$x->cpaginas,
          'listaPrecio'=>$x->listaPrecio,
          'stock'=>$x->existencia,
          'precio_con_iva'=>$x->precio,
          'existenciaAlmacen'=>$x->existenciaAlmacen,
          'comprometido'=>$x->comprometido,
          'pedido'=>$x->pedido,
          'longuitud'=>$x->longuitud,
          'ancho'=>$x->ancho,
          'alto'=>$x->alto,
          'volumen'=>$x->volumen,
          'peso'=>$x->peso,
          'almacen'=>$x->almacen,
          'PrecioV'=>$x->PrecioV,
          'UMLargo'=>$x->UMLargo,
          'UMAncho'=>$x->UMAncho,
          'UMAlto'=>$x->UMAlto,
          'UMPeso'=>$x->UMPeso,
          'porcentajeIva'=>$x->porcentajeIva,
          'descripcion'=>$x->descripcion,
          'categoria'=>$x->idCategoria,
          'Categoriat'=>$x->Categoria,
          'estatus'=>1);
          $result2=$this->General_model->get_select('productos',array('idProducto'=>$x->idProducto));
          $id=0;
          foreach ($result2->result() as $p){
            $id=$p->id;
          }
          // agregar cambios de estatus por si no existen 
          if($id==0){
            $this->General_model->add_record('productos',$data);
          }else{
            $this->General_model->edit_record('id',$id,$data,'productos');
          }

          $resultc2=$this->General_model->get_select('categoria',array('categoriaId'=>$x->idCategoria));
          $categoriaId=0;
          foreach ($resultc2->result() as $c2){
            $categoriaId=$c2->categoriaId;
          }
          
          $datac = array('categoriaId'=>$x->idCategoria,'categoria'=>$x->Categoria); 
          if($categoriaId==0){
            $this->General_model->add_record('categoria',$datac);
          }else{
            $this->General_model->edit_record('categoriaId',$categoriaId,$datac,'categoria');
          }

          $cont++;
        }
      
      } 
}


        /*$pagina1=1;
        if($pagina==1){
          $valor=4;
        }else if($pagina==2){
          $valor=8;
        }else if($pagina==3){
          $valor=12;
        }else if($pagina==4){
          $valor=16;
        }else if($pagina==5){
          $valor=20;
        }else if($pagina==6){  
          $valor=24;
        }else if($pagina==7){
          $valor=28;
        }else if($pagina==8){
          $valor=32;
        }else if($pagina==9){
          $valor=36;
        }else if($pagina==10){
          $valor=40;
        }else if($pagina==11){
          $valor=44;
        }else if($pagina==12){
          $valor=48;
        }else if($pagina==13){
          $valor=52;
        }else if($pagina==14){
          $valor=56;
        }else if($pagina==15){
          $valor=60;
        }else if($pagina==16){
          $valor=64;
        }else if($pagina==17){
          $valor=68;
        }else if($pagina==18){
          $valor=72;
        }else if($pagina==19){
          $valor=76;
        }else if($pagina==20){
          $valor=80;
        }else if($pagina==21){
          $valor=84;
        }else if($pagina==22){
          $valor=88;
        }else if($pagina==23){
          $valor=92;
        }else if($pagina==24){
          $valor=96;
        }else if($pagina==25){  
          $valor=100;
        }*/