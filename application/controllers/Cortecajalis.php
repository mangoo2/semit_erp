<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cortecajalis extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelClientes');
        $this->load->model('Modeloventas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->empleado=$this->session->userdata('empleado');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,20);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema');
        }
    }

    public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['emplerow']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['perfilid']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cortecaja/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('cortecaja/indexjs');
    }
    
    function generararqueo(){
        $params = $this->input->post();
        $suc =$params['suc'];
        $emple =$params['emple'];

        $result_tar=$this->ModeloCatalogos->generararqueosinfo($suc,$this->fechainicio);
        $folio=0;
        foreach ($result_tar->result() as $item) {
            $folio=$item->folio;
        }
        if($folio>0){
            $folio=$folio+1;
        }else{
            $folio=1;
        }
        $result_suc=$this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$suc));
        $sucursal='';
        foreach ($result_suc->result() as $item) {
            $sucursal=$item->name_suc;
        }

        $result_datos=$this->ModeloCatalogos->generararqueosql($emple,$suc,$this->fechainicio);

        $datos = array(
                    'folio'=>$folio,
                    'perid'=>$this->idpersonal,
                    'pername'=>$this->empleado,
                    'sucursalid'=>$suc,
                    'sucursalname'=>$sucursal,
                    'datosmontos'=>$result_datos->result()
                );
        echo json_encode($datos);


    }
    function arqueo_pdf($idarqueo=0){
        $data['idarqueo']=$idarqueo;
        $this->load->view('reportes/arqueopdf',$data);
    }
    function guardararqueo(){
        $params = $this->input->post();
        $conceptos=$params['conceptos'];
        $params['fecha']=$this->fechainicio;
        

        unset($params['conceptos']);
        
        $idarqueo=$this->ModeloCatalogos->Insert('arqueos',$params);

        $datainsertarray=array();

        $DATAc = json_decode($conceptos);
        for ($i=0;$i<count($DATAc);$i++) {
            $dataco['idarqueo']=$idarqueo;
            $dataco['clavefm']=$DATAc[$i]->clavefm;
            $dataco['bancoid']=$DATAc[$i]->bancoid;
            $dataco['monto']=$DATAc[$i]->monto;
            $datainsertarray[]=$dataco;
        }
        if(count($datainsertarray)>0){
            $this->ModeloCatalogos->insert_batch('arqueos_dll',$datainsertarray);
        }

        $result_dll =  $this->ModeloCatalogos->info_arqueo_dll($idarqueo);
        
        $array=array(
                    'formapago'=>$result_dll->result(),
                    'idarqueo'=>$idarqueo
                    );

        echo json_encode($array);
    }
    function guardararqueop2(){
        $params = $this->input->post();
        $conceptos=$params['conceptos'];
        $DATAc = json_decode($conceptos);
        for ($i=0;$i<count($DATAc);$i++) {
            $id=$DATAc[$i]->id;
            $monto_confirm=$DATAc[$i]->monto_confirm;
            $this->ModeloCatalogos->updateCatalogo('arqueos_dll',array('monto_confirm'=>$monto_confirm),array('id'=>$id));
        }
    }

   
}