<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AvisoPrivacidad extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,13);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=13;
        $resul=$this->General_model->getselectwhere('aviso_privacidad','id',1);
        foreach ($resul as $item){
            $data['avisoprivacidad']=$item->concepto;
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('aviso/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('aviso/indexjs');
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$this->General_model->edit_record('id',1,$data,'aviso_privacidad');
    }


}