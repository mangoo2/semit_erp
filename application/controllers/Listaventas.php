<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Listaventas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloventas');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,19);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=6;
        $data['btn_active_sub']=19;
        $data['get_categorias']=$this->General_model->getselectwhere('categoria','activo',1);
        //$data['get_sucuarsales']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1) );
        $data['get_suc']=$this->General_model->getSelectColOrder('*','sucursal',array('activo'=>1),"orden");
        $data['get_ventas']=$this->Modeloventas->get_vendedores();
        $data['idpersonal']=$this->idpersonal;
        $data['get_formaspagosv']=$this->General_model->formaspagosv();

        $data['get_cfdi']=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('activo'=>1));
        $data['get_regimen']=$this->General_model->get_select('f_regimenfiscal',array('activo'=>1));
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ventasp/index');
        $this->load->view('templates/footer');
        $this->load->view('ventasp/indexjs');
    }
    public function getlistventas() {
        $params = $this->input->post();
        if($this->idpersonal!=1 && $this->session->userdata('perfilid')!=1){
            $params['sucursal'] = $this->sucursal;
        }        
        $getdata = $this->Modeloventas->getventas($params);
        $totaldata= $this->Modeloventas->total_getventas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function consu_vendedores(){
        $params = $this->input->post();
        $suc = $params['suc'];

        $strq = "SELECT p.nombre,p.personalId
                FROM usuarios as u 
                INNER JOIN personal as p on p.personalId=u.personalId 
                WHERE u.sucursal='$suc' and (u.perfilId=2  or u.perfilId=3 or u.perfilId=7 or u.perfilId=10) AND p.estatus=1 and u.estatus=1";
        $query = $this->db->query($strq);
        $html='<option selected=""  value="0">Seleccionar vendedor</option>';
        foreach ($query->result() as $item) {
            $html.='<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
        }
        echo $html;
    }

    function view_productos(){
        $params = $this->input->post();
        $ventaid=$params['ventaid'];
        $result= $this->Modeloventas->ventas_detalles($ventaid);
        $html='';
        foreach ($result->result() as $item) {
            if($item->tipo==0){
                if($item->id_ps_lote==0 && $item->id_ps_serie==0){
                    $nombre=$item->nombre;
                }else{
                    if($item->id_ps_lote!=0){
                        $nombre=$item->nombre.' Lote: '.$item->lote;    
                    }else{
                        $nombre=$item->nombre.' Serie: '.$item->serie;
                    }
                }
            }if($item->tipo==1){
                $nombre=$item->codigo;
            }if($item->tipo==2){
                $nombre=$item->clave." / ".$item->desc_serv;
            }
            $style="";
            if($item->solicita_garantia==1){
                $style='style="background-color:rgb(255,64,64,0.7)"';
            }
            $importe=($item->cantidad*$item->precio_unitario)-$item->descuento;
            $html.='<tr '.$style.'>
                    <td>'.$item->cantidad.'</td>
                    <td>'.$nombre.'</td>
                    <td>$'.number_format($item->precio_unitario,2,".",",").'</td>
                    <td>$'.number_format($item->descuento,2,".",",").'</td>
                    <td>$'.number_format($importe,2,".",",").'</td>
                </tr>';
        }
        $result2 = $this->Modeloventas->ventas_detalles_servicio($ventaid);
        foreach ($result2->result() as $item) {
            $cant=$item->cantidad;
            $precio=$item->precio_unitario;
            if($item->incluye_iva==1){
                $iva=round($precio*0.16,5);
                $precio=round($precio+$iva,2);
            }
            $importe=$precio*$cant;
            $html.='<tr>
                    <td>'.$cant.'</td>
                    <td>'.$item->descripcion.'</td>
                    <td>$'.number_format($precio,2,".",",").'</td>
                    <td>$'.number_format($item->descuento,2,".",",").'</td>
                    <td>$'.number_format($importe,2,".",",").'</td>
                </tr>';
        }
        echo $html;
    }
    function view_formapagos(){
        $params = $this->input->post();
        $ventaid=$params['ventaid'];
        $total = $params['total'];
        $html='<div class="row"><div class="col-md-12"><h4>Total: '.$total.'<h4></div></div>';
        $html.='<table class="table" style="width: 100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Últimos 4 dígitos</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>';
        $montog=0;
        $result= $this->Modeloventas->ventas_formaspagos($ventaid);
        foreach ($result->result() as $item) {
            $montog=$montog+$item->monto;
            $html.='<tr>
                            <td>'.$item->formapago_text.'</td>
                            <td>'.$item->ultimosdigitos.'</td>
                            <td>$'.number_format($item->monto,2,".",",").'</td>
                        </tr>';
        }
        $cambio=$montog-$total;
        $html.='</tbody>';
        if($cambio>0){
            $html.='<tfoot><tr><td>Cambio</td><td>'.$cambio.'</td></tr></tfoot>';
        }
        
        $html.='</table>';
        echo $html;
    }

    function view_productos_garantia(){
        $params = $this->input->post();
        $ventaid=$params['ventaid'];
        $result= $this->Modeloventas->ventas_detalles($ventaid);
        $html=''; $motivo=""; $cli=""; $dir=""; $col=""; $cel=""; $tel=""; $mail=""; $rfc=""; $nom_contacto=""; $tel_contacto="";
        foreach ($result->result() as $item) {
            if($item->tipo!=2){
                if($item->tipo==0){
                    if($item->id_ps_lote==0 && $item->id_ps_serie==0){
                        $nombre=$item->nombre." Recarga de oxígeno ".$item->capacidad;
                    }else{
                        if($item->id_ps_lote!=0){
                            $nombre=$item->nombre.' Lote: '.$item->lote;    
                        }else{
                            $nombre=$item->nombre.' Serie: '.$item->serie;
                        }
                    }
                }if($item->tipo==1){
                    $nombre=$item->codigo."/ Recarga de oxígeno de ".$item->capacidad." L";
                }
                $importe=($item->cantidad*$item->precio_unitario)-$item->descuento;
                $dis=""; $style=""; $check="";
                if($item->solicita_garantia==1){
                    $motivo=$item->motivo;
                    $dis="disabled";
                    $style='style="background-color:rgb(255,64,64,0.7)"';
                    $check="checked";
                }
                if($item->capacidad>0){
                    $dis="disabled";
                }
                $cli=$item->nom_solicita; $dir=$item->dir_solicita; $col==$item->col_solicita; $cel=$item->cel_solicita;
                $tel=$item->tel_solicita; $mail==$item->mail_solicita; $rfc=$item->rfc_solicita; $nom_contacto=$item->nom_contacto; $tel_contacto=$item->tel_contacto;
                $html.='<tr '.$style.'>
                    <td><input type="hidden" id="idprod" value="'.$item->id_prod.'">
                        <input type="hidden" id="id_ps_lot" value="'.$item->id_ps_lot.'">
                        <input type="hidden" id="id_ps_ser" value="'.$item->id_ps_ser.'">
                        <input type="hidden" id="tipo_prod" value="'.$item->tipo_prod.'">
                        <input type="hidden" id="id_vd" value="'.$item->id_vd.'">
                        <input type="hidden" id="cant" value="'.$item->cantidad.'">
                        '.$item->cantidad.'
                    </td>
                    <td>'.$nombre.'</td>
                    <td>'.$item->precio_unitario.'</td>
                    <td>'.$item->descuento.'</td>
                    <td>$'.number_format($importe,2,".",",").'</td>
                    <td><span class="switch switch-icon">
                            <label>
                                <input '.$check.' type="checkbox" name="solicitar" id="solicitar_'.$item->id_prod.'_'.$item->id_vd.'" class="check_pago" '.$dis.'>
                                <span style="border: 2px solid #009bdb;"></span>
                            </label>
                        </span>
                    </td>
                </tr>';
                //agregar tr con datos en caso de que tenga motivo de rechazo, o si es aceptado lo muestre como aceptado y los comentarios
            } 
        }
        echo json_encode(array("html"=>$html,"motivo"=>$motivo,"cli"=>$cli,"dir"=>$dir,"col"=>$col,"cel"=>$cel,"tel"=>$tel,"mail"=>$mail,"rfc"=>$rfc,"nom_contacto"=>$nom_contacto,"tel_contacto"=>$tel_contacto));
    }

    function save_solicitudVenta(){
        $datos = $this->input->post();
        $id_venta=$datos['id_venta'];
        unset($datos["id_venta"]);
        $motivo=$datos['motivo'];
        unset($datos["motivo"]);
        $prods=$datos['array_prod'];
        unset($datos["array_prod"]);

        $datag["motivo"]=$motivo;
        $datag['idpersonal']=$this->idpersonal;
        $datag['id_origen']=$this->sucursal;
        $datag['reg']=$this->fechahoy;
        $datag['id_venta']=$id_venta;
        $id=$this->General_model->add_record('garantias',$datag);

        $dataog=$datos;
        $dataog["id_garantia"]=$id;
        $this->General_model->add_record('orden_garantia',$dataog);

        $DATA = json_decode($prods);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idprod']=$DATA[$i]->idprod;
            $data['id_ps_lot']=$DATA[$i]->id_ps_lot; 
            $data['id_ps_ser']=$DATA[$i]->id_ps_ser;
            $data['tipo']=$DATA[$i]->tipo;
            $data['id_origen']=$this->sucursal;
            $data['cant']=$DATA[$i]->cant;
            $data['id_garantia']=$id;

            $this->General_model->add_record('garantias_detalle',$data);
            $this->General_model->edit_record('id',$DATA[$i]->id_vd,array("solicita_garantia"=>1),'venta_erp_detalle');
        }
        echo $id_venta;
    }

    function guardar_pago(){
        $params = $this->input->post();
        $datap['ventaId'] = $params['ventaid'];
        $datap['fecha'] = $params['pago_fecha'];
        $datap['formapago'] = $params['pago_formapago'];
        $datap['pago'] = $params['pago_pago'];
        $datap['comentario'] = $params['pago_comentario'];
        $datap['personalId'] = $this->idpersonal;
        $this->ModeloCatalogos->Insert('venta_erp_credito_pagos',$datap);
    }
    function table_get_credito_pagos(){
        $id = $this->input->post('id');
        $result = $this->Modeloventas->pagos_ventas($id); 
        $html='';
        foreach ($result->result() as $item) {
            $html.='<tr class="tr_2"><td><input type="hidden" id="idventap" value="'.$item->id.'" >
                            '.$item->fecha.'</td><td>'.$item->formapago_text.'</td><td class="montoagregado">'.$item->pago.'</td><td>'.$item->comentario.'</td><td>'.$item->nombre.'</td> 
                         <td>
                            <!--<span 
                                class="new badge red" 
                                style="cursor: pointer;" 
                                onclick="deletepagov('.$item->id.')">
                                <i class="material-icons">delete</i>
                            </span>-->
                            
                        </td>
                        </tr>'; 
        }
        echo $html;
    }
    function infofac(){
        $id=$this->input->post('id');
        $result_fac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$id));
        $result_fac=$result_fac->row();
        
        $result_rf=$this->ModeloCatalogos->getselectwheren('f_regimenfiscal',array('clave'=>$result_fac->RegimenFiscalReceptor));
        $result_rf=$result_rf->row();
        
        $result_uc=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('uso_cfdi'=>$result_fac->uso_cfdi));
        $result_uc=$result_uc->row();

        $json_data=array(
                        'fechatimbrado'=>$result_fac->fechatimbre,
                        'razonsocial'=>$result_fac->Nombre,
                        'rfc'=>$result_fac->Rfc,
                        'regimen'=>$result_fac->RegimenFiscalReceptor.' '.$result_rf->descripcion,
                        'codigop'=>$result_fac->Cp,
                        'uso'=>$result_uc->uso_cfdi_text
                    );
        echo json_encode($json_data);
    }
    function infoventa(){
        $params=$this->input->post();
        $id=$params['id'];
        $fac=$params['fac'];
        $fac_folio='';
        $fac_uuid='';
        if($fac>0){
            $result_fac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$fac));
            $result_fac=$result_fac->row();
            $fac_folio=$result_fac->Folio;
            $fac_uuid=$result_fac->uuid;
        }else{
            $fac=0;
        }

        $result_ven=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$id));
        $result_ven=$result_ven->row();
        $r_vd = $this->Modeloventas->ventas_detalles($id);
        $json_data=array(
                        'folio'=>$result_ven->folio,
                        'subtotal'=>$result_ven->subtotal,
                        'iva'=>$result_ven->iva,
                        'total'=>$result_ven->total,
                        'fac'=>$fac,
                        'fac_folio'=>$fac_folio,
                        'fac_uuid'=>$fac_uuid,
                        'productos'=>$r_vd->result()
                    );
        echo json_encode($json_data);
    }
    function cancelarventa2(){
        $params = $this->input->post();
        $id = $params['id'];
        $mot = $params['mot'];
        //==================================================================
        $result_ven=$this->ModeloCatalogos->getselectwheren('venta_erp',array('id'=>$id));
        $result_ven=$result_ven->row();

        $sucursalid=$result_ven->sucursal;
        $activo=$result_ven->activo;

        //==================================================================
        if($activo==1){
           $data_up_v= array(
                        'delete_reg'=>$this->fechahoy,
                        'delete_mot'=>$mot,
                        'delete_user'=>$this->idpersonal,
                        'activo'=>0
            );

            $this->ModeloCatalogos->updateCatalogo('venta_erp',$data_up_v,array('id'=>$id));

            $result_vend=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$id));
            foreach ($result_vend->result() as $item) {

                $this->ModeloCatalogos->updatestock3('productos_sucursales','stock','+',$item->cantidad,'productoid',$item->idproducto,'sucursalid',$sucursalid,'activo',1);
            } 
        }
    }

    function getCorteDia(){
        $dia=date("Y-m-d");
        $suc=$this->input->post("id_sucursal");
        //$get_corte=$this->Modeloventas->generarCorte($suc,$dia,"01"); //efectivo
        $moto_devolucion = $this->ModeloCatalogos->obtenermontosdevoluciones($suc,$dia);
        $html=""; $supertot=0; $monto = 0;
        $efectivo=0;$tdc=0;$tdd=0;$tds=0;$che=0;$transf=0;
        /*foreach($get_corte->result() as $c){
            $supertot=$supertot+$c->monto;
            $alias = $c->clavefm;
            if($alias=="28" || $alias=="04"){
                $alias= "Tarjetas";
            }else{
                $alias = $c->formapago_alias;
            }
            $monto = $c->monto;
            if($c->clavefm=="28" || $c->clavefm=="04" || $c->clavefm=="29"){
                $monto = $monto + $c->monto;
            }
            $html.="<tr>
                    <td><b>".$alias."</b></td>
                    <td>".$monto."</td>
                </tr>";
        }*/
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"01"); //efectivo
        if(isset($get_corte)){
            $efectivo = $get_corte->monto;
        }
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"04"); //tarjeta dc
        if(isset($get_corte)){
            $tdc = $get_corte->monto;
        }
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"28"); //tarjeta dd
        if(isset($get_corte)){
            $tdd = $get_corte->monto;
        }
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"29"); //tarjeta servicio
        if(isset($get_corte)){
            $tds = $get_corte->monto;
        }
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"02"); //cheque
        if(isset($get_corte)){
            $che = $get_corte->monto;
        }
        $get_corte=$this->Modeloventas->generarCorte($suc,$dia,"03"); //transferencia
        if(isset($get_corte)){
            $transf = $get_corte->monto;
        }

        $tarjetas = number_format($tdc+$tdd+$tds,2);
        $supertot = number_format($efectivo+$tdc+$tdd+$tds+$che+$transf,2);

        echo json_encode(array("efectivo"=>number_format($efectivo,2),"tarjetas"=>$tarjetas,"che"=>number_format($che,2),"transf"=>number_format($transf,2),"supertot"=>$supertot,"moto_devolucion"=>number_format($moto_devolucion,2)));
    }

    /* ***************************************/
    public function cargaimagen(){
        $id_venta=$_POST['id_venta'];
        $DIR_SUC=FCPATH.'uploads/garantia';
        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 8000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload("file")){
            $data = array('error' => $this->upload->display_errors());
            //log_message('error', json_encode($data));                    
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $this->ModeloCatalogos->Insert('evidencia_garantia',array('id_venta'=>$id_venta,"imagen"=>$file_name,"id_personal"=>$this->idpersonal,"fecha_reg"=>$this->fechahoy));
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function deleteFile($id){
        $data = array('estatus' => 0);
        $result = $this->ModeloCatalogos->updateCatalogo("evidencia_garantia",$data,array('id'=>$id));
        echo $result;
    }

    public function getEvidencias(){
        $id_venta = $this->input->post("id_venta");
        $i=0; 
        $getdat=$this->ModeloCatalogos->getselectwheren('evidencia_garantia',array('estatus'=>1,'id_venta'=>$id_venta));
        foreach ($getdat->result() as $k) {
            $img=""; $imgdet=""; $imgp="";
            $i++;
            $img = "<img src='".base_url()."uploads/garantia/".$k->imagen."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '".base_url()."Listaventas/deleteFile/".$k->id."', caption: '".$k->imagen."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/garantia/".$k->imagen."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false";  
            echo '<div class="row_'.$k->id.'">
                <div class="col-md-5">
                    <label class="col-md-12"> Imagen: '.$k->imagen.'</label>
                    <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                    <hr style="background-color: red;">
                </div>
            </div>
            <script> 
                $("#inputFile'.$i.'").fileinput({
                  overwriteInitial: false,
                  showClose: false,
                  showCaption: false,
                  showUploadedThumbs: false,
                  showBrowse: false,
                  removeTitle: "Cancel or reset changes",
                  elErrorContainer: "#kv-avatar-errors-1",
                  msgErrorClass: "alert alert-block alert-danger",
                  defaultPreviewContent: "'.$img.'",
                  layoutTemplates: {main2: "{preview} {remove}"},
                  allowedFileExtensions: ["jpg", "png", "jpeg","webp"],
                  initialPreview: [
                  "'.$imgp.'"
                  ],
                  initialPreviewAsData: '.$typePDF.',
                  initialPreviewFileType: "image",
                  initialPreviewConfig: [
                      '.$imgdet.'
                  ]
                });
          </script>';
        }     
    }

    function mail_solicitudGaran(){ 
        $id = $this->input->post('id');
        $getpers=$this->Modeloventas->getPersonalSucus($id);
        $array_com = array();
        $array_adm = array();
        $suc_solicita=""; $cont_com=0; $cont_adm=0; $folio=""; 
        foreach ($getpers as $p) {
            $suc_solicita=$p->name_suc;
            $folio=$p->folio;
            if ($p->correo_comp!='') {
                array_push($array_com,$p->correo_comp);
                $cont_com++;  
            }if ($p->correo_adm!='') {
                array_push($array_adm,$p->correo_adm);
                $cont_adm++;
            }
        }
  
        $array_com=array_unique($array_com);
        $array_adm=array_unique($array_adm);

        log_message('error','array_com unique: '.json_encode($array_com));
        log_message('error','array_adm unique: '.json_encode($array_adm));

        $asunto='Solicitud de garantía';
        $this->load->library('email');
        //$this->load->library('email', NULL, 'ci_email');

        $config['protocol'] = 'smtp';
        $config["smtp_host"] ='mail.semit.mx'; 
        $config["smtp_user"] = 'noreply@semit.mx';
        $config["smtp_pass"] = 'l)sT^Iwk,e7n';
        $config["smtp_port"] = '465';
        $config["smtp_crypto"] = 'ssl';
        $config['charset'] = 'utf-8'; 
        $config['wordwrap'] = TRUE; 
        $config['validate'] = true;
        $config['mailtype'] = 'html';
         
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@semit.mx','Semit');

        //$this->email->to($correo,'Solicitud de traspaso');
        if($cont_com>0){
            $this->email->to($array_com);
        }
        if($cont_adm>0){
            $this->email->cc($array_adm);
        }
        $this->email->subject($asunto);
        
        $message ="Solicitud de garantía: ".$suc_solicita ." registra solicitud de garantía de la venta con folio ".$folio;
        $this->email->message($message);
        $this->email->send();
        /*if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }*/
    }
}