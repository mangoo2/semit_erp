<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');

    }
	public function index(){
        $this->load->view('login2');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();

        unset($_SESSION['des']);
        $_SESSION['des']=array();
        unset($_SESSION['desp']);
        $_SESSION['desp']=array();
        
        unset($_SESSION['cost']);
        $_SESSION['cost']=array();

        ///////////////////////////////////
        unset($_SESSION['rentr']);
        $_SESSION['rentr']=array();
        unset($_SESSION['cantr']);
        $_SESSION['cantr']=array();

        unset($_SESSION['desr']);
        $_SESSION['desr']=array();
        unset($_SESSION['despr']);
        $_SESSION['despr']=array();
        
        unset($_SESSION['costr']);
        $_SESSION['costr']=array();
        //////////////////////////
        unset($_SESSION['ser']);
        $_SESSION['ser']=array();
        unset($_SESSION['can_ser']);
        $_SESSION['can_ser']=array();
        unset($_SESSION['des_ser']);
        $_SESSION['des_ser']=array();
        unset($_SESSION['precio_ser']);
        $_SESSION['precio_ser']=array();
	}
    
	public function login(){
		$username = $this->input->post('usuario');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        //log_message('error', 'usuario: '.$username);
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasena;
        }
        // Verificamos si las contraseñas son iguales
        //log_message('error', 'contra1: '.$password);
        //log_message('error', 'contra2: '.$contrasena);
        $verificar = password_verify($password,$contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) 
        {
            $data = array(
                        'logeado' => true,
                        'usuarioid' => $respuesta[0]->UsuarioID,
                        'usuario' => $respuesta[0]->nombre,
                        'perfilid'=>$respuesta[0]->perfilId,
                        'idpersonal'=>$respuesta[0]->personalId,
                        'empleado'=>$respuesta[0]->empleado,
                        'perfil'=>$respuesta[0]->perfil,
                        'foto'=>$respuesta[0]->foto,
                        'sucursal'=>$respuesta[0]->sucursal,
                        'succlave'=>$respuesta[0]->succlave,
                        'tipo_venta'=>$respuesta[0]->tipo_venta,
                        'tipo_tecnico'=>$respuesta[0]->tipo_tecnico,
                        'con_insumos'=>$respuesta[0]->con_insumos,
                        'name_suc'=>$respuesta[0]->name_suc,
                    );
            $this->session->set_userdata($data);
            $this->ModeloCatalogos->Insert('historial_session',array('idpersonal'=>$respuesta[0]->personalId));
            $count=1;
        }
        // Devolvemos la respuesta
        echo $count;
	}

	public function logout(){
		$this->session->sess_destroy();
        redirect(base_url(), 'refresh');
	}
}
