<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelblog');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,16);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=16;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('blog/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('blog/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=4;
        $data['btn_active_sub']=16;
        if($id==0){
            $data['id'] = 0;
            $data['titulo']='';
            $data['articulo']='';
            $data['categoria']='';
            $data['resumen']='';
            $data['file']='';
        }else{
            $resul=$this->General_model->getselectwhere('blog','id',$id);
            foreach ($resul as $item) {
                $data['id'] =$item->id;
                $data['titulo']=$item->titulo;
                $data['articulo']=$item->articulo;
                $data['categoria']=$item->categoria;
                $data['resumen']=$item->resumen;
                $data['file']=$item->file;
            }
        }   
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('blog/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('blog/formjs',$data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['file']);

        if($id==0){
            $data['idpersonal']=$this->idpersonal;
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('blog',$data);
        }else{
            $data['idpersonal']=$this->idpersonal;
            $id=$this->General_model->edit_record('id',$id,$data,'blog');
            $id=$id;
        }
        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->Modelblog->get_listado($params);
        $totaldata= $this->Modelblog->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'blog');
    }
    

    public function select_datos_cat_get(){
        $id=$this->input->post('id');
        $html='<label class="form-label">Categoría</label>
                <select class="form-select" name="categoria" id="categoria">
                    <option selected disabled value="0">Selecciona una opción</option>';
                    $result=$this->General_model->getselectwhere_orden_desc('blog_categoria',array('activo'=>1),'id');
                    foreach ($result as $x){
                        $sel="";
                        if($id==$x->id)
                            $sel="selected";
                        $html.='<option '.$sel.' value="'.$x->id.'">'.$x->nombre.'</option>';
                    }    
        $html.='</select>';
        echo $html;
    }



    public function addcategoria(){
        $params = $this->input->post();
        $categoriaId = $params['id'];
        unset($params['id']);
        if ($categoriaId>0) {
            $this->ModeloCatalogos->updateCatalogo('blog_categoria',$params,array('id'=>$categoriaId));
            $id=$categoriaId;
        }else{
            $id=$this->ModeloCatalogos->Insert('blog_categoria',$params);
        }
        echo $id;
    }


    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->Modelblog->get_categoria($params);
        $totaldata= $this->Modelblog->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_categoria(){
        $params = $this->input->post();
        $categoriaId=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('blog_categoria',array('activo'=>0),array('id'=>$categoriaId));
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="blogimg";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('file'=>$newfile);
          $this->General_model->edit_record('id',$id,$array,'blog');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

}