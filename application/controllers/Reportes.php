<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelClientes');
        $this->load->model('Modeloventas');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            
        }else{
            redirect('login'); 
        }
    }

    function index(){
            
    }
    function arqueopreview(){
        //http://localhost/semit_erp/index.php/Reportes/arqueopreview?sucursal=2&folio=&personal=1&conceptos=[{%22clavefm%22:%2201%22,%22bancoid%22:%22null%22,%22monto%22:%223100.00%22},{%22clavefm%22:%2202%22,%22bancoid%22:%221%22,%22monto%22:%22100.00%22},{%22clavefm%22:%2203%22,%22bancoid%22:%221%22,%22monto%22:%22863.00%22},{%22clavefm%22:%2204%22,%22bancoid%22:%222%22,%22monto%22:%22600.00%22},{%22clavefm%22:%2228%22,%22bancoid%22:%221%22,%22monto%22:%22650.00%22}]

        $sucursalid=$_GET['sucursal'];
        $personal=$_GET['personal'];
        $data['folioarqueo']=$_GET['folio'];


        //============================================================================================
        $result_suc =  $this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$sucursalid));
        $result_suc=$result_suc->row();
        $data['sucursal']=$result_suc->name_suc;
        //============================================================================================
        $result_per =  $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$personal));
        $result_per=$result_per->row();
        $data['personal']=$result_per->nombre;
        //============================================================================================
        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $fomapago=$this->ModeloCatalogos->fomapago($item->clavefm);
            $name_ban='';
            if($item->bancoid>0){
               $bancoresult=$this->ModeloCatalogos->getbanco($item->bancoid);
                if($bancoresult->num_rows()>0){
                    $bancoresult=$bancoresult->row();
                    $name_ban=$bancoresult->name_ban;
                } 
            }

            $arrayconceptos[]=array(
                                    'monto'=>$item->monto,
                                    'formapago_alias'=>$fomapago->formapago_alias,
                                    'banco'=>$name_ban
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);
        $data['result_dll']=$arrayconceptos;


        $this->load->view('reportes/arqueopdf',$data);
    }
    function arqueo($id){
        //http://localhost/semit_erp/index.php/Reportes/arqueo/1

        $data['id']=$id;
        
        $result_ar =  $this->ModeloCatalogos->getselectwheren('arqueos',array('id'=>$id));
        $result_ar= $result_ar->row();
        $data['folioarqueo']=$result_ar->folio;
        //============================================================================================
        $result_suc =  $this->ModeloCatalogos->getselectwheren('sucursal',array('id'=>$result_ar->sucursal));
        $result_suc=$result_suc->row();
        $data['sucursal']=$result_suc->name_suc;
        //============================================================================================
        $result_per =  $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$result_ar->personal));
        $result_per=$result_per->row();
        $data['personal']=$result_per->nombre;
        //============================================================================================
        $result_dll =  $this->ModeloCatalogos->info_arqueo_dll($id);
        $data['result_dll']=$result_dll->result();


        $this->load->view('reportes/arqueopdf',$data);
    }
    function cortexz($idarqueo,$view=0){
        if($view==0){
            $nombre_fichero = FCPATH.'uploads/cortesxz/costexz_'.$idarqueo.'.pdf';
            $nombre_fichero_url = base_url().'uploads/cortesxz/costexz_'.$idarqueo.'.pdf?v=2';

            if (file_exists($nombre_fichero)) {
                redirect($nombre_fichero_url);
            }
        }

        $data['idarqueo']=$idarqueo;
        $this->load->view('reportes/cortexz',$data);
    }


    
}
?>
