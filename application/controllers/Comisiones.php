<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comisiones extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloventas');
        
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->fechainicio = date('Y-m-d');
        $this->anioactual = date('Y');
        if ($this->session->userdata('logeado')){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,45);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema');
        }
    }

    public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['perfil']=$this->perfilid;
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $data['get_vendedores']=$this->Modeloventas->get_vendedores();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('comisiones/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('comisiones/indexjs');
    }
    function generarteporte(){
        $params = $this->input->post();
        $suc=$params['suc'];
        $eje=$params['eje'];
        $mes=$params['mes'];
        $w_eje="";
        if($eje>0){
            $w_eje=" AND p.personalId='$eje' ";
        }else{
            //$w_eje=" AND (u.perfilId=3 or u.perfilId=1 or u.perfilId=2 or u.perfilId=7 or u.perfilId=10)";
        }
        if($suc>0){
            $w_suc=" AND u.sucursal='$suc' ";
        }else{
            $w_suc="";
        }
        if($mes>0){
            $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
            $primerDia = $dias['primerDia'];
            $ultimoDia = $dias['ultimoDia'];
            $w_mes=" AND verp.reg>='$primerDia 00:00:00' AND verp.reg<='$ultimoDia 23:59:59'";
            $w_mes2=" AND v_can.delete_reg>='$primerDia' AND v_can.delete_reg<='$ultimoDia'";
        }else{
            $w_mes="";
            $w_mes2="";
        }
        //===============================================
        $row_all=0;
        $html_tr_body=''; $html_tr_body_servs="";
        $html_tr_body_top=''; $html_tr_body_top_serv="";
        $btn_cancelas=""; 
        if($mes>0){
            $subtotal_servs=0; $sub_num_servs=0; $iva_servs=0; $total_servs=0; $comision_tecnico=0; $tipo_can="AND vdcan.tipo = 3";
            $strq0 ="SELECT 
            p.nombre,
            p.personalId,
            u.sucursal,
            suc.name_suc,
            p.comision_tecnico,
            IFNULL((
                SELECT SUM(v_can.subtotal)
                FROM venta_erp AS v_can
                WHERE v_can.sucursal = u.sucursal
                  AND v_can.personalid = p.personalId
                  AND v_can.activo = 0
                  $w_mes2
                  AND EXISTS ( 
                      SELECT 1
                      FROM venta_erp_detalle vdcan
                      WHERE vdcan.idventa = v_can.id
                        AND vdcan.activo = 1
                        AND vdcan.tipo = 3
                  )
            ), 0) AS subtotal_cancel,
            SUM(vd.cantidad) AS sub_num_servs,
            SUM(vd.cantidad * vd.precio_unitario) AS subtotal_servs,
            SUM(CASE 
                WHEN cs.iva = 1 THEN vd.cantidad * vd.precio_unitario * 0.16 
                ELSE 0 
            END) AS iva_servs,
            SUM(CASE 
                WHEN cs.iva = 1 THEN vd.cantidad * vd.precio_unitario * 1.16 
                ELSE vd.cantidad * vd.precio_unitario 
            END) AS total_servs
            FROM usuarios AS u 
            join perfiles pf on pf.perfilId=u.perfilId and pf.tipo_tecnico=1
            INNER JOIN personal AS p ON p.personalId = u.personalId 
            INNER JOIN sucursal AS suc ON suc.id = u.sucursal
            join mtto_externo as mtt on mtt.id_venta!=0 and mtt.activo=1
            join venta_erp as verp on verp.sucursal=u.sucursal /*and verp.personalid=p.personalId */ and verp.id=mtt.id_venta and verp.activo=1 $w_mes
            JOIN venta_erp_detalle vd ON vd.idventa = verp.id 
                AND vd.activo = 1 
                AND vd.tipo = 3
            LEFT JOIN cat_servicios cs ON cs.id = vd.idproducto
            WHERE p.estatus=1 and u.estatus=1 and pf.tipo_tecnico=1 $w_eje $w_suc
            GROUP BY p.personalId, u.sucursal
            ORDER BY sub_num_servs DESC";

            $row=0;
            $query0 = $this->db->query($strq0);
            foreach ($query0->result() as $s) {
                $row_all++;
                $subtotal_servs=$s->subtotal_servs;
                $sub_num_servs = $s->sub_num_servs; //ACA ME QUEDO --  OJOOOOOO --
                $iva_servs=$s->iva_servs;
                $total_servs=$s->total_servs;
                $comision_tecnico=$s->comision_tecnico;
                $row++;
                $subtotal_general = $subtotal_servs; 
                if($subtotal_general>0){}else{
                    $subtotal_general=0;
                }
                if($subtotal_general>0 && $comision_tecnico>0){
                    //$montocomision=($subtotal_general*$comision_tecnico)/100;
                    $montocomision = $s->sub_num_servs * $comision_tecnico;
                }else{
                    $montocomision=0;
                }
                //$get_subcan=$this->getCancelVenta($s->sucursal,$s->personalId,$w_mes2,$tipo_can);
                /*if($eje==$s->personalId){
                    $montos_meses='';
                    for ($mes_p = 1; $mes_p <= 12; $mes_p++) {
                        $dias_p = $this->obtenerPrimerYUltimoDia(date('Y'), $mes_p);
                        $primerDia = $dias_p['primerDia'];
                        $ultimoDia = $dias_p['ultimoDia'];
                        $w_mes=" and verp.reg>='$primerDia 00:00:00' and verp.reg<='$ultimoDia 23:59:59'";
                        
                        $strq_p = "SELECT 
                            p.nombre,
                            p.personalId,
                            u.sucursal,
                            suc.name_suc,
                            SUM(vd.cantidad * vd.precio_unitario) AS subtotal_servs,
                            (select IFNULL(sum(v_can.subtotal),0) as subtotal_cancel from venta_erp AS v_can 
                                join venta_erp_detalle vdcan ON vdcan.idventa = v_can.id AND vdcan.activo = 1 AND (vdcan.tipo = 2 OR vdcan.tipo = 3)
                                where v_can.sucursal=u.sucursal AND v_can.personalid=p.personalId AND v_can.activo=0 $w_mes2) as subtotal_cancel,
                            SUM(CASE 
                                WHEN cs.iva = 1 THEN vd.cantidad * vd.precio_unitario * 0.16 
                                ELSE 0 
                            END) AS iva_servs,
                            SUM(CASE 
                                WHEN cs.iva > 0 THEN vd.cantidad * vd.precio_unitario * 1.16 
                                ELSE vd.cantidad * vd.precio_unitario 
                            END) AS total_servs
                            FROM 
                                usuarios AS u 
                            INNER JOIN 
                                personal AS p ON p.personalId = u.personalId 
                            INNER JOIN 
                                sucursal AS suc ON suc.id = u.sucursal
                            left join venta_erp as verp on verp.sucursal=u.sucursal and verp.personalid=p.personalId and verp.activo=1 $w_mes
                            INNER JOIN 
                                venta_erp_detalle vd ON vd.idventa = verp.id 
                                AND vd.activo = 1 
                                AND (vd.tipo = 2 OR vd.tipo = 3)
                            LEFT JOIN cat_servicios cs ON cs.id = vd.idproducto
                            WHERE p.estatus=1 and u.estatus=1  $w_suc $w_eje
                            GROUP BY p.personalId, u.sucursal
                            ORDER BY subtotal_servs DESC";
                            $query_p = $this->db->query($strq_p);
                            foreach ($query_p->result() as $s) {
                                if($s->subtotal_servs>0){
                                    $subtotal_general_m = $s->subtotal_servs; 
                                }else{
                                    $subtotal_general_m=0;
                                }
                                $ml=$this->mesletra($mes_p);
                                if($s->subtotal_cancel>0){
                                    $btn_cancelas=number_format($s->subtotal_cancel,2,".",",") .'<button type="button" onclick="detCancela('.$s->personalId.',1)"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                                }else{
                                    $btn_cancelas="";
                                }
                                $montos_meses.= $btn_cancelas.' <span class="info_mont_eje" data-mes="'.$ml.'" data-monto="'.$subtotal_general_m.'"></span>';
                            }
                        //echo $i . "<br>";
                    }
                }else{ */
                    if($s->subtotal_cancel>0){
                        $btn_cancelas=number_format($s->subtotal_cancel,2,".",",") .' <br><button class="btn btn-primary" type="button" onclick="detCancela('.$s->personalId.',1)"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                    }else{
                        $btn_cancelas="";
                    }
                    $montos_meses=$btn_cancelas;
                //}
                $personalId=$s->personalId;
                ${'comision_vendedor_serv_'.$personalId}=$montocomision;
                ${'id_personal_'.$personalId}=$personalId;
                $html_tr_serv='';
                $html_tr_serv.='<tr>';
                    $html_tr_serv.='<td class="info_tr" ';
                        $html_tr_serv.=' data-subtotalg="'.$subtotal_general.'" ';
                        $html_tr_serv.=' data-nombre="'.$s->nombre.'" ';
                    $html_tr_serv.='>'.$s->nombre.'</td>';
                    
                    $html_tr_serv.='<td>$'.number_format(($subtotal_general),2,'.',',').'</td>';
                    $html_tr_serv.='<td>$'.number_format(($s->iva_servs),2,'.',',').'</td>';
                    $html_tr_serv.='<td>$'.number_format(($s->total_servs),2,'.',',').'</td>';
                    $html_tr_serv.='<td>'.$s->name_suc.'</td>';
                    $html_tr_serv.='<td>'.$comision_tecnico.'</td>';
                    $html_tr_serv.='<td>'.$sub_num_servs.'</td>';
                    $html_tr_serv.='<td>$'.number_format(($montocomision),2,'.',',').'</td>';
                    $html_tr_serv.='<td>'.$btn_cancelas.'</td>';
                $html_tr_serv.='</tr>';

                if($eje>0){
                    //log_message('error','html_tr_serv: '.$html_tr_serv);
                    if($eje==$s->personalId){
                        $html_tr_body_servs.=$html_tr_serv; 
                    }
                }else{
                   $html_tr_body_servs.=$html_tr_serv; 
                }
    
                /*${'tr_vendedor_'.$personalId}='';
                ${'subtotal_vendedor_serv_'.$personalId}=0;
                ${'iva_vendedor_serv_'.$personalId}=0;
                ${'total_vendedor_serv_'.$personalId}=0;
                ${'comision_vendedor_serv_'.$personalId}=0;
                if($row<=10 and $subtotal_general>0){
                    //$html_tr_body_top_serv.=$html_tr_serv;
                    ${'id_personal_serv_'.$personalId}=$personalId;
                    ${'subtotal_vendedor_serv_'.$personalId}+=$subtotal_general;
                    ${'iva_vendedor_serv_'.$personalId}+=$s->iva_servs;
                    ${'total_vendedor_serv_'.$personalId}+=$s->total_servs;
                    ${'comision_vendedor_serv_'.$personalId}=$montocomision;
                    ${'tr_vendedor_'.$personalId}.='<tr>';
                        ${'tr_vendedor_'.$personalId}.='<td class="info_tr" data-subtotalg="'.$subtotal_general.'" data-nombre="'.$s->nombre.'">'.$s->nombre.'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>$'.number_format(($subtotal_general),2,'.',',').'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>$'.number_format(($s->iva_servs),2,'.',',').'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>$'.number_format(($s->total_servs),2,'.',',').'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>'.$s->name_suc.'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>% '.$s->comision_tecnico.'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>$'.number_format(($montocomision),2,'.',',').'</td>';
                        ${'tr_vendedor_'.$personalId}.='<td>'.$montos_meses.'</td>';
                    ${'tr_vendedor_'.$personalId}.='</tr>';
                    //log_message('error','tr_vendedor_ servicios: '.${'tr_vendedor_'.$personalId});
                }*/
            }
            ///////////////////////////////////////////////////////////
            $suc=$params['suc'];
            $eje=$params['eje'];
            $mes=$params['mes'];
            $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
            $primerDia = $dias['primerDia'];
            $ultimoDia = $dias['ultimoDia'];
            $w_mes=" AND verp.reg>='$primerDia 00:00:00' AND verp.reg<='$ultimoDia 23:59:59'";
            $tipo_can=" AND (vdcan.tipo = 0 OR vdcan.tipo = 1)";
            /*$strq = "SELECT DISTINCT(vd2.id) as id_ventad,
                p.nombre,
                p.personalId,
                p.comision,
                u.sucursal,
                suc.name_suc,
                ps.incluye_iva,
                sum(verp.subtotal) subtotal_general,
                IFNULL((
                    SELECT SUM(v_can.subtotal)  -- Calculamos solo el subtotal de las ventas canceladas
                    FROM venta_erp AS v_can
                    WHERE v_can.sucursal = u.sucursal
                      AND v_can.personalid = p.personalId
                      AND v_can.activo = 0  -- Solo ventas canceladas
                      $w_mes2
                      AND EXISTS (  -- Solo consideramos las ventas que tienen detalles de tipo 0 o 1
                          SELECT 1
                          FROM venta_erp_detalle vdcan
                          WHERE vdcan.idventa = v_can.id
                            AND vdcan.activo = 1
                            AND (vdcan.tipo = 0 OR vdcan.tipo = 1)  -- Solo tipo 0 o 1
                      )
                ), 0) AS subtotal_cancel,
                IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_prods,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 and vd.tipo=0 THEN vd.cantidad * vd.precio_unitario * 0.16
                    ELSE 0
                END),0) AS iva_prods,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
                    ELSE vd.cantidad * vd.precio_unitario
                END),0) AS total_prods,

                IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal_recarga,
                0 AS iva_recarga,
                IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total_recarga
                 
                FROM usuarios AS u 
                INNER JOIN personal AS p on p.personalId=u.personalId 
                INNER JOIN sucursal AS suc on suc.id=u.sucursal
                left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                left join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
                left join productos prod on prod.id=vd.idproducto
                left join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal

                left join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
                WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
                group by p.personalId,u.sucursal
                ORDER BY (IFNULL(SUM(vd.cantidad * vd.precio_unitario), 0) ) DESC "; */
            
            $strq ="SELECT nombre, personalId, comision, sucursal, name_suc, incluye_iva, SUM(subtotal_general) AS subtotal_general, sum(subtotal_cancel) as subtotal_cancel, SUM(subtotal_prods) AS subtotal_prods, SUM(iva_prods) AS iva_prods, SUM(total_prods) AS total_prods, sum(subtotal_recarga) as subtotal_recarga, sum(iva_recarga) as iva_recarga, sum(total_recarga) as total_recarga 
            FROM(
            SELECT
                p.nombre,
                p.personalId,
                p.comision,
                u.sucursal,
                suc.name_suc,
                /*ps.incluye_iva, */
                (select ps.incluye_iva from productos_sucursales ps where ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal and ps.activo=1) as incluye_iva,
                sum(verp.subtotal) subtotal_general,
                IFNULL((
                    SELECT SUM(v_can.subtotal)  -- Calculamos solo el subtotal de las ventas canceladas
                    FROM venta_erp AS v_can
                    WHERE v_can.sucursal = u.sucursal
                        AND v_can.personalid = p.personalId
                        AND v_can.activo = 0  -- Solo ventas canceladas
                        $w_mes2
                        AND EXISTS (  -- Solo considera las ventas que tienen detalles de tipo 0 o 1
                          SELECT 1
                          FROM venta_erp_detalle vdcan
                          WHERE vdcan.idventa = v_can.id
                            AND vdcan.activo = 1
                            AND (vdcan.tipo = 0 or vdcan.tipo=1)  -- Solo tipo 0 o 1
                        )
                ), 0) AS subtotal_cancel,
                IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_prods,
                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 and vd.tipo=0 THEN vd.cantidad * vd.precio_unitario * 0.16
                    ELSE 0
                END),0) AS iva_prods,
                
                /* IFNULL((
                    SELECT SUM(CASE
                        WHEN ps.iva > 0 AND ps.incluye_iva = 1 AND vd.tipo = 0 THEN vd.cantidad * vd.precio_unitario * 0.16
                        ELSE 0
                    END)
                    FROM productos_sucursales ps
                    WHERE ps.productoid = vd.idproducto 
                    AND ps.sucursalid = verp.sucursal 
                    AND ps.activo = 1
                ), 0) AS iva_prods, */

                IFNULL(SUM(CASE
                    WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
                    ELSE vd.cantidad * vd.precio_unitario
                END),0) AS total_prods, 
                
                0 as subtotal_recarga,
                0 as iva_recarga,
                0 as total_recarga
                FROM usuarios AS u 
                INNER JOIN personal AS p on p.personalId=u.personalId 
                INNER JOIN sucursal AS suc on suc.id=u.sucursal
                left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                left join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
                /*left join productos prod on prod.id=vd.idproducto */
                join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal and ps.activo=1 
                WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
                group by p.personalId,u.sucursal
            union 
                SELECT
                    p.nombre,
                    p.personalId,
                    p.comision,
                    u.sucursal,
                    suc.name_suc,
                    0 as incluye_iva,
                    0 as subtotal_general,
                    0 AS subtotal_cancel,
                    0 as subtotal_prods,
                    0 as iva_prods,
                    0 as total_prods,
                    IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal_recarga,
                    0 AS iva_recarga,
                    IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total_recarga
                    FROM usuarios AS u 
                    INNER JOIN personal AS p on p.personalId=u.personalId 
                    INNER JOIN sucursal AS suc on suc.id=u.sucursal
                    left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                    left join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
                    WHERE p.estatus=1 AND u.estatus=1 $w_eje $w_suc
                    group by p.personalId,u.sucursal
            ) as datos 
            GROUP BY personalId, sucursal
            ORDER BY (subtotal_prods + subtotal_recarga) DESC";
            $query = $this->db->query($strq);
            //log_message('error','strq: '.$strq);
            $row=0;
            foreach ($query->result() as $item) {
                $row++;
                $row_all++;
                $subtotal_general = $item->subtotal_prods+$item->subtotal_recarga; 
                if($subtotal_general>0){}else{
                    $subtotal_general=0;
                }
                if($subtotal_general>0 && $item->comision>0){
                    $montocomision=($subtotal_general*$item->comision)/100;
                }else{
                    $montocomision=0;
                }
                //canceladas de productos y recargas
                $get_subcan=$this->getCancelVenta($item->sucursal,$item->personalId,$w_mes2,$tipo_can);
                /*if($eje==$item->personalId){
                    $montos_meses='';
                    for ($mes_p = 1; $mes_p <= 12; $mes_p++) {
                        $dias_p = $this->obtenerPrimerYUltimoDia(date('Y'), $mes_p);
                        $primerDia = $dias_p['primerDia'];
                        $ultimoDia = $dias_p['ultimoDia'];
                        $w_mes=" and verp.reg>='$primerDia 00:00:00' and verp.reg<='$ultimoDia 23:59:59'";
                        $strq_p = "SELECT 
                            p.nombre,
                            p.personalId,
                            p.comision,
                            u.sucursal,
                            suc.name_suc,
                            ps.incluye_iva,
                            sum(verp.subtotal) subtotal_general,
                            (select sum(v_can.subtotal) as subtotal_cancel from venta_erp AS v_can 
                                join venta_erp_detalle vdcan ON vdcan.idventa = v_can.id AND vdcan.activo = 1 AND (vdcan.tipo = 0 OR vdcan.tipo = 1)
                                where v_can.sucursal=u.sucursal AND v_can.personalid=p.personalId AND v_can.activo=0 $w_mes2) as subtotal_cancel,
                            IFNULL(SUM(vd.cantidad * vd.precio_unitario),0) AS subtotal_prods,
                            IFNULL(SUM(CASE
                                WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 0.16
                                ELSE 0
                            END),0) AS iva_prods,
                            IFNULL(SUM(CASE
                                WHEN ps.iva > 0 and ps.incluye_iva=1 THEN vd.cantidad * vd.precio_unitario * 1.16
                                ELSE vd.cantidad * vd.precio_unitario
                            END),0) AS total_prods,

                            IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS subtotal_recarga,
                            0 AS iva_recarga,
                            IFNULL(SUM(vd2.cantidad * vd2.precio_unitario),0) AS total_recarga
                            FROM usuarios AS u 
                            INNER JOIN personal AS p on p.personalId=u.personalId 
                            INNER JOIN sucursal AS suc on suc.id=u.sucursal
                            left join venta_erp AS verp on verp.sucursal=u.sucursal AND verp.personalid=p.personalId AND verp.activo=1 $w_mes
                            left join venta_erp_detalle vd on vd.idventa=verp.id AND vd.tipo = 0 AND vd.activo=1
                            left join productos prod on prod.id=vd.idproducto
                            left join productos_sucursales ps on ps.productoid=vd.idproducto AND ps.sucursalid=verp.sucursal

                            left join venta_erp_detalle vd2 on vd2.idventa=verp.id AND vd2.tipo = 1 AND vd2.activo=1
                            WHERE p.estatus=1 AND u.estatus=1  $w_suc $w_eje
                            group by p.personalId,u.sucursal
                            ORDER BY (IFNULL(SUM(vd.cantidad * vd.precio_unitario), 0) + IFNULL(SUM(vd2.cantidad * vd2.precio_unitario), 0)) DESC";
                            $query_p = $this->db->query($strq_p);
                            foreach ($query_p->result() as $itemp) {
                                if($itemp->subtotal_prods>0 || $itemp->subtotal_recarga>0){
                                    $subtotal_general_m = $itemp->subtotal_prods+$item->subtotal_recarga; 
                                }else{
                                    $subtotal_general_m=0;
                                }
                                if($itemp->subtotal_cancel>0){
                                    $btn_cancelas=number_format($itemp->subtotal_cancel,2,".",",") .'<button type="button" onclick="detCancela('.$itemp->personalId.',2)"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                                }else{
                                    $btn_cancelas="";
                                }
                                $ml=$this->mesletra($mes_p);
                                $montos_meses.= $btn_cancelas.' <span class="info_mont_eje" data-mes="'.$ml.'" data-monto="'.$subtotal_general_m.'"></span>';
                            }
                        //echo $i . "<br>";
                    }
                }else{ */
                    if($get_subcan>0){
                        $btn_cancelas=number_format($get_subcan,2,".",",") .' <br><button class="btn btn-primary" type="button" onclick="detCancela('.$item->personalId.',2)"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                    }else{
                        $btn_cancelas="";
                    }
                    $montos_meses=number_format($get_subcan,2,".",",");
                //}
                $html_tr='';
                $html_tr.='<tr>';
                    $html_tr.='<td class="info_tr"' ;
                        $html_tr.=' data-subtotalg="'.$subtotal_general.'" ';
                        $html_tr.=' data-nombre="'.$item->nombre.'" ';
                    $html_tr.='>'.$item->nombre.'</td>';
                    
                    $html_tr.='<td>$'.number_format(($subtotal_general),2,'.',',').'</td>';
                    $html_tr.='<td>$'.number_format(($item->iva_prods+$item->iva_recarga),2,'.',',').'</td>';
                    $html_tr.='<td>$'.number_format(($item->total_prods+$item->total_recarga),2,'.',',').'</td>';
                    $html_tr.='<td>'.$item->name_suc.'</td>';
                    $html_tr.='<td>% '.$item->comision.'</td>';
                    $html_tr.='<td>$'.number_format(($montocomision),2,'.',',').'</td>';
                    $html_tr.='<td>'.$btn_cancelas.'</td>';
                $html_tr.='</tr>';
                ${'comision_vendedor_'.$item->personalId}=$montocomision;
                ${'id_personal_'.$item->personalId}=$item->personalId;

                if($eje>0){
                    //log_message('error','html_tr: '.$html_tr);
                    if($eje==$item->personalId){
                        $html_tr_body.=$html_tr; 
                    }
                }else{
                   $html_tr_body.=$html_tr; 
                }
                
                /*$personalId=$item->personalId;
                if($row<=10 and $subtotal_general>0){
                    //$html_tr_body_top.=$html_tr;
                    ${'id_personal_'.$personalId}=$personalId;
                    ${'subtotal_vendedor_'.$personalId}=0;
                    ${'iva_vendedor_'.$personalId}=0;
                    ${'total_vendedor_'.$personalId}=0;
                    ${'comision_vendedor_'.$personalId}=0;                   
                    //log_message('error','id_personal_: '.$personalId);
                    //log_message('error','id_personal_dinamica: '.${'id_personal_'.$personalId});
                    //log_message('error','nombre: '.$item->nombre);
                    if (isset(${'id_personal_serv_'.$personalId}) && ${'id_personal_'.$personalId} == ${'id_personal_serv_'.$personalId}) {
                        log_message('error','id_personal_serv_ dinamica: '.${'id_personal_serv_'.$personalId});
                        ${'subtotal_vendedor_'.$personalId}=$subtotal_general;
                        ${'iva_vendedor_'.$personalId}=($item->iva_prods+$item->iva_recarga);
                        ${'total_vendedor_'.$personalId}=($item->total_prods+$item->total_recarga);
                        ${'comision_vendedor_'.$personalId}=$montocomision;
                    }if(!isset(${'id_personal_serv_'.$personalId})){
                        log_message('error','id_personal_distinta: '.$personalId);

                        ${'tr_vendedor_'.$personalId}="";
                        ${'tr_vendedor_'.$personalId} .= '<tr>';
                        ${'tr_vendedor_'.$personalId} .= '<td class="info_tr" data-subtotalg="'.$subtotal_general.'" data-nombre="'.$item->nombre.'">'.$item->nombre.'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>$'.number_format(($subtotal_general), 2, '.', ',').'</td>';   
                        ${'tr_vendedor_'.$personalId} .= '<td>$'.number_format(($item->iva_prods+$item->iva_recarga), 2, '.', ',').'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>$'.number_format(($item->total_prods+$item->total_recarga), 2, '.', ',').'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>'.$item->name_suc.'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>% '.$item->comision.'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>$'.number_format(($montocomision), 2, '.', ',').'</td>';
                        ${'tr_vendedor_'.$personalId} .= '<td>'.$montos_meses.'</td>';
                        ${'tr_vendedor_'.$personalId} .= '</tr>';
                    }
                    //log_message('error','tr_vendedor_: '.${'tr_vendedor_'.$personalId});
                }*/
            }
        }
        //===============================================
        $html='<h3 style="color: #1770aa;">Comisiones de productos y recargas</h3>';
        $html.='<table class="table table-sm" id="table_datos">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<th>Vendedor</th>';
                    $html.='<th>Subtotal</th>';
                    $html.='<th>IVA</th>';
                    $html.='<th>Total</th>';
                    $html.='<th>Sucursal</th>';
                    $html.='<th>% de comisión</th>';
                    $html.='<th>Total comisión</th>';
                    $html.='<th>Descuento</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody class="table_datos_tb">';
                        $html.=$html_tr_body;
            $html.='</tbody>';
        $html.='</table>';

        $html_serv='<h3 style="color: #1770aa;">Comisiones por servicios de mtto.</h3>';
        $html_serv.='<table class="table table-sm" id="table_datos_servs">';
            $html_serv.='<thead>';
                $html_serv.='<tr>';
                    $html_serv.='<th>Técnico</th>';
                    $html_serv.='<th>Subtotal</th>';
                    $html_serv.='<th>IVA</th>';
                    $html_serv.='<th>Total</th>';
                    $html_serv.='<th>Sucursal</th>';
                    $html_serv.='<th>$ de comisión</th>';
                    $html_serv.='<th># Servicios</th>';
                    $html_serv.='<th>Total comisión</th>';
                    $html_serv.='<th>Descuento</th>';
                $html_serv.='</tr>';
            $html_serv.='</thead>';
            $html_serv.='<tbody class="table_datos_tb">';
                        $html_serv.=$html_tr_body_servs;
            $html_serv.='</tbody>';
        $html_serv.='</table>';

        $html_top="";
        if($row_all>0){
            $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
            $primerDia = $dias['primerDia'];
            $ultimoDia = $dias['ultimoDia'];
            $w_mes=" AND verp.reg>='$primerDia 00:00:00' AND verp.reg<='$ultimoDia 23:59:59'";
            //agregar where por si es un solo empleado
            if($eje>0){
                $w_eje=" AND p.personalId='$eje' ";
            }else{
                //$w_eje=" AND (u.perfilId=3 OR u.perfilId=1 OR u.perfilId=2 or u.perfilId=7 or u.perfilId=10)";
                $w_eje="";
            }
            if($suc>0){
                $w_suc=" AND u.sucursal='$suc' ";
            }else{
                $w_suc="";
            }

            //LIMITAR A 10 VENDEDORES TOP /-/ ------ OJO            
            $html_top.='<div class="table_datos_top"><table class="table table-sm" id="table_datos_top">';
                $html_top.='<thead>';
                    $html_top.='<tr>';
                        $html_top.='<th>Vendedor</th>';
                        $html_top.='<th>Subtotal</th>';
                        $html_top.='<th>IVA</th>';
                        $html_top.='<th>Total</th>';
                        $html_top.='<th>Sucursal</th>';
                        $html_top.='<th>Total comisión</th>';
                        $html_top.='<th></th>';
                    $html_top.='</tr>';
                $html_top.='</thead>';
                $html_top.='<tbody class="table_datos_tb">';

            $getlist = $this->Modeloventas->getAllVendedoresTop($w_mes,$w_eje,$w_suc,10);
            foreach ($getlist as $s) {
                $subtotal_general = $s->subtotal; 
                if($subtotal_general>0){}else{
                    $subtotal_general=0;
                }
                $montos_meses="";
                /*if($subtotal_general>0 && $s->tipo<2 && $s->comision>0){
                    $montocomision=($subtotal_general*$s->comision)/100;
                }if($subtotal_general>0 && $s->tipo>1 && $s->comision>0){
                    $montocomision=($subtotal_general*$s->comision)/100;
                }else{
                    $montocomision=0;
                }*/
                $montocomision=0;
                if(isset(${'id_personal_'.$s->personalId}) && $s->personalId==${'id_personal_'.$s->personalId} /* && $s->tipo_tecnico==1 */){
                    //$montocomision=${'comision_vendedor_'.$s->personalId}+${'comision_vendedor_serv_'.$s->personalId};
                    $montocomision=${'comision_vendedor_'.$s->personalId};
                }
                if($s->subtotal>0){
                    $subtotal_general_m = $s->subtotal;
                }else{
                    $subtotal_general_m=0;
                }
                $ml=$this->mesletra($mes);
                $montos_meses.='<span class="info_mont_eje_all" data-mes="'.$ml.'" data-monto="'.$subtotal_general_m.'"></span>';

                $html_top .= '<tr>';
                $html_top .= '<td class="info_tr_final" data-subtotalg="'.$subtotal_general.'" data-nombre="'.$s->nombre.'">'.$s->nombre.'</td>';
                $html_top .= '<td>$'.number_format(($subtotal_general), 2, '.', ',').'</td>';   
                $html_top .= '<td>$'.number_format(($s->iva), 2, '.', ',').'</td>';
                $html_top .= '<td>$'.number_format(($s->total), 2, '.', ',').'</td>';
                $html_top .= '<td>'.$s->name_suc.'</td>';
                $html_top .= '<td>$'.number_format(($montocomision), 2, '.', ',').'</td>';
                $html_top .= '<td>'.$montos_meses.'</td>';
                $html_top .= '</tr>';

            }                
                $html_top.='</tbody>';
            $html_top.='</table></div>';
        }

        echo json_encode(array("html"=>$html,"html_top"=>$html_top,"html_serv"=>$html_serv));
    }

    public function getCancelVenta($suc,$per,$w_mes2,$tipo){
        $strq="SELECT IFNULL(SUM(v_can.subtotal), 0) AS subtotal_cancel
        FROM venta_erp AS v_can
        WHERE v_can.sucursal = $suc
            AND v_can.personalid = $per
            AND v_can.activo = 0
            $w_mes2
            AND EXISTS (  
                SELECT 1
                FROM venta_erp_detalle vdcan
                WHERE vdcan.idventa = v_can.id
                AND vdcan.activo = 1
                  $tipo
            );";
        $query = $this->db->query($strq);
        $get = $query->row();
        return $get->subtotal_cancel;
    }

    function obtenerPrimerYUltimoDia($anio, $mes) {
        // Asegurarse de que el mes esté en formato de dos dígitos
        $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);

        // Crear el objeto DateTime para el primer día del mes
        $primerDia = new DateTime("$anio-$mes-01");
        
        // Clonar el objeto DateTime y moverlo al último día del mes
        $ultimoDia = clone $primerDia;
        $ultimoDia->modify('last day of this month');

        return [
            'primerDia' => $primerDia->format('Y-m-d'),
            'ultimoDia' => $ultimoDia->format('Y-m-d')
        ];
    }
    function mesletra($num){
        switch ($num) {
            case 1:
                $mes_l='Enero';
                break;
            case 2:
                $mes_l='Febrero';
                break;
            case 3:
                $mes_l='Marzo';
                break;
            case 4:
                $mes_l='Abril';
                break;
            case 5:
                $mes_l='Mayo';
                break;
            case 6:
                $mes_l='Junio';
                break;
            case 7:
                $mes_l='Julio';
                break;
            case 8:
                $mes_l='Agosto';
                break;
            case 9:
                $mes_l='Septiembre';
                break;
            case 10:
                $mes_l='Octubre';
                break;
            case 11:
                $mes_l='Noviembre';
                break;
            case 12:
                $mes_l='Diciembre';
                break;
            
            default:
                $mes_l='';
                break;
        }
        return $mes_l;
    }
    function consu_vendedores(){
        $params = $this->input->post();
        $suc = $params['suc'];

        $strq = "SELECT p.nombre,p.personalId
                FROM usuarios as u 
                INNER JOIN personal as p on p.personalId=u.personalId 
                WHERE u.sucursal='$suc' and (u.perfilId=2  or u.perfilId=3 or u.perfilId=7 or u.perfilId=10) AND p.estatus=1 and u.estatus=1";
        $query = $this->db->query($strq);
        $html='<option selected=""  value="0">Seleccionar vendedor</option>';
        foreach ($query->result() as $item) {
            $html.='<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
        }
        echo $html;
    }

    function getCanceladas(){
        $idp=$this->input->post("idp");
        $suc=$this->input->post("suc");
        $mes=$this->input->post("mes");
        $tipo=$this->input->post("tipo");
        //log_message('error','idp: '.$idp);
        $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
        $primerDia = $dias['primerDia'];
        $ultimoDia = $dias['ultimoDia'];
        $w_mes="v.reg>='$primerDia 00:00:00' AND v.reg<='$ultimoDia 23:59:59'";
        $getv=$this->Modeloventas->getVentasCancel($idp,$suc,$w_mes,$tipo);
        $html="";
        $html.='<table class="table table-sm" id="table_datos_detalle">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<th>Folio</th>';
                    $html.='<th>Subtotal</th>';
                    $html.='<th>IVA</th>';
                    $html.='<th>Total</th>';
                    $html.='<th>Fecha Venta</th>';
                    //$html.='<th>Monto Cancelado</th>';
                    $html.='<th>Fecha Cancelado</th>';
                    $html.='<th>Motivo</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody class="body_datos_det">';
        $supertot=0;
        foreach ($getv as $c) {
            $html .= '<tr>';
            $html .= '<td>'.$c->folio.'</td>';
            $html .= '<td>$'.number_format(($c->subtotal), 2, '.', ',').'</td>'; 
            $html .= '<td>$'.number_format(($c->iva), 2, '.', ',').'</td>'; 
            $html .= '<td>$'.number_format(($c->total), 2, '.', ',').'</td>';   
            $html .= '<td>'.date("d-m-Y H:i", strtotime($c->reg)).'</td>';
            //$html .= '<td>$'.number_format(($c->subtotal_cancel), 2, '.', ',').'</td>'; 
            $html .= '<td>'.date("d-m-Y", strtotime($c->delete_reg)).'</td>';
            $html .= '<td>'.$c->delete_mot.'</td>';
            $html .= '</tr>';
            $supertot=$supertot+$c->subtotal;
        }                
        $html.='</tbody>
            <tfoot>
                <tr>
                    <td><b>Total: </b></td>
                    <td>$'.number_format(($supertot), 2, '.', ',').'</td>
                    <td colspan="7"></td>
                </tr>
            </tfoot>';
        $html.='</table>';
        echo $html;
    }

    function getProdsVendidos(){
        $idp=$this->input->post("eje");
        $suc=$this->input->post("suc");
        $mes=$this->input->post("mes");
        $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
        
        
        $primerDia = $dias['primerDia'];
        $ultimoDia = $dias['ultimoDia'];
        //log_message('error','primerDia: '.$primerDia);
        $w_mes="v.reg>='$primerDia 00:00:00' AND v.reg<='$ultimoDia 23:59:59'";

        $getv=$this->Modeloventas->getVentasProds($idp,$suc,$w_mes);
        $html="";
        $html.='<table class="table table-sm" id="table_prods_detalle">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<th>Código</th>';
                    $html.='<th>Nombre</th>';
                    $html.='<th>Cantidad vendida</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody class="body_prods_det">';
        $supertot=0;
        foreach ($getv as $c) {
            $html .= '<tr>';
            $html .= '<td>'.$c->codigo.'</td>';
            $html .= '<td>'.$c->nombre.'</td>';
            $html .= '<td>'.$c->sub_vendido.'</td>';
            $html .= '</tr>';
            $supertot=$supertot+$c->sub_vendido;
        }                
        $html.='</tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td><b>Total vendidos: </b></td>
                    <td>'.$supertot.'</td>
                </tr>
            </tfoot>';
        $html.='</table>';
        echo $html;
    }
    
    function exportListComisiones($suc,$idp,$mes){
        if($idp>0){
            $w_eje="AND p.personalId='$idp' ";
        }else{
            //$w_eje=" AND (u.perfilId=3 OR u.perfilId=1 OR u.perfilId=2 or u.perfilId=7 or u.perfilId=10)";
            $w_eje="";
        }
        if($suc>0){
            $w_suc="AND u.sucursal='$suc' ";
        }else{
            $w_suc="";
        }
        $data["mes"]=$mes;
        $dias = $this->obtenerPrimerYUltimoDia(date('Y'), $mes);
        $primerDia = $dias['primerDia'];
        $ultimoDia = $dias['ultimoDia'];
        $w_mes=" AND verp.reg>='$primerDia 00:00:00' AND verp.reg<='$ultimoDia 23:59:59'";

        $data["dets"]=$this->Modeloventas->getVentasComisServs($w_eje,$w_suc,$w_mes);
        $data["detsp"]=$this->Modeloventas->getVentasComisProds($w_eje,$w_suc,$w_mes);
        $data["all"] = $this->Modeloventas->getAllVendedoresTop($w_mes,$w_eje,$w_suc,0);
        $this->load->view('reportes/listaComisiones',$data);
    }
}