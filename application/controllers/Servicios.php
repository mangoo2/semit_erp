<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Servicios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');        
        $this->load->model('ModelServicios');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,36);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=36;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('servicios/index');
        $this->load->view('templates/footer');
        $this->load->view('servicios/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=36;
        if($id==0){
            $data['id']=0;
            $data['clave']='';
            $data['descripcion']='';
            $data['perido']=0;
            $data['dia1']='';
            $data['precios1']='';
            $data['depositogarantia1']='';
            $data['check1']=0;
            $data['dias15']='';
            $data['precios15']='';
            $data['depositogarantia15']='';
            $data['check15']=0;
            $data['dias30']='';
            $data['precios30']='';
            $data['depositogarantia30']='';
            $data['check30']=0;
        }else{
            $resul=$this->General_model->getselectwhere('servicios','id',$id);
            foreach ($resul as $item){
                $data['id']=$item->id;
                $data['clave']=$item->clave;
                $data['descripcion']=$item->descripcion;
                $data['perido']=$item->perido;
                $data['dia1']=$item->dia1;
                $data['precios1']=$item->precios1;
                $data['depositogarantia1']=$item->depositogarantia1;
                $data['check1']=$item->check1;
                $data['dias15']=$item->dias15;
                $data['precios15']=$item->precios15;
                $data['depositogarantia15']=$item->depositogarantia15;
                $data['check15']=$item->check15;
                $data['dias30']=$item->dias30;
                $data['precios30']=$item->precios30;
                $data['depositogarantia30']=$item->depositogarantia30;
                $data['check30']=$item->check30;
            }
        }      

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('servicios/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('servicios/formjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelServicios->get_list($params);
        $totaldata= $this->ModelServicios->total_list($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $id=$data['id'];

        if(isset($data['perido'])){
            $data['perido']=1;
        }else{
            $data['perido']=0;
        }

        if(isset($data['check1'])){
            $data['check1']=1;
        }else{
            $data['check1']=0;
        }

        if(isset($data['check15'])){
            $data['check15']=1;
        }else{
            $data['check15']=0;
        }

        if(isset($data['check30'])){
            $data['check30']=1;
        }else{
            $data['check30']=0;
        }

        if($id==0){
            $data['reg']=$this->fechahoy;
            $data['personalid'] = $this->idpersonal;
            $id=$this->General_model->add_record('servicios',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'servicios');
            $id=$id;
        }

    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'servicios');
    }

    function viewdetalles_list(){
        $id = $this->input->post('id');
        $html='<table class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Precios</th>
                    <th scope="col">Depósito en garantia</th>
                </tr>
            </thead>
            <tbody>';
            $resul=$this->General_model->getselectwhere('servicios','id',$id);
            foreach ($resul as $s){
                if($s->check1==1){
                    $html.='<tr>
                    <td>1 día</td>
                    <td>$'.number_format($s->precios1,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia1,2,'.',',').'</td>
                    </tr>';
                }
                if($s->check15==1){
                    $html.='<tr>
                    <td>15 días</td>
                    <td>$'.number_format($s->precios15,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia15,2,'.',',').'</td>
                    </tr>';
                }
                if($s->check30==1){
                    $html.='<tr>
                    <td>30 días</td>
                    <td>$'.number_format($s->precios30,2,'.',',').'</td>
                    <td>$'.number_format($s->depositogarantia30,2,'.',',').'</td>
                    </tr>';
                }
            }
            $html.='</tbody>
        </table>';
        echo $html;
    }

}