<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('General_model');
    $this->load->model('ModeloCatalogos');
    $this->perfilid=$this->session->userdata('perfilid');
  }
	public function index(){
    $id_pefil=$this->perfilid;
    $id_pefil_aux=intval($id_pefil);
    //var_dump($id_pefil_aux);die;
    /*
    $id_pefil_aux=0;
    if($id_pefil!=0){
      $id_pefil_aux=$this->perfilid;
    }else{
      $id_pefil_aux=0; 
    }
    */
    /*
    $resconfig=$this->General_model->get_records_menu($id_pefil_aux);
    $pagina='Empleados';
    foreach ($resconfig as $item) {
      $pagina=$item->Pagina;
    }
    */
    /*
    if($id_pefil_aux==2){
      $pagina='Operacion';
    }else{
      $pagina='Empleados';
    }
    */
    $pagina='Inicio';
		if (!isset($_SESSION['perfilid'])) {
      $perfilview=0;
      redirect('/Login');
    }else{
        $perfilview=$_SESSION['perfilid'];
    }
   	if ($perfilview>=1) {
    	redirect('/'.$pagina);
    }else{
    	redirect('/Login');
    }
	}
    function solicitarpermiso(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar) {
                $permiso=1;
            }
        }
        echo $permiso;
    }


    public function getSucursales(){
        $result=$this->General_model->getSelectColOrder('id','sucursal',array('activo'=>1),"orden");
        echo json_encode(array("array"=>$result));
    }

  public function getNamesSucursales(){
    $id = $this->input->post('id');
    $result=$this->General_model->getSelectColOrder('name_suc','sucursal',array('id'=>$id),"orden");
    echo json_encode(array("array"=>$result));
  }

  ////////////////////
    public function searchallpros(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->searchallpros($pro);
        //echo $results;
        echo json_encode($results);
    }

    function viewprecios_ptipo(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        $html='';
        if($tipo==0){
          $row=0;
          $suc_pro=$this->General_model->get_suc_prod($id);
          foreach ($suc_pro->result() as $x){
            $row++;
            if($row==1){
                $precio=$x->precio;
                $incluye_iva=$x->incluye_iva;
                $iva_tex='';
                $ivacal=0;
                if($incluye_iva==1){
                  $iva_tex=$x->iva.' %';
                  $ivacal=round($precio*0.16,5);
                }
                $precio_final=round($precio+$ivacal,2);
                $html.='<tr>
                <td></td>
                <td>$ '.number_format($precio,5,'.',',') .'</td>
                <td>$ '.number_format($precio_final,2,'.',',') .'</td>
                </tr>';
            }
          }
        }
        if($tipo==1){
          $result=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$id));
          foreach ($result->result() as $item) {
            $html.='<tr>
                <td></td>
                <td>$ '.number_format($item->preciov,5,'.',',') .'</td>
                <td>$ '.number_format($item->preciov,2,'.',',') .'</td>
                </tr>';
          }
        }
        if($tipo==2){
          $result=$this->ModeloCatalogos->getselectwheren('servicios',array('id'=>$id));
          foreach ($result->result() as $item) {
            $htmlprecios='';
            $precio1=$item->precios1;
            if($precio1>0){
                $htmlprecios=number_format($item->precios1,2,'.',',');
                $html.='<tr>
                <td>1 dia </td>
                <td> '.$htmlprecios .'</td>
                <td> '.$htmlprecios .'</td>
                </tr>';
            }
            $precio2=$item->precios15;
            if($precio2>0){
                $htmlprecios=number_format($item->precios15,2,'.',',');
                $html.='<tr>
                <td>15 dias </td>
                <td> '.$htmlprecios .'</td>
                <td> '.$htmlprecios .'</td>
                </tr>';
            }
            $precio3=$item->precios30;
            if($precio3>0){
                $htmlprecios=number_format($item->precios30,2,'.',',');
                $html.='<tr>
                <td>30 dias</td>
                <td> '.$htmlprecios .'</td>
                <td> '.$htmlprecios .'</td>
                </tr>';
            }      
          }
        }
        if($tipo==3){
            $result=$this->ModeloCatalogos->getselectwheren('cat_servicios',array('id'=>$id));
            foreach ($result->result() as $item) {
                    $precio=$item->precio_siva;
                    $incluye_iva=$item->iva;
                    $iva_tex='';
                    $ivacal=0;
                    if($incluye_iva==1){
                        $iva_tex='16 %';
                        $ivacal=round($precio*0.16,5);
                    }
                    $precio_final=round($precio+$ivacal,2);
                $html.='<tr>
                    <td></td>
                    <td>$ '.number_format($precio,5,'.',',') .'</td>
                    <td>$ '.number_format($precio_final,2,'.',',') .'</td>
                    </tr>';
            }
        }
        echo $html;
    }

  function viewexistencias_ptipo(){
    $params = $this->input->post();
    $id=$params['id'];
    $tipo=$params['tipo'];
    $ext=$params['ext'];
    $html='';
    if($tipo==0){
      $strq="SELECT suc.id as idsuc, suc.id_alias, pro.id,suc.name_suc,pro.tipo,pros.stock, IFNULL(count(*),0) as serie, IFNULL(sum(prosl.cantidad),0) as lotes,
      IFNULL((SELECT sum(psl2.cantidad) FROM productos_sucursales_lote psl2 WHERE activo=1 AND psl2.productoid=pro.id AND psl2.sucursalid=suc.id),0) AS lote_all,
      IFNULL((select sum(ht.cantidad) as traslado_stock_cant from historial_transpasos as ht
      join traspasos t on t.id=ht.idtranspasos and t.activo=1 and t.status=1 and t.rechazado=0
      where ht.idproducto=pro.id and ht.activo=1 and ht.idsucursal_sale=suc.id and ht.tipo=0 and (ht.status = 0 and t.tipo=0 or ht.status=1 and t.tipo=1) and ht.rechazado!=1),0) as traslado_stock_cant,
      IFNULL(tlh.cantidad,0) as tras_stock_lote,
      count(tsh.id) as tot_tras_serie,
      IFNULL(t.status,2) as status_trasr,
      IFNULL(t2.status,2) as status_traslt
      FROM productos as pro
      INNER JOIN sucursal as suc ON suc.activo=1
      left join productos_sucursales as pros on pros.productoid=pro.id AND pros.sucursalid=suc.id
      LEFT join productos_sucursales_serie as pross on pross.activo=1 and pross.disponible=1 AND pross.productoid=pro.id AND pross.sucursalid=suc.id
      LEFT join productos_sucursales_lote as prosl on prosl.activo=1 AND prosl.productoid=pro.id AND prosl.sucursalid=suc.id
      left join traspasos_lotes_historial tlh on tlh.idlotes=prosl.id and tlh.activo=1 and pro.tipo=2
      left join traspasos_series_historial tsh on tsh.idseries=pross.id and tsh.activo=1 and pro.tipo=1
      left join traspasos t on t.id=tsh.idtraspasos and t.status=2
      left join traspasos t2 on t2.id=tlh.idtraspasos and t2.status=2
      WHERE pro.id='$id'
      group by suc.id,pro.id
      order by suc.orden";
      $query = $this->db->query($strq);
      foreach ($query->result() as $item) {
        $stock=0;
        if($item->tipo==0){//normal
            $stock=$item->stock-$item->traslado_stock_cant;
        }
        if($item->tipo==1){//serie
            if($item->status_trasr!=2){
                $stock=$item->serie-$item->tot_tras_serie;
            }else{
                $stock=$item->serie;
            }
            $stock = $this->General_model->stock_producto_series($item->id,$item->idsuc);
        }
        if($item->tipo==2){//lote
            if($item->status_traslt!=2){
                $stock=$item->lote_all-$item->tras_stock_lote;
            }else{
                $stock=$item->lote_all;
            }
            $stock = $this->General_model->stock_producto_lotes($item->id,$item->idsuc);
        }

        $info_html='<tr>
        <td>'.$item->id_alias.'</td>
        <td>'.$item->name_suc.'</td>
        <td>'.$stock.'</td>
        </tr>';
        if($ext==0){
            $html.=$info_html;
        }
        if($ext==1 and $stock>0){
            $html.=$info_html;
        }
      }
    }
    if($tipo==1){
      /*
      $result=$this->ModeloCatalogos->getselectwheren('recargas',array('id'=>$id));
      foreach ($result->result() as $item) {
          $html.='<tr>
              <td></td>
              <td></td>
              <td>'.$item->capacidad.'</td>
              </tr>';
      }
      */
    }
    if($tipo==2){
        
    }
    if($tipo==3){
        
    }
    echo $html;
  }

  function apertura_caja_suc(){
        $monto = $this->input->post('monto');
        $sucursal = $this->input->post('sucursal');
        $validar=0;
        if($this->session->userdata('usuarioid')=="1" || $this->session->userdata('usuarioid')=="14"){
            //validar que no esté aperturado el turno
            //$get_ahc=$this->ModeloCatalogos->getselectwheren('turnos',array('sucursal'=>$sucursal,"fecha"=>date('Y-m-d'),"estatus"=>"2"));
            //if($get_ahc->num_rows()!=0){
            //    $validar=3;
            //}
            if($validar==0){
                $get_ah=$this->ModeloCatalogos->getselectwheren('turnos',array('sucursal'=>$sucursal,"fecha"=>date('Y-m-d'),"estatus"=>"1"));
                if($get_ah->num_rows()==0){
                    $validar=1;
                    $data = array('idpersonal'=>$this->session->userdata('idpersonal'),
                        'fecha'=>date('Y-m-d'),
                        'hora'=>date('H:i:s'),
                        'estatus'=>1,
                        'monto'=>$monto,
                        'sucursal'=>$sucursal
                    );
                    $idcliente=$this->ModeloCatalogos->Insert('turnos',$data);
                }else{
                    $validar=2;
                }
            }
        } 
        echo $validar;
    }

}
