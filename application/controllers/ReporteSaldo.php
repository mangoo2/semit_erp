<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReporteSaldo extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCompras');
        $this->load->model('General_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,54);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('reportes/saldo_compras');
        $this->load->view('templates/footer');
        $this->load->view('reportes/saldo_comprasjs');
        $this->load->view('ventasp/modalp');  
    }

    function searchproveedor(){
        $pro = $this->input->get('search');
        $results=$this->General_model->getseleclike2('proveedores','nombre',$pro);
        echo json_encode($results->result());
    }

    function getTotalCompras(){
        $id_provee=$this->input->post("id_provee");
        $fechai=$this->input->post("fechai");
        $fechaf=$this->input->post("fechaf");
 
        $getv=$this->ModeloCompras->getTotalCompras($id_provee,$fechai,$fechaf);
        $html="";
        $html.='<table class="table table-sm" id="table_total_compra">';
            $html.='<thead>';
                $html.='<tr>';
                    //$html.='<th>Proveedor</th>';
                    $html.='<th>Mes</th>';
                    $html.='<th>Subtotal</th>';
                    $html.='<th>IVA</th>';
                    $html.='<th>ISR</th>';
                    $html.='<th>Total</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody class="body_compras">';
        $supertot=0;
        foreach ($getv as $c) {
            $html .= '<tr>';
            //$html .= '<td>'.$c->provee.'</td>';
            $html .= '<td> <input type="hidden" id="name_mes" value="'.ucwords($c->mes_anio).'">'.ucwords($c->mes_anio).'</td>';
            $html .= '<td>$'.number_format($c->subtotal,2,".",",").'</td>';
            $html .= '<td>$'.number_format($c->iva,2,".",",").'</td>';
            $html .= '<td>$'.number_format($c->isr,2,".",",").'</td>';
            $html .= '<td><input type="hidden" id="total" value="'.round($c->total,2).'">$'.number_format($c->total,2,".",",").'</td>';
            $html .= '</tr>';
            $supertot=$supertot+$c->total;
        }                
        $html.='<tr>
                    <td colspan="3"></td>
                    <td><b>Total compras: </b></td>
                    <td><input type="hidden" id="supertot" value="'.$supertot.'">$'.number_format($supertot,2,".",",").'</td>
                </tr>
            </tbody>';
        $html.='</table>';
        echo json_encode(array("html"=>$html));
    }
    

    function exportSaldosExcel($idp,$fi,$ff){
        $data["getv"]=$this->ModeloCompras->getTotalCompras($idp,$fi,$ff);
        $this->load->view('reportes/excelSaldosProvee',$data);
    }
}

