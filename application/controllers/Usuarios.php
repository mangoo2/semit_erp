<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        $data['perfil']=$this->perfilid;
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('usuario/listado');
        $this->load->view('templates/footer');
        $this->load->view('usuario/listadojs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        if($id==0){
            $data['personalId'] = 0;
            $data['foto']='';
            $data['nombre'] = '';
            $data['puesto']='';
            $data['celular']='';
            $data['correo']='';
            $data['nss'] = '';

            $data['calle'] = '';
            $data['no_ext'] = '';
            $data['no_int'] = '';
            $data['colonia'] = '';
            $data['municipio'] = '';
            $data['estado'] = '';
            $data['cp'] = '';
            $data['pais'] = '';

            $data['UsuarioID'] = 0;
            $data['perfilId'] = 0;
            $data['Usuario'] = '';
            $data['contrasena'] = '';
            $data['contrasena2'] = '';
            $data['pin'] = '';
            $data['sucursal'] = 0;
            
            $data['acceso'] = 1;
            $data['motivo_desactivar'] = '';
            $data['comision']=0;
            $data['comision_tecnico']=0;
        }else{
            $resul=$this->General_model->getselectwhere('personal','personalId',$id);
            foreach ($resul as $item) {
                $data['personalId']=$item->personalId;
                $data['nombre']=$item->nombre;
                $data['puesto']=$item->puesto;      
                $data['celular']=$item->celular;
                $data['correo']=$item->correo;
                $data['foto']=$item->foto;   
                $data['nss']=$item->nss;   
                $data['calle'] = $item->calle;
                $data['no_ext'] = $item->no_ext;
                $data['no_int'] = $item->no_int;
                $data['colonia'] = $item->colonia;
                $data['municipio'] = $item->municipio;
                $data['estado'] = $item->estado;
                $data['cp'] = $item->cp;
                $data['pais'] = $item->pais;    
                $data['comision']=$item->comision; 
                $data['comision_tecnico']=$item->comision_tecnico;      
            }
            $resul=$this->General_model->getselectwhere('usuarios','personalId',$id);
            foreach ($resul as $item) {
                $data['UsuarioID']=$item->UsuarioID;
                $data['perfilId']=$item->perfilId;     
                $data['Usuario']=$item->Usuario;
                $data['contrasena']='x1f3[5]7w78{';     
                $data['contrasena2']='x1f3[5]7w78{'; 
                $data['pin'] = $item->pin;
                $data['sucursal'] = $item->sucursal;  
                $data['acceso'] = $item->acceso;
                $data['motivo_desactivar'] = $item->motivo_desactivar;
            }
        }   
        $data['perfil']=$this->General_model->getselectwhere('perfiles','estatus',1);
        $data['estadorow']=$this->ModeloCatalogos->getselect_tabla('estado');
        $data['sucrow']=$this->ModeloCatalogos->getselectwheren('sucursal',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('usuario/vista',$data);
        $this->load->view('templates/footer');
        $this->load->view('usuario/vistajs');
    }

    public function registrar_datos(){
        $data=$this->input->post();
        $personalId=$data['personalId'];
        unset($data['personalId']);
        unset($data['UsuarioID']);
        unset($data['perfilId']);
        unset($data['Usuario']);
        unset($data['contrasena']);
        unset($data['contrasena2']);
        
        unset($data['profile_avatar_remove']);
        unset($data['profile_avatar']);

        unset($data['sucursal']);
        unset($data['pin']);
        unset($data['acceso']);
        unset($data['motivo_desactivar']);
        if($personalId==0){
            $id=$this->General_model->add_record('personal',$data);
        }else{
            $id=$this->General_model->edit_record('personalId',$personalId,$data,'personal');
            $id=$personalId;
        }
        echo $id;
    }
    
    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        $personalId=$datos['personalId_aux'];

        unset($datos['personalId']);
        unset($datos['personalId_aux']);
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);

        unset($datos['personalId']);
        unset($datos['nombre']);
        
        unset($datos['nombre']); 
        unset($datos['puesto']);
        unset($datos['celular']);
        unset($datos['correo']);
        unset($datos['foto']);
        unset($datos['nss']);
        unset($datos['calle']);
        unset($datos['no_ext']);
        unset($datos['no_int']);
        unset($datos['colonia']);
        unset($datos['municipio']);
        unset($datos['estado']);
        unset($datos['cp']);
        unset($datos['pais']);

        unset($datos['profile_avatar_remove']);
        unset($datos['profile_avatar']);

        unset($datos['comision_tecnico']);

        $datos['personalId']=$personalId;
        if(isset($datos['acceso'])){
            $datos['acceso']=0;
        }else{
            $datos['acceso']=1;
        }
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            $this->General_model->add_record('usuarios',$datos);
            $result=1;
            $this->General_model->edit_recordw(array('personalId'=>$personalId),array('usuario'=>1),'personal');
        }   
        echo $result;
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="personal";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('personalId',$id,$array,'personal');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  


    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_empleados($params);
        $totaldata= $this->ModelCatalogos->total_empleados($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
    }

    public function suspender_record(){
        $id=$this->input->post('id');
        $motivo=$this->input->post('motivo');
        $data = array('check_baja'=>'on','motivo'=>$motivo,'fechabaja'=>$this->fecha_reciente);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
    }

    function reporte_excel()
    {  
      $data['anio']=date('d_m_Y');   
      $data['get_usuarios']=$this->ModeloCatalogos->get_usuarios();
      $this->load->view('usuario/excel',$data);
    }

}    