<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimientos extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloMttos');
        $this->load->model('Modeloventas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,46);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['btn_active']=9;
        $data['btn_active_sub']=46;
        $data["id_last"]=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/indexjs');
    }

    public function lista($id){
        $data['btn_active']=9;
        $data['btn_active_sub']=46;
        $data["id_last"]=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/indexjs');
    }

    public function solicitud($id=0){
        $data['btn_active']=9;
        $data['btn_active_sub']=46;
        //$data2[""]=array();
        if($id>0){
            $data["mtto"] = $this->ModeloMttos->get_mtto($id);
        }
        $data['estadorow']=$this->ModeloCatalogos->getselect_tabla('estado');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('mantenimiento/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('mantenimiento/formjs',$data);
    }

    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getclienterazonsociallike($pro);
        echo json_encode($results->result());
    }

    function searchInsumo(){
        $pro = $this->input->get('search');
        //$id_suc=$this->sucursal;
        $id_suc=7;
        $results=$this->ModeloCatalogos->searchInsumo_ventas_like($pro,$id_suc);
        //echo $results;
        echo json_encode($results->result());
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->ModelClientes->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function save_solicitud(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data["id_sucursal"]=$this->sucursal;
            $data["id_personal"]=$this->idpersonal;
            $data["reg"]=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('mtto_externo',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'mtto_externo');
            $id=$id;
        }
        echo $id;
    }

    public function save_cliente(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data['reg']=$this->fecha_hora_actual;
            $data['tipo_registro'] = 1;
            $data['sucursal'] = $this->sucursal;
            $id=$this->General_model->add_record('clientes',$data);
            $datosf = array('idcliente'=>$id);
            $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
        }else{
            $this->General_model->edit_record('id',$id,$data,'clientes');
            $resulf=$this->General_model->getselectwhere('cliente_fiscales','idcliente',$id);
            $idf=0;
            foreach ($resulf as $itemf) {
                $idf=$itemf->id;
            }
            $datosf = array('idcliente'=>$id);
            if($idf==0){
                $idf=$this->General_model->add_record('cliente_fiscales',$datosf);
            }else{
                $this->General_model->edit_record('id',$idf,$datosf,'cliente_fiscales');
            }
            $idf=$idf;
        }
        echo $id;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'mtto_externo');
    }

    public function get_data_list(){
        $params = $this->input->post();
        $params["perfilid"]=$this->perfilid;
        $params["sucursal"]=$this->sucursal;
        $getdata = $this->ModeloMttos->get_listMtto($params);
        $totaldata= $this->ModeloMttos->total_list_mtto($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function update_estatus(){
        $id=$this->input->post('id');
        $estatus=$this->input->post('estatus');
        $comentarios=$this->input->post('comentarios');
        if($estatus==2){ //reparacion
            $fecha_serv="fecha_serv";
        }if($estatus==3){ //reparado 
            $fecha_serv="fecha_repara";
        }if($estatus==4){ //no reparado 
            $fecha_serv="fecha_sin_repara";
        } 
        $data = array('estatus'=>$estatus,$fecha_serv=>$this->fecha_hora_actual,"comentarios"=>$comentarios);
        $this->General_model->edit_record('id',$id,$data,'mtto_externo');
    }

    public function getComentarios(){
        $id=$this->input->post("id");
        $coments="";
        $getmtto=$this->ModeloCatalogos->getselectwheren('mtto_externo',array('id'=>$id));
        if($getmtto->num_rows()>0){
            $getmtto = $getmtto->row();
            $coments = $getmtto->comentarios;
        }
        echo json_encode(array("comentarios"=>$coments));
    }

    public function getDetallePago(){
        $id=$this->input->post("id");
        $nombre=""; $marca=""; $serie=""; $modelo=""; $id_venta=0; $a_cuenta="0"; $total=0;
        $serv= $this->ModeloMttos->getPagoServicio($id);
        foreach ($serv as $r) {
            $nombre=$r->nombre; $marca=$r->marca; $serie=$r->serie; $modelo=$r->modelo; $a_cuenta=$r->a_cuenta; $id_venta=$r->id_venta;
        }
        echo json_encode(array("total"=>$total,"nombre"=>$nombre,"marca"=>$marca,"serie"=>$serie,"modelo"=>$modelo,"id_venta"=>$id_venta,"a_cuenta"=>$a_cuenta));
    }

    public function pdf_garantia($id){
        $data['id']=$id;
        $data['det']= $this->ModeloGarantia->get_detalle_solicitud($id);
        $data['evide']= $this->ModeloGarantia->getEvideGarantia($id,$data["det"][0]->id_venta);
        $this->load->view('reportes/solicitud_garantia',$data);
    }

    public function ordenTrabajo($id){
        $data['id']=$id;
        $data['det']= $this->ModeloMttos->get_detalle_orden($id);
        $data['evide']= $this->ModeloMttos->getMttosEvide($id);
        $this->load->view('reportes/orden_trabajo',$data);
    }

    public function getDetOrdTrabajo(){
        $id=$this->input->post('id');
        $fecha="";
        $observ="";
        $descrip=""; $cli=""; $dir=""; $col=""; $cel=""; $tel=""; $mail=""; $rfc=""; $nom_contacto=""; $tel_contacto="";
        $get=$this->ModeloMttos->getOrdenTrabajo($id);
        $id=0;
        foreach($get as $o){
            $id=$o->id_ot;
            $fecha=$o->fecha;
            $observ=$o->observ;
            //$descrip=$o->descrip;
            $cli=$o->nom_solicita;
            $dir=$o->dir_solicita;
            $col=$o->col_solicita;
            $cel=$o->cel_solicita;
            $tel=$o->tel_solicita;
            $mail=$o->mail_solicita; 
            $rfc=$o->rfc_solicita;
            if($o->id_ot==0){
                $cli=$o->cliente;
                $dir=$o->calle;
                $col=$o->colonia;
                $cel=$o->celular;
                $mail=$o->correo;
            }
            $nom_contacto=$o->nom_contacto; $tel_contacto=$o->tel_contacto;
        }
        
        echo json_encode(array("fecha"=>$fecha,"observ"=>$observ,"id"=>$id,"cli"=>$cli,"dir"=>$dir,"col"=>$col,"cel"=>$cel,"tel"=>$tel,"mail"=>$mail,"rfc"=>$rfc,"descrip"=>$descrip,"nom_contacto"=>$nom_contacto,"tel_contacto"=>$tel_contacto));
    }

    public function validaOTMtto(){
        $id=$this->input->post("id");
        $cont=0;
        $getmtto=$this->ModeloCatalogos->getselectwheren('mtto_externo',array('id'=>$id));
        $getmtto = $getmtto->row();
        if($getmtto->estatus!="4"){
            $get=$this->ModeloCatalogos->getselectwheren('orden_trabajo',array('id_mtto'=>$id));
            if($get->num_rows()>0){
                $cont++;
            }
        }else{
            $cont=100;
        }
        echo $cont;
    }

    public function save_orden(){
        $id=$this->input->post('id');
        $data["id_mtto"]=$this->input->post('id_mtto');
        $data["fecha"]=$this->input->post('fecha');
        $data["observ"]=$this->input->post('observ');
        $data["nom_solicita"]=$this->input->post('nom_solicita');
        $data["dir_solicita"]=$this->input->post('dir_solicita');
        $data["col_solicita"]=$this->input->post('col_solicita');
        $data["cel_solicita"]=$this->input->post('cel_solicita');
        $data["tel_solicita"]=$this->input->post('tel_solicita');
        $data["mail_solicita"]=$this->input->post('mail_solicita');
        $data["rfc_solicita"]=$this->input->post('rfc_solicita');
        $data["nom_contacto"]=$this->input->post('nom_contacto');
        $data["tel_contacto"]=$this->input->post('tel_contacto');

        if($id>0){
            unset($data["id_mtto"]);
            $this->General_model->edit_record('id',$id,$data,'orden_trabajo');
        }else{
            $this->ModeloCatalogos->Insert('orden_trabajo',$data);
        }
    }

    function saveDetaInsumos(){
        $params=$this->input->post();
        $insumos = $params['array_ins'];
        $DATAins = json_decode($insumos);

        ///////////////////insumos del mtto/////////////////////////
        $data_array_ins=array();
        for ($l=0;$l<count($DATAins);$l++) {
            $insert_bita_dll=array(
                'id_venta'=>$DATAins[$l]->id_venta,
                'id_insumo'=>$DATAins[$l]->id_ins,
                'id_prod_suc'=>$DATAins[$l]->id_ps,
                'cantidad'=>$DATAins[$l]->cant_ins,
                'id_mtto'=>$DATAins[$l]->id_mtto,
                'cant_ini'=>$DATAins[$l]->stock
            );
            $data_array_ins[]=$insert_bita_dll;
            //editar insumo utlizado en servicios
            $this->Modeloventas->restaInsumo_sucursal($DATAins[$l]->id_ps,$DATAins[$l]->cant_ins);
        }

        if(count($data_array_ins)>0){
            $this->ModeloCatalogos->insert_batch('bitacora_ins_servs_ventas',$data_array_ins);
        }
    }

    public function getInsumosMtto(){
        $id=$this->input->post("id");
        $id_venta=$this->input->post("id_venta");
        $html=""; $numero=""; $descripcion="";
        $get=$this->ModeloMttos->getInsumos($id,$id_venta);
        foreach($get as $i){
            $numero=$i->numero;
            $descripcion=$i->descripcion;
            $html.="<tr>
                <td>".$i->cantidad."</td>
                <td>".$i->idProducto." / ".$i->nombre."</td>
            </tr>";
        }
        echo json_encode(array("html"=>$html,"numero"=>$numero,"descripcion"=>$descripcion));
    }

    public function cargaimagen(){
        $id_mtto=$_POST['id_mtto'];
        //log_message('error', "id_mtto: ".$id_mtto);
        $DIR_SUC=FCPATH.'uploads/mttos';
        $config['upload_path']    = $DIR_SUC;
        $config['allowed_types']  = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']       = 5000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload("file")){
            $data = array('error' => $this->upload->display_errors());
            //log_message('error', json_encode($data));                    
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
            $this->ModeloCatalogos->Insert('evidencia_mtto',array('id_mtto'=>$id_mtto,"imagen"=>$file_name,"id_personal"=>$this->idpersonal,"fecha_reg"=>$this->fecha_hora_actual));
            $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }

    public function getEvidencias(){
        $id_mtto = $this->input->post("id_mtto");
        $id = $this->input->post("id");
        $i=0; 
        
        $getdat=$this->ModeloCatalogos->getselectwheren('evidencia_mtto',array('estatus'=>1,'id_mtto'=>$id_mtto));
        foreach ($getdat->result() as $k) {
            $img=""; $imgdet=""; $imgp="";
            $i++;
            $img = "<img src='".base_url()."uploads/mttos/".$k->imagen."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '', caption: '".$k->imagen."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/mttos/".$k->imagen."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false";  
            echo '<div class="row_'.$k->id.'">
                <div class="col-md-8">
                    <label class="col-md-12"> Imagen: '.$k->imagen.'</label>
                    <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                    <hr style="background-color: blue;">
                </div>
            </div>
            <script> 
                $("#inputFile'.$i.'").fileinput({
                  overwriteInitial: false,
                  showClose: false,
                  showCaption: false,
                  showUploadedThumbs: false,
                  showBrowse: false,
                  showRemove: false,
                  removeTitle: "Cancel or reset changes",
                  elErrorContainer: "#kv-avatar-errors-1",
                  msgErrorClass: "alert alert-block alert-danger",
                  defaultPreviewContent: "'.$img.'",
                  layoutTemplates: {main2: "{preview} {remove}"},
                  allowedFileExtensions: ["jpg", "png", "jpeg","webp"],
                  initialPreview: [
                  "'.$imgp.'"
                  ],
                  initialPreviewAsData: '.$typePDF.',
                  initialPreviewFileType: "image",
                  initialPreviewConfig: [
                      '.$imgdet.'
                  ]
                });
            </script>';
        }     
    }

    /* ***********************************/
 
    function mail_solicitudGaran(){ 
        $id = $this->input->post('id');
        $getpers=$this->ModeloGarantia->getPersonalGarantia($id);
        $array_com = array();
        $array_adm = array();
        $suc_solicita=""; $cont_com=0; $cont_adm=0; $reg=""; 
        foreach ($getpers as $p) {
            $suc_solicita=$p->name_suc;
            $reg=$p->reg;
            if ($p->correo_comp!='') {
                array_push($array_com,$p->correo_comp);
                $cont_com++;  
            }if ($p->correo_adm!='') {
                array_push($array_adm,$p->correo_adm);
                $cont_adm++;
            }
        }
  
        $array_com=array_unique($array_com);
        $array_adm=array_unique($array_adm);

        //log_message('error','array_com unique: '.json_encode($array_com));
        //log_message('error','array_adm unique: '.json_encode($array_adm));

        $asunto='Solicitud de garantíaa';
        $this->load->library('email');
        //$this->load->library('email', NULL, 'ci_email');

        $config['protocol'] = 'smtp';
        $config["smtp_host"] ='mail.semit.mx'; 
        $config["smtp_user"] = 'noreply@semit.mx';
        $config["smtp_pass"] = 'l)sT^Iwk,e7n';
        $config["smtp_port"] = '465';
        $config["smtp_crypto"] = 'ssl';
        $config['charset'] = 'utf-8'; 
        $config['wordwrap'] = TRUE; 
        $config['validate'] = true;
        $config['mailtype'] = 'html';
         
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@semit.mx','Semit');

        //$this->email->to($correo,'Solicitud de traspaso');
        if($cont_com>0){
            $this->email->to($array_com);
        }
        if($cont_adm>0){
            $this->email->cc($array_adm);
        }
        $this->email->subject($asunto);
        
        /*$message =utf8_encode("Solicitud de garantía: ".$suc_solicita ." registra solicitud de garantía de producto semit.
        <br>Con id: ".$id."
        <br>Con fecha: ".date("H:i a d-m-Y", strtotime($reg)));*/
        $message ="Solicitud de garantía: ".$suc_solicita ." registra solicitud de garantía de producto semit.
        <br>Con id: ".$id."
        <br>Con fecha: ".date("H:i a d-m-Y", strtotime($reg));
        $this->email->message($message);
        $this->email->send();
    }

}