<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conf_facturacion extends CI_Controller {
	  function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');

        date_default_timezone_set('America/Mexico_City');        
        $this->fecha_hoy_l=date('Y-m-d H:i:s');
        $this->fecha_hoy = date('Y-m-d');

        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursal=$this->session->userdata('sucursal');
            $this->succlave=$this->session->userdata('succlave');
            //$this->idpersonal=$this->session->userdata('idpersonal');
            //$this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,53);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=53;
        //===================================================
            $tipov=1;
            
            $data['tipov']=1;
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];
            $data['ConfiguracionesId']=$datosconfiguracion->ConfiguracionesId;
            $data['Rfc']=$datosconfiguracion->Rfc;
            $data['Curp']=$datosconfiguracion->Curp;
            $data['Nombre']=$datosconfiguracion->Nombre;
            $data['Direccion']=$datosconfiguracion->Direccion;
            $data['PaisExpedicion']=$datosconfiguracion->PaisExpedicion;
            $data['Regimen']=$datosconfiguracion->Regimen;
            $data['Calle']=$datosconfiguracion->Calle;
            $data['Estado']=$datosconfiguracion->Estado;
            $data['Municipio']=$datosconfiguracion->Municipio;
            $data['CodigoPostal']=$datosconfiguracion->CodigoPostal;
            $data['Noexterior']=$datosconfiguracion->Noexterior;
            $data['Colonia']=$datosconfiguracion->Colonia;
            $data['localidad']=$datosconfiguracion->localidad;
            $data['Nointerior']=$datosconfiguracion->Nointerior;

            $timbresvigentes=$datosconfiguracion->timbresvigentes;
            $data['timbresvigentes']=$timbresvigentes;
            $tv_inicio=$datosconfiguracion->tv_inicio;
            $tv_fin=$datosconfiguracion->tv_fin;

            $strq="SELECT * FROM timbres WHERE activo=1 ORDER BY id DESC LIMIT 1";
            $query=$this->db->query($strq);
            foreach ($query->result() as $item_t) {
                if($item_t->id>0){
                    $timbresvigentes=$item_t->cantidad;
                    $tv_inicio=$item_t->tv_inicio;
                    $tv_fin=$item_t->tv_fin;
                }
            }

            $facturastimbradas=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin,0);//todas las timbradas
            $facturastimbradas_n=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin,1);//solo facturas
            $data['facturastimbradas']=$facturastimbradas_n;
            $facturastimbradas_notas=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin,2);//solo notas de credito
            $data['facturastimbradas_notas']=$facturastimbradas_notas;
            $facturastimbradas_cartaporte=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin,7);//solo cartasporte
            $data['facturastimbradas_cartaporte']=$facturastimbradas_cartaporte;
             log_message('error',"$facturastimbradas:$facturastimbradas = facturastimbradas_n:$facturastimbradas_n + facturastimbradas_notas:$facturastimbradas_notas + facturastimbradas_cartaporte:$facturastimbradas_cartaporte" );
            $facturascanceladas_f=$this->ModeloCatalogos->total_facturas(0,$tv_inicio,$tv_fin,0);
            $facturascanceladas_c=$this->ModeloCatalogos->totalcomplementos(0,$tv_inicio,$tv_fin);

            $facturascanceladas=$facturascanceladas_f+$facturascanceladas_c;

            $complementotimbradas=$this->ModeloCatalogos->totalcomplementos(1,$tv_inicio,$tv_fin);
            //$data['facturastimbradas'] = $facturastimbradas;
            $data['facturascanceladas'] = $facturascanceladas;
            $data['complementotimbradas'] = $complementotimbradas;
            $data['facturasdisponibles'] = $timbresvigentes-$facturastimbradas-$facturascanceladas-$complementotimbradas;
            //==================================================================================
            $nombre_fichero = 'hulesyg/temporalsat/FinVigencia.txt';
            if (file_exists($nombre_fichero)) {
                $fh = fopen(base_url().$nombre_fichero, 'r') or die("Se produjo un error al abrir el archivo");
                    $linea = fgets($fh);
                fclose($fh); 

                $remplazar = array('notAfter=','  ','GMT'); 
                $remplazarpor = array('',' ','');
                $fechavigente = str_replace($remplazar,$remplazarpor,$linea);
                $fechavigenteex = explode(" ", $fechavigente);
                $fechafin=$fechavigenteex[1].' '.$fechavigenteex[0].' '.$fechavigenteex[3];
            }else{
                $fechafin='';
            }
            $data['fechafin']=$fechafin;
            //==================================================================================
        //===================================================
            $data['regimes_result'] =$this->ModeloCatalogos->getselectwheren('f_regimenfiscal',array('activo'=>1));
            $data['h_timbres'] =$this->ModeloCatalogos->getselectwheren('timbres',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('config/facturas');
        $this->load->view('templates/footer');
        $this->load->view('config/facturasjs');
    }
    function datosgenerales(){
        $data = $this->input->post();
        $id = $data['ConfiguracionesId']; 
        unset($data['ConfiguracionesId']);

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',$data,array('ConfiguracionesId'=>$id));
    }
    function cargacertificado(){
        $ConfiguracionesId=$_POST['ConfiguracionesId'];
        if($ConfiguracionesId==2){
            $rut_file='2';
            $ConfiguracionesId=2;
        }else{
            $rut_file='';
            $ConfiguracionesId=1;
        }
        $url_carp=FCPATH.'hulesyg/elementos'.$rut_file.'/';
        //============================eliminar el archivo anterior
            $archivo=$url_carp.'archivos.cer';
            if (file_exists($archivo)) {
                if (unlink($archivo)) {
                    //echo "El archivo '$archivo' ha sido eliminado.";
                } else {
                    //echo "No se pudo eliminar el archivo '$archivo'.";
                }
            } else {
                //echo "El archivo '$archivo' no existe.";
            }
        //============================
        $configUpload['upload_path'] = $url_carp;
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('cerdigital');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivocer'=>$file_name),array('ConfiguracionesId'=>$ConfiguracionesId));

        $output = [];
        echo json_encode($output);
    }
    function cargakey(){
        $ConfiguracionesId=$_POST['ConfiguracionesId'];
        if($ConfiguracionesId==2){
            $rut_file='2';
            $ConfiguracionesId=2;
        }else{
            $rut_file='';
            $ConfiguracionesId=1;
        }

        //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/semit_erp/';
        //$rutainterna= $_SERVER['DOCUMENT_ROOT'];
        $rutaarchivos=FCPATH.'/hulesyg/elementos'.$rut_file.'/';
        
        $pass = $_POST['pass'];
        $url_carp=FCPATH.'hulesyg/elementos'.$rut_file.'/';
        //============================eliminar el archivo anterior
            $archivo=$url_carp.'archivos.key';
            if (file_exists($archivo)) {
                if (unlink($archivo)) {
                    //echo "El archivo '$archivo' ha sido eliminado.";
                } else {
                    //echo "No se pudo eliminar el archivo '$archivo'.";
                }
            } else {
                //echo "El archivo '$archivo' no existe.";
            }
        //============================
        $configUpload['upload_path'] = $url_carp;
        //$configUpload['upload_path'] = base_url().'kyocera/elementos/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('claveprivada');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivokey'=>$file_name,'paswordkey'=>$pass),array('ConfiguracionesId'=>$ConfiguracionesId));
        file_put_contents($rutaarchivos.'passs.txt', $pass);
        file_put_contents($rutaarchivos.'pass.txt', $pass);
        $output = [];
        echo json_encode($output);
    }
    
    function limpiarCarpeta($carpeta) {
        // funcion para eliminar todo lo que esta dentro de una carpeta
        if (!is_dir($carpeta)) {
            echo "El directorio no existe.";
            return false;
        }

        // Abrir la carpeta
        $archivos = array_diff(scandir($carpeta), ['.', '..']);

        foreach ($archivos as $archivo) {
            $rutaCompleta = $carpeta . DIRECTORY_SEPARATOR . $archivo;

            if (is_dir($rutaCompleta)) {
                // Recursivamente limpiar subdirectorios
                limpiarCarpeta($rutaCompleta);
                rmdir($rutaCompleta); // Eliminar subdirectorio vacío
            } else {
                // Eliminar archivo
                unlink($rutaCompleta);
            }
        }

        return true;
    }
    public function get_data_factura_mes(){

        $total1=$this->Modelofacturas->total_facturas_mes_num(date('Y'),1);//Enero
        $mes1=number_format($total1, 0, '.', '');
        $total2=$this->Modelofacturas->total_facturas_mes_num(date('Y'),2);//Febrero
        $mes2=number_format($total2, 0, '.', '');
        $total3=$this->Modelofacturas->total_facturas_mes_num(date('Y'),3);//Marzo
        $mes3=number_format($total3, 0, '.', '');
        $total4=$this->Modelofacturas->total_facturas_mes_num(date('Y'),4);//Abril
        $mes4=number_format($total4, 0, '.', '');
        $total5=$this->Modelofacturas->total_facturas_mes_num(date('Y'),5);//Mayo
        $mes5=number_format($total5, 0, '.', '');
        $total6=$this->Modelofacturas->total_facturas_mes_num(date('Y'),6);//Junio
        $mes6=number_format($total6, 0, '.', '');
        $total7=$this->Modelofacturas->total_facturas_mes_num(date('Y'),7);//Julio
        $mes7=number_format($total7, 0, '.', '');
        $total8=$this->Modelofacturas->total_facturas_mes_num(date('Y'),8);//Agosto
        $mes8=number_format($total8, 0, '.', '');
        $total9=$this->Modelofacturas->total_facturas_mes_num(date('Y'),9);//Septiembre
        $mes9=number_format($total9, 0, '.', '');
        $total10=$this->Modelofacturas->total_facturas_mes_num(date('Y'),10);//Octubre
        $mes10=number_format($total10, 0, '.', '');
        $total11=$this->Modelofacturas->total_facturas_mes_num(date('Y'),11);//Noviembre
        $mes11=number_format($total11, 0, '.', '');
        $total12=$this->Modelofacturas->total_facturas_mes_num(date('Y'),12);//Diciembre
        $mes12=number_format($total12, 0, '.', '');
        $result = array('mes1'=>$mes1,'mes2'=>$mes2,'mes3'=>$mes3,'mes4'=>$mes4,'mes5'=>$mes5,'mes6'=>$mes6,'mes7'=>$mes7,'mes8'=>$mes8,'mes9'=>$mes9,'mes10'=>$mes10,'mes11'=>$mes11,'mes12'=>$mes12);
        echo json_encode($result);
    }
    public function get_data_factura_ventas_mes(){

        $total1=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),1);//Enero
        $mes1=number_format($total1, 2, '.', '');
        $total2=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),2);//Febrero
        $mes2=number_format($total2, 2, '.', '');
        $total3=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),3);//Marzo
        $mes3=number_format($total3, 2, '.', '');
        $total4=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),4);//Abril
        $mes4=number_format($total4, 2, '.', '');
        $total5=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),5);//Mayo
        $mes5=number_format($total5, 2, '.', '');
        $total6=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),6);//Junio
        $mes6=number_format($total6, 2, '.', '');
        $total7=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),7);//Julio
        $mes7=number_format($total7, 2, '.', '');
        $total8=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),8);//Agosto
        $mes8=number_format($total8, 2, '.', '');
        $total9=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),9);//Septiembre
        $mes9=number_format($total9, 2, '.', '');
        $total10=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),10);//Octubre
        $mes10=number_format($total10, 2, '.', '');
        $total11=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),11);//Noviembre
        $mes11=number_format($total11, 2, '.', '');
        $total12=$this->Modelofacturas->total_facturas_ventas_mes(date('Y'),12);//Diciembre
        $mes12=number_format($total12, 2, '.', '');

        $result = array('mes1'=>$mes1,'mes2'=>$mes2,'mes3'=>$mes3,'mes4'=>$mes4,'mes5'=>$mes5,'mes6'=>$mes6,'mes7'=>$mes7,'mes8'=>$mes8,'mes9'=>$mes9,'mes10'=>$mes10,'mes11'=>$mes11,'mes12'=>$mes12);
        echo json_encode($result);
    }

}