<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReporteVentas extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Modeloventas');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->sucursal=$this->session->userdata('sucursal');
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,48);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['sucus']=$this->ModeloCatalogos->getselectwherenallOrder('sucursal',array('activo'=>1),"orden","ASC");
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('reportes/ventas');
        $this->load->view('templates/footer');
        $this->load->view('reportes/ventasjs');
        $this->load->view('ventasp/modalp');  
    }

    function getTotalVentasList(){
        $ids=$this->input->post("idsuc");
        $fechai=$this->input->post("fechai");
        $fechaf=$this->input->post("fechaf");
 
        $getv=$this->Modeloventas->getTotalVentas($ids,$fechai,$fechaf);
        $html="";
        $html.='<table class="table table-sm" id="table_total_venta">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<th>Sucursal</th>';
                    $html.='<th>Subtotal</th>';
                    $html.='<th>IVA</th>';
                    $html.='<th>Total vendido</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody class="body_ventas">';
        $supertot=0;
        foreach ($getv as $c) {
            $html .= '<tr>';
            $html .= '<td> <input type="hidden" id="name_suc" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
            $html .= '<td>$'.number_format($c->subtotal,2,".",",").'</td>';
            $html .= '<td>$'.number_format($c->iva,2,".",",").'</td>';
            $html .= '<td><input type="hidden" id="total" value="'.$c->total.'">$'.number_format($c->total,2,".",",").'</td>';
            $html .= '</tr>';
            $supertot=$supertot+$c->total;
        }                
        $html.='<tr>
                    <td colspan="2"></td>
                    <td><b>Total vendido: </b></td>
                    <td><input type="hidden" id="supertot" value="'.$supertot.'">$'.number_format($supertot,2,".",",").'</td>
                </tr>
            </tbody>';
        $html.='</table>';

        $getv=$this->Modeloventas->getTotalUtilidad($ids,$fechai,$fechaf);
        $htmlutilidad="";
        $htmlutilidad.='<table class="table table-sm" id="table_total_utilidad">';
            $htmlutilidad.='<thead>';
                $htmlutilidad.='<tr>';
                    $htmlutilidad.='<th>Sucursal</th>';
                    $htmlutilidad.='<th>Total Ventas</th>';
                    $htmlutilidad.='<th>Utilidad</th>';
                $htmlutilidad.='</tr>';
            $htmlutilidad.='</thead>';
            $htmlutilidad.='<tbody class="body_utilidad">';
        $supertot=0; $sub_ganancia = 0;
        foreach ($getv as $c) {
            $sub_ganancia = $c->total - $c->sub_compra_iva;
            $htmlutilidad .= '<tr>';
            $htmlutilidad .= '<td> <input type="hidden" id="name_suc_uti" value="'.$c->name_suc.'">'.$c->name_suc.'</td>';
            $htmlutilidad .= '<td>$'.number_format($c->total,2,".",",").'</td>';
            $htmlutilidad .= '<td><input type="hidden" id="total_utilidad" value="'.$sub_ganancia.'">$'.number_format($sub_ganancia,2,".",",").'</td>';
            $htmlutilidad .= '</tr>';
            $supertot=$supertot+$sub_ganancia;
        }                
        $htmlutilidad.='<tr>
                    <td></td>
                    <td><b>Total utilidad: </b></td>
                    <td><input type="hidden" id="suptot_uti" value="'.$supertot.'">$'.number_format($supertot,2,".",",").'</td>
                </tr>
            </tbody>';
        $htmlutilidad.='</table>';
        echo json_encode(array("html"=>$html,"htmlutilidad"=>$htmlutilidad));
    }
    
    function exportResultados($ids,$fi,$ff){
        $data["fechai"]=$fi;
        $data["fechaf"]=$ff;
        $data["empleado"]=$this->session->userdata('empleado');
        $data["getv"]=$this->Modeloventas->getTotalVentas($ids,$fi,$ff);

        $data["get_uti"]=$this->Modeloventas->getTotalUtilidad($ids,$fi,$ff);
        $this->load->view('reportes/pdfVentaSucs',$data);
    }

    function exportResultadosExcel($ids,$fi,$ff){
        $data["fechai"]=$fi;
        $data["fechaf"]=$ff;
        $data["empleado"]=$this->session->userdata('empleado');
        $data["getv"]=$this->Modeloventas->getTotalVentas($ids,$fi,$ff);
        $data["get_uti"]=$this->Modeloventas->getTotalUtilidad($ids,$fi,$ff);
        $this->load->view('reportes/excelVentaSucs',$data);
    }
}

