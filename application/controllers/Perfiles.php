<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perfiles extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');

        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,40);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=40;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('perfiles/index');
        $this->load->view('templates/footer');
        $this->load->view('perfiles/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=40;
        if($id>0){
            $data["perf"] = $this->General_model->get_record("perfilId",$id,"perfiles");
        }
        $data["menus"]=$this->General_model->get_menus();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('perfiles/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('perfiles/formjs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $datas = $this->General_model->getselectwhereall("perfiles",array("estatus"=>1));
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function registro_datos(){
        $data=$this->input->post();
        $array_perms = $data['array_perms'];
        unset($data["array_perms"]);
        $id=$data['perfilId'];
        if($id==0){
            $id=$this->General_model->add_record('perfiles',$data);
        }else{
            $this->General_model->edit_record('perfilId',$id,$data,'perfiles');
            $id=$id;
        }

        $DATAc = json_decode($array_perms);  
        for ($i=0;$i<count($DATAc);$i++) {
            $dataf = array(
                    'perfilId'=>$id,
                    'MenusubId'=>$DATAc[$i]->menu_sub,
                    'estatus'=>$DATAc[$i]->estatus
            );
            if($DATAc[$i]->id==0){
                $this->General_model->add_record('perfiles_detalles',$dataf);
            }else{
                $this->General_model->edit_record('Perfil_detalleId',$DATAc[$i]->id,$dataf,"perfiles_detalles");
            }
        }
        echo $id;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('id',$id,$data,'perfiles');
    }
}