INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Recargas', 'Recargas', 'fas fa-cart-plus', '5', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '35');


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recargas`
--

DROP TABLE IF EXISTS `recargas`;
CREATE TABLE IF NOT EXISTS `recargas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `capacidad` decimal(6,0) NOT NULL,
  `preciov` float NOT NULL,
  `precioc` float NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `sucursal` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recargas`
--

INSERT INTO `recargas` (`id`, `codigo`, `capacidad`, `preciov`, `precioc`, `tipo`, `fecha_reg`, `sucursal`, `estatus`) VALUES
(1, 'RTO215', '215', 185, 56.44, '1', '2023-12-20 13:32:51', 2, '1'),
(2, 'RTO410', '410', 190, 107.63, '1', '2023-12-20 13:37:32', 2, '1'),
(3, 'RTO680', '680', 195, 178.5, '1', '2023-12-20 13:38:04', 2, '1'),
(4, 'RTO1700', '1700', 270, 446.25, '1', '2023-12-20 13:38:25', 2, '1'),
(5, 'RTO3500', '3500', 880, 918.75, '1', '2023-12-20 13:38:51', 2, '1'),
(6, 'TO9500', '9500', 0, 11875, '0', '2023-12-20 16:52:52', 2, '1');
COMMIT;


ALTER TABLE venta_erp_detalle DROP FOREIGN KEY v_erp_producto;
ALTER TABLE `venta_erp_detalle` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '0=producto,1=recarga' AFTER `incluye_iva`;
ALTER TABLE `recargas` ADD `stock` DECIMAL(14,2) NOT NULL AFTER `tipo`;

/* ******************21-12-23********************** */
ALTER TABLE `compra_erp_detalle` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=producto,1=recarga' AFTER `incluye_iva`;

/* *********************22-12-23************************* */
ALTER TABLE `historial_transpasos` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=producto,1=recarga' AFTER `idproducto`;

/* ******************26-12-23********************** */
ALTER TABLE `productos_sucursales_serie` CHANGE `serie` `serie` VARCHAR(35) NOT NULL;
ALTER TABLE `historial_transpasos` ADD `idproducto_series` INT NOT NULL AFTER `tipo`;
<<<<<<< HEAD
ALTER TABLE `usuarios` ADD `comision` DECIMAL(10,2) NOT NULL AFTER `sucursal`;
ALTER TABLE `productos_sucursales_lote` ADD `idcompra` INT NOT NULL AFTER `caducidad`;
ALTER TABLE `historial_transpasos` ADD `idproducto_lote` INT NOT NULL AFTER `idproducto_series`;
=======
-----------------------------------------
ALTER TABLE `productos_sucursales` ADD `precio_final` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'solo informativo no se ocupa' AFTER `activo`;

---------------------------- solo se cambiaron a null las columnas
ALTER TABLE `productos` CHANGE `stock` `stock` INT(11) NULL DEFAULT NULL, CHANGE `stockmin` `stockmin` INT(11) NULL DEFAULT NULL, CHANGE `stockmax` `stockmax` INT(11) NULL DEFAULT NULL, CHANGE `ubicacion` `ubicacion` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `ancho` `ancho` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `alto` `alto` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `largo` `largo` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `peso` `peso` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `tiempo_carga` `tiempo_carga` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `costo_envio` `costo_envio` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `envio_gratis` `envio_gratis` TINYINT(1) NULL DEFAULT NULL, CHANGE `precio_sin_iva` `precio_sin_iva` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `precio_con_iva` `precio_con_iva` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `descuento` `descuento` DECIMAL(10,2) NULL DEFAULT NULL, CHANGE `tipo_descuento` `tipo[...]

ALTER TABLE `venta_erp_detalle` ADD `idpro` INT NULL DEFAULT NULL COMMENT 'solo 2' AFTER `activo`, ADD `idserie` BIGINT NULL DEFAULT NULL COMMENT 'solo 2' AFTER `idpro`, ADD `periodo` INT NULL DEFAULT NULL COMMENT 'solo 2' AFTER `idserie`;
