-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-10-2024 a las 16:30:59
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwsemi_calidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operadores`
--

CREATE TABLE `operadores` (
  `id` int(11) NOT NULL,
  `TipoFigura` varchar(2) NOT NULL,
  `rfc_del_operador` varchar(50) NOT NULL,
  `no_licencia` varchar(50) DEFAULT NULL,
  `operador` varchar(200) NOT NULL,
  `num_identificacion` varchar(50) NOT NULL,
  `residencia_fiscal` varchar(50) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `num_ext` varchar(10) DEFAULT NULL,
  `num_int` varchar(10) DEFAULT NULL,
  `colo` varchar(10) DEFAULT NULL,
  `loca` varchar(10) DEFAULT NULL,
  `ref` varchar(300) DEFAULT NULL,
  `muni` varchar(10) DEFAULT NULL,
  `estado` varchar(100) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `operadores`
--

INSERT INTO `operadores` (`id`, `TipoFigura`, `rfc_del_operador`, `no_licencia`, `operador`, `num_identificacion`, `residencia_fiscal`, `calle`, `num_ext`, `num_int`, `colo`, `loca`, `ref`, `muni`, `estado`, `pais`, `codigo_postal`, `activo`, `reg`) VALUES
(1, '01', 'CACE611006IT4', 'GC35216', 'Emilio  Castillo Castillo', '', '', '', '', '', '', '', '', '', 'MEX', 'MEX', '50075', 1, '2022-12-02 04:16:10'),
(2, '02', '34534534', '', '345345', '', '', '', '', '', '', 'puebla', '', '', 'PUE', 'MEX', '72500', 1, '0000-00-00 00:00:00'),
(3, '02', '345345', '', '34534', '', '', '', '', '', '', 'puebla', '', '', 'PUE', 'MEX', '72500', 1, '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `operadores`
--
ALTER TABLE `operadores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `operadores`
--
ALTER TABLE `operadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
