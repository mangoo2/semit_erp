-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-10-2024 a las 16:05:36
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwsemi_calidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operadores`
--

CREATE TABLE `operadores` (
  `id` int(11) NOT NULL,
  `TipoFigura` varchar(2) NOT NULL,
  `rfc_del_operador` varchar(50) NOT NULL,
  `no_licencia` varchar(50) DEFAULT NULL,
  `operador` varchar(200) NOT NULL,
  `num_identificacion` varchar(50) NOT NULL,
  `residencia_fiscal` varchar(50) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `num_ext` varchar(10) DEFAULT NULL,
  `num_int` varchar(10) DEFAULT NULL,
  `colo` varchar(10) DEFAULT NULL,
  `loca` varchar(10) DEFAULT NULL,
  `ref` varchar(300) DEFAULT NULL,
  `muni` varchar(10) DEFAULT NULL,
  `estado` varchar(100) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `operadores`
--

INSERT INTO `operadores` (`id`, `TipoFigura`, `rfc_del_operador`, `no_licencia`, `operador`, `num_identificacion`, `residencia_fiscal`, `calle`, `num_ext`, `num_int`, `colo`, `loca`, `ref`, `muni`, `estado`, `pais`, `codigo_postal`, `activo`, `reg`) VALUES
(1, '01', 'CACE611006IT4', 'GC35216', 'Emilio  Castillo Castillo', '', '', '', '', '', '', '', '', '', 'MEX', 'MEX', '50075', 1, '2022-12-02 04:16:10'),
(2, '02', '34534534', '', '345345', '', '', '', '', '', '', 'puebla', '', '', 'PUE', 'MEX', '72500', 1, '0000-00-00 00:00:00'),
(3, '03', '345345', '', '34534', '', '', '', '', '', '', 'puebla', '', '', 'PUE', 'MEX', '72500', 1, '0000-00-00 00:00:00'),
(4, '04', 'CACE611006IT4', 'GC35216', 'Emilio  Castillo Castillo', '', '', '', '', '', '', '', '', '', 'MEX', 'MEX', '50075', 1, '2022-12-02 04:16:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantilla_carta_porte`
--

CREATE TABLE `plantilla_carta_porte` (
  `id` int(11) NOT NULL,
  `txtNumDocumento` int(11) NOT NULL,
  `txtIdentificador` varchar(30) NOT NULL,
  `selectTipoComprobante` varchar(1) NOT NULL,
  `selectTipoFecha` varchar(3) NOT NULL,
  `txtFechaInicio` date DEFAULT NULL,
  `hora_salida` time DEFAULT NULL,
  `txtFechaFin` date DEFAULT NULL,
  `hora_llegada` time DEFAULT NULL,
  `TranspInternac` varchar(2) DEFAULT NULL,
  `selectVehiculos` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plantilla_carta_porte`
--

INSERT INTO `plantilla_carta_porte` (`id`, `txtNumDocumento`, `txtIdentificador`, `selectTipoComprobante`, `selectTipoFecha`, `txtFechaInicio`, `hora_salida`, `txtFechaFin`, `hora_llegada`, `TranspInternac`, `selectVehiculos`, `activo`, `reg`) VALUES
(1, 2, '', 'T', 'SA', '2024-08-05', NULL, '2024-08-05', NULL, 'No', 11, 1, '2024-08-05 22:53:45'),
(2, 3, '', 'T', 'SA', '2024-07-19', NULL, '2024-07-19', NULL, 'No', 11, 1, '2024-10-21 21:09:23'),
(3, 4, '', 'T', 'SA', '2024-07-19', NULL, '2024-07-19', NULL, 'No', 11, 1, '2024-10-21 21:12:59'),
(4, 5, '', 'T', 'SA', '2024-07-19', NULL, '2024-07-19', NULL, 'No', 11, 1, '2024-10-21 21:18:03'),
(5, 6, '', 'T', 'SA', '2024-07-19', NULL, '2024-07-19', NULL, 'No', 11, 1, '2024-10-21 21:20:24'),
(6, 7, '', 'T', 'SA', '2024-07-19', NULL, '2024-07-19', NULL, 'No', 11, 1, '2024-10-21 21:22:45'),
(7, 8, '', 'T', 'SA', '2023-12-18', NULL, '2023-12-18', NULL, 'No', 11, 1, '2024-10-21 21:26:24'),
(8, 9, '', 'T', 'SA', '2024-01-04', NULL, '2024-01-04', NULL, 'No', 11, 1, '2024-10-21 21:29:59'),
(9, 10, '', 'T', 'SA', '2024-07-22', NULL, '2024-07-22', NULL, 'No', 1, 1, '2024-10-21 21:33:09'),
(10, 11, '', 'T', 'SA', '2023-12-07', NULL, '2023-12-07', NULL, 'No', 11, 1, '2024-10-21 21:40:34'),
(11, 12, '', 'T', 'SA', '2023-12-26', NULL, '2023-12-26', NULL, 'No', 11, 1, '2024-10-21 21:43:15'),
(12, 13, '', 'T', 'SA', '2023-12-26', NULL, '2023-12-26', NULL, 'No', 11, 1, '2024-10-21 21:48:55'),
(13, 14, '', 'T', 'SA', '2023-09-14', NULL, '2023-09-14', NULL, 'No', 11, 1, '2024-10-21 21:52:34'),
(14, 15, '', 'T', 'SA', '2023-12-26', NULL, '2023-12-26', NULL, 'No', 11, 1, '2024-10-21 21:56:41'),
(15, 16, '', 'T', 'SA', '2023-12-26', NULL, '2023-12-26', NULL, 'No', 11, 1, '2024-10-21 22:00:00'),
(16, 17, '', 'T', 'SA', '2023-11-28', NULL, '2023-11-28', NULL, 'No', 11, 1, '2024-10-21 22:01:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantilla_carta_porte_figuras`
--

CREATE TABLE `plantilla_carta_porte_figuras` (
  `id` int(11) NOT NULL,
  `id_pcp` int(11) NOT NULL,
  `id_fig` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plantilla_carta_porte_figuras`
--

INSERT INTO `plantilla_carta_porte_figuras` (`id`, `id_pcp`, `id_fig`, `activo`, `reg`) VALUES
(1, 1, 1, 1, '2024-10-19 00:48:09'),
(2, 1, 2, 0, '2024-10-19 00:48:09'),
(3, 1, 3, 0, '2024-10-19 00:48:10'),
(4, 1, 4, 0, '2024-10-19 00:48:10'),
(5, 2, 1, 1, '2024-10-21 21:11:41'),
(6, 3, 1, 1, '2024-10-21 21:17:08'),
(7, 4, 1, 1, '2024-10-21 21:19:34'),
(8, 5, 1, 1, '2024-10-21 21:21:27'),
(9, 6, 1, 1, '2024-10-21 21:24:14'),
(10, 7, 1, 1, '2024-10-21 21:27:44'),
(11, 8, 1, 1, '2024-10-21 21:31:11'),
(12, 9, 1, 1, '2024-10-21 21:38:20'),
(13, 10, 1, 1, '2024-10-21 21:42:13'),
(14, 11, 1, 1, '2024-10-21 21:48:02'),
(15, 12, 1, 1, '2024-10-21 21:50:03'),
(16, 13, 1, 1, '2024-10-21 21:55:56'),
(17, 14, 1, 1, '2024-10-21 21:58:07'),
(18, 15, 1, 1, '2024-10-21 22:01:16'),
(19, 16, 1, 1, '2024-10-21 22:03:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantilla_carta_porte_ubicaciones`
--

CREATE TABLE `plantilla_carta_porte_ubicaciones` (
  `id` int(11) NOT NULL,
  `id_pcp` int(11) NOT NULL,
  `TipoUbicacion` varchar(10) NOT NULL,
  `id_ubicacion` int(11) NOT NULL,
  `fechasalida` datetime NOT NULL,
  `DistanciaRecorrida` int(11) DEFAULT NULL COMMENT 'solo destino',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plantilla_carta_porte_ubicaciones`
--

INSERT INTO `plantilla_carta_porte_ubicaciones` (`id`, `id_pcp`, `TipoUbicacion`, `id_ubicacion`, `fechasalida`, `DistanciaRecorrida`, `activo`, `reg`) VALUES
(1, 1, 'Origen', 8, '2024-10-15 00:00:00', NULL, 1, '2024-10-15 23:33:45'),
(2, 1, 'Destino', 8, '2024-10-15 18:43:00', 100, 0, '2024-10-16 00:43:44'),
(3, 1, 'Destino', 8, '2024-10-18 15:54:00', 100, 0, '2024-10-18 21:55:04'),
(4, 1, 'Destino', 4, '2024-07-16 11:29:00', 2, 1, '2024-10-21 17:59:12'),
(5, 1, 'Destino', 4, '2024-07-16 11:29:00', 2, 1, '2024-10-21 18:00:46'),
(6, 2, 'Origen', 4, '2024-07-19 11:10:00', NULL, 1, '2024-10-21 21:11:41'),
(7, 2, 'Destino', 2, '2024-07-19 11:10:00', 4, 1, '2024-10-21 21:11:41'),
(8, 3, 'Origen', 2, '2024-07-19 11:16:00', NULL, 1, '2024-10-21 21:17:08'),
(9, 3, 'Destino', 3, '2024-07-19 15:16:00', 4, 1, '2024-10-21 21:17:08'),
(10, 4, 'Origen', 3, '2024-07-19 15:18:00', NULL, 1, '2024-10-21 21:19:34'),
(11, 4, 'Destino', 5, '2024-07-19 15:19:00', 6, 1, '2024-10-21 21:19:34'),
(12, 5, 'Origen', 2, '0000-00-00 00:00:00', NULL, 1, '2024-10-21 21:21:27'),
(13, 5, 'Destino', 5, '2024-07-19 15:21:00', 6, 1, '2024-10-21 21:21:27'),
(14, 6, 'Origen', 2, '0000-00-00 00:00:00', NULL, 1, '2024-10-21 21:24:06'),
(15, 6, 'Destino', 4, '2023-12-16 15:23:00', 3, 1, '2024-10-21 21:24:06'),
(16, 6, 'Origen', 2, '2023-12-16 15:25:00', NULL, 1, '2024-10-21 21:24:14'),
(17, 6, 'Destino', 4, '2023-12-16 15:23:00', 3, 0, '2024-10-21 21:24:14'),
(18, 7, 'Origen', 2, '2023-12-18 15:26:00', NULL, 1, '2024-10-21 21:27:43'),
(19, 7, 'Destino', 3, '2023-12-18 15:27:00', 6, 1, '2024-10-21 21:27:44'),
(20, 8, 'Origen', 5, '0000-00-00 00:00:00', NULL, 1, '2024-10-21 21:31:11'),
(21, 8, 'Destino', 2, '2024-01-04 15:30:00', 5, 1, '2024-10-21 21:31:11'),
(22, 9, 'Origen', 5, '2024-07-22 15:35:00', NULL, 1, '2024-10-21 21:36:32'),
(23, 9, 'Destino', 4, '2023-12-22 15:36:00', 6, 1, '2024-10-21 21:36:32'),
(24, 10, 'Origen', 5, '2023-12-07 15:41:00', NULL, 1, '2024-10-21 21:42:13'),
(25, 10, 'Destino', 3, '2023-12-07 15:41:00', 7, 1, '2024-10-21 21:42:13'),
(26, 11, 'Origen', 4, '0000-00-00 00:00:00', NULL, 1, '2024-10-21 21:48:01'),
(27, 11, 'Destino', 2, '2024-10-26 15:47:00', 2, 1, '2024-10-21 21:48:01'),
(28, 12, 'Origen', 4, '2024-10-26 15:49:00', NULL, 1, '2024-10-21 21:50:03'),
(29, 12, 'Destino', 5, '2024-10-21 15:49:00', 7, 1, '2024-10-21 21:50:03'),
(30, 13, 'Origen', 4, '2023-09-14 15:54:00', NULL, 1, '2024-10-21 21:55:56'),
(31, 13, 'Destino', 3, '2023-09-14 15:54:00', 7, 1, '2024-10-21 21:55:56'),
(32, 14, 'Origen', 3, '2023-12-26 15:57:00', NULL, 1, '2024-10-21 21:58:07'),
(33, 14, 'Destino', 2, '2023-12-26 15:57:00', 5, 1, '2024-10-21 21:58:07'),
(34, 15, 'Origen', 3, '0000-00-00 00:00:00', NULL, 1, '2024-10-21 22:01:16'),
(35, 15, 'Destino', 5, '2023-12-26 16:00:00', 7, 1, '2024-10-21 22:01:16'),
(36, 16, 'Origen', 3, '2024-10-28 14:02:00', NULL, 1, '2024-10-21 22:03:21'),
(37, 16, 'Destino', 4, '2023-11-28 16:02:00', 7, 1, '2024-10-21 22:03:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL,
  `clave` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_alias` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name_suc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domicilio` text COLLATE utf8_unicode_ci,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `direccion` text COLLATE utf8_unicode_ci NOT NULL,
  `orden` tinyint(4) NOT NULL,
  `con_insumos` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=no,1=incluye prods tipo insumo de sucursal propia',
  `cp_calle` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_n_ex` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_n_int` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_colonia` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_localidad` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_ref` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_minicipio` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_estado` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_cl_estado` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_pais` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_cp` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `clave`, `id_alias`, `name_suc`, `tel`, `domicilio`, `activo`, `direccion`, `orden`, `con_insumos`, `cp_calle`, `cp_n_ex`, `cp_n_int`, `cp_colonia`, `cp_localidad`, `cp_ref`, `cp_minicipio`, `cp_estado`, `cp_cl_estado`, `cp_pais`, `cp_cp`) VALUES
(1, 'SM', '', 'Matriz', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 0, '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SPN', '02', 'Pino Suárez', ' (722) 454 22 23', 'José Maria Pino Suarez, No 722 COL Cuauhtemoc CP 50130 TOLUCA ESTADO DE MEXICO', 1, 'av sur 4', 2, '0', 'Jose Maria Pino Suarez', '722', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50130'),
(3, 'SADM', '05', 'Alfredo del Mazo', '(722) 454 02 12', 'Boulevard Alfredo del Mazo No. 727 local 3 Plaza El Punto. Col. Científicos. ', 1, '', 5, '0', 'Blvd. Alfredo del Mazo', '727', '3', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50075'),
(4, 'SJC', '03', 'Jesús Carranza', '(722) 454 02 16', 'Jesús Carranza Sur, No. 323, (entre las Torres y Tollocan) Toluca México. ', 1, '', 3, '0', 'Jesus Carranza Sur', '323', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50180'),
(5, 'SM', '04', 'Metepec', '(722) 454 02 19', 'Av. Benito Juárez, No. 528, Barrio San Mateo (a 300 m del Centro Médico Toluca)', 1, '', 4, '0', 'Av. Benito Juarez', '528', 'A', '', '08', '', '054', 'MEX', 'MEX', 'MEX', '52140'),
(6, 'SAW', '06', 'Almacén WEB', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 1, '', 6, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'SAR', '07', 'Almacén Rentas', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 1, '', 7, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'SAG', '01', 'Almacén General', '(722) 217 26 26', 'FUERTE DE LORETO S/N  (ENTRE AVENIDA IGNACIO ZARAGOZA Y 1° DE MAYO) COL HEROES DE 5 MAYO, CP 50170, TOLUCA MEXICO, HORARIO: LUNES A VIERNES 9:30am a 13:30pm y de 14:00pm a 17:00pm', 1, '', 1, '0', 'Fuerte de Loreto', 'SN', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50170'),
(9, 'SPM', '08', 'Punto de Venta Móvil', '6456456456456', 'PUNTO DE VENTA MOVIL', 1, '', 9, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'DRDT', '09', 'La Bombonera', '45765756563', 'La Bombonera', 1, '', 9, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'B2', '10', 'LA BOMBONERA 2', '4324234523523', 'LA BOMBONERA 2', 1, '', 10, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte_federal`
--

CREATE TABLE `transporte_federal` (
  `id` int(11) NOT NULL,
  `txtCodigo` int(11) NOT NULL,
  `txtNombre` varchar(300) NOT NULL,
  `nombre_aseguradora` varchar(255) DEFAULT NULL,
  `num_poliza_seguro` varchar(100) DEFAULT NULL,
  `num_permiso_sct` varchar(100) DEFAULT NULL,
  `configuracion_vhicular` varchar(255) DEFAULT NULL,
  `placa_vehiculo_motor` varchar(50) DEFAULT NULL,
  `anio_modelo_vihiculo_motor` varchar(50) DEFAULT NULL,
  `subtipo_remolque` varchar(100) DEFAULT NULL,
  `placa_remolque` varchar(50) DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  `tipo_permiso_sct` varchar(50) DEFAULT NULL,
  `AseguraMedAmbiente` varchar(300) DEFAULT NULL,
  `PolizaMedAmbiente` varchar(10) DEFAULT NULL,
  `AseguraCarga` varchar(30) DEFAULT NULL,
  `PolizaCarga` varchar(30) DEFAULT NULL,
  `PrimaSeguro` decimal(15,2) DEFAULT NULL,
  `subtipo2_remolque` varchar(100) DEFAULT NULL,
  `placa2_remolque` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transporte_federal`
--

INSERT INTO `transporte_federal` (`id`, `txtCodigo`, `txtNombre`, `nombre_aseguradora`, `num_poliza_seguro`, `num_permiso_sct`, `configuracion_vhicular`, `placa_vehiculo_motor`, `anio_modelo_vihiculo_motor`, `subtipo_remolque`, `placa_remolque`, `activo`, `reg`, `tipo_permiso_sct`, `AseguraMedAmbiente`, `PolizaMedAmbiente`, `AseguraCarga`, `PolizaCarga`, `PrimaSeguro`, `subtipo2_remolque`, `placa2_remolque`) VALUES
(1, 1, 'NISSAN NV350 URVAN PANEL', 'QUALITAS', '0780600392', 'ca-c-11898316', 'VL', 'LF04620', '2022', 'CTR001', '', 1, '2022-12-02 06:13:14', 'TPAF02', '', '', '', '', 0.00, '', ''),
(11, 2, 'TOYOTA HAICE PANEL', 'QUALITAS', '0780600705', 'AU-C-10703275', 'VL', 'NWH7365', '2013', '', '', 1, '0000-00-00 00:00:00', 'TPAF02', '', '', '', '', 0.00, '', ''),
(12, 3, 'NISSAN NP300 PICK UP', 'QUALITAS', '0780594551', 'AU-C-9747330', 'VL', 'NVJ6672', '2012', '', '', 1, '0000-00-00 00:00:00', 'TPAF02', '', '', '', '', 0.00, '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `operadores`
--
ALTER TABLE `operadores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plantilla_carta_porte`
--
ALTER TABLE `plantilla_carta_porte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_cartap_fk_vehiculos` (`selectVehiculos`);

--
-- Indices de la tabla `plantilla_carta_porte_figuras`
--
ALTER TABLE `plantilla_carta_porte_figuras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_cartap_fk_figura` (`id_pcp`),
  ADD KEY `p_cartaf_fj_cat_fig` (`id_fig`);

--
-- Indices de la tabla `plantilla_carta_porte_ubicaciones`
--
ALTER TABLE `plantilla_carta_porte_ubicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pcp_ubi_fk_ubi` (`id_pcp`),
  ADD KEY `pcp_ubic_fk_ubica` (`id_ubicacion`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `transporte_federal`
--
ALTER TABLE `transporte_federal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `operadores`
--
ALTER TABLE `operadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `plantilla_carta_porte`
--
ALTER TABLE `plantilla_carta_porte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `plantilla_carta_porte_figuras`
--
ALTER TABLE `plantilla_carta_porte_figuras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `plantilla_carta_porte_ubicaciones`
--
ALTER TABLE `plantilla_carta_porte_ubicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `transporte_federal`
--
ALTER TABLE `transporte_federal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `plantilla_carta_porte`
--
ALTER TABLE `plantilla_carta_porte`
  ADD CONSTRAINT `p_cartap_fk_vehiculos` FOREIGN KEY (`selectVehiculos`) REFERENCES `transporte_federal` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plantilla_carta_porte_figuras`
--
ALTER TABLE `plantilla_carta_porte_figuras`
  ADD CONSTRAINT `p_cartaf_fj_cat_fig` FOREIGN KEY (`id_fig`) REFERENCES `operadores` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `p_cartap_fk_figura` FOREIGN KEY (`id_pcp`) REFERENCES `plantilla_carta_porte` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plantilla_carta_porte_ubicaciones`
--
ALTER TABLE `plantilla_carta_porte_ubicaciones`
  ADD CONSTRAINT `pcp_ubi_fk_ubi` FOREIGN KEY (`id_pcp`) REFERENCES `plantilla_carta_porte` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `pcp_ubic_fk_ubica` FOREIGN KEY (`id_ubicacion`) REFERENCES `sucursal` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
