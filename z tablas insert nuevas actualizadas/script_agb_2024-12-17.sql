ALTER TABLE `compra_erp_detalle` ADD `cant_inicial` INT NULL DEFAULT '0' AFTER `cantidad`, ADD `cant_final` INT NULL DEFAULT '0' AFTER `cant_inicial`;
ALTER TABLE `venta_erp_detalle` ADD `cant_inicial` INT NULL DEFAULT '0' AFTER `cantidad`, ADD `cant_final` INT NULL DEFAULT '0' AFTER `cant_inicial`;

ALTER TABLE `bitacora_ajustes` ADD `cant_final` INT NULL DEFAULT '0' AFTER `cantidad_ajuste`;

ALTER TABLE `bitacora_ajustes` CHANGE `fecha_reg` `fecha_reg` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `historial_transpasos` ADD `cant_ini_d` INT NULL DEFAULT '0' AFTER `cantidad`;

---------------------------------------
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '4', 'Conf. facturación', 'Conf_facturacion', 'settings', '8', '0', '0', '0');/* tiene que ser id 53*/
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '53', '1');
---------------------
INSERT INTO `productos_sucursales_serie`( `productoid`, `sucursalid`, `serie`, `idcompras`, `disponible`, `vendido`) VALUES 
(1542,8,'2052324',0,1,0),
(1542,8,'2068271',0,1,0),
(1542,8,'2068620',0,1,0),
(1542,8,'2071311',0,1,0),
(1542,8,'2066171',0,1,0),
(1542,8,'2068584',0,1,0),
(1542,8,'2068622',0,1,0),
(1542,8,'2071357',0,1,0),
(1542,8,'2071357',0,1,0),
(1542,8,'2066202',0,1,0),
(1542,8,'2068602',0,1,0),
(1542,8,'2068628',0,1,0),
(1542,8,'2071358',0,1,0),
(1542,8,'2066213',0,1,0),
(1542,8,'2068615',0,1,0),
(1542,8,'2070918',0,1,0),
(1542,8,'2072015',0,1,0),
(1542,8,'2066862',0,1,0),
(1542,8,'2068618',0,1,0),
(1542,8,'2071262',0,1,0),
(1542,8,'2072048',0,1,0);


INSERT INTO `productos_sucursales_serie`( `productoid`, `sucursalid`, `serie`, `idcompras`, `disponible`, `vendido`) VALUES 
(1336,2,'23100118',0,1,0),
(1364,2,'MM263792',0,1,0),
(1368,2,'JJ145704',0,1,0),
(1372,2,'Y371539',0,1,0),
(1372,2,'Y372057',0,1,0),
(1375,2,'KK037622',0,1,0),
(1375,2,'KK037022',0,1,0),
(1375,2,'KK038672',0,1,0),
(1375,2,'KK038673',0,1,0),
(1375,2,'KK038730',0,1,0),
(1375,2,'KK038807',0,1,0),
(1375,2,'KK038694',0,1,0),
(1375,2,'A239220183DS',0,1,0),
(1529,2,'2056884',0,1,0),
(1529,2,'2070600',0,1,0),
(1529,2,'2071775',0,1,0),
(1529,2,'2072567',0,1,0),
(1529,2,'2070416',0,1,0),
(1546,2,'F24801045DS',0,1,0),
(1546,2,'F24801076DS',0,1,0),
(1364,4,'MM263784',0,1,0),
(1368,4,'JJ145733',0,1,0),
(1372,4,'Y371983',0,1,0),
(1373,4,'Y378614',0,1,0),
(1529,4,'2068595',0,1,0),
(1529,4,'2071369',0,1,0),
(1546,4,'F24801039DS',0,1,0),
(1562,4,'A248040056DS',0,1,0),
(1563,4,'B247310156DS',0,1,0),
(1372,5,'Y2811333',0,1,0),
(1375,5,'KK038727',0,1,0),
(1375,5,'KK038708',0,1,0),
(1375,5,'KK038588',0,1,0),
(1375,5,'KK037655',0,1,0),
(1521,5,'A239220192DS',0,1,0),
(1529,5,'2072036',0,1,0),
(1529,5,'2070605',0,1,0),
(1546,5,'F24729107DS',0,1,0),
(1562,5,'A248040006DS',0,1,0),
(1332,3,'24207541',0,1,0),
(1562,3,'MM263793',0,1,0),
(1373,3,'Y336440',0,1,0),
(1375,3,'KK037592',0,1,0),
(1375,3,'KK038716',0,1,0),
(1375,3,'KK038707',0,1,0),
(1375,3,'KK038678',0,1,0),
(1529,3,'2062688',0,1,0),
(1529,3,'2017782',0,1,0);

INSERT INTO `productos_sucursales_serie`( `productoid`, `sucursalid`, `serie`, `idcompras`, `disponible`, `vendido`) VALUES 
(6097,8,'KK038699',0,1,0),
(6097,8,'KK038736',0,1,0),
(6097,8,'KK038650',0,1,0),
(6097,8,'KK037690',0,1,0),
(6097,8,'KK036084',0,1,0),
(6097,8,'KK037804',0,1,0),
(6097,8,'KK035172',0,1,0),
(6097,8,'KK038683',0,1,0),
(6097,8,'KK038629',0,1,0),
(6097,8,'KK038677',0,1,0),
(6097,8,'KK038718',0,1,0),
(6097,8,'KK038714',0,1,0),
(6097,8,'KK038721',0,1,0),
(6097,8,'KK039141',0,1,0),
(6097,8,'KK038684',0,1,0),
(6097,8,'KK038720',0,1,0),
(6097,8,'KK038590',0,1,0),
(6097,8,'KK037462',0,1,0),
(6097,8,'KK038722',0,1,0),
(6097,8,'KK038798',0,1,0),
(6097,8,'KK038696',0,1,0),
(6097,8,'KK039070',0,1,0),
(6097,8,'KK039512',0,1,0),
(6097,8,'KK039022',0,1,0),
(6097,8,'KK038689',0,1,0),
(6097,8,'KK038693',0,1,0),
(6097,8,'KK038703',0,1,0),
(6097,8,'KK038742',0,1,0),
(6097,8,'KK038746',0,1,0),
(6097,8,'KK034856',0,1,0),
(6097,8,'KK038687',0,1,0),
(6097,8,'KK037784',0,1,0),
(6097,8,'KK038668',0,1,0),
(6097,8,'KK038697',0,1,0),
(6097,4,'KK038712',0,1,0),
(6097,4,'KK038688',0,1,0),
(6097,4,'KK038723',0,1,0),
(6097,4,'KK038757',0,1,0),
(6097,4,'KK039289',0,1,0);

-----------------------------------------------------------------------------
CREATE TABLE `timbres` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `tv_inicio` date DEFAULT NULL,
  `tv_fin` date DEFAULT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `timbres`
--
ALTER TABLE `timbres`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `timbres`
--
ALTER TABLE `timbres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
---------------------------------------------------------------------------------
INSERT INTO `timbres` (`id`, `cantidad`, `tv_inicio`, `tv_fin`, `reg`, `activo`) VALUES (NULL, '10000', '2023-01-09', '2025-12-31', CURRENT_TIMESTAMP, '1');
-----------------------------------------------
ALTER TABLE `arqueos` ADD `id_turno` INT NULL DEFAULT NULL AFTER `view`;

-------------------
ALTER TABLE `f_facturas` ADD `mensajeerror` TEXT NULL DEFAULT NULL AFTER `cartaporte`;
-----------
ALTER TABLE `f_facturas` ADD `tim` TINYINT(1) NULL DEFAULT '1' COMMENT 'si es 0 solo el formato (carta porte)' AFTER `mensajeerror`;
--------------------------
ALTER TABLE `venta_erp` ADD `id_razonsocial_new` INT NULL DEFAULT NULL COMMENT 'solo aplica cuando es PG a otra razon social' AFTER `id_razonsocial`;
