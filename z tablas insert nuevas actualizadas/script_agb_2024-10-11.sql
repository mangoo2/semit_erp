ALTER TABLE `sucursal` ADD `cp_calle` VARCHAR(300) NULL DEFAULT NULL AFTER `con_insumos`, 
ADD `cp_n_ex` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_calle`, ADD `cp_n_int` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_n_ex`, 
ADD `cp_colonia` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_n_int`, ADD `cp_localidad` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_colonia`, 
ADD `cp_ref` VARCHAR(300) NULL DEFAULT NULL AFTER `cp_localidad`, ADD `cp_minicipio` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_ref`, 
ADD `cp_estado` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_minicipio`, ADD `cp_cl_estado` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_estado`, 
ADD `cp_pais` VARCHAR(15) NULL DEFAULT NULL AFTER `cp_cl_estado`, ADD `cp_cp` VARCHAR(6) NULL DEFAULT NULL AFTER `cp_pais`;

UPDATE `sucursal` SET `cp_calle` = 'Fuerte de Loreto', `cp_n_ex` = 'SN', `cp_n_int` = '', `cp_colonia` = '', `cp_localidad` = '20', `cp_ref` = '', `cp_minicipio` = '106', `cp_estado` = 'MEX', `cp_cl_estado` = 'MEX', `cp_pais` = 'MEX', `cp_cp` = '50170'
WHERE `id` = '8' ;

UPDATE `sucursal` SET `cp_calle` = 'Blvd. Alfredo del Mazo', `cp_n_ex` = '727', `cp_n_int` = '3', `cp_colonia` = '', `cp_localidad` = '20', `cp_ref` = '', `cp_minicipio` = '106', `cp_estado` = 'MEX', `cp_cl_estado` = 'MEX', `cp_pais` = 'MEX', `cp_cp` = '50075'
WHERE `id` = '3' ;

UPDATE `sucursal` SET `cp_calle` = 'Av. Benito Juarez', `cp_n_ex` = '528', `cp_n_int` = 'A', `cp_colonia` = '', `cp_localidad` = '08', `cp_ref` = '', `cp_minicipio` = '054', `cp_estado` = 'MEX', `cp_cl_estado` = 'MEX', `cp_pais` = 'MEX', `cp_cp` = '52140'
WHERE `id` = '5' ;

UPDATE `sucursal` SET `cp_calle` = 'Jesus Carranza Sur', `cp_n_ex` = '323', `cp_n_int` = '', `cp_colonia` = '', `cp_localidad` = '20', `cp_ref` = '', `cp_minicipio` = '106', `cp_estado` = 'MEX', `cp_cl_estado` = 'MEX', `cp_pais` = 'MEX', `cp_cp` = '50180'
WHERE `id` = '4';
UPDATE `sucursal` SET `cp_calle` = 'Jose Maria Pino Suarez', `cp_n_ex` = '722', `cp_n_int` = '', `cp_colonia` = '', `cp_localidad` = '20', `cp_ref` = '', `cp_minicipio` = '106', `cp_estado` = 'MEX', `cp_cl_estado` = 'MEX', `cp_pais` = 'MEX', `cp_cp` = '50130'
WHERE `id` = '2';



INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Figuras', 'Cat_complemento/figuras', NULL, '', '', '1', '42');/* sera id 50 */

ALTER TABLE `operadores` ADD `num_ext` VARCHAR(10) NULL DEFAULT NULL AFTER `calle`;

ALTER TABLE `operadores` ADD `num_int` VARCHAR(10) NULL DEFAULT NULL AFTER `num_ext`;

ALTER TABLE `operadores` ADD `colo` VARCHAR(10) NULL DEFAULT NULL AFTER `num_int`, ADD `loca` VARCHAR(10) NULL DEFAULT NULL AFTER `colo`, ADD `ref` VARCHAR(300) NULL DEFAULT NULL AFTER `loca`, ADD `muni` VARCHAR(10) NULL DEFAULT NULL AFTER `ref`;

ALTER TABLE `f_servicios` ADD `material_peligroso` VARCHAR(10) NULL DEFAULT '0' AFTER `precio`;


ALTER TABLE `f_facturas` ADD `cartaporte` TINYINT(1) NULL DEFAULT '0' AFTER `envio_m_p`;

ALTER TABLE `traspasos` ADD `Idfactura` INT NULL DEFAULT NULL AFTER `tipo`;

ALTER TABLE `traspasos` ADD `catp_operador` INT NULL DEFAULT NULL AFTER `Idfactura`, ADD `catp_vehiculo` INT NULL DEFAULT NULL AFTER `catp_operador`, ADD `catp_salida` DATETIME NULL DEFAULT NULL AFTER `catp_vehiculo`, ADD `catp_llegada` DATETIME NULL DEFAULT NULL AFTER `catp_salida`;

ALTER TABLE `f_facturas_ubicaciones` ADD `id_ubicacion` INT NULL DEFAULT NULL COMMENT 'id de la sucursal' AFTER `activo`;
