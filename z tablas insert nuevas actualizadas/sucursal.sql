-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-10-2024 a las 16:31:35
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwsemi_calidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL,
  `clave` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_alias` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name_suc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domicilio` text COLLATE utf8_unicode_ci,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `direccion` text COLLATE utf8_unicode_ci NOT NULL,
  `orden` tinyint(4) NOT NULL,
  `con_insumos` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=no,1=incluye prods tipo insumo de sucursal propia',
  `cp_calle` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_n_ex` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_n_int` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_colonia` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_localidad` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_ref` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_minicipio` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_estado` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_cl_estado` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_pais` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_cp` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `clave`, `id_alias`, `name_suc`, `tel`, `domicilio`, `activo`, `direccion`, `orden`, `con_insumos`, `cp_calle`, `cp_n_ex`, `cp_n_int`, `cp_colonia`, `cp_localidad`, `cp_ref`, `cp_minicipio`, `cp_estado`, `cp_cl_estado`, `cp_pais`, `cp_cp`) VALUES
(1, 'SM', '', 'Matriz', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 0, '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SPN', '02', 'Pino Suárez', ' (722) 454 22 23', 'José Maria Pino Suarez, No 722 COL Cuauhtemoc CP 50130 TOLUCA ESTADO DE MEXICO', 1, 'av sur 4', 2, '0', 'Jose Maria Pino Suarez', '722', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50130'),
(3, 'SADM', '05', 'Alfredo del Mazo', '(722) 454 02 12', 'Boulevard Alfredo del Mazo No. 727 local 3 Plaza El Punto. Col. Científicos. ', 1, '', 5, '0', 'Blvd. Alfredo del Mazo', '727', '3', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50075'),
(4, 'SJC', '03', 'Jesús Carranza', '(722) 454 02 16', 'Jesús Carranza Sur, No. 323, (entre las Torres y Tollocan) Toluca México. ', 1, '', 3, '0', 'Jesus Carranza Sur', '323', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50180'),
(5, 'SM', '04', 'Metepec', '(722) 454 02 19', 'Av. Benito Juárez, No. 528, Barrio San Mateo (a 300 m del Centro Médico Toluca)', 1, '', 4, '0', 'Av. Benito Juarez', '528', 'A', '', '08', '', '054', 'MEX', 'MEX', 'MEX', '52140'),
(6, 'SAW', '06', 'Almacén WEB', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 1, '', 6, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'SAR', '07', 'Almacén Rentas', '(722) 217 26 26', 'JOSE MARIA PINO SUAREZ NO 722 COL CUAUHTÉMOC CP 50130 TOLUCA ESTADO DE MEXICO', 1, '', 7, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'SAG', '01', 'Almacén General', '(722) 217 26 26', 'FUERTE DE LORETO S/N  (ENTRE AVENIDA IGNACIO ZARAGOZA Y 1° DE MAYO) COL HEROES DE 5 MAYO, CP 50170, TOLUCA MEXICO, HORARIO: LUNES A VIERNES 9:30am a 13:30pm y de 14:00pm a 17:00pm', 1, '', 1, '0', 'Fuerte de Loreto', 'SN', '', '', '20', '', '106', 'MEX', 'MEX', 'MEX', '50170'),
(9, 'SPM', '08', 'Punto de Venta Móvil', '6456456456456', 'PUNTO DE VENTA MOVIL', 1, '', 9, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'DRDT', '09', 'La Bombonera', '45765756563', 'La Bombonera', 1, '', 9, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'B2', '10', 'LA BOMBONERA 2', '4324234523523', 'LA BOMBONERA 2', 1, '', 10, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
