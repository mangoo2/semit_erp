-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-10-2024 a las 18:54:46
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwsemi_calidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte_federal`
--

CREATE TABLE `transporte_federal` (
  `id` int(11) NOT NULL,
  `txtCodigo` int(11) NOT NULL,
  `txtNombre` varchar(300) NOT NULL,
  `nombre_aseguradora` varchar(255) DEFAULT NULL,
  `num_poliza_seguro` varchar(100) DEFAULT NULL,
  `num_permiso_sct` varchar(100) DEFAULT NULL,
  `configuracion_vhicular` varchar(255) DEFAULT NULL,
  `placa_vehiculo_motor` varchar(50) DEFAULT NULL,
  `anio_modelo_vihiculo_motor` varchar(50) DEFAULT NULL,
  `subtipo_remolque` varchar(100) DEFAULT NULL,
  `placa_remolque` varchar(50) DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  `tipo_permiso_sct` varchar(50) DEFAULT NULL,
  `AseguraMedAmbiente` varchar(300) DEFAULT NULL,
  `PolizaMedAmbiente` varchar(10) DEFAULT NULL,
  `AseguraCarga` varchar(30) DEFAULT NULL,
  `PolizaCarga` varchar(30) DEFAULT NULL,
  `PrimaSeguro` decimal(15,2) DEFAULT NULL,
  `subtipo2_remolque` varchar(100) DEFAULT NULL,
  `placa2_remolque` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transporte_federal`
--

INSERT INTO `transporte_federal` (`id`, `txtCodigo`, `txtNombre`, `nombre_aseguradora`, `num_poliza_seguro`, `num_permiso_sct`, `configuracion_vhicular`, `placa_vehiculo_motor`, `anio_modelo_vihiculo_motor`, `subtipo_remolque`, `placa_remolque`, `activo`, `reg`, `tipo_permiso_sct`, `AseguraMedAmbiente`, `PolizaMedAmbiente`, `AseguraCarga`, `PolizaCarga`, `PrimaSeguro`, `subtipo2_remolque`, `placa2_remolque`) VALUES
(1, 1, 'NISSAN NV350 URVAN PANEL', 'QUALITAS', '0780600392', 'ca-c-11898316', 'VL', 'LF04620', '2022', 'CTR001', '', 1, '2022-12-02 06:13:14', 'TPAF02', '', '', '', '', 0.00, '', ''),
(11, 2, 'TOYOTA HAICE PANEL', 'QUALITAS', '0780600705', 'AU-C-10703275', 'VL', 'NWH7365', '2013', '', '', 1, '0000-00-00 00:00:00', 'TPAF02', '', '', '', '', 0.00, '', ''),
(12, 3, 'NISSAN NP300 PICK UP', 'QUALITAS', '0780594551', 'AU-C-9747330', 'VL', 'NVJ6672', '2012', '', '', 1, '0000-00-00 00:00:00', 'TPAF02', '', '', '', '', 0.00, '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `transporte_federal`
--
ALTER TABLE `transporte_federal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `transporte_federal`
--
ALTER TABLE `transporte_federal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
