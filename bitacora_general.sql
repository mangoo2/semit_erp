-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-02-2024 a las 13:48:20
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwsemi_calidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora_general`
--

CREATE TABLE `bitacora_general` (
  `id` bigint(20) NOT NULL,
  `tipo` tinyint(1) NOT NULL COMMENT '0 delete 1 insert 2 update',
  `id_item` int(11) NOT NULL,
  `table_name` int(11) NOT NULL,
  `descripcion` text,
  `personal` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora_general`
--
ALTER TABLE `bitacora_general`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora_general`
--
ALTER TABLE `bitacora_general`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `bitacora_general` CHANGE `table_name` `table_name` VARCHAR(300) NOT NULL;