var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        "ajax": {
            "url": base_url+"Servicios/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"clave"},
            {"data":"descripcion"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a onclick="modal_detalles('+row.id+')" class="btn"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                        html+='<a href="'+base_url+'Servicios/registro/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        html+='<a onclick="eliminar_registro('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar a este servicio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Servicios/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function modal_detalles(id){
    $('#modaldetalles').modal('show');
    $('.text_detalles').html('Cargando datos...');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Servicios/viewdetalles_list',
        data: {id:id},
        success:function(data){
            $('.text_detalles').html(data);
        }
    });
}


function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}