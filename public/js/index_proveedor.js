var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
    $('#searchtext').focus();

    $(".exportExcel").on("click",function(){
        id = $("#idprove").val();
        window.open(base_url+'Proveedores/exportProdsExcel/'+id,'_blank');
    });

    $(".printpdf").on("click",function(){
        id = $("#idprove").val();
        window.open(base_url+'Proveedores/imprimir_pdf/'+id,'_blank');
    });

    $('.exportExcel').on('click', function() {
        $('body').loading({ theme: 'dark', message: 'Generando archivo, por favor espere...' });
        $.ajax({
            url:base_url+'Proveedores/exportExcelPove',
            type: 'POST',
            success: function(response) {
                window.location.href = base_url + 'Proveedores/exportExcelPove';
                $('body').loading('stop');
            },
            error: function() {
                swal("Error","Hubo un error al generar el archivo, intente nuevamente","error");
                $('body').loading('stop');
            }
        });
    });
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"Proveedores/getlistado",
            type: "post",
            "data":{estatus:$('#estatus').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            //{"data":"id"},
            {"data":"codigo"},
            {"data":"nombre"},
            {"data":"tel1"},
            {"data":"contacto"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a class="btn" onclick="modal_datosfiscales('+row.id+')"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a class="btn" onclick="getProductos('+row.id+')"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    msj= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.total_compras)+ "<br><a class='btn' onclick='getCompras("+row.id+")'><i class='fa fa-eye'></i></a>";
                    return msj;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';   
                if(row.estatus==1){
                    html='<button class="btn btn-primary btn-sm" type="button">Activo</button>';
                }else{
                    html='<button class="btn btn-danger btn-sm" type="button">Inactivo</button>';
                }    
                return html;
                }
            },
            
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Proveedores/registro/'+row.id+'" class="btn btn-cli-edit"></a>\
                        <a onclick="eliminar_registro('+row.id+')" class="btn btn-cli-delete"></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "ASC" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function modal_datosfiscales(id){
    $('#modal_datos_fiscales').modal('show');
    $('.text_datos_fiscales').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Proveedores/get_datos_fiscales",
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_datos_fiscales').html(data);
        }
    }); 
}

function getProductos(id){
    $('#modal_prods').modal('show');
    $("#idprove").val(id);
    var tab_prods = $('#table_prods').DataTable({
        destroy:true,
        "ajax": {
           "url": base_url+"Proveedores/getDataProds",
           type: "post",
           data:{ id:id },
            error: function(){
               $("#table_prods").css("display","none");
            }
        },
        "columns": [
            {"data": "idProducto"},
            {"data": "nombre"},
            {"data": "costo_compra",
                "render" : function (data,type,row) {
                    var msj='';
                    msj+= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.costo_compra);
                    return msj;
                }
            },
            {"data": "precio",
                "render" : function (data,type,row) {
                    var msj='';
                    msj+= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.precio);
                    return msj;
                }
            },
            {"data": "cod_provee"},
            {"data": null,
                "render" : function (data,type,row) {
                    stock=0;
                    if(row.tipo!=1 && row.tipo!=2){
                        stock=row.stock_ps;
                    }if(row.tipo==1){
                        stock=row.stock_series;
                    }if(row.tipo==2){
                        stock=row.stock_lotes;
                    }
                    return stock;
                }
            },
        ],
        "pageLength": 10
    });
}

function getCompras(id){
    window.open(base_url+'Proveedores/saldoCompras/'+id,'_blank');
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este usuario?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proveedores/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}
