var table;
var base_url=$('#base_url').val();
var perfil = $('#perfil').val();
var idgdg=0;
$(document).ready(function() {	
    tabla();

    $("#file").fileinput({
        showCaption: false,
        showUpload: false,
        //maxFileCount: 6,
        language:'es',
        allowedFileExtensions: ["pdf"],
        browseLabel: 'Seleccionar documento',
        uploadUrl: base_url+'Garantias/cargaCartaProvee',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'pdf': '<i class="fa fa-file-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                //input_name:"file",
                id_garantiad:idgdg
            };
            return info;
        }
    }).on('fileuploaded', function(event, files, extra) {
        getDocCarta();
        $("#file").fileinput("clear");
    }).on('filedeleted', function(event, files, extra) {
        getDocCarta();
    });
});

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Garantias/get_data_prodslist",
            type: "post",
        },
        "columns": [
            {"data": "id_garantia"},
            {"data": "idProducto"},
            {"data": "nombre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.tipo==1){
                        html="Serie: "+row.serie;
                    }if(row.tipo==2){
                        html="Lote: "+row.lote;
                    }
                    return html;
                }
            },
            {"data": "cant"},
            {"data": "suc_solicita"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.id_venta==0){
                        html='Prod. Interno';
                    }if(row.id_venta>0){
                        html='Prod. de Venta';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==2 && row.retorno==0){
                        html="<span class='btn btn-primary'>En revisión</span>";
                    }if(row.estatus==2 && row.retorno==1){ //OJO VALIDAR ESTE ESTATUS, ES INCORRECTO -- SE CORRIGIÓ DESDE BD
                        html="<span onclick='viewDet("+row.id+",1)' class='btn btn-success'>Retornado</span>";
                    }if(row.estatus==2 && row.retorno==0 && row.tipo_mov==2){
                        html="<span onclick='viewDet("+row.id+",2)' class='btn btn-success'>Reemplazado</span>";
                    }if(row.estatus==2 && row.retorno==0 && row.tipo_mov==3){
                        html="<span onclick='viewDet("+row.id+",3)' class='btn btn-success'>Reparado y entregado</span>";
                    }if(row.estatus==2 && row.retorno==0 && row.tipo_mov==4){
                        html="<span onclick='viewDet("+row.id+",4)' class='btn btn-warning'>Equipo devuelto a proveedor</span>";
                    }if(row.estatus==2 && row.retorno==0 && row.tipo_mov==5){
                        html="<span onclick='viewDet("+row.id+",5)' class='btn btn-danger'>No aplica garantía</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html=''; var onclick1=""; var onclick2=""; var onclick3="";
                    if(row.id_venta==0){
                        if(row.tipo_mov==0){
                            onclick1='onclick="retornar('+row.id+')"';
                            onclick2='onclick="changeSerie('+row.id+','+row.id_ps_ser+')"';
                        }
                        html+='<a title="Retornar a Stock" style="cursor: pointer" '+onclick1+'><img style="width: 35px;" src="'+base_url+'public/img/ventasp/19.png"></a>';
                        html+='<a title="Cambiar serie y retornar a stock" style="cursor: pointer" '+onclick2+'><img style="width: 35px;" src="'+base_url+'public/img/duplicate.svg"></a>';
                        if(row.estatus==2 && row.retorno==1){
                            html+='<a title="PDF Orden" style="cursor:pointer" onclick="get_pdf_orden('+row.id_garantia+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                        }
                    }else{
                        if(row.tipo_mov==0){
                            onclick1='onclick="repair('+row.id+',3)"';
                            onclick2='onclick="devolver('+row.id+')"';
                            onclick3='onclick="repair('+row.id+',5)"';
                        }
                        html+='<a title="No aplica garantía" style="cursor: pointer" '+onclick3+'><img style="width: 35px;" src="'+base_url+'public/img/cancel.svg"></a>';
                        html+='<a title="Reparado y devuelto" style="cursor: pointer" '+onclick1+'><img style="width: 35px;" src="'+base_url+'public/img/pv/garantias.png"></a>';
                        if($("#perfil").val()=="1" || $("#perfil").val()=="8"){ //admin y contabilidad
                            html+='<a title="Devolución y generar nota" style="cursor: pointer" '+onclick2+'><img style="width: 35px;" src="'+base_url+'public/img/duplicate.svg"></a>';
                        }
                        if(row.estatus==2 && row.retorno==0 && row.tipo_mov==5){
                            html+='<a title="Carta de proveedor" style="cursor:pointer" onclick="get_carta('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                        }
                        
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function get_pdf_orden(id_garantia){
    window.open(base_url+'Garantias/pdf_garantia_orden/'+id_garantia,'_blank'); 
}

function get_carta(id){
    $("#modalfiles").modal("show");
    $("#id_gd").val(id);
    idgdg=id;
    getDocCarta(id);
}
function getDocCarta(){
    $("#cont_imgs").html("");
    $.ajax({
        type:'POST',
        url: base_url+'Garantias/getCartaProvee',
        data: { id: $("#id_gd").val() },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}

function viewDet(id,tipo){
    $("#modaldetalles").modal("show");
    $.ajax({
        type:'POST',
        url: base_url+"Garantias/detalleBitacora",
        data:{id:id},
        success:function(result){
            $("#det_bita").html(result);
        }
    });
}

function retornar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de retornar a stock el equipo?'+
                '<textarea class="form-control" id="comentarios" placeholder="Comentarios"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Garantias/retorna_equipo",
                    data:{id:id,comentarios:$("#comentarios").val()},
                    success:function(data){
                        swal("Éxito!", "Realizado correctamente", "success");
                        //MANDAR EL MAIL DE TRASLADO, EL RETORNO DEL PRODUCTO MEDIANTE TRASLADO
                        //AGREGAR Y PROBAR FUNCION
                        //FALTA PROBAR RETORNO PARA VALIDAR SOLICITUD - INGRESO DE TRASLADO
                        setTimeout(function(){ 
                            tabla(); 
                        }, 1500); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function changeSerie(id,id_ps_ser){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cambiar serie?'+
                '<div class="col-lg-12"><label>Serie:</label><input class="form-control" id="new_serie" placeholder="Nueva serie del producto"></div>'+
                '<div class="col-lg-12"><label>Comentarios:</label><textarea class="form-control" id="coment_change" placeholder="Comentarios"></textarea></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if($("#new_serie").val().trim()!=""){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Garantias/changeSerie",
                        data:{id:id, serie:$("#new_serie").val(),comentarios:$("#coment_change").val(),id_ps_ser:id_ps_ser},
                        success:function(data){
                            swal("Éxito!", "Rechazado correctamente", "success");
                            setTimeout(function(){ 
                                tabla(); 
                            }, 1500); 
                        }
                    });
                }else{
                    swal("Álerta!", "Indique la serie nueva", "warning");
                }

                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function repair(id,estatus){
    if(estatus==3)
        txt="¿Está seguro de indicar como reparado el equipo?";
    else
        txt="¿Está seguro de indicar que no aplica garantía?";

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: txt+
                '<textarea class="form-control" id="coment_repair" placeholder="Comentarios"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Garantias/repair_equipo",
                    data:{id:id,comentarios:$("#coment_repair").val(),estatus:estatus},
                    success:function(data){
                        //swal("Éxito!", "Realizado correctamente", "success");
                        swal({
                            title: "Éxito!",
                            text: "Realizado correctamente",
                            type: "success",
                            timer: 2500
                        });
                        setTimeout(function(){ 
                            tabla(); 
                        }, 1500);
                        setTimeout(function(){ 
                            if(estatus!=3){
                                get_carta(id);
                            }
                        }, 300);
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function devolver(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de devolver equipo? Se quedará en garantía y se iniciara su proceso de nota de crédito'+
                '<div class="col-lg-12" style="display:none"><label>Nota de crédito:</label><input class="form-control" id="nota_credito" placeholder="Nota de crédito en caso de existir"></div>'+
                '<div class="col-lg-12"><label>Comentarios:</label><textarea class="form-control" id="coment_devo" placeholder="Comentarios"></textarea></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Garantias/devolver_equipo",
                    data:{id:id,comentarios:$("#coment_devo").val(),nota_credito:$("#nota_credito").val()},
                    success:function(data){
                        swal("Éxito!", "Realizado correctamente", "success");
                        setTimeout(function(){ 
                            tabla(); 
                        }, 1500); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_compra(id){
    window.open(base_url+'Garantias/pdf_orden/'+id,'_blank'); 
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
