var base_url = $('#base_url').val();
var perfil = $('#perfil').val(); 
var tabla;
var tabla_entrenamiento;
var aux_idempleado=0;
$(document).ready(function() {
    $('#foto_avatar').change(function(){
         document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
    });
	table();
    $('textarea.js-auto-size').textareaAutoSize();
});
function modal_empleado(){
    $('#personalId').val('0');
    $('#nombre').val('');
    $('#correo').val('');
    $('#celular').val('');
    $('#puesto').val('');
    $('#verificar_check').prop('checked',false);
    $('.baja_texto').css('display','none');
    $('#fechabaja').val('');
    $('#motivo').val('');
    $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'images/annon.png">');   
	$('#registro_empleado').modal();
}
function guarda_empleado(){
    var form_register = $('#form_empleado');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_empleado").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/registra_empleado',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                add_file(id);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Empleados';
                }, 1500);
            }
        });
    }   
}
function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Empleados/getlistado",
            type: "post",
            "data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"personalId"},
            {"data":"nombre"},
            {"data":"puesto"},
            {"data":"celular"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=''; 
                    if(row.check_baja=='on'){
                       html+='<span class="label label-danger m-r-10">Baja</span>';    
                    }else{
                       html+='';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var color_usuario='';
                    if(row.usuario==1){
                        color_usuario='btn-warning';
                    }else{
                        color_usuario='btn-secondary';
                    }
                var html='';
                    if(perfil!=2){
                    html+='<button type="button"\
                            data-perid="'+row.personalId+'"\
                            data-nombre="'+row.nombre+'"\
                            data-correo="'+row.correo+'"\
                            data-celular="'+row.celular+'"\
                            data-puesto="'+row.puesto+'"\
                            data-fechabaja="'+row.fechabaja+'"\
                            data-motivo="'+row.motivo+'"<\
                            data-check_baja="'+row.check_baja+'"\
                            data-foto="'+row.foto+'"\
                            data-color="'+row.color+'"\
                            class="btn btn_sistema btn-circle paciente_datos_'+row.personalId+'" onclick="modal_editar('+row.personalId+')"><i class="far fa-edit"></i></button>';

                    html+='<button type="button" class="btn '+color_usuario+' btn-circle" onclick="modal_usuario('+row.personalId+');"><i class="fas fa-user"></i> </button>';
                    }
                    html+='<button type="button" class="btn btn-primary btn-circle" onclick="imprimir_gafete('+row.personalId+');"><i class="fas fa-id-card-alt"></i> </button>';
                    if(perfil!=2){
                    html+='<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_empleado('+row.personalId+');"><i class="fas fa-trash-alt"></i> </button>';
                    }
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}
function modal_editar(id){
	$('#personalId').val($('.paciente_datos_'+id).data('perid'));
	$('#nombre').val($('.paciente_datos_'+id).data('nombre'));
    $('#correo').val($('.paciente_datos_'+id).data('correo'));
    $('#puesto').val($('.paciente_datos_'+id).data('puesto'));
	$('#celular').val($('.paciente_datos_'+id).data('celular'));
    var check_baja = $('.paciente_datos_'+id).data('check_baja');
    if(check_baja=='on'){
       $('#verificar_check').prop('checked',true);
       $('.baja_texto').css('display','block');
    }else{
       $('#verificar_check').prop('checked',false);
       $('.baja_texto').css('display','none');
    }

    var foto = $('.paciente_datos_'+id).data('foto');
    if(foto!=''){
        $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'uploads/personal/'+foto+'">');
    }else{    
        $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'images/annon.png">');   
    }
    $('#fechabaja').val($('.paciente_datos_'+id).data('fechabaja'));
    $('#motivo').val($('.paciente_datos_'+id).data('motivo'));

    var color =$('.paciente_datos_'+id).data('color');
    if(color=='red'){
        $('#color1').prop('checked',true);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',false);
    }else if(color=='orange'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',true);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',false);
    }else if(color=='yellow'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',true);
        $('#color4').prop('checked',false);
    }else if(color=='#03a9f4'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',true);
    } 
	$('#registro_empleado').modal();
}
function check_baja_btn(){
    if($('#verificar_check').is(':checked')){
        $('.baja_texto').css('display','block');
    }else{
        $('.baja_texto').css('display','none');
    }
}
function eliminar_empleado(id){ 
    $('#id_empleado').val(id);
    $('#elimina_empleado_modal').modal();
}
function delete_empleado(){
    var idp=$('#id_empleado').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/delete_empleado",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_empleado_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}
function modal_usuario(id){
    $('#usuario_modal').modal();
    $.ajax({
        type:'POST',
        url: base_url+'Empleados/registro_usuario',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_usuario').html(data);
        }
    });
}
function guardar_usuario(){
    var form_register = $('#form_usuario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            usuario:{
              required: true
            },
            contrasena:{
              required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            personalId: {
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_usuario").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#usuario_modal').modal('hide');
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);
                reload_registro();
            }
        });
    }   
}
function verificar_usuario(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'Empleados/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data) {
            if (data == 1) {
                $('.txt_usuario').html('El usuario ya existe');
                $.toast({
                    heading: '¡Atención!',
                    text: 'El usuario ya existe',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            }else{
                $('.txt_usuario').html('');
            }
        }
    });
}
function imprimir_gafete(id){
    $('.iframe_datos').html('');
    var html='<iframe src="'+base_url+'Empleados/gafete/'+id+'"></iframe>';
    setTimeout(function(){     
        $('.iframe_datos').html(html);
    }, 1500);   
}
function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Empleados/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
//===========================================================