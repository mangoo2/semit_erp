var base_url = $('#base_url').val();
var tablaser;
$(document).ready(function() {
    tablaser = $('#table_datos').DataTable();
    load();
});
function load(){
    tablaser.destroy();
	tablaser=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Cservicios/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"numero"},
            {"data":"descripcion"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipo==1){
                        html='Mantenimiento';
                    }
                    if(row.tipo==2){
                        html='Correctivo';
                    }
                    if(row.tipo==3){
                        html='Logistica';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                    var precio=row.precio_siva;
                    if(row.iva==1){
                        var ivap = precio*0.16;
                            ivap=ivap.toFixed(5);
                        html+='<!--'+precio+' + '+ivap+' -->';
                        precio=parseFloat(precio)+parseFloat(ivap); 
                        precio=precio.toFixed(2)
                    }
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(precio);
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                        html+='<a href="'+base_url+'Cservicios/registro/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        html+='<a onclick="eliminar_registro('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                    html+='</div>';
                return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar a este servicio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Cservicios/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            load(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function search(){
    var searchtext = $('#searchtext').val();
    tablaser.search(searchtext).draw();
}