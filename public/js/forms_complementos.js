var base_url =$('#base_url').val();
var table_op;
var table_dir_o;
var table_dir_d;
var table_list_ro;
var table_list_rd;
var table_transporte;
$(document).ready(function($) {
	table_op=$('#table_operador').DataTable();

	table_dir_o=$('#table_dir_o').DataTable();
	table_dir_d=$('#table_dir_d').DataTable();

	table_list_ro=$('#table_list_ro').DataTable();
	table_list_rd=$('#table_list_rd').DataTable();

	table_transporte=$('#table_transporte').DataTable();

	loadtable_op();
	/*
	loadtable_dir_o();
	loadtable_dir_d();

	loadtable_list_ro();
	loadtable_list_rd();
	*/

	$('#btn_save_op').click(function(event) {
		$( "#btn_save_op" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#btn_save_op" ).prop( "disabled", false );
        }, 5000);
		btn_save_op();
	});
	$('#btn_save_dir').click(function(event) {
		$( "#btn_save_dir" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#btn_save_dir" ).prop( "disabled", false );
        }, 5000);
		btn_save_dir();
	});

	$('#btn_save_rut').click(function(event) {
		$( "#btn_save_rut" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#btn_save_rut" ).prop( "disabled", false );
        }, 5000);
		btn_save_rut();
	});


	//get_estaciones();
});
function loadtable_op(){
	table_op.destroy();
	table_op=$("#table_operador").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_op",
		        type: "post",
		        error: function(){
		           //$("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"rfc_del_operador"},
				{"data":"no_licencia"},
				{"data":"operador"},
				{"data":"num_identificacion"},
				{"data":"residencia_fiscal"},
				{"data":"calle"},
				{"data":"descripcion"},
				{"data":"pais"},
				{"data":"codigo_postal"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a  onClick="add_operador('+row.id+')" type="button" class="btn btn-success mr-1 mb-1 edit_op'+row.id+'" ';
		                    html+=' data-rfc="'+row.rfc_del_operador+'" ';
		                    html+=' data-lic="'+row.no_licencia+'" ';
		                    html+=' data-op="'+row.operador+'" ';
		                    html+=' data-niden="'+row.num_identificacion+'" ';
		                    html+=' data-rf="'+row.residencia_fiscal+'" ';
		                    html+=' data-calle="'+row.calle+'" ';
		                    html+=' data-estado="'+row.estado+'" ';
		                    html+=' data-cp="'+row.codigo_postal+'" ';
		                    html+='><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "desc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function eliminar_reg(id_reg){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cat_complemento/delete_op",
                        data: {
                            id:id_reg
                        },
                        success: function (response){  
							toastr.success('Registro eliminado');     
							loadtable_op();            
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error");  
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function add_operador(id){
	if(id>0){
		$('#modal_add_op .title').html('Editar Operador');
		$('#btn_save_op').html('Editar');
		var rfc =$('.edit_op'+id).data('rfc');
		var lic =$('.edit_op'+id).data('lic');
		var op =$('.edit_op'+id).data('op');
		var niden =$('.edit_op'+id).data('niden');
		var rf =$('.edit_op'+id).data('rf');
		var calle =$('.edit_op'+id).data('calle');
		var estado =$('.edit_op'+id).data('estado');
		var cp =$('.edit_op'+id).data('cp');
		$('#id_op').val(id);
		$('#rfc_del_operador').val(rfc);
		$('#no_licencia').val(lic);
		$('#operador').val(op);
		$('#num_identificacion').val(niden);
		$('#residencia_fiscal').val(rf);
		$('#calle').val(calle);
		$('#estado').val(estado);
		$('#pais').val('MEXICO');
		$('#codigo_postal').val(cp);
	}else{
		$('#modal_add_op .title').html('Agregar Operador');
		$('#btn_save_op').html('Agregar');
	}

	$('#modal_add_op').modal('show');
}
function btn_save_op(){
	var form = $('#form_ope');
	if(form.valid()){
		var datos = form.serialize();
		$.ajax({
        type:'POST',
        url: base_url+"Cat_complemento/save_op",
        data: datos,
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('404');
            },
            500: function(data){
            	console.log(data);
                toastr.error('500');
            }
        },
        success: function (data){
        	toastr.success('Operador Agregado');
        	loadtable_op();
        	$('#modal_add_op').modal('hide');
        	form[0].reset();
        }
    });
	}else{
		toastr.error('Favor de completar los datos requeridos');
	}
}
function btn_save_dir(){
	var dir_tipo = $('#dir_tipo').val();
	var form = $('#form_direccion');
	if(form.valid()){
		var datos = form.serialize();
		$.ajax({
        type:'POST',
        url: base_url+"Cat_complemento/save_dir",
        data: datos,
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('404');
            },
            500: function(data){
            	console.log(data);
                toastr.error('500');
            }
        },
        success: function (data){
        	toastr.success('Direccion Agregada');
        	if(dir_tipo==0){
        		loadtable_dir_o();
        	}else{
        		loadtable_dir_d();
        	}
        	$('#modal_add_direcciones').modal('hide');
        	form[0].reset();
        }
    });
	}else{
		toastr.error('Favor de completar los datos requeridos');
	}
}
function btn_save_rut(){
	var rut_tipo = $('#rut_tipo').val();
	var form = $('#form_ruta');
	if(form.valid()){
		var datos = form.serialize();
		$.ajax({
        type:'POST',
        url: base_url+"Cat_complemento/save_rut",
        data: datos,
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('404');
            },
            500: function(data){
            	console.log(data);
                toastr.error('500');
            }
        },
        success: function (data){
        	toastr.success('Ruta Agregada');
        	if(rut_tipo==0){
        		loadtable_list_ro();
        	}else{
        		loadtable_list_rd();
        	}
        	$('#modal_add_ruta').modal('hide');
        	form[0].reset();
        }
    });
	}else{
		toastr.error('Favor de completar los datos requeridos');
	}
}
function loadtable_dir_o(){
	table_dir_o.destroy();
	table_dir_o=$("#table_dir_o").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_dir_o",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"calle"},
				{"data":"num_ext"},
				{"data":"num_int"},
				{"data":"colonia"},
				{"data":"localidad"},
				{"data":"referencia"},
				{"data":"municipio"},
				{"data":"estado"},
				{"data":"pais"},
				{"data":"codigo_postal"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a onclick="add_direc('+row.id+',0)" type="button" class="btn btn-success mr-1 mb-1 add_direc_0_'+row.id+'" ';
		                    html+=' data-calle="'+row.calle+'" ';
		                    html+=' data-ne="'+row.num_ext+'" ';
		                    html+=' data-ni="'+row.num_int+'" ';
		                    html+=' data-col="'+row.colonia+'" ';
		                    html+=' data-colc="'+row.col_clave+'" ';
		                    html+=' data-loc="'+row.localidad+'" ';
		                    html+=' data-locc="'+row.loc_clave+'" ';
		                    html+=' data-ref="'+row.referencia+'" ';
		                    html+=' data-mun="'+row.municipio+'" ';
		                    html+=' data-munc="'+row.mun_clave+'" ';
		                    html+=' data-est="'+row.estado+'" ';
		                    html+=' data-estc="'+row.est_clave+'" ';
		                    html+=' data-cp="'+row.codigo_postal+'" ';

		                    html+='><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_dir('+row.id+',0)" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function loadtable_dir_d(){
	table_dir_d.destroy();
	table_dir_d=$("#table_dir_d").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_dir_d",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"calle"},
				{"data":"num_ext"},
				{"data":"num_int"},
				{"data":"colonia"},
				{"data":"localidad"},
				{"data":"referencia"},
				{"data":"municipio"},
				{"data":"estado"},
				{"data":"pais"},
				{"data":"codigo_postal"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a onclick="add_direc('+row.id+',1)" type="button" class="btn btn-success mr-1 mb-1 add_direc_1_'+row.id+'" ';
		                    html+=' data-calle="'+row.calle+'" ';
		                    html+=' data-ne="'+row.num_ext+'" ';
		                    html+=' data-ni="'+row.num_int+'" ';
		                    html+=' data-col="'+row.colonia+'" ';
		                    html+=' data-colc="'+row.col_clave+'" ';
		                    html+=' data-loc="'+row.localidad+'" ';
		                    html+=' data-locc="'+row.loc_clave+'" ';
		                    html+=' data-ref="'+row.referencia+'" ';
		                    html+=' data-mun="'+row.municipio+'" ';
		                    html+=' data-munc="'+row.mun_clave+'" ';
		                    html+=' data-est="'+row.estado+'" ';
		                    html+=' data-estc="'+row.est_clave+'" ';
		                    html+=' data-cp="'+row.codigo_postal+'" ';
		                    html+='><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_dir('+row.id+',1)" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function add_direc(id,tipo){
	$('#dir_id').val(id);
	$('#dir_tipo').val(tipo);
	if(id>0){
		var ti0='Editar';
		//=================================
			var calle = $('.add_direc_'+tipo+'_'+id).data('calle');
			var ne = $('.add_direc_'+tipo+'_'+id).data('ne');
			var ni = $('.add_direc_'+tipo+'_'+id).data('ni');
			var col = $('.add_direc_'+tipo+'_'+id).data('col');
			var colc = $('.add_direc_'+tipo+'_'+id).data('colc');
			var loc = $('.add_direc_'+tipo+'_'+id).data('loc');
			var locc = $('.add_direc_'+tipo+'_'+id).data('locc');
			var ref = $('.add_direc_'+tipo+'_'+id).data('ref');
			var mun = $('.add_direc_'+tipo+'_'+id).data('mun');
			var munc = $('.add_direc_'+tipo+'_'+id).data('munc');
			var est = $('.add_direc_'+tipo+'_'+id).data('est');
			var estc = $('.add_direc_'+tipo+'_'+id).data('estc');
			var cp = $('.add_direc_'+tipo+'_'+id).data('cp');

			$('#dir_calle').val(calle);
			$('#dir_num_ext').val(ne);
			$('#dir_num_int').val(ni);
			$('#dir_codigo_postal').val(cp);
			
			$('#dir_referencia').val(ref);
			if(colc!=''){
				$('#dir_colonia').html('<option value="'+colc+'">'+col+'</option>');
			}
			if(locc!=''){
				$('#dir_localidad').html('<option value="'+locc+'">'+loc+'</option>');
			}
			if(munc!=''){
				$('#dir_municipio').html('<option value="'+munc+'">'+mun+'</option>');
			}
			$('#dir_estado').val(estc).change();
		//=================================
	}else{
		var ti0='Agregar';
	}
	if(tipo==0){
		var ti1='Origen';
	}else{
		var ti1='Destino';
	}
	$('#btn_save_dir').html(ti0);
	var title=ti0+' Direccion '+ti1;
	$('#modal_add_direcciones .title').html(title);

	$('#modal_add_direcciones').modal('show');
}
function selectestado_dir(){
	var esta=$('#dir_estado option:selected').val();
	get_localidad(esta);
    get_municipio(esta);
    get_colonia();
}
function get_localidad(estado){
	$('#dir_localidad').select2({
		dropdownParent: $('#modal_add_direcciones'),
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Cat_complemento/search_localidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    estado:estado,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion+'('+element.c_Estado+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function get_municipio(estado){
	$('#dir_municipio').select2({
		dropdownParent: $('#modal_add_direcciones'),
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Cat_complemento/search_municipio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    estado:estado,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion+'('+element.c_Estado+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function get_colonia(){
	var dir_cp = $('#dir_codigo_postal').val();
	$('#dir_colonia').select2({
		dropdownParent: $('#modal_add_direcciones'),
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Cat_complemento/search_colonia',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    cp:dir_cp,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: '('+element.c_CodigoPostal+')'+element.nombre,
                        cp:element.c_CodigoPostal
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
            var data = e.params.data;
            $('#dir_codigo_postal').val(data.cp);
        });
}
function loadtable_list_ro(){
	table_list_ro.destroy();
	table_list_ro=$("#table_list_ro").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_ro",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"idorigen"},
				{"data":"num_estacion",
					"render": function ( data, type, row, meta ) {
						var html='';
							html=row.num_estacion+' ('+row.clave_identificacion+')';
						return html 
					}
				},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a onclick="add_ruta('+row.id+',0)" type="button" class="btn btn-success mr-1 mb-1 add_ruta_0_'+row.id+'" ';
		                    html+=' data-id="'+row.idorigen+'" ';
		                    html+=' data-estclave="'+row.clave_identificacion+'" ';
		                    html+=' data-estname="'+row.num_estacion+'" ';
		                    html+=' ><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_rut('+row.id+',0)" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function loadtable_list_rd(){
	table_list_rd.destroy();
	table_list_rd=$("#table_list_rd").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_rd",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"idorigen"},
				{"data":"num_estacion",
					"render": function ( data, type, row, meta ) {
						var html='';
							html=row.num_estacion+' ('+row.clave_identificacion+')';
						return html 
					}
				},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a onclick="add_ruta('+row.id+',1)" type="button" class="btn btn-success mr-1 mb-1 add_ruta_1_'+row.id+'" ';
		                    html+=' data-id="'+row.idorigen+'" ';
		                    html+=' data-estclave="'+row.clave_identificacion+'" ';
		                    html+=' data-estname="'+row.num_estacion+'" ';
		                    html+='><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_rut('+row.id+',1)" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 1, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function add_ruta(id,tipo){
	$('#id_rut').val(id);
	$('#rut_tipo').val(tipo);
	if(tipo==0){
		var ti1='Origen';
		$( "#idorigen" ).data( "inputmask", "'mask': 'OR999999'");
		var inputmask_v="'mask': 'OR999999'";

	}else{
		var ti1='Destino';
		$( "#idorigen" ).data( "inputmask", "'mask': 'DE999999'");
		var inputmask_v="'mask': 'DE999999'";
	}
	$('.addidorigenes').html('<label>ID '+ti1+'</label><input type="text" name="idorigen" id="idorigen" class="form-control mascarainput" placeholder="OR000123 / DE000123" data-inputmask="'+inputmask_v+'" inputmode="text" required>');

	$(".mascarainput").inputmask();

	if(id>0){
		var ti0='Editar';
		var idorigen =$('.add_ruta_'+tipo+'_'+id).data('id');
		var estclave =$('.add_ruta_'+tipo+'_'+id).data('estclave');
		var estname =$('.add_ruta_'+tipo+'_'+id).data('estname');

		$('#idorigen').val(idorigen);
		$('#num_estacion').html('<option value="'+estclave+'">'+estname+'</option>');
	}else{
		var ti0='Agregar';
		$('#num_estacion').html('');
	}
	get_estaciones();

	$('#btn_save_rut').html(ti0);
	var title=ti0+' '+ti1;
	$('#modal_add_ruta .title').html(title);

	$('#modal_add_ruta').modal('show');
}
function get_estaciones(){
    $('#num_estacion').select2({
    	dropdownParent: $('#modal_add_ruta'),
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        ajax: {
            url: base_url+'Cat_complemento/search_estaciones',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave_identificacion,
                        text: element.descripcion+' ('+element.clave_identificacion+')',
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function eliminar_dir(id,tipo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cat_complemento/delete_dir",
                        data: {
                            id:id,tipo:tipo
                        },
                        success: function (response){  
							toastr.success('Registro eliminado');     
							if(tipo==0){
								loadtable_dir_o();
							}else{
								loadtable_dir_d();
							}         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error");  
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function eliminar_rut(id,tipo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cat_complemento/delete_rut",
                        data: {
                            id:id,tipo:tipo
                        },
                        success: function (response){  
							toastr.success('Registro eliminado');     
							if(tipo==0){
								loadtable_dir_o();
							}else{
								loadtable_dir_d();
							}         
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error");  
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function loadtable_transporte(){
	table_transporte.destroy();
	table_transporte=$("#table_transporte").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_trans",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data":"nombre_aseguradora"},
				{"data":"num_poliza_seguro"},
				{"data":"num_permiso_sct"},
				{"data":"configuracion_vhicular"},
				{"data":"placa_vehiculo_motor"},
				{"data":"anio_modelo_vihiculo_motor"},
				{"data":"subtipo_remolque"},
				{"data":"placa_remolque"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a href="'+base_url+'Transporte_federal/registro/'+row.id+'" type="button" class="btn btn-success mr-1 mb-1"><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}