var base_url = $('#base_url').val();
$(document).ready(function(){
	$("#cerdigital").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["cer"],
            browseLabel: 'Seleccionar Certificado',
            uploadUrl: base_url+'Conf_facturacion/cargacertificado',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        ConfiguracionesId:$('#ConfiguracionesId').val()
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });
        $("#claveprivada").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["key"],
            browseLabel: 'Seleccionar clave privada',
            uploadUrl: base_url+'Conf_facturacion/cargakey',
            preferIconicZoomPreview: false,
            previewFileIcon: '<i class="fas fa-passport"></i>',
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'key': '<i class="fas fa-passport"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        pass:$('#passprivada').val(),
                        ConfiguracionesId:$('#ConfiguracionesId').val()
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo la claveprivada Correctamente','Hecho!');
        });
        graficas_mes();
        graficas_ventas_mes();
});
function instalararchivos(){
	$('#cerdigital').fileinput('upload');
	$('#claveprivada').fileinput('upload');
	setTimeout(function(){
        //var configuracionesId = $('#ConfiguracionesId').val();
        //if(configuracionesId==1){
        //    instalararchivosprocesar();    
        //}

		instalararchivosprocesar();
	}, 3000);
}
function instalararchivosprocesar(){
	$.ajax({
        type:'POST',
        url: base_url+"hulesyg/elementos/procesarfiles.php",
        success: function (data){
        	
        }
    });
}
/*
function instalararchivosprocesar2(){
    $.ajax({
        type:'POST',
        url: base_url+"kyocera/elementos2/procesarfiles.php",
        success: function (data){
            
        }
    });
}
*/
function actualizardatos(){
    
    var form = $('#formdatosgenerales');
    var datos = form.serialize();
    if (form.valid()) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Conf_facturacion/datosgenerales",
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('404','Error!');
                },
                500: function(){ 
                    toastr.error('500','Error!');
                }
            },
            success:function(data){  
                toastr.success('Se actualizo Correctamente','Hecho!');
            }
        });
    }
}
function view_tipov(){
    var tipov=$('#tipov option:selected').val();
    window.location.href = base_url+"index.php/Conf_facturacion?tipov="+tipov;
}


//=====================graficas====================
function get_mes_data(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Conf_facturacion/get_data_factura_mes',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
                pro.push(array.mes1,array.mes2,array.mes3,array.mes4,array.mes5,array.mes6,array.mes7,array.mes8,array.mes9,array.mes10,array.mes11,array.mes12);
        }
    });
    return pro;
}
function graficas_mes(){
    var data_mes=get_mes_data();
    var barData = {
        labels: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"],
        datasets: [{
            label: "Facturas realizadas",
            fillColor: "rgba(23 112 170 / 50%)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(23 112 170)",
            highlightStroke: vihoAdminConfig.primary,
            data: data_mes
            //data: [35, 59, 80, 81, 56, 55, 40,80, 81, 56, 55, 40]
        }]
    };
    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,0.1)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true,
        showTooltips: false,
        onAnimationComplete: function() {

            var ctx = this.chart.ctx;
            ctx.font = this.scale.font;
            ctx.fillStyle = this.scale.textColor
            ctx.textAlign = "center";
            ctx.textBaseline = "bottom";

            this.datasets.forEach(function(dataset) {
              dataset.bars.forEach(function(bar) {
                ctx.fillText(bar.value, bar.x, bar.y - 1);
              });
            })
        },
    };
    var barCtx = document.getElementById("chart_mes").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);
    var polarData = [
        {
            value: 300,
            color: vihoAdminConfig.primary,
            highlight: "rgba(36, 105, 92, 1)",
            label: "Yellow"
        }, {
            value: 50,
            color: vihoAdminConfig.secondary,
            highlight: "rgba(186, 137, 93, 1)",
            label: "Sky"
        }, {
            value: 100,
            color: "#222222",
            highlight: "rgba(34,34,34,1)",
            label: "Black"
        }, {
            value: 40,
            color: "#717171",
            highlight: "rgba(113, 113, 113, 1)",
            label: "Grey"
        }, {
            value: 120,
            color: "#ff8d42",
            highlight: "#616774",
            label: "Dark Grey"
        }
    ];
}


function get_mes_data_ventas(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Conf_facturacion/get_data_factura_ventas_mes',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
                pro.push(array.mes1,array.mes2,array.mes3,array.mes4,array.mes5,array.mes6,array.mes7,array.mes8,array.mes9,array.mes10,array.mes11,array.mes12);
        }
    });
    return pro;
}


function graficas_ventas_mes(){
    var data_mes=get_mes_data_ventas();
    var barData = {
        labels: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"],
        datasets: [{
            label: "Facturas realizadas",
            fillColor: "rgba(23 112 170 / 50%)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(23 112 170)",
            highlightStroke: vihoAdminConfig.primary,
            data: data_mes
            //data: [35, 59, 80, 81, 56, 55, 40,80, 81, 56, 55, 40]
        }]
    };
    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,0.1)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true,
        showTooltips: false,
        onAnimationComplete: function() {

            var ctx = this.chart.ctx;
            ctx.font = this.scale.font;
            ctx.fillStyle = this.scale.textColor
            ctx.textAlign = "center";
            ctx.textBaseline = "bottom";

            this.datasets.forEach(function(dataset) {
              dataset.bars.forEach(function(bar) {
                ctx.fillText(bar.value, bar.x, bar.y - 5);
              });
            })
        }
    };
    var barCtx = document.getElementById("chart_anio").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);
    var polarData = [
        {
            value: 300,
            color: vihoAdminConfig.primary,
            highlight: "rgba(36, 105, 92, 1)",
            label: "Yellow"
        }, {
            value: 50,
            color: vihoAdminConfig.secondary,
            highlight: "rgba(186, 137, 93, 1)",
            label: "Sky"
        }, {
            value: 100,
            color: "#222222",
            highlight: "rgba(34,34,34,1)",
            label: "Black"
        }, {
            value: 40,
            color: "#717171",
            highlight: "rgba(113, 113, 113, 1)",
            label: "Grey"
        }, {
            value: 120,
            color: "#ff8d42",
            highlight: "#616774",
            label: "Dark Grey"
        }
    ];
}