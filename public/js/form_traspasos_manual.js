var base_url = $('#base_url').val();
var prod=0; var cant=0;
$(document).ready(function() {
    search_producto_all();

    $("#recargas").on("change",function(){
        tipoTraslado();
    });

    $("#id_tanque").select2({
        width: '100%',
        //minimumInputLength: 2,
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if($("#cant_litros").val()!="" && $("#cant_litros").val()>0){
            get_recarga();
        }
    });
    $("#cant_litros").on("change",function(){
        var cant_lit =Number($("#cant_litros").val());
        if(cant_lit>0){
            get_recarga();
        }
    });
});

function get_recarga() {
    var stockn=Number($("#id_tanque option:selected").data("stock-alm"));
    var cant_lit =Number($("#cant_litros").val());
    if(cant_lit==0){
        swal("¡Atención!", "Ingresa una cantidad a solicitar", "error");
        return;
    }
    if(( cant_lit % 9500 ) == 0){
        if(stockn>=cant_lit){
            table_rechange();
        }else{
            swal("¡Atención!", "No se cuenta con los litros suficientes", "error");  
        }
    }else{
        swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");  
    }
}

function table_rechange(){
    //console.log("table rechange");
    var id =$("#id_tanque option:selected").val();
    var txt_recha =$("#id_tanque option:selected").text();
    var cant_lit =$("#cant_litros").val();
    if(id!=undefined && $("#table_datos_recarga tbody > tr").length==0){
        $(".text_transpasos_recarga").show();
        var html='<tr class="tr_recha">\
            <td><input type="hidden" id="idproductos" value="'+id+'"><input type="hidden" id="cat_reca" value="'+cant_lit+'">'+txt_recha+'</td>\
            <td>Almacén General</td>\
            <td>'+cant_lit+' L</td>\
            <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_rowrec()"><i class="fa fa-trash icon_font"></i></a></td>\
        </tr>';
        $('#body_recarga').append(html);
        $(".btn_add_reche").attr("disabled",true);
    }else{
        swal("¡Atención!", 'Ya existe un registro de solicitud de recarga en tabla "Recargas"', "error");  
    }
}

function save_recarga(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_recarga tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductos']").val();
        item ["cantidad"] = $(this).find("input[id*='cat_reca']").val();
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_recarga tbody > tr").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/guardar_soliciutd_recarga',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function delete_rowrec(){
    $('.tr_recha').remove();
    $(".btn_add_reche").attr("disabled",false);   
}

function tipoTraslado(){
    if($("#recargas").is(":checked")==true){
        $(".cont_search_tras").hide("slow");
        $(".cont_rechange").show("slow");
    }else{
        $(".cont_rechange").hide("slow");
        $(".cont_search_tras").show("slow");
    }
}

function formatStateProds (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + '</span> <span class="colum_in_3">$' + state.precio_con_ivax + '</span>'
  );
  return $state;
};

function search_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatStateProds,
        ajax: {
            url: base_url+'Traspasos/searchproductos',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    //id_sucursal_salida:$("#id_sucursal_salida option:selected").val(),
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var nom_text='';
                    if(element.incluye_iva==1){
                        precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        if(element.iva==0){
                            incluye_iva=0;
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio*1.16).toFixed(2);
                            sub_iva=parseFloat(element.precio*.16).toFixed(2);
                        }
                    }else{
                        if(element.iva>0){
                            siva=1.16;
                            precio_con_ivax=parseFloat(element.precio/siva).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);  
                        }
                    }
                    if(element.tipo==0){
                        nom_text=element.idProducto+' '+element.nombre+' / Cantidad:'+element.stock+" / $"+precio_con_ivax; 
                    }else if(element.tipo==1){
                        nom_text='Serie: '+element.idProducto+' '+element.nombre+' / Cantidad:'+element.serie8+" / $"+precio_con_ivax; 
                    }else if(element.tipo==2){
                        nom_text='Lote: '+element.idProducto+' '+element.nombre+' / Cantidad:'+element.lote8+" / $"+precio_con_ivax; 
                    }
                    
                    if(element.status_ht==2 && element.id_serht!=0 && element.tipo==1 || element.id_serht==0 && element.tipo==1 || element.status_traslt==2 && element.id_traslado_lote>0 && element.tipo==2 || element.id_traslado_lote==0 && element.tipo==2 || element.tipo==0){
                        itemscli.push({
                            id: element.id,
                            text: nom_text,
                            nombre: element.nombre,
                            tipo:element.tipo,
                            lote8:element.lote8,
                            serie8:element.serie8,
                            stock:element.stock,
                            id_prod_suc:element.id_prod_suc,
                            id_prod_suc_serie:element.id_prod_suc_serie,
                            incluye_iva:element.incluye_iva,
                            iva:element.iva
                        });
                    }
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
       // get_producto(data.id);
        prod=data.nombre;
        if(data.tipo==0){ //normal
            cant_stock=data.stock;
        }else if(data.tipo==1){ //serie
            cant_stock=data.serie8
        }else if(data.tipo==2){ //lote
            cant_stock=data.lote8;
        }
        $("#idproducto option:selected").attr('data-stock',cant_stock);
        $("#idproducto option:selected").attr('data-tipo',data.tipo);
        $("#idproducto option:selected").attr('data-id_prod_suc',data.id_prod_suc);
        $("#idproducto option:selected").attr('data-id_prod_suc_serie',data.id_prod_suc_serie);

        get_producto();
    });
}

function search_producto_all(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatStateProds,
        ajax: {
            url: base_url+'Traspasos/search_all_productos',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var nom_text='';
                    if(element.incluye_iva==1){
                        precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        if(element.iva==0){
                            incluye_iva=0;
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio*1.16).toFixed(2);
                            sub_iva=parseFloat(element.precio*.16).toFixed(2);
                        }
                    }else{
                        if(element.iva>0){
                            siva=1.16;
                            precio_con_ivax=parseFloat(element.precio/siva).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);  
                        }
                    }
                    /*if(element.tipo==0){
                        nom_text=element.idProducto+' '+element.nombre+' / Cantidad:'+element.stock+" / $"+precio_con_ivax; 
                    }else if(element.tipo==1){
                        nom_text='Serie: '+element.idProducto+' '+element.nombre+' / Cantidad:'+element.serie8+" / $"+precio_con_ivax; 
                    }else if(element.tipo==2){
                        nom_text='Lote: '+element.idProducto+' '+element.nombre+' / Cantidad:'+element.lote8+" / $"+precio_con_ivax; 
                    }*/
                    if(element.tipo==0){
                        nom_text=element.idProducto+' '+element.nombre+' '+" / $"+precio_con_ivax; 
                    }else if(element.tipo==1){
                        nom_text='Serie: '+element.idProducto+' '+element.nombre+' '+" / $"+precio_con_ivax; 
                    }else if(element.tipo==2){
                        nom_text='Lote: '+element.idProducto+' '+element.nombre+' '+" / $"+precio_con_ivax; 
                    }
                
                    itemscli.push({
                        id: element.id,
                        text: nom_text,
                        nombre: element.nombre,
                        tipo:element.tipo,
                        lote8:element.lote8,
                        serie8:element.serie8,
                        stock:element.stock,
                        incluye_iva:element.incluye_iva,
                        iva:element.iva,
                        precio_con_ivax:precio_con_ivax,
                        idProducto:element.idProducto
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
       // get_producto(data.id);
        prod=data.nombre;
        if(data.tipo==0){ //normal
            cant_stock=data.stock;
        }else if(data.tipo==1){ //serie
            cant_stock=data.serie8
        }else if(data.tipo==2){ //lote
            cant_stock=data.lote8;
        }
        $("#idproducto option:selected").attr('data-stock',cant_stock);
        $("#idproducto option:selected").attr('data-tipo',data.tipo);
        $("#idproducto option:selected").attr('data-id_prod_suc',0);
        $("#idproducto option:selected").attr('data-id_prod_suc_serie',0);
        //get_producto();
        //llamar funcion para mostrar sucursales en modal con cantidades, a aceptar bajarlo a la tabla
        //console.log("search_producto_all: "+data.tipo);
        get_productos_sucursal();
    });
}

function get_productos_sucursal(){
    //console.log("get_productos_sucursal: ");
    var id_prod=$("#idproducto option:selected").val();
    if(id_prod!=""){
        var tipo = Number($("#idproducto option:selected").data("tipo"));
        $("#modal_suc_prod").modal("show");
        $.ajax({
            data:{id_prod:id_prod,tipo:tipo},
            type: 'POST',
            url: base_url+'Traspasos/get_productos_sucursales',
            success: function(result){
                $("#body_prodsuc").html(result);
            }
        });
    }else{
        swal("¡Atención!","Elija un producto", "error");
    }
}

function aceptarPS(id_prod,id_suc,cant){
    get_producto(id_suc,cant);
    $("#modal_suc_prod").modal("hide");
}

function get_producto(id_suc,cant){
    var id=$("#idproducto option:selected").val();
    var nombre = prod;
    var cantidad = Number($("#cantidad").val());
    var stock = Number(cant);
    //var stock = Number($("#idproducto option:selected").data("stock"));
    var tipo = Number($("#idproducto option:selected").data("tipo"));
    var id_prod_suc = Number($("#idproducto option:selected").data("id_prod_suc"));
    var id_prod_suc_serie = Number($("#idproducto option:selected").data("id_prod_suc_serie"));
    var html="";
    //console.log("cantidad: "+cantidad);
    if(cantidad==0){
        swal("¡Atención!","Ingreso una cantidad a solicitar", "error");
        return;
    }
    if(stock<cantidad){
        swal("¡Atención!","No se cuenta con suficiente stock en almacén general", "error");
        return;
    }
    
    if(tipo==0){
        $('.text_transpasos_stock').css('display','block');
        var validars=0;
        var tabla_stocks = $("#table_datos_stock tbody > .tr_s");
        tabla_stocks.each(function(){         
            var stock_s = $(this).find("input[id*='idproductos']").val();
            if(stock_s==id){
                validars++;
            }
        });
        if(validars<1){
            tabla_stock(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('');
            }, 500);
        }else{
            swal("¡Atención!", "La serie ya existe", "error");    
        }
    }else if(tipo==1){
        $('.text_transpasos_series').css('display','block');
        var validarsr=0;
        var tabla_seriesr = $("#table_datos_series tbody > .tr_sr");
        tabla_seriesr.each(function(){         
            var serie_s = $(this).find("input[id*='idproductos']").val();
            if(serie_s==id){
                validarsr++;
            }
        });
        if(validarsr<1){
            tabla_series(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('');
            }, 500);
        }else{
            swal("¡Atención!", "La lote ya existe", "error");    
        }
    }else if(tipo==2){
        $('.text_transpasos_lotes').css('display','block');
        var validarlo=0;
        var tabla_seriesr = $("#table_datos_lotes tbody > .tr_lo");
        tabla_seriesr.each(function(){         
            var lotes_l = $(this).find("input[id*='idproductolo']").val();
            if(lotes_l==id){
                validarlo++;
            }
        });
        if(validarlo<1){
            tabla_lotes(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('');
            }, 500);
        }else{
            swal("¡Atención!", "El lote ya existe", "error");    
        }
    }   
    /*var repetido=0;
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='idprod']").val(); 
        if (id == prodexi) {
            repetido=1;
        }   
    });
    if(repetido==0){
        html="<tr class='row_p"+id+"'>\
                <td>"+nombre+"</td>\
                <td><input type='hidden' id='idprod' value='"+id+"'><input class='form-control' id='cantidad_sol' type='number' value='"+cantidad+"'></td>\
                <td>"+stock+"</td>\
                <td><button class='btn btn-danger' onclick='eliminar("+id+")'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>\
            </tr>";
        $("#body_prods").append(html);
        $('#idproducto').val(null).trigger('change');
        $("#cantidad").val("");
    }else{
        swal("¡Atención!","Producto ya existente en tabla de solicitud", "error");
    }*/
}

var cont_s=0;
function tabla_stock(id,producto,cantidad,id_prod,id_prodserie,id_suc){
    array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Renta","Almacén General"];
    var html='<tr class="tr_s row_s_'+cont_s+'">\
        <td><input type="hidden" id="idproductos" value="'+id+'">\
            <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
            <input type="hidden" id="cantidads" value="'+cantidad+'">'+producto+'</td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">'+array_suc[id_suc]+'</td>\
        <td>'+cantidad+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_stock('+cont_s+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_stock').append(html);
    cont_s++;
}

function delete_stock(cont){
    $('.row_s_'+cont).remove();    
}

var cont_sr=0;
function tabla_series(id,producto,cantidad,id_prod,id_prodserie,id_suc){
    array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General"];
    var html='<tr class="tr_sr row_sr_'+cont_sr+'">\
        <td><input type="hidden" id="idproductosr" value="'+id+'">\
        <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
        <input type="hidden" id="cantidadsr" class="cantidadsr_'+cont_sr+'" value="'+cantidad+'">'+producto+'\
           <!--<table class="table table-sm table_datos_series_detalles" id="table_datos_series_detalles_'+cont_sr+'">\
                <thead>\
                    <tr>\
                        <th scope="col">Serie</th>\
                        <th scope="col"></th>\
                    </tr>\
                </thead>\
                <tbody>\
                </tbody>\
            </table>-->\
        </td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">'+array_suc[id_suc]+'</td>\
        <td>'+cantidad+'</td>\
        <td><button type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+cont_sr+')"><i class="fa fa-trash icon_font"></i></button></td>\
        <!--<td><div class="select_txt_serie_'+cont_sr+'"></div></td>\
        <td><a class="btn btn-pill btn-primary" type="button" onclick="add_series_tabla('+cont_sr+')"><i class="fa fa-plus"></i></a><a type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+cont_sr+')"><i class="fa fa-trash icon_font"></i></a></td>-->\
    </tr>';
    /*html+='<tr class="row_sr_d_'+cont_sr+'">\
        <td colspan="4">\
            <div class="tabla_txt_serie_'+cont_sr+'">\
                <table class="table table-sm table_datos_series_detalles" id="table_datos_series_detalles_'+cont_sr+'">\
                    <thead>\
                        <tr>\
                            <th scope="col">Serie</th>\
                            <th scope="col"></th>\
                        </tr>\
                    </thead>\
                    <tbody>\
                    </tbody>\
                </table>\
            </div>\
        </td>\
    </tr>';*/
    $('#table_datos_series').append(html);
    //get_select_serie(cont_sr,id);
    cont_sr++;
}

function get_select_serie(cont_sr,idproducto){
    $.ajax({
        data:{idproducto:idproducto,cont:cont_sr},
        type: 'POST',
        url: base_url+'Traspasos/get_productos_series',
        success: function(result){
            $('.select_txt_serie_'+cont_sr).html(result);
        }
    });
}

function delete_serie(cont){
    $('.row_sr_'+cont).remove();
    //$('.row_sr_d_'+cont).remove();    
}

function add_series_tabla(cont){
    var idreg=$('#series_s_'+cont+' option:selected').val();
    var reg_text=$('#series_s_'+cont+' option:selected').text();
    if(idreg!=0){
        var validars=0;
        var tabla_serie = $("#table_datos_series_detalles_"+cont+" tbody > .tr_s"+cont);
        tabla_serie.each(function(){         
            var serie_x = $(this).find("input[id*='idserie_s']").val();
            if(serie_x==idreg){
                validars++;
            }
        });
        var num_serie=$("#table_datos_series_detalles_"+cont+" tbody > .tr_s"+cont).length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidadsr_'+cont).val();

        var cs=parseFloat(cantidad_serie);
        if(ns<cs){
            if(validars<1){
                tabla_series_detalle(idreg,reg_text,cont);
            }else{
                swal("¡Atención!", "La serie ya existe", "error");    
            }
        }else{
            swal("¡Atención!", "Solo puedes agregar "+cantidad_serie+"", "error");    
        }
            
    }else{
        swal("¡Atención!", "Falta seleccionar una serie", "error"); 
    }
}

var cont_srd=0;
function tabla_series_detalle(idreg,reg_text,cont){
    var html='<tr class="tr_srd tr_s'+cont+' row_srd_'+cont_srd+'">\
        <td><input type="hidden" id="idserie_s" value="'+idreg+'">'+reg_text+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_serie_detalle('+cont_srd+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_series_detalles_'+cont).append(html);
    cont_srd++; 
}

function delete_serie_detalle(cont){
    $('.row_srd_'+cont).remove();    
}

/////////////////////////////////////////////// LOTES

var cont_lo=0;
function tabla_lotes(id,producto,cantidad,id_prod,id_prodserie,id_suc){
    array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General"];
    var html='<tr class="tr_lo row_lo_'+cont_lo+'">\
        <td><input type="hidden" id="idproductolo" value="'+id+'">\
            <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
            <input type="hidden" id="cantidadlo" class="cantidadlo_'+cont_lo+'" value="'+cantidad+'">'+producto+'\
            <!--<table class="table table-sm table_datos_lotes_detalles" id="table_datos_lotes_detalles_'+cont_lo+'">\
                <thead>\
                    <tr>\
                        <th colspan="2" scope="col">Lotes</th>\
                        <th scope="col">Cantidad</th>\
                        <th scope="col"></th>\
                    </tr>\
                </thead>\
                <tbody>\
                </tbody>\
            </table>-->\
        </td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">'+array_suc[id_suc]+'</td>\
        <td>'+cantidad+'</td>\
        <!--<td><div class="select_txt_lotes_'+cont_lo+'"></div></td>\
        <td><input type"text" class="form-control cantidadlo_d'+cont_lo+'"></td>\
        <td><a class="btn btn-pill btn-primary" type="button" onclick="add_lotes_tabla('+cont_lo+')"><i class="fa fa-plus"></i></a><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote('+cont_lo+')"><i class="fa fa-trash icon_font"></i></a></td>-->\
    </tr>';
    /*html+='<tr class="row_lo_d_'+cont_lo+'">\
        <td colspan="4">\
            <div class="tabla_txt_lote_'+cont_lo+'">\
                <table class="table table-sm" id="table_datos_lotes_detalles_'+cont_lo+'">\
                    <thead>\
                        <tr>\
                            <th scope="col">Lotes</th>\
                            <th scope="col">Cantidad</th>\
                            <th scope="col"></th>\
                        </tr>\
                    </thead>\
                    <tbody>\
                    </tbody>\
                </table>\
            </div>\
        </td>\
    </tr>';*/
    $('#table_datos_lotes').append(html);
    //get_select_lote(cont_lo,id);
    cont_lo++;
}

/*function get_select_lote(cont,idproducto){
    $.ajax({
        data:{idproducto:idproducto,cont:cont},
        type: 'POST',
        url: base_url+'Traspasos/get_productos_lote',
        success: function(result){
            $('.select_txt_lotes_'+cont).html(result);
        }
    });
}

function delete_lote(cont){
    $('.row_lo_'+cont).remove();
    //$('.row_lo_d_'+cont).remove();    
}*/

function add_lotes_tabla(cont){
    var idreg=$('#lote_l_'+cont+' option:selected').val();
    var cantidad_lote=$('#lote_l_'+cont+' option:selected').data('cantidad');
    var reg_text=$('#lote_l_'+cont+' option:selected').data('lotetxt');
    var cant_lo=parseFloat(cantidad_lote);
    var cantidad=$('.cantidadlo_d'+cont).val();
    var cant=parseFloat(cantidad);
    if(idreg!=0){
        if(cantidad_lote>=cant){
            var validars=0;
            var tabla_lote = $("#table_datos_lotes_detalles_"+cont+" tbody > .row_lo_d"+cont);
            var cantida_x_lote=0;
            tabla_lote.each(function(){         
                var lote_x = $(this).find("input[id*='idlote_l']").val();
                var lote_num_x = $(this).find("input[id*='cantidad_l']").val();
                cantida_x_lote+=parseFloat(lote_num_x);
                if(lote_x==idreg){
                    validars++;
                }
                
            });
            var suma=parseFloat(cantidad)+parseFloat(cantida_x_lote);
            var nl=parseFloat(suma);
            var cantidad_lote_pro=$('.cantidadlo_'+cont).val();
            var cs=parseFloat(cantidad_lote_pro);
            var resta=cs-cantida_x_lote;
            if(nl<=cs){
                if(validars<1){
                    tabla_lotes_detalle(idreg,reg_text,cantidad,cont);
                    $("#lote_l_0").val(0);
                    $('.cantidadlo_d'+cont).val('');
                }else{
                    swal("¡Atención!", "El lote ya existe", "error");    
                }
            }else{
                swal("¡Atención!", "Solo puedes agregar "+resta+"", "error");    
            }  
        }else{
            swal("¡Atención!", "El lote no cuenta con cantidad suficiente", "error"); 
        } 
    }else{
        swal("¡Atención!", "Falta seleccionar un lote", "error"); 
    }
}

var cont_lo_d=0;
function tabla_lotes_detalle(idreg,reg_text,cantidad,cont){
    var html='<tr class="tr_lod row_lo_d'+cont_lo_d+'">\
        <td colspan="2"><input type="hidden" id="idlote_l" value="'+idreg+'">'+reg_text+'</td>\
        <td><input type="hidden" id="cantidad_l" value="'+cantidad+'">'+cantidad+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote_detalle('+cont_lo_d+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $("#table_datos_lotes_detalles_"+cont).append(html);
    cont_lo_d++; 
}

function delete_lote_detalle(cont){
    $('.row_lo_d'+cont).remove();    
}

function validar_cerrar_traspaso(){
    if($("#table_datos_recarga tbody > tr").length==0 && $("#table_datos_stock tbody > tr").length==0 && $("#table_datos_series tbody > tr").length==0 && $("#table_datos_lotes tbody > tr").length==0){
        swal("¡Atención!", 'Ingrese al menos un producto a solicitar', "error");
        return;
    }
    if($('#id_sucursal option:selected').val()!="0"){
        $.ajax({
        type:'POST',
        url: base_url+"Traspasos/registro_solicitud_traspaso",
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        beforeSend: function(){
            $('.btn_transitoy').attr('disabled',true);
        },
        success:function(data){
            ////////////
            swal("Éxito!", "Guardando por favor espere...", "success");
            var idreg=parseFloat(data);
            if($("#table_datos_recarga tbody > tr").length>0){
                save_recarga(idreg);
            }
            if($("#table_datos_stock tbody > tr").length>0){
                solicitud_stock(idreg);
            }
            if($("#table_datos_series tbody > tr").length>0){
                solicitud_serie(idreg);
            }
            if($("#table_datos_lotes tbody > tr").length>0){
                solicitud_lote(idreg);
            }
            setTimeout(function(){ 
                envia_mail(idreg);
            }, 2000); 
            
            setTimeout(function(){ 
                swal("Éxito!", "Guardado Correctamente", "success");
                window.location.href = base_url+"Traspasos";
            }, 3000);  
            ////////////
        }
    });
    }else{
        swal("¡Atención!", 'Elije una sucursal solicitante', "error"); 
    }
}

function envia_mail(id){
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/mail_solicitud",
        data: { id:id },
        success:function(data){

        }
    });
}

function solicitud_stock(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_stock tbody > .tr_s");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductos']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidads']").val();
        item ["id_ordenc"] = 0;
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_stock tbody > .tr_s").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitud_serie(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_series tbody > .tr_sr");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductosr']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadsr']").val();
        /*var DATA_d  = [];
        var TABLA_d = $(this).find('.table_datos_series_detalles tbody > .tr_srd');
        TABLA_d.each(function(){ 
            item_d = {};
            item_d ["idseries"] = $(this).find("input[id*='idserie_s']").val();
            DATA_d.push(item_d);
        });
        item ["serie_detalles"]   = DATA_d;*/
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_series tbody > .tr_sr").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto_series',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitud_lote(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_lotes tbody > .tr_lo");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductolo']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadlo']").val();
        /*var DATA_d  = [];
        var TABLA_d = $(this).find('.table_datos_lotes_detalles tbody > .tr_lod');
        TABLA_d.each(function(){ 
            item_d = {};
            item_d ["idlote"] = $(this).find("input[id*='idlote_l']").val();
            item_d ["cantidad"] = $(this).find("input[id*='cantidad_l']").val();
            DATA_d.push(item_d);
        });
        item ["lote_detalles"] = DATA_d;*/
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_lotes tbody > .tr_lo").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto_lote',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitar_traspasovvvv(id,tipo,cont){
    $('.btn_transito_'+cont).attr('disabled',true);
    if(tipo==1){
        var num_serie=$("#table_datos_traspasos tbody > .tr_sx").length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidad_serie_'+cont).val();
        var cs=parseFloat(cantidad_serie);
        if(ns==cs){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos tbody > .tr_sx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idseries"] = $(this).find("input[id*='idseriex']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar series", "error");
        }
    }else if(tipo==2){
        var num_lote=$("#table_datos_traspasos_lotes tbody > .tr_lx").length; 
        var nl=parseFloat(num_lote);
        if(nl!=0){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos_lotes tbody > .tr_lx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idlotes"] = $(this).find("input[id*='idlotex']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
                item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
                item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos_lotes',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar lote", "error");
        }
    }else{
        swal("Éxito!", "Guardado Correctamente", "success");
        /*$.ajax({
            type:'POST',
            url: base_url+"Traspasos/update_record_transpaso_transito",
            data:{id:id},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    tabla.ajax.reload(); 
                    $('#modaldetalles_apartado').modal('hide');
                    $('.btn_transito').attr('disabled',false);
                }, 1000); 
            }
        }); */
    }
    
}
