var base_url = $('#base_url').val();
$(document).ready(function() {
    $("#savesm").on("click",function(){
        save_solicita();
    });
    selectEquipo();
    setTimeout(function(){ 
        if($("#id").val()=="0" && $("#idrenta").val()=="0"){
            $('#id_equipo').select2('open');
            $('.select2-search__field').focus()
        };
        $('.select2-selection__placeholder').addClass('colorsel');
        $('.select2-selection__placeholder').attr('style', 'color: white !important');
    }, 1000);
    select_insumo();
});

function formatState_input_searchrec (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.producto + '</span><span class="colum_in_3">'+state.serie+'</span>'
  );
  return $state;
};

function selectEquipo(){
    $('#id_equipo').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un equipo',
        allowClear: true,
        templateResult: formatState_input_searchrec,
        ajax: {
            url: base_url+'Mttos_internos/searchEquipos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id_prod,
                        text: element.idProducto+" / "+element.producto+" Serie: "+element.serie,
                        idProducto: element.idProducto,
                        producto: element.producto,
                        serie: element.serie,
                        id_pss: element.id
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#id_equipo option:selected").attr('data-id_pss',data.id_pss);
        $("#id_equipo option:selected").attr('data-serie',data.serie);
        $("#serie").val(data.serie);
        $("#id_serie").val(data.id_pss);
    });
}

function save_solicita(){
    $("#id_equipo").attr("disabled",false);
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_equipo:{
              required: true,
            },
            /*fecha_termino:{
              required: true,
            },
            prox_mtto:{
              required: true,
            }*/
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        if($("#table_insumos tbody > tr").length==0){
            $("#id_equipo").attr("disabled",true);
            swal("Atención!", "No se puede guardar mantenimiento sin indicar insumos utilizados", "warning");
            return false;
        }
        var insumos = $("#table_insumos tbody > tr");
        var DATAi  = [];
        insumos.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id']").val();
            item ["stock"] = $(this).find("input[id*='stock']").val();
            item ["id_ins"] = $(this).find("input[id*='id_ins']").val();
            item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
            item ["cant_ins"] = $(this).find("input[id*='cant_ins']").val();
            if($(this).find("input[id*='id']").val()==0){
                DATAi.push(item);
            }
        });
        INFOi  = new FormData();
        aInfoi = JSON.stringify(DATAi);
        //console.log("aInfoi: "+aInfoi);

        var datos = form_register.serialize()+"&insumos="+aInfoi;
        //console.log("datos: "+datos);
        $('#savesm').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Mttos_internos/save_mtto',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $('#savesm').attr('disabled',true);
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Mttos_internos';
                }, 1500);
            }
        });  
    } 
}

function formatState_insumo(state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + '</span>'
  );
  return $state;
};

function select_insumo(){
    $('#sel_insumo').select2({
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un insumo',
        allowClear: true,
        templateResult: formatState_insumo,
        ajax: {
            url: base_url+'Mantenimientos/searchInsumo',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var cont=0;
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' / '+element.nombre + " (stock: "+element.stock_suc+")",
                        idProducto: element.idProducto,
                        nombre: element.nombre+ " (stock: "+element.stock_suc+")",
                        tipo: "3_1",
                        id_ps: element.id_ps,
                        stock_suc: element.stock_suc
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#sel_insumo option:selected").attr('data-tipo',data.tipo);
        $("#sel_insumo option:selected").attr('data-id_ps',data.id_ps);
        $("#sel_insumo option:selected").attr('data-stock_suc',data.stock_suc);
        if(data.stock_suc>0){
            get_insumo();
        }else{
            swal("Atención!", "Insumo sin stock suficiente", "warning");
            return false;
        }
    });
}

function get_insumo(){
    var id =$("#sel_insumo option:selected").val();
    var text_ins =$("#sel_insumo option:selected").text();
    var tipo =$("#sel_insumo option:selected").data("tipo");
    var id_ps =$("#sel_insumo option:selected").data("id_ps");
    var stock =$("#sel_insumo option:selected").data("stock_suc");
    var id_mtto = $("#id_mttoadd").val();
    var html="";
    var repetido=0;
    var TABLA   = $("#table_insumos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='id_ins']").val(); 
        if (id == prodexi) {
            repetido=1;
            $("#cant_ins").val(Number($("#cant_ins").val())+1);
        }   
    });
    if(repetido==0){
        html="<tr class='row_ins row_ins_"+id+"'>\
                <td><input type='hidden' id='id' value='0'><input type='hidden' id='id_ins' value='"+id+"'>\
                <input type='hidden' id='id_ps' value='"+id_ps+"'>\
                <input type='hidden' id='id_mtto' value='"+id_mtto+"'>\
                <input type='hidden' id='stock' value='"+stock+"'>\
                <input class='form-control' id='cant_ins' type='number' value='1'>\
            </td>\
            <td>"+text_ins+"</td>\
            <td><button class='btn btn-danger' onclick='deleteIns("+id+",0)'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>\
        </tr>";
        $(".det_ins_serv").append(html);
        $('#sel_insumo').val(null).trigger('change');
    }
}

function deleteIns(id,id_bi){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del insumo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if(id_bi==0){
                    $(".row_ins_"+id).remove();
                }else{
                    $.ajax({
                        type:'POST',
                        url: base_url+"Mttos_internos/delete_insumo_mtto",
                        data:{id:id_bi},
                        async:false,
                        success:function(data){
                            $(".row_ins_"+id).remove();
                            swal("Éxito!", "Eliminado correctamente", "success");
                        }
                    });
                }
            },
            cancelar: function (){
                
            }
        }
    });
}