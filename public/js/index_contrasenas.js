var base_url = $('#base_url').val();
function generar_contrasena(){
	$.ajax({
	    type:'POST',
	    url: base_url+"Contrasena/generarcontrasena",
	    data:{id:$('#valor').val()},
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
	        $('#codigo').val(data);
	        $('.btn_registro').attr('disabled',true);
	        setTimeout(function(){ 
                $('.btn_registro').css('cursor','no-drop');
            }, 500);
	        mi_visor=document.getElementById("reloj_mi"); //localizar pantalla del reloj
	        cs_visor=document.getElementById("reloj_cs"); //localizar pantalla del reloj
			sg_visor=document.getElementById("reloj_sg"); //localizar pantalla del reloj
			empezar ();
	        setTimeout(function(){ 
	            $('.btn_registro').attr('disabled',false);
	            $('.btn_registro').css('cursor','pointer');
	            $('#codigo').val('');
	            swal("Éxito!", "Contraseña finalizada", "success");
	            window.location = base_url+'Contrasena';
	        }, 300000);
	        //}, 5000);
	    }
	}); 
}

//variables de inicio:
var marcha;
var cro=0; 

function empezar() 
{
    emp=new Date() //fecha actual
    emp.setMinutes(emp.getMinutes() + 5); // agrego 1 min a la fecha actual
	elcrono=setInterval(tiempo,10); //función ejecutada cada decima de segundo
	marcha=1 //indicamos que se ha puesto en marcha.
}
	  
//función del temporizador
function tiempo() 
{ 
    actual=new Date() //fecha en el instante
	cro=emp- actual //resto fecha+60seg - fecha actual
	cr=new Date() //fecha donde guardamos el tiempo transcurrido
    cr.setTime(cro) 
    mi=cr.getMinutes();
    cs=cr.getMilliseconds() //milisegundos del cronómetro
    cs=cs/10; //paso a centésimas de segundo.
    cs=Math.round(cs)
    sg=cr.getSeconds(); //segundos del cronómetro
    if (mi<10) {mi="0"+mi;}  //poner siempre 2 cifras en los números
    if (cs<10) {cs="0"+cs;}  
	if (sg<10) {sg="0"+sg;} 
	mi_visor.innerHTML= mi; //pasar a pantalla.
    cs_visor.innerHTML= cs; //pasar a pantalla.
    sg_visor.innerHTML= sg; //pasar a pantalla.
  }