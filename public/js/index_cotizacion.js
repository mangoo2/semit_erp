var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
    $('#searchtext').focus();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Cotizaciones/getlistado",
            type: "post",
            "data":{sucursal:$('#sucursal').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"folio"},
            {"data":"name_suc"},
            {"data":"razon_social"},
            {"data":"nombre"},
            {"data":"reg"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a class="btn" onclick="modal_productos('+row.id+')"><i class="fa fa-eye" style="font-size: 24px; color: #012d6a;"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a style="cursor:pointer" onclick="get_cotizacion('+row.id+')"><img class="img_icon" src="'+base_url+'public/img/pdf1.png"></a>\
                        <a class="cotiza_mail_'+row.id+'" data-mail_cot="'+row.correo+'" style="cursor:pointer" onclick="modal_cotizacion('+row.id+')"><img class="img_icon_c" src="'+base_url+'public/img/ennvio3.svg"></a>';
                    html+='</div>';
                return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                     if(row.id==1){
                        html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">Aceptada</span>';
                     }else{
                        html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">No aceptada</span>';
                     }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}

function modal_productos(id){
    $('#modalproductos').modal('show');
    $('.text_tabla_productos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/get_productos",
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_tabla_productos').html(data);
        }
    }); 
}

var idcotizacionaux=0;
function modal_cotizacion(id){
    idcotizacionaux=id;
    $('#modal_cotizacion').modal('show');
    var mail = $(".cotiza_mail_"+id).data("mail_cot");
    $("#correo_cl").val(mail);
}

function enviar_correo_cotizacion(){
    var correo=$('#correo_cl').val();
    if(correo!=''){
        swal("Éxito!", "Enviando correo...", "success");
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/enviar_aviso",
            data: {id:idcotizacionaux,correo:correo},
            success: function (response){
                swal("Éxito!", "Cotización enviada", "success");
                setTimeout(function(){ 
                    $('#modal_cotizacion').modal('hide');
                }, 1000); 
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
    }else{
        swal("¡Atención!", "Falta agregar un correo", "error");
    }
        
}

function get_cotizacion(id){
    window.open(base_url+'Cotizaciones/imprimir_cotizacion/'+id,'_blank'); 
}