var table;
var base_url=$('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {	
    tabla();
    $(".btn_aceptar").on("click",function(){
        saveSolicitaDet();
    });

    $(".save_orden").on("click",function(){
        saveOrden();
    });
});

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Garantias/get_data_list",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "reg"},
            {"data": "personal"},
            {"data": "motivo"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.id_venta==0){
                        html='Prod. Interno';
                    }if(row.id_venta>0){
                        html='Prod. de Venta';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="getModal('+row.id+',0)"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    //if(row.id_venta>0){
                        html='<button type="button" class="btn btn-primary" onclick="getEvidencia('+row.id_venta+','+row.id+')"><i class="fas fa-file-image-o"></i></button>';
                    //}
                    return html;
                }
            },
            {"data": "suc_solicita"},
            {"data": null,
                render:function(data,type,row){
                    var html=''; var event="";
                    if(perfil==1 || perfil==4 || perfil==5){
                        event='onclick="getModal('+row.id+',1)"';
                    }
                    if(row.estatus==1){
                        html="<span class='btn btn-warning' "+event+">Solicitado</span>";
                    }if(row.estatus==2){
                        html="<span class='btn btn-success' "+event+">Con proveedor</span>";
                    }if(row.estatus==3){
                        html="<span class='btn btn-danger' "+event+">Rechazado</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<a title="PDF Solicitud" style="cursor:pointer" onclick="get_pdf('+row.id_venta+','+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    if(row.estatus!=3 && perfil==1 && row.estatus==1 || row.estatus!=3 && perfil==5 && row.estatus==1){
                        html+='<a title="Rechazar Solicitud" onclick="rechazar('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }
                    if(row.estatus==2){
                        html+='<a title="PDF Orden" style="cursor:pointer" onclick="get_pdf_orden('+row.id+','+row.id_venta+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        columnDefs: [ { orderable: false, targets: [4,6,7] }],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function getModal(id,tipo) {
    $('#modaldetalles').modal('show');
    view_productos(id,tipo);
}

function view_productos(id,tipo=0){
	$.ajax({
        type:'POST',
        url: base_url+"Garantias/view_detalles",
        data: {
        	id:id,tipo:tipo
        },
        success: function (response){
            $('#body_det').html(response);
            if(tipo==1){
                $('.cont_ok').show();
            }
        },
    });
}

function getEvidencia(idventa,id){
    $("#modalfiles").modal("show");
    $.ajax({
        type:'POST',
        url: base_url+'Garantias/getEvidencias',
        data: { id_venta: idventa, id: id },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}

function acepta_prod(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de aceptar la grantía del producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $(".rowprod_"+id).css({"background-color":"rgb(147,186,31,0.7)"});
                $("#rechazado_"+id).val(2);
                swal("Éxito!", "Aceptado correctamente", "success");
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function rechaza_prod(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de rechazar el producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $(".rowprod_"+id).css({"background-color":"rgb(255,64,64,0.7)"});
                $("#rechazado_"+id).val(3);
                $(".cont_mot_"+id).show();
                /*$.ajax({
                    type:'POST',
                    url: base_url+"Garantias/rechazar_producto",
                    data:{id:id,motivo:$("#motivo").val()},
                    success:function(data){
                        swal("Éxito!", "Rechazado correctamente", "success");
                        setTimeout(function(){ 
                            tabla(); 
                        }, 1000); 
                    }
                });*/ 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function saveSolicitaDet(){
    var DATA  = [];
    var TABLA   = $("#prod_detalle tbody > tr");
    var band_soli=0;
    TABLA.each(function(){         
        item = {};
        id = $(this).find("input[id*='id']").val();
        item ["id_garantia"] = $(this).find("input[id*='id_garantia']").val();
        item ["motivo"] = $('.motivo_'+id).val();
        item ["rechazado"] = $(this).find("input[id*='rechazado']").val();
        item ["id"] = $(this).find("input[id*='id']").val();
        item ["id_ps_ser"] = $(this).find("input[id*='id_ps_ser']").val();
        item ["id_psl"] = $(this).find("input[id*='id_psl']").val();
        item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
        item ["id_venta"] = $(this).find("input[id*='id_venta']").val();
        item ["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
        item ["cant"] = $(this).find("input[id*='cant']").val();
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        if($(this).find("input[id*='id']").val()){
            DATA.push(item);
        }
        if($(this).find("input[id*='rechazado']").val()=="1") { //está en estatus solicitado
            band_soli++;
        }
    });
    //console.log("band_soli: "+band_soli);
    if(band_soli>0){
        swal("Álerta!", "Indique si acepta o rechaza los productos", "warning");
        return;
    }

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);

    if($("#prod_detalle tbody > tr").length>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Garantias/editDetalles',
            processData: false, 
            contentType: false,
            async: false,
            beforeSend: function(){
                $('.btn_aceptar').attr('disabled',true);
            },
            success: function(data){
                $('#modaldetalles').modal('hide');
                swal("Éxito!", "Guardado correctamente", "success");
                $('.btn_aceptar').attr('disabled',false);
                setTimeout(function(){ 
                    tabla(); 
                }, 1000);
            }
        }); 
    }else{
        swal("¡Atención!","Ingresa al menos un producto", "error");
    }
}

function rechazar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de rechazar la solicitud?'+
                '<textarea class="form-control" id="motivo_all" placeholder="Motivo de rechazo"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Garantias/rechaza_record",
                    data:{id:id, motivo:$("#motivo_all").val()},
                    success:function(data){
                        swal("Éxito!", "Rechazado correctamente", "success");
                        setTimeout(function(){ 
                            tabla(); 
                        }, 1500); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_pdf(id_venta,id){
    window.open(base_url+'Garantias/pdf_garantia/'+id_venta+"/"+id,'_blank');  
}

function get_pdf_orden(id,id_venta){
    //window.open(base_url+'Garantias/pdf_garantia_orden/'+id,'_blank'); 
    $("#modalOrden").modal("show");
    $("#id_garantia").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Garantias/getDetOrdenG",
        data:{ id:id, id_venta:id_venta},
        success:function(data){
            var array = $.parseJSON(data);
            $("#id_ordeng").val(array.id);
            $("#fecha_orden").val(array.fecha);
            $("#observ").val(array.observ);
            $("#motivo_solicita").val(array.motivo);

            $('#nom_solicita').val(array.cli);
            $('#dir_solicita').val(array.dir);
            $('#col_solicita').val(array.col);
            $('#cel_solicita').val(array.cel);
            $('#tel_solicita').val(array.tel);
            $('#mail_solicita').val(array.mail);
            $('#rfc_solicita').val(array.rfc);
            $('#nom_contacto').val(array.nom_contacto);
            $('#tel_contacto').val(array.tel_contacto);
        }
    });
}

function saveOrden(){ 
    if($("#fecha_orden").val()==""){
       swal("Álerta!", "Indica una fecha para la orden de trabajo", "warning"); 
       return;
    }
    $.ajax({
        type:'POST',
        url: base_url+"Garantias/save_orden",
        data:{ id:$("#id_ordeng").val(), id_garantia:$("#id_garantia").val(), fecha:$("#fecha_orden").val(), observ:$("#observ").val(), 
            nom_solicita:$("#nom_solicita").val(), dir_solicita:$("#dir_solicita").val(), col_solicita:$("#col_solicita").val(), cel_solicita:$("#cel_solicita").val(),
            tel_solicita:$("#tel_solicita").val(), mail_solicita:$("#mail_solicita").val(), rfc_solicita:$("#rfc_solicita").val()
        },
        success:function(data){
            $("#modalOrden").modal("hide");
            swal("Éxito!", "Guardado correctamente", "success");
            setTimeout(function(){ 
                window.open(base_url+'Garantias/pdf_garantia_orden/'+$("#id_garantia").val(),'_blank'); 
            }, 1500); 
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
