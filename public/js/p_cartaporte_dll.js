var base_url =$('#base_url').val();
var id_pcp=$('#id_pcp').val();
$(document).ready(function($) {
	
	$('#save_general').click(function(event) {
		$( "#save_general" ).prop( "disabled", true );
	    setTimeout(function(){ 
	         $("#save_general" ).prop( "disabled", false );
	    }, 5000);
		save_general();
		
	});
	selectubica();
	info_veiculo();
	carga_destinos();
});

function info_veiculo(){
	var idv = $('#selectVehiculos option:selected').val();
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Cat_complemento/info_tras_fed",
            data: {
                id:idv
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                    console.log(array);
                    $('#txtCodigo').val(array.txtCodigo);
                    $('#txtNombre').val(array.txtNombre);
                    //falta tipo_permiso_sct
                    if(array.tipo_permiso_sct!=null){
                        $('#tipo_permiso_sct').html('<option value="'+array.tipo_permiso_sct+'">('+array.tipo_permiso_sct+') '+array.des_f_c_tp+'</option>');
                    }
                    $('#num_permiso_sct').val(array.num_permiso_sct);
                    $('#nombre_aseguradora').val(array.nombre_aseguradora);
                    $('#num_poliza_seguro').val(array.num_poliza_seguro);
                    $('#AseguraMedAmbiente').val(array.AseguraMedAmbiente);
                    $('#PolizaMedAmbiente').val(array.PolizaMedAmbiente);

                    $('#AseguraCarga').val(array.AseguraCarga);
                    $('#PolizaCarga').val(array.PolizaCarga);
                    $('#PrimaSeguro').val(array.PrimaSeguro);
                    //falta configuracion_vhicular
                    if(array.configuracion_vhicular!=null){
                        $('#configuracion_vhicular').html('<option value="'+array.configuracion_vhicular+'">('+array.configuracion_vhicular+') '+array.des_f_c_ct+'</option>');
                    }
                    $('#placa_vehiculo_motor').val(array.placa_vehiculo_motor);
                    $('#anio_modelo_vihiculo_motor').val(array.anio_modelo_vihiculo_motor);
                    $('#subtipo_remolque').val(array.subtipo_remolque);
                    $('#placa_remolque').val(array.placa_remolque);
                    $('#subtipo2_remolque').val(array.subtipo2_remolque);
                    $('#placa2_remolque').val(array.placa2_remolque);
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
}
function save_general(){
	var pasa_form=true;
		//==================================
			var form1=$('#form_platilla');
			var datosg = form1.serialize()
			if(form1.valid()){}else{
				pasa_form=false;
			}
		//=================================
			var form2=$('#form_ubi_o');
			var datosf2 = form2.serialize()
			if(form2.valid()){}else{
				pasa_form=false;
			}
		//=================================
			var destinos_tr = $("#table_destinos tbody > tr");
            //==============================================
                var DATAd  = [];
                destinos_tr.each(function(){         
                    item = {};                    
                    item ["id"]   = $(this).find("input[id*='ud_id']").val();
                    item ["des"]   = $(this).find("input[id*='ud_des']").val();
                    item ["fec"]   = $(this).find("input[id*='ud_fe']").val();
                    item ["dis"]   = $(this).find("input[id*='ud_dis']").val();
                    DATAd.push(item);
                });
                //INFOa  = new FormData();
                aInfod   = JSON.stringify(DATAd);
            //========================================
            if(destinos_tr.length==0){
            	pasa_form=false;
            	toastr.error('Favor de seleccionar por lo menos un destino');
            }
		//=================================
            var DATAfig  = [];
            var fig_tr_01 = $("#table_fig_01 tbody > tr");
            fig_tr_01.each(function(){         
                item = {};                    
                item ["idreg"]   = $(this).find("input[id*='idreg']").val();
                item ["id_fig"]  = $(this).find("input[id*='id_fig']").val();
                DATAfig.push(item);
            });
            var fig_tr_02 = $("#table_fig_02 tbody > tr");
            fig_tr_02.each(function(){         
                item = {};                    
                item ["idreg"]   = $(this).find("input[id*='idreg']").val();
                item ["id_fig"]  = $(this).find("input[id*='id_fig']").val();
                DATAfig.push(item);
            });
            var fig_tr_03 = $("#table_fig_03 tbody > tr");
            fig_tr_03.each(function(){         
                item = {};                    
                item ["idreg"]   = $(this).find("input[id*='idreg']").val();
                item ["id_fig"]  = $(this).find("input[id*='id_fig']").val();
                DATAfig.push(item);
            });
            var fig_tr_04 = $("#table_fig_04 tbody > tr");
            fig_tr_04.each(function(){         
                item = {};                    
                item ["idreg"]   = $(this).find("input[id*='idreg']").val();
                item ["id_fig"]  = $(this).find("input[id*='id_fig']").val();
                DATAfig.push(item);
            });
            //INFOa  = new FormData();
            aInfofig   = JSON.stringify(DATAfig);
        //==================================

	if(pasa_form){
		var selectVehiculos = $('#selectVehiculos option:selected').val();
		datosg=datosg+'&selectVehiculos='+selectVehiculos;
		$.ajax({
            type:'POST',
            url: base_url+"index.php/Cat_complemento/save_general",
            data: {
            	id_pcp:$('#id_pcp').val(),
                tableg:datosg,
                tableuo:datosf2,
                destino:aInfod,
                figuras:aInfofig
            },
            success: function (response){
                console.log(response);
                toastr.success('Guardado con exito');
                
                setTimeout(function(){ 
				    //location.reload();
				    location.href = base_url+'Cat_complemento/cat_carta_porte';
				}, 2000);
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
	}else{
		toastr.error('Verifica los campos requerios');
	}
}
function selectubica(){
	var ubi_o =$('#selectalmacenesorigen option:selected').val();
	$.ajax({
            type:'POST',
            url: base_url+"Cat_complemento/view_ubicaciones",
            data: {id:ubi_o},
            success: function (response){
                var array = $.parseJSON(response);
                $('#id_ubi').val(array.id);
                $('#cp_calle').val(array.cp_calle);
				$('#cp_n_ex').val(array.cp_n_ex);
				$('#cp_n_int').val(array.cp_n_int);
				$('#cp_colonia').val(array.cp_colonia);
				$('#cp_localidad').val(array.cp_localidad);
				$('#cp_ref').val(array.cp_ref);
				$('#cp_minicipio').val(array.cp_minicipio);
				$('#cp_estado').val(array.cp_estado);
				$('#cp_cl_estado').val(array.cp_cl_estado);
				$('#cp_pais').val(array.cp_pais);
				$('#cp_cp').val(array.cp_cp);
            },
            error: function(response){
            	console.log(response);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
}
function adddestino(){
	var des=$('#selectalmacenesorigen').html();
	var des_s=$('#selectalmacenesorigen option:selected').val();

	var html='';
		html+='<div class="row">';
			html+='<div class="col-md-12">';
				html+='Favor de seleccionar el destino';
			html+='</div>';
			html+='<div class="col-md-6">';
				html+='<label>Destino</label>';
				html+='<select id="s_destino"  class="form-control">';
				html+=des;
				html+='</select>';
			html+='</div>';
			html+='<div class="col-md-6">';
				html+='<label>Distancia</label>';
				html+='<input type="number" id="s_distancia" class="form-control">';
			html+='</div>';
			html+='<div class="col-md-6">';
				html+='<label>Fecha/Hora llegada</label>';
				html+='<input type="datetime-local" id="s_fecha" class="form-control">';
			html+='</div>';
		html+='</div>';


	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Destino',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var id_d=$('#s_destino option:selected').val();
                var dis=$('#s_distancia').val();
                var fe=$('#s_fecha').val();
                if(id_d>0 && dis>0){
                	adddestino_tr(0,id_d,dis,0,fe);
                }else{
                	toastr.error('Favor de seleccionar un destino y/o distancia ');
                	adddestino();
                }

                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){ 
	    //$('#s_destino option[value="'+des_s+'"]').prop('disabled', true);
	    $('#s_destino option[value="'+des_s+'"]').remove();
	}, 900);
    
}
function adddestino_tr(id,iddes,dis,row,fe){
	$.ajax({
            type:'POST',
            url: base_url+"Cat_complemento/view_ubicaciones",
            data: {id:iddes},
            success: function (response){
                var array = $.parseJSON(response);
                /*
                $('#id_ubi').val(array.id);
                $('#cp_calle').val(array.cp_calle);
				$('#cp_n_ex').val(array.cp_n_ex);
				$('#cp_n_int').val(array.cp_n_int);
				$('#cp_colonia').val(array.cp_colonia);
				$('#cp_localidad').val(array.cp_localidad);
				$('#cp_ref').val(array.cp_ref);
				$('#cp_minicipio').val(array.cp_minicipio);
				$('#cp_estado').val(array.cp_estado);
				$('#cp_cl_estado').val(array.cp_cl_estado);
				$('#cp_pais').val(array.cp_pais);
				$('#cp_cp').val(array.cp_cp);
				*/
                if(row>0){
                	var col=row;
                }else{
                	var col='';
                }

                var btn='<button type="button" class="btn btn-danger btn-sm" onclick="delete_tr_des('+id+','+iddes+')"><i class="fas fa-trash"></i></button>';
				var ht='';
					ht+='<tr class="tr_des_'+id+'_'+iddes+'">';
	                      ht+='<td>'+col;
	                      	ht+='<input type="hidden" value="'+id+'" id="ud_id">';
	                      	ht+='<input type="hidden" value="'+iddes+'" id="ud_des">';
	                      	ht+='<input type="hidden" value="'+fe+'" id="ud_fe">';
	                      	ht+='<input type="hidden" value="'+dis+'" id="ud_dis">';
	                      ht+='</td>';
	                      ht+='<td>'+array.name_suc+'</td>';
	                      ht+='<td>'+fe+'</td>';
	                      ht+='<td>'+dis+'</td>';
	                      ht+='<td>'+array.cp_calle+'</td>';
	                      ht+='<td>'+array.cp_n_ex+'</td>';
	                      ht+='<td>'+array.cp_n_int+'</td>';
	                      ht+='<td>'+array.cp_colonia+'</td>';
	                      ht+='<td>'+array.cp_localidad+'</td>';
	                      ht+='<td>'+array.cp_ref+'</td>';
	                      ht+='<td>'+array.cp_minicipio+'</td>';
	                      ht+='<td>'+array.cp_estado+'</td>';
	                      ht+='<td>'+array.cp_cl_estado+'</td>';
	                      ht+='<td>'+array.cp_pais+'</td>';
	                      ht+='<td>'+array.cp_cp+'</td>';
	                      ht+='<td>'+btn+'</td>';
                    ht+='</tr>';
				$('.tb_destinos').append(ht);
            },
            error: function(response){
            	console.log(response);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
}
function delete_tr_des(id,iddes){
	if(id>0){
		$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Eliminar destino',
	        content: '¿Confirma remover el destino seleccionado?',
	        type: 'red',
	        typeAnimated: true,
	        buttons:{
	            confirmar: function (){
	                $.ajax({
			            type:'POST',
			            url: base_url+"Cat_complemento/delete_destino",
			            data: {id:id},
			            success: function (response){
			                toastr.success('Destino eliminado');
			                carga_destinos();
			            },
			            error: function(response){
			            	console.log(response);
			                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
			            }
			        });

	                
	            },
	            cancelar: function () 
	            {
	                
	            }
	        }
	    });
	}else{
		$('.tr_des_'+id+'_'+iddes).remove();
	}
}
function carga_destinos(){
	$.ajax({
        type:'POST',
        url: base_url+"Cat_complemento/carga_destinos",
        data: {
        	id_pcp:$('#id_pcp').val()
    	},
        success: function (response){
             console.log(response); 
             $('.tb_destinos').html('');
             var row_t=1;
            var array = $.parseJSON(response);
            $.each(array, function(index, item) {
                adddestino_tr(item.id,item.id_ubicacion,item.DistanciaRecorrida,row_t,item.fechasalida)
                row_t++;
            });
        },
        error: function(response){
        	console.log(response);
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}
function view_fig(fig){
	var idf = $('#select_fig_'+fig+' option:selected').val();
	var addid='f'+fig+'_';
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Cat_complemento/row_figuras",
            data: {
                id:idf
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                    console.log(array);
                    console.log(array.rfc_del_operador);
                    $('#'+addid+'rfc_del_operador').val(array.rfc_del_operador);
					$('#'+addid+'no_licencia').val(array.no_licencia);
					$('#'+addid+'operador').val(array.operador);
					$('#'+addid+'num_identificacion').val(array.num_identificacion);
					$('#'+addid+'residencia_fiscal').val(array.residencia_fiscal);
					$('#'+addid+'calle').val(array.calle);
					$('#'+addid+'estado').val(array.estado);
					$('#'+addid+'pais').val('MEX');
					$('#'+addid+'codigo_postal').val(array.codigo_postal);

					$('#'+addid+'num_ext').val(array.num_ext);
					$('#'+addid+'num_int').val(array.num_int);

					$('#'+addid+'colo').val(array.colo);
					$('#'+addid+'loca').val(array.loca);
					$('#'+addid+'ref').val(array.ref);
					$('#'+addid+'muni').val(array.muni);
					//$('#TipoFigura').val(array.TipoFigura);
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
}
function addfigura(fig){
	var idf = $('#select_fig_'+fig+' option:selected').val();
	if(idf>0){
		$.ajax({
            type:'POST',
            url: base_url+"index.php/Cat_complemento/row_figuras",
            data: {
                id:idf
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                    console.log(array);
                    
					addfig(0,'',idf,fig,array.rfc_del_operador,array.no_licencia,array.operador,array.num_identificacion,array.residencia_fiscal,array.calle,array.estado,array.codigo_postal,array.num_ext,array.num_int,array.colo,array.loca,array.ref,array.muni);
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
	}else{
		toastr.error('Favor de seleccionar la figura');
	}
}
function addfig(idreg,row,idf,fig,rfc_del_operador,no_licencia,operador,num_identificacion,residencia_fiscal,calle,estado,codigo_postal,num_ext,num_int,colo,loca,ref,muni){
	var html='';
		
		var btn='<button type="button" class="btn btn-danger btn-sm" onclick="delete_tr_fig('+idreg+','+row+')"><i class="fas fa-trash"></i></button>';

		html+='<tr class="tr_fig_'+idreg+'_'+row+'">';
          	html+='<td>'+row;
          		html+='<input type="hidden" id="idreg" value="'+idreg+'">';
          		html+='<input type="hidden" id="id_fig" value="'+idf+'">';
          		html+='</td>';
          	html+='<td>'+operador+'</td>';
          	html+='<td>'+rfc_del_operador+'</td>';
          	html+='<td>'+residencia_fiscal+'</td>';
          	if(fig=='01'){
          	html+='<td>'+no_licencia+'</td>';
          	}
          	html+='<td>'+calle+'</td>';
          	html+='<td>'+num_ext+'</td>';
          	html+='<td>'+num_int+'</td>';
          	html+='<td>'+colo+'</td>';
          	html+='<td>'+codigo_postal+'</td>';
          	html+='<td>'+loca+'</td>';
          	html+='<td>'+ref+'</td>';
          	html+='<td>'+muni+'</td>';
          	html+='<td>'+estado+'</td>';
          	html+='<td>MEX</td>';
          	html+='<td>'+btn+'</td>';
        html+='</tr>';
	$('.tbody_fig_'+fig).append(html);
}
function delete_tr_fig(id,row){
	if(id>0){
		$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Eliminar destino',
	        content: '¿Confirma remover la figura seleccionada?',
	        type: 'red',
	        typeAnimated: true,
	        buttons:{
	            confirmar: function (){
	                $.ajax({
			            type:'POST',
			            url: base_url+"Cat_complemento/delete_fig",
			            data: {id:id},
			            success: function (response){
			                toastr.success('Destino eliminado');
			                $('.tr_fig_'+id+'_'+row).remove();

			            },
			            error: function(response){
			            	console.log(response);
			                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
			            }
			        });

	                
	            },
	            cancelar: function () 
	            {
	                
	            }
	        }
	    });
	}else{
		$('.tr_fig_'+id+'_'+row).remove();
	}
}
function consul_fig_add(fig){
	$('.tbody_fig_'+fig).html('');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Cat_complemento/consult_fig_add",
        data: {
            id_pcp:id_pcp,
            fig:fig
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
                console.log(array);
                var tr_row_col=1;
                $.each(array, function(index, item) {

                	addfig(item.id,tr_row_col,id_pcp,fig,item.rfc_del_operador,item.no_licencia,item.operador,item.num_identificacion,item.residencia_fiscal,item.calle,item.estado,item.codigo_postal,item.num_ext,item.num_int,item.colo,item.loca,item.ref,item.muni);

                	tr_row_col++;
                });
				
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}