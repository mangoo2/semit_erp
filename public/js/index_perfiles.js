var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
});

function table(){
	tabla=$("#table_datos").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Perfiles/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"perfilId"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                //html+='<a href="'+base_url+'Perfiles/registro/'+row.perfilId+'" class="btn btn-cli-edit" style="width: 50px;"></a>';
                //html+='<a onclick="eliminar_registro('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}