var base_url = $('#base_url').val();
var idg=0;
$(document).ready(function() {
    $("#savesm").on("click",function(){
        if($("#file").val()!=""){
            save_solicita();
        }else{
            swal("Atención!", "Ingrese evidencia del equipo", "error");
            return false;
        }
    });
    selectcliente();
    setTimeout(function(){ 
        if($("#id").val()=="0"){
            $('#id_cliente').select2('open');
            $('.select2-search__field').focus()
        };
        $('.select2-selection__placeholder').addClass('colorsel');
        $('.select2-selection__placeholder').attr('style', 'color: white !important');
    }, 1000);

    $("#savecli").on("click",function(){
        savecliente();
    });

    $("#file").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Mantenimientos/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {   
                        input_name:"file",
                        id_mtto:idg
                    };
            return info;
        } 
    }).on('filebatchuploadcomplete', function(event, files, extra) {
        //getEvidencia();
        $("#file").fileinput("clear");
    }).on('filebatchuploadsuccess', function(event, files, extra) {
        //getEvidencia();
        $("#file").fileinput("clear");
    });

    $(".kv-file-upload").hide();
    if($("#id").val()!="0"){
        getEvidencia();
    }
});

function selectcliente(){
    $('#id_cliente').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        allowClear: true,
        ajax: {
            url: base_url+'Mantenimientos/searchclientes',
            dataType: "json",
            delay: 300,
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        text: "ID: "+element.id_cliente +" / " +element.razon_social+" / "+element.nombre,
                        desactivar: element.desactivar,
                        motivo: element.motivo,
                        dias_saldo: element.dias_saldo,
                        saldopago: element.saldopago,
                        diaspago: element.diaspago,
                        idcliente:element.idcliente,
                        correo:element.correo
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#idrazonsocial option:selected").attr('data-id_cli',data.id);
        $("#idrazonsocial option:selected").attr('data-id_clidcli',data.idcliente);
        $("#idrazonsocial option:selected").attr('data-mailc',data.correo);
    });
}

function save_solicita(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_cliente:{
              required: true,
            },
            cantidad:{
              required: true,
            },
            marca:{
              required: true,
            },
            modelo:{
              required: true,
            },
            serie:{
              required: true,
            },
            a_cuenta:{
              required: true,
              min: 500
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Mantenimientos/save_solicitud',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $('#savesm').attr('disabled',true);
            },
            success:function(response){
                idg=response;
                setTimeout(function(){ 
                    saveFile(response);
                }, 500);
                
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    if($("#id_ord").val()==0){
                        window.location = base_url+'Mantenimientos/lista/'+response;
                    }else{
                        window.location = base_url+'Mantenimientos';
                    }
                }, 1500);
            }
        });  
    } 
}

function addCliente(argument) {
    $("#modal_cli").modal("show");
    setTimeout(function(){ 
        $('#id_cliente').select2('close');
    }, 1000);
}

function cambiaCP2(){
    var cp = $("#codigo_postal").val();  
    $("#colonia").html("");
    if(cp!=""){
        $.ajax({
            url: base_url+'Ventasp/getDatosCPSelect',
            dataType: "json",
            data: { cp: cp, col: "0" },
            success: function(data){ 
                edo = data[0].id_estado;
                //console.log("estado: "+edo);
                $("#estado").val(edo); 
                data.forEach(function(element){
                    $('#colonia').append("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                });
            }
        });
    }
}

function savecliente(){
    var form_register = $('#form_registro_cli');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
            calle:{
              required: true,
            },
            cp:{
              required: true,
            },
            colonia:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_cli").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Mantenimientos/save_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $('#savecli').attr('disabled',true);
            },
            success:function(data){
                var idc=data;
                swal("Éxito!", "Guardado Correctamente", "success");
                $('#savecli').attr('disabled',false);
                var data = {
                    id: idc,
                    text: $('#nombre').val()
                };
        
                var newOption = new Option(data.text, data.id, false, false);
                $('#id_cliente').append(newOption).trigger('change');
                $('#form_registro_cli').trigger("reset");
                var formID = $('#form_registro_cli');
                formID.validate().resetForm(); 
                $("#modal_cli").modal("hide");
            }
        });  
    } 
}

/* ****************EVIDENCIA************** */
function saveFile(id){
    //console.log("id: "+id);
    if($("#file").val()!=""){
        $('#file').fileinput('upload');
    }
}

function getEvidencia(){
    $("#cont_imgs").html("");
    $.ajax({
        type:'POST',
        url: base_url+'Mantenimientos/getEvidencias',
        data: { id_mtto:$("#id").val() },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}