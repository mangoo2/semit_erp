var base_url = $('#base_url').val();
var validar_input=0;
var idcompra_aux = $('#idcompra_aux').val();
var idcompra_auxv = $('#idcompra_auxv').val();
$(document).ready(function() {
    setTimeout(function(){ 
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        validar_input=1;    
    }, 1000);
    if(idcompra_aux!=0){
        $.ajax({
            type:'POST',
            url: base_url+"Comprasp/view_presv",
            data:{
                idcompra:idcompra_aux
            },
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log(data);
                limpiartabla();
                var array = $.parseJSON(data);
                $('#viewcompra').val(array.compra.id);
                //$('#idproveedor').html('<option value="'+array.compra.idproveedor+'">'+array.compra.nombre+'</option>');
                var data = {
                    id: array.compra.idproveedor,
                    text: array.compra.nombre
                };
                 
                var newOption = new Option(data.text, data.id, false, false);
                $('#idproveedor').append(newOption).trigger('change');
                $('#contacto').val(array.compra.contacto); 
                $('#fecha_entrega').val(array.compra.fecha); 
                $('#observaciones').val(array.compra.observaciones);
                $('.txt_codigo').html(array.compra.codigo); 
                $.each(array.compradll, function(index, item) {
                    get_producto_detalle(item.id,item.tipo);
                });

            }
        }); 
    }else{
        if(idcompra_auxv!=0){
            $.ajax({
                type:'POST',
                url: base_url+"Comprasp/view_presv",
                data:{
                    idcompra:idcompra_auxv
                },
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    limpiartabla();
                    var array = $.parseJSON(data);
                    var data = {
                        id: array.compra.idproveedor,
                        text: array.compra.nombre
                    };
                    var newOption = new Option(data.text, data.id, false, false);
                    $('#idproveedor').append(newOption).trigger('change');
                    $('#contacto').val(array.compra.contacto); 
                    $('#fecha_entrega').val(array.compra.fecha); 
                    $('#observaciones').val(array.compra.observaciones);
                    $('.txt_codigo').html(array.compra.codigo); 
                    $.each(array.compradll, function(index, item) {
                        get_producto_detalle(item.id,item.tipo);
                    });

                }
            }); 
        }else{
            setTimeout(function(){ 
                $('#idproveedor').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
            //console.log("idcompra_auxv==0");
            viewproductos();
        }
    }
  
    $('#idproveedor').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar proveedor',
        allowClear: true,
        ajax: {
            url: base_url+'Comprasp/searchproveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var item = [];
                //console.log(data);
                data.forEach(function(element) {
                    item.push({
                        id: element.id,
                        text: element.nombre,
                        codigo: element.codigo,
                        contacto: element.contacto
                    });
                });
                return {
                    results: item
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $('.txt_codigo').html(data.codigo);
        $('#contacto').val(data.contacto);
        if(validar_input==0){
            $('#input_barcode').show('show');
            $('#input_search').hide('show');
            $('.select_option_search .select2-container').hide('show');
            $('.icono1').css('color','red');
            $('.icono2').css('color','white');
            setTimeout(function(){ 
                $('#input_barcode').focus();
            }, 1000);
        }else{
            $('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
        }
    });
    
    $('#search_input_barcode').click(function(event) {
        $('#input_barcode').show('show');
        $('#input_search').hide('show');
        $('.select_option_search .select2-container').hide('show');
        $('.icono1').css('color','red');
        $('.icono2').css('color','white');
        validar_input=0;

        setTimeout(function(){ 
            $('#input_barcode').focus();
        }, 1000);
    });
    $('#search_input_search').click(function(event) {
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        validar_input=1;
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    });
    
    document.addEventListener("keydown", function(event) {
        //console.log(event.ctrlKey);
        //console.log(event.code);
        if (event.altKey && event.code === "Digit1") {
            limpiartabla();
        }
        if (event.altKey && event.code === "Numpad1") {
            limpiartabla();
        }
        if (event.altKey && event.code === "Digit2") {
            guardarcompras(0);
        }
        if (event.altKey && event.code === "Numpad2") {
            guardarcompras(0);
        }
        if (event.altKey && event.code === "Digit3") {
            //modal_user();
        }
        if (event.altKey && event.code === "Numpad3") {
            //modal_user();
        }
        if (event.altKey && event.code === "Digit4") {
            //limpiartabla();
        }
        if (event.altKey && event.code === "Numpad4") {
            //limpiartabla();
        }
        if (event.altKey && event.code === "Digit5") {
            guardarcompras(1);
        }
        if (event.altKey && event.code === "Numpad5") {
            guardarcompras(1);
        }
        if (event.altKey && event.code === "Digit6") {
            //buscar_cliente();
        }
        if (event.altKey && event.code === "Numpad6") {
            //buscar_cliente();
        }
    });
    $("#input_barcode").keypress(function(e) {
        if(e.which == 13) {
          obtenerdatosproducto();
        }
    });
    input_search();
    $("#recargas").on("change",function(){
        tipoCompra();
    });

    $("#id_tanque").select2({

    }).on('select2:select', function (e) {
        var data = e.params.data;
        addproducto(data.id,1);
    });

    /*$(".btn_ajustes").on("click",function(){
        console.log("click save prod");
    });*/
 
    /* ***************************************************/
    $("#btn_prodbe").on("click",function(){
        $('#idproveedor').select2('close');
        //$('.select2-search__field').focus();
        loadModal_exist();
    });

    $('#idproveedor_modal').select2({
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        dropdownParent: $("#modal_prods"),
        placeholder: 'Buscar proveedor',
        allowClear: true,
        ajax: {
            url: base_url+'Comprasp/searchproveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var item = [];
                data.forEach(function(element) {
                    item.push({
                        id: element.id,
                        text: element.nombre,
                        codigo: element.codigo,
                        contacto: element.contacto
                    });
                });
                return {
                    results: item
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("id proveedor: "+data.id);
        $('.txt_codigo').html(data.codigo);
        $('#contacto').val(data.contacto);
        loadProds_exist(data.id);
    });

    $("#add_prodcomp").on("click",function(){
        var cont_prod=0;
        var TABLA   = $("#table_prods_exist tbody > tr");
        TABLA.each(function(){
            if($(this).find("input[id*='add_prod']").is(":checked")==true){
                cont_prod++;
            }     
        });
        if(cont_prod>0){
            exportProdsComp();
        }else{
            swal("Álerta!", "Elige al menos un producto", "warning");
        }
    });

    $("#all_prov").on("click",function(){
        if($("#idproveedor_modal option:selected").val()!=undefined){
            loadProds_exist($("#idproveedor_modal option:selected").val());
            $("#all_prods").prop("checked",false);
        }else{
            setTimeout(function(){ 
                $('#idproveedor_modal').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
        }
    });

    $("#all_prods").on("click",function(){
        if($("#idproveedor_modal option:selected").val()!=undefined && $(".swi_prods").is(":checked")==false)
            $(".swi_prods").attr("checked",true);
        else
            $(".swi_prods").attr("checked",false);
    });

    /*window.onload = function () {
        var options = new GridViewScrollOptions();
        options.elementID = "table_prods_exist";
        options.width = 850;
        options.height = 350;
        options.freezeColumn = true;
        options.freezeFooter = true;
        options.freezeColumnCssClass = "GridViewScrollItemFreeze";
        options.freezeFooterCssClass = "GridViewScrollFooterFreeze";
        options.freezeHeaderRowCount = 2;
        options.freezeColumnCount = 3;

        gridViewScroll = new GridViewScroll(options);
    }*/
    //gridviewScroll();
    /* ***************************************************/
});

/*function gridviewScroll() {
    gridView1 = $('#table_prods_exist').gridviewScroll({                               
        width: "100%",
        height: 700,
        freezesize: 0,
         headerrowcount: 1
    });
}*/

function loadModal_exist(){
    $("#modal_prods").modal("show");
    setTimeout(function(){ 
        $('#idproveedor_modal').select2('open');
        $('.select2-search__field').focus();
    }, 1000);
}

function loadProds_exist(id){
    var all = ($("#all_prov").is(":checked")==true) ? "1" : "0";
    //tablec = $("#table_prods_exist").dataTable({ destroy:true });
    $.ajax({
        type:'POST',
        url: base_url+"Comprasp/get_productos_existencia",
        data: { id:id, all: all },
        success: function (response){
            //console.log(response);
            $(".prodsexist_tbody").html(response);
        },
        error: function(response){
              
        }
    });
}

function exportProdsComp(){
    //console.log("exportProdsComp");
    new Promise(function(resolve) {
        resolve(addProdExist());
    }).then(function(result) {
        hideModData();
    });   
}

function addProdExist(){
    //console.log("addProdExist");
    var TABLA   = $("#table_prods_exist tbody > tr");
    TABLA.each(function(){
        if($(this).find("input[id*='add_prod']").is(":checked")==true){
            var stock=Number($(this).find("input[id*='tot_stock']").val());
            var mins=Number($(this).find("input[id*='tot_min']").val());
            //console.log("stock: "+stock);
            //console.log("mins: "+mins);
            var prom=+mins-stock;
            //console.log("prom: "+prom);
            if($(this).find("input[id*='tipo']").val()==3){ //para recargas, si es que se modifica form de recargas para asginar provee
                tipo=1;
            }else{
                tipo=0;
            }
            //if(mins>0 && prom>0){
                addproducto($(this).find("input[id*='idprod']").val(),tipo,prom);
                /*swal("Éxito!", "Producto(s) agregado(s) a compra", "success");
            }else{
                swal("Álerta!", "No existen productos para compra", "warning");
            }*/
        }     
    });
}

function hideModData(){
    //console.log("hideModData");
    $(".swi_prods").attr("checked",false);
    $(".tr_prods").remove();
    
    var newOption = new Option($("#idproveedor_modal option:selected").text(),$("#idproveedor_modal option:selected").val(), false, false);
    $('#idproveedor').append(newOption).trigger('change');

    $('#idproveedor_modal').val(null).trigger('change');
    $("#modal_prods").modal("hide"); 
}

function tipoCompra(){
    if($("#recargas").is(":checked")==true){
        $("#cont_search").hide("slow");
        $("#cont_rechange").show("slow");
        $("#cont_det").show("slow");
    }else{
        $("#cont_rechange").hide("slow");
        $("#cont_det").hide("slow");
        $("#cont_search").show("slow");
    }
}

function obtenerdatosproducto(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Comprasp/obtenerdatosproducto",
        data: {
            barcode:$('#input_barcode').val()
        },
        success: function (response){
            //console.log(response);
            var array=$.parseJSON(response);
            var costo_compra=parseFloat(array.costo_compra);
            if(costo_compra==0){
                swal("¡Atención!", "Este producto no cuenta con un costo de compra", "error");
                $('#input_barcode').val('').focus();
                //console.log("id: "+array.id);
            }else{
                var idproducto= parseInt(array.id);
                if(idproducto>0){
                     addproducto(idproducto);
                     $('#input_barcode').val('').focus();
                }else{
                    toastr["error"]("No existe el producto");
                }
            }
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}

function input_search(){
    $('#input_search').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Comprasp/searchproductos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre,
                        costo_compra: element.costo_compra,
                        isr : element.isr
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        var costo = parseFloat(data.costo_compra); 

        if(costo==0){
            swal("¡Atención!", "Este producto no cuenta con un costo de compra", "error");
            $('#input_search').val(null).trigger('change');
        }else{
            addproducto(data.id);
            $('#input_search').val(null).trigger('change');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 2000);
        }
    });
}

function addproducto(id,tipo=0,cantidad=0){
    $.ajax({
        type:'POST',
        url: base_url+"Comprasp/addproducto",
        data: {
            id:id,tipo:tipo,cantidad:cantidad
        },
        success: function (response){
            if(response==0){
                swal("¡Atención!", "El producto no cuenta con precios establecidos", "error");
                if(validar_input==0){
                    $('#input_barcode').show('show');
                    $('#input_search').hide('show');
                    $('.select_option_search .select2-container').hide('show');
                    $('.icono1').css('color','red');
                    $('.icono2').css('color','white');
                    setTimeout(function(){ 
                        $('#input_barcode').focus();
                    }, 1000);
                }else{
                    $('#input_barcode').hide('show');
                    $('#input_search').show('show');
                    $('.select_option_search .select2-container').show('show');
                    $('.icono1').css('color','white');
                    $('.icono2').css('color','red');
                    setTimeout(function(){ 
                        $('#input_search').select2('open');
                        $('.select2-search__field').focus();
                    }, 1000);
                }
            }else{
                viewproductos();
                //console.log(response);
                $('.table_datos_tbody').html(response);
            }    
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}

function get_producto_detalle(id,tipo){
    //console.log("get_producto_detalle");
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Comprasp/id_compra_producto",
        data: {
            id:id
        },
        success: function (response){
            viewproductos();
            //console.log(response);
            //$('.table_datos_tbody').html(response);  
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}

function viewproductos(edit=0,id=0,tipo=0){
    //console.log("viewproductos");
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Comprasp/viewproductos",
        data: {
            id:id, edit:edit, tipo:tipo, eoce:$("#eoce").val()
        },
        success: function (response){
            //console.log(response);
            $('.table_datos_tbody').html(response);
            calculartotales();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}

function deletepro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                deleteprorow(id);
            },
            cancelar: function (){
                
            }
        }
    });
}

function editaProd(id,tipo){
    if(tipo==0){
        var ventana = window.open(base_url+"Productos/registro/"+id, "Editar producto", "width=780, height=612");
    }if(tipo==1){
        var ventana = window.open(base_url+"Recargas/alta/"+id, "Editar recarga", "width=780, height=612");
    }
    var tiempo= 0;
    var interval = setInterval(function(){
         //Comprobamos que la ventana no este cerrada
        if(ventana.closed !== false) {
            //Si la ventana ha sido cerrada, limpiamos el contador
            window.clearInterval(interval)
            //alert(`Tiempo total: ${tiempo} s`);
            //console.log("cierra ventana edit");
            viewproductos(1,id);
        } else {
            //Mientras no se cierra la ventana sumamos los segundos
            tiempo +=1;
        }
    },1000)
    //$('#iframeri').modal('show');
    //$('.iframereporte').html('<iframe src="'+base_url+'Productos/registro/'+id+'"></iframe>');
}

function deleteprorow(id){
    //console.log('deleteprorow:'+id);
    $.ajax({
        type:'POST',
        url: 'Comprasp/deleteproducto',
        data: {
            id: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                //$('.producto_'+id).remove();
                viewproductos(0,id);
                calculartotales();
                if(validar_input==0){
                    $('#input_barcode').show('show');
                    $('#input_search').hide('show');
                    $('.select_option_search .select2-container').hide('show');
                    $('.icono1').css('color','red');
                    $('.icono2').css('color','white');
                    setTimeout(function(){ 
                        $('#input_barcode').focus();
                    }, 1000);
                }else{
                    $('#input_barcode').hide('show');
                    $('#input_search').show('show');
                    $('.select_option_search .select2-container').show('show');
                    $('.icono1').css('color','white');
                    $('.icono2').css('color','red');
                    setTimeout(function(){ 
                        $('#input_search').select2('open');
                        $('.select2-search__field').focus();
                    }, 1000);
                }
            }
        });
}

function editarcantidad(row){
    var cantidad=$('.vscanti_'+row).val();
    var tipo=$('.vstipo_'+row).val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Comprasp/editarcantidad",
        data: {
            row:row,
            cantidad:cantidad
        },
        success: function (response){
            viewproductos(0,0,tipo);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
    if(cantidad>0){
        $('.vscanti_'+row).removeClass('cant_invalido');
    }else{
        $('.vscanti_'+row).addClass('cant_invalido');
    }
}

function editarcosto(row){
    var costo=$('.vscosto_'+row).val();
    var id=$('.vsproid_'+row).val();
    var tipo=$('.vstipo_'+row).val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Comprasp/editarcosto",
        data: {
            row:row,
            costo:costo,
            id:id,
            tipo:tipo
        },
        success: function (response){
            viewproductos(1,id,tipo);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
    if(costo>0){
        $('.vscosto_'+row).removeClass('cant_invalido');
    }else{
        $('.vscosto_'+row).addClass('cant_invalido');
    }
}

function calculartotales(){
    var subtotal=0;
    var descuentos=0;
    var impuest=0;
    var impuestos=0;
    var impuestosp=0;
    var impuestosp_isr=0;
    var litros=0;
    var TABLA   = $("#table_datos tbody > tr");
        TABLA.each(function(){         
            item = {};
            if($(this).find("input[id*='vstipo']").val()==0){ //producto
                subtotal+=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vscosto']").val());
            }else{
                subtotal+=parseFloat($(this).find("input[id*='vstotal']").val());
            }
            //descuentos+=parseFloat($(this).find("input[id*='vsdescuento']").val());
            descuentos=0;
            count_row=parseFloat($(this).find("input[id*='count']").val());

            incluye_iva=parseFloat($(this).find("input[id*='incluye_iva']").val());
            subtotaln=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vscosto']").val());
            //descuentosn=parseFloat($(this).find("input[id*='vsdescuento']").val());
            descuentosn=0;
            if(incluye_iva==1){
               impuestosp+=(parseFloat(subtotaln)-parseFloat(descuentosn))*0.16;
            }
            isr=Number($(this).find("input[id*='porc_isr']").val());
            //console.log("isr: "+isr);
            if(isr>0){
               impuestosp_isr+=(parseFloat(subtotaln)-parseFloat(descuentosn))*(isr/100);
            }
            //console.log(impuestosp);
            if($(this).find("input[id*='vstipo']").val()=="1"){
                litros=parseFloat($(this).find("input[id*='vscapac']").val())*parseFloat($(this).find("input[id*='vscanti']").val());
            }
        });
    $('.class_litros').html(litros);

    $('#subtotal').val(subtotal);
    if(isNaN(subtotal)){
        $('.class_subtotal').html('$ 0.00');
    }else{
        $('.class_subtotal').html('$ '+subtotal.toFixed(2));
    }
    $('#descuentos').val(descuentos);
    $('.class_descuentos').html('$ '+descuentos);
    //var impuestos=(parseFloat(subtotal)-parseFloat(descuentos))*0.16;

    impuestos=redondear(impuestosp,2);
        //console.log('impuestos:'+impuestos);
    $('#impuestos').val(impuestos);

    impuestos_isr=redondear(impuestosp_isr,2);
    $('#impuesto_isr').val(impuestos_isr);
    $('.class_imp_isr').html('$ '+impuestos_isr);

    if(isNaN(impuestos)){
        $('.class_impuestos').html('$ 0.00');
    }else{
        $('.class_impuestos').html('$ '+impuestos);
    }
    //console.log('subtotal:'+subtotal);
    //console.log('descuentos:'+descuentos);
    //console.log('impuestos:'+impuestos);
    var total=parseFloat(subtotal)-parseFloat(descuentos)+parseFloat(impuestos)-parseFloat(impuestos_isr);
    //console.log('total:'+total);
        total=redondear(total,2);
    $('#total').val(total);
    
    if(isNaN(total)){
        $('.class_total').html('$ 0.00');
    }else{
        $('.class_total').html('$ '+total);
    }
}

function guardarcompras(tipo){
    var viewcompra = $('#viewcompra').val();
    var productos = $("#table_datos tbody > tr");
    //==============================================
    var DATAa  = [];
    productos.each(function(){         
        item = {};                    
        item ["cant"]   = $(this).find("input[id*='vscanti']").val();
        item ["tipo"]   = $(this).find("input[id*='vstipo']").val();
        item ["pro"]   = $(this).find("input[id*='vsproid']").val();
        item ["cunit"]   = $(this).find("input[id*='vscosto']").val();
        item ["desc"]   = $(this).find("input[id*='vsdescuento']").val();
        DATAa.push(item);
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    //========================================
    var idproveedor =$('#idproveedor option:selected').val();
    var subtotal = $('#subtotal').val();
    var descuento = $('#descuentos').val();
    var iva = $('#impuestos').val();
    var isr = $('#impuesto_isr').val();
    var total = $('#total').val();
    var fecha_entrega = $('#fecha_entrega').val();
    var observaciones = $('#observaciones').val();
    var datos='viewcompra='+viewcompra+'&precompra='+tipo+'&idproveedor='+idproveedor+'&subtotal='+subtotal+'&descuento='+descuento+'&iva='+iva+'&isr='+isr+'&total='+total+'&productos='+aInfoa+'&fecha='+fecha_entrega+'&observaciones='+observaciones;
    if($('.cant_invalido').length==0){
        if(productos.length>0){
            if(tipo==0){
                var proveedor_id=$('#idproveedor option:selected').val();
                if(proveedor_id==undefined){
                    swal("¡Atención!", "Favor de elegir un proveedor", "error");
                }else{
                    $('body').loading({theme: 'dark',message: 'Generando compra...'});
                    $.ajax({
                        type:'POST',
                        url: base_url+"Comprasp/savecompra",
                        data: datos,
                        async:false,
                        success: function (response){
                            $('body').loading('stop')
                            if($("#eoce").val()=="0"){
                                var text_swal="Compra realizada correctamente"; 
                            }else{
                                var text_swal="Compra editada correctamente";
                            }
                            swal("Éxito!",text_swal,'success', {
                              buttons: false,
                              timer: 3000,
                            });
                            //console.log("tipo: "+tipo);
                            if($("#eoce").val()=="0"){
                                setTimeout(function(){ 
                                    window.open(base_url+'Comprasp/imprimir_compra/'+response,'_blank');
                                }, 2000);  
                            }else{
                                setTimeout(function(){ 
                                    window.close();
                                }, 2000);
                            }
                             
                            setTimeout(function(){ 
                                limpiarcompra(); 
                                if(tipo==1){
                                    window.location.href = base_url+"Comprasp";
                                }else{
                                    window.location.href = base_url+"OCS";
                                }
                            }, 2500);  
                        },
                        error: function(response){
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                        }
                    });
                }
            }else{
                $('body').loading({theme: 'dark',message: 'Generando compra...'});
                $.ajax({
                    type:'POST',
                    url: base_url+"Comprasp/savecompra",
                    data: datos,
                    async:false,
                    success: function (response){
                        $('body').loading('stop')
                        swal("Éxito!",'Compra realizada','success', {
                          buttons: false,
                          timer: 3000,
                        });
                        window.open(base_url+'Comprasp/imprimir_compra/'+response,'_blank'); 
                        setTimeout(function(){ 
                            //console.log("tipo de else: "+tipo);
                            limpiarcompra(); 
                            if(tipo==1){
                                window.location.href = base_url+"Comprasp";
                            }else{
                                window.location.href = base_url+"OCS";
                            }
                        }, 1500);  
                    },
                    error: function(response){
                        toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                        
                    }
                });
            }
        }else{
            //toastr.error('Agregar por lo menos un producto');
        }
    }else{
        swal("¡Atención!", "El campo de cantidad y/o costo tiene que ser mayor a 0", "error");
    }
}

function view_presv(){
    var precompras = $('#precompras option:selected').val();
    if(precompras>0){
        $.ajax({
            type:'POST',
            url: base_url+"Comprasp/view_presv",
            data:{
                idcompra:precompras
            },
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log(data);
                limpiartabla();
                var array = $.parseJSON(data);
                $('#viewcompra').val(array.compra.id);
                //$('#idproveedor').html('<option value="'+array.compra.idproveedor+'">'+array.compra.nombre+'</option>');
                var data = {
                    id: array.compra.idproveedor,
                    text: array.compra.nombre
                };
                 
                var newOption = new Option(data.text, data.id, false, false);
                $('#idproveedor').append(newOption);
                $('#idproveedor').val(data.id).trigger('change');
                $('#contacto').val(array.compra.contacto);
                $('#fecha_entrega').val(array.compra.fecha); 
                $('#observaciones').val(array.compra.observaciones);
                $('.txt_codigo').html(array.compra.codigo); 
                $.each(array.compradll, function(index, item) {
                    addproducto(item.idproducto,item.tipo,item.cantidad);
                });

            }
        }); 
    }
}

function limpiarcompra(){
    limpiartabla();
    $('#idproveedor').val(null).trigger('change');
    $('.class_subtotal').html('$0.00');
    $('.class_descuentos').html('$0.00');
    $('.class_impuestos').html('$0.00');
    $('.class_total').html('$0.00');
    $('.txt_codigo').html('');
    $('#contacto').val('');
}

function limpiartabla(){
    $.ajax({
        type:'POST',
        url: 'Comprasp/limpiartabla',
        data: {
            id: 0
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            viewproductos();
        }
    });
}

function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}

function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}

