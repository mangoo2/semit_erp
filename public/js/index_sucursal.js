var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"Sucursales/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            //{"data":"id"},
            {"data":"id_alias"},
            {"data":"clave"},
            {"data":"name_suc"},
            {"data":"tel"},
            {"data":"domicilio"},
            {"data":"orden"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Sucursales/registro/'+row.id+'" class="btn"><i class="fa fa-edit"></i></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 5, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}