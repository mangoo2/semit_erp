var base_url = $('#base_url').val();
$(document).ready(function() {
    $(".btn_save").on("click",function(){
        if($("#tipo_rec").val()=="1" || $(".tipo option:selected").val()==1){
            guardar_registro();
        }else{
            guardar_registro2();
        }
    });

    $("#preciov,#precioc").on("change",function(){
        if($("#preciov").val()>0 && $("#precioc").val()>0){
            calcularUtilidad();
        }
    });

    if($("#id").val()>0){
        calcularUtilidad();
    }

    if($("#tipo").val()=="0"){
        calculaStock();
    }
    $("#tipo").on("change",function(){
        verficaTP();
    });
});

function verficaTP(){
    if($("#band_tp").val()==1){
        swal("¡Atención!", "Esta sucursal ya cuenta con un tanque principal de llenado", "error");
        $("#tipo").val("1");
    }
}

function calculaStock(){ 
    $("#cont_det").show("slow");
    var capacidad=parseFloat($("#capacidad").val()).toFixed(2);
    var stock=parseFloat($("#stock").val()).toFixed(2);
    //console.log("capacidad: "+capacidad);

    var entanque=stock/capacidad;
    $("#stock_tanques").val(parseFloat(entanque).toFixed(2));
}

function calcularUtilidad(){ 
    if($("#tipo option:selected").val()=="1"){
        var preciov=parseFloat($("#preciov").val()).toFixed(2);
        var precioc=parseFloat($("#precioc").val()).toFixed(2);
        var utilidad=preciov-precioc;
        $("#utilidad").val(parseFloat(utilidad).toFixed(2));
    }
}

function guardar_registro(){
    var form_register = $('#form_recar');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            codigo:{
              required: true,
            },
            capacidad:{
              required: true,
            },
            preciov:{
              required: true,
            },
            precioc:{
              required: true,
            },
        },        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_recar").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_save').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Recargas/registro_datos',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                var idp=parseFloat(array.data);
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_save').attr('disabled',false);
                    window.location = base_url+'Recargas';
                }, 1500);
            }
        });
    }   
}

function guardar_registro2(){
    var DATA  = [];
    var TABLA   = $("#table_sucs tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id']").val();
        item ["sucursal"] = $(this).find("input[id*='sucursal']").val();
        item ["codigo"] = $(this).find("input[id*='codigo']").val();
        item ["capacidad"] = $(this).find("input[id*='capacidad']").val();
        item ["precioc"] = $(this).find("input[id*='precioc']").val();
        item ["tipo"] = $(this).find("select[id*='tipo'] option:selected").val();
        if($(this).find("input[id*='codigo']").val()!=""){
            DATA.push(item);
        }
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    $('.btn_save').attr('disabled',true);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Recargas/registro_datos2',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                //toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                //toastr.error('Error', '500');
            }
        },
        success: function(data){
            swal("Éxito!", "Guardado Correctamente", "success");
            setTimeout(function(){ 
                $('.btn_save').attr('disabled',false);
                window.location = base_url+'Recargas';
            }, 1500);
        }
    }); 
}