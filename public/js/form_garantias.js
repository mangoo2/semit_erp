var base_url = $('#base_url').val();
var prod=0; var cant=0; var idg=0;
//var array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Renta","Almacén General"];
$(document).ready(function() {
    search_producto();
    $("#cantidad").on("change",function(){
        if($("#idproducto option:selected").val()!=undefined && $("#idproducto option:selected").val()!=""){
            get_producto($("#sucursal").val(),$("#cantidad").val());
        }
    });
    $("#savesg").on("click",function(){
        if($("#motivo").val().trim()!=""){
            if($("#file").val()==""){
                swal("¡Atención!","Ingresa evidencia de la solicitud", "error");
                return false;
            }
            save_solicita();
            /*new Promise(function(resolve) {
                resolve(save_solicita());
            }).then(function(result) {
                saveFile($("#id").val());
            });*/
        }else{
            swal("¡Atención!","Ingresa un motivo de solicitud", "error");
            return false;
        }
    });
    //getSucs($("#sucursal").val());

    $("#file").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Garantias/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {   
                        input_name:"file",
                        id_garantia:idg
                    };
            return info;
        } 
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
    });

    if($("#id").val()!="0"){
        getEvidencia();
    }
});

function search_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Garantias/searchproductos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    id_suc: $("#id_sucursal option:selected").val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    if(element.tipo==0){
                        nombre=element.nombre+" ("+(element.stock_suc-element.traslado_stock_cant)+")";
                    }else{
                        nombre=element.nombre;
                    }
                    if(element.status_trasr==2 && element.id_traslado_serie!=0 && element.tipo==1 || element.status_traslt==2 && element.id_traslado_lote>0 && element.tipo==2 || element.id_traslado_serie==0 && element.tipo==1 || element.id_traslado_lote==0 && element.tipo==2 || element.tipo==0){
                        itemscli.push({
                            id: element.id,
                            text: element.idProducto+' '+nombre+' - '+element.lote+' '+element.serie,
                            tipo:element.tipo,
                            id_prod_suc:element.id_prod_suc,
                            id_prod_suc_serie:element.id_prod_suc_serie,
                            caducidad:element.caducidad,
                            serie8:element.serie8,
                            lote8:element.lote8,
                            stock_suc: element.stock_suc,
                            traslado_stock_cant: element.traslado_stock_cant,
                            lote8: element.lote8,
                            traslado_lote_cant: element.traslado_lote_cant
                        });
                    }
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        prod=data.nombre;
        if(data.tipo==0){ //normal
            cant_stock=data.stock;
            cant_stock = (data.stock_suc-data.traslado_stock_cant);
        }else if(data.tipo==1){ //serie
            cant_stock=data.serie8;
        }else if(data.tipo==2){ //lote
            cant_stock=data.lote8;
            cant_stock=element.lote8-element.traslado_lote_cant;
        }
        //console.log("cant_stock: "+cant_stock);
        var id_psl=0;
        if(data.tipo==1){ //serie
            id_psl=data.id_prod_suc_serie;
        }
        if(data.tipo==2){ //lote
            id_psl=data.id_prod_suc;
        }
        $("#idproducto option:selected").attr('data-stock',cant_stock);
        $("#idproducto option:selected").attr('data-tipo',data.tipo);
        $("#input_search option:selected").attr('data-caducidad',data.caducidad);
        $("#idproducto option:selected").attr('data-id_prod_suc',data.id_prod_suc);
        $("#idproducto option:selected").attr('data-id_prod_suc_serie',data.id_prod_suc_serie);
        //get_producto($("#sucursal").val(),cant_stock);
        get_producto($("#id_sucursal option:selected").val(),cant_stock);
    });
}

/*var name_suc=""; 
function getSucs(id){
    array_suc="";
    $.ajax({
        type:'POST',
        url: base_url+"Sistema/getNamesSucursales",
        data: { id:id },
        success:function(data){
            //console.log("data: "+data);
            var arr = $.parseJSON(data);
            arr.array.forEach(function(i){
                //console.log("id de array recorrido: "+i.id);
                name_suc=i.name_suc;
            });
            console.log("name_suc: "+name_suc);
        }
    });
    //return array_suc;
    //console.log("array_suc: "+array_suc);
}*/

function get_producto(id_suc,cant){
    var name_suc=$("#id_sucursal option:selected").text();
    var id=$("#idproducto option:selected").val();
    var prod=$("#idproducto option:selected").text();
    var nombre = prod;
    var cantidad = Number($("#cantidad").val());
    var stock = Number(cant);
    //var stock = Number($("#idproducto option:selected").data("stock"));
    var tipo = Number($("#idproducto option:selected").data("tipo"));
    var id_prod_suc = Number($("#idproducto option:selected").data("id_prod_suc"));
    var id_prod_suc_serie = Number($("#idproducto option:selected").data("id_prod_suc_serie"));
    var html="";;
    if(cantidad==0){
        swal("¡Atención!","Ingreso una cantidad a solicitar", "error");
        return;
    }
    if(stock<cantidad){
        swal("¡Atención!","No se cuenta con suficiente stock en almacén", "error");
        return;
    }
    if($("#id_sucursal option:selected").val()=="0"){
        swal("¡Atención!","Indique almacén que solicita", "error");
        return;
    }
    
    var repetido=0;
    var TABLA   = $("#table_datos_stock tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='idprod']").val(); 
        var tipoexi = $(this).find("input[id*='tipo']").val(); 
        var loteexi = $(this).find("input[id*='id_ps_lot']").val(); 
        var serieexi = $(this).find("input[id*='id_ps_ser']").val(); 
        if (id == prodexi && tipo == tipoexi && id_prod_suc == loteexi && id_prod_suc_serie == serieexi) {
            repetido=1;
        }   
    });

    if(repetido==0){
        html="<tr class='row_p"+id+"_"+tipo+"_"+id_prod_suc+"_"+id_prod_suc_serie+"'>\
                <td> \
                    <input type='hidden' id='idprod' value='"+id+"'>\
                    <input type='hidden' id='id_ps_lot' value='"+id_prod_suc+"'>\
                    <input type='hidden' id='id_ps_ser' value='"+id_prod_suc_serie+"'>\
                    <input type='hidden' id='tipo' value='"+tipo+"'>\
                    <input type='hidden' id='stock' value='"+cant+"'>\
                "+nombre+"</td>\
                <td><input type='hidden' id='id_origen' value='"+id_suc+"'>"+name_suc+"</td>\
                <td><input type='hidden' id='cant' value='"+cantidad+"'>"+cantidad+"</td>\
                <td><button class='btn btn-danger' onclick='eliminar("+id+","+tipo+","+id_prod_suc+","+id_prod_suc_serie+")'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>\
            </tr>";
        $("#body_prods").append(html);
        $('#idproducto').val(null).trigger('change');
        $("#idproducto").empty().trigger('change');
        $("#cantidad").val("");
    }else{
        swal("¡Atención!","Producto ya existente en tabla de solicitud", "error");
    }
}

function eliminar(id,tipo,id_prodscu,id_serie){
    $(".row_p"+id+"_"+tipo+"_"+id_prodscu+"_"+id_serie).remove();
}

function save_solicita(){
    var DATA  = [];
    var TABLA   = $("#table_datos_stock tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["motivo"] = $("#motivo").val();
        item ["idprod"] = $(this).find("input[id*='idprod']").val();
        item ["id_ps_lot"] = $(this).find("input[id*='id_ps_lot']").val();
        item ["id_ps_ser"] = $(this).find("input[id*='id_ps_ser']").val();
        item ["tipo"] = $(this).find("input[id*='tipo']").val();
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["cant"] = $(this).find("input[id*='cant']").val();
        item ["stock"] = $(this).find("input[id*='stock']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    //INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);

    if($("#table_datos_stock tbody > tr").length>0){
        var datos = 'motivo='+$("#motivo").val()+'&array_prod='+aInfo;
        $.ajax({
            data: datos,
            type: 'POST',
            url : base_url+'Garantias/save_solicitud',
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            beforeSend: function(){
                $('#savesg').attr('disabled',true);
            },
            success: function(response){
                //console.log("response: "+response);
                idg=response;
                //console.log("idg: "+idg);
                $("#id").val(response);
                saveFile(response);     
                envia_mail(idg) 
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Garantias';
                }, 1500);
            }
        }); 
    }else{
        swal("¡Atención!","Ingresa al menos un producto", "error");
    }
}

function saveFile(id){
    //console.log("id: "+id);
    if($("#file").val()!=""){
        $('#file').fileinput('upload');
    }
}

function getEvidencia(){
    $("#cont_imgs").html("");
    $.ajax({
        type:'POST',
        url: base_url+'Garantias/getEvidencias',
        data: { id_venta:0, id:$("#id").val() },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}

function envia_mail(id){
    $.ajax({
        type:'POST',
        url: base_url+"Garantias/mail_solicitudGaran",
        data: { id:id },
        success:function(data){

        }
    });
}