var base_url=$('#base_url').val();
var table;
var chart2;
$(document).ready(function($) {
	//table=$('#table_datos').DataTable();
	loadtable();
	generargrafica(0);
	$('#suc_selected').change(function(event) {
		var sucu =$('#suc_selected option:selected').val();
		consu_vendedores(sucu);
	});

	$('#suc_selected,#eje_selected,#mes_selected').change(function(event) {
		loadtableProds();
	});

	$("#btn_export").on("click",function(){
		exportComisiones();
	});
});

function loadtable(){
	$('body').loading({theme: 'dark',message: 'Procesando...'});
	var suc = $('#suc_selected option:selected').val();
	var eje = $('#eje_selected option:selected').val();
	var mes = $('#mes_selected option:selected').val();
	$.ajax({
            type:'POST',
            url: base_url+"index.php/Comisiones/generarteporte",
            data: {
                suc:suc,
                eje:eje,
                mes:mes
            },
            success: function (response){
            	var array = $.parseJSON(response);

            	$('body').loading('stop');
                $('.table_datos_tb').html(array.html);
                $('#table_datos').DataTable({"lengthMenu": [[30,50], [30,50]], "order": [[ 1, "desc" ]]});

                if(array.html_serv!=""){
	                $('.table_datos_tb_serv').html(array.html_serv);
	                $('#table_datos_servs').DataTable({"lengthMenu": [[30,50], [30,50]] , "order": [[ 1, "desc" ]]});
	            }
	            //console.log("html_top: "+array.html_top);
	            $('.info_top_vent').html(array.html_top); //LIMITAR SOLO A 10
	            $('#table_datos_top').DataTable({"lengthMenu": [[10], [10]] , "order": [[ 1, "desc" ]], columnDefs: [
            		{ orderable: false, targets: 6 } ]
         		});

                var top=$('.table_datos_top').html();
                $('.info_top_vent').html(top);
                $('.table_datos_top').html('');
                generargrafica(1);

            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                $('body').loading('stop');
            }
        });
}
function generargrafica(view){
	var eje = $('#eje_selected option:selected').val();
	var mes = $('#mes_selected option:selected').val();
	//console.log('mes 0 '+mes);
	if (view>0) {
        chart2.destroy();
    }
    if(mes>0){
    	if(eje>0){
    		var DATA_label  = [];
			var DATA_val  = [];
			DATA_label.push(''); 
			DATA_val.push(0); 

			$(".info_mont_eje_all").each(function() {
			    var sub  = $(this).data('monto');
			    var nam  = $(this).data('mes');
			    DATA_label.push(nam);
			    DATA_val.push(sub);
			});

			var options = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar: {
			            show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "Ventas",
			        data: DATA_val
			    }],
			    title: {
			        text: 'Montos de ventas del vendedor en el mes',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos sin IVA)',
			        align: 'left'
			    },
			    labels: DATA_label,
			    yaxis: {
			        opposite: true
			    },
			    xaxis: {
			        labels: {
			            show: true
			        },
			        title: {
			            text: ''
			        },
			        min: 0  // Establecer el límite mínimo en 0 para el eje X
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors: [vihoAdminConfig.primary]
			};

			chart2 = new ApexCharts(
			    document.querySelector("#basic-apex2"),
			    options
			);

			chart2.render();

    	}else{
    		var productos = $("#table_datos_top tbody > tr");
			var DATA_label  = [];
			var DATA_val  = [];
			DATA_label.push(''); 
			DATA_val.push(0); 
                productos.each(function(){                   
                    var sub  = $(this).find("td[class*='info_tr_final']").data('subtotalg');
                    var nam  = $(this).find("td[class*='info_tr_final']").data('nombre');
                    DATA_label.push(nam);
                    DATA_val.push(sub);
                });

    		var options = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar:{
			          show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "",
			        data: DATA_val
			    }],
			    title: {
			        text: 'Montos de todas las ventas',
			        align: 'left'
			    },
			    subtitle: {
			        text: 'Montos sin iva',
			        align: 'left'
			    },
			    labels: DATA_label,
			    
			    yaxis: {
			        opposite: true
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors:[vihoAdminConfig.primary]

			}

			chart2 = new ApexCharts(
			    document.querySelector("#basic-apex2"),
			    options
			);

			chart2.render();
    	}
    }else{
    	var options = {
		    chart: {
		        height: 350,
		        type: 'area',
		        zoom: {
		            enabled: false
		        },
		        toolbar:{
		          show: false
		        }
		    },
		    dataLabels: {
		        enabled: false
		    },
		    stroke: {
		        curve: 'straight'
		    },
		    series: [{
		        name: "",
		        data: [1]
		    }],
		    title: {
		        text: '',
		        align: 'left'
		    },
		    subtitle: {
		        text: '',
		        align: 'left'
		    },
		    labels: ['1'],
		    
		    yaxis: {
		        opposite: true
		    },
		    legend: {
		        horizontalAlign: 'left'
		    },
		    colors:[vihoAdminConfig.primary]

		}

		chart2 = new ApexCharts(
		    document.querySelector("#basic-apex2"),
		    options
		);

		chart2.render();
    }

    /*
	var options = {
	    chart: {
	        height: 350,
	        type: 'area',
	        zoom: {
	            enabled: false
	        },
	        toolbar:{
	          show: false
	        }
	    },
	    dataLabels: {
	        enabled: false
	    },
	    stroke: {
	        curve: 'straight'
	    },
	    series: [{
	        name: "STOCK ABC",
	        data: series.monthDataSeries1.prices
	    }],
	    title: {
	        text: 'Fundamental Analysis of Stocksd',
	        align: 'left'
	    },
	    subtitle: {
	        text: 'Price Movements',
	        align: 'left'
	    },
	    labels: series.monthDataSeries1.dates,
	    xaxis: {
	        type: 'datetime',
	    },
	    yaxis: {
	        opposite: true
	    },
	    legend: {
	        horizontalAlign: 'left'
	    },
	    colors:[vihoAdminConfig.primary]

	}

	chart2 = new ApexCharts(
	    document.querySelector("#basic-apex2"),
	    options
	);

	chart2.render();
	*/
}
function consu_vendedores(suc){
	$.ajax({
        type:'POST',
        url: base_url+"Comisiones/consu_vendedores",
        data:{suc:suc},
        statusCode:{
            404: function(data){
            	toastr.error('No Se encuentra el archivo');
            },
            500: function(){
            	toastr.error('500');
            }
        },
        success:function(data){
            $('#eje_selected').html(data);
        }
    });  
}

function detCancela(idpers,tipo){
	//console.log("idpers: "+idpers);
	$("#modalDetalle").modal("show");
	var suc = $('#suc_selected option:selected').val();
	var mes = $('#mes_selected option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"Comisiones/getCanceladas",
        data:{ idp:idpers, suc:suc, mes:mes, tipo:tipo },
        async:false,
        success:function(data){
            $('#cont_cancel').html(data);
        }
    });
}

function loadtableProds(){
	var suc = $('#suc_selected option:selected').val();
	var eje = $('#eje_selected option:selected').val();
	var mes = $('#mes_selected option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"Comisiones/getProdsVendidos",
        data: { suc:suc, eje:eje, mes:mes },
        async: false,
        success: function (response){
            $('#cont_prods').html(response);
            $('#table_prods_detalle').DataTable({"lengthMenu": [[30,50], [30,50]], "order": [[ 0, "asc" ]]});
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}

function exportComisiones(){
	var suc = $('#suc_selected option:selected').val();
	var eje = $('#eje_selected option:selected').val();
	var mes = $('#mes_selected option:selected').val();
	
	window.open(base_url+"Comisiones/exportListComisiones/"+suc+"/"+eje+"/"+mes,"_blank");
}