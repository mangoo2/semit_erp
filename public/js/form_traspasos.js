var base_url = $('#base_url').val();
var id_reg = $('#id_reg').val();
$(document).ready(function() {
    get_producto();
    setTimeout(function(){ 
        $('#idproducto').select2('open');
        $('.select2-search__field').focus();
    }, 1000);

    $("#recargas").on("change",function(){
        tipoTraslado();
    });

    $("#id_tanque").select2({
        width: '100%',
        //minimumInputLength: 2,
    }).on('select2:select', function (e) {
        var data = e.params.data;
        get_producto_info(data.id,1);
        setTimeout(function(){ 
            //$('#id_tanque').val(null).trigger('change');
        }, 1000);
    });

});

function tipoTraslado(){
    if($("#recargas").is(":checked")==true){
        $(".cont_search_tras").hide("slow");
        $(".cont_rechange").show("slow");
    }else{
        $(".cont_rechange").hide("slow");
        $(".cont_search_tras").show("slow");
    }
}

function get_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Traspasos/searchproductos',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        get_producto_info(data.id);
    });
}

function get_producto_info(id,tipo=0){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Traspasos/get_tabla_producto',
        data: {id:id,tipo:tipo},
        success:function(data){ 
            //console.log("data: "+data);
            if(tipo==0){
                $('.tabla_precios').html(data);
            }else{
                $('.tabla_tanques').html(data);
            }
        }
    });
}

var stock_sucn=0;
var sucursaly=0;
var sucursalzn=0;
function check_sy_btn(num,stock){
    sucursaly=num;
    stock_sucn=stock;
}

function check_sz_btn(num){
    sucursalzn=num;
}

function guardar_registro(tipo){
    if(tipo==0){
        var idproducto = $('#idproducto option:selected').val();
        var cantidad=$('#cantidad').val();
        var cann=parseFloat(cantidad);
    }else{
        var idproducto = $('#id_tanque option:selected').val();
        var cantidad=$('#cant_litros').val();
        var cann=parseFloat(cantidad);
    }
    if(idproducto==''){
        swal("¡Atención!","Debes seleccionar un producto para traspasar así como la cantidad.", "error");
    }else{
        var stockn=parseFloat(stock_sucn);
        
        if(isNaN(cann) || cann==0){
            swal("¡Atención!", "Falta agregar cantidad", "error");
        }else{    
            if(tipo==0){ //producto
                if(stockn>=cann){
                    registro_trasferencia(tipo);
                }else{
                    swal("¡Atención!", "No es posible traspasar la cantidad ingresada. El almacén de origen no cuenta con el inventario suficiente. ", "error");
                }
            }else{ //recarga de tanque padre
                //console.log("stockn: "+stockn);
                //console.log("cann: "+cann);
                if(( cann % 9500 ) == 0){
                    if(stockn>=cann){
                        registro_trasferencia(tipo);
                    }else{
                        swal("¡Atención!", "No se cuenta con los litros suficientes", "error");  
                    }
                }else{
                    swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");  
                }
            }
        }
    }
}

function registro_trasferencia(tipo){
    if(tipo==0){
        var idproducto = $('#idproducto option:selected').val();
        var cantidad=$('#cantidad').val();
        var cann=parseFloat(cantidad);
    }else{
        var idproducto = $('#id_tanque option:selected').val();
        var cantidad=$('#cant_litros').val();
        var cann=parseFloat(cantidad);
    }
 
    if(sucursaly==0 || sucursalzn==0){
        swal("¡Atención!", "Falta seleccionar surcursal", "error");
    }else{
        $.ajax({
            type:'POST',
            url: base_url+'Traspasos/registro_transferir',
            data: {
                id:$('#id_reg').val()
            },
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $('.btn_registro').attr('disabled',true);
            },
            success:function(data){ 
                var idr=parseFloat(data); 
                $('#id_reg').val(idr);
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Traspasos/transferir_cantidad_sucursal',
                    data: {
                        sucursaly:sucursaly,sucursalzn:sucursalzn,cantidad:cann,idproducto:idproducto,idtranspaso:idr,tipo:tipo
                    },
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){  
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            get_traspaso_detalles(idr);
                            get_producto_info(idproducto);
                            //
                        }, 500);
                    }
                });
            }   
        });
    } 
}

function get_traspaso_detalles(idr){
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/get_transpasos',
        data: {
            id:idr,tipo:0
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){  
            $('.text_transpasos').html(data);
            $('.btn_registro').attr('disabled',false);
        }
    });
}

function guardar_registro_href() {
    swal("Éxito!", "Guardado Correctamente", "success");
    setTimeout(function(){ 
       window.location = base_url+'Traspasos';
    }, 500);
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar transpaso?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/delete_record_transpaso",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        var idr=$('#id_reg').val();
                        get_traspaso_detalles(idr);
                        var idproducto = $('#idproducto option:selected').val();
                        get_producto_info(idproducto);
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}