var base_url = $('#base_url').val();
var tablec_pp;
$(document).ready(function($) {
    tablec_pp = $('#tabla_facturacion_pp').DataTable();
    
    $('#idcliente_pp').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        text: element.razon_social

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id,0)
    });
        
});
function loadtable_fpp(){
    //alert(1);
    $('.searchtext').css('display','none');
    $('.searchtext2').css('display','block');
    //$('body').loader('show');
    var personals=$('#personals_pp option:selected').val();
    var idcliente=$('#idcliente_pp option:selected').val()==undefined?0:$('#idcliente_pp option:selected').val();
    var inicio=$('#finicial_pp').val();
    var fin=$('#ffinal_pp').val();
    tablec_pp.destroy();
    tablec_pp = $('#tabla_facturacion_pp').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturas/getlistfacturas_com",
            type: "post",
            "data": {
                'personal':personals,
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin
            },
        },
        "columns": [
            {"data": "complementoId",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'">\
                              <label for="complemento_'+row.complementoId+'"></label>';
                    }else{
                        html='<input type="checkbox" class="filled-in complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'" disabled>\
                              <label for="complemento_'+row.complementoId+'"></label>';
                    }
                    return html;
                }
            },
             {"data": "R_nombre"},
            {"data": "folios",},
            {"data": "Monto",
                render:function(data,type,row){
                    var html='';
                        html=new Intl.NumberFormat('es-MX').format(row.Monto);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='Facturado';
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            html='Sin Facturar';
                        }
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": 'nombre'},
            {"data": null,
                 render:function(data,type,row){
                    var html='<div class="btns-factura">';
                    if (row.Estado==0) {//cancelado
                        html+='<a class="btn-pdf tooltipped" href="'+base_url+'Folio/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" title="PDF"></a> ';
                        html+='<a class="btn-xml tooltipped" href="'+base_url+'hulesyg/facturas/'+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" title="XML"></a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'hulesyg/facturas/'+row.rutaAcuseCancelacion+'" target="_blank" data-position="top" data-delay="50" data-tooltip="Acuse de cancelación" download="" title="Acuse de cancelación">\
                                <i class="far fa-file-code" style="color: #ef7e4f;"></i></a> ';
                        /*
                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped insertaracuse_'+row.complementoId+'_1 " \
                                    onclick="insertaracuse('+row.complementoId+',1)" \
                                    data-docacuse="'+row.doc_acuse_cancelacion+'" \
                                    data-position="top" data-delay="50" data-tooltip="subir Acuse de cancelación" title="subir Acuse de cancelación" \
                                  download ><i class="fas fa-file-pdf" style="color: #ef7e4f;"></i>\
                                    </a> ';
                        */

                    }
                    if (row.Estado==1) {//
                        html+='<a class="btn-pdf tooltipped" href="'+base_url+'Folio/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" title="PDF"></a> ';
                        html+='<a class="btn-xml tooltipped" href="'+base_url+''+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" title="XML"></a> ';
                    }
                    if (row.Estado==2) {
                        html+='<button type="button"\
                          class="tooltipped btn-retimbrar" \
                          onclick="retimbrarcomplemento('+row.complementoId+')"\
                          data-position="top" data-delay="50" title="Retimbrar" id="refacturar"></button> ';
                    }
                    if (row.Estado==1) {
                        if(row.envio_m_p>0){
                            var envmail='enviado tooltipped';
                            var envmailt=' title="'+row.per_envio+' '+row.envio_m_d+'" data-position="top" data-delay="50" data-tooltip="'+row.per_envio+' '+row.envio_m_d+'"';
                        }else{
                            var envmail='';
                            var envmailt='';
                        }
                        //html+=' <button type="button" class="b-btn b-btn-primary email '+envmail+'" '+envmailt+' onclick="enviarmailcompl('+row.complementoId+','+row.clienteId+')"><i class="fas fa-mail-bulk"></i></button>'; 
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        
    }).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf()"></i>');
        //verificarcomplementos();
        //verificarpagos();
        //verificarcancelacion();
        //$('body').loader('hide');
        $( ".tooltipped" ).tooltip();
    });
}

function search2(){
    var searchtext = $('#searchtext2').val();
    tablec_pp.search(searchtext).draw();
}