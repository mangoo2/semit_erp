var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {
    table();
    //$('#empleado_busqueda').val('').focus();
    setTimeout(function(){ 
        $('#empleado_busqueda').val('');
        reload_registro();
    }, 1000);
});


function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Usuarios/getlistado",
            type: "post",
            "data":{empleado:$('#empleado_busqueda').val(),desactivar:$('#desactivar').val(),sucursal:$('#sucursal').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data":"Usuario"},
            {"data":"perfil"},
            {"data":"puesto"},
            {"data":"name_suc"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.acceso==0){
                    html='<button class="btn btn-danger btn-sm" type="button">Suspendido</button>';
                    html+='<a class="btn idreg_'+row.personalId+'" type="button" data-motivo_desactivar="'+row.motivo_desactivar+'" onclick="modal_motivo('+row.personalId+')"><i class="fa fa-eye"></i></a>';
                }else{
                    html='<button class="btn btn-sm" style="background-color: #93ba1f !important;border-color: #93ba1f !important;" type="button">Activo</button>';
                }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                        html+='<a href="'+base_url+'Usuarios/registro/'+row.personalId+'" class="btn btn-cli-edit"></a>';
                        if(perfil!=2 || perfil!=3){
                            html+='<a onclick="eliminar_registro('+row.personalId+')" class="btn btn-cli-delete"></a>';
                        }
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function modal_motivo(id){
    $('#modal_suspension').modal('show');
    var motivo=$('.idreg_'+id).data('motivo_desactivar');
    $('.motivotxt').html(motivo);
}


function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este usuario?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Usuarios/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


var id_registro_sus_aux=0;
function suspender_registro(id){ 
    id_registro_sus_aux=id;
    $('#suspender_registro_modal').modal();
}

function ver_motivo(id){
    var motivo = $('.sus_'+id).data('motivo');
    var fechabaja = $('.sus_'+id).data('fechabaja');
    $('#motivo_registro_modal').modal();
    $('#text_motivo').html(motivo);
    $('.txt_fecha').html(fechabaja);
    
}

function get_excel(){
    location.href= base_url+'Usuarios/reporte_excel';
}