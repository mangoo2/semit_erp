var base_url = $('#base_url').val();
var id_reg = $('#id_reg').val();
$(document).ready(function() {

    $("#id_producto,#fechai,#fechaf,#id_sucursal").on("change",function(){
        if($("#id_producto").val()!=null && $("#fechai").val()!="" && $("#fechaf").val()!="" && $("#id_sucursal option:selected").val()!="0"){
            loatTable(0);
        }
        if($("#id_producto option:selected").val()==null && $("#id_sucursal option:selected").val()!="0" && $("#recargas").is(":checked")==true){
            loatTable(1);
        }
    });

    $('#id_producto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Kardex/search_allproductos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
    });

    $("#recargas").on("change",function(){
        if($("#recargas").is(":checked")==true){
            $('#id_producto').val(null).trigger('change');
            $("#id_producto").attr("disabled",true);
        }else{
            $("#id_producto").attr("disabled",false);
        }
    });

    $('.exportExcel').on('click', function() {
        var tipo=0;
        var id_suc = $("#id_sucursal option:selected").val();
        var id_prod=$("#id_producto option:selected").val();
        if($("#recargas").is(":checked")==true){
            tipo=1;
            id_prod=0;
        }
        if($("#id_producto").val()!=null && $("#fechai").val()!="" && $("#fechaf").val()!="" && $("#id_sucursal option:selected").val()!="0" ||
            $("#id_producto option:selected").val()==null && $("#id_sucursal option:selected").val()!="0" && $("#recargas").is(":checked")==true){
            $('body').loading({ theme: 'dark', message: 'Generando archivo, por favor espere...' });
            $.ajax({
                url:base_url+'Kardex/exportExcel/'+tipo+"/"+id_prod+"/"+$("#fechai").val()+"/"+$("#fechaf").val()+"/"+id_suc,
                type: 'POST',
                success: function(response) {
                    window.location.href = base_url + 'Kardex/exportExcel/'+tipo+"/"+id_prod+"/"+$("#fechai").val()+"/"+$("#fechaf").val()+"/"+id_suc;
                    $('body').loading('stop');
                },
                error: function() {
                    swal("Error","Hubo un error al generar el archivo, intente nuevamente","error");
                    $('body').loading('stop'); // Desbloquear la pantalla
                }
            });
        }
    });

});

function loatTable(recarga){
    //console.log("load table");
    table = $('#table_kardex').DataTable({
        destroy:true,
        "ajax": {
            type: "POST",
            "url": base_url+"Kardex/getDataKardex",
            data: { id_producto:$("#id_producto option:selected").val(), fechai:$("#fechai").val(), fechaf:$("#fechaf").val(), id_suc:$("#id_sucursal option:selected").val(), recarga:recarga },
        },
        "columns": [
            {"data": "idProducto", 
                "render": function ( data, type, row, meta ) {
                    var prod=row.idProducto+" / "+row.nombre;
                    return prod;
                }
            },
            {"data": null, 
                "render": function ( data, type, row, meta ) {
                    var det="";

                    if(row.tipo=="0")
                        det="De stock normal";
                    if(row.tipo=="1")
                        det="<b>De serie:</b> "+row.serie;
                    if(row.tipo=="2")
                        det="<b>De lote:</b> "+row.lote;
                    if(row.tipo=="3")
                        det="Insumo";
                    if(row.tipo=="4")
                        det="Refacción";

                    if(row.tipo=="1" && row.tipo_mov=="Ajuste de producto")
                        det=row.serie;

                    if(row.tipo=="r")
                        det="Oxígeno";

                    return det;
                }
            },
            {"data": "reg"},
            {"data": "tipo_mov", 
                "render": function ( data, type, row, meta ) {
                    var html='';
                    var tex=row.tipo_mov+" <b>#"+row.id+"</b>";
                    var con=row.consulta;//0 contra 2 venta 3 traspaso 4 traspaso 5 garantia 6 bitacora
                    var status='';
                    if(con==0){
                        status='comp';
                    }
                    if(con==2 || con==7){
                        status='vent';
                    }
                    if(con==3 || con==4 || con=="4_2"){
                        status='tras';
                    }
                    if(con==5){
                        status='gara';
                    }
                    if(con=='6_2'){
                        status='ret_garan';
                    }
                    if(con=="7_2" || con=="7_3_1" || con==8){
                        status='dev';
                    }
                    if(con=="7_3"){
                        status='renta';
                    }
                    if(con==9 || con==10){
                        status='ins';
                    }
                    html+='<div class="status_tipo '+status+'">'+tex+'</div>';
                    return html;
                }
            },
            //{"data": "cant_inicial"},
            {"data": "cant_inicial", 
                "render": function ( data, type, row, meta ) {
                    var cant="";
                    if(row.consulta=="3" || row.consulta=="4" || row.consulta=="4_2"){
                        if($("#perfil").val()=="1"){ //administrador
                            cant=row.cant_inicial+" "+row.cant_inicial_d;
                        }else{
                            if(row.idsucursal_sale==$("#sucursal").val()){
                                cant=row.cant_inicial;
                            }
                            if(row.idsucursal_entra==$("#sucursal").val()){
                                cant=row.cant_inicial_d;
                            }
                        }
                    }else{
                        cant = row.cant_inicial;
                        if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                            cant=row.cant_inicial+" (L)";
                        }
                    }
                    return cant;
                }
            },
            //{"data": "cantidad"},
            {"data": "cantidad", 
                "render": function ( data, type, row, meta ) {
                    var cant="";
                    /* se comento por que en el reporte aun que ayan entrado mas de una serie solo aparece uno y tendria que se por el total de las series 
                    if(row.tipo=="1")
                        cant="1";
                    else
                        cant=row.cantidad;
                    */
                    cant=row.cantidad;
                    if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                        cant_lit = row.cantidad * 9500;
                        cant=row.cantidad+" ("+cant_lit+" L)";
                    }

                    cant+='<br><!-- consulta: '+row.consulta+'-->';
                    return cant;
                }
            },
            //{"data": "cant_final"},
            {"data": "cant_final", 
                "render": function ( data, type, row, meta ) {
                    var cant="";
                    if(row.consulta=="3" || row.consulta=="4" || row.consulta=="4_2"){
                        if($("#perfil").val()=="1"){ //administrador
                            cant=row.cant_final+"<br>"+row.cant_final_d;
                            /*if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                                cant_lit = row.cant_final * 9500;
                                cant_lit_d = row.cant_final_d * 9500;
                                cant=row.cant_final+" ("+cant_lit+" L)<br>"+row.cant_final_d+" ("+cant_lit_d+" L)";
                            }*/
                        }else{
                            if(row.idsucursal_sale==$("#sucursal").val()){
                                cant=row.cant_final;
                                /*if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                                    cant_lit = row.cant_final * 9500;
                                    cant=row.cant_final+" ("+cant_lit+" L)";
                                }*/
                            }
                            if(row.idsucursal_entra==$("#sucursal").val()){
                                cant=row.cant_final_d;
                                /*if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                                    cant_lit_d = row.cant_final_d * 9500;
                                    cant=row.cant_final_d+" ("+cant_lit_d+" L)";
                                }*/
                            }
                        }
                    }else{
                        cant = row.cant_final;
                        if(row.consulta==0 && row.tipo=="r"){ //compra de oxigeno
                            cant_lit = row.cantidad * 9500;
                            cant_tan = parseFloat(row.cant_inicial / 9500).toFixed(2);
                            cant_tanq_fin = Number(cant_lit) + Number(row.cant_inicial);
                            cant=Number(row.cantidad)+Number(cant_tan)+" ("+cant_tanq_fin+" L)";
                        }
                    }
                    return cant;
                }
            },
            {"data": null, 
                "render": function ( data, type, row, meta ) {
                    var html='';
                    if(row.iva_comp>0 || row.iva>0){
                        var precio=row.precio_unitario*1.16;
                        if(row.consulta!=0 && row.tipo!="r"){
                            precio = Math.round((precio + Number.EPSILON) * 100) / 100;
                        }
                    }else{
                        //console.log("precio_unitario: "+row.precio_unitario);
                        var precio=parseFloat(row.precio_unitario).toFixed(5);
                    }
                    var costo = parseFloat(precio).toFixed(5);
                    //console.log("costo: "+costo);
                    if(row.consulta==0 && row.tipo=="r"){
                        html = "$"+costo;
                    }else{
                        html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(costo);
                    }
                    return html;
                }
            },
            {"data": null, 
                "render": function ( data, type, row, meta ) {
                    var html='';
                    var cant="";
                    /* se comento por que en el reporte aun que ayan entrado mas de una serie solo aparece uno y tendria que se por el total de las series 
                    if(row.tipo=="1"){
                        cant="1";
                    }else{
                        cant=row.cantidad;
                    }
                    */
                    cant=row.cantidad;
                    if(row.iva_comp>0 || row.iva>0){
                        var precio=row.precio_unitario*1.16;
                        if(row.consulta!=0 && row.tipo!="r"){
                            precio = Math.round((precio + Number.EPSILON) * 100) / 100;
                        }
                    }else{
                        var precio=parseFloat(row.precio_unitario).toFixed(5);
                    }

                    var costo = Number(precio) * cant;
                    costo = parseFloat(costo).toFixed(5);
                    if(row.consulta==0 && row.tipo=="r"){
                        costo = precio * 9500 * row.cantidad;
                    }
                    costo = parseFloat(costo).toFixed(5);
                    html+='<!--'+precio+'-->';
                    if(row.consulta==0 && row.tipo=="r"){
                        html = "$"+costo;
                    }else{
                        html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(costo);
                    }
                    
                    return html;
                }
            },
            {"data": null, 
                "render": function ( data, type, row, meta ) {
                    var html="";
                    if(row.incluye_iva==1){
                        if(row.iva>0){
                            html = "002 - IVA 16%";
                        }else{
                            html = "001 - IVA 0%"; 
                        }
                    }else{
                        html="EXENTO";
                    }   
                    return html;
                }
            },       
            
            //{"data": "tipo_mov"},
            {"data": "null", 
                "render": function ( data, type, row, meta ) {
                    var det="";
                    /*if(row.tipo_mov=="Compra")
                        det="Factura: "+row.factura+" Entrada a "+row.destino;
                    if(row.tipo_mov=="Dispersión" && row.distribucion==1)
                        det="Factura: "+row.factura+" Entrada a "+row.destino;*/
                    if(row.factura!=""){
                        det="Factura: <b>"+row.factura+" "+row.destino+"</b>";
                    }else{
                        det="<b>"+row.destino+"</b>";
                    }
                    if(row.tipo_mov=="Venta"){
                        det=row.destino;
                    }
                    det_mov="";
                    if(row.tipo_mov=="Ajuste de producto"){ //falta ajustar este dato cuando el modulo de ajustes esté listo, tambien en excel
                        if(row.tipo==0 || row.tipo==3 || row.tipo==4){
                            det_mov="Cant. inicial: "+row.distribusion+"<br>Cant. Mov: "+row.cantidad;
                        }
                        if(row.tipo==1){
                            det_mov=row.serie;
                        }
                        if(row.tipo==2){
                            det_mov="Cant. inicial: "+row.distribusion+"<br>Cant. Mov: "+row.cantidad;
                        }
                    }
                    pdf="";
                    if(row.tipo_mov=="Venta" && $("#perfil").val()=="1"){
                        pdf='<a href="'+base_url+'Kardex/ticket/'+row.id+'" class="btn-ticket tooltipped" target="_blank" data-placement="top" title="Ticket venta"></a> ';
                    }
                    if(row.tipo_mov=="Devolución" && $("#perfil").val()=="1" || row.tipo_mov=="Devolución insumos concentrador" && $("#perfil").val()=="1"){
                        pdf='<a href="'+base_url+'Kardex/ticket/'+row.distribusion+'" class="btn-ticket tooltipped" target="_blank" data-placement="top" title="Ticket venta"></a> ';
                    }
                    if(row.tipo_mov=="Compra" && $("#perfil").val()=="1"){
                        pdf='<a href="'+base_url+'OCS/imprimir_compra/'+row.id+'" target="_blank" title="PDF"><img class="img_icon" src="'+base_url+'public/img/pdf1.png"></a> ';
                    }
                    if(row.tipo_mov=="Traslado" && $("#perfil").val()=="1" || row.tipo_mov=="Traslado Aceptado" && $("#perfil").val()=="1"){
                        pdf='<a href="'+base_url+'Traspasos/imprimir_traspaso/'+row.id+'" target="_blank" title="PDF"><img class="img_icon" src="'+base_url+'public/img/pdf1.png"></a> ';
                    }
                    return det+"<br>"+det_mov+" "+pdf;
                }
            },
            {"data": "reg"}
        ],
        "lengthMenu": [[30,50,100], [30,50,100]],
        columnDefs: [
            { targets: 11, visible: false }
        ],
        order: [[11, 'asc']]
    });
}

