var tablef;
var base_url=$('#base_url').val();
var server=$('#server').val();
var nombre_per=$('#nombre_per').val();
var ventaid;
$(document).ready(function() {	
	tablef = $('#table_datos').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ entradas",
                "zeroRecords": "Lo sentimos - No se han encontrado elementos",
                "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "search": "Buscar : _INPUT_",
                "paginate": {
                    "previous": "Página previa",
                    "next": "Siguiente página"
                  }
            }
        });
    if(nombre_per!=1 && $("#perfil").val()!="1"){
        loadtable();
    }

    $("#getCorte").on("click",function(){
        $("#modalcorte").modal("show");
    });

    $("#generarc").on("click",function(){
        getCorteCaja()
    });

    $("#file").fileinput({
        showCaption: false,
        showUpload: false,
        //maxFileCount: 6,
        language:'es',
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Listaventas/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                //input_name:"file",
                id_venta:$("#id_venta").val()
            };
            return info;
        }
    }).on('fileuploaded', function(event, files, extra) {
        getEvidencia();
        $("#file").fileinput("clear");
    }).on('filedeleted', function(event, files, extra) {
        //$(".row_1").remove();
        getEvidencia();
    });

    $('#sucursalselect').change(function(event) {
        var sucu =$('#sucursalselect option:selected').val();
        consu_vendedores(sucu);
    });
    $('#btn-edit-df-timbre').click(function(event) {
        $('#btn-edit-df-timbre').prop('disabled',true);
        setTimeout(function(){ 
            $('#btn-edit-df-timbre').prop('disabled',false);
        }, 5000); 
        edittimbredf();
    });
    $('#cdf_razon_social').change(function(event) {
        var valor = $(this).val();
            valor = limpiarEspacios(valor);
            $(this).val(valor);
    });
    $('#cdf_rfc').change(function(event) {
        var valor = $(this).val();
            valor = limpiarEspacios(valor);
            $(this).val(valor);
    });
    $('#cdf_cp').change(function(event) {
        var valor = $(this).val();
            valor = limpiarEspacios(valor);
            $(this).val(valor);
    });
    $('#idrazonsocial').select2({
        dropdownParent: $('#modaltimbrado'),
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var nom=element.razon_social;
                    itemscli.push({
                        id: element.id,
                        text: nom,
                        rfc: element.rfc,
                        cp: element.cp,
                        RegimenFiscalReceptor: element.RegimenFiscalReceptor,
                        uso_cfdi: element.uso_cfdi
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        dias_saldox=data.dias_saldo;
        $('#cdf_razon_social').val(data.text);
        $('#cdf_rfc').val(data.rfc);
        $('#cdf_cp').val(data.cp);
        $('#cdf_rf').val(data.RegimenFiscalReceptor).change();
        $('#cdf_uso_cfdi').val(data.uso_cfdi);
        
    });
});

function consu_vendedores(suc){
    $('#idvendedor').html("");
    $.ajax({
        type:'POST',
        url: base_url+"Listaventas/consu_vendedores",
        data:{suc:suc},
        statusCode:{
            404: function(data){
                toastr.error('No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('500');
            }
        },
        success:function(data){
            $('#idvendedor').html(data);
        }
    });  
}

function getEvidencia(){
    $("#cont_imgs").html("");
    $.ajax({
        type:'POST',
        url: base_url+'Listaventas/getEvidencias',
        data: { id_venta: $("#id_venta").val() },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}

function getCorteCaja(){
   $.ajax({
        type:'POST',
        url: base_url+"Listaventas/getCorteDia",
        data: {
            id_sucursal:$("#id_sucursal option:selected").val()
        },
        success: function (response){
            var array = $.parseJSON(response);
            //$('.tbody_tablepagos').html(array.html);
            $('.tot_efe').html("$"+array.efectivo);
            $('.tot_tarj').html("$"+array.tarjetas);
            $('.tot_che').html("$"+array.che);
            $('.tot_tra').html("$"+array.transf);
            $('.supertot').html("$"+array.supertot);
            $('.tot_devo').html("$"+array.moto_devolucion);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    }); 
}

function loadtable(){
	var sucursal=$('#sucursalselect option:selected').val();
    var tipoventa=$('#tipo_venta option:selected').val();
    var activo = $('#activo option:selected').val();
	tablef.destroy();
	tablef = $('#table_datos').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Listaventas/getlistventas",
            type: "post",
            "data": {
                'sucursal':sucursal,
                'tipoventa':tipoventa,
                'idvendedor':$('#idvendedor').val(),
                'activo':activo
            },
        },
        "columns": [
            {"data": "folio"},
            {"data": "name_suc"},
            {"data": "nombre"},
            {"data": "reg"},
            //{"data": "razon_social"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.razon_social=="" || row.razon_social==undefined){
                        html=row.name_cli;
                    }else{
                        html=row.razon_social;
                    }
                    if(row.id_razonsocial_new>0){
                        html+=' / '+row.razon_social2;
                    }
                    return html;
                }
            },
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="view_productos('+row.id+')"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<button type="button" class="btn btn-primary" onclick="view_formapagos('+row.id+','+row.total+')"><i class="fas fa-dollar-sign"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                     if(row.activo==1){
                     	html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">Activa</span>';
                     }
                     if(row.activo==0){
                     	html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Eliminada</span>';
                     }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="div-btn">';
                    if(row.FacturasId>0){
                    	if(row.Estado==2){
                    		html+='<a type="button" class="btn btn-primary tooltipped" onclick="retimbrar('+row.FacturasId+')" data-placement="top" title="Retimbrar factura"><span class="fa-stack"><i class="fas fa-file fa-stack-2x"></i><i class="fas fa-sync fa-stack-1x" style="color: red;"></i></span></a>';
                    	}else{
                    		html+='<a  href="'+base_url+'Folio/facturapdf/'+row.FacturasId+'" class="btn-pdf tooltipped" target="_blank" data-placement="top" title="Factura pdf"></a > ';
                    		html+='<a  href="'+base_url+''+row.rutaXml+'" class="btn-xml tooltipped" target="_blank" download data-placement="top" title="XML de factura"></a> ';
                            if(row.faccliente==1){
                                html+='<a  onclick="infofac('+row.FacturasId+')" class="btn-infofac tooltipped" data-placement="top" title="Informacion de factura"><i class="clasmale fas fa-male"></i></a> ';
                            }
                            if(row.pg_global==1){
                                html+='<span class="badge badge-success">Factuta global '+row.fechatimbre+'</span>';

                                html+='<a  onclick="refacturarventa('+row.id+','+row.FacturasId+')" type="button" class="btn-timbrar tooltipped refacturarventa_'+row.id+'" data-url="'+server+'?monto='+row.total+'&folio='+row.folio+'" data-placement="top" title="Facturar Venta"></a >';
                            }
                            if(row.activo==1 && $("#perfil").val()=="1" || row.activo==1 && $("#perfil").val()=="8"){ //admin y contabilidad
                                html+='<a  onclick="cancelarventa('+row.id+','+row.FacturasId+')" type="button" class="btn-cancelarventa tooltipped" data-placement="top" title="Cancelar venta"></a >';
                            }
                    	}
                    }else{
                    	html+='<span class="badge m-l-10 class_btn_no" style="background: #93ba1f;">NO</span>';
                        if(row.activo==1){
                            //html+='<a  href="'+server+'?monto='+row.total+'&folio='+row.folio+'" type="button" class="btn-timbrar tooltipped" target="_blank" data-placement="top" title="Facturar Venta"></a >';
                            html+='<a onClick="modaltimbrado('+row.id+')"  type="button" class="btn-timbrar tooltipped" data-placement="top" title="Facturar Venta"></a >';
                            if(row.activo==1 && $("#perfil").val()=="1" || row.activo==1 && $("#perfil").val()=="8"){ //admin y contabilidad
                                html+='<a  onclick="cancelarventa('+row.id+',0)" type="button" class="btn-cancelarventa tooltipped" data-placement="top" title="Cancelar venta"></a >';
                            }
                        }
                    }
                        html+='</div>';
                    
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    
                    if(row.tipo_venta==1){
                        html+='<!--pagocre: '+row.pagocre+'-->';
                        html+='<!--pagocom: '+row.pagocom+'-->';
                        if(parseFloat(row.pagocre)>0){
                            var pagocre=parseFloat(row.pagocre);
                        }else{
                            var pagocre=0;
                        }
                        if(parseFloat(row.pagocom)>0){
                            var pagocom=parseFloat(row.pagocom);
                        }else{
                            var pagocom=0;
                        }
                        html+='<!--pagocre: '+pagocre+'-->';
                        html+='<!--pagocom: '+pagocom+'-->';
                        var totalpagado=parseFloat(pagocre)+parseFloat(pagocom);
                        html+='<!--totalpagado: '+totalpagado+'-->';
                        if(parseFloat(totalpagado)>0){
                            html+='<!--total: '+totalpagado+' >= '+row.total+'-->';
                            if(parseFloat(totalpagado)>=parseFloat(row.total)){
                                var estatuspago='<span class="badge badge-success">Saldada</span>';
                            }else{
                                var estatuspago='<span class="badge badge-warning">Saldada Parcial</span>';
                            }
                        }else{
                            var estatuspago='<span class="badge badge-danger">No saldada</span>';
                        }
                        html+='<br>'+estatuspago;
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.razon_social=="" || row.razon_social==undefined){
                        cli=row.name_cli;
                    }else{
                        cli=row.razon_social;
                    }
                    html+='<a href="'+base_url+'Ventasp/ticket/'+row.id+'" class="btn-ticket tooltipped" target="_blank" data-placement="top" title="Ticket venta"></a> ';
                    if(row.tot_servs==0 || row.tot_servs>0 && row.tot_prods>0){
                        html+='<a onclick="aplicarGarantia('+row.id+','+row.id_garantia+')" href="javascript:void(0)" data-cliente="'+cli+'" data-dir="'+row.calle+'" data-col="'+row.colonia+'" data-tel="'+row.telefono+'" data-mail="'+row.correo+'" data-rfc="'+row.rfc+'" \
                        data-tel_c="'+row.tel_contacto+'" data-nom_c="'+row.nom_contacto+'" class="tooltipped venta_'+row.id+'" data-placement="top" title="Garantía">\
                        <img style="width: 45px;" src="'+base_url+'public/img/pv/garantias.png"> </a> ';
                    }
                    if(row.tipo_venta==1){
                        var boton_pagos='<a onclick="pagosv('+row.id+','+row.total+')" class="btn btn-primary" target="_blank"><i class="fas fa-dollar-sign"></i></a>';
                        if(row.Estado==1){
                            if(row.FormaPago=='PPD'){
                                var boton_pagos='<a href="'+base_url+'Folio/complementos/'+row.FacturasId+'" class="btn btn-primary" target="_blank">Complemento</a>';
                            }    
                        }
                        html+=boton_pagos
                    }
                    if(row.FacturasId>0){
                        if(row.Estado==2){}else{
                            if(row.activo==1){
                                html+='<a  onclick="enviomail('+row.FacturasId+')" type="button" class="btn-enviofac tooltipped enviomail_'+row.FacturasId+'" data-correo="'+row.correo+'" data-placement="top" title="Envio factura"></a >';
                            }
                        }
                    }
                    return html;
                }
            }   
        ],
        "order": [[ 3, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        },
        createdRow: function( row, data, dataIndex ) {
            // Set the data-status attribute, and add a class
            if(data.id_garantia!=0){
                $(row).addClass("tr_garantia").css("background-color","rgb(255, 255, 128, 0.5)");
                //$('td:nth-child(1)').addClass("tr_garantia");
                $("td", row).addClass("tr_garantia").css("background-color","rgb(255, 255, 128, 0.5)");
            }
        }
    }).on('draw',function(){
        $('.tooltipped').tooltip();
    });
}

function aplicarGarantia(id,id_garantia){
    $('#modalgarantia').modal('show');
    $("#id_venta").val(id);
    $('#nom_solicita').val("");
    $('#dir_solicita').val("");
    $('#col_solicita').val("");
    $('#cel_solicita').val("");
    $('#tel_solicita').val("");
    $('#mail_solicita').val("");
    $('#rfc_solicita').val("");
    $('#nom_contacto').val("");
    $('#tel_contacto').val("");
    if(id_garantia>0){
        $(".save_sg").attr("disabled",true);
        $("#cont_save").hide();
    }else{
        $(".save_sg").attr("disabled",false);
        $("#cont_save").show();
    }

    $.ajax({
        type:'POST',
        url: base_url+"Listaventas/view_productos_garantia",
        data: {
            ventaid:id
        },
        success: function (response){
            var array=$.parseJSON(response);
            $('.det_prods_garan').html(array.html);
            $('#motivo_solicita').val(array.motivo);
            if(id_garantia==0){
                $('#nom_solicita').val($(".venta_"+id).data("cliente"));
                $('#dir_solicita').val($(".venta_"+id).data("dir"));
                $('#col_solicita').val($(".venta_"+id).data("col"));
                $('#cel_solicita').val($(".venta_"+id).data("tel"));
                $('#mail_solicita').val($(".venta_"+id).data("mail"));
                $('#rfc_solicita').val($(".venta_"+id).data("rfc"));
                $('#nom_contacto').val($(".venta_"+id).data("nom_c"));
                $('#tel_contacto').val($(".venta_"+id).data("tel_c"));
            }else{
                $('#nom_solicita').val(array.cli);
                $('#dir_solicita').val(array.dir);
                $('#col_solicita').val(array.col);
                $('#cel_solicita').val(array.cel);
                $('#tel_solicita').val(array.tel);
                $('#mail_solicita').val(array.mail);
                $('#rfc_solicita').val(array.rfc);
                $('#nom_contacto').val(array.nom_contacto);
                $('#tel_contacto').val(array.tel_contacto);
            }
            getEvidencia();        
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });  
}

function saveSolicitaGarantia(){
    var DATA  = [];
    var TABLA   = $("#table_solicita tbody > tr");
    var cont_serie=0;
    TABLA.each(function(){         
        item = {};
        item ["idprod"] = $(this).find("input[id*='idprod']").val();
        item ["id_ps_lot"] = $(this).find("input[id*='id_ps_lot']").val();
        item ["id_ps_ser"] = $(this).find("input[id*='id_ps_ser']").val();
        item ["cant"] = $(this).find("input[id*='cant']").val();
        item ["tipo"] = $(this).find("input[id*='tipo_prod']").val();
        item ["id_vd"] = $(this).find("input[id*='id_vd']").val();
        id_prod = $(this).find("input[id*='idprod']").val();
        id_vd = $(this).find("input[id*='id_vd']").val();
        if($("#solicitar_"+id_prod+"_"+id_vd).is(":checked")==true){
            DATA.push(item);
            cont_serie++;
        }
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    //INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    //var datos = $('#form_solicita').serialize()+'&id_venta='+$("#id_venta").val()+'&array_prod='+aInfo;
    //console.log("datos: "+datos);

    if($("#table_solicita tbody > tr").length>0 && cont_serie>0){
        //var datos = 'motivo='+$("#motivo_solicita").val()+'&id_venta='+$("#id_venta").val()+'&array_prod='+aInfo;
        var datos = $('#form_solicita').serialize()+'&id_venta='+$("#id_venta").val()+'&array_prod='+aInfo;
        $.ajax({
            data: datos,
            type: 'POST',
            url : base_url+'Listaventas/save_solicitudVenta',
            beforeSend: function(){
                $('#savesg').attr('disabled',true);
            },
            success: function(data){
                $('#modalgarantia').modal('hide');
                swal("Éxito!", "Solicitado correctamente", "success");
                loadtable();
                setTimeout(function(){ 
                    envia_mail(idreg);
                }, 2000); 
            }
        }); 
    }else{
        swal("¡Atención!","Elije al menos un producto", "error");
    }  
}

function envia_mail(id){
    $.ajax({
        type:'POST',
        url: base_url+"Listaventas/mail_solicitudGaran",
        data: { id:id },
        success:function(data){

        }
    });
}

function view_productos(id){
	$('#modalproductos').modal('show');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Listaventas/view_productos",
        data: {
        	ventaid:id
        },
        success: function (response){
            $('.table_pro').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
function view_formapagos(id,total){
	$('#modalformaspagos').modal('show');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Listaventas/view_formapagos",
        data: {
        	ventaid:id,
        	total:total
        },
        success: function (response){
            $('.table_forma').html(response);
            
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function retimbrar(idfactura){

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/retimbrar",
                    data: {
                        factura:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            loadtable();
                        }
                        $('body').loading('stop');
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function pagosv(id,total){
    ventaid=id;
    $('#montototalventa').val(total);
    var total_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(total);
    $('#mtv').html(total_l);

    $('#modalpagos').modal('show');
    table_credito_pagos(ventaid);
    setTimeout(function(){ 
        calcularrestante();
    }, 1000);
}
function guardar_pago_compras(){
    var form_pago = $('#form_pago');
    
    var valid = form_pago.valid();
    if(valid){
        var restante =parseFloat($('#montototalventarestante').val());
        var pago_p =parseFloat($('#pago_pago').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = form_pago.serialize()+'&ventaid='+ventaid;
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Listaventas/guardar_pago",
              data:datos,
              success: function(data){
                toastr["success"]("Guardado correctamente"); 
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_credito_pagos(ventaid);
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
              }
            });
        }else{
            toastr["warning"]("Monto mayor a 0 y menor o igual al restante"); 
        }
    }   
}
function table_credito_pagos(id){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Listaventas/table_get_credito_pagos",
      data:{id:id},
      success: function(data){
        $('.text_tabla_pago').html(data);
      }
    });
}
function calcularrestante(){
    var totalv=parseFloat($('#montototalventa').val());
    var pagos = 0;
    $(".montoagregado").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    pagos=pagos.toFixed(2);
    var restante = parseFloat(totalv)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
    }
    $('#montototalventarestante').val(restante);
    $('#mtv_r').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(restante));
}
function infofac(idfac){
    console.log(idfac);
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Listaventas/infofac",
      data:{id:idfac},
      success: function(data){
        var array = $.parseJSON(data);
        var html='';
            html+='<div class="row"><div class="col-md-5 if_c_g">Fecha y hora de timbrado:</div><div class="col-md-7">'+array.fechatimbrado+'</div></div>';
            html+='<div class="row"><div class="col-md-5 if_c_g">Razón social:</div><div class="col-md-7">'+array.razonsocial+'</div></div>';
            html+='<div class="row"><div class="col-md-5 if_c_g">RFC:</div><div class="col-md-7">'+array.rfc+'</div></div>';
            html+='<div class="row"><div class="col-md-5 if_c_g">Régimen fiscal:</div><div class="col-md-7">'+array.regimen+'</div></div>';
            html+='<div class="row"><div class="col-md-5 if_c_g">Código postal:</div><div class="col-md-7">'+array.codigop+'</div></div>';
            html+='<div class="row"><div class="col-md-5 if_c_g">Uso de CFDI:</div><div class="col-md-7">'+array.uso+'</div></div>';
        //swal("Facturación por cliente", html);
        $.alert({boxWidth: '30%',useBootstrap: false,title: 'Facturación por cliente',theme: 'bootstrap',content: html});
      }
    });
}
function cancelarventa(idventa,facturasid){
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Listaventas/infoventa",
      data:{
            id:idventa,
            fac:facturasid
        },
      success: function(data){
        var array = $.parseJSON(data);
        //console.log(array);
            if(array.fac>0){
                var html_folio='<div class="col-md-3 if_c_t">FOLIO</div><div class="col-md-3 if_c_g">'+array.fac_folio+'</div>';
            }else{
                var html_folio='';
            }
        var html='';
            html+='<div class="row wconf"><div class="col-md-3 if_c_t">TICKET No.</div><div class="col-md-3 if_c_g">'+array.folio+'</div>'+html_folio+'</div>';
            if(array.fac>0){
                html+='<div class="row wconf"><div class="col-md-3 if_c_t">UUID.</div><div class="col-md-9 if_c_g">'+array.fac_uuid+'</div></div>';
            }
            html+='<div class="row wconf"><div class="col-md-12">';
                   html+='<table class="table"><thead>';
                   html+='<tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Precio U</th><th>Importe</th></tr>';
                   html+='</thead><tbody>' ;
                    $.each(array.productos, function(index, item) {
                        var nombrex='';
                        if(item.id_ps_lote==0 && item.id_ps_serie==0){
                            nombrex=item.nombre;
                        }else{
                            if(item.id_ps_lote!=0){
                                nombrex='Lote: '+item.lote;    
                            }else{
                                nombrex='Serie: '+item.serie;
                            }
                        }
                        var importe=parseFloat(item.cantidad)*parseFloat(item.precio_unitario);

                        html+='<tr><td>'+item.idProducto+'</td><td>'+nombrex+'</td><td>'+item.cantidad+'</td><td>'+item.precio_unitario+'</td><td>'+importe.toFixed(2)+'</td></tr>';
                    });
                   html+='</tbody></table>';
            html+='</div></div>';
            html+='<div class="row wconf"><div class="col-md-3 if_c_t">SUBTOTAL</div><div class="col-md-7 if_c_g">'+array.subtotal+'</div></div>';
            html+='<div class="row wconf"><div class="col-md-3 if_c_t">IVA</div><div class="col-md-7 if_c_g">'+array.iva+'</div></div>';
            html+='<div class="row wconf"><div class="col-md-3 if_c_t">TOTAL</div><div class="col-md-7 if_c_g">'+array.total+'</div></div>';

            $.confirm({
                boxWidth: '40%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Cancelar ticket': function (){
                        cancelarventa2(idventa,facturasid);
                    },
                    'Regresar': function () {
                        
                    }
                }
            });
        }
    });
}
function cancelarventa2(idventa,facturasid){
    var html='';
        if(facturasid==0){
            html+='<div class="row wconf"><div class="col-md-12 if_c_t">No se generará una nota de crédito. El producto regresará a su almacén de origen</div></div>';
            html+='<div class="row wconf"><div class="col-md-12 if_c_t"></div></div>';
        }
        html+='<div class="row wconf"><div class="col-md-12 if_c_t">Motivo de devolución.</div></div>';
        html+='<div class="row wconf"><div class="col-md-12 if_c_t"><textarea class="form-control" id="motivocancelacion"></textarea></div></div>';
        if(facturasid>0){
            html+='<div class="row wconf"><div class="col-md-12 error">¿Confirma la devolución del ticket y generación de nota de crédito?</div></div>';
        }else{
            html+='<div class="row wconf"><div class="col-md-12 error">¿Confirma la devolución del ticket?</div></div>';
        }
        
        if(facturasid>0){
            $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
        }

        $.confirm({
            boxWidth: '40%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                'Cancelar ticket': function (){
                    var motivo=$('#motivocancelacion').val();
                    console.log('idventa: '+idventa+' facturasid: '+facturasid);
                    $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Timbrado/cancelarventa2",
                        data:{
                            id:idventa,
                            mot:motivo,
                            fac:facturasid
                        },
                        success: function(data){
                            console.log(data);
                            //toastr.success('Venta Cancelada con éxito');
                            //==================
                                $.confirm({
                                    boxWidth: '40%',
                                    useBootstrap: false,
                                    icon: 'fa fa-warning',
                                    title: 'Éxito!',
                                    content: 'Venta Cancelada con éxito',
                                    type: 'green',
                                    typeAnimated: true,
                                    buttons:{
                                        'ok': function () {
                                            
                                        }
                                    }
                                });
                            //==================
                            if(facturasid>0){

                                var array = $.parseJSON(data);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                }else{
                                    var id_fac = array.facturaId;
                                    if(id_fac>0){
                                        $.confirm({
                                            boxWidth: '40%',
                                            useBootstrap: false,
                                            icon: 'fa fa-warning',
                                            title: 'Éxito!',
                                            content: 'Se ha creado la factura',
                                            type: 'green',
                                            typeAnimated: true,
                                            buttons:{
                                                'Ver nota de crédito': function () {
                                                    var url=base_url+'Folio/facturapdf/'+id_fac;
                                                    window.open(url, '_blank'); 
                                                },
                                                'ok': function () {
                                                    
                                                }
                                            }
                                        });
                                    }else{
                                        swal("Éxito!", "Se ha creado la factura", "success");    
                                    }
                                    
                                    loadtable();
                                }
                                $('body').loading('stop');
                            }else{
                                loadtable();
                            }
                            
                        }
                    });

                },
                'Regresar': function () {
                    
                }
            }
        });
}

function enviomail(facturaid){
    $('.iframefacvent').html('<iframe src="'+base_url+'Folio/facturapdf/'+facturaid+'"></iframe>');
    setTimeout(function(){
        var correo=$('.enviomail_'+facturaid).data('correo');
        $('#fac_email').val(correo);
    }, 1000);
    var html='';
        html+='<div class="row wconf"><div class="col-md-12 if_c_t">¿Desea enviar la factura.</div></div>';
        html+='<div class="row wconf"><div class="col-md-12 if_c_t">Correo.</div></div>';
        html+='<div class="row wconf"><div class="col-md-12 if_c_t"><input type="email" class="form-control" id="fac_email"></div></div>';
    $.confirm({
            boxWidth: '40%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                'Enviar factura': function (){
                    var fac_email=$('#fac_email').val();
                    $.ajax({
                        type:'POST',
                        url: base_url+'Mailenvio/envio_fac',
                        data:{
                            'idfac':facturaid,
                            'email':fac_email
                        },
                        statusCode:{
                            404: function(data){
                                toastr.error('Se a detectado un error 404','Error!');
                            },
                            500: function(){
                                toastr.error('Se a detectado un error 500','Error!');
                            }
                        },
                        success:function(data){
                            toastr.success('Se a enviado');
                        }
                    });

                },
                'Regresar': function () {
                    
                }
            }
        });
}
//2317
//2330

///2327
function refacturarventa(idventa,facturasid){
    var html='';
        
            html+='<div class="row wconf"><div class="col-md-12 ">¿Confirma la generacion de factura, se generará una nota de credito de la factura anterior?</div></div>';
        
        
        if(facturasid>0){
            $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
        }

        $.confirm({
            boxWidth: '40%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                'Confirmar': function (){
                    $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Timbrado/cancelarnotaventa",
                        data:{
                            id:idventa,
                            fac:facturasid
                        },
                        success: function(data){
                            console.log(data);
                            toastr.success('Venta Cancelada con éxito');
                            if(facturasid>0){

                                var array = $.parseJSON(data);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                }else{
                                    swal("Éxito!", "Se ha creado la nota de credito", "success");
                                    //var url = $('.refacturarventa_'+idventa).data('url');
                                    modaltimbrado(idventa);
                                    //window.open(url, '_blank'); 
                                    loadtable();
                                }
                                $('body').loading('stop');
                            }else{
                                loadtable();
                            }
                            
                        }
                    });

                },
                'Regresar': function () {
                    $('body').loading('stop');
                }
            }
        });
}
function modaltimbrado(idventa){
    ventaid=idventa;
    $('#modaltimbrado').modal("show");
}
function regimefiscal(){
    $( "#cdf_uso_cfdi").val('');
    $( "#cdf_uso_cfdi option").prop( "disabled", true );
    var regimen = $('#cdf_rf option:selected').val();
    $( "#cdf_uso_cfdi ."+regimen ).prop( "disabled", false );
}
function edittimbredf(){
    var form=$('#form_datos_fis_timb');
    //form.valid();
    //form.serialize()
    if(form.valid()){
        $('#modaltimbrado').modal("hide");
        $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Timbrado/generafacturarventa/"+ventaid,
            data: {
                editdf:1,
                cdfrs:$('#cdf_razon_social').val(),
                cdfrfc:$('#cdf_rfc').val(),
                cdfcp:$('#cdf_cp').val(),
                cdfrf:$('#cdf_rf option:selected').val(),
                cdfuso:$('#cdf_uso_cfdi option:selected').val(),
                idrs:$('#idrazonsocial option:selected').val()
            },
            success: function (response){
                $('body').loading('stop');
                //console.log(response);
                var array = $.parseJSON(response);
                var facturaId = array.facturaId;
                if (array.resultado=='error') {
                    //console.log(array.MensajeError);
                    //console.log(array.info.TimbrarCFDIResult.MensajeErrorDetallado);
                    var texto=array.info.TimbrarCFDIResult.MensajeErrorDetallado;
                    if(texto==''){
                        var texto=array.MensajeError;
                    }
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: "Error "+array.CodigoRespuesta+"!",
                        content: texto,
                        type: 'red',
                        typeAnimated: true,
                        buttons:{
                            /*
                            'Editar datos': function (){
                                //var idrazonsocial = $('#idrazonsocial option:selected').val();
                                var idrazonsocial=id_razonsocial;
                                if(idrazonsocial>0){
                                    editaryretimbrar(idrazonsocial,facturaId);
                                }else{
                                    toastr.error('Algo salió mal, reintente desde el catalogo de clientes y retimbre');
                                }
                            },
                            */
                            Salir: function (){
                                window.location.href = base_url+"index.php/Listaventas"; 
                            }
                        }
                    });
                    setTimeout(function(){ 
                        //window.location.href = base_url+"index.php/Listaventas"; 
                    }, 5000);
                }else{
                        //var textofactura='Se ha Guardado la factura';
                    
                        var textofactura='Se ha creado la factura';
                    
                    swal({
                        title:"Éxito!", 
                        text:textofactura, 
                        type:"success"
                        });
                    setTimeout(function(){ 
                        window.open(base_url+'Folio/facturapdf/'+facturaId,'_blank'); 
                        //window.location.href = base_url+"index.php/Listaventas"; 
                    }, 2000);
                    loadtable();
                }
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                
            }
        });
    }else{
        toastr.error('Verifique los datos ingresados');
    }
}
function limpiarEspacios(texto) {
    if (typeof texto !== 'string') {
        throw new Error('El valor proporcionado no es un string');
    }
    return texto.trim();
}