var base_url = $('#base_url').val();
var id_reg = $('#id_reg').val();
$(document).ready(function() {
    selectEquipo();
    $("#id_equipo,#id_serie,#fechai,#fechaf").on("change",function(){
        if($("#id_equipo option:selected").val()!=null && $("#fechai").val()!="" && $("#fechaf").val()!=""){
            setTimeout(function(){ 
                loatTable();
            }, 1000);
        }
    });
    $("#id_sucursal").on("change",function(){
        $('#id_equipo').val(null).trigger('change');
        $("#id_serie").val("");
    });

    $("#exportExcel").on("click",function(){
        if($("#id_equipo option:selected").val()!=null && $("#fechai").val()!="" && $("#fechaf").val()!=""){
            setTimeout(function(){ 
                exportarKardex();
            }, 1000);
        }
    });
});

function formatState_input_searchrec (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="columss_in_1">' + state.idProducto + '</span> <span class="columss_in_2">' + state.producto + '</span><span class="columss_in_3">'+state.serie+'</span>'
  );
  return $state;
};

function selectEquipo(){
    $('#id_equipo').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un equipo',
        allowClear: true,
        templateResult: formatState_input_searchrec,
        ajax: {
            url: base_url+'Mttos_internos/searchEquipos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    id_sucursal: $("#id_sucursal option:selected").val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id_prod,
                        text: element.idProducto+" / "+element.producto,
                        idProducto: element.idProducto,
                        producto: element.producto,
                        serie: element.serie,
                        id_pss: element.id
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("id_pss: "+data.id_pss);
        $('#id_serie').html("");

        $("#id_equipo option:selected").attr('data-id_pss',data.id_pss);
        $("#id_equipo option:selected").attr('data-serie',data.serie);
        //$("#serie").val(data.serie);
        let $option = $('<option />', {
            text: data.serie,
            value: data.id_pss,
        });
        $('#id_serie').prepend($option);
        //$('#id_equipo').val(null).trigger('change');
        //$("#id_equipo").empty();
    });
}

function loatTable(){
    //console.log("load table");
    var id_equipo=$("#id_equipo option:selected").val();
    var id_serie=$("#id_serie option:selected").val();
    table = $('#table_kardex').DataTable({
        destroy:true,
        "ajax": {
            type: "POST",
            "url": base_url+"Mttos_internos/getDataKardex_equipos",
            data: { id_equipo: id_equipo, id_serie:id_serie, fechai:$("#fechai").val(), fechaf:$("#fechaf").val() },
        },
        "columns": [
            {"data": "id"},
            {"data": "idProducto", 
                "render": function ( data, type, row, meta ) {
                    var prod=row.idProducto+" / "+row.equipo;
                    return prod;
                }
            },
            {"data": "serie"},
            {"data": "fecha_termino"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.tipo==0){
                        html="<span class='btn btn-warning'>Preventivo</span>";
                    }if(row.tipo==1){
                        html="<span class='btn btn-danger'>Correctivo</span>";
                    }
                    return html;
                }
            },
            {"data": "prox_mtto"},
            {"data": "observ"},
        ],
        "order": [[ 3, "desc" ]],
    });
}

function exportarKardex(){
    var id_equipo=$("#id_equipo option:selected").val();
    var id_serie=$("#id_serie option:selected").val();
    var fechai=$("#fechai").val();
    var fechaf=$("#fechaf").val();
    
    window.open(base_url+"Mttos_internos/exportKardexEquipo/"+id_equipo+"/"+id_serie+"/"+fechai+"/"+fechaf,"_blank");
}
