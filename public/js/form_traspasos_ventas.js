var base_url = $('#base_url').val();
var prod=0; var cant=0;
$(document).ready(function() {
    search_producto();
    $("#dia_venta,#id_sucursal").on("change",function(){
        if($("#id_sucursal").val()!="0"){
            get_productos_ventas();
        }
    });

    $("#sol_tras").on("click",function(){
        saveSolicita();
    });
});

function formatStateProds (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + ' ('+state.disp_stock+')</span>'
  );
  return $state;
}

function search_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatStateProds,
        ajax: {
            url: base_url+'Traspasos/searchproductos',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    /*if(element.tipo==0){ //normal
                        disp_stock=element.stockps;
                    }else if(element.tipo==1){ //serie
                        disp_stock=element.serie8
                    }else if(element.tipo==2){ //lote
                        disp_stock=element.lote8;
                    }*/
                    if(element.tipo==0){
                        disp_stock=element.stockps-element.traslado_stock_cant;
                    }else if(element.tipo==1){
                        disp_stock=element.serie8-element.tot_tras;
                    }else if(element.tipo==2){
                        disp_stock=element.cant_lote-element.traslado_lote_cant;
                    }
                    itemscli.push({
                        id: element.id,
                        idProducto: element.idProducto,
                        text: element.idProducto+' '+element.nombre + " (stock: "+disp_stock+")",
                        nombre: element.nombre,
                        tipo:element.tipo,
                        lote8:element.lote8,
                        serie8:element.serie8,
                        stock:element.stock,
                        stockps:element.stockps,
                        disp_stock:disp_stock
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
       // get_producto(data.id);
        prod=data.nombre;
        /*if(data.tipo==0){ //normal
            cant_stock=data.stockps;
        }else if(data.tipo==1){ //serie
            cant_stock=data.serie8
        }else if(data.tipo==2){ //lote
            cant_stock=data.lote8;
        }*/
        //console.log("disp_stock: "+data.disp_stock);
        $("#idproducto option:selected").attr('data-stock',data.disp_stock);
        $("#idproducto option:selected").attr('data-codigo',data.idProducto);
        get_producto();
    });
}

function get_producto(){
    var id=$("#idproducto option:selected").val();
    var nombre = prod;
    var cantidad = Number($("#cantidad").val());
    var stock = Number($("#idproducto option:selected").data("stock"));
    var codigo = $("#idproducto option:selected").data("codigo");
    //console.log("codigo: "+codigo);
    var html="";

    if(cantidad==0){
        swal("¡Atención!","Ingreso una cantidad a solicitar", "error");
        return;
    }
    if(stock<cantidad){
        swal("¡Atención!","No se cuenta con suficiente stock en almacén general", "error");
        return;
    }
    var repetido=0;
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='idprod']").val(); 
        if (id == prodexi) {
            repetido=1;
        }   
    });
    if(repetido==0){
        html="<tr class='tr_prods_"+id+" even row_p"+id+"' role='row'>\
                <td class='sorting_1'>"+codigo+"</td>\
                <td>"+nombre+"</td>\
                <td class=''><input type='hidden' id='idprod' value='"+id+"'><input class='form-control' id='cantidad_ini' type='hidden' value='"+stock+"'>\
                <input class='form-control' id='cantidad_sol' type='number' value='"+cantidad+"'></td>\
                <td>"+stock+"</td>\
                <td><button class='btn btn-danger' onclick='eliminar("+id+",0)'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>\
            </tr>";
        $("#body_prods").append(html);
        $('#idproducto').val(null).trigger('change');
        $("#cantidad").val("");
    }else{
        swal("¡Atención!","Producto ya existente en tabla de solicitud", "error");
    }
}

function get_productos_ventas(){
    $.ajax({
        data: {dia:$("#dia_venta").val(),sucursal:$("#id_sucursal").val()},
        type: 'POST',
        url: base_url+'Traspasos/validar_dia_solicita',
        success: function(result){
            if(result==1){
                swal("¡Atención!","Productos del día ya solicitados", "error");
            }else{
                var tabla=$("#table_datos").DataTable({
                    "searching": false,
                    "info":     false,
                    "paging": false,
                    destroy:true,
                    "ajax": {
                        "url": base_url+"Traspasos/getlistaProds",
                        type: "post",
                        dataSrc:"",
                        data: { dia: $("#dia_venta").val(),sucursal:$("#id_sucursal").val() },
                        error: function(){
                           $("#table_datos").css("display","none");
                        }
                    },
                    "columns": [
                        {"data":"idProducto"},
                        {"data":"nombre"},
                        //{"data":"tot_prods"}
                        {"data": "total_prods",
                            "render": function ( data, type, row, meta ){
                                if(row.id_ps_lote>0){
                                    //cant_stock=row.lote8;
                                    //if(row.status_traslt!=2){
                                        cant_stock=(Number(row.lote8)-Number(row.traslado_stock_cant));
                                    //}
                                }else if(row.id_ps_serie>0){
                                    cant_stock=row.serie8
                                    //if(row.status_trasr!=2){
                                        cant_stock=(Number(row.serie8)-Number(row.traslado_stock_cant));
                                    //}
                                }else{
                                    //cant_stock=row.stock;
                                    cant_stock=(Number(row.stock)-Number(row.traslado_stock_cant));
                                }
                                dis=""; span1=""; span2="";
                                //tot_prods=Number(row.total_prods);
                                //console.log("total_concen_prods: "+row.total_concen_prods);
                                /*if(row.tipo=="0" && row.id_bpc!="0"){ //stock y son kit de concentrador
                                    tot_prods = Number(row.tot_prods) + Number(row.total_concen_prods);
                                }if(row.tipo=="1" && row.id_bpc=="0"){ //serie de concentrador
                                    tot_prods = Number(row.tot_prods);
                                }if(row.id_bpc==0){
                                    tot_prods=Number(row.total_prods);
                                }*/
                                tot_prods=Number(row.gg);
                                if(Number(cant_stock)<1 || tot_prods>cant_stock){
                                    dis="readonly disabled";
                                    //tot_prods=0;
                                    span1="<span style='opacity: 0.5;' class='btn btn-danger'>ALMACÉN GENERAL NO CUENTA CON SUFICIENTE STOCK DE ESTE PRODUCTO";
                                    span2="</span>"
                                }
                                var html="<input type='hidden' id='idprod' value='"+row.id+"'>\
                                        "+span1+"\
                                        <input "+dis+" onchange='cambiaCant($(this),"+tot_prods+")' data-cant_stock='"+cant_stock+"' type='number' id='cantidad_sol' value='"+tot_prods+"'>"+span2+"";
                                return html;
                            }
                        },
                        {"data": null,
                            "render": function ( data, type, row, meta ){
                                if(row.id_ps_lote>0){
                                    //cant_stock=row.lote8;
                                    //if(row.status_traslt!=2){
                                        cant_stock=(Number(row.lote8)-Number(row.traslado_stock_cant));
                                    //}
                                }else if(row.id_ps_serie>0){
                                    cant_stock=row.serie8
                                    //if(row.status_trasr!=2){
                                        cant_stock=(Number(row.serie8)-Number(row.traslado_stock_cant));
                                    //}
                                }else{
                                    //cant_stock=row.stock;
                                    cant_stock=(Number(row.stock)-Number(row.traslado_stock_cant));
                                }
                                return cant_stock+"<input type='hidden' id='cantidad_ini' value='"+cant_stock+"'>";
                            }
                        },
                        {"data": null,
                            "render": function ( data, type, row, meta ){
                                var html="<button class='btn btn-danger' onclick='eliminar("+row.id+",1)'> <i class='fa fa-trash' aria-hidden='true'></i></button>";
                                return html;
                            }
                        },
                    ],
                    "order": [[ 0, "desc" ]],
                    createdRow: function(row, data, dataIndex, cells) {
                        $(row).addClass('tr_prods_'+data.id+'');
                    },
                    "drawCallback": function( settings ) {
                        $(".btn_add").attr("disabled",false);   
                    }
                });
   
            }
        }
    });
}

function cambiaCant(ele,cant){
    var stock = ele.data("cant_stock");
    var solicita = ele.val();
    if(Number(solicita)>stock){
        swal("¡Atención!","Cantidad solicitada mayor a la disponible en almacén general", "error");
        ele.val(cant);
    }
}

function saveSolicita(){
    var cont_pros=0;
    var DATA  = [];
    var TABLA = $("#table_datos tbody > tr"); 
    TABLA.each(function(){         
        item = {};
        //item ["dia_venta"] = $("#dia_venta").val();
        item ["idprod"] = $(this).find("input[id*='idprod']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad_sol']").val();
        item ["cantidad_ini"] = $(this).find("input[id*='cantidad_ini']").val();
        DATA.push(item);
        cont_pros++;
    });
    if(cont_pros==0){
        swal("Álerta!","Ingrese un producto al menos a la solicitud", "warning");  
        return;
    }
    if($("#dia_venta").val()==""){
        swal("Álerta!","Ingrese una fecha a la solicitud", "warning");  
        return;
    }
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var suc=$('#id_sucursal option:selected').val();

    var datos = "prods="+aInfo+"&dia_venta="+$("#dia_venta").val()+'&suc='+suc;
    //console.log("datos: "+datos);
    $.ajax({
        //data: INFO+"&dia_venta="+$("#dia_venta").val(),
        data: datos,
        type: 'POST',
        url: base_url+'Traspasos/registro_solicitud',
        beforeSend: function(){
            $('.btn_add').attr('disabled',true);
        },
        success: function(data2){
            $(".btn_add").attr("disabled",false);
            swal("Correcto!","Solicitud creada correctamente", "success");
            setTimeout(function () { 
                envia_mail(data2);
            }, 1500);
            setTimeout(function () { 
                window.location.href = base_url+"Traspasos";
            }, 2500);
        }
    });
}

function envia_mail(id){
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/mail_solicitud",
        data: { id:id },
        success:function(data){

        }
    });
}

function eliminar(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el concepto del traspaso?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                /*$.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/delete_prod_solicitud",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        $(".row_p"+id).remove();
                    }
                });*/
                if(tipo==0)
                    $(".row_p"+id).remove();
                else
                    $(".tr_prods_"+id).remove();
            },
            cancelar: function () 
            {
                
            }
        }
    });
}