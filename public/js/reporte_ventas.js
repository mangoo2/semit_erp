var base_url=$('#base_url').val();
var chart2;
var chartuti;
$(document).ready(function($) {
	setTimeout(function(){ 
        generargrafica(0);
		graficaUtilidad(0);
    }, 1000);

	$('#id_suc,#fechai,#fechaf').on("change",function(){
		if($("#fechai").val()!="" && $("#fechaf").val()!=""){
			loadtable();
		}
	});

	$("#btn_export").on("click",function(){
		exportComisiones();
	});

	$("#btn_export_excel").on("click",function(){
		exportComisionesExcel();
	});
});

function loadtable(){
	$('body').loading({theme: 'dark',message: 'Procesando...'});
	var idsuc = $('#id_suc option:selected').val();
	var fechai = $('#fechai').val();
	var fechaf = $('#fechaf').val();

	$.ajax({
        type:'POST',
        url: base_url+"ReporteVentas/getTotalVentasList",
        data: { idsuc:idsuc, fechai:fechai, fechaf:fechaf },
        async:false,
        success: function (response){
        	$('body').loading('stop');
        	var array = $.parseJSON(response);
            $('.info_ventas').html(array.html);
            $('.info_utilidad').html(array.htmlutilidad);
            setTimeout(function(){ 
                generargrafica(1);
            	graficaUtilidad(1);
            }, 1000);
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            $('body').loading('stop');
        }
    });
}

function generargrafica(view){
	var idsuc = $('#id_suc option:selected').val();
	//console.log('idsuc: '+idsuc);
	if (view>0) {
        chart2.destroy();
    }
    if($("#fechai").val()!="" && $("#fechaf").val()!=""){
    	if(idsuc>0){
			var DATA_label  = [];
			var DATA_val  = [];
			DATA_label.push(''); 
			DATA_val.push(0); 
                
            var sup  = $("input[id*='supertot']").val();
            var nam  = $("input[id*='name_suc']").val();
            DATA_label.push(nam);
            DATA_val.push(sup);

			var options = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar: {
			            show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "Ventas",
			        data: DATA_val
			    }],
			    title: {
			        text: 'Montos de ventas',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos con IVA)',
			        align: 'left'
			    },
			    labels: DATA_label,
			    yaxis: {
			        opposite: true
			    },
			    xaxis: {
			        labels: {
			            show: true
			        },
			        title: {
			            text: ''
			        },
			        min: 0  // Establecer el límite mínimo en 0 para el eje X
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors: [vihoAdminConfig.primary]
			};

			chart2 = new ApexCharts(
			    document.querySelector("#basic-apex2"),
			    options
			);
			chart2.render();
    	}else{
    		var tab_ventas = $("#table_total_venta tbody > tr"); 
			var DATA_label  = [];
			var DATA_val  = [];
			DATA_label.push(); 
			DATA_val.push(); 
            tab_ventas.each(function(index){                   
                var sup  = $(this).find("input[id*='total']").val();
                var nam  = $(this).find("input[id*='name_suc']").val();
                //console.log("Index: " + index + ", Length: " + tab_ventas.length);
                if(index < tab_ventas.length - 1){
	                DATA_label.push(nam);
	                DATA_val.push(sup);
	            }
            });
            //console.log(DATA_label);
			//console.log(DATA_val);
    		var options = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar:{
			          show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "",
			        data: DATA_val
			    }],
			    title: {
			        text: 'Montos de ventas',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos con IVA)',
			        align: 'left'
			    },
			    labels: DATA_label,
			    
			    yaxis: { opposite: true }, legend:{ horizontalAlign: 'left' },
			    colors:[vihoAdminConfig.primary]

			}
			chart2 = new ApexCharts(
			    document.querySelector("#basic-apex2"),
			    options
			);
			chart2.render();
    	}
    }else{
    	//console.log('sin sucursal elegida de grafica ventas: ');
    	var options = {
		    chart: {
		        height: 350,
		        type: 'area',
		        zoom: {
		            enabled: false
		        },
		        toolbar:{
		          show: false
		        }
		    },
		    dataLabels: {
		        enabled: false
		    },
		    stroke: {
		        curve: 'straight'
		    },
		    series: [{
		        name: "",
		        data: [1]
		    }],
		    title: {
		        text: '',
		        align: 'left'
		    },
		    subtitle: {
		        text: '',
		        align: 'left'
		    },
		    labels: ['1'],
		    
		    yaxis: {
		        opposite: true
		    },
		    legend: {
		        horizontalAlign: 'left'
		    },
		    colors:[vihoAdminConfig.primary]
		}
		chart2 = new ApexCharts(
		    document.querySelector("#basic-apex2"),
		    options
		);
		chart2.render();
    }
}

function graficaUtilidad(view){
	var idsuc = $('#id_suc option:selected').val();
	//console.log('idsuc: '+idsuc);
	if (view>0) {
        chartuti.destroy();
    }
    if($("#fechai").val()!="" && $("#fechaf").val()!=""){
    	if(idsuc>0){
			////////////////////
			var DATAU_label  = [];
			var DATAU_val  = [];
			DATAU_label.push(''); 
			DATAU_val.push(0); 
                
            var sup_uti  = $("input[id*='suptot_uti']").val();
            var name_uti  = $("input[id*='name_suc_uti']").val();
            DATAU_label.push(name_uti);
            DATAU_val.push(sup_uti);

			var optionsuti = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar: {
			            show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "Utilidad",
			        data: DATAU_val
			    }],
			    title: {
			        text: 'Montos de utilidades',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos con IVA)',
			        align: 'left'
			    },
			    labels: DATAU_label,
			    yaxis: {
			        opposite: true
			    },
			    xaxis: {
			        labels: {
			            show: true
			        },
			        title: {
			            text: ''
			        },
			        min: 0  // Establecer el límite mínimo en 0 para el eje X
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors: [vihoAdminConfig.primary]
			};

			chartuti = new ApexCharts(
			    document.querySelector("#graph_utilidad"),
			    optionsuti
			);
			chartuti.render();
    	}else{
			/////////////////////////////////////
			var tab_utis = $("#table_total_utilidad tbody > tr"); 
			var DATAU_label  = [];
			var DATAU_val  = [];
			DATAU_label.push(); 
			DATAU_val.push(); 
            tab_utis.each(function(index){                   
                var sup_uti  = $(this).find("input[id*='total_utilidad']").val();
                var name_uti  = $(this).find("input[id*='name_suc_uti']").val();
                //console.log("sup_uti: "+sup_uti);
                if(index < tab_utis.length - 1){
	                DATAU_label.push(name_uti);
	                DATAU_val.push(sup_uti);
	            }
            });
    		var optionsuti = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar:{
			          show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "",
			        data: DATAU_val
			    }],
			    title: {
			        text: 'Montos de utilidades',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos con IVA)',
			        align: 'left'
			    },
			    labels: DATAU_label,
			    
			    yaxis: {
			        opposite: true
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors:[vihoAdminConfig.primary]
			}
			chartuti = new ApexCharts(
			    document.querySelector("#graph_utilidad"),
			    optionsuti
			);
			chartuti.render();
    	}
    }else{
    	var options = {
		    chart: {
		        height: 350,
		        type: 'area',
		        zoom: {
		            enabled: false
		        },
		        toolbar:{
		          show: false
		        }
		    },
		    dataLabels: {
		        enabled: false
		    },
		    stroke: {
		        curve: 'straight'
		    },
		    series: [{
		        name: "",
		        data: [1]
		    }],
		    title: {
		        text: '',
		        align: 'left'
		    },
		    subtitle: {
		        text: '',
		        align: 'left'
		    },
		    labels: ['1'],
		    
		    yaxis: {
		        opposite: true
		    },
		    legend: {
		        horizontalAlign: 'left'
		    },
		    colors:[vihoAdminConfig.primary]
		}
		chartuti = new ApexCharts(
		    document.querySelector("#graph_utilidad"),
		    options
		);
		chartuti.render();
    }
}

function exportComisiones(){
	var idsuc = $('#id_suc option:selected').val();
	var fechai = $('#fechai').val();
	var fechaf = $('#fechaf').val();

	if(fechai=="")
		fechai=0;
	if(fechaf=="")
		fechaf=0;
	
	window.open(base_url+"ReporteVentas/exportResultados/"+idsuc+"/"+fechai+"/"+fechaf,"_blank");
}

function exportComisionesExcel(){
	var idsuc = $('#id_suc option:selected').val();
	var fechai = $('#fechai').val();
	var fechaf = $('#fechaf').val();

	if(fechai=="")
		fechai=0;
	if(fechaf=="")
		fechaf=0;
	
	window.open(base_url+"ReporteVentas/exportResultadosExcel/"+idsuc+"/"+fechai+"/"+fechaf,"_blank");
}