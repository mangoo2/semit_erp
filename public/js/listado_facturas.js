var base_url = $('#base_url').val();
var tablef;
var tablep;
var perfilid = $('#perfilid').val();
var idfaccomp;
var tipofaccomp;
$(document).ready(function() {	
	tablef = $('#tabla_facturacion').DataTable();
    tablep = $('#tablependientesf').DataTable();
	loadtable();
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        text: element.razon_social

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id,0)
    });
  
    
});
function loadtable(){
    //console.log(base_url);
    var personals=$('#personals option:selected').val();
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var metodopago=$('#rs_metodopago option:selected').val()==undefined?0:$('#rs_metodopago option:selected').val();
    var tcfdi=$('#rs_tcfdi option:selected').val()==undefined?0:$('#rs_tcfdi option:selected').val();
    var inicio=$('#finicial').val();
    var fin=$('#ffinal').val();
    tablef.destroy();
    tablef = $('#tabla_facturacion').DataTable({
        
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturas/getlistfacturas",
            type: "post",
            "data": {
                'personal':personals,
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'metodopago':metodopago,
                'tcfdi':tcfdi
            },
        },
        "columns": [
            {"data": "FacturasId",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'">\
                              <label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
                              <label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
                }
            },
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        var serie=row.cadenaoriginal;
                        serie=serie.split('|');
                        if (serie[3]==undefined) {
                            var seriev='';
                        }else{
                            var seriev=serie[3];
                        }
                        html=seriev+''+row.Folio.padStart(4, 0);
                        html=row.Folio;
                        if(row.id_tras>0){
                            html+='<br>Traspaso: '+row.id_tras;
                        }


                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        
                        html=row.Nombre;
                        if(row.Rfc=='XAXX010101000'){
                            if(row.Nombre=='PUBLICO EN GENERAL'){
                                html=row.Nombre_alias;
                                if(row.Nombre_alias==null){
                                    html=row.Nombre;
                                }
                            }
                        }
                        html+='<!--'+row.Rfc+'-->';
                        html+='<!--'+row.Nombre+'-->';
                        html+='<!--'+row.Nombre_alias+'-->';
                    
                    return html;
                }
            },
            {"data": "Rfc",},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='<span class="btn btn-danger">Cancelada</span>';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='<span class="btn btn-success">Facturado</span>';
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            html='<span class="btn btn-primary">Sin Facturar</span>';
                        }
                    }else if(row.Estado==2){
                        html='<span class="btn btn-primary">Sin Facturar</span>';
                    }else{
                        html='';
                    }
                    if(row.tim==0){
                        html='<span class="btn btn-danger">Material Peligroso</span>';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": "FormaPago"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html=row.nombre;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='Factura';
                    
                    if(row.f_relacion==1 && row.f_r_tipo=='01'){
                        html='Factura 01 Nota Credito';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='04'){
                        html='Factura 04 Sustitucion';
                    }
                    if(row.f_relacion==0 && row.f_r_tipo=='07'){
                        html='Factura de Anticipo recibido';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='I'){
                        html='Factura con relacion de Anticipo';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='E'){
                        html='Factura Egreso de Anticipo';
                    }
                    if(row.cartaporte==1){
                        html='Carta Porte';
                    }

                        
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();


                    var btn_pdf='<a class="btn-pdf tooltipped" \
                                href="'+base_url+'Folio/facturapdf/'+row.FacturasId+'" target="_blank"\
                                data-facturasid="'+row.FacturasId+'" data-motofactura="'+row.total+'"\
                                data-position="top" data-delay="50" title="Factura" ></a> ';
                    var btn_refacturar='<a href="'+base_url+'Facturas/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=04" class="btn btn-primary tooltipped" data-position="top" title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
                    var btn_mail='<a style="cursor:pointer" onclick="modal_fact('+row.Folio+')"><img style="width: 33px;" src="'+base_url+'public/img/ennvio3.svg"></a>';
                    var btn_xml='<a class="btn-xml tooltipped" \
                                    href="'+base_url+row.rutaXml+'" target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML" title="xml" \
                                  download ></a> ';



                    if(row.f_relacion==1 && row.f_r_tipo=='01'){
                        //Factura 01 Nota Credito;
                        btn_refacturar='';
                        btn_mail='';
                    }

                    var html='<div class="btns-factura">';
                    var estadofactura=row.Estado;
                    if(estadofactura==1){
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            estadofactura=2;
                        } 
                    }
                    //if(row.FormaPago=='PUE'){
                        var classes_pue='consultar_fac_pue consultar_fac_pue_'+row.FacturasId+'';
                    //}else{
                        //var classes_pue='';
                    //}

                    if(estadofactura==0){ // cancelado
                        html+=btn_pdf;
                        html+=btn_xml;
                        html+='<a class="btn btn-primary tooltipped" \
                                    href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" target="_blank"\
                                    data-position="top" data-delay="50" title="Acuse de cancelación"\
                                  download ><i class="fas fa-file-invoice" style="color: #ef7e4f;"></i>\
                                    </a> ';
                                    /*
                        html+='<a\
                                    class="btn btn-primary tooltipped insertaracuse_'+row.FacturasId+'_0 " \
                                    onclick="insertaracuse('+row.FacturasId+',0)" \
                                    data-docacuse="'+row.doc_acuse_cancelacion+'" \
                                    data-position="top" data-delay="50" data-tooltip="subir Acuse de cancelación"\
                                  download ><i class="fas fa-file-pdf" style="color: #ef7e4f;"></i>\
                                    </a> ';
                                    */
                        if(perfilid==1){
                            /*
                            html+='<a class="btn btn-primary tooltipped" \
                                    onclick="regresarfacturaavigente('+row.FacturasId+')"  \
                                    data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Regresar a vigente"\
                                   ><i class="material-icons dp48" style="color: #ef7e4f;">replay</i>\
                                    </a> ';
                            */
                        }
                        //html+='<div class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></div>';
                    }else if(estadofactura==1){
                        if(row.FormaPago=='PPD'){
                            var complementoclass=' complementops_files_'+row.FacturasId+' ';
                        }else{
                            var complementoclass='';
                        }
                        html+=btn_pdf;
                        html+=btn_xml;
                                if(row.FormaPago=='PPD'){
                                    html+='<a href="'+base_url+'Folio/complementos/'+row.FacturasId+'" class="btn btn-primary tooltipped " \
                                            data-position="top" data-delay="50" title="Complemento de pago" target="_blank"><i class="fas fa-receipt " ></i></a> ';
                                }
                            if(row.totalnc>0){
                                html+='<a class="btn btn-primary tooltipped notascreditos_'+row.FacturasId+'" \
                                            onclick="notascreditos('+row.FacturasId+')"\
                                            data-position="top" data-delay="50" data-tooltip="Notas credito" data-uuid="'+row.uuid+'" \
                                          download ><i class="fas fa-object-group " ></i>\
                                            </a> ';
                            }
                        
                        html+='<div class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></div>';
                    }else if(estadofactura==2){
                        if(row.tim==1){
                            html+='<a type="button"\
                            class="tooltipped btn-retimbrar" \
                            onclick="retimbrar('+row.FacturasId+',0)"\
                            data-position="top" data-delay="50" data-tooltip="Retimbrar" title="Retimbrar" id="refacturar"></a> ';    
                        }
                        
                        /*
                        html+='<a type="button"\
                            href="'+base_url+'Facturaslis/add/'+row.FacturasId+'"\
                          class="btn btn-primary tooltipped btn-retimbrar" \
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-pencil-alt"></i>\
                          </span>\
                        </a> ';
                        */
                    }else{
                        html+='';
                    }
                    if(estadofactura==1){
                        if(row.cartaporte==0){
                            html+=btn_refacturar;
                        }
                        //============================
                        /*
                        html_nota='<a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=01" class="waves-effect btn-bmz blue"  title="Nota Credito"><i class="fas fa-file-invoice-dollar"></i></a>';
                        if(row.f_relacion==0 && row.f_r_tipo=='07'){
                            //html='Factura de Anticipo recibido';
                            html_nota='<a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=07&tc=I" class="waves-effect btn-bmz blue"  title="Relación al anticipo">RA</a>';
                        }
                        if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='I'){
                            //html='Factura con relacion de Anticipo';
                            html_nota='<a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=07&tc=E" class="waves-effect btn-bmz blue"  title="Relación al anticipo Egreso">EA</a>';
                        }
                        if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='E'){
                            //html='Factura Egreso de Anticipo';
                            html_nota='';
                        }
                        //=========================================
                        html+=html_nota;
                        */
                        if(row.cartaporte==0){
                            html+=btn_mail;
                        }
                    }
                    if(row.idventa>0){
                         html+='<a href="'+base_url+'Ventasp/ticket/'+row.idventa+'" class="btn-ticket tooltipped" target="_blank" data-position="top" data-delay="50" data-tooltip="Ticket de venta" title="Ticket de venta"></a> ';
                    }
                    if(row.cartaporte==1 && row.tim==0){
                        html+=btn_pdf;
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        columnDefs: [ { orderable: false, targets: [8,9] }],
        
    }).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf()"></i>');
        
        //verificarpagos();
        //verificarcancelacion();
        //verificarcomplementos();
        $( ".tooltipped" ).tooltip();
    });
}

var Folio=0;
function modal_fact(Folio){
    Folio=Folio;
    $('#modal_fact').modal('show');
}

function enviar_correo_cotizacion(){
    var correo=$('#correo_cl').val();
    if(correo!=''){
        swal("Éxito!", "Enviando correo...", "success");
        $.ajax({
            type:'POST',
            url: base_url+"Facturas/enviar_factura",
            data: {Folio:Folio,correo:correo},
            success: function (response){
                swal("Éxito!", "Cotización enviada", "success");
                setTimeout(function(){ 
                    $('#modal_fact').modal('hide');
                }, 1000); 
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
    }else{
        swal("¡Atención!", "Falta agregar un correo", "error");
    }
        
}

function search(){
    var searchtext = $('#searchtext').val();
    tablef.search(searchtext).draw();
}

function vista_x(){
    $('.searchtext').css('display','block');
    $('.searchtext2').css('display','none');
}

function retimbrar(idfactura,contrato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/retimbrar",
                    data: {
                        factura:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            loadtable();
                        }
                        $('body').loading('stop');

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function cancelarfacturas(){
    var html='<div class="row wconf">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" autocomplete="new-password" required/>\
                </div>\
              </div>\
              <div class="row wconf">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opción</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Cancelando Factura...'});
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    var facturaslis = $("#tabla_facturacion tbody > tr");
                    var DATAa  = [];
                    var num_facturas_selected=0;
                    facturaslis.each(function(){  
                        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                            num_facturas_selected++;
                            item = {};                    
                            item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                            
                            DATAa.push(item);
                        }       
                        
                    });
                    INFOa  = new FormData();
                    aInfoa   = JSON.stringify(DATAa);
                    //console.log(aInfoa);
                    if (num_facturas_selected==1) {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdi",
                            data: {
                                facturas:aInfoa,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                    $('body').loading('stop');
                                }else{
                                    swal("Éxito!", "Se ha Cancelado la factura", "success");
                                    loadtable();
                                    $('body').loading('stop');
                                }

                            }
                        });
                    }else{
                        toastr.error('Seleccione solo una factura');
                        $('body').loading('stop');
                    }
                //================================================
                                }else{
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'No tiene permisos'}); 
                                    $('body').loading('stop');
                                }
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            $('body').loading('stop');
                             
                        }
                    });
                    
                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                    $('body').loading('stop');
                }
            },
            salir: function () 
            {
                
            }
        }
    });
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
function cancelarcomplementos(){
    var complementoslis = $("#tabla_facturacion_pp tbody > tr");
    var DATAc  = [];
    var num_complemento_selected=0;
    var c_complementoId=0;
    complementoslis.each(function(){  
        if ($(this).find("input[class*='complementos_checked']").is(':checked')) {
            num_complemento_selected++;
                              
            c_complementoId  = $(this).find("input[class*='complementos_checked']").val();
            
        }       
        
    });
    if (num_complemento_selected==1) {
        //console.log(c_complementoId);
        cancelarcomplemento(c_complementoId);
        setTimeout(function(){ 
            $('#contrasena').val('');
         }, 1000);
        
    }else{
        toastr.error('Seleccione solo un complemento');
    }
}
function cancelarcomplemento(idcomplemento){
    var html='<div class="row wconf">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelación de un complemento?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                </div>\
              </div>\
              <div class="row wconf">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opcion</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Cancelando Complemento...'});
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    
                    //console.log(aInfoa);
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdicomplemento",
                            data: {
                                facturas:idcomplemento,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                    $('body').loading('stop');
                                }else{
                                    swal("Éxito!", "Se ha Cancelado el complemento", "success");
                                    loadtable_fpp();
                                    $('body').loading('stop');
                                }

                            },
                            error: function(response){
                                $.alert({
                                        boxWidth: '30%',
                                        useBootstrap: false,
                                        title: 'Error!',
                                        content: 'falla de comunicacion, intentar mas tarde'});   
                                  $('body').loading('stop');
                            }
                        });
                    
                //================================================
                                }else{
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'No tiene permisos'}); 
                                     $('body').loading('stop');
                                }
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});  
                                     $('body').loading('stop'); 
                             
                        }
                    });
                    
                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                     $('body').loading('stop');
                }
            },
            salir: function () 
            {
                
            }
        }
    });
}