var base_url = $('#base_url').val();
var id_reg = $('#id_reg').val();
$(document).ready(function() {
    $("#searchProd").on("click",function(){
        if($("#id_producto option:selected").val()!=null && $("#id_sucursal option:selected").val()!="0"){
            loatTable(0);
            loatTableAjustes(0);
        }
        if($("#id_producto option:selected").val()==null && $("#id_sucursal option:selected").val()!="0" && $("#recargas").is(":checked")==true){
            loatTable(1);
            loatTableAjustes(1);
        }
    });

    $("#recargas").on("change",function(){
        if($("#recargas").is(":checked")==true){
            $('#id_producto').val(null).trigger('change');
            $("#id_producto").attr("disabled",true);
        }else{
            $("#id_producto").attr("disabled",false);
        }
    });

    $('#id_producto').select2({
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        allowClear: true,
        templateResult: formatColums,
        ajax: {
            url: base_url+'AjustesProducto/searchproductos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre,
                        tipo: element.tipo,
                        idProducto: element.idProducto,
                        nombre: element.nombre
                    });
                });
                return {
                    results: itemscli
                };  
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //loatTable(data.id,data.tipo);
        $("#id_producto option:selected").attr('data-tipo',data.tipo);
        $("#id_producto option:selected").attr('data-id_prodsuc',data.tipo);
    });

    $(".btn_save").on("click",function(){
        if($("#table_inventario tbody > tr").length>0){
            saveAjuste();
        }
    });

});

function formatColums(state) {
    if (!state.id) {
        return state.text;
    } 
    var $state = $(
        '<span class="colum_ajuste_1">'+state.idProducto+'</span><span class="colum_ajuste_2">'+state.nombre+'</span>'
    );
    return $state;
}

function loatTable(recarga){
    $("#body_inventario").html("");
    $.ajax({
        type:'POST',
        url: base_url+'AjustesProducto/getTableProd',
        data: { id_prod:$("#id_producto option:selected").val(), tipo:$("#id_producto option:selected").data("tipo"), id_suc:$("#id_sucursal option:selected").val(), recarga:recarga },
        success:function(response){
            $("#body_inventario").html(response);
        }
    });
}

function saveAjuste(){
    var DATA  = [];
    var TABLA = $("#table_inventario tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["idp"] = $(this).find("input[id*='idp']").val();
        item ["id_suc"] = $(this).find("input[id*='id_suc']").val();
        item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
        item ["tipo"] = $(this).find("input[class*='tipo_prod']").val();
        item ["tipo_recarga"] = $(this).find("input[id*='tipo_recarga']").val();
        if($(this).find("input[class*='tipo_prod']").val()=="0" || $(this).find("input[class*='tipo_prod']").val()=="2" || $(this).find("input[class*='tipo_prod']").val()=="3" || $(this).find("input[class*='tipo_prod']").val()=="4"){
            item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item ["cantidad_ajuste"] = $(this).find("input[id*='stock_inv']").val();
        }
        if($(this).find("input[class*='tipo_prod']").val()=="1"){ //serie
            item ["num_serie"] = $(this).find("input[id*='num_serie']").val();
            item ["serie_ajuste"] = $(this).find("input[id*='serie_ajuste']").val();
        }
        if($(this).find("input[id*='tipo_recarga']").val()=="1"){
            item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item ["cantidad_ajuste"] = $(this).find("input[id*='stock_inv']").val();
        }
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'AjustesProducto/guardaAjusteProds',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
            swal("Éxito!", "Ajuste guardado correctamente", "success");
            setTimeout(function(){ 
                //window.location = base_url+'AjustesProducto';
                loatTableAjustes(data);
                //console.log("data: "+data);
            }, 1000);
        }
    }); 
}

function loatTableAjustes(recarga){
    $("#body_inventario").html("");
    $("#det_ajuste").show("slow");
    $("#body_dets_ajuste").html("");
    $.ajax({
        type:'POST',
        url: base_url+'AjustesProducto/getTableProdAjustes',
        async:false,
        data: { id_prod:$("#id_producto option:selected").val(), tipo:$("#id_producto option:selected").data("tipo"), id_suc:$("#id_sucursal option:selected").val(), recarga:recarga },
        success:function(response){
            $("#body_dets_ajuste").html(response);
        }
    });
}