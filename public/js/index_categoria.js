var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
});

function table(){
	tabla=$("#table_datos").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Categorias/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"clave"},
            {"data":"categoria"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                        html+='<a href="'+base_url+'Categorias/registro/'+row.categoriaId+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        html+='<a onclick="eliminar_registro('+row.categoriaId+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                    html+='</div>';
                return html;
                }
            }
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]]
    });
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Categorias/delete_record",
                    data: { id:id },
                    success:function(response){  
                        table();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}