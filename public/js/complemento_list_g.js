var base_url=$('#base_url').val();
$(document).ready(function($) {
	
});
function retimbrar_com(idfactura){

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/retimbrarcomplemento",
                    data: {
                        complemento:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Exito!", "Se ha creado la factura", "success");
                            window.location.reload();
                        }
                        $('body').loading('stop');
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}