var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('#table_data').DataTable();
	$('.modal').modal({backdrop: 'static', keyboard: false});
	$('#btn_save_ubi').click(function(event) {
		save_ubi();
	});
	$('#cp_estado').change(function(event) {
		var est = $('#cp_estado option:selected').val();
		$('#cp_cl_estado').val(est);
	});
});
function edit_suc(id){
	$('#modal_ubi').modal('show');
	$.ajax({
            type:'POST',
            url: base_url+"Cat_complemento/view_ubicaciones",
            data: {id:id},
            success: function (response){
                var array = $.parseJSON(response);
                $('#id_ubi').val(array.id);
                $('#cp_calle').val(array.cp_calle);
				$('#cp_n_ex').val(array.cp_n_ex);
				$('#cp_n_int').val(array.cp_n_int);
				$('#cp_colonia').val(array.cp_colonia);
				$('#cp_localidad').val(array.cp_localidad);
				$('#cp_ref').val(array.cp_ref);
				$('#cp_minicipio').val(array.cp_minicipio);
				$('#cp_estado').val(array.cp_estado);
				$('#cp_cl_estado').val(array.cp_cl_estado);
				$('#cp_pais').val(array.cp_pais);
				$('#cp_cp').val(array.cp_cp);
            },
            error: function(response){
            	console.log(response);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
}
function save_ubi(){
	var form = $('#form_ubicacion');
	if(form.valid()){
		var datos = form.serialize()
		$.ajax({
            type:'POST',
            url: base_url+"Cat_complemento/save_ubicaciones",
            data: datos,
            success: function (response){
                toastr.success('Guardado Correctamente'); 
                $('#modal_ubi').modal('hide'); 
            },
            error: function(response){
            	console.log(response);
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
            }
        });
	}
}