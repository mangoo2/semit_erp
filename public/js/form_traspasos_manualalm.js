var base_url = $('#base_url').val();
var prod=0; var cant=0; var idProductog=""; var nom_unidadg="";
var tipo_prodg=0; var id_prodg=0; var id_prod_sucg=0; var id_prodserieg=0; var cantidadg=0;
var serieg=0; var loteg=0; var cadg=0; var stockg=0; var nameprodg=""; var id_oc_ant=0; var cont_allgral=0;
$(document).ready(function() {
    search_producto();

    $("#recargas").on("change",function(){
        tipoTraslado();
    });

    $("#id_tanque").select2({
        width: '100%',
        //minimumInputLength: 2,
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if($("#cant_litros").val()!="" && $("#cant_litros").val()>0){
            //get_recarga();
            table_all_prods(0,0,0,0,0,$("#id_sucursal_salida option:selected").val(),3);
        }
    });
    $("#cant_litros").on("change",function(){
        var cant_lit =Number($("#cant_litros").val());
        if(cant_lit>0){
            //get_recarga();
            table_all_prods(0,0,0,0,0,$("#id_sucursal_salida option:selected").val(),3);
        }
    });

    $("#search_cod").keypress(function(e) {
        if (e.which==13) {
            //validar que exista sucursal origen (que surte)
            if($("#id_sucursal_salida option:selected").val()=="0")
                swal("¡Atención!", "Elija sucursal que surte", "error");
            else
                searchCode(this.value);
        }
    });

    $("#id_sucursal,#id_sucursal_salida").on("change",function(){
        validaSucursal();
    });
    validaSucursal();

    validaLitrosOxi();
    //$("#search_cod").attr("disabled",true);
    $("#id_sucursal_salida").on("change",function(){
        if($("#id_sucursal_salida option:selected").val()!=""){
            $("#search_cod").attr("disabled",false);
        }else{
            $("#search_cod").attr("disabled",true);
        }
        if($("#recargas").is(":checked")==true){
            validaLitrosOxi();
        }
    });

    $("#id_oc").select2();

    $("#id_oc").on("change",function(){
        loadPorOC();
    });
    $('#search_cod').focus();
    $('#id_sucursal').change(function(event) {
        /* Act on the event */
        var sel = $(this).val();
            if(sel>0){
                showSearch('search_sel');
            }
    });

    document.addEventListener('keydown', (event) => {
        //if (event.ctrlKey) {
            //if (event.keyCode == 66 || event.keyCode == 98) { //B - b
            if (event.altKey && (event.key === 'B' || event.key === 'b')) { 
                //$('.select_option_search .select2-container').hide();
                if($("#recargas").is(":checked")==true){
                    $("#recargas").prop("checked",false);
                    tipoTraslado();
                }
                $('#idproducto').select2('close');
                $('#id_tanque').select2('close');
                showSearch("cont_sel_idp");
                
                $("#search_cod").val("");
                $("#search_cod").focus();
                $('html, body').animate({
                    scrollTop: $(".container-fluid").offset().top
                }, 1000);
                setTimeout(function(){ 
                    console.log('entre ale cierre');
                    $('#id_tanque').select2('close');
                    $('#idproducto').select2('close');
                }, 900);
                
            }
        //}
        if (event.altKey && (event.key === 'R' || event.key === 'r')) { 
            console.log('se ejecuto '+event.key);
            if($("#recargas").is(":checked")==false){
                $("#recargas").prop("checked",true).change();
                setTimeout(function(){ 
                    console.log('entre ale cierre');
                    //$('#id_tanque').select2('close');
                    $('#idproducto').select2('close');
                }, 900);
            }
        }
    }, false);
    ////////////////
    document.addEventListener('keydown', (event) => {
        if (event.ctrlKey) {
            if (event.keyCode == 81 || event.keyCode == 113) {  //Q - q
                if($("#recargas").is(":checked")==true){
                    $("#recargas").prop("checked",false);
                    tipoTraslado();
                }
                $("#search_cod").val("");
                showSearch("search_sel");
                $('html, body').animate({
                    scrollTop: $(".container-fluid").offset().top
                }, 1000);
                setTimeout(function(){ 
                    $('#idproducto').select2('open');
                    $('.select2-search__field').focus();
                    $('#id_tanque').select2('close');
                }, 1500);
            }
        }
    }, false);
    ////////////////////
    document.addEventListener('keydown', (event) => {
        if (event.ctrlKey) {
            if (event.keyCode == 90 || event.keyCode == 122) {
                //console.log("cont_allgral: "+cont_allgral);
                deleteAlls(cont_allgral);
                cont_allgral--;
                //console.log("cont_allgral --: "+cont_allgral);
                //console.log("----------------------");
            }
        }
    }, false);

    /*$(".swal-button--confirm").on("click",function(){
        console.log("click confirm");
        if($("#search_cod").is(":visible")==true){
            $('#search_cod').focus();
        }
    });*/

    document.addEventListener('keydown', (event) => {
        if(event.which == 27) {
            if($("#search_cod").is(":visible")==true){
                $('#search_cod').focus();
            }
        }
        
    }, false);
});

function validaLitrosOxi() {
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/getLitrosSuc",
        data: { origen:$("#id_sucursal_salida option:selected").val(), id_prod:$("#id_tanque option:selected").val()},
        success: function (response){
            $('#id_tanque').html("");
            var array = $.parseJSON(response);
            var stock_disp = Number(array.stock) - Number(array.traslado);
            console.log("array.stock: "+array.stock);
            console.log("array.traslado_stock_cant: "+array.traslado);
            var data_opt = {
                id: array.id,
                text: array.codigo+" ("+stock_disp+" L)"
            };
            var newOption = new Option(data_opt.text, data_opt.id, false, false);
            $('#id_tanque').append(newOption).trigger('change');     
            $("#id_tanque option:selected").attr('data-stock-alm',stock_disp);
            $("#id_tanque option:selected").attr('data-codigo',array.codigo);
        }
    });
}

function validaSucursal() {
    if($("#id_sucursal option:selected").val()==$("#id_sucursal_salida option:selected").val() && $("#id_sucursal option:selected").val()!="0"){
        swal("¡Atención!", "La sucursal que solicita y la sucursal que surte no pueden ser la misma", "error");
        $("#id_sucursal_salida").val(0);
        return;
    }else if($("#id_sucursal option:selected").val()!=$("#id_sucursal_salida option:selected").val() && $("#id_sucursal option:selected").val()!="0"){
        if($("#id_sucursal option:selected").val()!="" && $("#id_sucursal_salida option:selected").val()!=""){
            $('#search_cod').focus(); 
        }
    }
    var suc_destino=$("#id_sucursal option:selected").val();
    var suc_origen=$("#id_sucursal_salida option:selected").val();
    if(suc_destino>0 && suc_origen>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Traspasos/validarsucusalesccp",
            data: {
                origen:suc_origen,
                destino:suc_destino
            },
            success: function (response){
                var array = $.parseJSON(response);
                console.log(array);
                if(array.length==0){
                    swal("¡Atención!", "Las sucursales de origen y fin no se encuentra en los catalogos de carta porte", "error");
                    $('.validar_cerrar_traspaso').prop('disabled',true);
                }else{
                    $('.validar_cerrar_traspaso').prop('disabled',false);
                }
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
    }
}

/*function loadPorOC(){
    $.ajax({
        type: 'POST',
        url: base_url+'Traspasos/searchPorOC',
        data: { id: $("#id_oc option:selected").val() },
        async: false,
        success: function(data) {
            //console.log(data);
            //console.log("id_oc_ant: "+id_oc_ant);
            $(".tr_oc_"+id_oc_ant).remove();
            var array = $.parseJSON(data);
            var cont_prods=0;
            array.forEach(function(element) {
                cont_prods++;
                id_prodg=element.id;
                id_prod_sucg=element.id_prod_suc;
                id_prodserieg=element.id_prod_suc_serie;
                tipo_prodg=element.tipo;
                serieg=element.serie;
                cadg=element.caducidad;
                loteg=element.lote;
                cantidadg=element.cant_compra;
                if(element.tipo==0){ //normal
                    cant_stock=element.stock;
                }else if(element.tipo==1){ //serie
                    cant_stock=element.serie8;
                }else if(element.tipo==2){ //lote
                    cant_stock=element.lote8;
                    cantidadg=element.cant_lote;
                }
                stockg=cant_stock;
                nameprodg=element.nombre;       

                get_producto(8,cant_stock,1,$("#id_oc option:selected").val());
            });
            if(cont_prods==0 && $("#id_oc option:selected").val()!="0"){
                swal("¡Atención!", "Productos de OC dispersados", "error");
            }
        }
    });
    id_oc_ant=$("#id_oc option:selected").val();
}*/
var tipo_input=0;
function showSearch(id){
    if(id=="search_sel"){
        $("#cont_sel_idp").show();
        $("#search_cod").hide();
        tipo_input=1;
        setTimeout(function(){ 
            $('#idproducto').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    }else{
        $("#cont_sel_idp").hide();
        $("#search_cod").show();
        tipo_input=2;
    }
}

function get_recarga() {
    var stockn=Number($("#id_tanque option:selected").data("stock-alm"));
    var cant_lit =Number($("#cant_litros").val());
    if(cant_lit==0){
        swal("¡Atención!", "Ingresa una cantidad a solicitar", "error");
        return false;
    }
    if(( cant_lit % 9500 ) == 0){
        if(stockn>=cant_lit){
            //table_rechange();
            return true;
        }else{
            swal("¡Atención!", "No se cuenta con los litros suficientes", "error");  
            return false;
        }
    }else{
        swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");  
        return false;
    }
}

var add_recarga=0; var cont_all=0;
function table_all_prods(id,producto,cantidad,id_prod,id_prodserie,id_suc,tipo,stock,peligro){
    if($("#id_sucursal option:selected").val()=="0"){
        swal("¡Atención!", 'Seleccione una sucursal de entrada', "error"); 
        return false; 
    }

    if($("#recargas").is(":checked")==true){
        var id =$("#id_tanque option:selected").val();
        var peligro =$("#id_tanque option:selected").data('peli');
        var txt_recha =$("#id_tanque option:selected").text();
        var codigo =$("#id_tanque option:selected").data("codigo");
        var stock_alm =$("#id_tanque option:selected").data("stock-alm");
        var cant_lit =$("#cant_litros").val();
        //console.log("add_recarga table_all_prods: "+add_recarga);
        //console.log("id table_all_prods: "+id);
        //get_recarga();
        if(get_recarga()){
            if(id!=undefined && add_recarga==0){
                var html='<tr class="tr_alls_'+cont_all+' info_m_p producto_'+id+'" data-peli="'+peligro+'">\
                    <td><input type="hidden" id="idproductos" value="'+id+'">\
                        <input type="hidden" id="tipo" value="'+tipo+'">\
                        <input type="hidden" id="idproductos" value="'+id+'">\
                        <input type="hidden" class="cant_stock" value="'+stock_alm+'">\
                        <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
                        <input class="cant_sol_rec cant_'+id+' cantidad_input" type="hidden" id="cantidad" value="'+cant_lit+'">\
                        <input type="hidden" id="id_ordenc" value="0">\
                        '+codigo+'</td>\
                    <td style="text-align:center">'+cant_lit+' L</td>\
                    <td>Recarga de oxígeno</td>\
                    <td></td>\
                    <td>Litros</td>\
                    <td><input type="hidden" id="id_origen" value="'+id_suc+'">'+$("#id_sucursal_salida option:selected").text()+'</td>\
                    <td>'+$("#id_sucursal option:selected").text()+'</td>\
                    <td><a type="button" class="btn btn-pill btn-danger" onclick="deleteAlls('+cont_all+'); deRech('+add_recarga+')"><i class="fa fa-trash icon_font"></i></a></td>\
                </tr>';
                $('#body_prods').append(html);
                $(".btn_add_reche").attr("disabled",true);
                add_recarga++;
                cont_allgral=cont_all;
                cont_all++;
                $("#cant_litros").val("");
            }else{
                swal("¡Atención!", 'Ya existe un registro de solicitud de recarga en tabla "Recargas"', "error");  
            }
        }else{
            return false;
        }
        
    }else{
        var peligro =$("#idproducto").data('peligro');
        if(tipo==0){
            class_tr="tr_s";
            td_txt="";
        }if(tipo==1){ //series
            class_tr="tr_sr";
            td_txt=serieg;
        }if(tipo==2){ //lotes
            class_tr="tr_lo";
            td_txt=loteg+" / "+cadg;
        }
        var html='<tr class="tr_alls_'+cont_all+' '+class_tr+' info_m_p producto_'+id+'" data-peli="'+peligro+'">\
            <td><input type="hidden" id="idproductos" value="'+id+'">\
                <input type="hidden" id="tipo" value="'+tipo+'">\
                <input type="hidden" class="cant_stock" id="cant_stock_'+id+'" value="'+stock+'">\
                <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
                <input class="cant_sol_aux cant_aux_'+id_prod+'_'+id_prodserie+'_'+id+'" type="hidden" id="cantidad_aux" value="'+cantidad+'">\
                <input type="hidden" id="id_ordenc" value="0">\
                '+idProductog+'</td>\
            <td style="text-align:center"><input onchange="validaCant(this.value,'+id+','+stock+','+id_prod+','+id_prodserie+')" max="'+stock+'" class="cant_sol cant_'+id+'_'+id_prod+'_'+id_prodserie+' form-control cantidad_input" type="number" min="1" id="cantidad" value="'+cantidad+'"></td>\
            <td>'+producto+'</td>\
            <td>'+td_txt+'</td>\
            <td>'+nom_unidadg+'</td>\
            <td><input type="hidden" id="id_origen" value="'+id_suc+'">'+$("#id_sucursal_salida option:selected").text()+'</td>\
            <td>'+$("#id_sucursal option:selected").text()+'</td>\
            <td><a type="button" class="btn btn-pill btn-danger" onclick="deleteAlls('+cont_all+')"><i class="fa fa-trash icon_font"></i></a></td>\
        </tr>';
        $('#body_prods').append(html);
        cont_allgral=cont_all;
        cont_all++;
    }
    calculaTotalProds();
}

function deRech(val){
    add_recarga=0;
    console.log("add_recarga: "+add_recarga);
}

function validaCant(val,id,stock,id_prod,id_prodserie){
    //console.log("val: "+val);
    if(Number(val)>Number(stock)){
        swal({
            title: "Álerta",
            text: "Cantidad mayor a la disponible en stock!",
            icon: "warning",
            dangerMode: true,
            showCancelButton: false,
            showConfirmButton: true,
        })
        .then((willDelete) => {
            if (willDelete) {                
                if($("#search_cod").is(":visible")==true){
                    $('#search_cod').focus();
                }
            }
        }); 
        $(".cant_"+id+"_"+id_prod+"_"+id_prodserie).val($(".cant_aux_"+id_prod+"_"+id_prodserie+"_"+id).val());
    }else{
        calculaTotalProds();
    }
}

function calculaTotalProds(){
    var totp=0;
    $(".cant_sol").each(function(){ //total de prods
        totp=totp+Number($(this).val());
    });
    //console.log("totp: "+totp);
    //poner total en total de footer
    $("#tot_prod").html(totp);

    var totr=0;
    $(".cant_sol_rec").each(function(){ //total de litros
        totr=totr+Number($(this).val());
    });
    //console.log("totp: "+totp);
    //poner total en total de footer
    $("#tot_litros").html(totr);
}

function deleteAlls(cont) {
    $('.tr_alls_'+cont).remove();
    if(tipo_input==1){
        setTimeout(function(){ 
            $('#idproducto').select2('open');
            $('.select2-search__field').focus();
        }, 500);
    }else{
        $('#search_cod').focus();
    }
    calculaTotalProds();
    $('html, body').animate({
        scrollTop: $(".container-fluid").offset().top
    }, 1000);
}

function table_rechange(){
    //console.log("table rechange");
    var id =$("#id_tanque option:selected").val();
    var txt_recha =$("#id_tanque option:selected").text();
    var cant_lit =$("#cant_litros").val();
    if(id!=undefined && $("#table_datos_recarga tbody > tr").length==0){
        $(".text_transpasos_recarga").show();
        var html='<tr class="tr_recha">\
            <td><input type="hidden" id="idproductos" value="'+id+'"><input type="hidden" id="cat_reca" value="'+cant_lit+'">'+txt_recha+'</td>\
            <td>Almacén General</td>\
            <td>'+cant_lit+' L</td>\
            <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_rowrec()"><i class="fa fa-trash icon_font"></i></a></td>\
        </tr>';
        $('#body_recarga').append(html);
        $(".btn_add_reche").attr("disabled",true);
    }else{
        swal("¡Atención!", 'Ya existe un registro de solicitud de recarga en tabla "Recargas"', "error");  
    }
}

function save_recarga(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_recarga tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["idproductos"] = $(this).find("input[id*='idproductos']").val();
        item ["cantidad"] = $(this).find("input[id*='cat_reca']").val();
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_recarga tbody > tr").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/guardar_soliciutd_recarga',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function delete_rowrec(){
    $('.tr_recha').remove();
    $(".btn_add_reche").attr("disabled",false);   
}

function tipoTraslado(){
    if($("#recargas").is(":checked")==true){
        $(".cont_search_tras").hide("slow");
        $(".cont_rechange").show("slow");
        setTimeout(function(){ 
            $('#id_tanque').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    }else{
        $(".cont_rechange").hide("slow");
        $(".cont_search_tras").show("slow");
    }
}

function formatStateProds (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + ' ('+state.cant_stock+')</span> <span class="colum_in_3">$' + state.precio_con_ivax + '</span> <span class="colum_in_4">' + state.detalle + '</span> <span class="colum_in_4">' + state.detalle2 + '</span>'
  );
  return $state;
}

function searchCode(barcode){
    $.ajax({
        type: 'POST',
        url: base_url+'Traspasos/searchBarcode',
        data: { cod: barcode, idsuc:$("#id_sucursal_salida option:selected").val() },
        async: false,
        success: function(data) {
            //console.log(data);
            var array = $.parseJSON(data);
            if(array.length>0){
                id_prodg=array[0].id;
                idProductog=array[0].idProducto;
                id_prod_sucg=array[0].id_prod_suc;
                id_prodserieg=array[0].id_prod_suc_serie;
                tipo_prodg=array[0].tipo;
                serieg=array[0].serie;
                cadg=array[0].caducidad;
                loteg=array[0].lote;
                stockg=array[0].stock;
                nameprodg=array[0].nombre;
                nom_unidadg=array[0].nom_unidad;
                //console.log("tipo: "+tipo_prodg);
                //console.log("id_prod_sucg: "+array[0].id_prod_suc);
                //console.log("id_prodserieg: "+array[0].id_prod_suc_serie);
                if(tipo_prodg==0){
                    get_producto(array[0].sucursalid,array[0].stock,1);
                }
                if(tipo_prodg==1 && id_prodserieg==0){
                    getSeriesProd(id_prodg);
                }if(tipo_prodg==1 && id_prodserieg!=0){
                    get_producto(array[0].sucursalid,array[0].stock,1);
                }
                if(tipo_prodg==2 && id_prod_sucg==0){
                    getLotesProd(id_prodg);
                }if(tipo_prodg==2 && id_prod_sucg!=0){
                    get_producto(array[0].sucursalid,array[0].stock,1);
                }
                $("#idproducto").attr('data-peligro',array[0].peligro);
            }
        }
    });
}

function getSeriesProd(id){
    $("#modaldetalles").modal("show");
    $.ajax({
        data:{idproducto:id, id_suc:$("#id_sucursal_salida option:selected").val()},
        type: 'POST',
        async:false,
        url: base_url+'Traspasos/get_series_producto',
        success: function(result){
            $('#det_prods').html(result);
        }
    });   
}

function asignaSerie(ids,serie,id_suc){
    /*var cont_selser=0;
    var TABLA = $("#prod_detalle tbody > .tr_series");
    TABLA.each(function(){
        if($(this).find("input[id*='add_serie']").is(":checked")==true){
            var idserie=$(this).find("input[id*='idserie']").val();
            var sserie=$(this).find("input[id*='sserie']").val();
            //console.log("idserie: "+idserie);
            $('#modal_series').modal('hide');
            cont_selser++;
        }
    });
    if(cont_selser==0){
        swal("Álerta!", "Elija una serie", "warning");
    }*/
    tipo_prodg=1;
    id_prod_sucg=0;
    id_prodserieg=ids;
    stockg=1;
    serieg=serie;
    get_producto(id_suc,1,1);  
    $('#modaldetalles').modal('hide');
}

function getLotesProd(id){
    $("#modaldetalles").modal("show");
    $.ajax({
        data:{idproducto:id, id_suc:$("#id_sucursal_salida option:selected").val(), cant:$("#cantidad").val()},
        type: 'POST',
        async:false,
        url: base_url+'Traspasos/get_lotes_producto',
        success: function(result){
            $('#det_prods').html(result);
        }
    });   
}

function asignaLote(idl,lote,cad,id_suc,stock){
    tipo_prodg=2;
    id_prod_sucg=idl;
    id_prodserieg=0;
    serieg="";
    loteg=lote
    cadg=cad;
    stockg=stock;
    get_producto(id_suc,stock,1);  
    $('#modaldetalles').modal('hide');
}

function search_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        allowClear: true,
        templateResult: formatStateProds,
        ajax: {
            url: base_url+'Traspasos/searchproductosDeta',
            dataType: "json",
            delay:300,
            data: function(params) {
                var query = {
                    search: params.term,
                    idsuc : $("#id_sucursal_salida option:selected").val(),
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var nom_text='';
                    var precio_con_ivax=parseFloat(element.precio_con_ivax).toFixed(2);
                    nom_text=element.nom_text;
                    detalle=element.detalle;
                    detalle2=element.detalle2;
                    /*if(element.incluye_iva==1){
                        precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        if(element.iva==0){
                            incluye_iva=0;
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio*1.16).toFixed(2);
                            sub_iva=parseFloat(element.precio*.16).toFixed(2);
                        }
                    }else{
                        if(element.iva>0){
                            siva=1.16;
                            precio_con_ivax=parseFloat(element.precio/siva).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);  
                        }
                    } */
                    if(element.tipo==0){
                        //nom_text=element.idProducto+' '+element.nombre+' / Cantidad: '+element.stock+" / $"+precio_con_ivax; 
                        //nom_text=element.nom_text; 
                        //detalle="";
                        //detalle2="";
                        cant_stock=element.stock-element.traslado_stock_cant;
                    }else if(element.tipo==1){
                        //nom_text='Serie: '+element.serie+' '+element.nombre+' / Cantidad: '+element.serie8+' / $ '+precio_con_ivax; 
                        //detalle=element.serie;
                        //detalle2="";
                        //cant_stock=element.serie8;
                        cant_stock=1;
                    }else if(element.tipo==2){
                        //nom_text='Lote: '+element.lote+' '+element.nombre+' / Cantidad: '+element.cant_lote+" / $"+precio_con_ivax; 
                        //detalle=element.lote;
                        //detalle2=element.caducidad;
                        cant_stock=element.cant_lote-element.traslado_lote_cant;
                    }

                    if(element.tipo==0 || element.status_ht==2 && element.id_serht==0 && element.tipo==1 || element.status_ht==0 && element.id_serht==0 && element.tipo==1 || element.status_ht==2 && element.id_serht!=0 && element.tipo==1
                       || cant_stock>0 && element.tipo==2)
                        itemscli.push({
                            id: element.id,
                            text: nom_text,
                            nombre: element.nombre,
                            tipo:element.tipo,
                            lote8:element.lote8,
                            serie8:element.serie8,
                            stock:element.stock,
                            id_prod_suc:element.id_prod_suc,
                            id_prod_suc_serie:element.id_prod_suc_serie,
                            incluye_iva:element.incluye_iva,
                            iva:element.iva,
                            idProducto:element.idProducto,
                            precio_con_ivax:precio_con_ivax,
                            detalle:detalle,
                            detalle2:detalle2,
                            cant_stock:cant_stock,
                            sucursalid:element.sucursalid,
                            lote:element.lote,
                            caducidad:element.caducidad,
                            serie:element.serie,
                            peligro:element.peligro
                            //nom_unidad:element.nom_unidad
                        });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
       // get_producto(data.id);
        prod=data.nombre;
        idProductog=data.idProducto;
        //console.log("tipo: "+data.tipo);
        if(data.tipo==0){ //normal
            //cant_stock=data.stock;
            cant_stock=data.cant_stock;
        }else if(data.tipo==1){ //serie
            //cant_stock=1;
            cant_stock=data.cant_stock;
        }else if(data.tipo==2){ //lote
            //cant_stock=data.lote8;
            cant_stock=data.cant_stock;
        }
        $("#idproducto option:selected").attr('data-stock',cant_stock);
        $("#idproducto option:selected").attr('data-tipo',data.tipo);
        $("#idproducto option:selected").attr('data-id_prod_suc',data.id_prod_suc);
        $("#idproducto option:selected").attr('data-id_prod_suc_serie',data.id_prod_suc_serie);
        $("#idproducto option:selected").attr('data-lote',data.lote);
        $("#idproducto option:selected").attr('data-caducidad',data.caducidad);
        $("#idproducto option:selected").attr('data-serie',data.serie);
        //$("#idproducto").attr('data-peligro',data.peligro);
        varificar_material_peligroso(id_prodg);
        if(cant_stock<=0){
            $("#idproducto option:selected").prop('disabled',true);
            //$('#idproducto').find("option").eq(1).attr('disabled','disabled');
        }
        id_prodg=data.id;
        id_prod_sucg=data.id_prod_suc;
        id_prodserieg=data.id_prod_suc_serie;
        tipo_prodg=data.tipo;
        serieg=data.serie;
        cadg=data.caducidad;
        loteg=data.lote;
        //nom_unidadg=data.nom_unidad;
        //console.log("cant_stock: "+cant_stock);
        $.ajax({
            url: base_url+'Traspasos/get_unidad_prod',
            type: 'POST',
            data:{id:id_prodg},
            async:false,
            success: function(response){
                nom_unidadg=response;
                setTimeout(function(){ 
                    get_producto(data.sucursalid,cant_stock,0);
                }, 900);
                
                setTimeout(function(){ 
                    $('#idproducto').select2('open');
                    $('.select2-search__field').focus();
                }, 500);
                setTimeout(function(){ 
                    varificar_material_peligroso(id_prodg);
                }, 1200);
                
            }
        });        
    });
}
function varificar_material_peligroso(id){
    $.ajax({
        url: base_url+'Traspasos/varificar_material_peligroso',
        type: 'POST',
        data:{id:id},
        async:false,
        success: function(response){
            var array = $.parseJSON(response);
            console.log(array);
            $.each(array, function(index, item) {
                $("#idproducto").attr('data-peligro',item.material_peligroso);
                $('.producto_'+item.id).attr('data-peli',item.material_peligroso);
            });
        }
    });        
}
function get_producto(id_suc,cant,searc,oc=0){
    /*console.log("serieg: "+serieg);
    console.log("cadg: "+cadg);
    console.log("loteg: "+loteg);
    console.log("id_prod_sucg: "+id_prod_sucg);*/

    var cantidad = Number($("#cantidad").val());
    if($("#cantidad").val()==""){
        cantidad=1;
    }
    var nombre = prod;
    var band_asigna=0;
    var peligro = $("#idproducto").data('peligro');
    if(searc==0){ //por select2
        var id=$("#idproducto option:selected").val();
        //var stock = Number(cant);
        var stock = Number($("#idproducto option:selected").data("stock"));
        var tipo = Number($("#idproducto option:selected").data("tipo"));
        var id_prod_suc = Number($("#idproducto option:selected").data("id_prod_suc"));
        var id_prod_suc_serie = Number($("#idproducto option:selected").data("id_prod_suc_serie"));
        var lote = $("#idproducto option:selected").data('lote');
        
        /*console.log("id: "+id);
        console.log("stock: "+stock);
        console.log("tipo: "+tipo);
        console.log("id_prod_suc: "+id_prod_suc);
        console.log("id_prod_suc_serie: "+id_prod_suc_serie);
        console.log("lote: "+lote);*/
    }
    else if(searc!=0){
        tipo=tipo_prodg;
        id_prod_suc=id_prod_sucg;
        id_prod_suc_serie=id_prodserieg;
        stock=stockg;
        nombre = nameprodg;
        id=id_prodg;
        if($("#cantidad").val()==""){
            cantidad=1;
        }
    }
    if(oc!=0){
        tipo=tipo_prodg;
        id_prod_suc=id_prod_sucg;
        id_prod_suc_serie=id_prodserieg;
        stock=stockg;
        nombre = nameprodg;
        id=id_prodg;
        cantidad=cantidadg;
    }

    var html="";
    //console.log("stock prod: "+stock);
    //console.log("cantidad: "+cantidad);
    //console.log("tipo: "+tipo)
    if(tipo==1){ //series
        //mostrar series disponibles    
    }
    if(tipo==2){ //lote
        //mostrar lotes disponibles (por fechas mas recientes a caducar)
    }
    if(cantidad==0){
        swal("¡Atención!","Ingreso una cantidad a solicitar", "error");
        return;
    }
    if($("#search_cod").is(":visible")==true && $("#search_cod").val()==""){
        swal("¡Atención!","Indique un producto a trasladar", "error");
        return;
    }
    if($("#cont_sel_idp").is(":visible")==true && $("#idproducto option:selected").val()==""){
        swal("¡Atención!","Indique un producto a trasladar", "error");
        return;
    }

    if(stock<cantidad && cantidad!=null && cantidad!=undefined && cantidad!="" || stock<Number($("#cantidad").val())){
        //swal("¡Atención!","No se cuenta con suficiente stock en almacén", "error");        
        swal({
            title: "¡Atención",
            text: "No se cuenta con suficiente stock en almacén!",
            icon: "warning",
            dangerMode: true,
            showCancelButton: false,
            showConfirmButton: true,
        })
        .then((willDelete) => {
            if (willDelete) {                
                if($("#search_cod").is(":visible")==true){
                    $("#search_cod").val("");
                    $('#search_cod').focus();
                }else{
                    //console.log("es select2");
                    setTimeout(function(){ 
                        $('#idproducto').val(null).trigger('change');
                        $('#idproducto').select2('open');
                        $('.select2-search__field').focus();
                    }, 1000);
                }
            }
        });
        return; 
    }
    //console.log("tipo: "+tipo);
    //VALIDAR QUE NO PERMITE AGREGAR OTRA SERIE NI OTRO LOTE POR EL ID DE PRODUCTO,
    if(tipo==0 && band_asigna==0){
        //$('.text_transpasos_stock').css('display','block');
        var validars=0;
        var tabla_stocks = $("#table_datos_prods tbody > .tr_s");
        tabla_stocks.each(function(){         
            var stock_s = $(this).find("input[id*='idproductos']").val();
            if(stock_s==id){
                validars++;
            }
        });
        if(validars<1){
            //tabla_stock(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc,oc);
            table_all_prods(id_prodg,nombre,cantidad,id_prod_sucg,id_prodserieg,id_suc,tipo,stock,peligro);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('1');
                tipo_prodg=0;
                id_prod_sucg=0;
                id_prodserieg=0;
                stockg=0;
                nameprodg=0;
                id_prodg=0;
                if($("#search_cod").is(":visible")==true){
                    $('#search_cod').focus();
                }
            }, 500);
        }else{
            swal("¡Atención!", "Producto ya existe", "error");    
        }
    }else if(tipo==1 && band_asigna==0){
        //$('.text_transpasos_series').css('display','block');
        var validarsr=0;
        var tabla_seriesr = $("#table_datos_prods tbody > .tr_sr");
        tabla_seriesr.each(function(){         
            var serie_s = $(this).find("input[id*='idproductos']").val();
            var id_ps_tab = $(this).find("input[id*='idproducto_series']").val();
            if(serie_s==id && id_prodserieg==id_ps_tab){
                validarsr++;
            }
        });
        if(validarsr<1){
            //tabla_series(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc,oc);
            table_all_prods(id_prodg,nombre,cantidad,id_prod_sucg,id_prodserieg,id_suc,tipo,stock);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('1');
                tipo_prodg=0;
                id_prod_sucg=0;
                id_prodserieg=0;
                stockg=0;
                nameprodg=0;
                id_prodg=0;
                if($("#search_cod").is(":visible")==true){
                    $('#search_cod').focus();
                }
            }, 500);
        }else{
            //swal("¡Atención!", "La serie ya existe", "error");
            swal({
                title: "Álerta",
                text: "La serie ya existe en el listado!",
                icon: "warning",
                //buttons: false,
                dangerMode: true,
                showCancelButton: false,
                showConfirmButton: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //console.log("click confirm");
                    if($("#search_cod").is(":visible")==true){
                        $('#search_cod').focus();
                    }
                } else {
                    
                }
            });    
        }
    }else if(tipo==2 && band_asigna==0){
        //$('.text_transpasos_lotes').css('display','block');
        var validarlo=0;
        var table_dlotes = $("#table_datos_prods tbody > .tr_lo");
        table_dlotes.each(function(){         
            var lotes_l = $(this).find("input[id*='idproductos']").val();
            var lote_s = $(this).find("input[id*='idproducto_lote']").val();
            /*console.log("lotes_l: "+lotes_l);
            console.log("id_prod_sucg: "+id_prod_sucg);
            console.log("id de comparacion: "+id);*/
            if(lotes_l==id && lote_s==id_prod_sucg){
                validarlo++;
            }
        });
        if(validarlo<1){
            //tabla_lotes(id,nombre,cantidad,id_prod_suc,id_prod_suc_serie,id_suc,oc);
            table_all_prods(id_prodg,nombre,cantidad,id_prod_sucg,id_prodserieg,id_suc,tipo,stock);
            setTimeout(function(){ 
                $('#idproducto').val(null).trigger('change');
                $('#cantidad').val('1');
                tipo_prodg=0;
                id_prod_sucg=0;
                id_prodserieg=0;
                stockg=0;
                nameprodg=0;
                id_prodg=0;
                if($("#search_cod").is(":visible")==true){
                    $('#search_cod').focus();
                }
            }, 500);
        }else{
            swal({
                title: "Álerta",
                text: "Este lote ya existe en el listado!",
                icon: "warning",
                //buttons: false,
                dangerMode: true,
                showCancelButton: false,
                showConfirmButton: true,
                //timer: 2000,
                /*onOpen: function (element) {
                  swal.disableConfirmButton();
                  $(element).on('change', '[type="checkbox"]', function (e) {
                    if ($(this).val()) {
                       swal.enableConfirmButton();
                    } else {
                       swal.disableConfirmButton();
                    }
                  });
                }*/
            })
            .then((willDelete) => {
                if (willDelete) {
                    //console.log("click confirm");
                    if($("#search_cod").is(":visible")==true){
                        $('#search_cod').focus();
                    }
                } else {
                    
                }
            });
        }
    } 

    if(searc!=0){ //por input
        $("#search_cod").val("");
        $("#search_cod").focus();
        $("#search_cod").on("focus");
    }
}

function updateCant(idp,tipo,val){
    if(tipo==0){ //stock normal
        if(Number(val)>Number($(".cants_"+idp).val()) ){
            swal("¡Atención!", "La cantidad ingresada es mayor que la compra", "error"); 
            $(".cant_ssol_"+idp).val($(".cants_"+idp).val());
            return false;  
        }
    }if(tipo==2){ //stock de lote
        if(Number(val)>Number($(".cantl_"+idp).val()) ){
            swal("¡Atención!", "La cantidad ingresada es mayor que la compra", "error"); 
            $(".cant_lsol_"+idp).val($(".cantl_"+idp).val());
            return false;  
        }
    }
}

var cont_s=0;
function tabla_stock(id,producto,cantidad,id_prod,id_prodserie,id_suc,oc){
    //array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Renta","Almacén General"];
    var class_oc=""; txt_cant=cantidad;
    if(oc!=0){
        class_oc="tr_oc_"+oc;
        txt_cant='<input type="number" id="cant_tras" class="form-control cant_'+cont_s+' cant_ssol_'+id+'" value="'+cantidad+'" onchange="updateCant('+id+',0,this.value)">';
    }
    var html='<tr class="tr_s row_s_'+cont_s+' '+class_oc+'">\
        <td><input type="hidden" id="idproductos" value="'+id+'">\
            <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
            <input class="cants_'+id+'" type="hidden" id="cantidads" value="'+cantidad+'">\
            <input type="hidden" id="id_ordenc" value="'+oc+'">'+producto+'</td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">Almacén General</td>\
        <td>'+txt_cant+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_stock('+cont_s+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_stock').append(html);
    cont_s++;
}

function delete_stock(cont){
    $('.row_s_'+cont).remove();    
}

var cont_sr=0;
function tabla_series(id,producto,cantidad,id_prod,id_prodserie,id_suc,oc){
    //array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General"];
    var class_oc="";
    if(oc!=0){
        class_oc="tr_oc_"+oc;
    }
    var html='<tr class="tr_sr row_sr_'+cont_sr+' '+class_oc+'">\
        <td><input type="hidden" id="idproductosr" value="'+id+'">\
        <input type="hidden" id="idproducto_series" value="'+id_prodserieg+'"><input type="hidden" id="idproducto_lote" value="'+id_prod+'">\
        <input type="hidden" id="cantidadsr" class="cantidadsr_'+cont_sr+'" value="1">\
        <input type="hidden" id="id_ordenc" value="'+oc+'">'+producto+'\
           <!--<table class="table table-sm table_datos_series_detalles" id="table_datos_series_detalles_'+cont_sr+'">\
                <thead>\
                    <tr>\
                        <th scope="col">Serie</th>\
                        <th scope="col"></th>\
                    </tr>\
                </thead>\
                <tbody>\
                </tbody>\
            </table>-->\
        </td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">Almacén General</td>\
        <td>1</td>\
        <td>'+serieg+'</td>\
        <td><button type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+cont_sr+')"><i class="fa fa-trash icon_font"></i></button></td>\
        <!--<td><div class="select_txt_serie_'+cont_sr+'"></div></td>\
        <td><a class="btn btn-pill btn-primary" type="button" onclick="add_series_tabla('+cont_sr+')"><i class="fa fa-plus"></i></a><a type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+cont_sr+')"><i class="fa fa-trash icon_font"></i></a></td>-->\
    </tr>';
    /*html+='<tr class="row_sr_d_'+cont_sr+'">\
        <td colspan="4">\
            <div class="tabla_txt_serie_'+cont_sr+'">\
                <table class="table table-sm table_datos_series_detalles" id="table_datos_series_detalles_'+cont_sr+'">\
                    <thead>\
                        <tr>\
                            <th scope="col">Serie</th>\
                            <th scope="col"></th>\
                        </tr>\
                    </thead>\
                    <tbody>\
                    </tbody>\
                </table>\
            </div>\
        </td>\
    </tr>';*/
    $('#table_datos_series').append(html);
    //get_select_serie(cont_sr,id);
    cont_sr++;
}

function get_select_serie(cont_sr,idproducto){
    $.ajax({
        data:{idproducto:idproducto,cont:cont_sr},
        type: 'POST',
        url: base_url+'Traspasos/get_productos_series',
        success: function(result){
            $('.select_txt_serie_'+cont_sr).html(result);
        }
    });
}

function delete_serie(cont){
    $('.row_sr_'+cont).remove();
    //$('.row_sr_d_'+cont).remove();    
}

/*function add_series_tabla(cont){
    var idreg=$('#series_s_'+cont+' option:selected').val();
    var reg_text=$('#series_s_'+cont+' option:selected').text();
    if(idreg!=0){
        var validars=0;
        var tabla_serie = $("#table_datos_series_detalles_"+cont+" tbody > .tr_s"+cont);
        tabla_serie.each(function(){         
            var serie_x = $(this).find("input[id*='idserie_s']").val();
            if(serie_x==idreg){
                validars++;
            }
        });
        var num_serie=$("#table_datos_series_detalles_"+cont+" tbody > .tr_s"+cont).length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidadsr_'+cont).val();

        var cs=parseFloat(cantidad_serie);
        if(ns<cs){
            if(validars<1){
                tabla_series_detalle(idreg,reg_text,cont);
            }else{
                swal("¡Atención!", "La serie ya existe", "error");    
            }
        }else{
            swal("¡Atención!", "Solo puedes agregar "+cantidad_serie+"", "error");    
        }
            
    }else{
        swal("¡Atención!", "Falta seleccionar una serie", "error"); 
    }
}

var cont_srd=0;
function tabla_series_detalle(idreg,reg_text,cont){
    var html='<tr class="tr_srd tr_s'+cont+' row_srd_'+cont_srd+'">\
        <td><input type="hidden" id="idserie_s" value="'+idreg+'">'+reg_text+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_serie_detalle('+cont_srd+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_series_detalles_'+cont).append(html);
    cont_srd++; 
}

function delete_serie_detalle(cont){
    $('.row_srd_'+cont).remove();    
}*/

/////////////////////////////////////////////// LOTES

var cont_lo=0;
function tabla_lotes(id,producto,cantidad,id_prod,id_prodserie,id_suc,oc){
    //array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General"];
    var class_oc="";
    txt_cant=cantidad;
    if(oc!=0){
        class_oc="tr_oc_"+oc;
        txt_cant='<input type="number" id="cant_tras" class="form-control cant_'+cont_lo+' cant_lsol_'+id+'" value="'+cantidad+'" onchange="updateCant('+id+',2,this.value)">';
    }
    
    var html='<tr class="tr_lo row_lo_'+cont_lo+' '+class_oc+'">\
        <td><input type="hidden" id="idproductolo" value="'+id+'">\
            <input type="hidden" id="idproducto_series" value="'+id_prodserie+'"><input type="hidden" id="idproducto_lote" value="'+id_prod_sucg+'">\
            <input class="cantl_'+id+'" type="hidden" id="cantidadlo" class="cantidadlo_'+cont_lo+'" value="'+cantidad+'">\
            <input type="hidden" id="id_ordenc" value="'+oc+'">'+producto+'\
            <!--<table class="table table-sm table_datos_lotes_detalles" id="table_datos_lotes_detalles_'+cont_lo+'">\
                <thead>\
                    <tr>\
                        <th colspan="2" scope="col">Lotes</th>\
                        <th scope="col">Cantidad</th>\
                        <th scope="col"></th>\
                    </tr>\
                </thead>\
                <tbody>\
                </tbody>\
            </table>-->\
        </td>\
        <td><input type="hidden" id="id_origen" value="'+id_suc+'">Almacén General</td>\
        <td>'+txt_cant+'</td>\
        <td>'+loteg+'</td>\
        <td>'+cadg+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote('+cont_lo+')"><i class="fa fa-trash icon_font"></i></a></td>\
        <!--<td><div class="select_txt_lotes_'+cont_lo+'"></div></td>\
        <td><input type"text" class="form-control cantidadlo_d'+cont_lo+'"></td>\
        <td><a class="btn btn-pill btn-primary" type="button" onclick="add_lotes_tabla('+cont_lo+')"><i class="fa fa-plus"></i></a><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote('+cont_lo+')"><i class="fa fa-trash icon_font"></i></a></td>-->\
    </tr>';
    $('#table_datos_lotes').append(html);
    //get_select_lote(cont_lo,id);
    cont_lo++;
}

/*function get_select_lote(cont,idproducto){
    $.ajax({
        data:{idproducto:idproducto,cont:cont},
        type: 'POST',
        url: base_url+'Traspasos/get_productos_lote',
        success: function(result){
            $('.select_txt_lotes_'+cont).html(result);
        }
    });
}*/

function delete_lote(cont){
    $('.row_lo_'+cont).remove();
    //$('.row_lo_d_'+cont).remove();    
}

/*function add_lotes_tabla(cont){
    var idreg=$('#lote_l_'+cont+' option:selected').val();
    var cantidad_lote=$('#lote_l_'+cont+' option:selected').data('cantidad');
    var reg_text=$('#lote_l_'+cont+' option:selected').data('lotetxt');
    var cant_lo=parseFloat(cantidad_lote);
    var cantidad=$('.cantidadlo_d'+cont).val();
    var cant=parseFloat(cantidad);
    if(idreg!=0){
        if(cantidad_lote>=cant){
            var validars=0;
            var tabla_lote = $("#table_datos_lotes_detalles_"+cont+" tbody > .row_lo_d"+cont);
            var cantida_x_lote=0;
            tabla_lote.each(function(){         
                var lote_x = $(this).find("input[id*='idlote_l']").val();
                var lote_num_x = $(this).find("input[id*='cantidad_l']").val();
                cantida_x_lote+=parseFloat(lote_num_x);
                if(lote_x==idreg){
                    validars++;
                }
                
            });
            var suma=parseFloat(cantidad)+parseFloat(cantida_x_lote);
            var nl=parseFloat(suma);
            var cantidad_lote_pro=$('.cantidadlo_'+cont).val();
            var cs=parseFloat(cantidad_lote_pro);
            var resta=cs-cantida_x_lote;
            if(nl<=cs){
                if(validars<1){
                    tabla_lotes_detalle(idreg,reg_text,cantidad,cont);
                    $("#lote_l_0").val(0);
                    $('.cantidadlo_d'+cont).val('');
                }else{
                    swal("¡Atención!", "El lote ya existe", "error");    
                }
            }else{
                swal("¡Atención!", "Solo puedes agregar "+resta+"", "error");    
            }  
        }else{
            swal("¡Atención!", "El lote no cuenta con cantidad suficiente", "error"); 
        } 
    }else{
        swal("¡Atención!", "Falta seleccionar un lote", "error"); 
    }
}

var cont_lo_d=0;
function tabla_lotes_detalle(idreg,reg_text,cantidad,cont){
    var html='<tr class="tr_lod row_lo_d'+cont_lo_d+'">\
        <td colspan="2"><input type="hidden" id="idlote_l" value="'+idreg+'">'+reg_text+'</td>\
        <td><input type="hidden" id="cantidad_l" value="'+cantidad+'">'+cantidad+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote_detalle('+cont_lo_d+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $("#table_datos_lotes_detalles_"+cont).append(html);
    cont_lo_d++; 
}

function delete_lote_detalle(cont){
    $('.row_lo_d'+cont).remove();    
}*/

function validar_cerrar_traspaso(){
    var vpeli = 0;
    $(".info_m_p").each(function() {
        var vvp = $(this).data('peli');
        //console.log(vvp);
        if(vvp!='0'){
            vpeli=1;
        }
    });
    //console.log('vpeli:'+vpeli);
    if(vpeli==1){
        //swal("Material peligroso", 'Se detecto un producto como material peligroso. Se generará únicamente el formato de carta porte sin el timbre', "error"); 
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Material peligroso',
            content: 'Se detecto un producto como material peligroso. Se generará únicamente el formato de carta porte sin el timbre',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    
                    
                }
            }
        });
    }
    /*if($("#table_datos_recarga tbody > tr").length==0 && $("#table_datos_stock tbody > tr").length==0 && $("#table_datos_series tbody > tr").length==0 && $("#table_datos_lotes tbody > tr").length==0){
        swal("¡Atención!", 'Ingrese al menos un producto a solicitar', "error");
        return;
    }*/
    if($("#table_datos_prods tbody > tr").length==0 ){
        swal("¡Atención!", 'Ingrese al menos un producto a trasladar', "error");
        return;
    }
    if($('#id_sucursal option:selected').val()!="0"){
        var carta_porte=0;
        //if($("#carta_porte").is(":checked")==true){
            //carta_porte=1;
        //}
        $.ajax({
        type:'POST',
        url: base_url+"Traspasos/reg_soli_tras_almacen",
        data: $('#form_registro').serialize()+"&carta_porte="+carta_porte,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        beforeSend: function(){
            $('.btn_transitoy').attr('disabled',true);
        },
        success:function(data){
            ////////////
            swal("Éxito!", "Guardando por favor espere...", "success");
            var idreg=parseFloat(data);
            //console.log('Id traspaso: '+idreg);
            save_solicitud_all(idreg);
            /*if($("#table_datos_recarga tbody > tr").length>0){
                save_recarga(idreg);
            }
            if($("#table_datos_stock tbody > tr").length>0){
                solicitud_stock(idreg);
            }
            if($("#table_datos_series tbody > tr").length>0){
                solicitud_serie(idreg);
            }
            if($("#table_datos_lotes tbody > tr").length>0){
                solicitud_lote(idreg);
            }*/
            setTimeout(function(){ 
                envia_mail(idreg);
            }, 2000); 
            
            setTimeout(function(){ 
                //swal("Éxito!", "Guardado Correctamente", "success");
                //window.location.href = base_url+"Traspasos";
                $('body').loading({theme: 'dark',message: 'Generando carta porte...'});
                generarcartaporte(idreg);//xxx solo comentado para las pruebas
            }, 3000); 
            ////////////
        }
    });
    }else{
        swal("¡Atención!", 'Elije una sucursal solicitante', "error"); 
    }
    
}
function generarcartaporte(idtraspaso){
    $.ajax({
        type:'POST',
        url: base_url+"Timbrado/generafacturarcartaporte/"+idtraspaso,
        data: { id:0 },
        success:function(response){
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                /*
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError,
                              type: "warning",
                              showCancelButton: false
                 });
                */
                //retimbrar(array.facturaId,0);
            }else{
                    var textofactura='Guardado Correctamente';
                
                swal({
                    title:"Éxito!", 
                    text:'Guardado Correctamente', 
                    type:"success"
                    });
                
            }
            setTimeout(function(){ 
                    window.location.href = base_url+"Traspasos";
                }, 2000); 
        }
    });
}
function envia_mail(id){
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/mail_solicitud",
        data: { id:id },
        success:function(data){

        }
    });
}

function save_solicitud_all(id){
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_prods tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductos']").val();
        //item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
        item ["cantidad"] = $(this).find("input[class*='cantidad_input']").val();// se cambio de esta forma por que la cantidad no estaba jalando la correcta si no que se jalaba la informacion de cantidad_aux
        item ["id_ordenc"] = $(this).find("input[id*='id_ordenc']").val();
        item ["tipo"] = $(this).find("input[id*='tipo']").val();
        item ["cant_ini"] = $(this).find("input[class*='cant_stock']").val();
        var DATA_dl = [];
        item_dl = {};
        if($(this).find("input[id*='tipo']").val()==2){
            item_dl ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
            item_dl ["cantidad"] = $(this).find("input[id*='cantidad']").val();
            DATA_dl.push(item_dl);
            item ["lote_detalles"] = DATA_dl;
        }

        var DATA_ds = [];
        item_ds = {};
        if($(this).find("input[id*='tipo']").val()==1){
            item_ds ["idseries"] = $(this).find("input[id*='idproducto_series']").val();
            DATA_ds.push(item_ds);
            item ["serie_detalles"] = DATA_ds;
        }
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    var num_stock=$("#table_datos_prods tbody > tr").length; 
    var num_stock_n=parseFloat(num_stock);
    //console.log('num_stock_n:'+num_stock_n);
    if(num_stock_n>0){
        console.log('INFO:'+INFO);
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_all_producto_alm',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitud_stock(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_stock tbody > .tr_s");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductos']").val();
        //item ["cantidad"] = $(this).find("input[id*='cantidads']").val();
        item ["cantidad"] = $(this).find("input[id*='cant_tras']").val();
        item ["id_ordenc"] = $(this).find("input[id*='id_ordenc']").val();
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    var num_stock=$("#table_datos_stock tbody > .tr_s").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitud_serie(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_series tbody > .tr_sr");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductosr']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadsr']").val();
        item ["id_ordenc"] = $(this).find("input[id*='id_ordenc']").val();
        var DATA_d  = [];
        //var TABLA_d = $(this).find('.table_datos_series_detalles tbody > .tr_srd');
        //var TABLA_d   = $("#table_datos_series tbody > .tr_sr");
        //TABLA_d.each(function(){ 
            item_d = {};
            //item_d ["idseries"] = $(this).find("input[id*='idserie_s']").val();
            item_d ["idseries"] = $(this).find("input[id*='idproducto_series']").val();
            DATA_d.push(item_d);
        //});
        item ["serie_detalles"]   = DATA_d;
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    var num_stock=$("#table_datos_series tbody > .tr_sr").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto_series_alm',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitud_lote(id){
    //var sucursal=$('#sucursal option:selected').val();
    var sucursal=$('#id_sucursal').val();
    var DATA  = [];
    var TABLA   = $("#table_datos_lotes tbody > .tr_lo");
    TABLA.each(function(){         
        item = {};
        item ["idtraspasos"] = id;
        item ["sucursal"] = sucursal;
        item ["id_origen"] = $(this).find("input[id*='id_origen']").val();
        item ["idproductos"] = $(this).find("input[id*='idproductolo']").val();
        //item ["cantidad"] = $(this).find("input[id*='cantidadlo']").val();
        item ["cantidad"] = $(this).find("input[id*='cant_tras']").val();
        item ["id_ordenc"] = $(this).find("input[id*='id_ordenc']").val();
        var DATA_d  = [];
        //var TABLA_d = $(this).find('.table_datos_lotes_detalles tbody > .tr_lod');
        //var TABLA_d = $('#table_datos_lotes tbody > .tr_lo');
        //TABLA_d.each(function(){ 
            item_d = {};
            //console.log("idlote_l: "+$(this).find("input[id*='idproducto_lote']").val());
            //item_d ["idlote"] = $(this).find("input[id*='idlote_l']").val();
            //item_d ["cantidad"] = $(this).find("input[id*='cantidad_l']").val();
            item_d ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
            //item_d ["cantidad"] = $(this).find("input[id*='cantidadlo']").val();
            item_d ["cantidad"] = $(this).find("input[id*='cant_tras']").val();
            DATA_d.push(item_d);
        //});
        item ["lote_detalles"] = DATA_d;
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    console.log("aInfo: "+aInfo);
    var num_stock=$("#table_datos_lotes tbody > .tr_lo").length; 
    var num_stock_n=parseFloat(num_stock);
    if(num_stock_n>0){
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Traspasos/registro_solicitud_traspaso_producto_lote_alm',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                },
                500: function(){
                }
            },
            success: function(data){
            }
        }); 
    }
}

function solicitar_traspasovvvv(id,tipo,cont){
    $('.btn_transito_'+cont).attr('disabled',true);
    if(tipo==1){
        var num_serie=$("#table_datos_traspasos tbody > .tr_sx").length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidad_serie_'+cont).val();
        var cs=parseFloat(cantidad_serie);
        if(ns==cs){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos tbody > .tr_sx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idseries"] = $(this).find("input[id*='idseriex']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar series", "error");
        }
    }else if(tipo==2){
        var num_lote=$("#table_datos_traspasos_lotes tbody > .tr_lx").length; 
        var nl=parseFloat(num_lote);
        if(nl!=0){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos_lotes tbody > .tr_lx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idlotes"] = $(this).find("input[id*='idlotex']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
                item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
                item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos_lotes',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar lote", "error");
        }
    }else{
        swal("Éxito!", "Guardado Correctamente", "success");
        /*$.ajax({
            type:'POST',
            url: base_url+"Traspasos/update_record_transpaso_transito",
            data:{id:id},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    tabla.ajax.reload(); 
                    $('#modaldetalles_apartado').modal('hide');
                    $('.btn_transito').attr('disabled',false);
                }, 1000); 
            }
        }); */
    }
    
}
