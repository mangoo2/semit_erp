var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Solicitudesrentas/getlistado",
            type: "post",
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data":"correo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                    if(row.productos==1){
                        html='Camas';
                    }else if(row.productos==2){
                        html='Barandales';
                    }else if(row.productos==3){
                        html='Gruas';
                    }else if(row.productos==4){
                        html='Trapecios';
                    }else if(row.productos==5){
                        html='Marcas ortopédicas';
                    }else if(row.productos==6){
                        html='Concentradores de oxígeno';
                    }else if(row.productos==7){
                        html='Sillas de ruedas';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<span class="badge pill-badge-danger m-l-10">Pendiente</span>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}