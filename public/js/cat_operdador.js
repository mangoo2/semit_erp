var base_url =$('#base_url').val();
var table_op;
$(document).ready(function($) {
	table_op=$('#table_operador').DataTable();
	loadtable_op();
	$('#btn_save_op').click(function(event) {
		$( "#btn_save_op" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#btn_save_op" ).prop( "disabled", false );
        }, 5000);
		btn_save_op();
	});
	$('#TipoFigura').change(function(event) {
		var tf = $('#TipoFigura option:selected').val();
		if(tf=='01'){
			$('.solooperador').show();
		}else{
			$('.solooperador').hide();
			$('#no_licencia').val('');
		}
	});
});
function loadtable_op(){
	var fig = $('#tipfig option:selected').val();
	table_op.destroy();
	table_op=$("#table_operador").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_op",
		        type: "post",
		        "data": {
	                'fig':fig
	            },
		        error: function(){
		           //$("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"id"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		            		var html='';
		            	if(row.TipoFigura=='01'){
		            		html='Operador';
		            	}
		            	if(row.TipoFigura=='02'){
		            		html='Propietario';
		            	}
		            	if(row.TipoFigura=='03'){
		            		html='Arrendatario';
		            	}
		            	if(row.TipoFigura=='04'){
		            		html='Notificado';
		            	}
		            	return html;
		            }
		        },
		        {"data":"rfc_del_operador"},
				{"data":"no_licencia"},
				{"data":"operador"},
				{"data":"num_identificacion"},
				{"data":"residencia_fiscal"},
				{"data":"calle"},
				{"data":"descripcion"},
				{"data":"pais"},
				{"data":"codigo_postal"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a  onClick="add_operador('+row.id+')" type="button" class="btn btn-success mr-1 mb-1 edit_op'+row.id+'" ';
		                    html+=' data-fig="'+row.TipoFigura+'" ';
		                    html+=' data-rfc="'+row.rfc_del_operador+'" ';
		                    html+=' data-lic="'+row.no_licencia+'" ';
		                    html+=' data-op="'+row.operador+'" ';
		                    html+=' data-niden="'+row.num_identificacion+'" ';
		                    html+=' data-rf="'+row.residencia_fiscal+'" ';
		                    html+=' data-calle="'+row.calle+'" ';
		                    html+=' data-estado="'+row.estado+'" ';
		                    html+=' data-cp="'+row.codigo_postal+'" ';

		                    html+=' data-ne="'+row.num_ext+'" ';
		                    html+=' data-ni="'+row.num_int+'" ';

		                    html+=' data-colo="'+row.colo+'" ';
		                    html+=' data-loca="'+row.loca+'" ';
		                    html+=' data-ref="'+row.ref+'" ';
		                    html+=' data-muni="'+row.muni+'" ';

		                    html+='><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "desc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        if(fig=='01'){
        	table_op.column(3).visible(true);
        }else{
        	table_op.column(3).visible(false);
        }
   	});
}
function eliminar_reg(id_reg){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma eliminar registro',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cat_complemento/delete_op",
                        data: {
                            id:id_reg
                        },
                        success: function (response){  
							toastr.success('Registro eliminado');     
							loadtable_op();            
                        },
                        error: function(response){
                            toastr["error"]("No se pudo procesar", "Error");  
                        }
                    });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function add_operador(id){
	if(id>0){
		$('#modal_add_op .title').html('Editar Figura');
		$('#btn_save_op').html('Editar');
		var rfc =$('.edit_op'+id).data('rfc');
		var lic =$('.edit_op'+id).data('lic');
		var op =$('.edit_op'+id).data('op');
		var niden =$('.edit_op'+id).data('niden');
		var rf =$('.edit_op'+id).data('rf');
		var calle =$('.edit_op'+id).data('calle');
		var estado =$('.edit_op'+id).data('estado');
		var cp =$('.edit_op'+id).data('cp');

		var ne =$('.edit_op'+id).data('ne');
		var ni =$('.edit_op'+id).data('ni');

		var colo =$('.edit_op'+id).data('colo');
		var loca =$('.edit_op'+id).data('loca');
		var ref =$('.edit_op'+id).data('ref');
		var muni =$('.edit_op'+id).data('muni');
		var fig =$('.edit_op'+id).data('fig');

		$('#id_op').val(id);
		$('#rfc_del_operador').val(rfc);
		$('#no_licencia').val(lic);
		$('#operador').val(op);
		$('#num_identificacion').val(niden);
		$('#residencia_fiscal').val(rf);
		$('#calle').val(calle);
		$('#estado').val(estado);
		$('#pais').val('MEX');
		$('#codigo_postal').val(cp);

		$('#num_ext').val(ne);
		$('#num_int').val(ni);

		$('#colo').val(colo);
		$('#loca').val(loca);
		$('#ref').val(ref);
		$('#muni').val(muni);
		$('#TipoFigura').val(fig);
	}else{
		$('#modal_add_op .title').html('Agregar Figura');
		$('#btn_save_op').html('Agregar');
	}

	$('#modal_add_op').modal('show');
}
function btn_save_op(){

	var form = $('#form_ope');
	if(form.valid()){
		var save_fig=$('#TipoFigura option:selected').val();
		$('#tipfig').val(save_fig);

		var datos = form.serialize();
		$.ajax({
        type:'POST',
        url: base_url+"Cat_complemento/save_op",
        data: datos,
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('404');
            },
            500: function(data){
            	console.log(data);
                toastr.error('500');
            }
        },
        success: function (data){
        	toastr.success('Operador Agregado');
        	loadtable_op();
        	$('#modal_add_op').modal('hide');
        	form[0].reset();
        }
    });
	}else{
		toastr.error('Favor de completar los datos requeridos');
	}
}