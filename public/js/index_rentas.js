var table;
var base_url=$('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {	
    tabla();
    $("#addpay").on("click",function(){
        addPay();
    });

    $("#folio").on("change",function(){
        saveFolio();
    });

    $(".save_contra").on("click",function(){
        save_datos_pdf();
    });

});

function saveFolio(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de actualizar el folio fisico del contrato?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Rentas/updateFolio",
                    data:{id:$("#idrc").val(), folio:$("#folio").val()},
                    success:function(data){
                        swal("Éxito!", "Actualizado correctamente", "success");
                    }
                });                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "ajax": {
            "url": base_url+"Rentas/getData_rentas",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "descripcion"},
            {"data": "serie"},
            {"data": null,
                render:function(data,type,row){
                    var html=row.fecha_inicio+ ' '+row.hora_inicio;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html=row.fecha_fin+ ' '+row.hora_fin;
                    return html;
                }
            },
            {"data": "name_suc"},
            {"data": null,
                render:function(data,type,row){
                    var total=Number(row.total);
                    total = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(parseFloat(total).toFixed(2));
                    return total;
                }
            },
            /*{"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.requiere_factura==1){
                        html="<span class='btn btn-primary'>Requiere</span>";
                    }if(row.requiere_factura==0){
                        html="<span class='btn btn-danger'>No requiere</span>";
                    }
                    return html;
                }
            }, */
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==0 || row.pagado==0){
                        html="<span class='btn btn-danger'>Pendiente de pago</span>";
                    }
                    if(row.estatus==1){
                        html="<span class='btn btn-success'>Activo</span>";
                    }if(row.estatus==2){
                        html="<span class='btn btn-primary'>Finalizado</span>";
                    }if($("#tipo_tec").val()=="1" && row.estatus==2 && row.id_renta!=0 && row.estatus_mtto=="1"){
                        html+=" <span class='btn btn-primary'>En mtto</span>";
                    }if($("#tipo_tec").val()=="1" && row.estatus==2 && row.id_renta!=0 && row.estatus_mtto=="2"){
                        html+=" <span class='btn btn-success'>Mtto. terminado</span>";
                    }if($("#tipo_tec").val()=="1" && row.estatus==2 && row.id_renta!=0 && row.estatus_mtto=="3"){
                        html+=" <span class='btn btn-danger'>Sin reparación</span>";
                    }
                    if(row.estatus==3){
                        html="<span class='btn btn-danger'>Cancelado</span>";
                    }
                    if(row.renovacion==1){
                        html="<span title='Ver Renovaciones' onclick='verRenova("+row.id+")' class='btn btn-success'>Renovado</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';            
                    if(row.pagado=="1"){
                        html+='<a title="Generar Contrato" style="cursor:pointer" onclick="get_modal_pdf('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    }
                    if(row.estatus!=3){
                        if($("#perfil").val()=="1" || $("#tipo_tec").val()!="1"){
                            html+='<a title="Pagos Contrato" style="cursor:pointer" onclick="pay_contra('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pay.svg"></a>';
                        }
                    }
                    if(row.estatus==1 && $("#perfil").val()=="1" || row.estatus==1 && $("#tipo_tec").val()=="1"){
                        html+='<a title="Finalizar Contrato" onclick="change_estatus('+row.id+',2,'+row.id_serie+','+row.id_prod+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/bill-invoice.svg"></a>';
                    }
                    if(row.pagado==0 && row.renovacion!=1 && $("#tipo_tec").val()!="1" && row.estatus!=3){
                        html+='<a title="Cancelar Contrato" onclick="change_estatus('+row.id+',3,'+row.id_serie+','+row.id_prod+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }if(row.estatus==0 && $("#tipo_tec").val()!="1" || row.estatus==1 && $("#tipo_tec").val()!="1"){
                        html+='<a title="Editar" href="'+base_url+'Rentas/alta/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        //html+='<a title="Cancelar Contrato" onclick="change_estatus('+row.id+',3,'+row.id_serie+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }if(row.estatus==2 && $("#tipo_tec").val()!="1"){
                        html+='<a title="Renovar Contrato" onclick="change_estatus('+row.id+',1,'+row.id_serie+','+row.id_prod+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/duplicate.svg"></a>';
                    }
                    if(row.estatus==2 && $("#tipo_tec").val()=="1"){
                        html+='<a title="Realizar Mantenimiento" href="'+base_url+'Mttos_internos/registrarMtto/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/ventasp/66.png"></a>';
                    }
                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        columnDefs: [ { orderable: false, targets: [4,6,7] }],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function pay_contra(id){
    $("#modal_pays").modal("show");
    $("#id_contrap").html(id);
    $("#idrc").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Rentas/getPagos",
        data:{id:id},
        success:function(data){
            var array=$.parseJSON(data);
            $("#name_clip").html(array.nombre);
            $("#name_servp").html(array.descripcion);
            $("#num_seriep").html(array.serie);
            $("#det_pays").html(array.html);
            $("#tot_renta").html(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.total));
            if(array.pagado==1){
                $("#status_renta").html('<h3 style="color: #1770aa; text-align: center;" id="status_renta">Renta con registro de pago, ticket de venta # <a target="_blank" href="'+base_url+'Ventasp/ticket/'+array.id_venta+'">'+array.id_venta+'</a></h3>');
                $("#folio").val(array.folio);
            }else{
                $("#status_renta").html('<h3 style="color: #1770aa; text-align: center;" id="status_renta">Renta sin pago, generar pago en pantalla de <a target="_blank" href="'+base_url+'Ventasp?idr='+id+'">ventas</a></h3>');
                //$("#status_renta").html('<h3 style="color: #1770aa; text-align: center;" id="status_renta">Renta sin pago, generar pago en pantalla de ventas</h3>')
                $("#cont_folio").html('');
            }
        }
    });
}

var contpay=0;
function addPay(){
    metodo="";
    if($("#periodo option:selected").val()=="1")
        metodo="Efectivo";
    if($("#periodo option:selected").val()=="2")
        metodo="Tarjeta de Crédito";
    if($("#periodo option:selected").val()=="3")
        metodo="Tarjeta de Débito";
    if($("#periodo option:selected").val()=="4")
        metodo="Transferencia";
    if($("#periodo option:selected").val()=="4")
        metodo="Cheque";
    
    if($('#monto').val()==""){
        swal("Álerta!", "Ingrese un monto", "warning");
        return false;
    }if($('#fecha').val()==""){
        swal("Álerta!", "Ingrese una fecha", "warning");
        return false;
    }if($('#folio').val()==""){
        swal("Álerta!", "Ingrese un folio o referencia", "warning");
        return false;
    }

    var html="<tr class='pay_row_"+contpay+"'>\
        <td>1</td>\
        <td>"+$('#fecha').val()+"</td>\
        <td>"+$('#monto').val()+"</td>\
        <td>"+$('#folio').val()+"</td>\
        <td>"+metodo+"</td>\
        <td><button type='button' onclick='deletePay("+contpay+")' class='btn btn-danger'><i class='fa fa-trash' aria-hidden='true'></i></button></td>\
    </tr>";
    $("#det_pays").append(html);
    contpay++;
    $('#fecha').val("");
    $('#monto').val("");
    $('#folio').val("");
}

function deletePay(contpay){
    $(".pay_row_"+contpay).remove();
}

function change_estatus(id,estatus,id_serie,id_prod){
    if(estatus==2){ //finalizar
        txt ="¿Está seguro de finalizar el contrato?"
    }if(estatus==3){ //finalizar
        txt ="¿Está seguro de cancelar el contrato?"
    }if(estatus==1){ //renovar
        txt ="¿Está seguro de renovar el contrato?"
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: txt,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Rentas/updateEstatusContrato",
                    data:{id:id,estatus:estatus,id_serie:id_serie, id_prod:id_prod },
                    success:function(data){
                        if(estatus!=1){
                            swal("Éxito!", "Actualizado correctamente", "success");
                        }else{
                            swal("Éxito!", "Renovado correctamente", "success");
                        }
                        tabla();
                    }
                });                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_modal_pdf(id){
    $("#modal_contrato").modal("show");
    $("#id_renta").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Rentas/getDetaPDF",
        data:{ id_renta : id },
        success:function(data){
            var array = $.parseJSON(data);
            $("#id_det_cont").val(array.id);
            //console.log("en_pagare: "+array.en_pagare);
            //console.log("el_pagare: "+array.el_pagare);
            if(array.en_pagare!="" && array.en_pagare!=null){
                $("#en_pagare").val(array.en_pagare);
            }else{
                //console.log("Toluca");
                $("#en_pagare").val("Toluca");
            }
            if(array.el_pagare!="" && array.el_pagare!=null){
                $("#el_pagare").val(array.el_pagare);
            }
            
            $("#cant_pagare").val(array.cant_pagare);
            $('#porc_interes').val(array.porc_interes);
            $('#nom_pagare').val(array.nom_pagare);
            $('#dir_pagare').val(array.dir_pagare);
            $('#pobla_pagare').val(array.pobla_pagare);
            $('#tel_pagare').val(array.tel_pagare);
        }
    });
}

function get_pdf(id){
    window.open(base_url+'Rentas/pdf_contrato/'+id,'_blank');  
}

function save_datos_pdf(){
    var form_register = $('#form_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            en_pagare:{
              required: true,
            },
            el_pagare:{
              required: true,
            },
            cant_pagare:{
              required: true,
            },
            porc_interes:{
              required: true,
            },
            nom_pagare:{
              required: true,
            },
            dir_pagare:{
              required: true,
            },
            pobla_pagare:{
              required: true,
            },
            tel_pagare:{
              required: true,
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_datos").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Rentas/save_datos_pdf',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $('.save_contra').attr('disabled',true);
            },
            success:function(data){
                $("#modal_contrato").modal("hide");
                var formID = $('#form_datos');
                formID.validate().resetForm(); 
                swal("Éxito!", "Guardado correctamente", "success");
                $('.save_contra').attr('disabled',false);
                get_pdf($("#id_renta").val());
            }
        });  
    } 
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}

function verRenova(id){
    $("#modal_renovas").modal("show");
    $("#id_contra").html(id);
    /*$("#table_renovas").DataTable({ 
        destroy:true,
        "ajax": {
            "url": base_url+"Rentas/getRenovaciones",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                render:function(data,type,row){
                    var html=row.fecha_inicio+ ' '+row.hora_inicio;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html=row.fecha_fin+ ' '+row.hora_fin;
                    return html;
                }
            },
            {"data": "name_suc"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.estatus==1){
                        html="<span class='btn btn-success'>Activo</span>";
                    }if(row.estatus==2){
                        html="<span class='btn btn-primary'>Finalizado</span>";
                    }if(row.estatus==3){
                        html="<span class='btn btn-danger'>Cancelado</span>";
                    }
                    if(row.renovacion==1){
                        html="<span title='Ver Renovaciones' onclick='verRenova("+row.id+")' class='btn btn-success'>Renovado</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';

                    html+='<a title="Generar Contrato" style="cursor:pointer" onclick="get_pdf('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    html+='<a title="Pagos Contrato" style="cursor:pointer" onclick="pay_contra('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pay.svg"></a>';
                    if(row.estatus==1){
                        html+='<a title="Finalizar Contrato" onclick="change_estatus('+row.id+',2,'+row.id_serie+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/bill-invoice.svg"></a>';
                        html+='<a title="Cancelar Contrato" onclick="change_estatus('+row.id+',3,'+row.id_serie+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }if(row.estatus!=3){
                        html+='<a title="Editar" href="'+base_url+'Rentas/alta/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        //html+='<a title="Cancelar Contrato" onclick="change_estatus('+row.id+',3,'+row.id_serie+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }if(row.estatus==2){
                        html+='<a title="Renovar Contrato" onclick="change_estatus('+row.id+',1,'+row.id_serie+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/duplicate.svg"></a>';
                    }
                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
    });*/
    $.ajax({
        type:'POST',
        url: base_url+"Rentas/getRenovaciones",
        data:{id:id},
        success:function(response){
            var array=$.parseJSON(response);
            $("#name_cli").html(array.nombre);
            $("#name_serv").html(array.descripcion);
            $("#num_serie").html(array.serie);
            $("#det_renovas").html(array.data);
        }
    });  
}