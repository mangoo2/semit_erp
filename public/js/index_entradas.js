var base_url = $('#base_url').val();
var tabla;
var id_prodg=0;

jQuery(function($) {
    $('#modalingreso').on('shown.bs.modal', function() {
        $('input[id="search_cod"]').focus();
    });
});

$(document).ready(function() {
    table();
    //$('#searchtext').focus();
    $('#searchcode').focus();
    
    $("#search_cod").keypress(function(e) {
        if (e.which==13) {
            searchCode(this.value);
        }
    });

    document.addEventListener('keydown', (event) => {
        if (event.ctrlKey) {
            if (event.keyCode == 66 || event.keyCode == 98) {
                $("#search_cod").val("");
                $("#search_cod").focus();
                $("#div_scn_"+id_prodg).hide();
                showHideTd(id_prodg);
                $('#modalingreso').animate({
                    scrollTop: $(".container-fluid").offset().top
                }, 1000);
            }
        }
    }, false);
});

function searchOC_Code(){
    var code = $('#searchcode').val();
    if($('#searchcode').val()==""){
        code=0;
    }
    //if($('#searchcode').val().trim()!=""){
        table(code);
    //}
    if(code!=0){
        $.ajax({
            type: 'POST',
            url: base_url+'Entradas/get_oc_estatus',
            data: { code: code },
            async: false,
            success: function(result) {
                //console.log("result: "+result);
                var array = $.parseJSON(result);
                modal_ingreso(array[0].id,array[0].reg,array[0].proveedor);
            }
        });
    }
}

var id_prodg_ant=0;
function searchCode(barcode){
    $.ajax({
        type: 'POST',
        url: base_url+'Entradas/searchBarcode',
        data: { cod: barcode },
        async: false,
        success: function(data) {
            var array = $.parseJSON(data);
            //console.log("array: "+array.length);
            if(array.length>0){
                id_prodg=array[0].id;
                $("#div_scn_"+id_prodg_ant).hide();
                //console.log("id_prodg: "+id_prodg);
                if(array[0].tipo==1){ //serie
                    $('#modalingreso').animate({
                        //scrollTop: $(".tr_serie_det_"+array[0].serie).offset().top
                        scrollTop: $(".tr_det_accordion"+id_prodg).offset().top
                    }, 1000);
                }else{
                    $('#modalingreso').animate({
                        scrollTop: $(".tr_det_accordion"+id_prodg).offset().top
                    }, 1000);
                }
                $("#div_scn_"+id_prodg).show();
                id_prodg_ant=id_prodg;
                //console.log("tipo: "+array[0].tipo);
                if(array[0].tipo==1 || array[0].tipo==2){ //solo tipo serie y lote
                    showHideTd(id_prodg,array[0].serie);
                }
            }
        }
    });
}

function showHideTd(id,serie=0){
    //console.log("showHideTd: "+id);
    //console.log("serie: "+serie);
    if($(".tr_det_accordion"+id).is(":visible")){
        $(".tr_det_accordion"+id).hide("slow");
        $(".exp-btn_"+id).html("+");
        if(serie!=0){
            $(".tr_serie_det_"+serie).addClass("foc");
        }
    }
    else{
        $(".tr_det_accordion"+id).show("slow");
        $(".exp-btn_"+id).html("-");
        if(serie!=0){
            $(".tr_serie_det_"+serie).addClass("foc");
        }
    }
}

function showHideTdBef(id) {
    //console.log("showHideTd: "+id);
    setTimeout(function(){ 
        $(".tr_det_accordion"+id).hide("slow");
        $(".exp-btn_"+id).html("+");
    }, 1500);
}

function reload_registro(){
    tabla.destroy();
    table();
}

function reload_registrox(){
    var f1=$('#fe1').val();
    var f2=$('#fe2').val();
    if(f1!='' && f2!=''){
        tabla.destroy();
        table();
    }
}

function table(code=0){
	tabla = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        destroy:true,
        "ajax": {
            "url": base_url+"Entradas/getlistentradas",
            type: "post",
            "data":{estatus:$('#estatus').val(),f1:$('#fe1').val(),f2:$('#fe2').val(), code:code },
        },
        "columns": [
            {"data": "id"},
            {"data": "personal"},
            {"data": "reg"},
            {"data": "proveedor"},
            {"data": "codigo"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="view_productos('+row.id+',\''+row.reg+'\',\''+row.proveedor+'\')"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            //{"data": "factura"},
            {"data": "factura",
                render:function(data,type,row){
                    var html='';
                    html='<input class="form-control num_factura_'+row.id+'" onchange="updateFactura('+row.id+',this.value)" type="text" value="'+row.factura+'">';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        if(row.estatus==1){
                            html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">OC Ingresada</span>';
                        }else if(row.estatus==3){    
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">No recibido</span>';
                        }else{
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Pendiente de ingreso</span>';
                        }
                    }else{
                        html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Eliminado</span>';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<a title="PDF de compra" style="cursor:pointer" onclick="get_compra('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    
                    if(row.estatus==0){
                        //html+='<a title="Editar OC" onclick="modal_edit('+row.id+')" class="btn" type="button"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        html+='<button data-reg="'+row.reg+'" data-provee="'+row.proveedor+'" title="Ingresar Productos de compra a stock" onclick="modal_ingreso('+row.id+',\''+row.reg+'\',\''+row.proveedor+'\')" class="btn btn-sm id_ent_'+row.id+'" style="background-color: #93ba1f !important;border-color: #93ba1f !important;" type="button">Ingresar</button>';
                        //html+='<button class="btn badge m-l-10" style="background: #ba1f1f;" onclick="update_estatus('+row.id+')">No recibido</button>';
                    }else if(row.estatus==3){

                    }if(row.estatus==1){
                        //console.log("estatus: "+row.estatus);
                        if(row.distribusion==0){
                            //html+='<button title="Revertir entrada" onclick="revertir('+row.id+')" class="btn btn-warning" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></button>';
                            //html+='<a title="Dispersar producto(s)" style="cursor:pointer" onclick="modal_distribucion('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/update.svg"></a>';
                        }else{
                            html+='<button title="Revertir entrada" onclick="revertir('+row.id+')" class="btn btn-warning" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></button>';
                        }
                        //html+='<button title="Revertir entrada" onclick="revertir('+row.id+')" class="btn btn-warning" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></button>';
                    }
                    if(row.id_re>0){
                        html+=' <button title="Bitacora" onclick="bitacoraRever('+row.id+')" class="btn btn-warning" type="button"><i class="fa fa-eye" aria-hidden="true"></i></button>';
                    }

                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function modal_edit(id){
    window.open(base_url+'Comprasp?idcompra='+id+'&eoce=1',"Editar OC", "width=980, height=840"); 
}

function modal_edit_ingre(){
    id = $("#id_oc_ed").val();
    var reg = $(".id_ent_"+id).data("reg");
    var prov = $(".id_ent_"+id).data("provee");
    var win_editoc = window.open(base_url+'Comprasp?idcompra='+id+'&eoce=1',"Editar OC", "width=980, height=840");
    var tiempo= 0;
    var interval = setInterval(function(){
        //Comprobamos que la ventana no este cerrada
        if(win_editoc.closed !== false) {
            //Si la ventana ha sido cerrada, limpiamos el contador
            window.clearInterval(interval);
            modal_ingreso(id,reg,prov);
        }else {
            //Mientras no se cierra la ventana sumamos los segundos
            tiempo +=1;
        }
    },1000)
}

function updateFactura(id,value){

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de actualizar número de factura?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if(value!=""){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Entradas/update_factura",
                        data:{id:id,factura:value},
                        success:function(data){
                            swal("Éxito!", "Actualizado correctamente", "success");
                            table();
                        }
                    });
                }else{
                    toastr.error('Alerta, ingrese un folio o número de factura');  
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function revertir(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¿Está seguro de revertir esta entrada?',
        content: 'Los stocks se regresarán al almacen general'+
                '<br><textarea placeholder="Ingrese un Motivo" id="motivo" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if($("#motivo").val().trim()!=""){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Entradas/revertirEntrada",
                        data:{id:id,motivo:$("#motivo").val()},
                        success:function(data){
                            swal("Éxito!", "Revertido correctamente", "success");
                            table();
                        }
                    });
                }else{
                    toastr.error('Alerta, ingrese motivo de cancelación');  
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function bitacoraRever(id){
    $("#modal_rever").modal("show");
    $.ajax({
        type:'POST',
        url: base_url+"Entradas/getBitacora",
        data:{id:id},
        success:function(data){
            $(".body_bita").html(data); 
        }
    });
}

var cont_add_lt=1;
function addRow(id_prod,element,contg){
    var html="";
        html='<tr class="tr_lc tr_lc_'+cont_add_lt+' tr_det_accordion'+id_prod+'">\
            <td></td>\
            <td><input type="hidden" id="idproductox" value="'+id_prod+'">\
                <input class="form-control cant cant_'+id_prod+'" type="text" id="cantidadx" value="">\
            </td>\
            <td><input class="form-control lote_'+id_prod+' loteadd_'+id_prod+'_'+cont_add_lt+'" type="text" id="lotex" value="" onchange="createCDL(8,'+id_prod+','+cont_add_lt+')">\
            </td>\
            <td><input class="form-control caduc_'+id_prod+' caducadd_'+id_prod+'_'+cont_add_lt+'" type="date" id="caducidadx" value="" onchange="createCDL(8,'+id_prod+','+cont_add_lt+')"></td>\
            <td><input class="form-control cod_barras_'+id_prod+' cod_barradd_'+id_prod+'_'+cont_add_lt+'" type="number" id="cod_barrasx" value=""></td>\
            <td><button onclick="deleteRow('+id_prod+','+cont_add_lt+')" class="btn btn-danger" type="button"> <i class="fas fa-minus-square" aria-hidden="true"></i></button></td>\
        </tr>';
    //$(".table_proy").append(html);
    $(".tr_lc_"+contg).after(html);
    cont_add_lt++;
}

function changeLote(val,id_prod,id_inp,cont){
    if(cont==1){
        if(id_inp=="lotex"){
            $(".lote_"+id_prod).val(val);
        }else{
            $(".caduc_"+id_prod).val(val);
        }
    }
}

function deleteRow(id_prod,element){
    $(".tr_lc_"+element).remove();
}

var cont_disp_add=0; var idps_ant=0;
function addRowDisp(id_prod,idps,cant){
    /*console.log("cant: "+cant);
    console.log("idps: "+idps);
    console.log("idps_ant: "+idps_ant);*/
    
    var html="";
        html='<tr class="tr_disp_" id="tr_disp_'+id_prod+'_'+idps+'_'+cont_disp_add+' tr_det_accordion'+id_prod+'">\
            <td><input type="hidden" id="idproducto_lote" value="'+idps+'"></td>\
            <td></td>\
            <td></td>\
            <td><input class="form-control" type="number" id="cantidad" value=""></td>\
            <td id="cont_sel_dis_'+idps+'"></td>\
            <td><button onclick="deleteRowDisp('+id_prod+','+idps+','+cont_disp_add+')" class="btn btn-danger" type="button"> <i class="fas fa-minus-square" aria-hidden="true"></i></button></td>\
        </tr>';
    //console.log("cont_disp_add: "+cont_disp_add);
    if(idps_ant!=idps){
        cont_disp_add=0;
    }
    if(cont_disp_add<cant){
        if(cont_disp_add==0){
            $("#tr_disp_"+id_prod+"_"+idps).after(html);
        }else{
            $("#tr_disp_"+id_prod+"_"+idps).after(html);
            //$("#tr_disp_"+id_prod+"_"+idps+"_"+cont_disp_add).after(html);
            /*if($("#tr_disp_"+id_prod).is("visible")){
                $("#tr_disp_"+id_prod).after(html);
            }else{
                $("#tr_disp_"+id_prod+"_"+cont_disp_add).after(html);
            }*/
        }
        $('#select_fath').clone().attr('id', 'select_clone').appendTo($('#cont_sel_dis_'+idps));
        cont_disp_add++;
        idps_ant=idps;
    }
}

function deleteRowDisp(id_prod,idps,element){
    $("#tr_disp_"+id_prod+"_"+idps+"_"+element).remove();
    cont_disp_add--;
}

function view_productos(id,reg,prov){
	$('#modalproductos').modal('show');
    $('#name_prov').html('Proveedor: '+prov);
    $('#fecha_oc').html('Fecha de compra: '+reg);
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Entradas/view_productos",
        data: {
        	idcompras:id,band:0
        },
        success: function (response){
            var array = $.parseJSON(response);
            //$(".style_prods").html(array.html_sty);
            $('.table_pro').html(array.html);
            $(".html_scr").html(array.html_scr);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}

function createCDL(idsuc,idprod,cont=0){
    if(cont==0){
        var lote=$(".lote_"+idprod).val();
        var cad=$(".caduc_"+idprod).val();
    }else{
        var lote=$(".loteadd_"+idprod+"_"+cont).val();
        var cad=$(".caducadd_"+idprod+"_"+cont).val();
    }
    /*console.log("cont: "+cont);
    console.log("lote: "+lote);
    console.log("cad: "+cad);*/
    $.ajax({
        type:'POST',
        url: base_url+'Entradas/crearCodigoLote',
        async: false,
        data: {idsuc:idsuc,idprod:idprod,lote:lote,cad:cad},
        success:function(response){
            if(cont==0){
                $(".cod_barras_"+idprod).val(response); 
            }else{
                $(".cod_barradd_"+idprod+"_"+cont).val(response); 
            }
        }
    });
}

var idcompra_aux=0;
function modal_ingreso(id,reg,prov){
    idcompra_aux=id;
    $('#modalingreso').modal('show');
    $('#factura').val('');
    $('#name_provi').html('Proveedor: '+prov);
    $('#fecha_oci').html('Fecha de compra: '+reg);
    $("#search_cod").focus();


    var numfac=$('.num_factura_'+id).val();
    $('#factura').val(numfac);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Entradas/view_productos",
        data: {
            idcompras:id,band:1
        },
        success: function (response){
            var array = $.parseJSON(response);
            //$(".style_prods").html(array.html_sty);
            $('.table_proy').html(array.html);
            $(".html_scr").html(array.html_scr);
            $('#id_oc_ed').val(id);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}

function ingresar_productos(){
    var factura=$('#factura').val();
    var lotesval = $("#tabla_productos .table_proy > .tr_lc");
    var cant=0; var catidadcomplc=0; var valida_ser=0; var valida_lot=0;
    lotesval.each(function(){
        //cant+=Number($("#cantidadx").val());
        cant+=Number($(this).find("input[id*='cantidadx']").val());
    });
    //console.log("cant: "+cant);
    if(factura!=''){
        var serie = $("#tabla_productos .table_proy > .tr_s");
        var DATAs  = [];
        serie.each(function(){         
            item = {};
            item['id']=$(this).find("input[id*='id']").val();
            item['idproducto']=$(this).find("input[id*='idproductox']").val();
            item['serie']=$(this).find("input[id*='seriex']").val();
            if($(this).find("input[id*='seriex']").val()!="" && $(this).find("input[id*='seriex']").is(":visible")==true){
                DATAs.push(item);
            }
            if($(this).find("input[id*='seriex']").val()=="" && $(this).find("input[id*='seriex']").is(":visible")==true){
                valida_ser++;
                swal("!Atención!", "Ingrese una serie", "error");   
                return;
            }
        });
        INFOa  = new FormData();
        aInfos   = JSON.stringify(DATAs);
        //console.log("aInfos: "+aInfos);
        var lotes = $("#tabla_productos .table_proy > .tr_lc");
        var DATAlt  = [];
        lotes.each(function(){
            item = {};
            item['id']=$(this).find("input[id*='id']").val();
            item['idproducto']=$(this).find("input[id*='idproductox']").val();
            item['lote']=$(this).find("input[id*='lotex']").val();
            item['caducidad']=$(this).find("input[id*='caducidadx']").val();
            item['cantidad']=$(this).find("input[id*='cantidadx']").val();
            item['cod_barras']=$(this).find("input[id*='cod_barrasx']").val();
            DATAlt.push(item);
            
            if($(this).find("input[id*='id']").val()==0){
                catidadcomplc+=Number($(this).find("input[id*='catidadcomplc']").val());
            }else{
                //catidadcomplc+=Number($(this).find("input[id*='cantidadx']").val());
                catidadcomplc+=Number($(this).find("input[id*='catidadcomplc']").val());
            }
            //console.log("catidadcomplc: "+catidadcomplc);
            if($(this).find("input[id*='lotex']").val()=="" || $(this).find("input[id*='caducidadx']").val()==""){
                valida_lot++;
                swal("!Atención!", "Ingrese un lote", "error");   
                return false;
            }
        });
        INFOlt  = new FormData();
        aInfolt   = JSON.stringify(DATAlt);
        //console.log("aInfolt: "+aInfolt);
        var band_lotes=0;
        if($("#tabla_productos .table_proy > .tr_lc").length>0 && cant<= Number(catidadcomplc)){
            band_lotes=0;
        }else if($("#tabla_productos .table_proy > .tr_lc").length>0 && cant> Number(catidadcomplc)){
            band_lotes=1;
            swal("!Atención!", "La cantidad capturada es mayor a la cantidad de compra", "error"); 
        }
        //console.log("cant: "+cant);
        //console.log("catidadcomplc: "+catidadcomplc);

        if(band_lotes==0 && valida_ser==0 && valida_lot==0){
            var datos = 'idcompras='+idcompra_aux+'&factura='+factura+'&series='+aInfos+'&lotes='+aInfolt;
            $.ajax({
                type:'POST',
                url: base_url+"Entradas/ingresar_inventario",
                data: datos,
                success: function (response){
                    swal("Éxito!", "Guardado Correctamente", "success"); 
                    setTimeout(function(){ 
                        $('#modalingreso').modal('hide');
                        $('#searchcode').val('');
                        table(); 
                    }, 1000); 
                    /*setTimeout(function(){ 
                        $('#modaldispersion').modal('show');
                    }, 1500); */ 
                },
                error: function(response){
                    toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
                }
            });
        }
    }else{
        swal("!Atención!", "Falta agregar folio de factura", "error");
    }
}

function closeIngreso(){
    $('#searchcode').val('');
    //table();
}

function get_compra(id){
    window.open(base_url+'Entradas/imprimir_compra/'+id,'_blank'); 
}

function modal_distribucion(id){
    idcompra_aux=id;
    get_dispersion();
}

function get_dispersion(){
    $('#modaldispersion').modal('hide');
    setTimeout(function(){ 
        $('#modaldispersion_cantidad').modal('show');
    }, 1000); 
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Entradas/view_productos_dispersion_r",
        data: {
            idcompras:idcompra_aux
        },
        success: function (response){
            $('.ul_tad').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Entradas/view_productos_dispersion",
        data: {
            idcompras:idcompra_aux
        },
        success: function (response){
            $('.tab-content').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}

function validar_cantidad(cont){
    var pro_can=$('#idproducto_'+cont).val();
    var p_cant=parseFloat(pro_can);
    var TABLA   = $("#table_distribusion_"+cont+" .table_pro_dis > .tr_dis");
    var suma=0;
    TABLA.each(function(){ 
        var cantidad=$(this).find("input[id*='cantidadx']").val();
        if(cantidad=='' || cantidad==0){
            suma+=parseFloat(0);
        }else{
            suma+=parseFloat(cantidad);
        }
    });
    var band_disp_lot=0;
    /*var suma2=0;
    var cant_comp=$('#cant_comp_'+cont).val();
    var TABLALO = $("#table_dist_lote_"+cont+" tbody > tr");
    TABLALO.each(function(){ 
        var cantidadl=$(this).find("input[id*='cantidad']").val();
        if(cantidadl=='' || cantidadl==0){
            suma2+=parseFloat(0);
        }else{
            suma2+=parseFloat(cantidadl);
        }
    });
    console.log("cant_comp: "+cant_comp);
    console.log("suma2: "+suma2);
    if(cant_comp>=suma2){
        band_disp_lot=0;
    }else{
        band_disp_lot=1;
    }
    console.log("band_disp_lot: "+band_disp_lot);*/

    if(p_cant>=suma && band_disp_lot==0){     
        var vali=0;
        var TABLA   = $("#table_distribusion_serie_"+cont+" tbody > tr");
        TABLA.each(function(){ 
            var idsucursal = $(this).find("select[class*='idsucursals']").val();
            if(idsucursal==0){
                vali=1;
            }
        });  
        if(vali==0){
            $('.validar_pro_'+cont).val(1);
            $('#icon-'+cont+'_tab').css('background','#93ba1f8a');
            $('.cantidadx_'+cont).prop('readonly',true);
            $('.cantidadx_'+cont).css('cursor','no-drop');
            $('.btn_can_'+cont).prop('disabled',true);
            if($("#table_distribusion_"+cont+" .table_pro_dis > .tr_dis").length>0){
                guardar_distribucion(cont);
            }
            if($("#table_distribusion_serie_"+cont+" tbody > tr").length>0 || $("#table_dist_lote_"+cont+" tbody > tr").length>0){
                guardar_distribucion_serie(cont); 
            }
            swal("Éxito!", "Guardado Correctamente", "success"); 
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1500);
        }else{
            swal("¡Atención!", "Falta seleccionar una sucursal", "error");
        }
    }else{
        $('#icon-'+cont+'_tab').css('background','white');
        swal("¡Atención!", "La suma de la cantidad de los productos es mayor a la cantidad comprada.", "error");
    }
}

var contcar=0;
function guardar_distribucion(cont){
    var DATA  = [];
    var TABLA   = $("#table_distribusion_"+cont+" tbody > tr");
    TABLA.each(function(){ 
        contcar=1;
        item = {};
        item ["idcompra"] = idcompra_aux;
        item ["idsucursal"] = $(this).find("input[id*='idsucursalx']").val();
        item ["idproducto"] = $(this).find("input[id*='idproductox']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
        item ["tipo"] = $(this).find("input[id*='tipoprodx']").val();
        if($(this).find("input[id*='tipoprodx']").val()!=""){
            DATA.push(item);
        }
    });  
    if(contcar==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'index.php/Entradas/registro_distribusion',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
                $.ajax({
                    data: INFO, 
                    type: 'POST',
                    url : base_url + 'index.php/Entradas/registro_distribusion2',
                    processData: false, 
                    contentType: false,
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success: function(data){
                        $.ajax({
                            data: INFO, 
                            type: 'POST',
                            url : base_url + 'index.php/Entradas/registro_distribusion3',
                            processData: false, 
                            contentType: false,
                            async: false,
                            statusCode:{
                                404: function(data){
                                    toastr.error('Error!', 'No Se encuentra el archivo');
                                },
                                500: function(){
                                    toastr.error('Error', '500');
                                }
                            },
                            success: function(data){
                                swal("Éxito!", "Guardado Correctamente", "success"); 
                                setTimeout(function(){ 
                                    //$('#modaldispersion_cantidad').modal('hide');
                                    tabla.ajax.reload(); 
                                }, 1000); 
                                
                            }
                        });   
                    }
                });    
            }
        });  
    }
}

var contser=0;
function guardar_distribucion_serie(cont) {
    //console.log("cont de guardar_distribucion_serie: "+cont);
    var DATA  = [];
    var TABLA   = $("#table_distribusion_serie_"+cont+" tbody > tr");
    TABLA.each(function(){ 
        contser=1;
        item = {};
        item ["idcompra"] = idcompra_aux;
        item ["idsucursal"] = $(this).find("select[class*='idsucursals']").val();
        item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        item ["idproducto"] = $("#idproducto_padre_"+cont+"").val();
        DATA.push(item);
    });
    var DATALO  = [];
    var TABLALO   = $("#table_dist_lote_"+cont+" tbody > tr");
    TABLALO.each(function(){ 
        contser=1;
        item = {};
        item ["idcompra"] = idcompra_aux;
        item ["idsucursal"] = $(this).find("select[class*='idsucursals']").val();
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto"] = $("#idproducto_padre_"+cont+"").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
        DATALO.push(item);
        if($(this).find("input[id*='cantidad']").val()==""){
            contser++;
        }
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    //INFO.append('data', aInfo);
    //console.log("series: "+aInfo);
    INFOlt  = new FormData();
    aInfolt = JSON.stringify(DATALO);
    //console.log("lotes: "+aInfolt);

    //console.log("contser: "+contser);
    if(contser==1){
        $.ajax({
            data: "series="+aInfo+'&lotes='+aInfolt, 
            type: 'POST',
            url : base_url + 'index.php/Entradas/registro_distribusion_series',
            /*processData: false, 
            contentType: false,
            async: false,*/
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
                /*swal("Éxito!", "Guardado Correctamente", "success"); 
                setTimeout(function(){ 
                    //$('#modaldispersion_cantidad').modal('hide');
                    tabla.ajax.reload(); 
                }, 1000); */
            }
        });  
    }
}

function confirmar_distribucion(){
    var validar=0;
    var TABLA   = $(".tabla_validar_pro thead > tr");
    TABLA.each(function(){ 
        var vali = $(this).find("input[id*='validar_pro']").val();
        if(vali==0){
            validar=1;
        }
    });
    if(validar==0){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de confirmar dispersión?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Entradas/update_distribusion",
                        data: {
                            idcompra:idcompra_aux
                        },
                        success: function (response){
                            swal("Éxito!", "Guardado Correctamente", "success"); 
                            setTimeout(function(){ 
                                $('#modaldispersion_cantidad').modal('hide');
                                tabla.ajax.reload(); 
                            }, 1000); 
                        },
                        error: function(response){
                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
        swal("¡Atención!", "Falta validación de producto", "error");
    }
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}

function validar_serie(val,ele){
    var num_serie=val;
    $.ajax({
        type:'POST',
        url: base_url+"Entradas/get_validar_serie",
        data:{serie:num_serie},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var num = parseFloat(data);
            if(num==0){
                var validar=0;
                var serie = $("#tabla_productos .table_proy > .tr_s");
                serie.each(function(){         
                    var serie_x = $(this).find("input[id*='seriex']").val();
                    if(serie_x==num_serie){
                        validar++;
                    }
                });
                //alert(validar);
                if(validar<2){
                    
                }else{
                    ele.val('');
                    swal("¡Atención!", "La serie ya existe", "error");    
                }
               /* var validar=0;
                var tcantidades = $("#table_cantidades tbody > .tr_c");
                tcantidades.each(function(){         
                    var TABLA_d = $(this).find('.tabla_cantidades_sucursal tbody > .tr_s');
                    TABLA_d.each(function(){ 
                        var serie_x = $(this).find("input[id*='seriex']").val();
                        if(serie_x==num_serie){
                            validar=1;
                        }
                    });
                });
                if(validar==0){
                }else{
                    $(this).val('');
                    swal("¡Atención!", "La serie ya existe", "error");    
                }*/
            }else{
                ele.val('');
                swal("¡Atención!", "La serie ya existe", "error");
            }
        }
    }); 
         
}

function update_estatus(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de indicar como "OC no recibida"?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Entradas/cambio_estatus",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Actualizado correctamente", "success");
                        table();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}