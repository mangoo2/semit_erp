var base_url = $('#base_url').val();
$(document).ready(function() {
    $("#ingresar_login").keypress(function(e) {
        if (e.which==13) {
            ingresar();
        }
    });
    img_cont();
    setInterval(function() {
        img_cont();
        //console.log('hola');
    }, 5000);
    
});

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}


//var cont=1;
function img_cont(){
    var cont=getRandomInt(37);
    //console.log(cont);
    if(cont==1){
        //console.log(cont+'.jpg');
        $('.class_img').html('<img class="bg-img-cover bg-center" src="'+base_url+'public/img/login/1.jpg">');
    }else{
        //console.log(cont+'.png');
        $('.class_img').html('<img class="bg-img-cover bg-center" src="'+base_url+'public/img/login/'+cont+'.png">');
    }
    $(".bg-center").parent().addClass('b-center');
    $(".bg-img-cover").parent().addClass('bg-size');
    $('.bg-img-cover').each(function () {
        var el = $(this),
            src = el.attr('src'),
            parent = el.parent();
        parent.css({
            'background-image': 'url(' + src + ')',
            'background-size': 'cover',
            'background-position': 'center',
            'display': 'block'
        });
        el.hide();
    });
    /*if(cont==20){
       cont=1;
    }
    cont++;*/
}

function ingresar(){
    //$('.login-box').animateCss("zoomIn");
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Login/login",
        data: $('#sign_in').serialize(), 
        beforeSend: function(){

        },
        success: function (result) {
            //console.log(result);
            setTimeout(function () { $('#loader').fadeOut(); }, 50);
            if(result==1){
                //setTimeout(function () { $('#success').fadeIn(); }, 300);
                //setTimeout(function () { location.reload(); }, 2000);
                //$('.login_texto').css('display','none'); 
                //$('.sesion_texto').css('display','block'); 
                
                var btn = KTUtil.getById("kt_btn_2");
                KTUtil.btnWait(btn, "spinner spinner-right spinner-white pr-15", "Cargando");
                setTimeout(function() {
                    KTUtil.btnRelease(btn);
                }, 10000);
                $('.alert1').css('display','block'); 
                $('.alert2').css('display','none'); 
                setTimeout(function(){window.location.href = base_url+"Sistema"},3500);

            }
            else{
                //setTimeout(function () { $('#error').fadeIn(); }, 500);
                $('.alert1').css('display','none'); 
                $('.alert2').css('display','block'); 
            }
        }
    });
    /*
    $('#sign_in').validate({
        rules: {
            usuario: "required",
            
        },
        messages: {
            usuario: "Ingrese su nombre de usuario",
            password: "Ingrese su contraseña"
            
        },
        submitHandler: function (form) {
             $.ajax({
                 type: "POST",
                 url: base_url+"index.php/Login/login",
                 data: $(form).serialize(), 
                 beforeSend: function(){
                    $("#loader").show();
                    $('#success').hide();
                    $('#error').hide();
                 },
                 success: function (result) {
                    console.log(result);
                    setTimeout(function () { $('#loader').fadeOut(); }, 50);
                    if(result==1){
                        //setTimeout(function () { $('#success').fadeIn(); }, 300);
                        //setTimeout(function () { location.reload(); }, 2000);
                        $('.login_texto').css('display','none'); 
                        $('.sesion_texto').css('display','block'); 
                        setTimeout(function(){window.location.href = base_url+"Sistema"},3500);

                    }
                    else{
                        setTimeout(function () { $('#error').fadeIn(); }, 500);
                    }
                    
                    
                 }
             });
             return false; // required to block normal submit for ajax
         },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
    */
}

