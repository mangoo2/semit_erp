var base_url = $('#base_url').val();
var perfil = $('#perfil').val(); 
var table;
$(document).ready(function () {
    table = $('#table_rechange').DataTable({
        "ajax": {
           "url": base_url+"Recargas/getDataTable",
           type: "post",
            error: function(){
               $("#table_rechange").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "codigo"},
            {"data": "capacidad"},
            {"data": "preciov",
                "render" : function (data,type,row) {
                    var msj='';
                    msj+= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.preciov);
                    return msj;
                }
            },
            {"data": "precioc",
                "render" : function (data,type,row) {
                    var msj='';
                    msj+= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.precioc);
                    return msj;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<div class="btn-group">';
                        if(perfil==1 || perfil==4){
                            html+='<a href="'+base_url+'Recargas/alta/'+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>';
                        }
                    html+='</div>';
                return html;
                }
            },
        ],
        "pageLength": 10
    });    
});

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}

function delete_record(id) {
    swal({
        title: "¿Desea eliminar este operador?",
        text: "Se eliminará definitivamente del listado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }).then(function (isConfirm) {
        if (isConfirm) {
           $.ajax({
                 type: "POST",
                 url: base_url+"Personal/delete/"+id,
                 success: function (result) {
                    console.log(result);
                    table.ajax.reload();
                    swal("Éxito!", "Se ha eliminado correctamente", "success");
                 }
             });
        }
    });

}

