var table;
var base_url=$('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {	
    tabla();
    $(".btn_aceptar").on("click",function(){
        saveSolicitaDet();
    });

    $(".save_orden").on("click",function(){
        saveOrden();
    });
});

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Mttos_internos/get_data_list",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "fecha_reg"},
            {"data": "observ"},
            {"data": "equipo"},
            {"data": "serie"},
            {"data": "fecha_termino"},
            {"data": "prox_mtto"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.tipo==0){
                        html="<span class='btn btn-warning'>Preventivo</span>";
                    }if(row.tipo==1){
                        html="<span class='btn btn-danger'>Correctivo</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html=''; var event="";
                    if(row.estatus==1){
                        html="<span class='btn btn-warning'>En mtto.</span>";
                    }if(row.estatus==2){
                        html="<span class='btn btn-primary'>Mtto. terminado</span>";
                    }if(row.estatus==3){
                        html="<span class='btn btn-danger'>Sin reparación</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<a href="'+base_url+'Mttos_internos/registro/'+row.id+'" class="btn btn-cli-edit" style="width: 40px;"></a> ';
                    //if(row.estatus==2){
                        html+='<a title="PDF Orden" style="cursor:pointer" onclick="get_pdf_orden('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    //}  
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        columnDefs: [ { orderable: false, targets: [4,6,7] }],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function get_pdf_orden(id){
    $("#modalOrden").modal("show");
    $("#id_mttoord").val(id);
    $("#id_mtto_show").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Mttos_internos/getDetOrdTrabajo",
        data:{ id:id },
        success:function(data){
            var array = $.parseJSON(data);
            $("#id_ordeng").val(array.id);
            $("#fecha_orden").val(array.fecha);
            $("#observ").val(array.observ);
        }
    });
}

function saveOrden(){ 
    if($("#fecha_orden").val()==""){
       swal("Álerta!", "Indica una fecha para la orden de trabajo", "warning"); 
       return;
    }
    $.ajax({
        type:'POST',
        url: base_url+"Mttos_internos/save_orden",
        data:{ id:$("#id_ordeng").val(), id_mtto:$("#id_mttoord").val(), fecha:$("#fecha_orden").val(), observ:$("#observ").val() },
        success:function(data){
            $("#modalOrden").modal("hide");
            swal("Éxito!", "Guardado correctamente", "success");
            setTimeout(function(){ 
                window.open(base_url+'Mttos_internos/ordenTrabajo/'+$("#id_mttoord").val(),'_blank'); 
            }, 1500); 
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
