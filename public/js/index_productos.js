var base_url = $('#base_url').val();
var perfil = $('#perfil').val(); 
var tabla;var row_pet=1; var row_eje=1;
$(document).ready(function() {
    tabla=$("#table_datos").DataTable();
    table();
    tabla.search('').draw();
    $('#searchtext').focus();

    $('#imprimiretiqueta').click(function(event) {
      var idp=$('#idproetiqueta').val();
      var nump=$('#numprint').val();
      $('#iframeetiqueta').html('<iframe src="'+base_url+'Etiquetas?id='+idp+'&page='+nump+'&print=true"></iframe>');
    });
    //getSucs();

    $('.exportExcel').on('click', function() {
        $('body').loading({ theme: 'dark', message: 'Generando archivo, por favor espere...' });
        $.ajax({
            url:base_url+'Productos/exportExcelProds',
            type: 'POST',
            success: function(response) {
                window.location.href = base_url + 'Productos/exportExcelProds';
                $('body').loading('stop');
            },
            error: function() {
                swal("Error","Hubo un error al generar el archivo, intente nuevamente","error");
                $('body').loading('stop'); // Desbloquear la pantalla
            }
        });
    });

});

function reload_registro(){
    row_pet=1;row_eje=1;
    table();
}
function table(){
    tabla.destroy();
	tabla=$("#table_datos").DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"Productos/getlistado",
            type: "post",
            "data":{idcategoria:$('#idcategoria').val(),idproductos:$('#idproductos option:selected').val(),idmasvendidos:0 },
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"idProducto"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a class="btn" onclick="modal_img('+row.id+')"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data":"nombre"},
            {"data":"categoria"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.precio_final=== null){
                        html='$0.00';
                    }else{
                        var html=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.precio_final);
                    }
                return html;
                }
            },
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a class="btn" onclick="modal_precios('+row.id+')"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },*/
            /*{"data": null,
                "render": function ( data, type, row, meta ){ //almacen gral
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie8;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',8)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote8;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',8)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock8;
                    }
                
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){ //pino suarez
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie2;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',2)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote2;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',2)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock2;
                    }
                
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){ //jesus carrranza
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie4;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',4)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote4;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',4)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock4;
                    }
                
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){ //metepec
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie5;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',5)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote5;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',5)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock5;
                    }
                
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){ //alfredo del mazo
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie3;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',3)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote3;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',3)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock3;
                    }
                
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){ //almacen web
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie6;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',6)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote6;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',6)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock6;
                    }
                
                return html;
                }
            },            
            {"data": null,
                "render": function ( data, type, row, meta ){ //almacen rentas
                    var html='';
                    if(row.tipo==1){
                        html+=row.serie7;
                        html+='<a class="btn" onclick="modal_serie('+row.id+',7)"><i class="fa fa-eye"></i></a>';
                    }else if(row.tipo==2){
                        html+=row.lote7;
                        //html+='<a class="btn" onclick="modal_lote('+row.id+',7)"><i class="fa fa-eye"></i></a>';
                    }else{
                        html=row.stock7;
                    }
                
                return html;
                }
            },*/
            {"data": null,
                "render": function ( data, type, row, meta ){ 
                    //var html='<span class="consulta_stock_pro consulta_stock_pro_'+row.id+'_'+row.tipo+'" data-id="'+row.id+'" data-tipo="'+row.tipo+'"></span>';
                    var html='';
                        //html+='<div class="consulta_stock_pro consulta_stock_pro_'+row.id+'_'+row.tipo+'" data-id="'+row.id+'" data-tipo="'+row.tipo+'" data-idp="'+row.idProducto+'"></div>'; se comenta pero si se quiere re utilizar se descomenta
                        html+='<button class="btn btn-primary btn-sm" type="button" onclick="setIndex();infosucursales('+row.id+','+row.tipo+')"><i class="fas fa-eye"></i></button>';
                    //return getStock(row.id,row.tipo);
                    return html;
                }
            },
            /*{"data":"stock3"},
            {"data":"stock4"},
            {"data":"stock5"},
            {"data":"stock6"},
            {"data":"stock8"},
            {"data":"stock7"},*/
            /*{"data":"mas_vendidos"},*/
            {"data": null,
                "render": function ( data, type, row, meta ){
                //var html='';
                //if(row.mostrar_pagina==1){
                    html='<button class="btn btn-primary btn-sm" type="button">Activo</button>';
                //}else{
                //    html='<button class="btn btn-danger btn-sm" type="button">Inactivo</button>';
                //}
                return html;
                }
            },
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                var check='';    
                if(row.mostrar_pagina==1){
                    check='checked';
                }else{
                    check='';
                }    
                var html='<span class="switch switch-icon">\
                      <label>\
                          <input type="checkbox" id="mostrar_pagina_'+row.id+'" '+check+' onclick="mostra_pagina_web('+row.id+')">\
                          <span></span>\
                      </label>\
                  </span>';
                return html;
                }
            },*/
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                    if(perfil!=11){ //pagina web
                        html+='<a onclick="codigo_modal('+row.id+')" class="btn idreg_'+row.id+'" data-codigo="'+row.idProducto+'" data-producto="'+row.nombre+'" data-categoria="'+row.categoria+'" data-precio="'+row.precio+'" ><img style="width: 20px; margin-top:5px;" src="'+base_url+'public/img/scanner.svg"></a>';   
                    }
                    
                    if(perfil!=5){ //almacen
                        html+='<a href="'+base_url+'Productos/registro/'+row.id+'" class="btn btn-cli-edit" style="width: 40px;"></a>';
                    }
                    if(perfil!=4 && perfil!=5){
                        //html+='<a onclick="validar_modal('+row.id+')" class="btn btn-cli-delete" style="width: 40px;"></a>';
                    } 
                    html+='</div>';
                return html;
                }
            },
        ],
        "columnDefs": [
          { "orderable": false, "targets": 0 },//ocultar para columna 0
          { "orderable": false, "targets": 1 },//ocultar para columna 1
          { "orderable": false, "targets": 2 },//ocultar para columna 2
          { "orderable": false, "targets": 6 },//ocultar para columna 3
          { "orderable": false, "targets": 7 },//ocultar para columna 4
          //{ "orderable": false, "targets": 8 },//ocultar para columna 5
          //{ "orderable": false, "targets": 9 },//ocultar para columna 6
          //{ "orderable": false, "targets": 10 },//ocultar para columna 7
          //{ "orderable": false, "targets": 11 },//ocultar para columna 8
          //{ "orderable": false, "targets": 12 },//ocultar para columna 9
          //{ "orderable": false, "targets": 13 },//ocultar para columna 10
          //{ "orderable": false, "targets": 14 },//ocultar para columna 11
          //{ "orderable": false, "targets": 15 },//ocultar para columna 12
          //`Asi para cada columna`...
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(3)')
                .addClass('addref addref_'+data.id)
                .attr('data-id',data.id).attr('data-ref',data.referencia);
        }
    
     }).on('draw',function(){
        //setTimeout(function(){
            if(row_eje==1){
                consultarstockpro();
            }
            row_eje++;
        //},200);
            setTimeout(function(){ row_eje=1; },1000);
        
        //generarref();
        /*
        $('#search').keyup(function(){
            table.search($(this).val()).draw() ;
        });
        */
    });
    tabla.columns(0).visible(false); 
}

function getStock(id,tipo){ //falta restarle los que esté en tralado
    var stock_suc="";
    $.ajax({
        type:'POST',
        async: false,
        url: base_url+"Productos/getStockSucs",
        data: {id:id, tipo:tipo },
        success:function(response){
            //console.log("response: "+response);
            stock_suc+=response;
        }
    });
    return stock_suc;
}
function consultarstockpro(){
    //console.log("consulta: ");
    $(".consulta_stock_pro").each(function() {
        
        var id = $(this).data('id');
        var tipo = $(this).data('tipo');
        var idp = $(this).data('idp');
        console.log('peticion '+row_pet+ ' '+idp);
        $.ajax({
            type:'POST',
            async: false,
            url: base_url+"Productos/getStockSucs",
            data: {id:id, tipo:tipo },
            success:function(response){
                console.log("response: "+response);
                //stock_suc+=response;
                $('.consulta_stock_pro_'+id+'_'+tipo).html(response);
            }
        });
        row_pet++;
    });
}
var array_suc=[];
function getSucs(){
    //array_suc =["","Matriz","Pino Suárez","Alfredo del Mazo","Jesús Carranza","Metepec","Almacén WEB","Almacén Rentas","Almacén General"];
    array_suc=[];
    $.ajax({
        type:'POST',
        url: base_url+"Sistema/getSucursales",
        success:function(data){
            //console.log("data: "+data);
            var arr = $.parseJSON(data);
            arr.array.forEach(function(i){
                //console.log("id de array recorrido: "+i.id);
                array_suc.push(i.id);
            });
            //console.log("array_suc: "+array_suc);
        }
    });
    return array_suc;
}

var idproductosaux=0;
function validar_modal(id){
    $('#modalcodigo').modal('show');
    setTimeout(function(){ 
        $('#codigo_t1').focus();
    }, 1000); 
    idproductosaux=id;
}

function validar_codigo(){
    var t1 = $('#codigo_t1').val();
    var t2 = $('#codigo_t2').val();
    var t3 = $('#codigo_t3').val();
    var t4 = $('#codigo_t4').val();
    var t5 = $('#codigo_t5').val();
    var n1=0;
    var n2=0; 
    var n3=0;
    var n4=0;
    var n5=0;
    if(t1==''){
        $('#codigo_t1').css('border-color','red');
        n1=0;
    }else{
        $('#codigo_t1').css('border-color','#012d6a');
        n1=1;
    }

    if(t2==''){
        $('#codigo_t2').css('border-color','red');
        n2=0;
    }else{
        $('#codigo_t2').css('border-color','#012d6a');
        n2=1;
    }

    if(t3==''){
        $('#codigo_t3').css('border-color','red');
        n3=0;
    }else{
        $('#codigo_t3').css('border-color','#012d6a');
        n3=1;
    }

    if(t4==''){
        $('#codigo_t4').css('border-color','red');
        n4=0;
    }else{
        $('#codigo_t4').css('border-color','#012d6a');
        n4=1;
    }

    if(t5==''){
        $('#codigo_t5').css('border-color','red');
        n5=0;
    }else{
        $('#codigo_t5').css('border-color','#012d6a');
        n5=1;
    }
    var suma = t1+t2+t3+t4+t5;

    if(n1==1 && n2==1 && n3==1 && n4==1 && n5==1){
        $('.btn_registro_valida').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/validar_codigo',
            data: {codigo:suma},
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                var validarc = parseFloat(data);
                if(validarc==0){
                    swal("¡Atención!","El código es incorrecto","error");
                    $('.btn_registro_valida').attr('disabled',false);
                }else{
                    $('#modalcodigo').modal('hide');
                    $('#codigo_t1').val('');
                    $('#codigo_t2').val('');
                    $('#codigo_t3').val('');
                    $('#codigo_t4').val('');
                    $('#codigo_t5').val('');
                    swal({
                        title: "¡Éxito!",
                        text: "Código correcto",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    setTimeout(function(){ 
                        eliminar_registro(idproductosaux);
                    }, 2000); 
                    
                }
                
            }
        });
    }
    
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Productos/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function modal_img(id){
    $('#modalimagenes').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Productos/viewimages_list',
        data: {id:id},
        success:function(data){
            $('.text_imagenes').html(data);
            //$('.materialboxed').materialbox();
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    //console.log(searchtext);
    tabla.search(searchtext).draw();
}

function mostra_pagina_web(id){
    var mostrar=0;
    if( $('#mostrar_pagina_'+id).is(':checked') ) {
        mostrar=1;
    }else{
        mostrar=0;
    }
    $.ajax({
        type:'POST',
        url: base_url+"Productos/update_pagina",
        data:{id:id,mostrar:mostrar},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            if(mostrar==1){
                swal("Éxito!", "Activado correctamente", "success");
            }else{
                swal("Éxito!", "Desactivado Correctamente", "success");
            }
            
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });
}

function btn_mas_vendido(id,tipo){
    var color='';
    var valor=0;
    var texto='';
    if(tipo==1){
        color='red';
        valor=0;
        texto='¿Está seguro de quitar producto de los mas mas vendidos?';
    }else{
        color='green'; 
        valor=1;
        texto='¿Está seguro de agregar producto a los mas mas vendidos?';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: texto,
        type: color,
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Productos/estatus_record",
                    data:{id:id,tipo:valor},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Actuzalizado Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function generarref(){
    $(".addref").each(function() {
        
        var idrow = $(this).data('id');
        var ref = $(this).data('ref');
        if(ref!=''){
            console.log(ref);
            valorreferencia(ref);
        }
    });
}
function valorreferencia(ref){
    var valorref='';
    var valoresStr = ref.toString();
    console.log(valoresStr);

    // Separar la cadena en un arreglo utilizando la coma como delimitador
    var valoresArr = valoresStr.split(',');
    console.log(valoresArr.length);
    // Recorrer el arreglo con un bucle for
    for (var i = 0; i < valoresArr.length; i++) {
        // Convertir cada elemento a número (ya que son cadenas)
        var valorNumero = parseInt(valoresArr[i]);
        valorref=valorreferencia(valorNumero);
        // Hacer algo con el valor, por ejemplo, imprimirlo en la consola
        console.log('Valor en la posición ' + i + ': ' + valorNumero);
    }
    return valorref;
}

function modal_precios(id){
    $('.text_precios').html('');
    $('#modalprecios').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Productos/viewprecios_list',
        data: {id:id},
        success:function(data){
            $('.text_precios').html(data);
        }
    });
}

function codigo_modal(id){
    $('#idproetiqueta').val(id);
    $('#modalcodigobarras').modal('show');
    var codigo = $('.idreg_'+id).data('codigo');
    var producto = $('.idreg_'+id).data('producto');
    var categoria = $('.idreg_'+id).data('categoria');
    var precio = $('.idreg_'+id).data('precio');
    var preciotxt=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(precio);
    $('.codigo').html(codigo);
    $('.producto').html(producto);
    $('.categoria').html(categoria);
    $('.precio').html(preciotxt);
    $('#iframeetiqueta').html('<iframe src="'+base_url+'Etiquetas?id='+id+'&page=1"></iframe>'); 
}
var tipo_s=0;
function modal_serie(id,sucursal){
    $('#modalinfosuc').modal('hide');
    $('#modalseries').modal('show');
    $('.text_series').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Productos/viewseries_list',
        data: {id:id,sucursal:sucursal},
        success:function(data){
            $('.text_series').html(data);
            $('#modalinfosuc').css('z-index', '1040');  // Dejar atrás
            $('#modalseries').css('z-index', '1051');
            setTimeout(function(){ 
                infosucursales(id,tipo_s);
            }, 500); 
        }
    });
}

function setIndex(){
    $('#modalinfosuc').css('z-index', '1051');
}

function modal_lote(id,sucursal){
    $('#modalinfosuc').modal('hide');
    $('#modallote').modal('show');
    $('.text_lote').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Productos/viewlotes_list',
        data: {id:id,sucursal:sucursal},
        success:function(data){
            $('.text_lote').html(data);
            $('#modalinfosuc').css('z-index', '1040');  // Dejar atrás
            $('#modallote').css('z-index', '1051');
            setTimeout(function(){ 
                infosucursales(id,2); //vuelve a traer los datos del modal -- NO SE OCULTA EL MODAL DE DETALLES, COMO EL DE SERIES
            }, 500); 
        }
    });
}
function infosucursales(id,tipo){
    $('#modalinfosuc').modal('show');
    tipo_s=tipo;
    $.ajax({
        type:'POST',
        async: false,
        url: base_url+"Productos/getStockSucs",
        data: {id:id, tipo:tipo },
        success:function(response){
            $('.infosucursal').html(response);
        }
    });
}