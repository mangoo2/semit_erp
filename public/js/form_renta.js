var base_url = $('#base_url').val();
var validar_razon=0;
var validar_rfc=0; 
var validar_input=0;
var idg=0;

$(document).ready(function() {
    if($("#id").val()!=0){
        idg =$("#id").val();
        console.log("idg: "+idg);
    }
    search_servicio();
    $("#periodo").on("change",function(){
        if($(this).val()==0){
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: false});
            $("#periodo option[value=3]").attr({selected: false});
        }if($(this).val()==1){
            $("#periodo option[value=0]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: false});
            $("#periodo option[value=3]").attr({selected: false});
        }if($(this).val()==2){
            $("#periodo option[value=0]").attr({selected: false});
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=3]").attr({selected: false});
        }if($(this).val()==3){
            $("#periodo option[value=0]").attr({selected: false});
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: false});
        }

        console.log("preriodo select: "+$(this).val());
        get_periodo($("#id_servicio option:selected").val(),$(this).val());
        asignaFecha();
    });
    $("#fecha_inicio").on("change",function(){
        if($("#fecha_inicio").val()!="")
            asignaFecha();
    });
    $("#hora_inicio").on("change",function(){
        if($("#hora_inicio").val()!="")
            $("#hora_fin").val($("#hora_inicio").val());
    });

    $("#deja_docs").on("change",function(){
        compDocs();
    });
    compDocs();

    search_cliente();
    //get_periodo($("#id_servicio option:selected").val(),$("#periodo option:selected").val());

    $('#id_serie').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un equipo/serie',
        allowClear: true,
        ajax: {
            url: base_url+'Rentas/searchSeries',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        //text: element.producto+" / "+element.serie
                        text: element.producto,
                        concentrador: element.concentrador
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#id_prod").val(data.id);
        $("#concentra").val(data.concentrador);
        modal_series(data.id);
    });

    ////////////////////////////////////
    var preview =""; typePDF = "false"; 
    imgdet={type:"image", url: ""+base_url+"Rentas/deleteDoc/"+$("#id_file").val(), caption: $("#file_aux").val(), key:1};
    if($("#file_aux").val()!=""){
        preview=''+base_url+"uploads/ine/"+$("#file_aux").val()+'';
        
        ext = $("#file_aux").val().split('.');
        if(ext[1].toLowerCase()!="pdf"){
            typePDF = "false";  
        }else{
            imgdet = {type:"pdf", url: base_url+"Rentas/deleteDoc/"+$("#id_file").val(), caption: $("#file_aux").val(), key:1};
            typePDF = "true";
        }
    }
    $("#doc_ine").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
        browseLabel: 'Seleccionar documento',
        uploadUrl: base_url+'Rentas/cargaimagen',
        maxFilePreviewSize: 4096,
        maxFileSize: 4096, //4mb
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+preview+'',    
        ],
        initialPreviewAsData: typePDF,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdet
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"doc_ine",
                    id_renta:idg
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    var previewp =""; typePDF2 = "false"; 
    imgdet2 = {type:"image", url: ""+base_url+"Rentas/deleteDoc/"+$("#id_file2").val(), caption: $("#file_aux2").val(), key:2}
    if($("#file_aux2").val()!=""){
        previewp=''+base_url+"uploads/comprobante/"+$("#file_aux2").val()+'';
        
        ext = $("#file_aux2").val().split('.');
        if(ext[1].toLowerCase()!="pdf"){
            typePDF2 = "false";  
        }else{
            imgdet2 = {type:"pdf", url: base_url+"Rentas/deleteDoc/"+$("#id_file2").val(), caption: $("#file_aux2").val(), key:2};
            typePDF2 = "true";
        }
    }
    $("#doc_comp").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp","pdf"],
        browseLabel: 'Seleccionar documento',
        uploadUrl: base_url+'Rentas/cargaimagen',
        maxFilePreviewSize: 4096,
        maxFileSize: 4096, //4mb
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+previewp+'',    
        ],
        initialPreviewAsData: typePDF2,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            imgdet2
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"doc_comp",
                    id_renta:idg
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    searchCliEdit();

    $("#costo_entrega").on("change",function(){
        var cost_ent=$("#costo_entrega").val();
        $("#costo_recolecta").val(cost_ent);
    }); 

    $("#costo_entrega,#costo_recolecta").on("change",function(){
        compDocs();
    });

    /*setTimeout(function(){ 
        $(".kv-file-upload").hide(); //ocultar boton upload
    }, 1000);

    $("#doc_comp,#doc_ine").on("change",function(){
        setTimeout(function(){ 
            $(".kv-file-upload").hide(); //ocultar boton upload
        }, 1000);
    });*/
    
});

function modal_series(id){
    $('#modal_series').modal('show');
    $("#id_prod").val(id);
    $('#id_serie').val(null).trigger('change');
    $("#id_serie option[value='"+id+"']").remove();
    $.ajax({
        type: 'POST',
        url: base_url+'Rentas/searchSeriesEquipo',
        data: { id: id },
        async: false,
        success: function(data) {
            //console.log(data);
            $(".series_tbody").html(data);
        }
    });
}

function asignaSerie(){
    var cont_selser=0;
    var TABLA = $("#table_series tbody > tr");
    TABLA.each(function(){
        if($(this).find("input[id*='add_serie']").is(":checked")==true){
            var idserie=$(this).find("input[id*='idserie']").val();
            var sserie=$(this).find("input[id*='sserie']").val();
            //console.log("idserie: "+idserie);
            var newOption = new Option("Serie: "+sserie,idserie, false, false);
            $('#id_serie').append(newOption).trigger('change');   
            $('#modal_series').modal('hide');
            cont_selser++;
        }
    });
    if(cont_selser==0){
        swal("Álerta!", "Elija una serie", "warning");
    }
}

/*function new_cli(){
    var win = window.open(base_url+"Clientes/registro", "Nuevo Cliente", "width=780, height=612");
    var tiempo= 0;
    var interval = setInterval(function(){
        //Comprobamos que la ventana no este cerrada
        if(win.closed !== false) {
            window.clearInterval(interval)
            //console.log("Tiempo total: ${tiempo} s ");
            search_cliente();
        } else {
            tiempo +=1;
        }
    },1000);
}

function edit_cli(){
    var id_cli=$('#id_cliente option:selected').val();
    if(id_cli!=undefined){
        var win = window.open(base_url+"Clientes/registro/"+id_cli, "Nuevo Cliente", "width=780, height=612");
    }else{
        swal("Álerta!", "Elija un cliente a editar", "warning");
    }
}*/

function modal_user(){
    $('#modal_user_data').modal('show');
}

function guardar_registro_datos(){
    var form_register = $('#form_registro_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datos").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Rentas/registro_datos_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log("data: "+data);
                var array = $.parseJSON(data);

                var idp=parseFloat(array.id);
                var idf=parseFloat(array.idf);
                //console.log("idp: "+idp);
                //console.log("idf: "+idf);
                $('#idreg1').val(idp);
                $('#idreg2').val(idp);
                $('#idreg3').val(idp);
                $('#idreg4').val(idp);
                //swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);

                swal("Éxito!", "Se a guardado el cliente", "success");
                //$('#modal_user_data').modal('hide');
                var data = {
                    id: idf,
                    text: $('#razon_social').val()
                };
        
                var newOption = new Option(data.text, data.id, false, false);
                $('.idrazonsocial_cliente').append(newOption).trigger('change');
                $('.idrazonsocial_cliente').val(parseFloat(idf)).trigger('change.select2');
                $('#idrazonsocial option:selected').attr("data-id_cli",idp);
                setTimeout(function(){ 
                    validar_icono_user();
                }, 1000);

            }
        });    
    }   
}
function verificar_correo(){
    $.ajax({
        type: 'POST',
        url: base_url+'Rentas/validar_correo',
        data: {
            correo:$('#correo').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_correo=1;
                swal("¡Atención!", "El correo electrónico ya existe", "error");
            }else{
                validar_correo=0;
            }
        }
    });
}
function guardar_registro_datos_fiscales(){
    var idreg2=$('#idreg2').val();
    
    $("#form_registro_datos_fiscales").validate().destroy(); 

    if(idreg2!=0){
        var form_register = $('#form_registro_datos_fiscales');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        if($('#activar_datos_fiscales').is(':checked')){
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    razon_social:{
                    required: true,
                    },
                    rfc:{
                    required: true,
                    },
                    cp:{
                    required: true,
                    },
                    RegimenFiscalReceptor:{
                    required: true,
                    },
                    uso_cfdi:{
                    required: true,
                    },
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Rentas/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }else{
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Rentas/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }   
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function guardar_registro_pagos(){
    var idreg2=$('#idreg4').val();
    if(idreg2!=0){
        var form_register = $('#form_registro_pagos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                diascredito:{
                  required: true,
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_pagos").valid();
        if($valid) {
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Rentas/registro_datos_pago',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){  
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_registro').attr('disabled',false);
                        //window.location = base_url+'Clientes';
                    }, 1000);

                }
            });      
        }   
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function guardar_registro_usuario(){
    var idreg3=$('#idreg3').val();
    if(idreg3!=0){
        var form_register = $('#form_registro_usuario');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Usuario:{
                  required: true,
                  minlength: 4
                },
                contrasena:{
                  required: true,
                  minlength: 5
                },
                contrasena2: {
                    equalTo: contrasena,
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_usuario").valid();
        if($valid) {
            if(validar_user==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Rentas/registro_datos',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        var idp=data;
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //window.location = base_url+'Clientes';
                        }, 1000);

                    }
                });    
            }else{
                swal("¡Atención!", "El usuario ya existe", "error");
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }   
}
function tipo_pago(num){
    if(num==1){
       $('.diaspagotxt').css('display','block');
       $('.saldopagotxt').css('display','none');
    }else if(num==2){
       $('.diaspagotxt').css('display','none');
       $('.saldopagotxt').css('display','block');
    }
}
function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Rentas/validar',
        data: {
            usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}
var pass_aux=0;
function clickoverpass(){
    var obj = document.getElementById('contrasena');
    var obj2 = document.getElementById('contrasena2');
    if(pass_aux==0){
        pass_aux=1;  
        obj.type = "text";
        obj2.type = "text";
    }else{
        pass_aux=0;
        obj.type = "password";
        obj2.type = "password";
    }  

}
function guardar_registro_datosx(){
    var form_register = $('#form_registro_datosx');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datosx").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Rentas/registro_datos_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                var idp=parseFloat(array.id);
                var idf=parseFloat(array.idf);
                //console.log("idp: "+idp);
                //console.log("idf: "+idf);
                $('#idreg1').val(idp);
                $('#idreg2').val(idp);
                $('#idreg3').val(idp);
                $('#idreg4').val(idp);

                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);

                swal("Éxito!", "Se a guardado el cliente", "success");
                //$('#modal_user_data').modal('hide');
                var data = {
                    id: parseFloat(idp),
                    text: $('#razon_socialx').val()
                };
        
                setTimeout(function(){ 
                    validar_icono_user();
                }, 1000);

            }
        });    
    }   
}
function validar_icono_user(){
    var razon_social=$('#idrazonsocial option:selected').val();
    if(razon_social==2){
        $('.icon_user').css('color','#009bdb');
    }else{
        $('.icon_user').css('color','red');
    } 
}
/* ********************************* */
function searchCliEdit(){
    $('#input_search_clientex').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar cliente',
        dropdownParent: $('#modal_cliente'),
        allowClear: true,
        ajax: {
            url: base_url+'Rentas/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        //id: element.id,
                        text: element.razon_social,
                        desactivar:element.desactivar,
                        motivo: element.motivo,
                        id_cf: element.id,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("id_cf: "+data.id_cf);
        $("#input_search_clientex option:selected").attr('data-id_cf',data.id_cf);
        if(data.desactivar==1){
            setTimeout(function(){ 
                $('#input_search_clientex').val(null).trigger('change');
            }, 2000);
            swal("¡Atención!", "El cliente seleccionado se encuentra inactivo, favor de contactar al administrador.  Motivo de suspensión: "+data.motivo, "error");
        }else{
            obtenercliente(data.id);
        }
        
    });
}
function buscar_cliente(){
    $('#modal_cliente').modal('show');
    setTimeout(function(){ 
        $('#input_search_clientex').select2('open');
        $('.select2-search__field').focus();
    }, 2000);
}

function add_venta_select(){
    var clienteid=$('#input_search_clientex option:selected').val();
    var id_razon=$('#input_search_clientex option:selected').data("id_cf");
    //var cliente=$('#input_search_clientex option:selected').text();
    var cliente=$('#razon_socialx').val();
    var newOption = new Option(cliente,clienteid, false, false);
    //var newOption = new Option(cliente,id_razon, false, false);
    $('#id_cliente').append(newOption).trigger('change');
    $('#id_cliente').val(parseFloat(clienteid)).trigger('change.select2');
    //$('#id_cliente').val(parseFloat(id_razon)).trigger('change.select2');
    $('#modal_cliente').modal('hide');
    if(validar_input==0){
        $('#input_barcode').show('show');
        $('#input_search').hide('show');
        $('.select_option_search .select2-container').hide('show');
        setTimeout(function(){ 
            $('#input_barcode').focus();
        }, 1000);
    }else{
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 2000);
    }
}
function limpiar_select_cliente(){
    $('#input_search_clientex').val(null).trigger('change');
    $('.text_cliente').html('');
}
function obtenercliente(id){
    $('.text_cliente').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Rentas/get_data_cliente",
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_cliente').html(data); 
            regimefiscalx(); 
            setTimeout(function(){ 
                var cliuso=$('#uso_cfdix').data('cliuso');
                if(cliuso != ''){
                    $('#uso_cfdix').val(cliuso);
                }
            }, 2000);
        }
    }); 
}
function regimefiscalx(){
    $( "#uso_cfdix").val(0);
    $( "#uso_cfdix option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptorx option:selected').val();
    $( "#uso_cfdix ."+regimen ).prop( "disabled", false );
}
function validar_razon_socialx(){
    var razon_social=$('#razon_socialx').val();
    if(razon_social!=""){
        $.ajax({
            type:'POST',
            url: base_url+"Rentas/get_validar_razon_social",
            data: {
                razon_social:razon_social,
            },
            success: function (response){
                if (response == 1) {
                    validar_razon=1;
                    swal("¡Atención!", "La razón social ya existe", "error");
                }else{
                    validar_razon=0;
                }
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                
            }
        });
    }
}
function validar_rfc_clientex(){
    var rfc=$('#rfcx').val();
    $.ajax({
        type:'POST',
        url: base_url+"Rentas/get_validar_rfc",
        data: {
            rfc:rfc,
        },
        success: function (response){
            if (response == 1) {
                validar_rfc=1;
                swal("¡Atención!", "El rfc ya existe", "error");
            }else{
                validar_rfc=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function guardar_registro_datos_fiscalesx(){
    var idreg2=$('#idreg2x').val();
    
    $("#form_registro_datos_fiscalesx").validate().destroy(); 

    if(idreg2!=0){
        var form_register = $('#form_registro_datos_fiscalesx');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        if($('#activar_datos_fiscalesx').is(':checked')){
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    razon_social:{
                    required: true,
                    },
                    rfc:{
                    required: true,
                    },
                    cp:{
                    required: true,
                    },
                    RegimenFiscalReceptor:{
                    required: true,
                    },
                    uso_cfdi:{
                    required: true,
                    },
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscalesx").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Rentas/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }else{ 
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: { },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscalesx").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Rentas/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }  
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function cambiaCP2(){
    var cp = $("#codigo_postal").val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Rentas/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                //$(".fis_estado_"+id).val(array[0].estado);
                //$(".fis_ciudad_"+id).val(array[0].ciudad);
                cambia_cp_colonia2();
            }
        });

    }
}
function cambia_cp_colonia2(){
    var cp = $("#codigo_postal").val();
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Rentas/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Rentas/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }
            }
        });
    }else{
        //$(".fis_estado_"+id).attr("readonly",false);
    }
}
/* ************************************ */

function compDocs(){
    if($("#deja_docs option:selected").val()=="0"){ //no deja docs
        $(".cont_docs").hide("slow");
        $("#cont_depo").show();
        setTimeout(function(){ 
            get_periodo($("#id_servicio option:selected").val(),$("#periodo option:selected").val());
        }, 1500);
    }
    else{
        $(".cont_docs").show("slow");
        $("#deposito").val(0);
        var costo = Number($("#costo").val());
        var costo_entrega = Number($("#costo_entrega").val());
        var costo_recolecta = Number($("#costo_recolecta").val());
        $("#total").val(costo+costo_entrega+costo_recolecta);
        $("#cont_depo").hide();
        setTimeout(function(){ 
            $(".kv-file-upload").hide(); //ocultar boton upload
        }, 1000);

    }
}

function search_cliente(){
    $('#id_cliente').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        allowClear: true,
        ajax: {
            url: base_url+'Rentas/searchclientes',
            dataType: "json",
            delay: 300,
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        text: "ID: "+element.id_cliente+" / "+element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
    });
}

function search_servicio(){
    $('#id_servicio').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        allowClear: true,
        ajax: {
            url: base_url+'Rentas/searchServicios',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.clave+" "+element.descripcion,
                        descripcion: element.descripcion,
                        check1: element.check1,
                        check15: element.check15,
                        check30: element.check30
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if(data.check1=="1"){
            $("#periodo").val("1");
            $("#periodo option[value=1]").attr({selected: true});
        }else{
            $("#periodo option[value='1']").attr("disabled",true);
        }

        if(data.check15=="1"){
            $("#periodo").val("2");
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: true});
        }else{
            $("#periodo option[value='2']").attr("disabled",true);
        }

        if(data.check30=="1"){
            $("#periodo").val("3");
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: false});
            $("#periodo option[value=3]").attr({selected: true});
        }else{
            $("#periodo option[value='3']").attr("disabled",true);
        }
        if(data.check1=="1" && data.check15=="1" && data.check30=="1"){
            $("#periodo option[value=0]").attr({selected: true});
            $("#periodo option[value=1]").attr({selected: false});
            $("#periodo option[value=2]").attr({selected: false});
            $("#periodo option[value=3]").attr({selected: false});
        }else{
            get_periodo(data.id,$("#periodo option:selected").val());
        }
        
    });
}

function get_periodo(id,periodo){
    //console.log("periodo: "+periodo);
    if($("#id_servicio option:selected").val()!=undefined && periodo!="0"){
        $.ajax({
            data:{id:id, periodo:periodo},
            type: 'POST',
            url: base_url+'Rentas/get_periodo_servs',
            success: function(result){
                var array=$.parseJSON(result);
                //console.log("array check30: "+array.check30);
                if(array.check1=="1" && periodo=="1"){
                    $("#periodo").val("1");
                    $("#periodo option[value=1]").attr({selected: true});
                    $('#costo').val(array.costo);
                    $('#deposito').val(array.depo);
                    costo=array.costo;
                    depo=array.depo;
                }else{
                    $("#periodo option[value='1']").attr("disabled",true);
                }

                if(array.check15=="1" && periodo=="2"){
                    $("#periodo").val("2");
                    $("#periodo option[value=2]").attr({selected: true});
                    $('#costo').val(array.costo15);
                    $('#deposito').val(array.depo15);
                    costo=array.costo15;
                    depo=array.depo15;
                }else{
                    $("#periodo option[value='2']").attr("disabled",true);
                }

                if(array.check30=="1" && periodo=="3"){
                    $("#periodo").val("3");
                    $("#periodo option[value=3]").attr({selected: true});
                    $('#costo').val(array.costo30);
                    $('#deposito').val(array.depo30);
                    costo=array.costo30;
                    depo=array.depo30;
                    //console.log("array costo30: "+array.costo30);
                    //console.log("array depo30: "+array.depo30);
                }else{
                    $("#periodo option[value='3']").attr("disabled",true);
                }
                if(array.check1=="1" && array.check15=="1" && array.check30=="1"){
                    $("#periodo option[value='1']").attr("disabled",false);
                    $("#periodo option[value='2']").attr("disabled",false);
                    $("#periodo option[value='3']").attr("disabled",false);
                }

                var costo_entrega = Number($("#costo_entrega").val());
                var costo_recolecta = Number($("#costo_recolecta").val());
                $('#total').val(Number(depo)+Number(costo)+costo_entrega+costo_recolecta);

                if($('#fecha_inicio').val()!=""){
                    setTimeout(function(){ 
                        asignaFecha();
                    }, 1500);  
                }
            }
        });
    }if(periodo==0){
        $('#costo').val("");
        $('#deposito').val("");
        $('#total').val("");
    }
}

function asignaFecha(){
    /*var fecha = new Date($('#fecha_inicio').val());
    var dias = $("#periodo option:selected").val();
    if($("#periodo option:selected").val()=="2"){
        dias=15;
    }if($("#periodo option:selected").val()=="3"){
        dias=30;
    }
    if(dias==1){
        dias=2;
    }
    console.log("dias: "+dias);
    fecha.setDate(fecha.getDate() + Number(dias));
    console.log("fecha: "+fecha);
    var diaf = fecha.getDate();
    var mesf = fecha.getMonth();
    if(diaf<10){
        diaf="0"+diaf;
    }
    if(mesf<10){
        mesf="0"+mesf;
    }

    var fechaFinal=(diaf+"/"+mesf)+"/"+fecha.getFullYear();
    var fechaFinal2=fecha.getFullYear()+"-"+(mesf+"-"+fecha.getDate());
    console.log('fecha especial2: '+fechaFinal2);
    $('#fecha_fin').val(fechaFinal2);
    //console.log('fecha especial: '+fechaFinal);*/
    calcular();
}

function calcular() {
    var TuFecha = new Date($('#fecha_inicio').val());
    var dias = parseInt($("#periodo option:selected").val());

    if(dias==1){
        dias = 2;
    }
    if($("#periodo option:selected").val() == "2"){
        dias = 16;
    }
    if($("#periodo option:selected").val() == "3"){
        TuFecha.setMonth(TuFecha.getMonth() + 1);  // Sumar un mes
        TuFecha.setDate(TuFecha.getDate() + 1);  // Sumar 1 día adicional
        dias = 0;
    }
    if($("#periodo option:selected").val() != "3"){
        TuFecha.setDate(TuFecha.getDate() + dias); 
    }

    var diaf = TuFecha.getDate();
    var mesf = (TuFecha.getMonth() + 1);

    if (diaf < 10) {
        diaf = "0" + diaf;  // Añadir un cero si el día es menor que 10
    }
    if (mesf < 10) {
        mesf = "0" + mesf;  // Añadir un cero si el mes es menor que 10
    }

    // Formato de la nueva fecha (YYYY-MM-DD)
    var nva_fecha = TuFecha.getFullYear() + "-" + mesf + '-' + diaf;

    // Asignar la fecha calculada al campo de fecha final
    $('#fecha_fin').val(nva_fecha);

}

function save() {
    var form_register = $('#form_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_cliente: {
                required: true
            },id_servicio: {
                required: true
            },
            id_serie: {
                required: true
            },
            fecha_inicio: {
                required: true
            },
            hora_inicio: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        },       
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_datos").valid();
    if($valid) {
        if($("#requiere_factura").is(":checked")==true)
            requiere_factura=1;
        else
            requiere_factura=0;
        //$('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Rentas/save_datos',
            data: form_register.serialize()+"&requiere_factura="+requiere_factura,
            beforeSend: function(){
                $('.btn_registro').attr('disabled',true);
            },
            success:function(result){
                if(result!="NO"){
                    $("#id").val(result);
                    idg = result;
                    setTimeout(function(){ 
                        if($("#doc_ine").val()!=""){
                            $('#doc_ine').fileinput('upload');
                        }
                        if($("#doc_comp").val()!=""){
                            $('#doc_comp').fileinput('upload');
                        }
                    }, 1000);
                    
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){
                        window.location = base_url+'Rentas';
                    }, 2500);
                }else{
                    swal("Atención!", "No se cuenta con stock suficiente para kit", "warning");
                    $('.btn_registro').attr('disabled',false);
                }
            }
        });
    }
}

function activar_campo(){
    if($('#desactivar').is(':checked')){
        $('.motivo_txt').css('display','block');
    }else{
        $('.motivo_txt').css('display','none');
    }
}

function regimefiscal(){
    $( "#uso_cfdi").val(0);
    $( "#uso_cfdi option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptor option:selected').val();
    $( "#uso_cfdi ."+regimen ).prop( "disabled", false );
}

function verificar_usuariox(){
    $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/validar',
        data: {
            usuario:$('#usuariox').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}

function guardar_registro_usuariox(){
    var idreg3=$('#idreg3x').val();
    if(idreg3!=0){
        var form_register = $('#form_registro_usuariox');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Usuario:{
                  required: true,
                  minlength: 4
                },
                contrasenax:{
                  required: true,
                  minlength: 5
                },
                contrasena2x: {
                    equalTo: contrasenax,
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_usuariox").valid();
        if($valid) {
            if(validar_user==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Rentas/registro_datosx',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        var idp=data;
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //window.location = base_url+'Clientes';
                        }, 1000);

                    }
                });    
            }else{
                swal("¡Atención!", "El usuario ya existe", "error");
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }   
}