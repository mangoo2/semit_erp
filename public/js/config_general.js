var base_url = $('#base_url').val();
var fechaactual = $('#fechaactual').val();
var tabla;
$(document).ready(function() {
    tabla=$("#table_datos").DataTable();
    load();
    
});
function load(){
    
    tabla.destroy();
    tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"ConfigGeneral/getlistado",
            type: "post",
            "data":{
                suc:0
            },
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            
            {"data":"UsuarioID"},
            {"data":"Usuario"},
            {"data":"nombre"},
            {"data":"name_suc"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.suc_date==fechaactual){
                    html='Cambio temporal a PV-Venta';
                }
                //html='<a onclick="viewcorte('+row.id+')" class="btn btn-primary"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.suc_date==fechaactual){
                        var cambio=0;
                        var icon='<i class="fas fa-times"></i>';
                        var title='Quitar permiso';
                        var color='btn-danger';
                    }else{
                        var cambio=1;
                        var icon='<i class="fas fa-arrows-alt-h"></i>';
                        var title='Cambiarle permiso a PV-Venta';
                        var color='btn-primary';
                    }
                    html='<a onclick="cambiarpermiso('+row.UsuarioID+','+cambio+')" class="btn '+color+'" title="'+title+'">'+icon+'</a>';
                    return html;
                }
            },
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        
    
    });
}
function cambiarpermiso(id,tipo){
    var title='';
    if(tipo==0){
        var title='Quitar permiso';
    }
    if(tipo==1){
        var title='Cambiarle permiso a PV-Venta';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: title,
        content: '¿Desea realizar el cambio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"ConfigGeneral/cambiarpermiso",
                    data:{
                        id:id,
                        tipo:tipo
                    },
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        toastr.success('Cambio realizado');
                        load();
                    }
                }); 
            },
            cancelar: function () {
                
            }
        }
    });
}