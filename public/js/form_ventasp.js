var base_url = $('#base_url').val();
var tabla;
var tabla_r;
var count_row=0;
var cambio=0;
var validar_razon=0;
var validar_rfc=0; 
var estatus_turno=$('#estatus_turno').val();
var validar_input=0;
var validar_input_renta=0;
var tipo_codigo=0; var function_name="searchproductos";
var tipo_venta_renta=1;
var depositogarantia=0;
$(document).ready(function() {
    $("#modal_notificacion").on("hidden.bs.modal", function () {
        setTimeout(function(){ 
            $('#modal_user_data').css('overflow-x','hidden');
            $('#modal_user_data').css('overflow-y','auto');
            $('#modal_cliente').css('overflow-x','hidden');
            $('#modal_cliente').css('overflow-y','auto');
        }, 1000);
    });
    $('.ventacredito').hide('show');
    if(estatus_turno==0){
        abrir_turno();
    }
    input_search();
    input_searchx();
    $('.select2-selection__placeholder').css('color','white');

    tabla=$("#table_datos").DataTable(
    	{
        "paging": false,
        "searching": false,
        "info": false,
        "ordering":false
    });

    tabla_r=$("#table_datos_rentas").DataTable(
        {
        "paging": false,
        "searching": false,
        "info": false,
        "ordering":false
    });
    selectcliente();
    /*$('#save_form_df').click(function(event) {
        $( ".save_form_df" ).prop( "disabled", true );
        setTimeout(function(){ 
             $(".save_form_df" ).prop( "disabled", false );
        }, 3000);
        var form_df = $('#form_cliente');
        if(form_df.valid()){
            //var datos = form_df.serialize();
            if(validar_razon==0){
                if(validar_rfc==0){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Ventasp/insercliente",
                        data: {
                            razon_social:$('#razon_social').val(),
                            rfc:$('#rfc').val(),
                            cp:$('#cp').val(),
                            RegimenFiscalReceptor:$('#RegimenFiscalReceptor').val(),
                            uso_cfdi:$('#uso_cfdi').val(),
                            correo:$('#correo').val(),
                            direccion:$('#direccion').val()
                        },
                        success: function (response){
                            swal("Éxito!", "Se a guardado el cliente", "success");
                            $('#modal_user_data').modal('hide');
                            var data = {
                                id: parseFloat(response),
                                text: $('#razon_social').val()
                            };
                    
                            var newOption = new Option(data.text, data.id, false, false);
                            $('.idrazonsocial_cliente').append(newOption).trigger('change');
                            $('.idrazonsocial_cliente').val(parseFloat(response)).trigger('change.select2');
                            setTimeout(function(){ 
                                validar_icono_user();
                            }, 1000);
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
                        }
                    });
                }else{
                    swal("¡Atención!", "El rfc ya existe", "error");
                }
            }else{
                swal("¡Atención!", "La razón social ya existe", "error");
            }
        }else{
            swal("¡Atención!", "Faltan campos requeridos", "error");
        }
    });*/

    setTimeout(function(){ 
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        $('.select2-selection__placeholder').css('color','white');
        validar_input=1;
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 500);
    }, 500);

    $('#search_input_barcode').click(function(event) {
        $('#input_barcode').show('show');
        $('#input_search').hide('show');
        $('.select_option_search .select2-container').hide('show');
        $('.icono1').css('color','red');
        $('.icono2').css('color','white');
        validar_input=0;

        setTimeout(function(){ 
            $('#input_barcode').focus();
        }, 1000);
    });
    $('#search_input_search').click(function(event) {
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        $('.select2-selection__placeholder').css('color','white');
        validar_input=1;
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    });
    ////////////Verificador////////////
    $('#search_input_barcodex').click(function(event) {
        $('#input_barcodex').show('show');
        $('#input_searchx').hide('show');
        $('.select_option_search .select2-container').hide('show');
        $('.iconox1').css('color','red');
        $('.iconox2').css('color','white');
        setTimeout(function(){ 
            $('#input_barcodex').focus();
        }, 1000);
    });
    $('#search_input_searchx').click(function(event) {
        $('#input_barcodex').hide('show');
        $('#input_searchx').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.iconox1').css('color','white');
        $('.iconox2').css('color','red');
        setTimeout(function(){ 
            $('#input_searchx').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    });

    $('#input_searchserv').click(function(event) {
        $('#input_barcodex').hide('show');
        $('#input_searchx').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.iconox1').css('color','white');
        $('.iconox2').css('color','red');
        setTimeout(function(){ 
            $('#input_searchserv').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    });

    ////////////////////////
    $("#input_barcode").keypress(function(e) {
        if(tipo_venta_renta==2){
            if(e.which == 13) {
                obtenerdatosrenta();
            }    
        }else{
            if(e.which == 13) {
                obtenerdatosproducto();
            }    
        }
    });

    $("#input_barcodex").keypress(function(e) {
        if(e.which == 13) {
            tabla_precios_verificador(1);
        }
    });

    viewproductos();
    //viewservicios(); //se agrega line para prueba y se comenta la de ventas crl  550
    //https://www.techiedelight.com/es/create-keyboard-shortcuts-with-javascript-jquery-hotkeys-mousetrap-library/
    document.addEventListener("keydown", function(event) {
        //console.log(event.ctrlKey);
        //console.log(event.code);
        if (event.altKey && event.code === "Digit1") {
            //console.log('Alt + 1 pressed!');
            deleteprorow(count_row);
        }
        if (event.altKey && event.code === "Numpad1") {
            deleteprorow(count_row);
        }
        if (event.altKey && event.code === "Digit2") {
            realizarventa();
        }
        if (event.altKey && event.code === "Numpad2") {
            realizarventa();
        }
        if (event.altKey && event.code === "Digit3") {
            modal_user();
        }
        if (event.altKey && event.code === "Numpad3") {
            modal_user();
        }
        if (event.altKey && event.code === "Digit4") {
            limpiartabla();
        }
        if (event.altKey && event.code === "Numpad4") {
            limpiartabla();
        }
        /*se comento por que se coloco de manera general
        if (event.altKey && event.code === "Digit5") {
            //buscar_precio();
            modal_e_y_p();
        }
        if (event.altKey && event.code === "Numpad5") {
            //buscar_precio();
            modal_e_y_p();
        }*/
        if (event.altKey && event.code === "Digit6") {
            buscar_cliente();
        }
        if (event.altKey && event.code === "Numpad6") {
            buscar_cliente();
        }

        if (event.altKey && event.code === "Digit7") {
            window.location.href = base_url+"Listaventas"; 
        }
        if (event.altKey && event.code === "Numpad7") {
            window.location.href = base_url+"Listaventas"; 
        }

        if (event.altKey && event.code === "Digit8") {
            $('#input_barcode').hide('show');
            $('#cont_input_search').show('show');
            $('#cont_input_searchrec').hide('show');
            $('#cont_input_searchrent').hide('show');
            $('#cont_input_searchserv').hide('show');
            input_search();
            tipo_venta_renta=1;
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            validar_input=1;
            setTimeout(function(){
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1500);
            $('.tabla_v').css('display','block');
            //$('.tabla_r').css('display','none');
            //$('.tabla_s').css('display','none');
            //$('.tipo_venta_renta_ocultar').css('display','block');
            $('.tipo_venta_renta_ocultar').show('show');
            $('#venta_prod').prop('checked', true);
        }
        if (event.altKey && event.code === "Numpad8") {
            $('#input_barcode').hide('show');
            $('#cont_input_search').show('show');
            $('#cont_input_searchrec').hide('show');
            $('#cont_input_searchrent').hide('show');
            $('#cont_input_searchserv').hide('show');
            input_search();
            tipo_venta_renta=1;
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            validar_input=1;
            setTimeout(function(){
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1500);
            $('.tabla_v').css('display','block');
            //$('.tabla_r').css('display','none');
            //$('.tabla_s').css('display','none');
            //$('.tipo_venta_renta_ocultar').css('display','block');
            $('.tipo_venta_renta_ocultar').show('show');
            $('#venta_prod').prop('checked', true);
        }

        if (event.altKey && event.code === "Digit9") {
            $('#input_barcode').hide('show');
            $('#cont_input_search').hide('show');
            $('#cont_input_searchrec').show('show');
            $('#cont_input_searchrent').hide('show');
            $('#cont_input_searchserv').hide('show');
            changeUri();
            tipo_venta_renta=3;
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            validar_input=1;
            setTimeout(function(){
                $('#input_searchrec').select2('open');
                $('.select2-search__field').focus();
            }, 1500);
            $('.tabla_v').css('display','block');
            $('#recargas').prop('checked', true);
        }
        if (event.altKey && event.code === "Numpad9") {
            $('#input_barcode').hide('show');
            $('#cont_input_search').hide('show');
            $('#cont_input_searchrec').show('show');
            $('#cont_input_searchrent').hide('show');
            $('#cont_input_searchserv').hide('show');
            changeUri();
            tipo_venta_renta=3;
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            validar_input=1;
            setTimeout(function(){
                $('#input_searchrec').select2('open');
                $('.select2-search__field').focus();
            }, 1500);
            $('.tabla_v').css('display','block');
            $('#recargas').prop('checked', true);
        }

        
    });

    $('#input_barcode').focus();
    
    setTimeout(function(){ 
        tabla_metodo('01','01 Efectivo',1);
        validar_icono_user();
        $('#input_barcode').val('');
    }, 500);
    setTimeout(function(){ 
        tabla_metodo('28','28 Tarjeta de Débito',0); 
    }, 1000);
    setTimeout(function(){ 
        tabla_metodo('04','04 Tarjetas de Crédito',0);
    }, 1500);
       
    $("#monto").keypress(function(e) {
        if(e.which == 13) {
          validar_turno();
        }
    }); 

    $('#input_search_clientex').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar cliente',
        dropdownParent: $('#modal_cliente'),
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchclientes',
            dataType: "json",
            delay: 300,
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idcliente,
                        //id: element.id,
                        text: "ID: "+element.id_cliente+" / "+element.razon_social,
                        desactivar:element.desactivar,
                        motivo: element.motivo,
                        id_cf: element.id,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("id_cf: "+data.id_cf);
        $("#input_search_clientex option:selected").attr('data-id_cf',data.id_cf);
        if(data.desactivar==1){
            setTimeout(function(){ 
                $('#input_search_clientex').val(null).trigger('change');
            }, 2000);
            swal("¡Atención!", "El cliente seleccionado se encuentra inactivo, favor de contactar al administrador.  Motivo de suspensión: "+data.motivo, "error");
        }else{
            obtenercliente(data.id);
        }
        
    });
    
    $('#iframeri').on('hidden.bs.modal', function() {
        if(validar_input==0){
            $('#input_barcode').show('show');
            $('#input_search').hide('show');
            $('.select_option_search .select2-container').hide('show');
            $('.icono1').css('color','red');
            $('.icono2').css('color','white');
            setTimeout(function(){ 
                $('#input_barcode').focus();
            }, 1000);
        }else{
            $('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
        }
    });

    $('#modal_user_data').on('hidden.bs.modal', function() {
        //alert(validar_input);
        if(validar_input==0){
            $('#input_barcode').show('show');
            $('#input_search').hide('show');
            $('.select_option_search .select2-container').hide('show');
            $('.icono1').css('color','red');
            $('.icono2').css('color','white');
            setTimeout(function(){ 
                $('#input_barcode').focus();
            }, 1000);
        }else{
            $('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            $('.select2-selection__placeholder').css('color','white');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
        }
    });

    $('#modal_cliente').on('hidden.bs.modal', function() {
        if(validar_input==0){
            $('#input_barcode').show('show');
            $('#input_search').hide('show');
            $('.select_option_search .select2-container').hide('show');
            $('.icono1').css('color','red');
            $('.icono2').css('color','white');
            setTimeout(function(){ 
                $('#input_barcode').focus();
            }, 1000);
        }else{
            $('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            $('.select2-selection__placeholder').css('color','white');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1000);
        }
    });
    
    $('#modal_resumen').on('hidden.bs.modal', function() {
        if(validar_input==0){
            $('#input_barcode').show('show');
            $('#input_search').hide('show');
            $('.select_option_search .select2-container').hide('show');
            $('.icono1').css('color','red');
            $('.icono2').css('color','white');
            setTimeout(function(){ 
                $('#input_barcode').focus();
            }, 1000);
        }else{
            $('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            $('.select2-selection__placeholder').css('color','white');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
                $('.select2-selection__placeholder').css('color','white');
            }, 1000);
        }
    });
    $('.demo-startpop').popover({
        content: `<div class="row">
                    <div class="col-md-6 class_label">Eliminar partida</div>
                    <div class="col-md-6 alt1"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 class_label">Realizar venta</div>
                    <div class="col-md-6 alt2"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 class_label">Alta de cliente</div>
                    <div class="col-md-6 alt3"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 class_label">Limpiar tabla</div>
                    <div class="col-md-6 alt4"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 class_label">Precios y existencias</div>
                    <div class="col-md-6 alt5"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 class_label">Búsqueda de clientes</div>
                    <div class="col-md-6 alt6"></div>
                </div>`,
        html: true,
        placement: 'right',
        title: 'Atajos',
        trigger: 'click'
    });
    $('#tipo_venta').click(function(event) {
        setTimeout(function(){
        }, 1000);
        var tv=$('#tipo_venta').is(':checked');
        if(tv==true){
            $('#venta_facturada').prop('checked', true);
            $('#table_formapagos').hide();
        }else{
            $('#venta_facturada').prop('checked', false);
            $('#table_formapagos').show();
        }
    });
    $('#venta_facturada').click(function(event) {
        var venf=$('#venta_facturada').is(':checked');
        var tven=$('#tipo_venta').is(':checked');
        if(venf==false){
            if(tven==true){
                $('#venta_facturada').prop('checked', true);
            }
        }
    });

    /* ************************* */
    $("#recargas").on("click",function(){
        $('#input_barcode').hide('show');
        $('#cont_input_search').hide('show');
        $('#cont_input_searchrec').show('show');
        $('#cont_input_searchrent').hide('show');
        $('#cont_input_searchserv').hide('show');
        changeUri();
        tipo_venta_renta=3;
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        validar_input=1;
        setTimeout(function(){
            $('#input_searchrec').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
        $('.tabla_v').css('display','block');
        //$('.tabla_r').css('display','none');
        //$('.tabla_s').css('display','none');
        //$('.tipo_venta_renta_ocultar').css('display','none');
        //$('.tipo_venta_renta_ocultar').hide('show');
    });

    $("#venta_prod").on("click",function(){
        $('#input_barcode').hide('show');
        $('#cont_input_search').show('show');
        $('#cont_input_searchrec').hide('show');
        $('#cont_input_searchrent').hide('show');
        $('#cont_input_searchserv').hide('show');
        input_search();
        tipo_venta_renta=1;
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        validar_input=1;
        setTimeout(function(){
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
        $('.tabla_v').css('display','block');
        //$('.tabla_r').css('display','none');
        //$('.tabla_s').css('display','none');
        //$('.tipo_venta_renta_ocultar').css('display','block');
        $('.tipo_venta_renta_ocultar').show('show');
    });

    $("#rentas").on("click",function(){
        tipo_venta_renta=2;
        var idcliente_aux = $('#idrazonsocial option:selected').val();
        if(idcliente_aux!=2){
            $('#input_barcode').hide('show');
            $('#cont_input_search').hide('show');
            $('#cont_input_searchrec').hide('show');
            $('#cont_input_searchrent').show('show');
            $('#cont_input_searchserv').hide('show');
            changerentas();
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            validar_input=1;
            setTimeout(function(){
                $('#input_searchrent').select2('open');
                $('.select2-search__field').focus();
            }, 1500);
            
            $('.tabla_v').css('display','block');
            //$('.tabla_r').css('display','block');
            //$('.tipo_venta_renta_ocultar').css('display','none');
            $('.tipo_venta_renta_ocultar').hide('show');
            viewrentas();
        }else{
            swal("¡Atención!", "Debes seleccionar un cliente", "error");
            $( "#rentas").prop( "checked",false);
            $( "#venta_prod").prop( "checked",true);
            $('.tabla_v').css('display','block');
            //$('.tabla_r').css('display','none');
            //$('.tabla_s').css('display','none');
            //$('.tipo_venta_renta_ocultar').css('display','block');
            $('.tipo_venta_renta_ocultar').show('show');
        }
    });
    $("#servicios").on("click",function(){
        $('#input_barcode').hide('show');
        $('#cont_input_search').hide('show');
        $('#cont_input_searchrec').hide('show');
        $('#cont_input_searchrent').hide('show');
        $('#cont_input_searchserv').show('show');
        input_search_ser();
        tipo_venta_renta=4;
        $('.select_option_search .select2-container').show('show');
        
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        validar_input=4;
        setTimeout(function(){
            $('#input_searchserv').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
        $('.tabla_v').css('display','block');
        //$('.tabla_v').css('display','none');
        //$('.tabla_r').css('display','none');
        //$('.tabla_s').css('display','block');
        //$('.tipo_venta_renta_ocultar').css('display','block');
        $('.tipo_venta_renta_ocultar').show('show');
        //viewservicios();
        viewproductos();
        $('.select2-selection__clear').css('color','white');
    });

    $("#recargas_modal").on("change",function(){
        tipoTraslado();
    });

    $("#id_tanque").select2({
        dropdownParent: $('#modalverificar'),
    }).on('select2:select', function (e) {
        var data = e.params.data;
        tabla_precios_verificador(2,1);
        setTimeout(function(){ 
            $('#id_tanque').val(null).trigger('change');
        }, 1000);
    });

    $('#iframeri').modal({backdrop: 'static', keyboard: false})

    ////////////////////////////
    $("#id_renta").on("click",function(){
        //getRentasCli();
    });

    if($("#id_renta option:selected").val()!="0" && $("#id_renta option:selected").val()!="" && $("#id_renta").is(":visible")==true){
        getRentaDet();
    }
    if($("#id_mtto option:selected").val()!="0" && $("#id_mtto option:selected").val()!="" && $("#id_mtto").is(":visible")==true){
        //console.log("es visible select mttos, llamna getMttoDet()");
        getMttoDet();
    }

    select_insumo();
    setTimeout(function(){ 
        showTableIns(0);
    }, 1000);
    $('#venta_facturada').change(function(event) {
        var venf=$('#venta_facturada').is(':checked');
        if(venf){
            var pro_datos_sat=1;
            $(".validar_sat").each(function() {
                var uni=$(this).data('uni');
                var ser=$(this).data('ser');
                //console.log('uni:'+uni+' ser:'+ser);
                if(uni=='' || ser==''){
                    //console.log('hay uno sin datos');
                    pro_datos_sat=0;
                }
            });

            if(pro_datos_sat==0){
                $('#venta_facturada').prop({'checked':false});
                //toastr.error('Uno de los Productos no tiene especificado la unidad y el servicio SAT');
                swal("¡Atención!", "Uno de los Productos no tiene especificado la unidad y el servicio SAT", "error");
            }
        }
    });
    load_preventas();
    /*$('#preventas').on('focus', function () { 
        load_preventas();
    }); */
    
    //$('#preventas').off('select2:open');
    setTimeout(function(){ 
        $("#preventas").select2({ placeholder: 'Preventas' });
    }, 1000);
    
    $('#preventas').on('select2:closing', function () {
        load_preventas();
    });

    $('#preventas').on('select2:select', function () {
        //view_presv(); // Carga las opciones al abrir el select
    });
    $('#btn-soldescuento').click(function(event) {
        $('#btn-soldescuento').prop('disabled',true);
        setTimeout(function(){ 
            $('#btn-soldescuento').prop('disabled',false);
        }, 1000);
        soldescuento();
    });
});

function checkTab(number){
    $(".tab"+number).removeClass("active");
}

function showTableIns(tipo){
    //console.log("tipo show tabla ins: "+tipo);
    var cont_serv=0;
    if(tipo==0){
        var TABLAi = $("#table_datos tbody > tr");
        TABLAi.each(function(){
            //console.log("vstipo: "+$(this).find("input[id*='vstipo']").val());
            if($(this).find("input[id*='vstipo']").val()==3){
                cont_serv++;
            }
        });
        //console.log("cont_serv: "+cont_serv);
        if(cont_serv>0){
            $(".tabla_ins").show();
        }else{
            $(".tabla_ins").hide();
        }
    }else{
        $(".tabla_ins").show();
    }
}

function select_insumo(){
    $('#sel_insumo').select2({
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        dropdownParent: $('#modalInsumos'),
        placeholder: 'Buscar un insumo',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchInsumo',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var cont=0;
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' / '+element.nombre + " (stock: "+element.stock_suc+")",
                        tipo: "3_1",
                        id_ps: element.id_ps,
                        stock_suc: element.stock_suc
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#sel_insumo option:selected").attr('data-tipo',data.tipo);
        $("#sel_insumo option:selected").attr('data-id_ps',data.id_ps);
        $("#sel_insumo option:selected").attr('data-stock_suc',data.stock_suc);

        get_insumo();
    });
}

function modalInsumos(id,desc=0,precio=0){
    //console.log("muestra modal de insumos");
    $("#modalInsumos").modal("show");

    setTimeout(function(){ 
        $('#sel_insumo').select2('open');
        $('.select2-search__field').focus();
    }, 500);
    $("#id_servadd").val(id);
    if(desc==0){
        $("#name_servadd").val($(".add_serv_"+id).data("nameserv"));
        $("#precio_servadd").val($(".add_serv_"+id).data("precioserv"));
    }else{
        $("#name_servadd").val(desc);
        $("#precio_servadd").val(precio);
    }
}

function get_insumo(){
    var id =$("#sel_insumo option:selected").val();
    var text_ins =$("#sel_insumo option:selected").text();
    var tipo =$("#sel_insumo option:selected").data("tipo");
    var id_ps =$("#sel_insumo option:selected").data("id_ps");
    var stock =$("#sel_insumo option:selected").data("stock_suc");
    var id_serv = $("#id_servadd").val();
    var html="";
    var repetido=0;

    if(stock<=0){
        swal("Atención!", "Insumo con stock insuficiente", "warning");
        return false;
    }
    var TABLA   = $("#table_insumos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='id_ins']").val(); 
        if (id == prodexi) {
            repetido=1;
            $("#cant_ins").val(Number($("#cant_ins").val())+1);
        }   
    });
    var dis_cant=""; var btn_del="<button class='btn btn-danger' onclick='deleteIns("+id+",0)'> <i class='fa fa-trash' aria-hidden='true'></i></button>";
    if($("#tipo_tec").val()!="1"){
        dis_cant="readonly";
        btn_del="";
    }
    if(repetido==0){
        html="<tr class='row_ins row_ins_"+id+" row_ins_serv_"+$("#id_servadd").val()+"'>\
                <td><input type='hidden' id='id' value='0'><input type='hidden' id='id_ins' value='"+id+"'><input type='hidden' id='id_ps' value='"+id_ps+"'>\
                <input type='hidden' id='tipo' value='"+tipo+"'><input type='hidden' id='id_serv' value='"+id_serv+"'>\
                <input type='hidden' id='stock' value='"+stock+"'>\
                <input "+dis_cant+" class='form-control' id='cant_ins' type='number' value='1'>\
            </td>\
            <td><input class='form-control' readonly type='hidden' id='name_servtab' value='"+$("#name_servadd").val()+" ($"+$("#precio_servadd").val()+")"+"'></td>\
            <td>"+text_ins+"</td>\
            <td>"+btn_del+"</td>\
        </tr>";
        $(".det_ins_serv").append(html);
        $('#sel_insumo').val(null).trigger('change');
    }
}

function addInsumos_save(){
    var $tr = $(".row_ins").clone().removeClass("row_ins").addClass("row_ins_deta");
    $tr.find("#name_servtab").attr("type","text");
    //$(".datos_insumos_servs").after($tr);
    $(".datos_insumos_servs").append($tr);

    setTimeout(function(){ 
        $(".row_ins").remove();
        $('#modalInsumos').modal('hide');
        swal("Éxito!", "Insumo(s) asignado(s) correctamente", "success");
    }, 1000);
    //$(".datos_insumos_servs").html(html); //tabla para recorrer y guarda bitacora de serv en venta
}

/*function addInsumos_save(){
    var DATA  = [];
    var TABLA   = $("#table_insumos tbody > tr");
    
    if($("#table_insumos tbody > tr").length==0){
        swal("¡Atención!","Elije al menos un insumo", "warning");
        return;
    }

    TABLA.each(function(){         
        item = {};
        item ["id_ins"] = $(this).find("input[id*='id_ins']").val();
        item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
        item ["cant_ins"] = $(this).find("input[id*='cant_ins']").val();
        item ["tipo"] = $(this).find("input[id*='tipo']").val();
        item ["id_serv"] = $(this).find("input[id*='id_serv']").val();
        item ["stock"] = $(this).find("input[id*='stock']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);

    var datos = 'array_ins='+aInfo;
    $.ajax({
        data: datos,
        type: 'POST',
        url : base_url+'Ventasp/addInsumos_servicios',
        beforeSend: function(){
            $('.add_insumos').attr('disabled',true);
        },
        success: function(data){
            $('.add_insumos').attr('disabled',false);
            $('#modalInsumos').modal('hide');
            swal("Éxito!", "Asignado correctamente", "success");
            viewInsumos_serv();
        }
    }); 
}

function viewInsumos_serv(){
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/view_insumos_servs",
        data: {
            id:0
        },
        success: function (response){
            //console.log(response);
            $('.datos_insumos_servs').html(response); ///nva tabla de insumos asignados a serv
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}*/

function deleteIns(id,id_bi){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del insumo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if(id_bi>0){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Ventasp/delete_insumo_mttoext",
                        data:{ id:id_bi},
                        statusCode:{
                            404: function(data){
                                alert("No Se encuentra el archivo!");
                            },
                            500: function(){
                                alert("500");
                            }
                        },
                        success:function(data){
                            $(".row_ins_serv_"+id).remove();
                        }
                    }); 
                    
                }else{
                     $(".row_ins_"+id).remove();
                }
            },
            cancelar: function (){
                
            }
        }
    });
}

function getRentasCli(){
    if($("#idrazonsocial option:selected").val()!=undefined && $("#idrazonsocial option:selected").val()!="2"){
        $.ajax({
            type:'POST',
            url: base_url+"Ventasp/getRentas",
            data:{ id_cli: $("#idrazonsocial option:selected").data("id_clidcli") },
            success:function(response){
                var array = $.parseJSON(response);
                if(array.length>0){
                    array.forEach(function(i) {
                        $("#id_renta").append('<option value="'+i.id+'"># '+i.id+' / '+i.descripcion+'</option>');
                    });
                }  
            }
        }); 
    }else{
        swal("¡Atención!", "Seleccione antes un cliente", "error");
    }
}

function getRentaDet(){  
    var band_rent=0;
    if($("#id_renta").is(":visible") && $("#id_renta option:selected").val()!=0){
        var productosr = $("#table_datos_rentas tbody > tr");   
        productosr.each(function(){
            if($(this).find("input[id*='id_renta']").val()==$("#id_renta option:selected").val()){
                band_rent++;
            }               
        });
        if(band_rent==0 && $("#id_renta option:selected").val()!=0){
            $(".tr_rentas").remove();
            addrentas($("#id_renta option:selected").val());
            //$("#id_renta").val(0);
        }
        else{
            swal("¡Atención!", "Esta renta ya existe como concepto de venta", "error");
            $("#id_renta").val(0);
        }
    }

}

function getMttosCli(){
    if($("#idrazonsocial option:selected").val()!=undefined && $("#idrazonsocial option:selected").val()!="2"){
        $.ajax({
            type:'POST',
            url: base_url+"Ventasp/getMttosCli",
            data:{ id_cli: $("#idrazonsocial option:selected").data("id_clidcli") },
            success:function(response){
                var array = $.parseJSON(response);
                if(array.length>0){
                    array.forEach(function(i) {
                        $("#id_mtto").append('<option value="'+i.id+'"># '+i.id+'</option>');
                    });
                }  
            }
        }); 
    }else{
        swal("¡Atención!", "Seleccione antes un cliente", "error");
    }
}

function getMttoDet(){
    if($("#id_mtto option:selected").val()!=0){
        $("#servicios").attr("checked",true);
        $('#input_barcode').hide('show');
        $('#cont_input_search').hide('show');
        $('#cont_input_searchrec').hide('show');
        $('#cont_input_searchrent').hide('show');
        $('#cont_input_searchserv').show('show');
        input_search_ser();
        tipo_venta_renta=4;
        $('.select_option_search .select2-container').show('show');
        validar_input=4;
        setTimeout(function(){
            $('#input_searchserv').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
        $('.tabla_v').css('display','block');
        $('.tipo_venta_renta_ocultar').show('show');
        $('.select2-selection__clear').css('color','white');
    }
}

function rechange(){
    //location.reload();
    window.location.href = base_url+"Ventasp"; 
}

function tipoTraslado(){
    if($("#recargas_modal").is(":checked")==true){
        $("#cont_search_tras").hide("slow");
        $("#cont_rechange").show("slow");
        //$("#cont_det").show("slow");
    }else{
        $("#cont_rechange").hide("slow");
        //$("#cont_det").hide("slow");
        $("#cont_search_tras").show("slow");
    }
}

var idvendedoraux=0;
function validar_vendendor(){
    var idvendedor=$('#idvendedor option:selected').val(); 
    if(idvendedor!=0){
        idvendedoraux = idvendedor;
        $('#modal_pin').modal('show');
        $('#input_pin').val('');
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
    }else{
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
    }
}

var validarpin=0;
function get_validarpin(){
    var input_pin=$('#input_pin').val();
    var cont=input_pin.length;
    if(cont==4){
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/get_validar_pin',
            data: {
                input_pin:input_pin,
                idvendedor:idvendedoraux
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                if(data==1){
                    $('#modal_pin').modal('hide');
                    validarpin=1;
                    setTimeout(function(){ 
                        $('#input_search').select2('open');
                        $('.select2-search__field').focus();
                    }, 1500);
                }else{
                    $('#input_pin').val('');
                    swal("¡Atención!",'Contraseña incorrecta','error', {
                      buttons: false,
                      timer: 2000,
                    });
                    setTimeout(function(){ 
                        $('#input_pin').focus();
                    }, 2000);
                    validarpin=0;
                }
            }
        });
    }
}

function validar_icono_user(){
    var razon_social=$('#idrazonsocial option:selected').val();
    if(razon_social==2){
        $('.icon_user').css('color','#009bdb');
    }else{
        $('.icon_user').css('color','red');
    } 
}

function changeUri(){
    function_name="searchRecargas";
    $('#input_searchrec').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatState_input_searchrec,
        ajax: {
            url: base_url+'Ventasp/'+function_name,
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.codigo+" / $"+element.preciov,
                        codigo:element.codigo,
                        precio:element.preciov
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        addproducto(data.id,1,0,0,0);
        $('#input_searchrec').val(null).trigger('change');
        setTimeout(function(){ 
            $('#input_searchrec').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
        viewproductos();
        //console.log('viewproductos');
        
    });
}
function formatState_input_searchrec (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.codigo + '</span> <span class="colum_in_2">$ ' + state.precio + '</span><!--'+state.text+'-->'
  );
  return $state;
};

function changerentas(){
    function_name="searchRentas";
    $('#input_searchrent').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/'+function_name,
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.descripcion
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        addrentas(data.id);
        $('#input_searchrent').val(null).trigger('change');
        setTimeout(function(){ 
            $('#input_searchrent').select2('open');
            $('.select2-search__field').focus();
        }, 1000);
    });
}

function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
      return data;
    }

    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
      return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (data.text.indexOf(params.term) > -1) {
      var modifiedData = $.extend({}, data, true);
      modifiedData.text += ' (matched)';

      // You can return modified objects from here
      // This includes matching the `children` how you want in nested data sets
      return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}

function input_search(){
    $('#input_search').select2({
        matcher: matchCustom,
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatState,
        ajax: {
            url: base_url+'Ventasp/searchproductos',
            dataType: "json",
            delay:300,
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var cont=0;
                data.forEach(function(element) {
                    cont++;
                    if(element.incluye_iva==1){
                        precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        if(element.iva==0){
                            incluye_iva=0;
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio*1.16).toFixed(2);
                            sub_iva=parseFloat(element.precio*.16).toFixed(2);
                        }
                    }else{
                        if(element.iva>0){
                            siva=1.16;
                            precio_con_ivax=parseFloat(element.precio/siva).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);  
                        }
                    }
                    if(element.tipo==0){
                        nombre=element.nombre+" ("+(element.stock_suc-element.traslado_stock_cant)+")";
                        var stock=(element.stock_suc-element.traslado_stock_cant);
                    }else{
                        nombre=element.nombre;
                        var stock=element.lote+' '+element.serie;
                    }

                    if(element.tipo==0){
                        cant_stock=element.stock-element.traslado_stock_cant;
                    }else if(element.tipo==1){
                        cant_stock=1;
                    }else if(element.tipo==2){
                        cant_stock=element.lote8-element.traslado_lote_cant;
                    }

                    if(element.status_trasr==2 && element.id_traslado_serie!=0 && element.tipo==1 || element.id_traslado_serie==0 && element.tipo==1 || 
                        cant_stock>0 && element.tipo==2 && element.id_compra!=0 && element.estatus_compra=="1" || cant_stock>0 && element.tipo==2 && element.id_compra==0 
                        || element.tipo==0 ){
                        itemscli.push({
                            id: element.id,
                            text: ''+element.idProducto+' '+nombre+' - '+element.lote+' '+element.serie+"   $"+precio_con_ivax+' ',
                            tipo:element.tipo,
                            id_prod_suc:element.id_prod_suc,
                            id_prod_suc_serie:element.id_prod_suc_serie,
                            caducidad:element.caducidad,
                            cont:cont,
                            idProducto:element.idProducto,
                            nombre:element.nombre,
                            stock:stock,
                            cant_stock:cant_stock,
                            precio:precio_con_ivax,
                            concentrador: element.concentrador,
                            tanque: element.tanque,
                            capacidad: element.capacidad,
                            id_ps: element.id_ps

                        });
                    }
                });
                return {
                    results: itemscli
                };
                
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("tipo: "+data.tipo);
        if(data.stock<=0){
            swal("¡Atención!", "Sin existencias", "error");
        }else{
            var id_psl=0;
            if(data.tipo==0){ //stock
                id_psl=data.id_ps;
            }
            if(data.tipo==1){ //serie
                id_psl=data.id_prod_suc_serie;
            }
            if(data.tipo==2){ //lote
                id_psl=data.id_prod_suc;
            }
            $("#input_search option:selected").attr('data-tipo',data.tipo);
            $("#input_search option:selected").attr('data-caducidad',data.caducidad);
            $("#input_search option:selected").attr('data-cont',data.cont);
            $("#input_search option:selected").attr('data-concentra',data.concentrador);
            $("#input_search option:selected").attr('data-tanque',data.tanque);
            $("#input_search option:selected").attr('data-capacidad',data.capacidad);
            addproducto(data.id,0,data.tipo,id_psl,0); //console.log('se selecciono');
            //console.log('add producto elejido select');
            $('#input_search').val(null).trigger('change');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
                $('.select2-selection__placeholder').attr('style','color: white !important');
                $('.select2-selection__placeholder').addClass('select2_colorw');
            }, 2000);
        }
        //viewproductos();
    });

}
function formatState (state) {
    if (!state.id) {
        return state.text;
    }
  
    var $state = $(
        '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + '</span> <span class="colum_in_3">' + state.stock + '</span> <span class="colum_in_4">$ ' + state.precio + '</span><!--'+state.text+'-->'
    );
    return $state;
};
function input_search_ser(){
    $('#input_searchserv').select2({
        matcher: matchCustom,
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatState_ser,
        ajax: {
            url: base_url+'Ventasp/searchservicios',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var cont=0;
                data.forEach(function(element) {
                    cont++;
                    if(element.iva==1){
                        precio=parseFloat(element.precio_siva*1.16).toFixed(4);
                    }else{
                        precio=parseFloat(element.precio_siva).toFixed(4);
                    }
                    itemscli.push({
                        id: element.id,
                        text: element.numero+' '+element.descripcion+" $"+precio,
                        numero: element.numero,
                        descripcion: element.descripcion,
                        precio:precio
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //console.log("tipo: "+data.tipo);
        addservicios(data.id,0);
        if($("#tipo_tec").val()=="1"){
            modalInsumos(data.id,data.descripcion,data.precio);
            showTableIns(1);
        }
        /*setTimeout(function(){ 
            //viewproductos();
            $('#input_searchserv').select2('open');
            $('.select2-search__field').focus();
            $('#input_searchserv').val(null).trigger('change');
        }, 1500);*/
        setTimeout(function(){ 
            $('#input_searchserv').val(null).trigger('change');
        }, 1500);
        
    });
}
function formatState_ser (state) {
  if (!state.id) {
    return state.text;
  }
  
  var $state = $(
    '<span class="colum_in_1">' + state.numero + '</span> <span class="colum_in_2">' + state.descripcion + '</span> <span class="colum_in_3">$ ' + state.precio + '</span><!--'+state.text+'-->'
  );
  return $state;
};
var addservicio_inser=1;
function addservicios(id,id_venta){
    //console.log("addproducto function id: "+id);
    if(addservicio_inser==1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/addservicios",
            data: {
                id:id, id_venta:id_venta
            },
            async:false,
            success: function (response){
                addservicio_inser=1;
                viewproductos();
                //viewservicios();
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
            }
        });
        //addservicio_inser=0;
        setTimeout(function(){ 
            addservicio_inser=1;
        }, 1000);
        
    }
}
function input_searchx(){
    $('#input_searchx').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        dropdownParent: $('#modalverificar'),
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchproductosSolicita',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre,
                        tipo_prod:element.tipo
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        tabla_precios_verificador(2,0,data.tipo_prod);
        $('#input_searchx').val(null).trigger('change');
        setTimeout(function(){ 
            //$('#input_searchx').select2('open');
            //$('.select2-search__field').focus();
        }, 2000);
        viewproductos();

    });
}
function modal_user(){
    //$("#form_cliente")[0].reset();
	$('#modal_user_data').modal('show');
    setTimeout(function(){ 
        $(".razon_social_n").focus();
    }, 1000);

}
var dias_saldox=0;
var saldopagox=0;
function selectcliente(){
    $('#idrazonsocial').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchclientes',
            dataType: "json",
            delay: 300,
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var nom=element.razon_social+" / "+element.nombre;
                    if(element.razon_social==element.nombre){
                        var nom =element.razon_social;
                    }
                    itemscli.push({
                        id: element.id,
                        text: "ID: "+element.id_cliente +" / " +nom,
                        desactivar: element.desactivar,
                        motivo: element.motivo,
                        dias_saldo: element.dias_saldo,
                        saldopago: element.saldopago,
                        diaspago: element.diaspago,
                        idcliente:element.idcliente,
                        correo:element.correo
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        dias_saldox=data.dias_saldo;
        saldopagox=data.saldopago;
        $("#idrazonsocial option:selected").attr('data-id_cli',data.id);
        $("#idrazonsocial option:selected").attr('data-id_clidcli',data.idcliente);
        $("#idrazonsocial option:selected").attr('data-mailc',data.correo);
        
        if(data.desactivar==1){
            setTimeout(function(){ 
                $('#idrazonsocial').val(null).trigger('change');
            }, 2000);
            swal("¡Atención!", "El cliente seleccionado se encuentra inactivo, favor de contactar al administrador. Motivo de suspensión: "+data.motivo, "error");
        }
        if(data.diaspago>0 && $("#perfil").val()!=3 || data.saldopago>0 && $("#perfil").val()!=3){
            $('.ventacredito').show('show');
        }else{
            $('.ventacredito').hide('show');
            $('#tipo_venta').prop('checked', false);
        }
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 1500);
        getRentasCli();
        //if($("#table_datos_rentas tbody > tr").length>0){
            //$(".tabla_r").show();
            viewrentas();
        //}
        getMttosCli();
    });
}
function regimefiscal(){
    $( "#uso_cfdi").val(0);
    $( "#uso_cfdi option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptor option:selected').val();
    $( "#uso_cfdi ."+regimen ).prop( "disabled", false );
}
function adddir(){
    if($('.div_form_direccion').is(':visible')){
        $('.div_form_direccion').hide('show');
    }else{
        $('.div_form_direccion').show('show');
    }
    $('#direccion').val('');
}
function obtenerdatosproducto(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/obtenerdatosproducto",
        data: {
            barcode:$('#input_barcode').val()
        },
        success: function (response){
            //console.log(response);
            var idproducto= parseInt(response);
            if(idproducto>0){
                addproducto(idproducto,0,0,0,0);
                $('#input_barcode').val('').focus();
            }else{
                //toastr["error"]("No existe el producto");
                swal("¡Atención!", "No existe el producto", "error");
            }
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}
var addproducto_inser=1;
function addproducto(id,tipo=0,tipo_prod=0,id_prod_suc=0,tras_por,id_venta=0){
    //console.log("addproducto function");
    //console.log("addproducto function : "+id);
    //console.log("tras_por : "+tras_por);
    //console.log("tipo : "+tipo);
    if(tras_por==1){
       addproducto_inser=1; 
    }
    if(addproducto_inser==1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/addproducto",
            data: {
                id:id, tipo:tipo,tipo_prod:tipo_prod,id_prod_suc:id_prod_suc, id_venta: id_venta
            },
            async:false,
            success: function (response){
                //console.log("response: "+response);
                var arr = $.parseJSON(response);
                //console.log("validar: "+arr.validar);
                //console.log("tipo: "+arr.tipo);
                if(arr.validar==0 && arr.tipo!="3" && arr.tipo!="2"){
                    swal("¡Atención!", "El producto no cuenta con precios establecidos", "error");
                    if(validar_input==0){
                        $('#input_barcode').show('show');
                        $('#input_search').hide('show');
                        $('.select_option_search .select2-container').hide('show');
                        $('.icono1').css('color','red');
                        $('.icono2').css('color','white');
                        setTimeout(function(){ 
                            $('#input_barcode').focus();
                        }, 1000);
                    }else{
                        $('#input_barcode').hide('show');
                        $('#input_search').show('show');
                        $('.select_option_search .select2-container').show('show');
                        $('.icono1').css('color','white');
                        $('.icono2').css('color','red');
                        setTimeout(function(){ 
                            $('#input_search').select2('open');
                            $('.select2-search__field').focus();
                        }, 1000);
                    }
                }if(arr.validar==1 /* || arr.tipo=="3" */){
                    //console.log("tipo: "+arr.tipo);
                    viewproductos();
                    //console.log(response);
                    $('.table_datos_tbody').html(response);
                }if(arr.tipo=="3"){
                    //console.log("tipo: "+arr.tipo);
                    //console.log("id: "+id);
                    addservicios(id,id_venta);
                    //llamar a los insumos asociados al servicio, cuando sea preventa y venta
                    setTimeout(function(){ 
                        getInsumosServ(id);
                    }, 1000);
                    
                }
                /*if(arr.tipo=="2"){ //renta
                    //console.log("tipo: "+arr.tipo);
                    var nuevaOpcion = $("<option></option>")
                        .val(id) 
                        .text("Renta # "+id); 

                    $("#id_renta").append(nuevaOpcion);
                    $("#id_renta").val(id);

                    getRentaDet(id);
                }*/
                if(arr.validar==2){
                    swal("¡Atención!", "No se cuenta con los litros suficientes", "error");
                } 
                if(arr.validar==3){
                    swal("¡Atención!", "No se cuenta con stock suficiente para kit", "error");
                }   
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
            }
        });
        addproducto_inser=0;
        setTimeout(function(){ 
            addproducto_inser=1;
        }, 1000);
    }
}

function getInsumosServ(id){
    //console.log("getInsumosServ: "+id);
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/getInsumosServicio",
        data: { id:id, id_venta:$('#preventas option:selected').val() },
        async:false,
        success: function (response){
            showTableIns(1);
            var result = JSON.parse(response);
            var id_mttobi=0;
            result.forEach(function(it){
                id_mttobi = it.id_mtto;
                var dis_cant=""; var btn_del="<button class='btn btn-danger' onclick='deleteIns("+it.id+","+it.id+")'> <i class='fa fa-trash' aria-hidden='true'></i></button>";
                if($("#tipo_tec").val()!="1"){
                    dis_cant="readonly";
                    btn_del="";
                }
                html="<tr class='row_ins_deta row_ins_"+it.id_insumo+" row_ins_serv_"+it.id+"'>\
                    <td><input type='hidden' id='id' value='"+it.id+"'><input type='hidden' id='id_ins' value='"+it.id_insumo+"'><input type='hidden' id='id_ps' value='"+it.id_prod_suc+"'>\
                        <input type='hidden' id='tipo' value='"+it.tipo+"'><input type='hidden' id='id_serv' value='"+it.id_serv+"'>\
                        <input type='hidden' id='stock' value='"+it.stock+"'>\
                        <input "+dis_cant+" class='form-control' id='cant_ins' type='number' value='"+it.cantidad+"'>\
                    </td>\
                    <td>"+it.descripcion+" ($"+it.precio_siva+")"+"</td>\
                    <td>"+it.name_ins+"</td>\
                    <td>"+btn_del+"</td>\
                </tr>";
                $(".datos_insumos_servs").append(html);
            });
            var nuevaOpcion = $("<option></option>")
                .val(id_mttobi) 
                .text("Mtto. # "+id_mttobi); 

            // Agregar la nueva opción al <select> con el id 'id_mtto'
            $("#id_mtto").append(nuevaOpcion);
            $("#id_mtto").val(id_mttobi);
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}

function viewproductos(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/viewproductos",
        data: {
            id:0
        },
        success: function (response){
            //console.log(response);
            $('.table_datos_tbody').html(response);
            calculartotales();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}
function viewservicios(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/viewservicio",
        data: {
            id:0
        },
        success: function (response){
            //console.log(response);
            //$('.table_datos_servicios_tbody').html(response);
            $('.table_datos_tbody').html(response);
            calculartotales();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}
function deletepro(id,tipo,id_serv=0){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                deleteprorow(id,tipo);
                var productos = $("#table_datos tbody > tr");
                var pro=productos.length;
                if(pro==1){
                    quitar_radio();
                }
                if(tipo=="3"){ // -- para eliminar insumos del serv
                    //console.log("eliminar insumos del servicio: "+id_serv);
                    $(".row_ins_serv_"+id_serv+"").remove();
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function deleteprorow(id,tipo){
    //console.log('deleteprorow:'+id);
    $.ajax({
        type:'POST',
        url: 'Ventasp/deleteproducto',
        data: {
            id: id,tipo:tipo
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                //$('.producto_'+id).remove();
                viewproductos();
                viewrentas();
                calculartotales();
                if(validar_input==0){
                    $('#input_barcode').show('show');
                    $('#input_search').hide('show');
                    $('.select_option_search .select2-container').hide('show');
                    $('.icono1').css('color','red');
                    $('.icono2').css('color','white');
                    setTimeout(function(){ 
                        $('#input_barcode').focus();
                    }, 1000);
                }else{
                    $('#input_barcode').hide('show');
                    $('#input_search').show('show');
                    $('.select_option_search .select2-container').show('show');
                    $('.icono1').css('color','white');
                    $('.icono2').css('color','red');
                    setTimeout(function(){ 
                        $('#input_search').select2('open');
                        $('.select2-search__field').focus();
                    }, 1000);
                }
            }
        });
}
var tipo_aux=0;
var id_aux=0;
function validar_descuento(id,tipo){
    setTimeout(function(){ 
        $('#codigo_t1').focus();
    }, 1000); 
    
    $('#modalcodigo').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/add_alerta_traspasos",
        data:{idsucursal:0,tipo:0},
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            //$('.tabla_precios').html(data);  
        }
    }); 
    tipo_aux=tipo;
    id_aux=id;
    tipo_codigo=1;
}
function adddescuento(){
    var html='';
    if(tipo_aux==0){
        html='Ingrese el porcentaje del descuento';
    }
    if(tipo_aux==1){
        html='Ingrese el Monto del descuento';
    }
    $('#modal_porcentaje').modal('show');
    
    /*$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html+'<input type="number" class="form-control" id="confirm_descuento">',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var des=$('#confirm_descuento').val();
                $.ajax({
                    type:'POST',
                    url: 'Ventasp/adddescuento',
                    data: {
                        id: id,
                        tipo:tipo,
                        des:des
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        viewproductos();
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });*/
}
function get_descuento(){
    var des=$('#confirm_descuento option:selected').val();
    $.ajax({
        type:'POST',
        url: 'Ventasp/adddescuento',
        data: {
            id: id_aux,
            tipo:tipo_aux,
            des:des
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('#modal_porcentaje').modal('hide');
            viewproductos();
        }
    });
}

function calculartotales(){
    var subtotal=0;
    var descuentos=0;
    var impuest=0;
    var impuestosp=0;
    //=========================================== PRODUCTOS
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        item = {};
        subtotal+=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
        descuentos+=parseFloat($(this).find("input[id*='vsdescuento']").val());
        count_row=parseFloat($(this).find("input[id*='count']").val());
        incluye_iva=parseFloat($(this).find("input[id*='incluye_iva']").val());
        subtotaln=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
        descuentosn=parseFloat($(this).find("input[id*='vsdescuento']").val());
        //console.log("subtotaln de prods: "+subtotaln);
        //console.log("descuentosn de prods: "+descuentosn);
        if(incluye_iva==1 && $(this).find("input[id*='vstipo']").val()!="1"){
           impuestosp+=(parseFloat(subtotaln)-parseFloat(descuentosn))*0.16;
        }else{
            impuestosp+=0;
        }
        //console.log("impuestosp de prods: "+impuestosp);
    });
    //===========================================================RENTAS SERVICIOS
    if($("#table_datos_rentas").is(":visible")==true){
        var TABLA2   = $("#table_datos_rentas tbody > tr");
        TABLA2.each(function(){         
            item = {};
            var vscanti_info =$(this).find("input[id*='vscanti']").val();
            var vsprecio_info=$(this).find("input[id*='vstotal']").val()

            vsprecio_info_sini = vsprecio_info / 1.16;

            subtotal+=parseFloat(vscanti_info)*parseFloat(vsprecio_info_sini);
            //descuentos+=parseFloat($(this).find("input[id*='vsdescuento']").val());
            descuentos+=parseFloat(0);
            count_row=parseFloat($(this).find("input[id*='count']").val());
            //incluye_iva=parseFloat($(this).find("input[id*='incluye_iva']").val());
            incluye_iva=parseFloat(1); //servicios y rentas generan iva
            subtotaln=parseFloat(vscanti_info)*parseFloat(vsprecio_info_sini);
            //descuentosn=parseFloat($(this).find("input[id*='vsdescuento']").val());
            descuentosn=parseFloat(0);
            if(incluye_iva==1){
               impuestosp+=(parseFloat(subtotaln)-parseFloat(descuentosn))*0.16;
               //impuestosp+=0;
            }
            //impuestosp+=0;
            //console.log("impuestosp de rentas: "+impuestosp);
        });
    }
    //=========================================== SERVICIOS
    /*var TABLA   = $("#table_datos_servicios tbody > tr");
        TABLA.each(function(){         
            item = {};
            subtotal+=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
            descuentos+=parseFloat($(this).find("input[id*='vsdescuento']").val());
            count_row=parseFloat($(this).find("input[id*='count']").val());
            incluye_iva=parseFloat($(this).find("input[id*='incluye_iva']").val());
            subtotaln=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
            descuentosn=parseFloat($(this).find("input[id*='vsdescuento']").val());
            if(incluye_iva==1){
               impuestosp+=(parseFloat(subtotaln)-parseFloat(descuentosn))*0.16;
            }
            //console.log(impuestosp);
        });
    */
    //==========================================================


    $('#subtotal').val(subtotal);
    if(isNaN(subtotal)){
        $('.class_subtotal').html('$ 0.00');
    }else{
        $('.class_subtotal').html('$ '+subtotal.toFixed(2));
    }
    $('#descuentos').val(descuentos);
    $('.class_descuentos').html('$ '+descuentos);
    if(descuentos>0){
        $('.div_descuentos').show();
    }else{
        $('.div_descuentos').hide();
    }
    //var impuestos=(parseFloat(subtotal)-parseFloat(descuentos))*0.16;
    var impuestos=redondear(impuestosp,2);
        //console.log('impuestos:'+impuestos);
    $('#impuestos').val(impuestos);


    if(isNaN(impuestos)){
        $('.class_impuestos').html('$ 0.00');
    }else{
        $('.class_impuestos').html('$ '+impuestos);
    }

    var total=parseFloat(subtotal)-parseFloat(descuentos)+parseFloat(impuestos);
        total=redondear(total,2);
    $('#total').val(total);
    
    if(isNaN(total)){
        $('.class_total').html('$ 0.00');
    }else{
        $('.class_total').html('$ '+total);
    }
}
/*function calculartotales(){
    var subtotal=0;
    var descuentos=0;
    var impuest=0;
    var impuestospro=0;
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        item = {};
        subtotal+=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
        descuentos+=parseFloat($(this).find("input[id*='vsdescuento']").val());
        count_row=parseFloat($(this).find("input[id*='count']").val());
        incluye_iva=parseFloat($(this).find("input[id*='incluye_iva']").val());
        ///////////
        subtotaln=parseFloat($(this).find("input[id*='vscanti']").val())*parseFloat($(this).find("input[id*='vsprecio']").val());
        descuentosn=parseFloat($(this).find("input[id*='vsdescuento']").val());
        if(incluye_iva==1){
            impuestospro+=(parseFloat(subtotaln)-parseFloat(descuentosn))*0.16;
        }else{
            impuestospro+=(parseFloat(subtotaln)-parseFloat(descuentosn));
        }
        console.log(impuestospro);
    });
    console.log(impuestospro);
    //// Subtotal
    $('#subtotal').val(subtotal);
    if(isNaN(subtotal)){
        $('.class_subtotal').html('$ 0.00');
    }else{
        $('.class_subtotal').html('$ '+subtotal.toFixed(2));
    }

    $('#descuentos').val(descuentos);
    $('.class_descuentos').html('$ '+descuentos);

    var impuestos=redondear(impuestospro,2);
    
    //console.log('impuestos:'+impuestos);
    $('#impuestos').val(impuestos);


    if(isNaN(impuestos)){
        $('.class_impuestos').html('$ 0.00');
    }else{
        $('.class_impuestos').html('$ '+impuestos);
    }

    var total=parseFloat(subtotal)-parseFloat(descuentos)+parseFloat(impuestos);
        total=redondear(total,2);
    $('#total').val(total);
    
    if(isNaN(total)){
        $('.class_total').html('$ 0.00');
    }else{
        $('.class_total').html('$ '+total);
    }
}*/
function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}
function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}
function limpiartabla(){
    $('.tabla_ins').hide();
    $('.tabla_r').hide();
    $('.table_datos_rentas_tbody').html('');
    $(".datos_insumos_servs").html('');
    $.ajax({
        type:'POST',
        url: 'Ventasp/limpiartabla',
        data: {
            id: 0
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            viewproductos();
        }
    });
}
function addmetodo(){
    var idmp=$('#select_formapagos option:selected').val();
    var idmp_text=$('#select_formapagos option:selected').text();
    var tipo=0;
    if(idmp=='01'){
        tipo=1;
    }else{
        tipo=0;
    }
    tabla_metodo(idmp,idmp_text,tipo);
}
var rowmetodo=0;
function tabla_metodo(idmp,idmp_text,tipo){ 
    var option_ban=$('#select_ban').html();
    if(idmp=='04' || idmp=='28' || idmp=='02' || idmp=='03' || idmp=='29'){
        var input_disable='';
        var d_banco=1;
    }else{
        option_ban='<option value="0"></option>';
        var input_disable='style="display:none;"';
        var d_banco=0;
    }
    /// 
    if(idmp=='29'){
        option_ban='<option value="10">AMEX</option>';
    }else if(idmp=='04'){

    }else{
        option_ban=option_ban.replace('<option value="10">AMEX</option>', "");
    }
    var html='<tr class="addmetodo_'+rowmetodo+'">\
            <td>\
                '+idmp_text+'\
                <input type="hidden" id="for_mp" value="'+idmp+'">\
            </td>\
            <td>\
                <input type="number" id="for_mp_monto" value="" class="form-control tx_mp_monto mp_monto'+rowmetodo+'" data-banco="'+d_banco+'" oninput="cal_metodo()">\
            </td>\
            <td><select id="for_ban" class="form-control" '+input_disable+'>'+option_ban+'</select></td>\
            <td>\
                <input type="text" id="for_mp_digitos" placeholder="4 últimos digitos" class="form-control for_mp_digitos" '+input_disable+'>\
            </td>\
            <td><a onclick="deletemp('+rowmetodo+')" class="btn"><i class="fa fa-trash"></i></a></td>\
            <td><span class="switch switch-icon">\
                    <label>\
                        <input type="radio" id="metodo_check'+rowmetodo+'" class="check_pago" name="check_pago" onchange="check_pago_btn('+rowmetodo+','+tipo+',this.id)">\
                        <span style="border: 2px solid #009bdb;"></span>\
                    </label>\
                </span>\
            </td>\
        </tr>';
    $('.tbody_formapagos').append(html);
    rowmetodo++;
}
var cont_check=0;
function check_pago_btn(cant,tipo,id){
    $('.tx_mp_monto').val('');
    var totalx=$('#total').val();
    //$('.mp_monto'+cant).val(totalx);
    if(tipo!=1){
        $('.class_cambio').html('$0.00');
    }
    setTimeout(function(){ 
            if($("#"+id).is(":checked")){
                $('.mp_monto'+cant).val(totalx);
            }else{
                $('.mp_monto'+cant).val('');
            }
        
    }, 500);
    /*
    cont_check++;
    if($("#"+id).is(":checked")==true && cont_check>1){
        $("#"+id).prop("checked",false);
        $('.mp_monto'+cant).val(0);
        cont_check=0;
    }
    */
}
function deletemp(row){
    $('.addmetodo_'+row).remove();
    cal_metodo();
}
function cal_metodo(){
    var pagosmonto=0;
    var TABLA   = $("#table_formapagos tbody > tr");
        TABLA.each(function(){         
            pagosmontoval=parseFloat($(this).find("input[id*='for_mp_monto']").val());
            if(pagosmontoval>0){

            }else{
                pagosmontoval=0;
            }
            if($(this).find("input[id*='for_mp_monto']").data("banco")=="0"){
                pagosmonto+=pagosmontoval;
            }
            if($(this).find("input[id*='for_mp_monto']").data("banco")=="1" && Number($(this).find("input[id*='for_mp_monto']").val()) <= Number($('#total').val())){
                pagosmonto+=pagosmontoval;
            }
            if($(this).find("input[id*='for_mp_monto']").data("banco")=="1" && Number($(this).find("input[id*='for_mp_monto']").val()) > Number($('#total').val())){
                swal("¡Atención!", "No se puede capturar un monto mayor al de la venta en este tipo de método", "error");
                $(this).find("input[id*='for_mp_monto']").val("0");
                return false;
            }
            if($(this).find("input[id*='for_mp_monto']").data("banco")=="0" && pagosmonto > Number($('#total').val())){ //efectivo
                swal("¡Atención!", "La suma es mayor al total de venta", "error");
                $(this).find("input[id*='for_mp_monto']").val($('#total').val());
                return false;
            }
            if($(this).find("input[id*='for_mp_monto']").data("banco")=="1" && pagosmonto > Number($('#total').val())){ //tarjeta
                swal("¡Atención!", "La suma es mayor al total de venta", "error");
                $(this).find("input[id*='for_mp_monto']").val("0");
                return false;
            }
        });
    var total=$('#total').val();
    //console.log();
    var cambio=pagosmonto-total;
    cambio=redondear(cambio,2);
    var total_r=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(cambio);
    if(cambio>0){
        $('.class_cambio').html(total_r);
    }else{
        $('.class_cambio').html('$0.00');
    }
    //console.log(pagosmonto+'-'+total);
}
function realizarventa(){
    var viewventa = $('#viewventa').val();
    var preventa = $('#preventa').val();
    var banco_digitos=1;
    if(cambio>=0){      
        //==============================================
            var productos = $("#table_datos tbody > tr");
            var DATAa  = [];
            productos.each(function(){         
                item = {};                    
                item ["cant"] = $(this).find("input[id*='vscanti']").val();
                item ["id_ps_lot"] = $(this).find("input[id*='vsproid_ps_lot']").val();
                item ["id_ps_ser"] = $(this).find("input[id*='vsproid_ps_ser']").val();
                item ["tipo"] = $(this).find("input[id*='vstipo']").val();
                item ["capac"] = $(this).find("input[id*='vscapac']").val();
                item ["pro"] = $(this).find("input[id*='vsproid']").val();
                item ["cunit"] = $(this).find("input[id*='vsprecio']").val();
                item ["desc"] = $(this).find("input[id*='vsdescuento']").val();
                item ["incluye_iva"] = $(this).find("input[id*='incluye_iva']").val();

                item ["concentrador"] = $(this).find("input[id*='concentrador']").val();
                item ["tanque"] = $(this).find("input[id*='tanque']").val();
                item ["capacidad"] = $(this).find("input[id*='capacidad']").val();
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);
            //console.log("aInfoa: "+aInfoa);
        //========================================
        var pagosfp = $("#table_formapagos tbody > tr");
        //==============================================
        var pagosfp_length=0;
        var DATAp  = [];
        pagosfp.each(function(){    
            var for_mp_monto = $(this).find("input[id*='for_mp_monto']").val();
            if(for_mp_monto>0){    
                var check_pago = $(this).find("input:radio[name*='check_pago']:checked").val(); 
                //if(check_pago=='on'){
                    item = {};                    
                    item ["tipo"]   = $(this).find("input[id*='for_mp']").val();
                    item ["monto"]   = $(this).find("input[id*='for_mp_monto']").val();
                    var ultimosdigitos   = $(this).find("input[id*='for_mp_digitos']").val();
                    item ["ultimosdigitos"]   = ultimosdigitos;
                    var bancoval = $(this).find("select[id*='for_ban'] option:selected").val();
                    item ["banco"]   = bancoval;

                    var v_b_d = $(this).find("input[id*='for_mp_monto']").data('banco');
                    if(v_b_d==1){
                        if(ultimosdigitos==''){
                            banco_digitos=0;
                        }
                        if(bancoval==0){
                            banco_digitos=0;
                        }
                    }
                //}
                DATAp.push(item);
                pagosfp_length++;
            }
        });
        INFOp  = new FormData();
        aInfop   = JSON.stringify(DATAp);
        //console.log("aInfop: "+aInfop);
        if(preventa==1){
            pagosfp_length=1;
        }
            
        //========================================
        //==============================================
            var productosr = $("#table_datos_rentas tbody > tr");
            var DATAar  = [];
            if($("#table_datos_rentas tbody > tr").is(":visible")==true){    
                productosr.each(function(){         
                    item = {};     
                    item ["id_renta"]   = $(this).find("input[id*='id_renta']").val();               
                    item ["cant"]   = $(this).find("input[id*='vscanti']").val();
                    item ["tipo"]   = 2;
                    item ["pro"]   = $(this).find("input[id*='id_prod']").val(); //id del producto
                    item ["cunit"]   = $(this).find("input[id*='vstotal']").val();
                    item ["id_serv"]   = $(this).find("input[id*='vsproid']").val(); //id_servicio
                    item ["idserie"]   = $(this).find("input[id*='id_sserie']").val();
                    item ["periodo"]   = $(this).find("input[id*='periodo']").val();
                    DATAar.push(item);
                });
                //aInfoar = JSON.stringify(DATAar);
            }
            aInfoar = JSON.stringify(DATAar);
            //console.log("aInfoar: "+aInfoar);
        //========================================
        //==============================================
            var proservicios = $("#table_datos_servicios tbody > tr");
            var DATAps  = [];
            proservicios.each(function(){         
                item = {};                    
                item ["cant"] = $(this).find("input[id*='vscanti']").val();
                item ["pro"] = $(this).find("input[id*='vsproid']").val();
                item ["cunit"] = $(this).find("input[id*='vsprecio']").val();
                item ["desc"] = $(this).find("input[id*='vsdescuento']").val();
                item ["incluye_iva"] = $(this).find("input[id*='incluye_iva']").val();
                DATAps.push(item);
            });
            //INFOa  = new FormData();
            aInfops   = JSON.stringify(DATAps);
            //console.log("aInfops: "+aInfops);
        //========================================

        //==============================================
        //if($("#table_insumos tbody > tr").length>0){
            var insumos = $("#table_insumos_list tbody > tr");
            var DATAi  = [];
            insumos.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["id_ins"] = $(this).find("input[id*='id_ins']").val();
                item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
                item ["cant_ins"] = $(this).find("input[id*='cant_ins']").val();
                item ["tipo"] = $(this).find("input[id*='tipo']").val();
                item ["id_serv"] = $(this).find("input[id*='id_serv']").val();
                item ["stock"] = $(this).find("input[id*='stock']").val();
                DATAi.push(item);
            });
            INFOi  = new FormData();
            aInfoi = JSON.stringify(DATAi);
            //console.log("aInfoi: "+aInfoi);
        //}
        //========================================

        var id_razonsocial =$('#idrazonsocial option:selected').val();
        var id_cliente =$('#idrazonsocial option:selected').data("id_clidcli");
        var subtotal = $('#subtotal').val();
        var descuento = $('#descuentos').val();
        var iva = $('#impuestos').val();
        var total = $('#total').val();
        var vf=$('#venta_facturada').is(':checked')==true?1:0;
        var vc=$('#tipo_venta').is(':checked')==true?1:0;
        if(vc==1){
            pagosfp_length=1;
        }
        //validar que se envié el id de razon social cuando se crea un cliente desde esta pantala, se está enviando el id de cliente
        var datos='viewventa='+viewventa+'&preventa='+preventa+'&id_razonsocial='+id_razonsocial+'&id_cliente='+id_cliente+'&subtotal='+subtotal+'&descuento='+descuento+'&iva='+iva+'&total='+total+'&vf='+vf+'&productos='+aInfoa+'&montosp='+aInfop+'&tipo_venta='+vc+'&validarpin='+validarpin+'&idvendedor='+idvendedoraux+'&productosr='+aInfoar+'&proserv='+aInfops+'&insumos='+aInfoi+'&observaciones='+$("#observaciones").val()+"&id_mtto="+$("#id_mtto option:selected").val()+"&id_renta="+$("#id_renta option:selected").val();
        var rowproductos=parseFloat(productos.length)+parseFloat(productosr.length);
        if($('#idrazonsocial option:selected').val()==undefined || $('#idrazonsocial option:selected').val()==""){
            swal("¡Atención!", "Para realizar la venta, elija un cliente", "error");
            return false;
        }
        if(rowproductos>0){
            if(pagosfp_length>0){
                if(banco_digitos==1){
                    var pagosmonto=0;
                    var TABLA   = $("#table_formapagos tbody > tr");
                    TABLA.each(function(){         
                        pagosmontoval=parseFloat($(this).find("input[id*='for_mp_monto']").val());
                        if(pagosmontoval>0){

                        }else{
                            pagosmontoval=0;
                        }
                        pagosmonto+=pagosmontoval;
                    });
                    var pm=parseFloat(pagosmonto);
                    var totalx=$('#total').val();
                    var tx=parseFloat(totalx);
                    //console.log("PM: "+pm);
                    //console.log("tx: "+tx);
                    if(preventa==1){
                        pm=tx;
                    }
                    if(vc==1){
                        pm=tx;
                    }
                    if(pm>=tx){
                        var validar_valor=0; var valida_banco=0;
                        var TABLAv   = $("#table_formapagos tbody > tr");
                        TABLAv.each(function(){         
                            var check_pago = $(this).find("input:radio[name*='check_pago']:checked").val();
                            var for_mp=parseFloat($(this).find("input[id*='for_mp']").val());
                            var for_tbanco=parseFloat($(this).find("select[id*='for_ban'] option:selected").val());
                            if(check_pago=='on' && for_mp=='28' || check_pago=='on' && for_mp=='04'){
                                var for_mp_digitos=$(this).find("input[id*='for_mp_digitos']").val().length;
                                if(for_mp_digitos<=3) {
                                   validar_valor=1;
                                } 
                                if(for_tbanco=="0") {
                                   valida_banco++;
                                   swal("¡Atención!", "Elige un banco en metodo de pago", "error");
                                   return;
                                }     
                            }
                            //console.log("for_tbanco: "+for_tbanco);
                            //console.log("valida_banco: "+valida_banco);
                        });
                        if(valida_banco>0) {
                            //console.log("valida_banco: "+valida_banco);
                            swal("¡Atención!", "Elige un banco en metodo de pago", "error");
                            return;
                        } 
                        if(validar_valor==0 && valida_banco==0){
                            if(valida_banco==0){
                                var validarp=0;
                                var TABLAp   = $("#table_datos tbody > tr");
                                TABLAp.each(function(){  
                                    var stockn= $(this).find("input[id*='stockn']").val();
                                    if(stockn==0){
                                        validarp=1;
                                    }
                                });  
                                if(validarp==0){
                                    var validar_check=0;
                                    if(dias_saldox==2){
                                        var totalaux=parseFloat(total);
                                        var saldopagoaux=parseFloat(saldopagox);
                                        if(saldopagoaux==0){
                                            validar_check=1;
                                        }else{
                                            if(saldopagoaux<totalaux){
                                                validar_check=2;
                                            }
                                        }
                                        
                                    }
                                    if(validar_check==0){
                                        $('body').loading({theme: 'dark',message: 'Generando venta...'});
                                        $.ajax({
                                            type:'POST',
                                            url: base_url+"index.php/Ventasp/savefactura",
                                            data: datos,
                                            success: function (response){
                                                //console.log(response);
                                                var array = $.parseJSON(response);
                                                if(array.facturar==1){
                                                    timbrarventa(array.idventa,id_razonsocial);
                                                    $('#iframeri').modal('show'); 
                                                    var perfilid=$('#perfilid').val();
                         
                                                    if(perfilid!=3){
                                                        setTimeout(function(){ 
                                                            $('.iframereporte').html('<iframe src="' + base_url + 'Ventasp/ticket/'+array.idventa+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
                                                        }, 1000); 
                                                    }else{
                                                        swal("Éxito!",'Venta # '+array.folio+' con monto total de $'+total+' enviada a caja con éxito','success', {
                                                            buttons: true,
                                                            //timer: 9000,
                                                            //focusConfirm: false,
                                                            showConfirmButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes",
                                                            showCancelButton: false, 
                                                            showCloseButton: true,
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                console.log("click ok");
                                                                rechangeScreen();
                                                            }else{
                                                                rechangeScreen();
                                                            }
                                                        });
                                                        
                                                        /*setTimeout(function(){ 
                                                            rechange();
                                                        }, 6500); */ 
                                                    }
                                                }else{
                                                    $('body').loading('stop')      
                                                    var perfilid=$('#perfilid').val();
                                                    if(perfilid!=3 && $('#tipo_tec').val()!="1"){
                                                        $('#iframeri').modal('show'); 
                                                        setTimeout(function(){ 
                                                               $('.iframereporte').html('<iframe src="' + base_url + 'Ventasp/ticket/'+array.idventa+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
                                                        }, 1000); 
                                                         swal("Éxito!",'Venta realizada','success', {
                                                              buttons: false,
                                                              timer: 3000,
                                                            });
                                                    }else{
                                                        swal("Éxito!",'Venta # '+array.folio+' con monto total de $'+total+' enviada a caja con éxito','success', {
                                                            buttons: true,
                                                            //timer: 9000,
                                                            //focusConfirm: false,
                                                            showConfirmButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes",
                                                            showCancelButton: false, 
                                                            showCloseButton: true,
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                console.log("click ok");
                                                                rechangeScreen();
                                                            }else{
                                                                rechangeScreen();
                                                            }
                                                        });
                                                        
                                                        /*setTimeout(function(){ 
                                                            rechange();
                                                        }, 6500); */
                                                    }
                                                }

                                                var idcotizacion_cl=$('#idcotizacion_cl').val();
                                                if(idcotizacion_cl>0){
                                                    $.ajax({
                                                        type:'POST',
                                                        url: base_url+"index.php/Ventasp/update_cotizacion",
                                                        data: {
                                                            id:idcotizacion_cl
                                                        },
                                                        success: function (response){
                                
                                                        },
                                                        error: function(response){
                                                            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                                                            
                                                        }
                                                    });
                                                }
                                                setTimeout(function(){ 
                                                    limpiarventa(); 
                                                }, 1500);
                                            },
                                            error: function(response){
                                                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                                                
                                            }
                                        });
                                    }else if(validar_check==1){
                                        swal("¡Atención!", "No es posible realizar la venta a crédito ya que el cliente no cuenta con ningún saldo registrado. Necesita contactar a su administrador.", "error");
                                    }else if(validar_check==2){
                                        swal("¡Atención!", "El cliente, no cuenta con suficiente saldo disponible para efectuar la venta", "error");
                                    }

                                }else{
                                    swal("¡Atención!", "No puedes vender el o los productos seleccionados, ya que el almacén no cuenta con stock suficiente", "error");
                                }
                            }else{
                                swal("¡Atención!", "Elige un banco en metodo de pago", "error");
                            }
                        }else{
                            swal("¡Atención!", "Necesitas agregar los últimos 4 dígitos de la tarjeta en el método de pago", "error");
                        }
                    }else{
                        swal("¡Atención!", "Para realizar la venta, debe capturar por lo menos un método de pago", "error");
                    }
                }else{
                    swal("¡Atención!", "Para realizar la venta, uno de los métodos de pagos requiere los ultimos digitos y/o seleccion de banco", "error");
                }
                

            }else{
                //console.log(pagosfp_length);
                //toastr.error('Agregar por lo menos un Metodo de pago');
                swal("¡Atención!", "Para realizar la venta, debe capturar por lo menos un método de pago", "error");
            }
        }else{
            //toastr.error('Agregar por lo menos un producto');
            swal("¡Atención!", "Debe agregar por lo menos un producto para venta", "error");
        }
    }
}

function rechangeScreen(){
    rechange();
}

function limpiarventa(){
    limpiartabla();
    $('#idrazonsocial').val(null).trigger('change');
    $('.tx_mp_monto').val('');
    $('.for_mp_digitos').val(0);
    $('.check_pago').prop("checked", false);
    $('.class_subtotal').html('$0.00');
    $('.class_descuentos').html('$0.00');
    $('.class_impuestos').html('$0.00');
    $('.class_total').html('$0.00');
    $('#idrazonsocial').val('2').trigger('change.select2');
    $('.class_cambio').html('$0.00'); 

    quitar_radio();
    
    $('#input_barcode').hide('show');
    $('#cont_input_search').show('show');
    $('#cont_input_searchrec').hide('show');
    $('#cont_input_searchrent').hide('show');
    //input_search();
    tipo_venta_renta=1;
    $('.select_option_search .select2-container').show('show');
    $('.icono1').css('color','white');
    $('.icono2').css('color','red');
    validar_input=1;
    setTimeout(function(){
        $('#input_search').select2('open');
        $('.select2-search__field').focus();
    }, 1500);
    $('.tabla_v').css('display','block');
    //$('.tabla_r').css('display','none');
    //$('.tipo_venta_renta_ocultar').css('display','block');
    $('.tipo_venta_renta_ocultar').show('show');
}
function timbrarventa(idventa,id_razonsocial){
    $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Timbrado/generafacturarventa/"+idventa,
        data: {
            id:0
        },
        success: function (response){
            $('body').loading('stop');
            //console.log(response);
            var array = $.parseJSON(response);
            var facturaId = array.facturaId;
            if (array.resultado=='error') {
                //console.log(array.MensajeError);
                //console.log(array.info.TimbrarCFDIResult.MensajeErrorDetallado);
                var texto=array.info.TimbrarCFDIResult.MensajeErrorDetallado;
                if(texto==''){
                    var texto=array.MensajeError;
                }
                texto = cambiarmensajeserror(array.CodigoRespuesta,texto);


                
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: "Error "+array.CodigoRespuesta+"!",
                    content: texto,
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        
                        'Editar datos': function (){
                            //var idrazonsocial = $('#idrazonsocial option:selected').val();
                            var idrazonsocial=id_razonsocial;
                            if(idrazonsocial>0){
                                editaryretimbrar(idrazonsocial,facturaId);
                            }else{
                                toastr.error('Algo salió mal, reintente desde el catalogo de clientes y retimbre');
                            }
                        },
                        Salir: function (){
                            window.location.href = base_url+"index.php/Listaventas"; 
                        }
                    }
                });
                setTimeout(function(){ 
                    //window.location.href = base_url+"index.php/Listaventas"; 
                }, 5000);
            }else{
                    //var textofactura='Se ha Guardado la factura';
                
                    var textofactura='Se ha creado la factura';
                
                swal({
                    title:"Éxito!", 
                    text:textofactura, 
                    type:"success"
                    });
                setTimeout(function(){ 
                    window.open(base_url+'Folio/facturapdf/'+facturaId+'?envio=1','_blank'); 
                    //window.location.href = base_url+"index.php/Listaventas"; 
                }, 2000);
            }
            
            
            
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function editaryretimbrar(idrazonsocial,facturaId){
    var html='<label>Razón social</label>';
        html+='<input class="form-control" type="text" id="edit_razon_social" > ';
        html+='<label>RFC</label>';
        html+='<input class="form-control" type="text" id="edit_rfc" > ';
        html+='<label>C.P.</label>';
        html+='<input class="form-control" type="text" id="edit_cp" > ';
        html+='<label>Régimen fiscal</label>';
        var option_rf=$('#RegimenFiscalReceptor').html();
        html+='<select class="form-control" type="text" id="edit_RegimenFiscalReceptor" >'+option_rf+'</select>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: "Editar!",
        content: html,
        type: 'gren',
        typeAnimated: false,
        buttons:{
            'Actualizar y retimbrar': function (){
                var e_razon_social = $('#edit_razon_social').val();
                var e_rfc = $('#edit_rfc').val();
                var e_cp = $('#edit_cp').val();
                var e_RFR = $('#edit_RegimenFiscalReceptor option:selected').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Ventasp/editardatosfiscales",
                    data: {
                        id:idrazonsocial,
                        razon_social:e_razon_social,
                        rfc:e_rfc,
                        cp:e_cp,
                        RegimenFiscalReceptor:e_RFR
                    },
                    success: function (response){
                        retimbrar(facturaId,0);
                    },
                    error: function(response){
                        toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                        
                    }
                }); 
            },
            'Salir': function (){
                window.location.href = base_url+"index.php/Listaventas"; 
            }
        }
    });
    view_df(idrazonsocial);
}
function retimbrar(idfactura,contrato){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Timbrado/retimbrar",
        data: {
            factura:idfactura
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                              type: "warning",
                              showCancelButton: false
                 });
            }else{
                $.confirm({
                                boxWidth: '30%',
                                useBootstrap: false,
                                icon: 'fa fa-warning',
                                title: 'Éxito!',
                                content: 'Se ha creado la factura',
                                type: 'green',
                                typeAnimated: true,
                                buttons:{
                                    Aceptar: function (){
                                        //$(location).attr('href',base_url+'Facturas');
                                        window.open(base_url+'Folio/facturapdf/'+idfactura,'_blank'); 
                                    }
                                }
                            });
                
            }
            $('body').loading('stop');
        }
    });      
}
function view_df(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/datosfiscales_view",
        data: {
            id:id
        },
        success: function (response){
            var array = $.parseJSON(response);
            $('#edit_razon_social').val(array.df.razon_social);
            $('#edit_rfc').val(array.df.rfc);
            $('#edit_cp').val(array.df.cp);
            $('#edit_RegimenFiscalReceptor').val(array.df.RegimenFiscalReceptor);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function editarcantidad(row,stock_prod){
    var cantidad=$('.vscanti_'+row).val();
    var tipo=$('.vstipo_'+row).val();
    var capac=$('.vscapac_'+row).val();
    var stock=$('.stockn_'+row).val();
    var band_cap=0; var band_stock=0;
    if(tipo==1){
        pre_tot=capac*cantidad;
        //console.log("pre_tot: "+pre_tot);
        if(pre_tot>stock){
            band_cap++;
            swal("¡Atención!", "No se cuenta con los litros suficientes", "error");
            $('.vscanti_'+row).val("1");
        }
    }
    //console.log("tipo: "+tipo);
    //console.log("cantidad: "+cantidad);
    //console.log("stock_prod: "+stock_prod);
    if(tipo==0){ //prod normal
        if(Number(cantidad)>Number(stock_prod)){
            band_stock++;
            swal("¡Atención!", "No se cuenta con stock suficiente", "error");
            $('.vscanti_'+row).val("1");
        }
    }
    //console.log("band_cap: "+band_cap);
    if(band_cap==0 && band_stock==0){    
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/editarcantidad",
            data: {
                row:row,
                cantidad:cantidad
            },
            success: function (response){
                viewproductos();
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                
            }
        });
    }
}
function editarcantidad_ser(row){
    var cantidad=$('.vscanti_'+row).val();
    
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/editarcantidad_ser",
        data: {
            row:row,
            cantidad:cantidad
        },
        success: function (response){
            //viewservicios();
            viewproductos();
            $('.stockn_'+row).val(cantidad);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function validar_razon_social(){
    var razon_social=$('#razon_social_modal').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/get_validar_razon_social",
        data: {
            razon_social:razon_social,
        },
        success: function (response){
            if (response == 1) {
                validar_razon=1;
                swal("¡Atención!", "La razón social ya existe", "error");
            }else{
                validar_razon=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function validar_rfc_cliente(){
    var rfc=$('#rfc').val();
    var id = $("#idreg1").val();
    if(rfc!=""){
        $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/get_validar_rfc",
        data: {
            rfc:rfc, id:id
        },
        success: function (response){
            if (response == 1) {
                validar_rfc=1;
                swal("¡Atención!", "El rfc ya existe", "error");
            }else{
                validar_rfc=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
    }
}
function cerrar_turno(){
    $('#modalturno_cerrar').modal('show');
}
function abrir_turno(){
    $('#modalturno').modal({backdrop: 'static', keyboard: false}) 
    $('#modalturno').modal('show');
    setTimeout(function(){ 
        $('#monto').focus();
    }, 2000);
}

function validar_turno(){
    var monto = $('#monto').val();
    if(monto!='' && monto!=0){
        $.ajax({
            type:'POST',
            url: base_url+"Ventasp/set_abrir_turno",
            data:{monto:monto},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                if(data==1){
                    $(".cont_open").hide();
                    swal("Éxito!", "Guardado correctamente", "success");
                    setTimeout(function(){ 
                        //$('.adbn-wrap').css('display','none');
                        //$('#modalturno').modal('hide');
                        window.location.href = base_url+"index.php/Ventasp"; 
                    }, 1000); 
                }if(data==0){
                    swal("¡Atención!", "No puede abrir turno", "error");
                    $(".cont_open").hide();
                }
                if(data==2){
                    $(".cont_open").show();
                }
            }
        }); 
    }else{
        swal("¡Atención!", "Ingresa un monto", "error");
    }
}
function validar_cerrar_turno(){
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/set_cerrar_turno",
        //data:{monto:monto},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito!", "Turno cerrado correctamente", "success");
            setTimeout(function(){ 
                //$('.adbn-wrap').css('display','none');
                //$('#modalturno').modal('hide');
                window.location.href = base_url+"index.php/Ventasp"; 
            }, 1000); 
        }
    }); 
}
function buscar_precio(){
    $('#modalverificar').modal('show');
    $('.tabla_precios').html('');  
    /*setTimeout(function(){ 
        $('#input_barcodex').focus();
    }, 1000);*/

    setTimeout(function(){ 
        $('#input_searchx').select2('open');
        $('.select2-search__field').focus();
    }, 1000);
}
function tabla_precios_verificador(tipo,prod=0,tipo_prod=0){
    var codigo='';
    if(tipo==1){
        codigo=$('#input_barcodex').val();
    }if(tipo==2){
        codigo=$('#input_searchx').val();
    }
    //console.log("tipo: "+tipo);
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/get_tabla_verificador",
        data:{codigo:codigo,tipo:tipo,prod:prod, tipo_prod:tipo_prod},
        statusCode:{
            404: function(data){
                swal("¡Atención!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("¡Atención!", "500", "error");
            }
        },
        success:function(data){
            $('.tabla_precios').html(data);  
        }
    }); 
}
function check_producto(id){
    if($('#check_idproducto').is(':checked')){
        addproducto(id);
        $('#input_barcodex').val('');
        $('#modalverificar').modal('hide');
        $('.tabla_precios').html('');  
    }
}
function buscar_cliente(){
    $('#modal_cliente').modal('show');
    setTimeout(function(){ 
        $('#input_search_clientex').select2('open');
        $('.select2-search__field').focus();
    }, 2000);
}
function obtenercliente(id){
    $('.text_cliente').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/get_data_cliente",
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_cliente').html(data); 
            regimefiscalx(); 
            setTimeout(function(){ 
                var cliuso=$('#uso_cfdix').data('cliuso');
                if(cliuso != ''){
                    $('#uso_cfdix').val(cliuso);
                }
            }, 2000);
        }
    }); 
}

function guardar_registro_datos_cliente(){
    var form_register = $('#form_registro_datos_cliente_fiscales');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
            direccion:{
              required: true,
            },
            razon_social:{
              required: true,
            },
            rfc:{
              required: true,
            },
            cp:{
              required: true,
            },
            RegimenFiscalReceptor:{
              required: true,
            },
            uso_cfdi:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datos_cliente_fiscales").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/registro_datos_cliente_edit',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){  
                swal("Éxito!", "Guardado Correctamente", "success");
                
                setTimeout(function(){ 
                    $('#modal_cliente').modal('hide');
                    $('#input_search_clientex').val(null).trigger('change');
                    $('.text_cliente').html('');  
                }, 1000); 
            }
        });  
    }   
}

function regimefiscalcf(){
    $( "#uso_cfdixx").val(0);
    $( "#uso_cfdixx option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptorxx option:selected').val();
    $( "#uso_cfdixx ."+regimen ).prop( "disabled", false );
}
function view_presv(){
    //console.log("view_presv");
    var preventas = $('#preventas option:selected').val();
    if(preventas>0){ 
        limpiartabla();
        $.ajax({
            type:'POST',
            url: base_url+"Ventasp/view_presv",
            data:{
                ventaid:preventas
            },
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log(data);
                var array = $.parseJSON(data);
                $('#viewventa').val(array.venta.id);
                $('#observaciones').val(array.venta.observaciones);
                $('#idrazonsocial').html('<option data-id_cli="'+array.venta.id_razonsocial+'" data-id_clidcli="'+array.venta.id_cliente+'" value="'+array.venta.id_razonsocial+'">ID: '+array.venta.id_cliente+' / '+array.venta.razon_social+' / '+array.venta.nom_cli+'</option>');
                selectcliente();
                if($('#tipo_tec').val()!="1" && $("#perfilid").val()!=3){
                    $('#preventa').val(0);
                    $('.ocultarparapreenta').show();
                    $('.div_realizarventa').html('<b>Ingresar<br>venta</b>');
                    $('.label_realizarventa').html('Ingresar Venta');
                }

                $.each(array.ventadll, function(index, item) {
                    //console.log("id venta det: "+item.id);
                    if(item.tipo==1){ //producto o recarga
                        id_prod_suc=0;
                    }
                    if(item.tipo==0 || item.tipo==1){ //producto
                        if(item.tipo_prod==0){ //stock normal
                            id_prod_suc=item.id_ps;
                        }
                        if(item.tipo_prod==1){//series
                            id_prod_suc=item.idprod_serie;
                        }if(item.tipo_prod==2){ //lote
                            id_prod_suc=item.idprod_lote;
                        }
                        //addproducto(item.idproducto,item.tipo,item.tipo_prod,id_prod_suc);
                    }
                    if(item.tipo==2 || item.tipo==3){ //renta o servicio
                        //addservicios(item.idproducto);
                        id_prod_suc=0;
                    }
                    if(item.tipo!=2){
                        addproducto(item.idproducto,item.tipo,item.tipo_prod,id_prod_suc,1,preventas); //ultima varibale para entrar a if de timeout
                    }if(item.tipo==2){ //renta
                        var nuevaOpcion = $("<option></option>")
                            .val(item.id_renta) 
                            .text("Renta # "+item.id_renta); 

                        $("#id_renta").append(nuevaOpcion);
                        $("#id_renta").val(item.id_renta);
                        getRentaDet(item.id_renta);
                    }
                });
            }
        }); 
    }else{
        limpiartabla();
        $('#viewventa').val("");
        $('#observaciones').val("");
        //$('#idrazonsocial').html('<option data-id_cli="8255" value="8255">PUBLICO EN GENERAL</option>');
        $('#idrazonsocial').empty();  // Elimina todas las opciones
        $('#idrazonsocial').trigger('change'); 

        //1= preventa, 0 = venta normal
        //$('#preventa').val(0); //select de preventa limpia -- venta normal
    }
}
function view_cotizacion(){
    var idcotizacion = $('#idcotizacion option:selected').val();
    if(idcotizacion>0){
        $('#idcotizacion_cl').val(idcotizacion);
        $.ajax({
            type:'POST',
            url: base_url+"Ventasp/view_cotiza",
            data:{
                idcotizacion:idcotizacion
            },
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log(data);
                var array = $.parseJSON(data);
                $('#idcotizacion_cl').val(array.venta.id);
                $('#idrazonsocial').html('<option value="'+array.venta.id_razonsocial+'">'+array.venta.razon_social+'</option>');
                selectcliente();
                $('#preventa').val(0);
                $('.ocultarparapreenta').show();
                //$('.div_realizarventa').html('<b>Ingresar<br>venta</b>');
                //$('.label_realizarventa').html('Ingresar Venta');
                $.each(array.ventadll, function(index, item) { //acá me quedo y con el pdf de cotizacion
                    //console.log("tipo: "+item.tipo);           
                    //console.log("idproducto: "+item.idproducto);
                    tipo_prod=item.tipo_prod;
                    if(item.tipo_prod==1){//series
                        id_prod_suc=item.idprod_serie;
                    }else{
                        id_prod_suc=item.idprod_lote;
                    }
                    if(item.tipo==3){
                        tipo_prod=3;
                        addservicios(item.idproducto,array.venta.id);
                    }else{
                        addproducto(item.idproducto,item.tipo,tipo_prod,id_prod_suc,1);
                    }
                    //console.log("tipo_prod: "+item.tipo_prod); 
                });
            }
        }); 
    }
}
function modal_precios(id){
    $('.text_precios').html('');
    $('#modalprecios').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventasp/viewprecios_list',
        data: {id:id},
        success:function(data){
            $('.text_precios').html(data);
        }
    });
}
var stock_sucn=0;
var idsucursal=0;
var idproducto_suc=0;
var tipo_prod_tras=0;
function trasnpaso_suc(id,stock,idpro,tipo){
    var transsuc='';
    if(id==2){
        transsuc='Pino Suárez'; 
    }
    if(id==3){
        transsuc='Alfredo del Mazo'; 
    }
    if(id==4){
        transsuc='Jesús Carranza'; 
    }
    if(id==5){
        transsuc='Metepec'; 
    }
    if(id==6){
        transsuc='Almacén WEB'; 
    }
    if(id==7){
        transsuc='Almacén Rentas'; 
    }
    if(id==8){
        transsuc='Almacén General'; 
    }
    $('.suc_tras').html(transsuc);
    
    $('#cantidad_trans').val('');
    $('.ocultar_transpaso').css('display','block');
    stock_sucn=stock;
    idsucursal=id;
    idproducto_suc=idpro;
    tipo_prod_tras=tipo;
}
function transferir_cantidad() {
    var stockn=parseFloat(stock_sucn);
    var cantidad=$('#cantidad_trans').val();
    var cann=parseFloat(cantidad);
    if (isNaN(cann) || cann==0){
        swal("¡Atención!", "Falta agregar cantidad", "error");
    }else{  
        if(tipo_prod_tras==0){ //producto normal  
            if(stockn>=cann){
                /*tipo_codigo=0;
                setTimeout(function(){ 
                    $('#codigo_t1').focus();
                }, 1000); 
                $('#modalcodigo').modal('show');
                $.ajax({
                    type:'POST',
                    url: base_url+"Ventasp/add_alerta_traspasos",
                    data:{idsucursal:idsucursal,tipo:1},
                    statusCode:{
                        404: function(data){
                            alert("No Se encuentra el archivo!");
                        },
                        500: function(){
                            alert("500");
                        }
                    },
                    success:function(data){
                        //$('.tabla_precios').html(data);  
                    }
                }); */
                validar_codigo();
            }else{
                swal("¡Atención!", "La cantidad es mayor al stock", "error");
            }
        }else{ //recarga de tanque padre
            //console.log("stockn: "+stockn);
            //console.log("cann: "+cann);
            if(( cann % 9500 ) == 0 && stockn>=cann){
                //se debe guardar el detalle de la solicitud
                saveSolicitud(cann);
            }else{
                swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");  
            }
        }
    }
}
function saveSolicitud(cann){ //tipo 1 porque es de recarga de oxigeno
    $.ajax({
        type:'POST',
        url: base_url+'Ventasp/transferir_cantidad_sucursal',
        data: {
            idsucursal:idsucursal,cantidad:cann,idproducto:idproducto_suc,tipo:1
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){  
            $('#modalverificar').modal('hide');
            $('#input_searchx').val(null).trigger('change');
            $('.tabla_precios').html('');
            swal("Éxito!", "Solicitud generada", "success");
        }
    });
}
function validar_codigo(){
    /*var t1 = $('#codigo_t1').val();
    var t2 = $('#codigo_t2').val();
    var t3 = $('#codigo_t3').val();
    var t4 = $('#codigo_t4').val();
    var t5 = $('#codigo_t5').val();
    var n1=0;
    var n2=0; 
    var n3=0;
    var n4=0;
    var n5=0;
    if(t1==''){
        $('#codigo_t1').css('border-color','red');
        n1=0;
    }else{
        $('#codigo_t1').css('border-color','#012d6a');
        n1=1;
    }

    if(t2==''){
        $('#codigo_t2').css('border-color','red');
        n2=0;
    }else{
        $('#codigo_t2').css('border-color','#012d6a');
        n2=1;
    }

    if(t3==''){
        $('#codigo_t3').css('border-color','red');
        n3=0;
    }else{
        $('#codigo_t3').css('border-color','#012d6a');
        n3=1;
    }

    if(t4==''){
        $('#codigo_t4').css('border-color','red');
        n4=0;
    }else{
        $('#codigo_t4').css('border-color','#012d6a');
        n4=1;
    }

    if(t5==''){
        $('#codigo_t5').css('border-color','red');
        n5=0;
    }else{
        $('#codigo_t5').css('border-color','#012d6a');
        n5=1;
    }
    var suma = t1+t2+t3+t4+t5;

    if(n1==1 && n2==1 && n3==1 && n4==1 && n5==1){
        $('.btn_registro_valida').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/validar_codigo',
            data: {codigo:suma},
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                var validarc = parseFloat(data);
                if(validarc==0){
                    swal("¡Atención!","El código es incorrecto","error");
                    $('.btn_registro_valida').attr('disabled',false);
                }else{
                    if(tipo_codigo==0){
                        $('#modalcodigo').modal('hide');
                        $('#codigo_t1').val('');
                        $('#codigo_t2').val('');
                        $('#codigo_t3').val('');
                        $('#codigo_t4').val('');
                        $('#codigo_t5').val('');
                        swal({
                            title: "¡Éxito!",
                            text: "Código correcto",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        });*/
                        var cantidad=$('#cantidad_trans').val();
                        var cann=parseFloat(cantidad);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Ventasp/transferir_cantidad_sucursal',
                            data: {
                                idsucursal:idsucursal,cantidad:cann,idproducto:idproducto_suc,tipo:0
                            },
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                $('#modalverificar').modal('hide');
                                $('#input_searchx').val(null).trigger('change');
                                $('.tabla_precios').html('');
                                swal("Éxito!", "Solicitud generada", "success");
                                /*$.ajax({
                                    type:'POST',
                                    url: base_url+"Ventasp/get_tabla_verificador",
                                    data:{codigo:idproducto_suc,tipo:0},
                                    statusCode:{
                                        404: function(data){
                                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                                        },
                                        500: function(){
                                            Swal.fire("Error!", "500", "error");
                                        }
                                    },
                                    success:function(data){
                                        $('.tabla_precios').html(data);  
                                    }
                                }); */
                            }
                        });
                    /*}else{
                        adddescuento();
    
                        $('#modalcodigo').modal('hide');
                        
                        $('#codigo_t1').val('');
                        $('#codigo_t2').val('');
                        $('#codigo_t3').val('');
                        $('#codigo_t4').val('');
                        $('#codigo_t5').val('');
                        swal({
                            title: "¡Éxito!",
                            text: "Código correcto",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false
                        });
                        
                    }

                }
                
            }
        });
    }*/ 
}
/// cliente
function verificar_correo(){
    $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/validar_correo',
        data: {
            correo:$('#correo').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_correo=1;
                swal("¡Atención!", "El correo electrónico ya existe", "error");
            }else{
                validar_correo=0;
            }
        }
    });
}
function cambiaCP2(){
    var cp = $("#codigo_postal").val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Ventasp/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                //$(".fis_estado_"+id).val(array[0].estado);
                //$(".fis_ciudad_"+id).val(array[0].ciudad);
                cambia_cp_colonia2();
            }
        });

    }
}
function cambia_cp_colonia2(){
    var cp = $("#codigo_postal").val();
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Ventasp/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Ventasp/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }
            }
        });
    }else{
        //$(".fis_estado_"+id).attr("readonly",false);
    }
}
function regimefiscal(){
    $( "#uso_cfdi").val(0);
    $( "#uso_cfdi option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptor option:selected').val();
    $( "#uso_cfdi ."+regimen ).prop( "disabled", false );
}
function activar_campo(){
    if($('#desactivar').is(':checked')){
        $('.motivo_txt').css('display','block');
    }else{
        $('.motivo_txt').css('display','none');
    }
}
function guardar_registro_datos(){
    var form_register = $('#form_registro_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datos").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/registro_datos_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                //console.log("data: "+data);
                var array = $.parseJSON(data);

                var idp=parseFloat(array.id);
                var idf=parseFloat(array.idf);
                //console.log("idp: "+idp);
                //console.log("idf: "+idf);
                $('#idreg1').val(idp);
                $('#idreg2').val(idp);
                $('#idreg3').val(idp);
                $('#idreg4').val(idp);
                //swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);

                swal("Éxito!", "Se ha guardado el cliente", "success");
                //$('#modal_user_data').modal('hide');
                var data = {
                    id: idf,
                    text: "ID: "+idp+" / "+$('#razon_social').val()
                };
        
                var newOption = new Option(data.text, data.id, false, false);
                $('.idrazonsocial_cliente').append(newOption).trigger('change');
                $('.idrazonsocial_cliente').val(parseFloat(idf)).trigger('change.select2');
                $('#idrazonsocial option:selected').attr("data-id_cli",idf);
                $('#idrazonsocial option:selected').attr("data-id_clidcli",idp);
                setTimeout(function(){ 
                    validar_icono_user();
                }, 1000);

            }
        });    
    }   
}
function tipo_pago(num){
    if(num==1){
       $('.diaspagotxt').css('display','block');
       $('.saldopagotxt').css('display','none');
    }else if(num==2){
       $('.diaspagotxt').css('display','none');
       $('.saldopagotxt').css('display','block');
    }
}
function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/validar',
        data: {
            usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}
var pass_aux=0;
function clickoverpass(){
    var obj = document.getElementById('contrasena');
    var obj2 = document.getElementById('contrasena2');
    if(pass_aux==0){
        pass_aux=1;  
        obj.type = "text";
        obj2.type = "text";
    }else{
        pass_aux=0;
        obj.type = "password";
        obj2.type = "password";
    }  

}
function guardar_registro_datos_fiscales(){
    var idreg2=$('#idreg2').val();
    
    $("#form_registro_datos_fiscales").validate().destroy(); 

    if(idreg2!=0){
        var form_register = $('#form_registro_datos_fiscales');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        if($('#activar_datos_fiscales').is(':checked')){
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    razon_social:{
                    required: true,
                    },
                    rfc:{
                    required: true,
                    },
                    cp:{
                    required: true,
                    },
                    RegimenFiscalReceptor:{
                    required: true,
                    },
                    uso_cfdi:{
                    required: true,
                    },
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Ventasp/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);
                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }else{
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Ventasp/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function guardar_registro_pagos(){
    var idreg2=$('#idreg4').val();
    if(idreg2!=0){
        var form_register = $('#form_registro_pagos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                diascredito:{
                  required: true,
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_pagos").valid();
        if($valid) {
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Ventasp/registro_datos_pago',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){  
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_registro').attr('disabled',false);
                        //window.location = base_url+'Clientes';
                    }, 1000);

                }
            });      
        }   
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function guardar_registro_usuario(){
    var idreg3=$('#idreg3').val();
    if(idreg3!=0){
        var form_register = $('#form_registro_usuario');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Usuario:{
                  required: true,
                  minlength: 4
                },
                contrasena:{
                  required: true,
                  minlength: 5
                },
                contrasena2: {
                    equalTo: contrasena,
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_usuario").valid();
        if($valid) {
            if(validar_user==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Ventasp/registro_datos',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        var idp=data;
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //window.location = base_url+'Clientes';
                        }, 1000);

                    }
                });    
            }else{
                swal("¡Atención!", "El usuario ya existe", "error");
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }   
}
function guardar_registro_datosx(){
    var form_register = $('#form_registro_datosx');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datosx").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/registro_datos_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                var idp=parseFloat(array.id);
                var idf=parseFloat(array.idf);
                //console.log("idp: "+idp);
                //console.log("idf: "+idf);
                $('#idreg1').val(idp);
                $('#idreg2').val(idp);
                $('#idreg3').val(idp);
                $('#idreg4').val(idp);

                /*var idp=parseFloat(data);
                $('#idreg1x').val(idp);
                $('#idreg2x').val(idp);
                $('#idreg3x').val(idp);
                $('#idreg4x').val(idp);*/
                //swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);

                swal("Éxito!", "Se ha guardado el cliente", "success");
                //$('#modal_user_data').modal('hide');
                var data = {
                    id: parseFloat(idp),
                    text: $('#razon_socialx').val()
                };
        
                /*var newOption = new Option(data.text, data.id, false, false);
                $('.idrazonsocial_cliente').append(newOption).trigger('change');
                $('.idrazonsocial_cliente').val(parseFloat(idp)).trigger('change.select2');*/
                setTimeout(function(){ 
                    validar_icono_user();
                }, 1000);

            }
        });    
    }   
}
function activar_campox(){
    if($('#desactivarx').is(':checked')){
        $('.motivo_txt').css('display','block');
    }else{
        $('.motivo_txt').css('display','none');
    }
}
function cambiaCP2x(){
    var cp = $("#codigo_postalx").val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Ventasp/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                //$(".fis_estado_"+id).val(array[0].estado);
                //$(".fis_ciudad_"+id).val(array[0].ciudad);
                cambia_cp_colonia2x();
            }
        });

    }
}
function cambia_cp_colonia2x(){
    var cp = $("#codigo_postalx").val();
    $('#coloniax').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Ventasp/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Ventasp/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#coloniax').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }
            }
        });
    }else{
        //$(".fis_estado_"+id).attr("readonly",false);
    }
}
function regimefiscalx(){
    $( "#uso_cfdix").val(0);
    $( "#uso_cfdix option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptorx option:selected').val();
    $( "#uso_cfdix ."+regimen ).prop( "disabled", false );
}
function validar_rfc_clientex(){
    var rfc=$('#rfcx').val();
    var id = $("#idreg1x").val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/get_validar_rfc",
        data: {
            rfc:rfc, id:id
        },
        success: function (response){
            if (response == 1) {
                validar_rfc=1;
                swal("¡Atención!", "El rfc ya existe", "error");
            }else{
                validar_rfc=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}
function verificar_usuariox(){
    $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/validar',
        data: {
            usuario:$('#usuariox').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}
function guardar_registro_datos_fiscalesx(){
    var idreg2=$('#idreg2x').val();
    
    $("#form_registro_datos_fiscalesx").validate().destroy(); 

    if(idreg2!=0){
        var form_register = $('#form_registro_datos_fiscalesx');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        if($('#activar_datos_fiscalesx').is(':checked')){

            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    razon_social:{
                    required: true,
                    },
                    rfc:{
                    required: true,
                    },
                    cp:{
                    required: true,
                    },
                    RegimenFiscalReceptor:{
                    required: true,
                    },
                    uso_cfdi:{
                    required: true,
                    },
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscalesx").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Ventasp/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }else{
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: { },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscalesx").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Ventasp/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function validar_razon_socialx(){
    var razon_social=$('#razon_socialx').val();
    if(razon_social!=""){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/get_validar_razon_social",
            data: {
                razon_social:razon_social,
            },
            success: function (response){
                if (response == 1) {
                    validar_razon=1;
                    swal("¡Atención!", "La razón social ya existe", "error");
                }else{
                    validar_razon=0;
                }
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                
            }
        });
    }
}
function guardar_registro_pagosx(){
    var idreg2=$('#idreg4x').val();
    if(idreg2!=0){
        var form_register = $('#form_registro_pagosx');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                diascredito:{
                  required: true,
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_pagosx").valid();
        if($valid) {
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Ventasp/registro_datos_pago',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){  
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_registro').attr('disabled',false);
                        //window.location = base_url+'Clientes';
                    }, 1000);

                }
            });      
        }   
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}
function guardar_registro_usuariox(){
    var idreg3=$('#idreg3x').val();
    if(idreg3!=0){
        var form_register = $('#form_registro_usuariox');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Usuario:{
                  required: true,
                  minlength: 4
                },
                contrasenax:{
                  required: true,
                  minlength: 5
                },
                contrasena2x: {
                    equalTo: contrasenax,
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_usuariox").valid();
        if($valid) {
            if(validar_user==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Ventasp/registro_datosx',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        var idp=data;
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //window.location = base_url+'Clientes';
                        }, 1000);

                    }
                });    
            }else{
                swal("¡Atención!", "El usuario ya existe", "error");
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }   
}
function modal_resumen_validar(){
    var perf=$('#perfilid').val();
    if(perf==3){
        setTimeout(function(){ 
            $('#input_pinx').focus();
            $('#input_pinx').val('');
        }, 500);
        $('#modal_pin_resumen').modal('show');
    }else{
        get_modal_resumen();
    }
}
function get_modal_resumen(){
    $('#modal_resumen').modal('show');
}
function get_resumen_venta(){
    $.ajax({
        type:'POST',
        url: base_url+'Ventasp/get_resumen_ventas_sucursal',
        data: {f1:$('#f1').val(),f2:$('#f2').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.resumen_ventatxt').html(data);
        }
    });  

    $.ajax({
        type:'POST',
        url: base_url+'Ventasp/get_resumen_ventas_sucursal_total',
        data: {f1:$('#f1').val(),f2:$('#f2').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            var total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(data);
            $('.total_resumen').html('$'+data);
        }
    });    
}
function get_resumen_venta_tipo(){
    $('#modal_resumen_total').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'Ventasp/get_resumen_ventas_sucursal_tipos_pagos',
        data: {f1:$('#f1').val(),f2:$('#f2').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.resumen_venta_tipopagotxt').html(data);
        }
    });  
}
function get_validarpinx(){
    var input_pin=$('#input_pinx').val();
    var cont=input_pin.length;
    if(cont==4){
        $.ajax({
            type:'POST',
            url: base_url+'Ventasp/get_validar_pin_resumen',
            data: {
                input_pin:input_pin,
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                if(data==1){
                    $('#modal_pin_resumen').modal('hide');
                    get_modal_resumen();
                }else{
                    swal("¡Atención!",'Pin incorrecto, favor de intentar de nuevo','error', {
                      buttons: false,
                      timer: 2000,
                    });
                    setTimeout(function(){ 
                        $('#input_pinx').focus();
                        $('#input_pinx').val('');
                    }, 1000);
                }
            }
        });
    }
}
function add_venta_select(){
    var clienteid=$('#input_search_clientex option:selected').val();
    var id_razon=$('#input_search_clientex option:selected').data("id_cf");
    //var cliente=$('#input_search_clientex option:selected').text();
    var cliente= "ID: "+clienteid+" / "+$('#razon_socialx').val();
    //var newOption = new Option(cliente,clienteid, false, false);
    var newOption = new Option(cliente,id_razon, false, false);
    $('.idrazonsocial_cliente').append(newOption).trigger('change');
    //$('.idrazonsocial_cliente').val(parseFloat(clienteid)).trigger('change.select2');
    $('.idrazonsocial_cliente').val(parseFloat(id_razon)).trigger('change.select2');
    $('.idrazonsocial_cliente option:selected').attr("data-id_cli",id_razon);
    $('.idrazonsocial_cliente option:selected').attr("data-id_clidcli",clienteid);
    $('#modal_cliente').modal('hide');
    if(validar_input==0){
        $('#input_barcode').show('show');
        $('#input_search').hide('show');
        $('.select_option_search .select2-container').hide('show');
        $('.icono1').css('color','red');
        $('.icono2').css('color','white');
        setTimeout(function(){ 
            $('#input_barcode').focus();
        }, 1000);
    }else{
        $('#input_barcode').hide('show');
        $('#input_search').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.icono1').css('color','white');
        $('.icono2').css('color','red');
        
        setTimeout(function(){ 
            $('#input_search').select2('open');
            $('.select2-search__field').focus();
        }, 2000);
    }
}
function limpiar_select_cliente(){
    $('#input_search_clientex').val(null).trigger('change');
    $('.text_cliente').html('');
    setTimeout(function(){ 
        $('#input_search_clientex').select2('open');
        $('.select2-search__field').focus();
    }, 1000);
}
function modal_clausulas(){
    
    // Validar productos
    var productos = $("#table_datos tbody > tr");
    var num_p=productos.length;
    if(num_p!=0){
        $('#modal_clausulas').modal('show');
        $("#correo_cl").val("");
        tabla_clausulas_get();
    }else{
        swal("¡Atención!", "No es posible generar una cotización ya que no existe ningún producto seleccionado", "error");
    }
}
function enviar_correo_cotizacion(){
    realizarcotizacion(1);
}
////////////// COTIZACION
function realizarcotizacion(tipo){
    var viewventa = 0;
    var preventa = $('#preventa').val();
    //==============================================
    var productos = $("#table_datos tbody > tr");
    var DATAa  = [];
    productos.each(function(){         
        item = {};                    
        item ["tipo"]   = $(this).find("input[id*='vstipo']").val();
        item ["id_ps_lote"]   = $(this).find("input[id*='vsproid_ps_lot']").val();
        item ["id_ps_serie"]   = $(this).find("input[id*='vsproid_ps_ser']").val();
        item ["cant"]   = $(this).find("input[id*='vscanti']").val();
        item ["pro"]   = $(this).find("input[id*='vsproid']").val();
        item ["cunit"]   = $(this).find("input[id*='vsprecio']").val();
        item ["desc"]   = $(this).find("input[id*='vsdescuento']").val();
        item ["incluye_iva"]   = $(this).find("input[id*='incluye_iva']").val();

        DATAa.push(item);
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);

    var clausulas = $("#table_clausulas tbody > tr");
    var DATAcl  = [];
    clausulas.each(function(){         
        item = {};      
        var cl_txt=$(this).find("input[id*='clausula_cl']").val();
        if(cl_txt!=''){              
            item ["id"]   = $(this).find("input[id*='id_cl']").val();
            item ["clausula"] = $(this).find("input[id*='clausula_cl']").val();
            DATAcl.push(item);
        }
    });
    //INFOcl  = new FormData();
    aInfoacl   = JSON.stringify(DATAcl);
    //========================================
    var id_razonsocial =$('#idrazonsocial option:selected').val();
    var subtotal = $('#subtotal').val();
    var descuento = $('#descuentos').val();
    var iva = $('#impuestos').val();
    var total = $('#total').val();
    var vf=$('#venta_facturada').is(':checked')==true?1:0;
    var vc=$('#tipo_venta').is(':checked')==true?1:0;

    var datos='viewventa='+viewventa+'&preventa='+preventa+'&id_razonsocial='+id_razonsocial+'&subtotal='+subtotal+'&descuento='+descuento+'&iva='+iva+'&total='+total+'&vf='+vf+'&productos='+aInfoa+'&clausulas='+aInfoacl+'&tipo_venta='+vc+'&validarpin='+validarpin+'&idvendedor='+idvendedoraux+'&correo_cl='+$('#correo_cl').val();
    if(productos.length>0){
        var mail_Cli =$('#correo_cl').val();
        $('#modal_clausulas').modal('hide');
        if(tipo==1){
            $('body').loading({theme: 'dark',message: 'Enviando cotización por correo...'});
        }else{
            $('body').loading({theme: 'dark',message: 'Generando cotización...'});
        }
        
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/savecotizacion",
            data: datos,
            success: function (response){
                window.open(base_url+'Ventasp/imprimir_cotizacion/'+response,'_blank');
                
                //var new_win = window.open(window.location.href, base_url+'Cotizaciones/imprimir_cotizacion/'+response,'_blank') ;
                /*var new_win = window.open(base_url+'Cotizaciones/imprimir_cotizacion/'+response,'_blank') ;
                window.open(this.href,'_self');*/
                //if(new_win){
                    //console.log("cargó la pagina pdf de cotizacion");
                    setTimeout(function(){ 
                        if(tipo==1){
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Ventasp/enviar_aviso",
                                data: {id:response,correo:mail_Cli},
                                success: function (response){
                                    //console.log("envia pdf de cotizacion");
                                    swal("Éxito!", "Cotización enviada", "success");
                                     
                                    setTimeout(function(){ 
                                        limpiarventa(); 
                                        $('body').loading('stop');
                                        window.location = base_url+'Ventasp';
                                    }, 1000); 
                                },
                                error: function(response){
                                    toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                                    
                                }
                            });
                        }else{
                            swal("Éxito!", "Cotización generada", "success");
                            setTimeout(function(){ 
                                limpiarventa(); 
                                $('body').loading('stop');
                                window.location = base_url+'Ventasp';
                            }, 1000); 

                        }
                    }, 2500); 
                //}
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
                
            }
        });
    }else{
        //toastr.error('Agregar por lo menos un producto');
        swal("¡Atención!", "Debe agregar por lo menos un producto para venta", "error");
    }   
}
function add_clausulas(){
    tabla_clausulas(0,'',cont);
}
var cont_cl=0;
function tabla_clausulas(id,clausula,cont){ 

    var btnx='';
    if(cont_cl==0){
        btnx='<button class="btn btn-pill btn-primary" type="button" onclick="add_clausulas()"><i class="fa fa-plus"></i></button>';
    }else{
        btnx='<a onclick="deletecl('+cont_cl+')" class="btn"><i class="fa fa-trash" style="color:red"></i></a>'; 
    } 
    var html='<tr class="row_'+cont_cl+'">\
            <td>\
                <input type="hidden" id="id_cl" value="'+id+'">\
                <input type="text" id="clausula_cl" class="form-control" value="'+clausula+'">\
            </td>\
            <td>\
                '+btnx+'\
            </td>\
        </tr>';
    $('.tbody_clausulas').append(html);
    cont_cl++;
}
function deletecl(cont){
    $('.row_'+cont).remove();
}
function tabla_clausulas_get(){
    /*
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/get_clausulas",
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    tabla_clausulas(0,element.clausula);
                });
            }else{
                add_clausulas();
            } 
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
    */
    $('.tbody_clausulas').html("");
    tabla_clausulas(0,'Vigencia de cotización: 7 días hábiles',1);
    tabla_clausulas(0,'Cotizado: M.N',2);
    tabla_clausulas(0,'Condiciones de pago: 100% el costo puede variar sin previo aviso.',3);
    tabla_clausulas(0,'Tiempo de Entrega: Sobre  pedido 15 días hábiles libre en nuestras instalaciones o envío por cobrar',4);
    $("#correo_cl").val($("#idrazonsocial option:selected").data("mailc"));
}
function quitar_radio(){
    //$("input[type=radio][name=check_pago]").prop('checked', false);
    $(".tx_mp_monto").val('');
    $(".for_mp_digitos").val('');
    $("input[type=radio][id=venta_prod]").prop('checked', true);
}
function addrentas(id){
    $.ajax({
        type:'POST',
        url: base_url+"Ventasp/add_rentas",
        data: {
            id:id
        },
        success: function (response){
            /*$('#input_barcode').hide('show');
            $('#input_search').show('show');
            $('.select_option_search .select2-container').show('show');
            $('.icono1').css('color','white');
            $('.icono2').css('color','red');
            setTimeout(function(){ 
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 1000);*/

            $('#servicios').prop('checked', false);
            setTimeout(function(){ 
                $('#input_searchserv').select2('close');
                $('.select2-search__field').focus();
            }, 1500);
          
            /*setTimeout(function(){ 
                $('#venta_prod').prop('checked', true);
                input_search();
                $('#input_search').val(null).trigger('change');
                $('#input_search').select2('open');
                $('.select2-search__field').focus();
            }, 2000); */

            $(".tabla_r").show();
            viewrentas();
            
            //$('#venta_prod').prop('checked', true);

        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}
function viewrentas(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/viewrentas",
        data: {
            id:0
        },
        success: function (response){
            //console.log("response rentas: "+response);
            if(response!=""){
                $(".tabla_r").show();
                $('.table_datos_rentas_tbody').html(response);
            }else{
                $(".tr_rentas").remove();
            }
            
            setTimeout(function(){ 
                //select_producto();
            }, 1000); 
            //calculartotales();
            calculartotales();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
        }
    });
}
function select_producto(){
    $('.id_producto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasp/searchproductos_serie',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function producto_series(row){
    var id = $('#id_producto_s_'+row+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/get_producto_select_series",
        data: {
            id:id
        },
        success: function (response){
            //console.log(response);
            var array = $.parseJSON(response);
            var htmt='';
            array.forEach(function(element) {
                htmt+='<option value="'+element.id+'">'+element.serie+'</option>';
            });
            
            //alert(id);
            $('#id_series_s_'+row).html(htmt);
            calculartotales();
        },
    });
}
function obtenerperiodo(row){
    var id_ser = $('.vsproid_'+row+'').val();
    var per = $('.obtenerperiodo_'+row+' option:Selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/get_servicio_venta",
        data: {
            id:id_ser,
            per:per
        },
        success: function (response){
            //console.log(response);
            var array = $.parseJSON(response);
            
            $('.vstotal_'+row).val(array.precio);
            depositogarantia=array.depositogarantia;
            calculartotales();
        },
    });
}
function editarprecio(row){
    var precio=$('.vsprecio_'+row).val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/editarprecio",
        data: {
            row:row,
            precio:precio
        },
        success: function (response){
            //viewservicios();
            viewproductos()
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}

function modal_alert(){
    $('#modal_notificacion').modal('show');
}

function cambiarmensajeserror(codigo,text){
    if(codigo=='CFDI40144'){
        text='El campo Razón social del receptor, debe encontrarse en la lista de RFC inscritos no cancelados en el SAT.'
    }
    if(codigo=='301'){
        const pattern = /The 'Rfc' attribute is invalid/;
        if(pattern.test(text)){

            const textModificado = text
              .replace(/^The 'Rfc' attribute is invalid - The value/, 'El atributo \'Rfc\' no es válido: el valor')
              .replace(/is invalid according to its datatype '[^']*' - The Pattern constraint failed./, 'no es válido de acuerdo a su tipo de dato');
              text=textModificado;
            //console.log(textModificado);
        }
        const pattern2 = /The 'Descripcion' attribute is invalid/;
        if(pattern2.test(text)){

            const textModificado = text
              .replace(/^The 'Descripcion' attribute is invalid - The value/, 'El atributo \'Descripcion\' no es válido - El valor')
              .replace(/is invalid according to its datatype 'String' - The Pattern constraint failed./, 'no es válido según su tipo de datos \'String\' - La restricción de patrón falló.');
              text=textModificado;
            //console.log(textModificado);
        }

    }
    return text;
}
function load_preventas(){
    //console.log("load_preventas");
    var pre = $('#preventas option:selected').val();
    //if(pre>0){}else{pre=0;}
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/load_preventas",
        data: { row:0, pre:pre },
        success: function (response){
            //console.log("pre: "+pre);
            $('#preventas').html(response);
            //$('#preventas').val(pre);
            //$('#preventas').val(pre).trigger('change'); 
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
var s_cant=0;var s_name=''; var s_price_cu =0;var s_con_iva=0; var s_total=0;var count_row=0; var s_idpro=0;
var tipo_pro=0;
function sol_pro_desc(idpro,tipo){
    $('.info_de_solicitud').html('');$('.sol_aceptar').html('');$("#s_mont_por option").prop('disabled',false);$('#s_mont_efe').val(0).prop('readonly',false);tipo_desc();
    
    tipo_pro=tipo;
    $('#modal_descuentos').modal({backdrop: 'static', keyboard: false});
    $('#modal_descuentos').modal('show');
    count_row=$('.info_pro_'+idpro+'_'+tipo+' #count').val();
    s_idpro=$('.info_pro_'+idpro+'_'+tipo+' #vsproid').val();
    s_cant=$('.info_pro_'+idpro+'_'+tipo+' #vscanti').val();
    $('.s_cant').html(s_cant);
    s_name=$('.info_pro_'+idpro+'_'+tipo+' .name_pros').text();
    $('.s_name').html(s_name);
    s_price_cu=$('.info_pro_'+idpro+'_'+tipo+' #vsprecio').val();
    $('.s_price_cu').html(s_price_cu);
    s_con_iva=$('.info_pro_'+idpro+'_'+tipo+' #incluye_iva').val();
    calcular_sol_pro();
    
}
function calcular_sol_pro(){
    var desc=$('#s_mont_efe').val();
    if(desc==''){
        desc=0;
    }
    var subpro=(parseFloat(s_cant)*parseFloat(s_price_cu))-parseFloat(desc);
    if(s_con_iva==1){
        subpro=parseFloat(subpro)*1.16;
        subpro=redondear(subpro,2);
    }
    $('#s_mont_final').val(subpro);
}
function tipo_desc(){
    var tipo = $('input[name=tipo_descuento]:checked').val();
    if(tipo==1){
        $('#s_mont_por').hide('show');
        $('#s_mont_efe').show('show');
    }
    if(tipo==2){
        $('#s_mont_por').show('show');
        $('#s_mont_efe').hide('show');
    }
    
}
function cal_por_sol(){
    var por = $('#s_mont_por option:selected').val();
    if(por>0){
        var cant_des =(parseFloat(s_cant)*parseFloat(s_price_cu))*(parseFloat(por)/100);
        cant_des=redondear(cant_des,2);
    }else{
        var cant_des=0;
    }
    $('#s_mont_efe').val(cant_des);
    calcular_sol_pro();
}
var intervalo;
function soldescuento(){
    var tipo = $('input[name=tipo_descuento]:checked').val();
    var por = $('#s_mont_por option:selected').val();
    var desc_efe=$('#s_mont_efe').val();
    var m_final=$('#s_mont_final').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/soldescuento",
        data: { 
            cant:s_cant,
            idproduto:s_idpro,
            name:s_name,
            tipo_descuento:tipo,
            s_mont_por:por,
            s_mont_efe:desc_efe,
            s_mont_final:m_final,
            precio:s_price_cu,
            coniva:s_con_iva

        },
        success: function (response){
            toastr.success('Solicitud enviada');
            var v_sol='';
                v_sol='<h2>Solicitando descuento... Por favor no cierre la pantalla</h2>';
                v_sol+='<p><img src="'+base_url+'/public/img/loading0.gif"></p>'
            $('.info_de_solicitud').html(v_sol);
            intervalo = setInterval(() => {
                verificar_respuesta_sol_des();
            }, 2000);
            $('.sol_aceptar').html('<button class="btn btn-primary "  type="button" disabled>Aceptar</button>');
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
function verificar_respuesta_sol_des(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/soldescuento_verif",
        data: { row:0 },
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0){
                var datos=array[0];
                if(datos.status==1){//autorizado
                    $('input[name=tipo_descuento][value='+datos.tipo_descuento+']').prop('checked',true);
                    $('#s_mont_por').append('<option value="'+datos.s_mont_por+'">'+datos.s_mont_por+'%</option>');
                    $('#s_mont_por').val(datos.s_mont_por);
                    //=============================== desabilita las otras opciones
                    $("#s_mont_por option").each(function() {
                        if ($(this).val() != datos.s_mont_por) {
                            $(this).prop("disabled", true);
                        }
                    });
                    //=========================================
                    $('#s_mont_efe').val(datos.s_mont_efe).prop('readonly',true);
                    $('#s_mont_final').val(datos.s_mont_final);
                    tipo_desc();
                    //cal_por_sol();


                    clearInterval(intervalo);
                    $('.sol_aceptar').html('<button class="btn btn-success"  type="button" onclick="ok_desc()">Aceptar</button>');
                    var v_sol='';
                        v_sol='<h2>Descuento autorizado</h2>';
                        v_sol+='<p><img src="'+base_url+'/public/img/checked.svg"></p>'
                    $('.info_de_solicitud').html(v_sol);
                }
                if(datos.status==2){//rechazado
                    var v_sol='';
                        v_sol='<h2 style="color:red">Descuento rechazado</h2>';
                        //v_sol+='<p><img src="'+base_url+'/public/img/checked.svg"></p>'
                    $('.info_de_solicitud').html(v_sol);
                    $('.sol_aceptar').html('<button class="btn btn-primary" type="button" data-bs-dismiss="modal">Cerrar</button>');
                }
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
function ok_desc(){
    var n_des=$('#s_mont_efe').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasp/adddescuento",
        data: { 
            row:count_row,
            des:n_des,
            tipo:tipo_pro 
        },
        success: function (response){
            viewproductos();
            //viewservicios();
            $('#modal_descuentos').modal('hide');
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}