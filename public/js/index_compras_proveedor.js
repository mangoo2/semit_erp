var table;
var base_url=$('#base_url').val();

$(document).ready(function() {	
    tabla();
    $('#searchtext').focus();
    $("#fe1,#fe2").on("change",function(){
        if($("#fe1").val()!="" && $("#fe2").val()!="")
        tabla();
    });

    $("#btn_export_excel").on("click",function(){
        var id = $('#id').val();
        var fechai = $('#fe1').val();
        var fechaf = $('#fe2').val();

        if(fechai=="")
            fechai=0;
        if(fechaf=="")
            fechaf=0;

        window.open(base_url+"Proveedores/exportExcel/"+id+"/"+fechai+"/"+fechaf,"_blank");
    });

    /* ********************************/
});

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        destroy:true,
        "ajax": {
            "url": base_url+"Proveedores/getComprasProvee",
            type: "post",
            "data":{ id:$('#id').val(),f1:$('#fe1').val(),f2:$('#fe2').val()},
        },
        "columns": [
            {"data": "id"},
            {"data": "personal"},
            {"data": "reg"},
            {"data": "proveedor"},
            {"data": "codigo"},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="view_productos('+row.id+')"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        if(row.precompra=="0"){
                            if(row.estatus==1){
                                html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">OC Ingresada</span>';
                            }if(row.estatus==3){    
                                html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">No recibido</span>';
                            }if(row.estatus!=1 && row.estatus!=3){
                                html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Pendiente de ingreso</span>';
                            }
                        }if(row.precompra=="1" && row.cancelado==0){
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Pre compra</span>';
                        }if(row.precompra=="1" && row.cancelado==1){
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Cancelada</span>';
                        }
                    }else{
                        html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Eliminado</span>';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<a style="cursor:pointer" onclick="get_compra('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    
                    return html;
                }
            }
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function view_productos(id){
	$('#modalproductos').modal('show');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/OCS/view_productos",
        data: {
        	idcompras:id
        },
        success: function (response){
            $('.table_pro').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}
/* **************************************/
function get_compra(id){
    window.open(base_url+'OCS/imprimir_compra/'+id,'_blank'); 
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
