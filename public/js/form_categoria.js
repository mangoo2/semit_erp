var base_url = $('#base_url').val();

$(document).ready(function($) {
    $(".btn_registro").on("click",function(){
        guardar_registro_datos();
    });
});
function guardar_registro_datos(){
    var form_register = $('#form_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            clave: {
                required: true,
            },
            categoria: {
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        },        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error input
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_datos").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Categorias/registro_datos',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                    window.location = base_url+'Categorias';
                }, 1000);
            }
        });  
    }
}

