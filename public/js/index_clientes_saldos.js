var tablef;
var base_url=$('#base_url').val();
var idcliente=$('#idcliente').val();
$(document).ready(function() {	
    tablef = $('#table_datos').DataTable();
    loadtable();
});
function loadtable(){
	tablef.destroy();
	tablef = $('#table_datos').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Clientes/getlistventas",
            type: "post",
            "data": {
                'idcliente':idcliente,
            },
        },
        "columns": [
            {"data": "folio"},
            {"data": "name_suc"},
            {"data": "nombre"},
            {"data": "reg"},
            {"data": "razon_social"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="view_productos('+row.id+')"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<button type="button" class="btn btn-primary" onclick="view_formapagos('+row.id+','+row.total+')"><i class="fas fa-dollar-sign"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                     if(row.activo==1){
                     	html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">Activa</span>';
                     }
                     if(row.activo==0){
                     	html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Eliminada</span>';
                     }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
        /*"order": [[ 2, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        columnDefs: [ { orderable: false, targets: [8,9] }],*/
        
    }).on('draw',function(){
        
    });
}

function view_productos(id){
    $('#modalproductos').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/view_productos",
        data: {
            ventaid:id
        },
        success: function (response){
            $('.table_pro').html(response);
            
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}

function view_formapagos(id,total){
    $('#modalformaspagos').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/view_formapagos",
        data: {
            ventaid:id,
            total:total
        },
        success: function (response){
            $('.table_forma').html(response);
            
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}