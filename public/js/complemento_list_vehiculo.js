var base_url =$('#base_url').val();

$(document).ready(function($) {
	$('.modal').modal({
                        backdrop: 'static',
                        keyboard: true
                });

	table_transporte=$('#table_transporte').DataTable();
	loadtable_transporte();
	$('#btn_save_vehiculos').click(function(event) {
        var form = $('#form_vehiculos');
        if(form.valid()){
            var datos = form.serialize();
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cat_complemento/insertupdatevehiculos",
                data: datos,
                success:function(data){  
                    toastr.success('Guardado con exito');
                    loadtable_transporte();
                    $('#modal_add_trans').modal('hide');
                },
                error: function(response){
                    toastr.error("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                }
            });
        }else{
            toastr.error('Complete los campos requeridos');
        }
    });
});

function loadtable_transporte(){
	table_transporte.destroy();
	table_transporte=$("#table_transporte").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_trans",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"txtCodigo"},
		        {"data":"txtNombre"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a onclick="add_trans(1,'+row.id+')" type="button" class="btn btn-success mr-1 mb-1"><i class="fa fa-edit"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "asc" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});
}
function add_trans(tipo,id){
	$('#modal_add_trans').modal('show');
    if(tipo==0){
        var html='<div class="row">\
                    <input id="id" type="hidden" name="id" class="form-control"  readonly>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label for="txtCodigo" class="active" >Código</label>\
                        <input id="txtCodigo" name="txtCodigo" type="text" class="form-control" >\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                      <label id="" for="txtNombre" >Nombre</label> \
                      <input id="txtNombre" name="txtNombre" type="text" class="form-control">\
                    </div>\
                </div>';
    }else{
        var html='<div class="row">\
                    <input id="id" type="hidden" name="id" value="'+id+'" class="form-control"  readonly>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label for="txtCodigo"  class="active" >Código</label>\
                        <input id="txtCodigo" name="txtCodigo" type="text" class="form-control"  readonly>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                      <label id="" for="txtNombre" >Nombre</label> \
                      <input id="txtNombre" name="txtNombre" type="text" class="form-control">\
                    </div>\
                    <div class="col-sm-12 col-md-8 col-lg-8">\
                        <label  class="active" data-translate="">Tipo de permiso STC</label>\
                        <select id="tipo_permiso_sct" name="tipo_permiso_sct" required></select>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-4 col-lg-4 ">\
                        <label id="" for="txtNoPermiso" class="active" data-translate="">No. Permiso</label>\
                        <input id="num_permiso_sct" name="num_permiso_sct" class="form-control" type="text" required>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label id="" for="nombre_aseguradora" class="active" data-translate="">Aseguradora de responsabilidad civil*</label>\
                        <input id="nombre_aseguradora" name="nombre_aseguradora" class="form-control" type="text" required>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label id="" for="num_poliza_seguro" class="active" data-translate="">Póliza de responsabilidad civil*</label>\
                        <input id="num_poliza_seguro" name="num_poliza_seguro" type="text" class="form-control" required>\
                    </div>\
                    <!-- campos nuevos -->\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label  class="active" data-translate="">Aseguradora medio ambiente</label>\
                        <input id="AseguraMedAmbiente" name="AseguraMedAmbiente" type="text" class="form-control">\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label  class="active" data-translate="">Póliza medio ambiente</label>\
                        <input id="PolizaMedAmbiente" name="PolizaMedAmbiente" type="text" class="form-control">\
                    </div>\
                     <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label  class="active" data-translate="">Aseguradora de carga</label>\
                        <input id="AseguraCarga" name="AseguraCarga" type="text" class="form-control">\
                    </div>\
                     <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label  class="active" data-translate="">Póliza de carga</label>\
                        <input id="PolizaCarga" name="PolizaCarga" type="text" class="form-control">\
                    </div>\
                     <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label  class="active" data-translate="">Prima de seguro</label>\
                        <input id="PrimaSeguro" name="PrimaSeguro" type="number" class="form-control">\
                    </div>\
                    <!-- end campos nuevos -->\
                    <div class="col col-sm-12 col-md-12 col-lg-12 mb-2">\
                        <label  class="active" data-translate="">Tipo de configuración vehicular</label>\
                        <select id="configuracion_vhicular" name="configuracion_vhicular"></select>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label id="" for="txtPlaca" class="active" data-translate="">Placa</label>\
                        <input id="placa_vehiculo_motor" name="placa_vehiculo_motor" type="text" class="form-control">\
                    </div>\
                    <div class="input-field col-sm-12 col-md-6 col-lg-6">\
                        <label id="" for="txtAño" class="active" data-translate="">Año de modelo</label>\
                        <div>\
                            <input id="anio_modelo_vihiculo_motor" name="anio_modelo_vihiculo_motor" type="number" class="form-control">\
                        </div>\
                    </div>\
                    <div class="col-sm-12 col-md-8 col-lg-8">\
                        <label  class="active" data-translate="">Tipo de subremolque 1</label>\
                        <select class="form-control" name="subtipo_remolque" id="subtipo_remolque" requerido>\
                            <option></option>\
                            <option value="CTR001" > Caballete</option><option value="CTR002" > Caja</option>\
                            <option value="CTR003" > Caja Abierta</option><option value="CTR004" > Caja Cerrada</option>\
                            <option value="CTR005" > Caja De Recolección Con Cargador Frontal</option><option value="CTR006" > Caja Refrigerada</option>\
                            <option value="CTR007" > Caja Seca</option><option value="CTR008" > Caja Transferencia</option>\
                            <option value="CTR009" > Cama Baja o Cuello Ganso</option><option value="CTR010" > Chasis Portacontenedor</option>\
                            <option value="CTR011" > Convencional De Chasis</option><option value="CTR012" > Equipo Especial</option>\
                            <option value="CTR013" > Estacas</option><option value="CTR014" > Góndola Madrina</option>\
                            <option value="CTR015" > Grúa Industrial</option><option value="CTR016" > Grúa </option>\
                            <option value="CTR017" > Integral</option><option value="CTR018" > Jaula</option>\
                            <option value="CTR019" > Media Redila</option><option value="CTR020" > Pallet o Celdillas</option>\
                            <option value="CTR021" > Plataforma</option><option value="CTR022" > Plataforma Con Grúa</option>\
                            <option value="CTR023" > Plataforma Encortinada</option><option value="CTR024" > Redilas</option>\
                            <option value="CTR025" > Refrigerador</option><option value="CTR026" > Revolvedora</option>\
                            <option value="CTR027" > Semicaja</option><option value="CTR028" > Tanque</option>\
                            <option value="CTR029" > Tolva</option><option value="CTR031" > Volteo</option>\
                            <option value="CTR032" > Volteo Desmontable</option>\
                        </select>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-4 col-lg-4">\
                        <label id="" for="txtPlacaSubRemolque1" class="active" data-translate="">Placa</label>\
                        <input id="placa_remolque" name="placa_remolque" type="text" class="form-control" requerido>\
                    </div>\
                    <div class="col-sm-12 col-md-8 col-lg-8">\
                        <label  class="active" data-translate="">Tipo de subremolque 2</label>\
                        <select class="form-control" name="subtipo2_remolque" id="subtipo2_remolque" >\
                            <option></option>\
                            <option value="CTR001" > Caballete</option><option value="CTR002" > Caja</option>\
                            <option value="CTR003" > Caja Abierta</option><option value="CTR004" > Caja Cerrada</option>\
                            <option value="CTR005" > Caja De Recolección Con Cargador Frontal</option><option value="CTR006" > Caja Refrigerada</option>\
                            <option value="CTR007" > Caja Seca</option><option value="CTR008" > Caja Transferencia</option>\
                            <option value="CTR009" > Cama Baja o Cuello Ganso</option><option value="CTR010" > Chasis Portacontenedor</option>\
                            <option value="CTR011" > Convencional De Chasis</option><option value="CTR012" > Equipo Especial</option>\
                            <option value="CTR013" > Estacas</option><option value="CTR014" > Góndola Madrina</option>\
                            <option value="CTR015" > Grúa Industrial</option><option value="CTR016" > Grúa </option>\
                            <option value="CTR017" > Integral</option><option value="CTR018" > Jaula</option>\
                            <option value="CTR019" > Media Redila</option><option value="CTR020" > Pallet o Celdillas</option>\
                            <option value="CTR021" > Plataforma</option><option value="CTR022" > Plataforma Con Grúa</option>\
                            <option value="CTR023" > Plataforma Encortinada</option><option value="CTR024" > Redilas</option>\
                            <option value="CTR025" > Refrigerador</option><option value="CTR026" > Revolvedora</option>\
                            <option value="CTR027" > Semicaja</option><option value="CTR028" > Tanque</option>\
                            <option value="CTR029" > Tolva</option><option value="CTR031" > Volteo</option>\
                            <option value="CTR032" > Volteo Desmontable</option>\
                        </select>\
                    </div>\
                    <div class="input-field col-sm-12 col-md-4 col-lg-4">\
                        <label id="" for="txtPlacaSubRemolque2" class="active" data-translate="">Placa</label>\
                        <input id="placa2_remolque" name="placa2_remolque" type="text" class="form-control" >\
                    </div>\
                </div>';
    }
    $('#form_vehiculos').html(html);
	if(id>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cat_complemento/info_tras_fed",
            data: {
                id:id
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                    console.log(array);
                    $('#txtCodigo').val(array.txtCodigo);
                    $('#txtNombre').val(array.txtNombre);
                    //falta tipo_permiso_sct
                    if(array.tipo_permiso_sct!=null){
                        $('#tipo_permiso_sct').html('<option value="'+array.tipo_permiso_sct+'">('+array.tipo_permiso_sct+') '+array.des_f_c_tp+'</option>');
                    }
                    $('#num_permiso_sct').val(array.num_permiso_sct);
                    $('#nombre_aseguradora').val(array.nombre_aseguradora);
                    $('#num_poliza_seguro').val(array.num_poliza_seguro);
                    $('#AseguraMedAmbiente').val(array.AseguraMedAmbiente);
                    $('#PolizaMedAmbiente').val(array.PolizaMedAmbiente);

                    $('#AseguraCarga').val(array.AseguraCarga);
                    $('#PolizaCarga').val(array.PolizaCarga);
                    $('#PrimaSeguro').val(array.PrimaSeguro);
                    //falta configuracion_vhicular
                    if(array.configuracion_vhicular!=null){
                        $('#configuracion_vhicular').html('<option value="'+array.configuracion_vhicular+'">('+array.configuracion_vhicular+') '+array.des_f_c_ct+'</option>');
                    }
                    $('#placa_vehiculo_motor').val(array.placa_vehiculo_motor);
                    $('#anio_modelo_vihiculo_motor').val(array.anio_modelo_vihiculo_motor);
                    $('#subtipo_remolque').val(array.subtipo_remolque);
                    $('#placa_remolque').val(array.placa_remolque);
                    $('#subtipo2_remolque').val(array.subtipo2_remolque);
                    $('#placa2_remolque').val(array.placa2_remolque);
                    search_config_transporte();
                    search_f_c_tipopermiso();
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
	}else{
        search_config_transporte();
        search_f_c_tipopermiso();
	}
	
}
function search_config_transporte(){
	$('#configuracion_vhicular').select2({
		dropdownParent: $('#modal_add_trans'),
        width: 'resolve',
        minimumInputLength: 0,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        allowClear: true,
        ajax: {
            url: base_url+'Cat_complemento/search_config_transporte',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: '('+element.clave+')'+element.descripcion,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function search_f_c_tipopermiso(){
	$('#tipo_permiso_sct').select2({
		dropdownParent: $('#modal_add_trans'),
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar:',
        allowClear: true,
        ajax: {
            url: base_url+'Cat_complemento/search_f_c_tipopermiso',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clave,
                        text: '('+element.clave+')'+element.descripcion,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}