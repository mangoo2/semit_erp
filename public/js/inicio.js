var base_url = $('#base_url').val();
$(document).ready(function() {
    ////////////Verificador////////////
    $('#search_input_barcodex').click(function(event) {
        $('#input_barcodex').show('show');
        $('#input_searchx').hide('show');
        $('.select_option_search .select2-container').hide('show');
        $('.iconox1').css('color','red');
        $('.iconox2').css('color','white');
    });
    $('#search_input_searchx').click(function(event) {
        $('#input_barcodex').hide('show');
        $('#input_searchx').show('show');
        $('.select_option_search .select2-container').show('show');
        $('.iconox1').css('color','white');
        $('.iconox2').css('color','red');
    });

    $("#input_barcodex").keypress(function(e) {
        if(e.which == 13) {
            tabla_precios_verificador(1);
        }
    });

    input_searchx();
});

function buscar_precio(){
    $('#modalverificar').modal('show');
    $('.tabla_precios').html('');
    setTimeout(function(){ 
        $('#input_barcodex').focus();
    }, 1000);
}

function tabla_precios_verificador(tipo){
    var codigo='';
    if(tipo==1){
        codigo=$('#input_barcodex').val();
    }else{
        codigo=$('#input_searchx').val();
    }

    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_tabla_verificador",
        data:{codigo:codigo,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tabla_precios').html(data);  
        }
    }); 
}


function check_producto(id){
    if($('#check_idproducto').is(':checked')){
        addproducto(id);
        $('#input_barcodex').val('');
        $('#modalverificar').modal('hide');
        $('.tabla_precios').html('');  
    }
}

function input_searchx(){
    $('#input_searchx').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Inicio/searchproductos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' '+element.nombre

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        tabla_precios_verificador(2);
        $('#input_searchx').val(null).trigger('change');
        setTimeout(function(){ 
            //$('#input_searchx').select2('open');
            //$('.select2-search__field').focus();
        }, 2000);
    });
}
