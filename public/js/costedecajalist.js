var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    tabla=$("#tablecortes").DataTable();
    load();
    
});
function load(){
    var suc=$('#sucursalselect option:selected').val();
    tabla.destroy();
    tabla=$("#tablecortes").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Cortecaja/getlistado",
            type: "post",
            "data":{
                suc:suc
            },
            error: function(){
               $("#tablecortes").css("display","none");
            }
        },
        "columns": [
            
            {"data":"folio"},
            {"data":"name_suc"},
            {"data":"reg"},
            {"data":"idpersonal",
                "render": function ( data, type, row, meta ){
                var html='';
                    html=row.nom_a;
                    return html;
                }
            },
            {"data":"hora"},
            {"data":"idpersonal_c",
                "render": function ( data, type, row, meta ){
                var html='';
                    html=row.nom_c;
                    return html;
                }
            },
            {"data":"hora_c"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                html='<a onclick="viewcorte('+row.id+')" class="btn btn-primary"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            
        ],
        "order": [[ 2, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        
    
    });
}
function viewcorte(id){
    var url = base_url+'reportes/cortexz/'+id+'/0';//producccion normal
    //var url = base_url+'reportes/cortexz/'+id+'/1';//entrar directamente a la generacion de archivo
    window.open(url, '_blank');
}