var base_url = $('#base_url').val();
var validar_user=0;
$(document).ready(function() {
	var avatar1 = new KTImageInput('kt_image_1');
    check_acceso();
    verificaTipo();

    $("#perfilId").on("change",function(){
        verificaTipo();
    });
});

function verificaTipo(){
    if($("#perfilId option:selected").data("tecnico")==1){
        $(".cont_porc_tec").show();
    }else{
        $(".cont_porc_tec").hide();
    }
}

function guardar_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
        	nombre:{
              required: true
            },
            Usuario:{
              required: true,
              minlength: 4
            },
            contrasena:{
              required: true,
              minlength: 5
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            perfilId:{
              required: true,
            },
            cp:{
              maxlength: 5
            },
        },

        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        if(validar_user==0){
            var sucursal = $('#sucursal option:selected').val();
            var validar=0;
            var validarp=0;
            if(sucursal==2){
                validar=1;
            }else{
                validar=0;
            }
            if(validar==1){
                var pin=$('#pin').val();
                if(pin==''){
                    validarp=1;
                    swal("¡Atención!", "El campo pin de venta esta vacio", "error");
                }else{
                    validarp=0;
                }
            }
            if(validarp==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Usuarios/registrar_datos',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    	var idp=data;
                    	guardar_registro_usu(idp);
                        add_file(idp);
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            window.location = base_url+'Usuarios';
                        }, 1000);

                    }
                });
            }
        }else{
            swal("¡Atención!", "El usuario ya existe", "error");
        }
    }   
}

function v_pin(){
    var Max_Length = 4;
    var length = $("#pin").val().length;
    if (length > Max_Length) {
       swal("¡Atención!", "Por favor solo ingresar 4 caracteres", "error");
       $("#pin").val('');
    }
}

function guardar_registro_usu(id){
	var datos = $('#form_registro').serialize()+'&personalId_aux='+id;
	$.ajax({
            type:'POST',
            url: base_url+'Usuarios/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Usuarios/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Usuarios/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
var pass_aux=0;
function clickoverpass(){
    var obj = document.getElementById('contrasena');
    var obj2 = document.getElementById('contrasena2');
    if(pass_aux==0){
        pass_aux=1;  
        obj.type = "text";
        obj2.type = "text";
    }else{
        pass_aux=0;
        obj.type = "password";
        obj2.type = "password";
    }  

}

function perfil_change(){
    var perfilId = $('#perfilId option:selected').val();
    var id_ult= Number($("#id_ult").val());
    /*if(perfilId==1 || perfilId==6){ //PREGUNTAR SI SE QUEDA 
        $('.sucursal_div').show('show');
        //$('.pin_div').css('display','none');
    }else{
        $('.sucursal_div').hide('show');
        //$('.pin_div').css('display','none');
        //$('#sucursal').val(1);
        $('#pin').val('');
    }*/

    if(perfilId==3){
        setTimeout(function(){ 
            $( "#sucursal option").prop( "disabled", true );
            $( "#sucursal .option_"+2).prop( "disabled", false );
            $('select[id="sucursal"] option[value="2"]').prop("selected",true);
        }, 500); 
    }else if(perfilId==2){
        setTimeout(function(){ 
            $( "#sucursal option").prop( "disabled", true );
            for (var i=0; i<=id_ult; i++) {
                $( "#sucursal .option_"+i).prop( "disabled", false );
            }
            $( "#sucursal .option_"+6).prop( "disabled",true);
            $( "#sucursal .option_"+7).prop( "disabled",true);
            $( "#sucursal .option_"+8).prop( "disabled",true);
        }, 500); 
    }else if(perfilId==5){ //almacen
        setTimeout(function(){ 
            $( "#sucursal option").prop( "disabled", true );
            $( "#sucursal .option_"+8).prop( "disabled",false);
            $('select[id="sucursal"] option[value="8"]').prop("selected",true);
        }, 500); 
    }
    else{
        $( "#sucursal option").prop( "disabled", false );
    }
}

function select_sucursal(){
    var sucursal = $('#sucursal option:selected').val();
    if(sucursal==2){
        $('.pin_div').show('show');
    }else{
        $('.pin_div').hide('hide');
    }
}

function check_acceso(){
    if($('#acceso').is(':checked')){
        $('.ocultar_motivo').css('display','block');
    }else{
        $('.ocultar_motivo').css('display','none');
    }
}