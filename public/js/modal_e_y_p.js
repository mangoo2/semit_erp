var base_url=$('#base_url').val();
var idpro_temp=0;
var tipo_temp=0;
$(document).ready(function($) {
	$('#id_select_all_pro').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Producto',
        dropdownParent: $('#modal_existencias_precios'),
        allowClear: true,
        templateResult: formatStategen,
        ajax: {
            url: base_url+'Sistema/searchallpros',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                var stock=0;
                //console.log(data);
                data = $.parseJSON(data);
                data.forEach(function(element) {
                    /*if(element.incluye_iva==1){
                        precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        if(element.iva==0){
                            incluye_iva=0;
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio*1.16).toFixed(2);
                            sub_iva=parseFloat(element.precio*.16).toFixed(2);
                        }
                    }else{
                        if(element.iva>0){
                            siva=1.16;
                            precio_con_ivax=parseFloat(element.precio/siva).toFixed(2);
                        }else{
                            precio_con_ivax=parseFloat(element.precio).toFixed(2);  
                        }
                    }*/
                    if(element.tipo==0){
                        stock=(Number(element.stock)-Number(element.traslado_stock_cant));
                    }if(element.tipo==1){ //serie
                        if(element.status_trasr!=2){
                            stock=(Number(element.serie8)-Number(element.traslado_serie_cant));
                        }else{
                            stock=Number(element.serie8);
                        }
                        
                    }if(element.tipo==2){ //lote
                        if(element.status_traslt!=2){
                            stock=(Number(element.lote8)-Number(element.traslado_lote_cant));
                        }else{
                            stock=Number(element.lote8);
                        }
                    }if(element.tipop!=0){
                        stock=Number(element.stock);
                    }
                    /*console.log("tipo: "+element.tipo);
                    console.log("tipop: "+element.tipop);
                    console.log("codigo: "+element.codigo);
                    console.log("nombre: "+element.nombre);
                    console.log("stock: "+stock);
                    console.log("precio: "+element.precio);
                    console.log("precio_con_ivax: "+precio_con_ivax);*/
                    precio_con_ivax = parseFloat(element.precio_con_ivax).toFixed(2);
                    itemscli.push({
                        id: element.id,
                        //id: element.id,
                        text: element.codigo+' / '+element.nombre,
                        micod:element.codigo,
                        tipop: element.tipop,
                        nombre:element.nombre,
                        stock: stock,
                        //precio:precio_con_ivax
                        precio:precio_con_ivax
                    });
                });
                /*itemscli.sort(function(a, b) {
                    return a.micod.localeCompare(b.micod);
                });*/
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
    	//$('#id_select_all_pro').html('');
        var data = e.params.data;
        consultarproductosall(data.id,data.tipop,data.nombre);

        /*setTimeout(function(){ 
            //$('#id_select_all_pro').val(null).trigger('change');
            //$('#id_select_all_pro').select2('open');
            //$('.select2-search__field').focus(); 
        }, 1000); */
    });
	document.addEventListener("keydown", function(event) {
        //console.log(event.ctrlKey);
        //console.log(event.code);
        if (event.altKey && event.code === "Digit5") {
            //buscar_precio();
            modal_e_y_p();
        }
        if (event.altKey && event.code === "Numpad5") {
            //buscar_precio();
            modal_e_y_p();
        }
    });
});

function formatStategen (state) {
    if (!state.id) {
        return state.text;
    }
  
    var $state = $(
    '<span class="colum_in_1">'+state.micod+'</span><span class="colum_in_2">'+state.nombre+'</span><span class="colum_in_3">'+state.stock+'</span> <span class="colum_in_4">$ '+state.precio +'</span>'
    );
    return $state;
}

function modal_e_y_p(){
	$('.tbody_lis_pre_e_y_p').html('');
	$('.nombre_producto_all').html('');
    $('.tbody_lis_exi_e_y_p').html('');
	$('#modal_existencias_precios').modal('show');
    setTimeout(function(){ 
        $('#id_select_all_pro').select2('open');
        $('.select2-search__field').focus(); 
    }, 1000);
    
}
function consultarproductosall(idpro,tipo,nombre){
	$('.nombre_producto_all').html("Producto: "+nombre);
    idpro_temp=idpro;
    tipo_temp=tipo;
	consultarpreciosgeneral(idpro,tipo);
}
function consultarpreciosgeneral(idpro,tipo){
    var tipo_ext=$('#soloexit').is(':checked')==true?1:0;
	$.ajax({
        type:'POST',
        url: base_url+'Sistema/viewprecios_ptipo',
        data: {
        	id:idpro,
        	tipo:tipo
        },
        success:function(data){
            $('.tbody_lis_pre_e_y_p').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'Sistema/viewexistencias_ptipo',
        data: {
        	id:idpro,
        	tipo:tipo,
            ext:tipo_ext
        },
        success:function(data){
            $('.tbody_lis_exi_e_y_p').html(data);
        }
    });
    setTimeout(function(){ 
        $('#id_select_all_pro').select2('open');
        $('.select2-search__field').focus(); 
    }, 1000);
}

function refreshdata(){
    consultarpreciosgeneral(idpro_temp,tipo_temp);
}


function aperturar_caja(){
    var monto = $('#monto').val();
    if($("#id_sucu_aper option:selected").val()=="0"){
        swal("¡Atención!", "Indique una sucursal", "error");
        return;
    }
    if(monto!='' && monto!=0){
        $.ajax({
            type:'POST',
            url: base_url+"Sistema/apertura_caja_suc",
            data:{monto:monto, sucursal: $("#id_sucu_aper option:selected").val()},
            success:function(data){
                //console.log("data: "+data);
                if(data==1){
                    $(".cont_open").hide();
                    swal("Éxito!", "Aparturado correctamente", "success");
                    location.reload();
                }if(data==0){
                    $(".cont_open").hide();
                    swal("¡Atención!", "No puede abrir turno", "error");
                }if(data==2){
                    $(".cont_open").hide();
                    swal("¡Atención!", "Sucursal ya aperturada. No puede volver a aperturar", "error");
                }
                if(data==2){
                    $(".cont_open").show();
                }
            }
        }); 
    }else{
        swal("¡Atención!", "Ingresa un monto", "error");
    }
}