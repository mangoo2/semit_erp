var base_url =$('#base_url').val();

$(document).ready(function($) {
	

	table_platilla=$('#table_platilla').DataTable();
	loadtable_platilla();
	$('#btn_save_plat').click(function(event) {
        var form = $('#form_platilla');
        if(form.valid()){
        	var datos = form.serialize();
        	$.ajax({
	            type:'POST',
	            url: base_url+"index.php/Cat_complemento/insertaplatillag",
	            data: datos,
	            success:function(data){  
	            	toastr.success('Guardado con exito');
	                loadtable_platilla();
	                $('#modal_add_platilla').modal('hide');
	                
	            },
	            error: function(response){
	                toastr.error("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
	            }
	        });
        }else{
        	toastr.error('Complete los campos requeridos');
        }
	});
	
});

function loadtable_platilla(){
	table_platilla.destroy();
	table_platilla=$("#table_platilla").DataTable({
    		stateSave: true,
    		"bProcessing": true,
    		"serverSide": true,
		    search: {
		                return: true
		            },
		    "ajax": {
		        "url": base_url+"Cat_complemento/get_listado_plantilla",
		        type: "post",
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        {"data":"txtNumDocumento"},
		        {"data":"txtIdentificador"},
		       	{"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	if(row.selectTipoComprobante=='T'){
		                		html='Traslado';
		                	}
		                	if(row.selectTipoComprobante=='I'){
		                		html='Ingreso';
		                	}
		                	
		            return html;
		            }
		        },
		        {"data":"txtFechaInicio"},
		        {"data":"hora_salida"},
		        {"data":"txtFechaFin"},
		        {"data":"hora_llegada"},
		        {"data":"txtNombre"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		                var html="";
		                	html+='<div class="divbuttonstable">';
		                    html+='<a href="'+base_url+'Cat_complemento/cat_carta_porte_inf/'+row.id+'" type="button" class="btn btn-success mr-1 mb-1"><i class="fa fa-file"></i></a>';
		                    html+='<a onclick="eliminar_reg('+row.id+')" type="button" class="btn btn-danger mr-1 mb-1"><i class="fa fa-trash"></i></a>';
		                    html+='</div>';
		            return html;
		            }
		        },
		    ],
    		"order": [[ 0, "DESC" ]],
    		"lengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
  	}).on('draw',function(){
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
   	});

}
function add_platilla(id){
	$('#modal_add_platilla').modal('show');
	if(id>0){

	}else{

	}
}