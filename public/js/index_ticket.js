var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
    tabla.search('').draw();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"Ticket/getlistado",
            type: "post",
            "data":{estatus:$('#estatus').val(),tipoticket:$('#tipoticket').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row) {
                    var tc='TSPXC';
                    var str=row.id;   
                    return tc+pad(str, 4);
                }
            },
            {"data":"nombre"},
            {"data":"reg"},
            {"data":"asunto"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.estatus==1){
                    html='<button class="btn btn-danger btn-sm" type="button">Por revisar</button>';
                }else if(row.estatus==2){
                    html='<button class="btn btn-sm" style="background-color: #FF9800 !important; border-color: #FF9800 !important;" type="button">Revisión</button>';
                }else if(row.estatus==3){
                    html='<button class="btn btn-success btn-sm" type="button">Inactivo</button>';
                }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Ticket/form/'+row.id+'" class="btn btn-primary btn-sm">Ver ticket</a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
     }).on('draw',function(){
        /*
        $('#search').keyup(function(){
            table.search($(this).val()).draw() ;
        });
        */

    });
}

function pad(str, max){ 
    str = str.toString(); 
    return str.length < max ? pad("0" + str, max) : str; 
}


function search(){
    var searchtext = $('#searchtext').val();
    //console.log(searchtext);
    tabla.search(searchtext).draw();
}
