var table;
var base_url=$('#base_url').val();

$(document).ready(function() {	
    tabla();
    $('#searchtext').focus();

    /* ********************************* */
    $("#addLote").on("click",function(){
        addLote(0,"","","");
    });

    $(".cantpri").on("change",function(){
        validaCant();
    });

    $("#back_table").on("click",function(){
        $("#cont_deta_lote").hide();
        $("#cont_table").show("slow");
    });
    $("#saveLotes").on("click",function(){
        saveLotes();
    });

    $("#back_table2").on("click",function(){
        $("#cont_deta_serie").hide();
        $("#cont_table").show("slow");
    });
    $("#saveSeries").on("click",function(){
        saveSeries();
    });
    /* ********************************/
});

function reload_registro(){
    //table.destroy();
    tabla();
}

function reload_registrox(){
    var f1=$('#fe1').val();
    var f2=$('#fe2').val();
    if(f1!='' && f2!=''){
        table.destroy();
        tabla();
    }
}

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        destroy:true,
        "ajax": {
            "url": base_url+"index.php/OCS/getlistcompras",
            type: "post",
            "data":{estatus:$('#estatus').val(),f1:$('#fe1').val(),f2:$('#fe2').val()},
        },
        "columns": [
            {"data": "id"},
            {"data": "personal"},
            {"data": "reg"},
            {"data": "fecha_ingreso",
                render:function(data,type,row){
                    var html='';
                    if(row.fecha_ingreso!=""){
                        html=row.fecha_ingreso;
                    }
                    return html;
                }
            },
            {"data": "proveedor"},
            {"data": "codigo"},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<button type="button" class="btn btn-primary" onclick="view_productos('+row.id+')"><i class="fas fa-eye"></i></button>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.precompra==0){
                        html='<button type="button" class="btn btn-primary" onclick="view_lote_serie('+row.id+')"><i class="fas fa-eye"></i></button>';
                    }
                    return html;
                }
            },
            {"data": "factura"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.activo==1){
                        if(row.precompra=="0"){
                            if(row.estatus==1){
                                html='<span class="badge m-l-10" style="background: #93ba1f; width: 90%;">OC Ingresada</span>';
                            }if(row.estatus==3){    
                                html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">No recibido</span>';
                            }if(row.estatus!=1 && row.estatus!=3){
                                html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Pendiente de ingreso</span>';
                            }
                        }if(row.precompra=="1" && row.cancelado==0){
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Pre compra</span>';
                        }if(row.precompra=="1" && row.cancelado==1){
                            html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Cancelada</span>';
                        }
                    }else{
                        html='<span class="badge m-l-10" style="background: #ba1f1f; width: 90%;">Eliminado</span>';
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    //if(row.precompra==0){
                        html+='<a style="cursor:pointer" onclick="get_compra('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    //}
                    if(row.activo==1 && row.precompra==0){
                        if(row.estatus==0){
                            html+='<a href="'+base_url+'Comprasp?idcompra='+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/edit.svg"></a>\
                            <a onclick="validar_modal('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                            html+='<button class="badge m-l-10" style="background: #ba1f1f;" onclick="update_estatus('+row.id+')">No recibido</button>';
                        }
                    }
                    if(row.precompra==0){
                        html+='<a href="'+base_url+'Comprasp?idcomprav='+row.id+'" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/duplicate.svg"></a>';
                    }if(row.precompra==1 && row.cancelado==0){
                        html+='<a title="Cancelar Precompra" onclick="change_estatus('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                    }
                    return html;
                }
            }
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
        /*"order": [[ 2, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        columnDefs: [ { orderable: false, targets: [8,9] }],*/
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function change_estatus(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de cancelar la precompra?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"OCS/cancelarPre",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Actualizado correctamente", "success");
                        tabla();
                    }
                });                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function view_productos(id){
	$('#modalproductos').modal('show');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/OCS/view_productos",
        data: {
        	idcompras:id
        },
        success: function (response){
            $('.table_pro').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
}
/* ****************************** */
function view_lote_serie(id){
    $('#modaldeta').modal('show');
    $("#cont_deta_lote").hide();
    $("#cont_deta_serie").hide();
    $("#cont_table").show("slow");
    /*$.ajax({
        type:'POST',
        url: base_url+"OCS/view_prods_lote_serie",
        data: {
            idcompras:id
        },
        success: function (response){
            $('.deta_prods').html(response);
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
        }
    });
    */
    tabla=$("#table_detas").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"OCS/view_prods_lote_serie",
            type: "post",
            data: {idcompras:id},
            error: function(){
               $("#table_detas").css("display","none");
            }
        },
        "columns": [
            {"data":"idProducto"},
            {"data":"nombre"},
            {"data":"cantidad"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var color='';
                if(row.tipo_prod==1 || row.tipo_prod==2){
                    if(row.total_series>0){
                        color='style="background: #216f21 !important; border: solid #216f21 !important;"';
                    }
                    if(row.total_lote>0){
                        color='style="background: #216f21 !important; border: solid #216f21 !important;"';
                    }
                    html+='<button type="button" '+color+' onclick="setLoteSerie('+row.id_cd+','+row.cantidad+','+row.tipo_prod+',\''+row.idProducto+'\','+row.id+','+id+')" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
                }
                return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }
    });
}

function setLoteSerie(id,cant,tipo,cod,idprod,idcomp){
    //console.log("tipo: "+tipo);
    $("#cont_table").hide("slow");
    if(tipo==1){ //serie
        $("#cont_deta_serie").show();
        $("#cont_deta_lote").hide();
        $("#cod_prod_s").html(cod);
        $('#id_cd_s').val(id);
        $("#cant_comp_s").val(cant);
        $("#id_prod_com_s").val(idprod);
        $("#id_comp_s").val(idcomp);
        $(".series_add").remove(); 
        $.ajax({
            type:'POST',
            url: base_url+"OCS/get_data_series",
            data: {idcomp:idcomp, idprod:idprod},
            success: function (response){
                var array = $.parseJSON(response);
                //console.log("array.length: "+array.length);
                if(array.length>0) {
                    array.forEach(function(i) {
                        addSerie(i.id,i.serie);
                    });
                    if(array.length==cant){
                        $(".tr_col_sr").remove();
                    }
                }else{
                    for(var j=1; j<cant; j++){
                        addSerie(0,"");
                    }
                }
            }
        });
    }else{ //lote
        $("#cont_deta_serie").hide();
        $("#cont_deta_lote").show();
        $("#cod_prod").html( cod);
        $('#id_cd').val(id);
        $("#cant_comp").val(cant);
        $(".cantpri").val(cant);
        $("#id_prod_com").val(idprod);
        $("#id_comp").val(idcomp);
        $(".lotes_add").remove(); 
        if(cant==1){
            $('#addLote').attr("disabled",true);  
        }else{
            $('#addLote').attr("disabled",false);
        }
        $.ajax({
            type:'POST',
            url: base_url+"OCS/get_data_lotes",
            data: {idcomp:idcomp, idprod:idprod},
            success: function (response){
                var array = $.parseJSON(response);
                if(array.length>0){
                    $(".lotepri").val("");
                    $(".caducidadpri").val("");
                    $(".cantpri").val("");
                    array.forEach(function(i) {
                        addLote(i.id,i.lote,i.caducidad,i.cantidad);
                    });
                }else{
                    //addLote(0,"","","");
                }
            }
        });
    } 
}

function validaCant(){
    //var val=Number($("#cantidad").val());
    var val =0;
    /*$(".cantidad").each(function(){
        val=val+Number($(".cantidad").val());
    });*/
    var TABLA = $("#table_lotes_add tbody > tr");
    TABLA.each(function(){
        if($(this).find("input[class*='cantidad']").val()!=""){
            val=val+Number($(this).find("input[class*='cantidad']").val());
        }     
    });
    //console.log("val: "+val);
    var cant_comp=Number($("#cant_comp").val());
    if(cant_comp<val){
        swal("¡Atención!","La cantidad es mayor a la cantidad de compra","error");
        //$("#cantidad").val(cant_comp);
        $(".cantidad_add").val("");
        return;
    }
    if(val==0){
        swal("¡Atención!","La cantidad no puede ser 0","error");
        if($("#table_lotes_add tbody > .lotes_add").length==0){
            $("#cantidad").val(cant_comp);
        }
        return;
    }
}

function saveLotes(){
    var lotes = $("#table_lotes_add tbody > tr");
    var DATAlt  = []; var valid=0;
    lotes.each(function(){
        item = {};
        item['idcompra']=$("#id_comp").val();
        item['idproducto']=$("#id_prod_com").val();
        item['id']=$(this).find("input[id*='id']").val();
        item['lote']=$(this).find("input[id*='lote']").val();
        item['caducidad']=$(this).find("input[id*='caducidad']").val();
        item['cantidad']=$(this).find("input[id*='cantidad']").val();
        DATAlt.push(item);
        if($(this).find("input[id*='lote']").val()=="" || $(this).find("input[id*='caducidad']").val()=="" || $(this).find("input[id*='cantidad']").val()==""){
            valid++;
            swal("¡Atención!","Se debe llenar todos los campos","error");
            return false;
        }
    });
    INFOlt  = new FormData();
    aInfolt   = JSON.stringify(DATAlt);
    if(valid==0){
        var datos='lotes='+aInfolt;
        $.ajax({
            type:'POST',
            url: base_url+"OCS/ingresar_lotes",
            data: datos,
            success: function (response){
                swal("Éxito!", "Lote(s) guardado correctamente", "success"); 
                $("#cont_deta_lote").hide();
                $("#cont_table").show("slow");
            }
        });
    }
}
var cont_lot=1;
function addLote(id,lote,cad,cant){
    var html='<tr class="lotes_add" id="cont_lotes_'+cont_lot+'">\
                <td width="30%">\
                    <input type="hidden" id="id" value="'+id+'">\
                    <label>Lote</label>\
                    <input type="text" id="lote" class="form-control" value="'+lote+'">\
                </td>\
                <td width="30%">\
                    <label>Caducidad</label>\
                    <input type="date" id="caducidad" class="form-control" value="'+cad+'">\
                </td>\
                <td width="30%">\
                    <label>Cantidad</label>\
                    <input type="number" min="1" onchange="validaCant()" id="cantidad" class="form-control cantidad cantidad_add" value="'+cant+'">\
                </td>\
                <td width="10%">\
                    <label>Eliminar</label>\
                    <button type="button" onclick="deleteLote('+cont_lot+','+id+')" class="btn btn-danger form-control"><i class="fa fa-trash" aria-hidden="true"></i></button>\
                </td>\
            </tr>';
    $("#body_lotesd").append(html);
    cont_lot++;
}

function deleteLote(cont,id=0){
    if(id==0){
        $("#cont_lotes_"+cont).remove();
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar este registro?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"OCS/delete_detalle_prod",
                        data:{id:id, table: "productos_sucursales_lote" },
                        success:function(data){
                            swal("Éxito!", "Eliminado correctamente", "success");
                            $("#cont_lotes_"+cont).remove();
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

var cont_ser=1;
function addSerie(id,serie){
    var html='<tr  class="series_add" id="cont_series_'+cont_ser+'">\
            <td >\
                <input type="hidden" id="id" value="'+id+'">\
                <label>Serie</label>\
                <input type="text" id="serie" onchange="validar_serie(this.value,$(this))" class="form-control seriepri serie" value="'+serie+'">\
            </td>\
        </tr>';
    $("#body_seriesd").append(html);
    cont_ser++;
}

function saveSeries(){
    var series = $("#table_series_add tbody > tr");
    var DATAsr  = []; var valid=0;
    series.each(function(){
        item = {};
        item['id']=$(this).find("input[id*='id']").val();
        item['idcompra']=$("#id_comp_s").val();
        item['idproducto']=$("#id_prod_com_s").val();
        item['serie']=$(this).find("input[id*='serie']").val();
        DATAsr.push(item);
        if($(this).find("input[id*='serie']").val()==""){
            valid++;
            swal("¡Atención!","Ingrese serie de producto","error");
            return false;
        }
    });
    INFOsr  = new FormData();
    aInfosr   = JSON.stringify(DATAsr);
    if(valid==0){
        var datos='series='+aInfosr;
        $.ajax({
            type:'POST',
            url: base_url+"OCS/ingresar_series",
            data: datos,
            success: function (response){
                swal("Éxito!", "Serie(s) guardado correctamente", "success");
                $("#cont_deta_serie").hide();
                $("#cont_table").show("slow");
            }
        });
    }
}

function validar_serie(val,ele){
    var num_serie=val;
    if(val==""){
        //swal("¡Atención!", "Ingrese una serie", "error"); 
        return;
    }
    $.ajax({
        type:'POST',
        url: base_url+"OCS/get_validar_serie",
        data:{serie:num_serie},
        success:function(data){
            var num = parseFloat(data);
            if(num==0){
                var validar=0;
                var serie = $("#table_series_add tbody > tr");
                serie.each(function(){         
                    var serie_x = $(this).find("input[id*='serie']").val();
                    if(serie_x==num_serie){
                        validar++;
                    }
                });
                //alert(validar);
                if(validar<2){
                    
                }else{
                    ele.val('');
                    swal("¡Atención!", "La serie ya existe", "error");    
                }
            }else{
                ele.val('');
                swal("¡Atención!", "La serie ya existe", "error");
            }
        }
    }); 
         
}

/* **************************************/

var idcompraaux=0;
function validar_modal(id){
    $('#modalcodigo').modal('show');
    setTimeout(function(){ 
        $('#codigo_t1').focus();
    }, 1000); 
    idcompraaux=id;
}

function validar_codigo(){
    var t1 = $('#codigo_t1').val();
    var t2 = $('#codigo_t2').val();
    var t3 = $('#codigo_t3').val();
    var t4 = $('#codigo_t4').val();
    var t5 = $('#codigo_t5').val();
    var n1=0;
    var n2=0; 
    var n3=0;
    var n4=0;
    var n5=0;
    if(t1==''){
        $('#codigo_t1').css('border-color','red');
        n1=0;
    }else{
        $('#codigo_t1').css('border-color','#012d6a');
        n1=1;
    }

    if(t2==''){
        $('#codigo_t2').css('border-color','red');
        n2=0;
    }else{
        $('#codigo_t2').css('border-color','#012d6a');
        n2=1;
    }

    if(t3==''){
        $('#codigo_t3').css('border-color','red');
        n3=0;
    }else{
        $('#codigo_t3').css('border-color','#012d6a');
        n3=1;
    }

    if(t4==''){
        $('#codigo_t4').css('border-color','red');
        n4=0;
    }else{
        $('#codigo_t4').css('border-color','#012d6a');
        n4=1;
    }

    if(t5==''){
        $('#codigo_t5').css('border-color','red');
        n5=0;
    }else{
        $('#codigo_t5').css('border-color','#012d6a');
        n5=1;
    }
    var suma = t1+t2+t3+t4+t5;

    if(n1==1 && n2==1 && n3==1 && n4==1 && n5==1){
        $('.btn_registro_valida').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'OCS/validar_codigo',
            data: {codigo:suma},
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                var validarc = parseFloat(data);
                if(validarc==0){
                    swal("¡Atención!","El código es incorrecto","error");
                    $('.btn_registro_valida').attr('disabled',false);
                }else{
                    $('#modalcodigo').modal('hide');
                    $('#codigo_t1').val('');
                    $('#codigo_t2').val('');
                    $('#codigo_t3').val('');
                    $('#codigo_t4').val('');
                    $('#codigo_t5').val('');
                    swal({
                        title: "¡Éxito!",
                        text: "Código correcto",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    setTimeout(function(){ 
                        eliminar_registro(idcompraaux);
                    }, 2000); 
                    
                }
                
            }
        });
    }
    
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"OCS/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Eliminado Correctamente", "success");
                        setTimeout(function(){ 
                            table.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_compra(id){
    window.open(base_url+'OCS/imprimir_compra/'+id,'_blank'); 
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}

function update_estatus(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de indicar como "OC no recibida"?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Entradas/cambio_estatus",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Actualizado correctamente", "success");
                        reload_registro();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}