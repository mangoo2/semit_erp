var base_url = $('#base_url').val();
$(document).ready(function() {
    $("#tipo_venta").on("click",function(){
        setPermisos();
    });

    $(".classnopv").on("click",function(){
        if($(this).val()=="51"){ //permiso a ajustes
            if($("#id").val()!="1"){
                swal("Álerta!", "No se puede otorgar permiso al perfil actual", "warning");
                $(this).prop("checked",false);
                $(this).attr("disabled",true);
            }
        }
    });
});

function setPermisos(){
    if($("#tipo_venta").is(":checked")==true){
        $(".classpv").attr("checked",true);
        $(".check_ms").attr("disabled",true);
        $(".classnopv").attr("checked",false);
    }else{
        $(".classpv").attr("checked",false);
        $(".check_ms").attr("disabled",false);
    }
}

function guardar_registro_datos(){
    var form_register = $('#form_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_datos").valid();
    if($valid) {
        tipo_venta=0;
        var cont_sub=0;
        if($("#tipo_venta").is(":checked")==true){
            tipo_venta=1;
        }
        tipo_tecnico=0;
        if($("#tipo_tecnico").is(":checked")==true){
            tipo_tecnico=1;
        }
        var datos = form_register.serialize();
        
        //recorre tabla de permisos
        var table_perms = $("#tabla_perms tbody > tr");
        var DATAp  = []; var estatus=0;
        table_perms.each(function(){         
            item = {};
            if($(this).find("input[id*='menu_sub']").is(":checked")==true){
                estatus=1;
            }else{
                estatus=0;
            }
            if($(this).find("input[id*='menu_sub']").is(":checked")==true || $(this).find("input[id*='id']").val()>0){
                item['id']=$(this).find("input[id*='id']").val();
                item['menu_sub']=$(this).find("input[id*='menu_sub']").val();
                item['estatus']=estatus;
                DATAp.push(item);
                cont_sub++;
            }
        });
        array_perms   = JSON.stringify(DATAp);
        //console.log("array_perms: "+array_perms);
        //console.log("cont_sub: "+cont_sub);
        if(cont_sub==0){
            swal("Álerta!", "Elige una opción de acceso", "warning");
            return;
        }
        var datos = $('#form_datos').serialize()+"&tipo_venta="+tipo_venta+"&tipo_tecnico="+tipo_tecnico+'&array_perms='+array_perms;
        $.ajax({
            type:'POST',
            url: base_url+'Perfiles/registro_datos',
            data: datos,
            beforeSend: function(){
                $('.btn_registro').attr('disabled',true);
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Perfiles';
                }, 1500);
            }
        });    
    }   
}