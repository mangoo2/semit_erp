var base_url = $('#base_url').val();
var validar_alias=0;
function guardar_registro_datos(){
    var form_register = $('#form_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            clave:{
              required: true,
            },
            orden:{
              required: true,
            },
            name_suc:{
              required: true,
            },
            /*tel:{
              required: true,
            },*/
            domicilio:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_datos").valid();
    if($valid) {
        if(validar_alias==0){
            con_insumos=0;
            if($("#con_insumos").is(":checked")==true){
                con_insumos=1;
            }
            var datos = form_register.serialize()+"&con_insumos="+con_insumos;
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Sucursales/registro_datos',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        window.location = base_url+'Sucursales';
                    }, 1000);

                }
            });    
        }else{
            $('#id_alias').val('');
            swal("¡Atención!", "El # de Sucursal ya existe", "error");
        }
    }   
}

function validar_num_sucursal(){
    $.ajax({
        type:'POST',
        url: base_url+'Sucursales/get_validar_num_sucursal',
        data: {num:$('#id_alias').val()},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                validar_alias=1;
                //swal("¡Atención!", "El # de Sucursal ya existe", "error");
                //$('#id_alias').css('border','1px solid red');
            }else{
                validar_alias=0;
                //$('#id_alias').css('border','1px solid #E4E6EF');
            }
        }
    });   
}