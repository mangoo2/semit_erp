var table;
var base_url=$('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {	
    tabla();
    $(".btn_aceptar").on("click",function(){
        saveSolicitaDet();
    });

    $(".save_orden").on("click",function(){
        saveOrden();
    });

    select_insumo();

    setTimeout(function(){ 
        if($("#id_last").val()!="0"){
            validaOT($("#id_last").val());
            //get_pdf_orden($("#id_last").val());
        }
    }, 1500); 
});

function validaOT(id){
    $.ajax({
        type:'POST',
        url: base_url+"Mantenimientos/validaOTMtto",
        data:{ id:id },
        success:function(response){
            if(response=="0" && response!="100"){
               get_pdf_orden($("#id_last").val()); 
            }
        }
    });
}

function tabla(){
	table = $('#table_datos').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Mantenimientos/get_data_list",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "reg"},
            {"data": "descrip"},
            {"data": "marca"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "suc_solicita"},
            {"data": null,
                render:function(data,type,row){
                    var html=''; var event="";
                    if(row.estatus==1){
                        html="<span class='btn btn-warning'>En diagnóstico</span>";
                    }if(row.estatus==2){
                        html="<span class='btn btn-success'>En reparación</span>";
                    }if(row.estatus==3){
                        html="<span onclick='detMtto("+row.id+")' class='btn btn-primary'>Reparado</span>";
                    }if(row.estatus==4){
                        html="<span onclick='detMtto("+row.id+")' class='btn btn-danger'>Sin reparación</span>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<a href="'+base_url+'Mantenimientos/solicitud/'+row.id+'" class="btn btn-cli-edit" style="width: 40px;"></a> ';
                    //if(row.estatus==2){
                        html+='<a title="PDF Orden" style="cursor:pointer" onclick="get_pdf_orden('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    //} 
                    //html+='<a title="PDF Solicitud" style="cursor:pointer" onclick="get_pdf('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pdf1.png"></a>';
                    if(row.estatus==1){
                        html+="<button type='button' class='btn btn-primary' onclick='editaStatus("+row.id+",2)'>Reparar<br> <i class='fa fa-cogs' aria-hidden='true'></i></button type='button'> ";
                        html+="<button type='button' class='btn btn-info' onclick='editaStatus("+row.id+",4)'>No repara<br> <i class='fa fa-undo' aria-hidden='true'></i></button type='button'> ";
                    }
                    if(row.estatus==2){
                        html+="<button type='button' class='btn btn-success' onclick='editaStatus("+row.id+",3)'>Reparado<br> <i class='fa fa-check-square' aria-hidden='true'></i></button> ";
                        
                    }
                     
                    html+='<a title="Venta de servicio" style="cursor:pointer" onclick="pay_serv('+row.id+')"><img style="width: 32px;" src="'+base_url+'public/img/pay.svg"></a>';
                    //if(row.id_venta>0){
                        html+='<a title="Asignar Insumos utilizados" style="cursor:pointer" onclick="modalInsumos('+row.id+','+row.id_venta+')"><img style="width: 32px;" src="'+base_url+'public/img/ventasp/41.png"></a>';
                    //}
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        columnDefs: [ { orderable: false, targets: [4,6,7] }],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function detMtto(id){
    $.ajax({
        type:'POST',
        url: base_url+"Mantenimientos/getComentarios",
        data:{ id:id },
        success:function(response){
            var array = $.parseJSON(response);
            $("#modalDetalle").modal("show");
            $("#coment_det").val(array.comentarios);
        }
    });
}

function editaStatus(id,status){ 
    txt="¿Está seguro de ingresar el equipo a reparación?";
    if(status==3){
        txt="¿Está seguro de indicar el equipo como reparado?"+
            '<textarea class="form-control" id="comentarios" placeholder="Comentarios"></textarea>';
    }
    if(status==4){
        txt="¿Está seguro de devolver al cliente el equipo sin reparar?"+
            '<textarea class="form-control" id="comentarios" placeholder="Comentarios"></textarea>';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: txt,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Mantenimientos/update_estatus",
                    data:{ id:id, estatus:status, comentarios:$("#comentarios").val() },
                    success:function(data){
                        $("#modalOrden").modal("hide");
                        swal("Éxito!", "Guardado correctamente", "success");
                        tabla();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function get_pdf(id){
    window.open(base_url+'Mantenimientos/pdf_garantia/'+id,'_blank');  
}

function get_pdf_orden(id){
    $("#modalOrden").modal("show");
    $("#id_mttoord").val(id);
    $("#id_mtto_show").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Mantenimientos/getDetOrdTrabajo",
        data:{ id:id },
        success:function(data){
            var array = $.parseJSON(data);
            $("#id_ordeng").val(array.id);
            $("#fecha_orden").val(array.fecha);
            $("#observ").val(array.observ);
            $("#motivo_solicita").val(array.motivo);

            $('#nom_solicita').val(array.cli);
            $('#dir_solicita').val(array.dir);
            $('#col_solicita').val(array.col);
            $('#cel_solicita').val(array.cel);
            $('#tel_solicita').val(array.tel);
            $('#mail_solicita').val(array.mail);
            $('#rfc_solicita').val(array.rfc);
            $('#nom_contacto').val(array.nom_contacto);
            $('#tel_contacto').val(array.tel_contacto);
        }
    });
}

function saveOrden(){ 
    if($("#fecha_orden").val()==""){
       swal("Álerta!", "Indica una fecha para la orden de trabajo", "warning"); 
       return;
    }
    $.ajax({
        type:'POST',
        url: base_url+"Mantenimientos/save_orden",
        data:{ id:$("#id_ordeng").val(), id_mtto:$("#id_mttoord").val(), fecha:$("#fecha_orden").val(), observ:$("#observ").val(), 
            nom_solicita:$("#nom_solicita").val(), dir_solicita:$("#dir_solicita").val(), col_solicita:$("#col_solicita").val(), cel_solicita:$("#cel_solicita").val(),
            tel_solicita:$("#tel_solicita").val(), mail_solicita:$("#mail_solicita").val(), rfc_solicita:$("#rfc_solicita").val(), nom_contacto:$("#nom_contacto").val(), tel_contacto:$("#tel_contacto").val()
        },
        success:function(data){
            $("#modalOrden").modal("hide");
            swal("Éxito!", "Guardado correctamente", "success");
            setTimeout(function(){ 
                window.open(base_url+'Mantenimientos/ordenTrabajo/'+$("#id_mttoord").val(),'_blank'); 
            }, 1500); 
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}

function pay_serv(id){
    $("#modal_pays").modal("show");
    $("#id_contrap").html(id);
    $("#idrc").val(id);
    $.ajax({
        type:'POST',
        url: base_url+"Mantenimientos/getDetallePago",
        data:{id:id},
        success:function(data){
            var array=$.parseJSON(data);
            $("#cliname").html(array.nombre);
            $("#marca").html(array.marca);
            $("#modelo").html(array.modelo);
            $("#serie").html(array.serie);
            $("#a_cuenta").html(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.a_cuenta));
            if(array.id_venta!=0){
                $("#status_serv").html('<h3 style="color: #1770aa; text-align: center;" id="status_serv">Servicio con registro de pago, ticket de venta # <a target="_blank" href="'+base_url+'Ventasp/ticket/'+array.id_venta+'">'+array.id_venta+'</a></h3>');
            }else{
                $("#status_serv").html('<h3 style="color: #1770aa; text-align: center;" id="status_serv">Sin pago de servicio relacionado, generar pago en pantalla <a target="_blank" href="'+base_url+'Ventasp?idsrv='+id+'">ventas</a></h3>');
                //$("#status_serv").html('<h3 style="color: #1770aa; text-align: center;" id="status_serv">Sin pago de servicio relacionado, generar pago en pantalla ventas</h3>');
            }
        }
    });
}

function select_insumo(){
    $('#sel_insumo').select2({
        width: '99%',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        dropdownParent: $('#modalInsumos'),
        placeholder: 'Buscar un insumo',
        allowClear: true,
        ajax: {
            url: base_url+'Mantenimientos/searchInsumo',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var cont=0;
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.idProducto+' / '+element.nombre + " (stock: "+element.stock_suc+")",
                        tipo: "3_1",
                        id_ps: element.id_ps,
                        stock_suc: element.stock_suc
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#sel_insumo option:selected").attr('data-tipo',data.tipo);
        $("#sel_insumo option:selected").attr('data-id_ps',data.id_ps);
        $("#sel_insumo option:selected").attr('data-stock_suc',data.stock_suc);

        if(data.stock_suc>0){
            get_insumo();
        }else{
            swal("Atención!", "Insumo sin stock suficiente", "warning");
            return false;
        }
    });
}

function getInsumosMtto(id,id_venta){
    $.ajax({
        type: 'POST',
        url : base_url+'Mantenimientos/getInsumosMtto',
        data: { id:id, id_venta:id_venta },
        success: function(response){
            var array = $.parseJSON(response);
            $('.det_ins_serv').html(array.html);
            $("#serv_name").html(array.numero+" / "+array.descripcion);
        }
    }); 
}

function modalInsumos(id,id_venta){
    $("#modalInsumos").modal("show");
    if(id_venta!=0){
        $("#cont_search").hide();
        $("#cont_save").hide();
    }else{
        $("#cont_search").show();
        $("#cont_save").show();
        setTimeout(function(){ 
            $('#sel_insumo').select2('open');
            $('.select2-search__field').focus();
        }, 500);

    }
    getInsumosMtto(id,id_venta);
    $("#id_mttoadd").val(id);
    $("#id_vtaadd").val(id_venta);
}

function get_insumo(){
    var id =$("#sel_insumo option:selected").val();
    var text_ins =$("#sel_insumo option:selected").text();
    var tipo =$("#sel_insumo option:selected").data("tipo");
    var id_ps =$("#sel_insumo option:selected").data("id_ps");
    var stock =$("#sel_insumo option:selected").data("stock_suc");
    var id_mtto = $("#id_mttoadd").val();
    var html="";
    var repetido=0;
    var TABLA   = $("#table_insumos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='id_ins']").val(); 
        if (id == prodexi) {
            repetido=1;
            $("#cant_ins").val(Number($("#cant_ins").val())+1);
        }   
    });
    if(repetido==0){
        html="<tr class='row_ins row_ins_"+id+" row_ins_serv_"+$("#id_mttoadd").val()+"'>\
                <td><input type='hidden' id='id_ins' value='"+id+"'><input type='hidden' id='id_ps' value='"+id_ps+"'>\
                <input type='hidden' id='tipo' value='"+tipo+"'><input type='hidden' id='id_mtto' value='"+id_mtto+"'>\
                <input type='hidden' id='stock' value='"+stock+"'><input type='hidden' id='id_venta' value='"+$("#id_vtaadd").val()+"'>\
                <input class='form-control' id='cant_ins' type='number' value='1'>\
            </td>\
            <td>"+text_ins+"</td>\
            <td><button class='btn btn-danger' onclick='deleteIns("+id+",0)'> <i class='fa fa-trash' aria-hidden='true'></i></button></td>\
        </tr>";
        $(".det_ins_serv").append(html);
        $('#sel_insumo').val(null).trigger('change');
    }
}

function addInsumos_save(){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¿Desea asignar los insumos al mantenimiento?',
        content: 'Se descontará existencia y no habrá forma de revertir!',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if($("#table_insumos tbody > tr").length>0){
                var insumos = $("#table_insumos tbody > tr");
                var DATAi  = [];
                insumos.each(function(){         
                    item = {};
                    item ["id_venta"] = $(this).find("input[id*='id_venta']").val();
                    item ["id_ins"] = $(this).find("input[id*='id_ins']").val();
                    item ["id_ps"] = $(this).find("input[id*='id_ps']").val();
                    item ["cant_ins"] = $(this).find("input[id*='cant_ins']").val();
                    item ["tipo"] = $(this).find("input[id*='tipo']").val();
                    item ["id_mtto"] = $(this).find("input[id*='id_mtto']").val();
                    item ["stock"] = $(this).find("input[id*='stock']").val();
                    DATAi.push(item);
                });
                INFOi  = new FormData();
                aInfoi = JSON.stringify(DATAi);
                //console.log("aInfoi: "+aInfoi);
                var datos = 'array_ins='+aInfoi;
                $.ajax({
                    data: datos,
                    type: 'POST',
                    url : base_url+'Mantenimientos/saveDetaInsumos',
                    statusCode:{
                        404: function(data){
                        },
                        500: function(){
                        }
                    },
                    beforeSend: function(){
                        $('.add_insumosadd_insumos').attr('disabled',true);
                    },
                    success: function(response){
                        $('.add_insumosadd_insumos').attr('disabled',false);
                        $(".row_ins").remove();
                        $("#modalInsumos").modal("hide");
                        swal("Éxito!", "Guardado correctamente", "success");
                        tabla();
                    }
                }); 
            }else{
                swal("¡Atención!","Ingresa al menos un producto", "error");
            }
            },
            cancelar: function (){
                
            }
        }
    });
}

function deleteIns(id){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del insumo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $(".row_ins_"+id).remove();
            },
            cancelar: function (){
                
            }
        }
    });
}
