var base_url = $('#base_url').val();
var idreg = $('#idreg1').val();
var idcategoria_aux = $('#idcategoria_aux').val();

var id_categoria=0;
var tabla_categoria;

var cont_s=0;
var cont_l=0;

var numbersewg = []; 
$(document).ready(function() {
    $('#resumen').ckeditor();
    $('#descripcion').ckeditor();
    $('#observaciones').ckeditor();

    $("#files").fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Productos/baner_imagenes_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        fileActionSettings: {
            removeIcon: '<i class="fa fa-trash"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idproducto:$('#idreg1').val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view($('#idreg1').val());
        $('#files').fileinput('refresh');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view($('#idreg1').val());
    });

    cateforia_get(idcategoria_aux);
    if(idreg!=0){
        upload_data_view($('#idreg1').val()); 
        get_tabla_caracteristcas($('#idreg1').val());
        get_tabla_proveedor($('#idreg1').val());
    }else{
        add_caracteristicas();
    }
    $('#unidad_sat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar unidad sat',
          ajax: {
            url: base_url+'Productos/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    
    $('#servicioId_sat').select2({
        //width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar concepto sat',
          ajax: {
            url: base_url+'Productos/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });

    $('#idproveedor').select2({
        //width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar proveedor',
          ajax: {
            url: base_url+'Productos/searchproveedor',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    var sum=$('#cont_s_n').val();
    if(sum!=0){
        var suma=parseFloat(sum)+1;
        cont_s=suma;
    }else{
        cont_s=0;
    }

    var suml=$('#cont_s_l').val();
    if(suml!=0){
        var sumal=parseFloat(suml)+1;
        cont_l=sumal;
    }else{
        cont_l=0;
    }

    mismo_precio(1);
    setTimeout(function(){ 
        mismo_precio(2);
    }, 1500);

    $("#imprimiretiqueta").on("click",function(){
        window.frames["iframeCodeBar"].focus();
        window.frames["iframeCodeBar"].print();
        var newWin = window.frames["iframeCodeBar"];
        newWin.document.close(); 
    });

    $("#id_categoria").on("change",function(){
        getCodigoProd();
    });

    $("#isr").on("click",function(){
        showPorcentajes(1);
    });
    $("#ieps").on("click",function(){
        showPorcentajes(2);
    });

    if($("#idreg").val()!=0){
        setTimeout(function(){ 
            showPorcentajes(1);
            showPorcentajes(2);
        }, 1500);
        setTimeout(function(){ 
            ocultar_porc($("#iva_comp option:selected").val());
        }, 2500);
    }

    $("#concentrador").on("click", function(){
        if($("#concentrador").is(":checked")==true){
            $("#tanque").prop("checked",false);
            $(".cont_capa").hide("slow");
        }
    });

    $("#tanque").on("click", function(){
        showSubtipo();
    });

    showSubtipo();

    if($("#idreg1").val()!=0){
        validar_tipo();
    }else{
        $("#tipo").val("");
    }
}); 

function checkTab(number){
    $(".tab"+number).removeClass("active");
}

function showSubtipo(){
    if($("#tanque").is(":checked")==true){
        $("#concentrador").prop("checked",false);
    }
    if($("#tanque").is(":checked")==true){
        $(".cont_capa").show("slow");
    }else{
        $(".cont_capa").hide("slow");
    }
}

function showPorcentajes(tipo){
    //console.log("showPorcentajes: "+tipo);
    if(tipo==1 && $("#isr").is(":checked")==true){
        $(".oculto_isr").show("slow");
    }else if(tipo==1 && $("#isr").is(":checked")==false){
        $(".oculto_isr").hide("slow");
    }
    if(tipo==2 && $("#ieps").is(":checked")==true){
        $(".oculto_ieps").show("slow");
    }else if(tipo==2 && $("#ieps").is(":checked")==false){
        $(".oculto_ieps").hide("slow");
    }
}

function getCodigoProd(){
    var id_cat=$("#id_categoria option:selected").val();
    var clave=$("#id_categoria option:selected").data("clave");

    $.ajax({
        type:'POST',
        url: base_url+'Productos/getLastCatProd',
        data: { id_cat:id_cat, clave:clave },
        success:function(response){
            //console.log("cod: "+response);
            $("#idProducto").val(response);
        }
    });
}

function createCDL(idsuc,idprod){
    var lote=$(".lote_"+idsuc).val();
    var cad=$(".caducidad_"+idsuc).val();
    $.ajax({
        type:'POST',
        url: base_url+'Productos/crearCodigoLote',
        data: {idsuc:idsuc,idprod:idprod,lote:lote,cad:cad},
        success:function(response){
           $(".cod_barras_"+idsuc).val(response); 
        }
    });
}

function printCode(idps) {
    $('#modalcodebar').modal('show');
    $('#iframeCode').html('<iframe id="iframeCodeBar" name="iframeCodeBar" src="'+base_url+'Productos/printCodeBarLote?id='+idps+'&print=true"></iframe>');
}    

function upload_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Productos/viewimages',
        data: {id:id},
        success:function(data){
            $('.lis_catacter').html(data);
            //$('.materialboxed').materialbox();
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        }
    });
}

function guardar_ajustes(){
    var form_register = $('#form_ajustes');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            idProducto: {
                required: true
            },
            tipo: {
                required:true
            },
            categoria: {
                required: true
            },
            nombre: {
                required: true
            },
            unidad_sat: {
                required: true
            },
            servicioId_sat: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register,-100);
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_ajustes").valid();
    if($valid) {
        if($("#table_datos_proveedor tbody > tr").length==0){
            swal("¡Atención!", "Ingresar al menos un proveedor y su código", "error");
            $("#cont_cod_provee").addClass("cont_cod_provee");
            return false;
        }
        var concen=$('#concentrador').is(':checked')==true?1:0;
        var tanque=$('#tanque').is(':checked')==true?1:0;
        var capacidad = 0;
        if($('#tanque').is(':checked')==true){
            capacidad = $("#capacidad").val();
        }
        var mp=$('#mostrar_pagina').is(':checked')==true?1:0;
        var datos = form_register.serialize();//+'&id='+$('#idreg1').val();
        //alert(validar_barras);
        if(validar_barras==0){
            $('.btn_ajustes').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Productos/registrar_datos_ab',
                data: datos+"&mostrar_pagina="+mp+"&concentrador="+concen+"&tanque="+tanque+"&capacidad="+capacidad,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                beforeSend: function(){
                    $('.btn_ajustes').attr('disabled',true);
                },
                success:function(data){
                    var array = $.parseJSON(data);
                    if(array.validar==0){
                        var idp=parseFloat(array.id);
                        //$('#idreg').val(idp);
                        $('#idreg1').val(idp);
                        $('#idreg2').val(idp);
                        $('#idreg3').val(idp);
                        $('#idreg4').val(idp);
                        $('#idreg5').val(idp);
                        $('#idreg6').val(idp);
                        $('#idreg7').val(idp);
                        swal("Éxito!", "Guardado Correctamente", "success");
                        $("#cont_cod_provee").removeClass("cont_cod_provee");
                        setTimeout(function(){ 
                            $('.btn_ajustes').attr('disabled',false);
                            window.location = base_url+'Productos';
                        }, 1000);
                        add_cantidad_sucu();
                        //alert('1 ddd');
                        guardar_proveedores(idp);
                    }else{
                        $('.btn_ajustes').attr('disabled',false);
                        swal("¡Atención!", "El nombre de producto ya existe", "error");
                        //alert('2 ddd');
                    }
                    //alert();
                }
            }); 

        }else{
            swal("¡Atención!", "El Código/ID de articulo ya existe", "error");
        }
    }
}

/////////////////////
function modal_categoria(){
    $('#modalcategoria').modal('show');
    reload_categoria();
    $('#idcategoria_categoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function add_categoria(){
    var form_register = $('#form_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            categoria: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_categoria').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/addcategoria',
            data: datos,
            statusCode:{
                404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_categoria').attr('disabled',false);
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                    $('.vd_red').remove();
                }, 2000);
                cateforia_get(parseFloat(data));
                reload_categoria();
            }
        });
    }
}

function reload_categoria(){
    tabla_categoria=$("#table_categoria").DataTable();
    tabla_categoria.destroy();
    table_categorias();
}
function table_categorias(){
    tabla_categoria=$("#table_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"categoriaId"},
            {"data":"categoria"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.categoriaId+'"\
                            data-categoria="'+row.categoria+'"\
                            class="btn btn-primary catec_'+row.categoriaId+'" onclick="editar_categoria('+row.categoriaId+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_categoria('+row.categoriaId+');"><i class="fa fa-trash icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_categoria(id){
    id_categoria=1;
    $('#idcategoria_categoria').val($('.catec_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.catec_'+id).data('categoria'));
}

function delete_registro_categoria(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta categoria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_categoria",
                    data: {
                        categoriaId:id
                    },
                    statusCode: {
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(response){  
                        swal("Éxito!", "Eliminado correctamente", "success");
                        reload_categoria();
                        $('#categoriaId option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function cateforia_get(id){ //si es edicion mandar el id de categoria para selected
    $.ajax({
        type:'POST',
        url: base_url+"Productos/select_datos_cat_get",
        data: { id:id},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.categoria_txt').html(data);
        }
    });  
}

function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/deleteimgbaner",
                    data: {id:id},
                    success: function (response){
                        swal("Éxito!", "Eliminado correctamente", "success");
                        upload_data_view(idreg);
                    },
                    error: function(response){
                        swal("Error!", "500", "error");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}

function add_cantidad_sucu(){
    //console.log("add_cantidad_sucu");
    var tcantidades = $("#table_cantidades tbody > .tr_c");
    //==============================================
    var DATAc  = [];
    tcantidades.each(function(){         
        item = {};
        item['idrow']=$(this).find("input[id*='id_suc_pro']").val();
        item['suc']=$(this).find("input[id*='sucursalid_suc_pro']").val();
        item['stock']=$(this).find("input[id*='stock']").val();
        item['stockmi']=$(this).find("input[id*='stockmin']").val();
        item['stockma']=$(this).find("input[id*='stockmax']").val();
        //item['ubi']=$(this).find("input[id*='ubicacion']").val();
        DATAc.push(item);
    });
    INFOa  = new FormData();
    aInfoc   = JSON.stringify(DATAc);
    //========================================
    var datos = 'idpro='+$('#idreg1').val()+'&cantidades='+aInfoc;
    $.ajax({
        type:'POST',
        url: base_url+'Productos/registrar_datos_ca',
        data: datos,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    }); 
}

function guardar_cantidades(){
    var idreg2=$('#idreg2').val();
    if(idreg2!=0){
        var tcantidades = $("#table_cantidades tbody > .tr_c");
        var validar_stock=0;
        tcantidades.each(function(){         
            var cant=$(this).find("input[id*='stock']").val();
            if(cant=='' && cant==0){
                validar_stock=1;
            }
        });
        /*if(validar_stock==1){
             swal("¡Atención!", "El campo stock esta vacío", "error");
        }else{*/
        //==============================================
            var DATAc  = [];
            tcantidades.each(function(){         
                item = {};
                item['idrow']=$(this).find("input[id*='id_suc_pro']").val();
                item['suc']=$(this).find("input[id*='sucursalid_suc_pro']").val();
                item['stock']=$(this).find("input[id*='stock']").val();
                item['stockmi']=$(this).find("input[id*='stockmin']").val();
                item['stockma']=$(this).find("input[id*='stockmax']").val();
                //item['ubi']=$(this).find("input[id*='ubicacion']").val();
                var DATA_d  = [];
                var TABLA_d = $(this).find('.tabla_cantidades_sucursal tbody > .tr_s');
                TABLA_d.each(function(){ 
                    item_d = {};
                    item_d ["id"] = $(this).find("input[id*='idx']").val();
                    item_d ["sucursal"] = $(this).find("input[id*='sucursalx']").val();
                    item_d ["serie"] = $(this).find("input[id*='seriex']").val();
                    item_d ["editar_x"] = $(this).find("input[id*='editar_x']").val();
                    item_d ["seriey"] = $(this).find("input[id*='seriey']").val();
                    DATA_d.push(item_d);
                });
                item["serie_nume"]   = DATA_d;

                var DATA_l  = [];
                var TABLA_l = $(this).find('.tabla_cantidades_sucursal_lote tbody > .tr_l');
                TABLA_l.each(function(){ 
                    item_l = {};
                    item_l ["id"] = $(this).find("input[id*='idl']").val();
                    item_l ["sucursal"] = $(this).find("input[id*='sucursall']").val();
                    item_l ["cantidad"] = $(this).find("input[id*='cantidadl']").val();
                    item_l ["lote"] = $(this).find("input[id*='lotel']").val();
                    item_l ["caducidad"] = $(this).find("input[id*='caducidadl']").val();
                    item_l ["cod_barras"] = $(this).find("input[id*='cod_barrasl']").val();
                    DATA_l.push(item_l);
                });
                item["lote_nume"]   = DATA_l;

                DATAc.push(item);
            });
            INFOa  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
            //console.log("aInfoc: "+aInfoc);
            //========================================

            var datos = 'idpro='+$('#idreg1').val()+'&cantidades='+aInfoc;
            $('.btn_ajustes').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Productos/registrar_datos_ca2',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_ajustes').attr('disabled',false);
                        window.location = base_url+'Productos/registro/'+idreg2;
                        //location.reload();
                    }, 1000);
                }
            }); 
        }
    //}else{
    //    swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    //}
}

function guardar_logistica(){
    var idreg3=$('#idreg3').val();
    if(idreg3!=0){
        var form_register = $('#form_logistica');
        var datos = form_register.serialize()+'&id='+$('#idreg3').val();
        $('.btn_ajustes').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registrar_datos_lg',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var id=parseFloat(data);
                //$('#idreg1').val(id);
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_ajustes').attr('disabled',false);
                    //window.location = base_url+'Productos';
                }, 1000);
            }
        }); 
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    }
}

function guardar_precios(){
    var idreg4=$('#idreg4').val();
    if(idreg4!=0){
        var precio=$('.precio_2').val();
        if(precio!=''){
            var tprecios = $("#table_precios tbody > tr");
            var DATAp  = [];
            tprecios.each(function(){         
                item = {};
                item['idrow']=$(this).find("input[id*='id_suc_pro']").val();
                item['suc']=$(this).find("input[id*='sucursalid_suc_pro']").val();

                item['precio']=$(this).find("input[id*='precioxx']").val();
                //item['iiva']=$(this).find("input[id*='incluye_iva']").is(':checked')==true?1:0;
                item['iiva']=$(this).find("input:radio[class*='incluye_iva']:checked").val();
                item['iva']=$(this).find("select[id*='iva'] option:selected").val();
                item['desc']=$(this).find("input[id*='descuento']").val();
                item['ti_des']=$(this).find("select[id*='tipo_descuento'] option:selected").val();
                item['precioff']=$(this).find("input[id*='precio_ff']").val();
                DATAp.push(item);
            });
            INFOa  = new FormData();
            aInfop   = JSON.stringify(DATAp);
            //console.log("aInfop:" +aInfop);
            //========================================
            var datos = 'idpro='+$('#idreg1').val()+'&precios='+aInfop;
            $('.btn_ajustes').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Productos/registrar_datos_pre',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    var id=parseFloat(data);
                    //$('#idreg1').val(id);
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_ajustes').attr('disabled',false);
                        //window.location = base_url+'Productos';
                    }, 1000);
                }
            }); 
        }else{
            swal("¡Atención!", "El precio esta vacío", "error");
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    }
}


function add_caracteristicas(){
    tabla_caracteristicas(0,'','');
}

var cantc=0;
function tabla_caracteristicas(id,titulo,descripcion){
    var btn='';
    if(cantc==0){
        btn='<button class="btn btn-pill btn-primary" type="button" onclick="add_caracteristicas()"><i class="fa fa-plus"></i></button>';
    }else{
        btn='<button class="btn btn-pill btn-danger" type="button" onclick="delete_registro('+id+','+cantc+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<div class="row g-3 div_'+cantc+'">\
        <div class="col-md-4">\
          <label class="form-label">Título</label>\
          <input type="hidden" id="id_x" value="'+id+'">\
          <input class="form-control" type="text" id="titulo_x" value="'+titulo+'">\
        </div>\
        <div class="col-md-7">\
          <label class="form-label">Descripción</label>\
          <input class="form-control" type="text" id="descripcion_x" value="'+descripcion+'">\
        </div>\
        <div class="col-md-1">\
          <label class="form-label" style="color: white">__</label><br>\
          '+btn+'\
        </div>  \
      </div>';
    $('.caracteristicas_2').append(html);  
    cantc++;  
}

function delete_registro(id,cont){
    if(id==0){
        $('.div_'+cont).remove();
    }else{
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar caracteristica?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_caracteristica",
                    data: {id:id},
                    success: function (response){
                        swal("Éxito!", "Eliminado correctamente", "success");
                        $('.div_'+cont).remove();
                    },
                    error: function(response){
                        swal("Error!", "500", "error");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
    }
}

function get_tabla_caracteristcas(id){
    $.ajax({
        type:'POST',
        url: base_url+"Productos/get_tabla_caracteristcas",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    tabla_caracteristicas(element.id,element.titulo,element.descripcion);
                });
            }else{
                add_caracteristicas();
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

var contcar=0;
function guardar_caracteristicas(id){
    var DATA  = [];
    var TABLA   = $(".caracteristicas_1 .caracteristicas_2 > div");
    TABLA.each(function(){ 
        contcar=1;
        item = {};
        item ["idproducto"] = id;
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["titulo"] = $(this).find("input[id*='titulo_x']").val();
        item ["descripcion"] = $(this).find("input[id*='descripcion_x']").val();
        DATA.push(item);
    });     
    if(contcar==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'index.php/Productos/registro_caracteristicas',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}
function mismo_precio(row){
    //console.log("row :"+row);
    var mipre=$('#mismo_precio').is(':checked');
    if(mipre){
        var precio=$('.precio_'+row).val();
        var pre_2=$('.precio_2').val();
        var pref_2=$('.preciof_2').val();
        //console.log(pre_2);
        var incluye_iva_2 = $('input[id="incluye_iva1"]:checked').val();
        //console.log("incluye_iva_2: "+incluye_iva_2);
        $("input[class=incluye_iva][value='"+incluye_iva_2+"']").prop("checked",true);
        if(incluye_iva_2==1){
            $('.oculto').css('display','block'); 
            var iva = $('.iva_2 option:selected').val(); 
            $(".iva_suc option[value='"+iva+"']").attr("selected", true);
            var iva_suc=$('.iva_'+row+' option:selected').val();
            //console.log("iva_suc: "+iva_suc);
            if(Number(iva_suc)==16){
                var mult=parseFloat(precio)*0.16;
                var suma=parseFloat(precio)+parseFloat(mult);
                //console.log("mult: "+mult);
                //console.log("suma: "+suma);
                //console.log("row en iva 16: "+row);
                $('.preciof_'+row).val(suma);
            }else{ 
                //console.log("row en else iva 16: "+row);
                $('.preciof_'+row).val(precio);
            }
        }
        //$("input[type=checkbox]").prop("checked",true);
        $('.inputprecios').val(pre_2);
        $('.precio_f').val(pref_2);
        $('.precio_2').change(function(event) {
            var pre_1=$('.precio_2').val();
            $('.inputprecios').val(pre_1);
        });
    }
    //incluye_iva(row);
}
function incluye_iva(row){
    //var iiv=$("input:radio[name='incluye_iva_"+row+"']:checked").val();
    var precio=$('.precio_'+row).val();
    var preciof=$('.preciof_'+row).val();
    var check = $('input[name="incluye_iva_'+row+'"]:checked').val();
    //alert(check);
    setTimeout(function(){ 
        mismo_precio();
    }, 500);
    setTimeout(function(){ 
        if(check==1){
            var iva_suc=$('.iva_'+row+' option:selected').val();
            if(iva_suc==16){
                var mult=parseFloat(precio)*0.16;
                var suma=parseFloat(precio)+parseFloat(mult);
                $('.preciof_'+row).val(suma);
            }else{ 
                //$('.preciof_'+row).val(precio);
                $('.precio_'+row).val(preciof);
            }
            $('.oculto').show();
            $('input[class=incluye_iva][value=1]').prop('checked', true);
        }else{
            $('.oculto').hide();
            $('.iva_suc').val(0);
            //$('.precio_f').val(precio);
            $('.precio_'+row).val(preciof);
            //$('.iva_suc option:selected').remove();
            $('input[class=incluye_iva][value=0]').prop('checked', true);
            $('.incluye_iva').prop('checked', true);
        }
    }, 1000);  
}

function ocultar_por(row){
    //$('.iva_suc option').prop('selected',false);
    var precio=$('.precio_'+row).val();
    var iva_suc=$('.iva_'+row+' option:selected').val();
    if(iva_suc==16){
        var mult=parseFloat(precio)*0.16;
        var suma=parseFloat(precio)+parseFloat(mult);
        $('.preciof_'+row).val(suma);
        //$('.iva_suc > option[value="16"]').attr('selected', 'selected');
    }else{ 
        $('.preciof_'+row).val(precio);
        //$('.iva_suc > option[value="0"]').attr('selected', 'selected');
    }
    mismo_precio(row);
}

function ocultar_por2(row){
    //$('.iva_suc option').prop('selected',false);
    var precio=$('.preciof_'+row).val();
    var iva_suc=$('.iva_'+row+' option:selected').val();
    if(iva_suc==16){
        var precio_ini=parseFloat(precio)/(1+0.16);
        precio_ini =redondear(precio_ini, 5)
        $('.precio_'+row).val(precio_ini);
        //$('.iva_suc > option[value="16"]').attr('selected', 'selected');
    }else{ 
        $('.precio_'+row).val(precio);
        //$('.iva_suc > option[value="0"]').attr('selected', 'selected');
    }
    mismo_precio(row);
}
function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}
function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}
function ocultar_porc(band){
    //$('.iva_suc option').prop('selected',false);
    if($("#incluye_iva2").is(":checked")==true) band=1; else band=0;
    var sub_isr=0; var suma=0; var mult=0;
    if(band==0){ //no grava iva
        $("#iva_comp").val(0);
        $("#precioc_final").val($("#costo_compra").val());
        $(".oculto_comp").hide();
    }else{
        $(".oculto_comp").show();
        var precio=$("#costo_compra").val();
        var iva_suc=$('#iva_comp option:selected').val();
        if(iva_suc==16){
            mult=parseFloat(precio)*0.16;
            suma=parseFloat(precio)+parseFloat(mult);
            $('#precioc_final').val(suma);
        }else{ 
            $('#precioc_final').val(precio);
            suma = precio;
        }
    }
    if($("#porc_isr").is(":visible")==true && $("#porc_isr").val()!="0"){
        var porc_isr = Number($("#porc_isr").val());
        sub_isr= Number(suma-mult)*(Number(porc_isr)/100);
        costo_fin = Number(suma) - Number(sub_isr);
        //costo_fin =parseFloat(costo_fin).toFixed(5);
        //console.log("porc_isr: "+porc_isr);
        //console.log("sub_isr: "+sub_isr);
        //console.log("costo_fin: "+costo_fin);
        $('#precioc_final').val(costo_fin);
    }
}

function guardar_web(){
    var idreg5=$('#idreg5').val();
    if(idreg5!=0){
        $('.btn_web').attr('disabled',true);
        setTimeout(function(){ 
            $('.btn_web').attr('disabled',false);
        }, 1000);
        guardar_caracteristicas(idreg5);
        //var mp=$('#mostrar_pagina').is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registrar_datos_web',
            data: {
                'idreg':idreg5,
                'resumen':$('#resumen').val(),
                //'mostrar_pagina':mp
            },
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    //$('.btn_web').attr('disabled',false);
                    //window.location = base_url+'Productos';
                }, 1000);
            }
        });
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    }
    
}
function guardar_sat(){
    var idreg6=$('#idreg6').val();
    if(idreg6!=0){
        var form_register = $('#form_registro_sat');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                unidad_sat: {
                    required: true
                },
                servicioId_sat: {
                    required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_registro_sat").valid();
        if($valid) {
            $('.btn_sat').attr('disabled',true);
            setTimeout(function(){ 
                $('.btn_sat').attr('disabled',false);
            }, 1000);
            guardar_caracteristicas(idreg6);
            var mp=$('#mostrar_pagina').is(':checked')==true?1:0;
            $.ajax({
                type:'POST',
                url: base_url+'Productos/registrar_datos_sat',
                data: {
                    'idreg':idreg6,
                    'unidad_sat':$('#unidad_sat option:selected').val(),
                    'servicioId_sat':$('#servicioId_sat option:selected').val()
                },
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        //$('.btn_sat').attr('disabled',false);
                        //window.location = base_url+'Productos';
                    }, 1000);
                }
            });
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    }
}


function guardar_compra(){
    var idreg7=$('#idreg7').val();
    if(idreg7!=0){ 
        var form_register = $('#form_registro_compra');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                costo_compra: {
                    required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_registro_compra").valid();
        if($valid) {
            $('.btn_compra').attr('disabled',true);
            setTimeout(function(){ 
                $('.btn_compra').attr('disabled',false);
            }, 1000);
            if($("#incluye_iva2").is(":checked")==true) incluye_iva_comp=1; else incluye_iva_comp=0;
            var isr=$('#isr').is(':checked')==true?1:0;
            var ieps=$('#ieps').is(':checked')==true?1:0;
            $.ajax({
                type:'POST',
                url: base_url+'Productos/registrar_datos_compra',
                data: {
                    'idreg':idreg7,
                    'costo_compra':$('#costo_compra').val(),
                    'iva_comp':$("#iva_comp option:selected").val(),
                    'incluye_iva_comp':incluye_iva_comp,
                    'isr':isr,
                    'ieps':ieps,
                    'porc_isr':$('#porc_isr').val(),
                    'porc_ieps':$('#porc_ieps').val()
                },
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                    }, 1000);
                }
            });
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar ajustes básicos", "error");
    }
}


function mostrar_sucu(){
    if($('#mostrar_sucu').is(':checked')){
        $('.sucusar_text').removeClass('sucusar_text2');
    }else{
        $('.sucusar_text').addClass('sucusar_text2');
    }
}

var validar_barras=0;
function verificar_codigo(){
    $.ajax({
        type: 'POST',
        url: base_url+'Productos/validar_codigobarras',
        data: {
            codigo:$('#idProducto').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            var num = parseFloat(data);
            if (num == 1) {
                validar_barras=1;
                swal("¡Atención!", "El Código/ID de articulo ya existe", "error");
            }else{
                validar_barras=0;
            }
            //alert(validar_barras);
        }
    });
}

function validar_tipo(){
    var tipo=$('#tipo option:selected').val();
    if(tipo==1){
        $('.txt_cantidades_stock').css('display','none');
        $('.txt_cantidades_serie').css('display','block');
        $('.txt_cantidades_lote').css('display','none');
    }else if(tipo==2){    
        $('.txt_cantidades_stock').css('display','none');
        $('.txt_cantidades_serie').css('display','none');
        $('.txt_cantidades_lote').css('display','block');
    }else{
        $('.txt_cantidades_stock').css('display','block');
        $('.txt_cantidades_serie').css('display','none');
        $('.txt_cantidades_lote').css('display','none');
    }

    if(tipo==0 || tipo==1 || tipo==2 || tipo==4){ // stock, serie, lote, refacciones
        $("#ref_1").attr("checked",true); //inventario
        $("#ref_2").attr("checked",true); //venta
        $("#ref_3").attr("checked",true); //compra
        $("#ref_4").attr("checked",false); //servicio
    }else if(tipo==3){ // insumo
        $("#ref_4").attr("checked",true); //servicio
        $("#ref_2").attr("checked",true); //venta
        $("#ref_3").attr("checked",true); //compra
        $("#ref_1").attr("checked",false); //inventario
    }
}

function add_serie(sucursal){
    var num_serie=$('.num_serie_'+sucursal).val();
    $.ajax({
        type:'POST',
        url: base_url+"Productos/get_validar_serie",
        data:{serie:num_serie},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var num = parseFloat(data);
            if(num==0){
                var validar=0;
                var tcantidades = $("#table_cantidades tbody > .tr_c");
                tcantidades.each(function(){         
                    var TABLA_d = $(this).find('.tabla_cantidades_sucursal tbody > .tr_s');
                    TABLA_d.each(function(){ 
                        var serie_x = $(this).find("input[id*='seriex']").val();
                        if(serie_x==num_serie){
                            validar=1;
                        }
                    });
                });
                if(validar==0){
                    if(num_serie!='' && sucursal!=0){
                        get_add_tabla(0,sucursal,num_serie);
                        $('.num_serie_'+sucursal).val('');
                    }else{
                        swal("¡Atención!", "Falta agregar número de serie", "error");
                    }
                }else{
                    swal("¡Atención!", "La serie ya existe", "error");    
                }
            }else{
                swal("¡Atención!", "La serie ya existe", "error");
            }
        }
    }); 
            /*var num_serie=$('.num_serie_'+sucursal).val();
            */
}

function get_add_tabla(id,sucursal,serie){
    var html='<tr class="tr_s row_s_'+sucursal+'_'+cont_s+'">\
            <td><input type="hidden" id="idx" value="'+id+'">\
                <input type="hidden" id="sucursalx" value="'+sucursal+'">\
                <input type="hidden" id="seriex" value="'+serie+'">\
                <input type="hidden" id="editar_x" value="'+serie+'">\
                <input type="hidden" id="seriey" value="'+serie+'">\
                '+serie+'</td>\
            <td><button type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+id+','+sucursal+','+cont_s+')"><i class="fa fa-trash icon_font"></i> </button></td>\
        </tr>';
    $('.tbody_'+sucursal).append(html);
    cont_s++;
}

function delete_serie(id,sucursal,cont){
    if(id==0){
        $('.row_s_'+sucursal+'_'+cont).remove();    
    }else{  
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar esta serie?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Productos/delete_record_serie",
                        data:{id:id},
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                            $('.row_s_'+sucursal+'_'+cont).remove();    
                        }
                    }); 
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function add_lote(sucursal){
    var cantidad=$('.cantidad_'+sucursal).val();
    var lote=$('.lote_'+sucursal).val();
    var caducidad=$('.caducidad_'+sucursal).val();
    var cod_barras=$('.cod_barras_'+sucursal).val();
    if(cantidad!='' && cantidad!=0 && lote!='' && lote!=0 && caducidad!=''){
        get_add_lote_tabla(0,sucursal,cantidad,lote,caducidad,cod_barras);
        $('.cantidad_'+sucursal).val('');
        $('.lote_'+sucursal).val('');
        $('.caducidad_'+sucursal).val('');
        $('.cod_barras_'+sucursal).val('');
    }else{
        swal("¡Atención!", "Falta agregar lote y caducidad", "error");
    }
}

function get_add_lote_tabla(id,sucursal,cantidad,lote,caducidad,cod_barras){

    var html='<tr class="tr_l row_l_'+sucursal+'_'+cont_l+'">\
            <td><input type="hidden" id="idl" value="'+id+'">\
                <input type="hidden" id="sucursall" value="'+sucursal+'">\
                <input type="hidden" id="cantidadl" value="'+cantidad+'">\
                <input type="hidden" id="lotel" value="'+lote+'">\
                <input type="hidden" id="caducidadl" value="'+caducidad+'">\
                <input type="hidden" id="cod_barrasl" value="'+cod_barras+'">\
                '+cantidad+'</td>\
            <td>'+lote+'</td>\
            <td>'+ChangeDate(caducidad)+'</td>\
            <td>'+cod_barras+'</td>\
            <td><button disabled type="button" class="btn btn-info" onclick="printCode('+id+')"><i class="fa fa-print icon_font"></i></button></td>\
            <td><button type="button" class="btn btn-pill btn-danger" onclick="delete_lote('+id+','+sucursal+','+cont_l+')"><i class="fa fa-trash icon_font"></i> </button></td>\
        </tr>';
    $('.tbody_l_'+sucursal).append(html);
    cont_l++;
}

function delete_lote(id,sucursal,cont){
    if(id==0){
        $('.row_l_'+sucursal+'_'+cont).remove();    
    }else{  
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar este lote?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Productos/delete_record_lote",
                        data:{id:id},
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                            $('.row_l_'+sucursal+'_'+cont).remove();    
                        }
                    }); 
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function editar_serie(id,sucursal,cont){
    var serie = $('.seriex_'+cont).data('serie');
    $('.editar_txt_'+cont).html('<input type="text" id="seriey" style="width:100%" class="form-control" value="'+serie+'">');
    $('.editar_x_'+cont).val(1);
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"-"+parte[1]+"-"+parte[0];
}


/*proveedores*/
var vali=0;
function add_proveedorres(){
    var id=$('#idproveedor option:selected').val();
    var proveedortext=$('#idproveedor option:selected').text();
    var codigo=$('#codigo').val();
    vali=id;
    if(id==undefined && codigo==''){
        swal("¡Atención!", "Falta selecionar una opción", "error");
    }else{
        var veri = numbersewg.some(testNumber); 
        if(veri==false){
            numbersewg.push(id);
            tabla_proveedores(0,id,proveedortext,codigo); 
            $('#idproveedor').val(null).trigger('change');
            $('#codigo').val('');
            $("#cont_cod_provee").removeClass("cont_cod_provee");
        }else{
            swal("¡Atención!", "El proveedor ya existe", "error");
        }
    }

}

function validarCodigProv(val){
    $.ajax({
        type:'POST',
        url: base_url+"Productos/validar_cod_prove",
        data: {codigo:val},
        success: function (response){
            if(response==1){
                swal("Álerta!", "Código ya existe en otro producto", "warning");
                $("#codigo").val("");
            }
        },
        error: function(response){
            //swal("Error!", "500", "error");
        }
    });
}

function testNumber(element){ 
    return element == vali; 
}

var cont_pro=0;
function tabla_proveedores(id,idproveedor,proveedortext,codigo){
    var btn="";
    if($('#perfil').val()!=11){ 
        btn+='<button class="btn btn-pill btn-danger" type="button" onclick="delete_registro_proveedor('+id+','+cont_pro+','+idproveedor+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_p'+cont_pro+'">\
            <td scope="col" style="place-content: center !important;">\
                <input type="hidden" id="idx" value="'+id+'">\
                <input type="hidden" id="idproveedorx" value="'+idproveedor+'">\
                <input type="hidden" id="codigox" value="'+codigo+'">'+proveedortext+'</td>\
            <td scope="col">'+codigo+'</td>\
            <td scope="col">'+btn+'</td>\
          </tr>';
    $('.t_docy_pro').append(html);  
    cont_pro++;  
}

function delete_registro_proveedor(id,cont,idproveedor){
    if(id==0){
        $('.row_p'+cont).remove();
            let pos = numbersewg.indexOf(idproveedor);
            numbersewg.splice(pos, 1);
    }else{
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Productos/delete_proveedor",
                    data: {id:id},
                    success: function (response){
                        swal("Éxito!", "Eliminado correctamente", "success");
                        $('.row_p'+cont).remove();
                        let pos = numbersewg.indexOf(idproveedor);
                        numbersewg.splice(pos, 1);
                    },
                    error: function(response){
                        swal("Error!", "500", "error");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
    }
}

function get_tabla_proveedor(id){
    $.ajax({
        type:'POST',
        url: base_url+"Productos/get_tabla_proveedor",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    var text_proveedor=element.nombre
                    tabla_proveedores(element.id,element.idproveedor,text_proveedor,element.codigo);
                    numbersewg.push(element.idproveedor);
                });
            }else{
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

var contcar=0;
function guardar_proveedores(id){
    var DATA  = [];
    var TABLA   = $("#table_datos_proveedor .t_docy_pro > tr");
    TABLA.each(function(){ 
        contcar=1;
        item = {};
        item ["idproducto"] = id;
        item ["id"] = $(this).find("input[id*='idx']").val();
        item ["idproveedor"] = $(this).find("input[id*='idproveedorx']").val();
        item ["codigo"] = $(this).find("input[id*='codigox']").val();
        DATA.push(item);
    });     
    if(contcar==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'index.php/Productos/registro_proveedores',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });  
    }
}