var base_url=$('#base_url').val();
var chart2;
var chartuti;
$(document).ready(function($) {
	setTimeout(function(){ 
        generargrafica(0);
    }, 1000);

    $('#id_provee').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar proveedor',
        allowClear: true,
        ajax: {
            url: base_url+'ReporteSaldo/searchproveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var item = [];
                //console.log(data);
                data.forEach(function(element) {
                    item.push({
                        id: element.id,
                        text: element.nombre,
                        codigo: element.codigo,
                        contacto: element.contacto
                    });
                });
                return {
                    results: item
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
    });

	$('#id_provee,#fechai,#fechaf').on("change",function(){
		const fechaInicio = new Date($("#fechai").val());
   		const fechaFin = new Date($("#fechaf").val());

   		const fechaInicioValida = esFechaValida(fechaInicio);
    	const fechaFinValida = esFechaValida(fechaFin);
		if($('#id_provee option:selected').val() != undefined && $("#fechai").val()!="" && $("#fechaf").val()!="" && fechaInicioValida && fechaFinValida){
			loadtable();
		}
	});

	$("#btn_export_excel").on("click",function(){
		exportTable();
	});
});

function esFechaValida(fecha) {
    const fechaObjeto = new Date(fecha);
    return !isNaN(fechaObjeto.getTime()); // Verifica si es una fecha válida
}

function loadtable(){
	$('body').loading({theme: 'dark',message: 'Procesando...'});
	var id_provee = $('#id_provee option:selected').val();
	var fechai = $('#fechai').val();
	var fechaf = $('#fechaf').val();

	$.ajax({
        type:'POST',
        url: base_url+"ReporteSaldo/getTotalCompras",
        data: { id_provee:id_provee, fechai:fechai, fechaf:fechaf },
        async:false,
        success: function (response){
        	$('body').loading('stop');
        	var array = $.parseJSON(response);
            $('.info_compras').html(array.html);
            setTimeout(function(){ 
                generargrafica(1);
            }, 1500);
            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            $('body').loading('stop');
        }
    });
}

function generargrafica(view){
	var id_provee = $('#id_provee option:selected').val();
	//console.log('id_provee: '+id_provee);
	if (view>0) {
        chart2.destroy();
    }
    if($("#fechai").val()!="" && $("#fechaf").val()!=""){
    	if(id_provee>0){
    		var total_compras = Number($("#supertot").val());
			var DATA_label  = [];
			var DATA_val  = [];
			DATA_label.push(""); 
			DATA_val.push(0); 
			var tab_compras = $("#table_total_compra tbody > tr"); 
            tab_compras.each(function(index){                   
                var sup  = $(this).find("input[id*='total']").val();
                var nam  = $(this).find("input[id*='name_mes']").val();
                //console.log("Index: " + index + ", Length: " + tab_compras.length);
                if(index < tab_compras.length - 1){
	                DATA_label.push(nam);
	                DATA_val.push(sup);
	            }
            });


			var options = {
			    chart: {
			        height: 350,
			        type: 'area',
			        zoom: {
			            enabled: false
			        },
			        toolbar: {
			            show: false
			        }
			    },
			    dataLabels: {
			        enabled: false
			    },
			    stroke: {
			        curve: 'straight'
			    },
			    series: [{
			        name: "Compras",
			        data: DATA_val
			    }],
			    title: {
			        text: 'Montos de compras',
			        align: 'left'
			    },
			    subtitle: {
			        text: '(Montos finales por mes)',
			        align: 'left'
			    },
			    labels: DATA_label,
			    yaxis: {
			    	//min: 0,
			        opposite: true
			    },
			    xaxis: {
			        labels: {
			            show: true
			        },
			        title: {
			            text: ''
			        },
			        min: 0  // Establecer el límite mínimo en 0 para el eje X
			    },
			    legend: {
			        horizontalAlign: 'left'
			    },
			    colors: [vihoAdminConfig.primary],
			    annotations: {
			        yaxis: [{
			            y: total_compras, // Línea horizontal en el valor total de compras
			            borderColor: '#FF5733', // Color de la línea
			            label: {
			                borderColor: '#FF5733',
			                style: {
			                    color: '#fff',
			                    background: '#FF5733',
			                    fontSize: '14px', // Tamaño de la fuente
                   				whiteSpace: 'pre-line'
			                },
			                //text: 'Total Compras: $' + total_compras.toFixed(2) // Mostrar el total
			                text: 'Total Compras: \n' + new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_compras),
			                html: true,
			                align: 'left'
			            },
			            offsetY: 1
			        }]
			    }
			};

			chart2 = new ApexCharts(
			    document.querySelector("#basic-apex2"),
			    options
			);
			chart2.render();
    	}
    }else{
    	//console.log('sin sucursal elegida de grafica ventas: ');
    	var options = {
		    chart: {
		        height: 350,
		        type: 'area',
		        zoom: {
		            enabled: false
		        },
		        toolbar:{
		          show: false
		        }
		    },
		    dataLabels: {
		        enabled: false
		    },
		    stroke: {
		        curve: 'straight'
		    },
		    series: [{
		        name: "",
		        data: [1]
		    }],
		    title: {
		        text: '',
		        align: 'left'
		    },
		    subtitle: {
		        text: '',
		        align: 'left'
		    },
		    labels: ['1'],
		    
		    yaxis: {
		        opposite: true
		    },
		    legend: {
		        horizontalAlign: 'left'
		    },
		    colors:[vihoAdminConfig.primary]
		}
		chart2 = new ApexCharts(
		    document.querySelector("#basic-apex2"),
		    options
		);
		chart2.render();
    }
}

function exportTable(){
	var id_provee = $('#id_provee option:selected').val();
	var fechai = $('#fechai').val();
	var fechaf = $('#fechaf').val();

	if(fechai=="")
		fechai=0;
	if(fechaf=="")
		fechaf=0;
	
	window.open(base_url+"ReporteSaldo/exportSaldosExcel/"+id_provee+"/"+fechai+"/"+fechaf,"_blank");
}