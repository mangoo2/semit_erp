var base_url = $('#base_url').val();
var validar_user=0;
var validar_correo=0;

var validar_razon=0;
var validar_rfc=0; var idpgral=0;
$(document).ready(function() {

});

function checkTab(number){
    $(".tab"+number).removeClass("active");
}

function guardar_registro_datos(){
    var form_register = $('#form_registro_datos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true,
            },
            correo:{
              required: true,
            },
            cp:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_datos").valid();
    if($valid) {
        var validar_registro=0;
        if($('#desactivar').is(':checked')){
            var desactivarx=$('#motivo').val();
            console.log(desactivarx);
            if(desactivarx==''){
                validar_registro=1;
            }else{
                validar_registro=0;
            }  
            
        }else{
            validar_registro=0;
        }
        console.log(validar_registro);
        if(parseFloat(validar_registro)==0){
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Clientes/registro_datos_cliente',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    var array = $.parseJSON(data);

                    if(array.validar==0){
                        var idp=parseFloat(array.data);
                        idpgral=idp;
                        $('#idreg1').val(idp);
                        $('#idreg2').val(idp);
                        $('#idreg3').val(idp);
                        $('#idreg4').val(idp);
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            window.location = base_url+'Clientes';
                        }, 1000);
                    }else{
                        $('.btn_registro').attr('disabled',false);
                        swal("¡Atención!", "El nombre de cliente ya existe", "error");
                    }
                }
            });  
        }else{
            swal("¡Atención!", "El campo motivo esta vacio", "error");
        }  
    }   
}

function guardar_registro_datos_fiscales(){
    var idreg2=$('#idreg2').val();
    
    $("#form_registro_datos_fiscales").validate().destroy(); 

    if(idreg2!=0){
        var form_register = $('#form_registro_datos_fiscales');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        if($('#activar_datos_fiscales').is(':checked')){
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    razon_social: {
                            required: true,
                        },
                        rfc: {
                            required: true,
                        },
                        cp: {
                            required: true,
                        },
                        RegimenFiscalReceptor: {
                            required: true,
                        },
                        uso_cfdi: {
                            required: true,
                        },
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Clientes/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }   
        }else{
            var $validator1=form_register.validate({
                errorElement: 'div', //default input error message container
                errorClass: 'vd_red', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                },
                
                errorPlacement: function(error, element) {
                    if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                        element.parent().append(error);
                    } else if (element.parent().hasClass("vd_input-wrapper")){
                        error.insertAfter(element.parent());
                    }else {
                        error.insertAfter(element);
                    }
                }, 
                
                invalidHandler: function (event, validator) { //display error alert on form submit              
                        success_register.fadeOut(500);
                        error_register.fadeIn(500);
                        scrollTo(form_register,-100);

                },

                highlight: function (element) { // hightlight error inputs
            
                    $(element).addClass('vd_bd-red');
                    $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label, element) {
                    label
                        .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                        .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    $(element).removeClass('vd_bd-red');
                }
            });
            //////////Registro///////////
            var $valid = $("#form_registro_datos_fiscales").valid();
            if($valid) {
                if(validar_razon==0){
                    if(validar_rfc==0){
                        var datos = form_register.serialize();
                        $('.btn_registro').attr('disabled',true);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Clientes/registro_datos_fiscales',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    swal("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    swal("Error!", "500", "error");
                                }
                            },
                            success:function(data){  
                                swal("Éxito!", "Guardado Correctamente", "success");
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    //window.location = base_url+'Clientes';
                                }, 1000);

                            }
                        });    
                    }else{
                        swal("¡Atención!", "El rfc ya existe", "error");
                    }
                }else{
                    swal("¡Atención!", "La razón social ya existe", "error");
                }   
            }   
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}


function guardar_registro_usuario(){
    var idreg3=$('#idreg3').val();
    if(idreg3!=0){
        var form_register = $('#form_registro_usuario');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Usuario:{
                  required: true,
                  minlength: 4
                },
                contrasena:{
                  required: true,
                  minlength: 5
                },
                contrasena2: {
                    equalTo: contrasena,
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_usuario").valid();
        if($valid) {
            if(validar_user==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Clientes/registro_datos',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    	var idp=data;
                        swal("Éxito!", "Guardado Correctamente", "success");
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //window.location = base_url+'Clientes';
                        }, 1000);

                    }
                });    
            }else{
                swal("¡Atención!", "El usuario ya existe", "error");
            }
        }
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }   
}

function verificar_correo(){
    $.ajax({
        type: 'POST',
        url: base_url+'Clientes/validar_correo',
        data: {
            correo:$('#correo').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_correo=1;
                swal("¡Atención!", "El correo electrónico ya existe", "error");
            }else{
                validar_correo=0;
            }
        }
    });
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Clientes/validar',
        data: {
            usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                swal("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}

function regimefiscal(){
    $( "#uso_cfdi").val(0);
    $( "#uso_cfdi option").prop( "disabled", true );
    var regimen = $('#RegimenFiscalReceptor option:selected').val();
    $( "#uso_cfdi ."+regimen ).prop( "disabled", false );
}

function validar_razon_social(){
    var razon_social=$('#razon_social').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/get_validar_razon_social",
        data: {
            razon_social:razon_social,
        },
        success: function (response){
            if (response == 1) {
                validar_razon=1;
                swal("¡Atención!", "La razón social ya existe", "error");
            }else{
                validar_razon=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}

function validar_rfc_cliente(){
    var rfc=$('#rfc').val();
    var id=$('#idreg1').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/get_validar_rfc",
        data: {
            rfc:rfc, id:id
        },
        success: function (response){
            if (response == 1) {
                validar_rfc=1;
                swal("¡Atención!", "El rfc ya existe", "error");
            }else{
                validar_rfc=0;
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            
        }
    });
}

function cambiaCP2(){
    var cp = $("#codigo_postal").val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Clientes/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                //$(".fis_estado_"+id).val(array[0].estado);
                //$(".fis_ciudad_"+id).val(array[0].ciudad);
                cambia_cp_colonia2();
            }
        });

    }
}

function cambia_cp_colonia2(){
    var cp = $("#codigo_postal").val();
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Clientes/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Clientes/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }
            }
        });
    }else{
        //$(".fis_estado_"+id).attr("readonly",false);
    }
}

function activar_campo(){
    if($('#desactivar').is(':checked')){
        $('.motivo_txt').css('display','block');
    }else{
        $('.motivo_txt').css('display','none');
    }
}

function guardar_registro_pagos(){
    var idreg2=$('#idreg4').val();
    if(idreg2!=0){
        var form_register = $('#form_registro_pagos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                diascredito:{
                  required: true,
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        //////////Registro///////////
        var $valid = $("#form_registro_pagos").valid();
        if($valid) {
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Clientes/registro_datos_pago',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){  
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        $('.btn_registro').attr('disabled',false);
                        //window.location = base_url+'Clientes';
                    }, 1000);

                }
            });      
        }   
    }else{
        swal("¡Atención!", "Por favor primero tienes que guardar datos generales", "error");
    }
}

function tipo_pago(num){
    if(num==1){
       $('.diaspagotxt').css('display','block');
       $('.saldopagotxt').css('display','none');
    }else if(num==2){
       $('.diaspagotxt').css('display','none');
       $('.saldopagotxt').css('display','block');
    }
}

var pass_aux=0;
function clickoverpass(){
    var obj = document.getElementById('contrasena');
    var obj2 = document.getElementById('contrasena2');
    if(pass_aux==0){
        pass_aux=1;  
        obj.type = "text";
        obj2.type = "text";
    }else{
        pass_aux=0;
        obj.type = "password";
        obj2.type = "password";
    }  

}

function modal_alert(){
    $('#modal_notificacion').modal('show');
}