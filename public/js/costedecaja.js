var idarqueo;
var perfilid = $('#perfilid').val();
$(document).ready(function($) {
	$('#wizard-2').bootstrapWizard({
		'tabClass': 'nav nav-pills nav-justified',
		'nextSelector': '.wizard .next',
		'previousSelector': '.wizard .prev',
		'onTabShow' :  function(tab, navigation, index){
			$('#wizard-2 .finish').hide();
			$('#wizard-2 .next').show();
			
			if ($('#wizard-2 .nav li:last-child').hasClass('active')){
						$('#wizard-2 .next').hide();
						$('#wizard-2 .finish').show();
			}
			

			var $total = navigation.find('li').length;
			var $current = index+1;
			var $percent = ($current/$total) * 100;
			$('#wizard-2').find('.progress-bar').css({width:$percent+'%'});
		},
		'onTabClick': function(tab, navigation, index) {
			//return false;	
			// comentar al finalizar solo para pruebas	
			$('#wizard-1 .finish').hide();
			$('#wizard-1 .next').show();
			if ($('#wizard-1 .nav li:last-child').hasClass('active')){
				$('#wizard-1 .next').hide();
				$('#wizard-1 .finish').show();
			}
			//
		},
		'onNext': function(tab, navigation, index){
			console.log('index: '+index);
			if (index==1) {
				$('#wizard-2 .next').hide();
						$('#wizard-2 .finish').show();
			}
			if (index==2) {
				$('#wizard-2 .next').hide();
						$('#wizard-2 .finish').show();
			}
			/*
			if (index==3) {
				var $valid3 = $("#formcaptura3").valid();
                if(!$valid3) {
                    $validator3.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                }
			}
			if (index==4) {
				var $valid4 = $("#formcaptura4").valid();
                if(!$valid4) {
                    $validator4.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                }
			}
			if (index==5) {
				var $valid5 = $("#formcaptura5").valid();
                if(!$valid5) {
                    $validator5.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                }
			}
			if (index==6) {
				var $valid6 = $("#formcaptura6").valid();
                if(!$valid6) {
                    $validator6.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                }
			}
			if (index==7) {
				var $valid7 = $("#formcaptura7").valid();
                if(!$valid7) {
                    $validator7.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                }
			}
			if (index==8) {
				var $valid8 = $("#formcaptura8").valid();
                if(!$valid8) {
                    $validator8.focusInvalid();
                    return false;
                    scrollTo('#wizard-2',-100);
                    $('#wizard-2 .next').removeClass('disabled');
                }
			}
			if (index==9) {
				var $valid9 = $("#formcaptura9").valid();
                if(!$valid9) {
                    $validator9.focusInvalid();
                    //return false;
                    //scrollTo('#wizard-2',-100);
                    if ($('#wizard-2 .nav li:last-child').hasClass('active')){
						$('#wizard-2 .next').hide();
						$('#wizard-2 .finish').show();
					}
                }
                $('#wizard-2 .next').removeClass('disabled');
			}
			*/
			scrollTo('#wizard-2',-100);
		},
		'onPrevious': function(){
			scrollTo('#wizard-2',-100);
		}

	});	
	verificarturno();
	$('#sucursalselect').change(function(event) {
		verificarturno();
	});
});
function verificarturno(){
	var suc = $('#sucursalselect option:selected').val();;
	var turno = $('#sucursalselect option:selected').data('idturno');
	if(suc>0){
		if(turno>0){
			$('.generar_arqueo').prop('disabled',false);
		}else{
			$('.generar_arqueo').prop('disabled',true);
			swal("¡Atención!", "La sucursal no tiene un turno abierto", "error");
		}
	}
}
function generararqueo(){
	var suc = $('#sucursalselect option:selected').val();
	var emple=$('#empleadoselect option:selected').val();
	if(suc>0){
		 $.ajax({
            type:'POST',
            url: base_url+"index.php/Cortecaja/generararqueo",
            data: {
                suc:suc,
                emple:emple
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                	$('.numerode_arqueo').html('<span class="ar_title">No. Arqueo:</span><span class="ar_subtitle">'+array.folio+'</span>');
                	$('#numerode_arqueo').val(array.folio);
                	$('.emp_arqueo').html('<span class="ar_title">Empleado:</span><span class="ar_subtitle">'+array.pername+'</span>');
                	$('#emp_arq_n').val(array.pername);
                	$('#emp_arqueo').val(array.perid);
                	
                	$('.select_suc').html(array.sucursalname);
                	$('#suc_arqueo').val(array.sucursalid);
                	$('.tbody_tablepagos').html('');
                	var totales_ar = 0;
                	$.each(array.datosmontos, function(index, item) {
                		if(item.clavefm=='01'){
                			if(perfilid==3){
                				var monto=parseFloat(item.monto);//para perfil ventam no agre
                			}else{
                				var monto=parseFloat(item.monto)+parseFloat(array.montocaja);	
                			}
                			
                		}else{
                			var monto=parseFloat(item.monto);
                		}
                		totales_ar += Number(monto);
                		var montostring=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(monto);
                		var html ='<tr>\
                                        <td >\
                                        	<input type="hidden" id="clavefm" value="'+item.clavefm+'">\
                                        	<input type="hidden" id="bancoid" value="'+item.bancoid+'">\
                                        	<input type="hidden" id="monto" value="'+monto+'">\
                                        	'+item.formapago_alias+'\
                                        	'+item.name_ban+'\
                                        </td>\
                                        <td>'+montostring+'</td>\
                                    </tr>';
                		$('.tbody_tablepagos').append(html);
		        		
		        	});
		        	var totales_arstring=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totales_ar);
		        	$('.total_tablepagos').html(totales_arstring);

            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
	}else{
		toastr.warning('Seleccione una sucursal');
	}
}
function imprimirarqueo(){
	var sucursal = $('#suc_arqueo').val();
	var folio = $('#numerode_arqueo').val();

	var personal = $('#emp_arqueo').val();
	var productos = $("#tablepagos tbody > tr");
    //==============================================
        var DATAa  = [];
        productos.each(function(){         
            item = {};                    
            item ["clavefm"]   = $(this).find("input[id*='clavefm']").val();
            item ["bancoid"]   = $(this).find("input[id*='bancoid']").val();
            item ["monto"]   = $(this).find("input[id*='monto']").val();

            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
    //==================================================
    if(folio!=''){
        var datos='sucursal='+sucursal+'&folio='+folio+'&personal='+personal+'&conceptos='+aInfoa;
        window.open(base_url+'index.php/Reportes/arqueopreview?'+datos, '_blank');
    }else{
        swal("¡Atención!", "Para poder imprimir el arqueo de caja, es necesario generar primero el arqueo mediante el botón azul llamado Generar Arqueo", "error");
    }
}
function guardararqueo(){
	var sucursal = $('#suc_arqueo').val();
	var idturno = $('#sucursalselect option:selected').data('idturno');
	var folio = $('#numerode_arqueo').val();
	var personal = $('#emp_arqueo').val();
	var productos = $("#tablepagos tbody > tr");
    //==============================================
        var DATAa  = [];
        productos.each(function(){         
            item = {};                    
            item ["clavefm"]   = $(this).find("input[id*='clavefm']").val();
            item ["bancoid"]   = $(this).find("input[id*='bancoid']").val();
            item ["monto"]   = $(this).find("input[id*='monto']").val();

            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
    //==================================================
    if(folio!=''){
        $('.generar_arqueo').css('display','none');
        $('.regresar_corte').css('display','block');
        $('.btn_regresar').html('<a class="btn btn-primary regresar_corte" href="'+base_url+'Cortecaja">Regresar</a>');
        

        var datos='sucursal='+sucursal+'&folio='+folio+'&personal='+personal+'&conceptos='+aInfoa+'&id_turno='+idturno;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cortecaja/guardararqueo",
            data: datos,
            success: function (data){
            	console.log(data);
            	$('.btn_paso1').hide('show');
				$('.btn_paso2').show('show');
				var array = $.parseJSON(data);
            	console.log(array); 
            	idarqueo=array.idarqueo;

            	var html='<h3>Confirmar montos</h3>';
            		html+='<input type="hidden" id="idarqueo" value="'+idarqueo+'">';
            		html+='<table class="table" id="table_confirm_montos"><thead><tr><td>Forma de pago</td><td>Monto</td><td>Confirmar monto</td></tr></thead><tbody>';
            	$.each(array.formapago, function(index, item) {
            		if(item.clavefm=='00'){
            			var readonly='';
            		}else{
            			var readonly=' readonly ';
            		}
            		var monto=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(item.monto);

            		html+='<tr><td>'+item.formapago_alias+' '+item.banco+'</td><td>'+monto+'</td>\
                        <td>\
                            <input type="hidden" id="idardll" value="'+item.id+'">\
                            <input type="number" id="montodll" class="form-control" value="'+item.monto+'" '+readonly+'>\
                        </td></tr>';
	        	});
	        		
	        		html+='</tbody></table>';
	        	$('.row_confirmarpagos').html(html);

            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
    }else{
        swal("¡Atención!", "Para poder continuar, es necesario generar el arqueo de caja mediante el botón azul llamado Generar Arqueo", "error");
    }
}
function guardararqueop2(){
	var in_idarqueo=$('#idarqueo').val();
		if(in_idarqueo>0){

		}else{
			in_idarqueo=idarqueo;
		}
	var productos = $("#table_confirm_montos tbody > tr");
    //==============================================
        var DATAa  = [];
        productos.each(function(){         
            item = {};                    
            item ["id"]   = $(this).find("input[id*='idardll']").val();
            item ["monto_confirm"]   = $(this).find("input[id*='montodll']").val();

            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);
    //==================================================

    var datos='conceptos='+aInfoa;
    $.ajax({
            type:'POST',
            url: base_url+"index.php/Cortecaja/guardararqueop2",
            data: datos,
            success: function (response){
            	$('.btn_paso1').hide('show');
				$('.btn_paso2').hide('show');
				//toastr.success('Confirmación registrada');
				swal("Éxito!", "Arqueo guardado con éxito", "success");
				setTimeout(function(){ 
					location.href =base_url+'Cortecaja?cortexz=1&idarqueo='+idarqueo;
				}, 2000);
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
}