var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
var id_traspaso = $('#id_traspaso').val();
var prod=0; var cant=0;
$(document).ready(function() {
    $('.sel_deta_prod_lot').select2({ width:"95%" });
    $('.sel_deta_prod_ser').select2({ width:"99%" });
    consultar_unidades();

    search_producto();
    $("#cantidad").on("change",function(){
        if($(this).val()!="" && $(this).val()>0 && $("#idproducto option:selected").val()!=undefined && $("#idproducto option:selected").val()!=""){
            get_producto();
        }
    });
});

function validaSerie(element,val){
    //console.log("val: "+val);
    var exist=0;
    var TABLA = $("#table_datos_prods tbody > tr");
    TABLA.each(function(){      
        if($(this).find("select[id*='series_x'] option:selected").val()==val){//
            exist++;
        }
    });

    if(exist>1){
        swal("¡Atención!", "Serie ya elejida con anterioridad", "error");
        $(element).val(null).trigger('change');
        $(element).val("");
    }
}

function formatStateProds (state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span class="colum_in_1">' + state.idProducto + '</span> <span class="colum_in_2">' + state.nombre + ' ('+state.disp_stock+')</span>'
    );
    return $state;
}


function search_producto(){
    $('#idproducto').select2({
        width: 'resolve',
        minimumInputLength: 5,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        templateResult: formatStateProds,
        ajax: {
            url: base_url+'Traspasos/searchproductosAsigna',
            dataType: "json",
            delay: 300, // Espera 300 ms después de la última pulsación antes de enviar la petición
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    destino: $("#id_dest").val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                var disp_stock = 0;
                data.forEach(function(element) {
                    if(element.tipo!=1 && element.tipo!=2){
                        disp_stock=element.stockps-element.traslado_stock_cant;
                    }else if(element.tipo==1){
                        disp_stock=element.serie8-element.tot_tras;
                    }else if(element.tipo==2){
                        disp_stock=element.cant_lote-element.traslado_lote_cant;
                    }
                    itemscli.push({
                        id: element.id,
                        idProducto: element.idProducto,
                        text: element.idProducto+' '+element.nombre + " (stock: "+disp_stock+")",
                        nombre: element.nombre,
                        tipo:element.tipo,
                        lote8:element.lote8,
                        serie8:element.serie8,
                        stock:element.stock,
                        stockps:element.stockps,
                        disp_stock:disp_stock,
                        stockps_ent: element.stockps_ent,
                        stock_series_ent: element.stock_series_ent,
                        stock_lotes_ent: element.stock_lotes_ent,
                        unisat:element.unidad_sat
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
       // get_producto(data.id);
        prod=data.nombre;
        //console.log("disp_stock: "+data.disp_stock);
        $("#idproducto option:selected").attr('data-stock',data.disp_stock);
        $("#idproducto option:selected").attr('data-codigo',data.idProducto);
        $("#idproducto option:selected").attr('data-tipo_prod',data.tipo);
        var stock_ent=0;
        if(data.tipo!=1 && data.tipo!=2){
            stock_ent= data.stockps_ent;   
            $("#idproducto option:selected").attr('data-stockps_ent',data.stockps_ent);
        }if(data.tipo==1){
            stock_ent= data.stock_series_ent; 
            $("#idproducto option:selected").attr('data-stockps_ent',data.stock_series_ent);
        }if(data.tipo==2){
            stock_ent= data.stock_lotes_ent; 
            $("#idproducto option:selected").attr('data-stockps_ent',data.stock_lotes_ent);
        } 
        $("#idproducto option:selected").attr('data-unisat',data.unisat);
        //get_producto();
        validaTrasladoCant(data.id,data.tipo,stock_ent);
    });
}

function validaTrasladoCant(id,tipo,stock_ent){
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/getCantidadTras",
        data: { id:id, id_suc: $("#id_dest").val() },
        success:function(response){
            //console.log("response: "+response);
            var array = $.parseJSON(response);
            //disp_ent = stock_ent - array.tot_tras_all;
            //$("#idproducto option:selected").attr('data-stockps_ent',disp_ent); 
            if(tipo!=1 && tipo!=2){
                disp_ent = stock_ent - array.traslado_stock_cant;
                $("#idproducto option:selected").attr('data-stockps_ent',disp_ent);   
            }if(tipo==1){
                disp_ent = stock_ent - array.tot_tras2;
                $("#idproducto option:selected").attr('data-stockps_ent',disp_ent); 
            }if(tipo==2){
                disp_ent = stock_ent - array.traslado_lote_cant;
                $("#idproducto option:selected").attr('data-stockps_ent',disp_ent); 
            }
            get_producto();
        }
    });   
}

function getSeries(id,id_suc) {
    var datos="";
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/get_series_tras",
        data:{id:id, id_suc:id_suc},
        //async: false,
        success:function(response){
            //console.log(response);
            //datos = response;
            var array=$.parseJSON(response);
            $(".sel_new_serie_"+id).append(array.html);
            $(".sel_new_serie_"+id).select2({});
        }
    });
    //return datos;
}

function getLotes(id,id_suc) {
    var datos="";
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/get_lotes_tras",
        data:{id:id, id_suc:id_suc},
        //async: false,
        success:function(response){
            //console.log(response);
            //datos = response;
            var array=$.parseJSON(response);
            //console.log("array.html: "+array.html);
            $(".sel_new_lote_"+id).append(array.html);
            $(".sel_new_lote_"+id).select2({});
        }
    });
    //return datos;
}

var cont_new=0;
function get_producto(){ //verificar funciones de conteo, cant disponible, eliminacion, y guardar el traslado con los nuevos prods-stock, lote y de serie
    var id=$("#idproducto option:selected").val();
    var codigo=$("#idproducto option:selected").data("codigo");
    var tipo=$("#idproducto option:selected").data("tipo_prod");
    var stockps_ent=$("#idproducto option:selected").data("stockps_ent");
    var unisat=$("#idproducto option:selected").data("unisat");
    var nombre = prod;
    var cantidad = Number($("#cantidad").val());
    var stock = Number($("#idproducto option:selected").data("stock"));
    var html="";

    if(cantidad==0){
        swal("¡Atención!","Ingreso una cantidad a solicitar", "error");
        return;
    }
    dis_ser="";
    if(tipo=="1"){//serie
        cantidad=1;
        dis_ser="disabled";
    }
    if(stock<cantidad){
        swal("¡Atención!","No se cuenta con suficiente stock en almacén general", "error");
        return;
    }

    var repetido=0;
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        var prodexi = $(this).find("input[id*='idprod']").val(); 
        if (id == prodexi) {
            repetido=1;
        }   
    });
    if(repetido==0){
        html="<tr class='rowprod_0 row_new_"+cont_new+"'>\
        <td><input type='hidden' id='id_ht' value='0'>\
            <input type='hidden' id='idtraspaso' value='"+$('#id_traspaso').val()+"'>\
            <input type='hidden' class='rechazado_0' id='rechazado' value='0'>\
            <input type='hidden' id='tipo' value='0'>\
            <input type='hidden' id='tipo_prod' value='"+tipo+"'>\
            <input type='hidden' id='idsucursal_entra' value='"+$('#id_dest').val()+"'>\
            <input type='hidden' id='idsucursal_sale' value='"+$('#id_origen').val()+"'>\
            <input type='hidden' id='id_compra' value='"+$('#id_compra').val()+"'>\
            <input type='hidden' id='idproducto_series' value='0'>\
            <input type='hidden' id='id_lote' value='0'>\
            <input type='hidden' id='idproducto' value='"+id+"'>\
            <input type='hidden' id='stockps' value='"+stock+"'>\
            <input type='hidden' id='stockps_entra' value='"+stockps_ent+"'>\
            "+codigo+"\
        </td>\
        <td>"+cantidad+"</td>\
        <td><input type='number' "+dis_ser+" class='form-control cantidad_sol cantidad_sol_new cant_new_"+id+" cantidad_sol_"+id+" cantidad_sol_new_"+id+"' id='cantidad_sol' value='"+cantidad+"' onchange='changeCantNew(0,"+stock+","+cantidad+","+id+","+tipo+")'></td>\
        <td>"+nombre+"</td>";
        if(tipo==0){ //detalles
            html+='<td></td>';
        }
        if(tipo==1){ //series
            html+='<td style="color:#152342"><div class="row"><div class="col-md-12">';
            html+='<select onchange="validaSerie($(this),this.value)" class="sel_new_serie_'+id+' sel_deta_prod_ser form-control selec_new_'+id+'" id="series_x">';
                html+='<option value="0" selected>Selecciona una opción</option>';
                getSeries(id,$("#id_origen").val());   
            html+='</select>\
            </div></div></td>';
        }else if(tipo==2){
            col="12";
            if(cantidad>1){
                col="10";
            }
            html+='<td><div class="row">';
            html+='<div class="col-md-'+col+'">';
                html+='<select class="sel_new_lote_'+id+' sel_deta_prod_lot form-control selec_new_'+id+'" id="lote_x" onchange="verificaCantLoteNew('+id+')">';
            html+='<option value="0" selected>Selecciona una opción</option>';
                getLotes(id,$("#id_origen").val());           
            html+='</select></div>';
            /*if(cantidad>1){
                html+='<div class="col-md-1">';
                    html+='<button class="btn_add_'+cont_new+' btn btn-pill btn-primary" type="button" onclick="add_traspasos_lotes('+cont_new+')"><i class="fa fa-plus"></i></button>';
                html+='</div>';
            }*/
            html+='</td>';
        }
        html+='<td class="unidad_sat unidad_sat_'+unisat+'" data-val="'+unisat+'">'+unisat+'</td>';
        html+='<td>'+$("#name_ori").val()+'</td>';
        html+='<td>'+$("#name_dest").val()+'</td>';
        html+='<td>';
        html+='<button title="Eliminar Concepto" class="btn_del_'+cont_new+' btn btn-danger btn_delete_conc_'+cont_new+'" type="button" onclick="deleteProdNew('+cont_new+')"><i class="fa fa-trash" aria-hidden="true"></i></button>';
        //html+='<button title="Rechazar concepto" class="btn_recha_'+cont_new+' btn btn-danger btn_rechaza_conc_'+cont_new+'" type="button" onclick="rechazaProd('+cont_new+','+tipo+')"><i class="fa fa-close" aria-hidden="true"></i></button>';
        html+='</td>';
        
        $("#body_prods").append(html);
        $('#idproducto').val(null).trigger('change');
        $("#cantidad").val("");
        calculaTotalProds(); 
        cont_new++;
    }else{
        swal("¡Atención!","Producto ya existente en tabla de solicitud", "error");
    }
}

function save_solicitud_all(id){
    var band_serie=0;
    var band_lote=0;
    var DATA  = [];
    var TABLA   = $("#table_datos_prods tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_ht']").val();
        item ["idtraspaso"] = $(this).find("input[id*='idtraspaso']").val();
        item ["idproducto"] = $(this).find("#idproducto").val(); //no lo recopila
        item ["rechazado"] = $(this).find("input[id*='rechazado']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
        item ["tipo"] = $(this).find("input[id*='tipo']").val();
        item ["tipo_prod"] = $(this).find("input[id*='tipo_prod']").val();
        item ["idsucursal_entra"] = $(this).find("input[id*='idsucursal_entra']").val();
        item ["idsucursal_sale"] = $(this).find("input[id*='idsucursal_sale']").val();
        item ["id_compra"] = $(this).find("input[id*='id_compra']").val();
        //item ["idproducto_series"] = $(this).find("input[id*='idproducto_series']").val();
        //item ["id_lote"] = $(this).find("input[id*='id_lote']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad_sol']").val();
        item ["cant_ini"] = $(this).find("input[id*='stockps_entra']").val();

        if($(this).find("input[id*='tipo_prod']").val()==0){//
            item ["idproducto_series"] = 0;
            item ["id_lote"] = 0;
        }
        if($(this).find("input[id*='tipo_prod']").val()==1){//series
            //item ["idseries"] = $(this).find("select[id*='series_x'] option:selected").val();
            if($(this).find("select[id*='series_x'] option:selected").val()=="0"){
                band_serie++;
                item ["idproducto_series"] = 0;
            }else{
                item ["idproducto_series"] = $(this).find("select[id*='series_x'] option:selected").val();
            }
            item ["id_lote"] = 0;
        }

        if($(this).find("input[id*='tipo_prod']").val()==2){ //lotes
            //item ["idproducto_lote"] = $(this).find("select[id*='lote_x'] option:selected").val();
            if($(this).find("select[id*='lote_x'] option:selected").val()=="0"){
                band_lote++;
                item ["id_lote"] = 0;
            }else{
                item ["id_lote"] = $(this).find("select[id*='lote_x'] option:selected").val();
            }
            item ["idproducto_series"] = 0;
        }
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    console.log("aInfo: "+aInfo);
    if(band_serie>0){
        swal("¡Atención!", "Elija una serie a trasladar", "error");
        return false;
    }
    if(band_lote>0){
        swal("¡Atención!", "Elija un lote a trasladar", "error");
        return false;
    }
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Traspasos/save_asigna_prods_solicitud',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        beforeSend: function(){
            $('.btn_transitoy').attr('disabled',true);
        },
        success: function(data){
            setTimeout(function(){ 
                swal("Éxito!", "Guardado Correctamente", "success");
                //window.location.href = base_url+"Traspasos";
                generarcartaporte(id_traspaso);
            }, 1000); 
            
        }
    });
}
function generarcartaporte(idtraspaso){
    $.ajax({
        type:'POST',
        url: base_url+"Timbrado/generafacturarcartaporte/"+idtraspaso,
        data: { id:0 },
        success:function(response){
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                /*
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError,
                              type: "warning",
                              showCancelButton: false
                 });
                */
                //retimbrar(array.facturaId,0);
            }else{
                    var textofactura='Guardado Correctamente';
                
                swal({
                    title:"Éxito!", 
                    text:'Guardado Correctamente', 
                    type:"success"
                    });
                
            }
            setTimeout(function(){ 
                    window.location.href = base_url+"Traspasos";
                }, 1000); 
        }
    });
}
function changeCant(tipo,stock,cant,idht,tipo_prod){ //validar cantidades solicitadas de lotes y series
    //console.log("cant: "+cant);
    if(tipo==1){ //recargas
        var cant_change =Number($(".cantidad_sol_"+idht).val());
        if(( cant_change % 9500 ) == 0){
            if(stock>=cant_change){
                calculaTotalProds(); 
            }else{
                $(".cantidad_sol_"+idht).val(cant);
                swal("¡Atención!", "No se cuenta con los litros suficientes", "error");
                calculaTotalProds();
                return;
            }
        }else{
            $(".cantidad_sol_"+idht).val(cant);
            swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");
            calculaTotalProds();  
        }
    }else{
        var cant_change =Number($(".cantidad_sol_"+idht).val());
        //console.log("cant_change: "+cant_change);
        if(tipo_prod!=2){
            if(Number(stock)<Number(cant_change)){
                swal("¡Atención!", "Cantidad mayor al stock de sucursal solicitada", "error");
                $(".cantidad_sol_"+idht).val(cant);
                calculaTotalProds();
                return;
            }else{
                calculaTotalProds();
            }
        }if(tipo_prod==2){ //lotes
            var cant_disp = $(".selec_"+idht+" option:selected").data("cantidad");
            //console.log("cant_disp: "+cant_disp);
            if(cant_disp!=undefined && Number(cant_disp) < Number($(".cant_"+idht).val()) ){
                swal("¡Atención!", "Cantidad mayor a la disponible del lote", "error");
                //$(".cant_"+idht).val(cant_disp);
                $(".cantidad_sol_"+idht).val(cant);
            }else{
                calculaTotalProds();
            }
        }
    }
}
function changeCantNew(tipo,stock,cant,id,tipo_prod){ //validar cantidades solicitadas de lotes y series -- falta validar mismo prod diferente lote
    //console.log("tipo: "+tipo);
    //console.log("cant: "+cant);
    if(tipo==1){ //recargas
        var cant_change =Number($(".cantidad_sol_"+id).val());
        if(( cant_change % 9500 ) == 0){
            if(stock>=cant_change){
                calculaTotalProds(); 
            }else{
                $(".cantidad_sol_new_"+id).val(cant);
                swal("¡Atención!", "No se cuenta con los litros suficientes", "error");
                calculaTotalProds();
                return;
            }
        }else{
            $(".cantidad_sol_new_"+id).val(cant);
            swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");
            calculaTotalProds();  
        }
    }else{
        var cant_change =Number($(".cantidad_sol_new_"+id).val());
        console.log("cant_change: "+cant_change);
        if(tipo_prod!=2){
            if(Number(stock)<Number(cant_change)){
                swal("¡Atención!", "Cantidad mayor al stock de sucursal solicitada", "error");
                $(".cantidad_sol_new_"+id).val(cant);
                calculaTotalProds();
                return;
            }else{
                calculaTotalProds();
            }
        }if(tipo_prod==2){ //lotes
            var cant_disp = $(".selec_new_"+id+" option:selected").data("cantidad");
            //console.log("cant_disp: "+cant_disp);
            if(cant_disp!=undefined && Number(cant_disp) < Number($(".cant_new_"+id).val()) ){
                swal("¡Atención!", "Cantidad mayor a la disponible del lote", "error");
                //$(".cant_"+idht).val(cant_disp);
                $(".cantidad_sol_new_"+id).val(cant);
            }else{
                calculaTotalProds();
            }
        }
    }
}

function verificaCantLote(idht){
    var cant_disp = $(".selec_"+idht+" option:selected").data("cantidad");
    if(Number(cant_disp) < Number($(".cant_"+idht).val()) ){
        swal("¡Atención!", "Cantidad mayor a la disponible del lote", "error");
        $(".cant_"+idht).val(cant_disp);
    }
}

function verificaCantLoteNew(id){
    var cant_disp = $(".selec_new_"+id+" option:selected").data("cantidad");
    if(Number(cant_disp) < Number($(".cant_new_"+id).val()) ){
        swal("¡Atención!", "Cantidad mayor a la disponible del lote", "error");
        $(".cant_new_"+id).val(cant_disp);
    }
}

function calculaTotalProds(){
    var totp=0;
    $(".cantidad_sol").each(function(){
        totp=totp+Number($(this).val());
    });
    $("#tot_prod").html(totp);
}

function rechazaProd(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de rechazar el concepto de la solicitud?'+
                '<br><textarea placeholder="Ingrese un motivo por el cual rechaza" id="motivo" class="motivo_'+id+' form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                if($(".motivo_"+id).val().trim()!=""){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Traspasos/rechaza_prod_solicitud",
                        data:{id:id, motivo:$("#motivo").val()},
                        success:function(data){
                            swal("Éxito!", "Rechazado correctamente", "success");
                            $(".rechazado_"+id).val("1");
                            $(".cant_"+id).attr("disabled",true);
                            $(".btn_del_"+id).attr("disabled",true);
                            $(".btn_recha_"+id).attr("disabled",true);
                            $(".selec_"+id).attr("disabled",true);
                            $(".btn_add_"+id).attr("disabled",true);
                            //calculaTotalProds();
                        }
                    });
                }else{
                    //toastr.error('Ingrese un motivo de rechazo al concepto');
                    swal("¡Atención!", 'Ingrese un motivo de rechazo al concepto', "warning");  
                }
            },
            cancelar: function(){
            }
        }
    });
}

function deleteProd(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el concepto de la solicitud?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/delete_prod_solicitud",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Eliminando correctamente", "success");
                        $(".rowprod_"+id).remove();
                        calculaTotalProds();
                    }
                });
            },
            cancelar: function(){
            }
        }
    });
}

function deleteProdNew(id){
    $(".row_new_"+id).remove();
    calculaTotalProds();
}

var cont_clone=0;
function add_traspasos_lotes(id){
    /*var clonSelect = $(".selec_"+id).clone();
    clonSelect.attr('id', 'lote_x_clone_'+cont_clone);*/

    clone = $(".rowprod_"+id).clone().attr('class', "rowprod_clone_"+id+"_"+cont_clone);
    clone.find("#cantidad_sol").val("");
    clone.find("#id_ht").val("0");
    clone.find("#lote_x").val("0");

    clone.find("span.select2 ").remove();
    clone.find("select").select2({ width:"95%" });
    
    clone.find(".btn_add_"+id).remove();
    clone.find(".btn_recha_"+id).remove();
    clone.find(".btn_delete_conc_"+id).attr("onclick", "delete_lote("+id+","+cont_clone+")");
    $('.rowprod_'+id).after(clone);
    //$('.selec_'+id).select2();
    //clone.insertAfter(".rowprod_"+id);

    /*$('.rowprod_clone_'+id+'_'+cont_clone+' #lote_x').replaceWith(clonSelect);
    $('.rowprod_clone_'+id+'_'+cont_clone+' #lote_x').select2({width: '100%'});*/
}

function delete_lote(id,cont){
    $('.rowprod_clone_'+id+"_"+cont).remove();    
}

var cont_clone=0;
function add_traspasos_serie(id){
    clone = $(".rowprod_"+id).clone().attr('class', "rowprodser_clone_"+id+"_"+cont_clone);
    clone.find("#cantidad_sol").val("");
    clone.find("#id_ht").val("0");
    clone.find("#series_x").val("0");

    clone.find("span.select2 ").remove();
    clone.find("select").select2({ width:"95%" });
    
    clone.find(".btn_add_"+id).remove();
    clone.find(".btn_recha_"+id).remove();
    clone.find(".btn_delete_conc_"+id).attr("onclick", "delete_serie("+id+","+cont_clone+")");
    $('.rowprod_'+id).after(clone);
    //$('.selec_'+id).select2();
    //clone.insertAfter(".rowprod_"+id);

    /*$('.rowprod_clone_'+id+'_'+cont_clone+' #lote_x').replaceWith(clonSelect);
    $('.rowprod_clone_'+id+'_'+cont_clone+' #lote_x').select2({width: '100%'});*/
}

function delete_serie(id,cont){
    $('.rowprod_clone_'+id+"_"+cont).remove();    
}
function consultar_unidades(){
    var DATAu  = [];
    $(".unidad_sat").each(function() {
        item = {};
        item ["uni"] = $(this).data('val');
        DATAu.push(item);
    });
    //============== elimina duplicados ==================
    DATAu = DATAu.filter((item, index, self) =>
        index === self.findIndex((t) => t.uni === item.uni)
    );
    //=====================================================
    //console.log(DATAu);
        aInfo   = JSON.stringify(DATAu);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/consultar_unidades",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            //console.log(array);
            $.each(array, function(index, item) {
                $('.unidad_sat_'+item.uni).html(item.nom);
                
            });
        }
    });
}