var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
var tabla;
$(document).ready(function() {
    table();
    $('#searchtext').focus();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Clientes/getlistado",
            type: "post",
            "data":{tipocliente:$('#tipocliente').val(),desactivar:$('#desactivar').val(),sucursal:$('#sucursal_cli option:selected').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html=row.id;
                return html;
                }
            },
            {"data":"nombre"},
            {"data":"usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a onclick="modal_datos_fiscales('+row.id+')" class="btn"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.desactivar==1){
                    html='<button class="btn btn-danger btn-sm" type="button">Suspendido</button>';
                    html+='<a class="btn idreg_'+row.id+'" type="button" data-motivo="'+row.motivo+'" onclick="modal_motivo('+row.id+')"><i class="fa fa-eye"></i></a>';
                }else{
                    html='<button class="btn btn-sm actionactivo" type="button">Activo</button>';
                }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.saldo);
                    if(row.saldo!='0.00'){
                       html+='<a class="btn" type="button" href="'+base_url+'Clientes/cliente_saldo/'+row.id+'"><i class="fa fa-eye"></i></a>';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">';
                        html+='<a href="'+base_url+'Clientes/registro/'+row.id+'" class="btn btn-cli-edit"></a>';
                        if(perfil==1 || perfil==6){
                            html+='<a onclick="eliminar_registro('+row.id+')" class="btn btn-cli-delete"></a>';
                        }
                    html+='</div>';
                return html;
                }
            },
        ],
        "columnDefs": [
          { "orderable": false, "targets": 3 },
          { "orderable": false, "targets": 4 },
          { "orderable": false, "targets": 5 },
          { "orderable": false, "targets": 6 },
        ],
        "order": [[ 0, "ASC" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function modal_motivo(id){
    $('#modal_suspension').modal('show');
    var motivo=$('.idreg_'+id).data('motivo');
    $('.motivotxt').html(motivo);
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar a este cliente?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Clientes/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}

function modal_datos_fiscales(id){
    $('#modal_datosficales').modal('show');
    $('.datos_fiscalestxt').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Clientes/get_cliente_fiscales",
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.datos_fiscalestxt').html(data);
        }
    }); 
}

function get_excel(){
    location.href= base_url+'Clientes/reporte_excel';
}