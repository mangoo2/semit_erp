var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
$(document).ready(function() {
    table();
    $('#searchtext').focus();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        destroy:true,
        "ajax": {
            "url": base_url+"Traspasos/getlistado",
            type: "post",
            data: { estatus:$("#estatus option:selected").val() },
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"reg"},
            {"data":"personal"},
            {"data":"suc_solicita"},
            {"data":"suc_salida"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a class="btn" onclick="modal_detalles('+row.id+')"><i class="fa fa-eye"></i></a>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipo==0){
                        html="Por venta";
                    }else{
                        html="Manual";
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.status==0){
                        if(perfil==2 || perfil==3){
                            html='<button class="btn btn-danger btn-sm" type="button">Apartado</button>';
                        }else{
                            html='<button class="btn btn-danger btn-sm" type="button" onclick="modal_apartado('+row.id+')">Apartado</button>';
                        }
                    }else if(row.status==1){    
                        if(perfil==1 || perfil==2 || perfil==3 || perfil==4 || perfil==10){
                            html='<button class="btn btn-sm" style="background-color:#ffc107" onclick="modal_transito('+row.id+')" type="button">Tránsito</button>';
                        }else{
                            html='<button class="btn btn-sm" style="background-color:#ffc107" type="button">Tránsito</button>';
                        }
                    }else if(row.status==2){    
                        html='<button class="btn btn-sm" type="button" style="background-color:#93ba1f;">Ingresado</button>';
                    }/*else{
                        html='<button class="btn btn-danger btn-sm" type="button">Eliminado</button>';
                    }*/if(row.rechazado==1){
                        html='<button onclick="verMotivo('+row.id+')" class="btn btn-danger btn-sm" type="button">Rechazado</button>';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a style="cursor:pointer" onclick="get_traspasos_pdf('+row.id+')"><img class="img_icon" src="'+base_url+'public/img/pdf1.png"></a>';
                    if(perfil==2 || perfil==3){
                    }else{
                        if(row.rechazado==0 && row.status==0 && $('#perfil').val()==1){
                            //html+='<a onclick="cancelar_tras('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/cancel.svg"></a>';
                            html+='<a onclick="eliminar_registro('+row.id+')" class="btn"><img style="width: 30px;" src="'+base_url+'public/img/trash.svg"></a>';
                        }
                    }
                    html+='<button title="Re enviar mail de notificación" class="btn btn-success" onclick="reenviar_mai('+row.id+')"><i class="fa fa-envelope-open"></i></button>';
                    if(row.FacturasId>0){
                        html+='<a class="" \
                                href="'+base_url+'Folio/facturapdf/'+row.FacturasId+'" target="_blank"\
                                data-facturasid="'+row.FacturasId+'" \
                                 ><img class="img_icon" src="'+base_url+'public/img/pdf1.png"></a> ';
                        if(row.Estado==1){
                            html+='<a class="" \
                                    href="'+base_url+row.rutaXml+'" target="_blank"\
                                    data-position="top" data-delay="50" title="XML" \
                                  download ><img class="img_icon" src="'+base_url+'public/img/xml2b.png"></a> ';
                        }
                    }
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

function reenviar_mai(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de re enviar mail de notificación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/mail_solicitud",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Enviado correctamente", "success");
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function modal_detalles(id){
    $('#modaldetalles').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/get_transpasos',
        data: {
            id:id,tipo:0
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){  
            $('.text_transpasos').html(data);
        }
    });
}

function verMotivo(id){
    $('#modalmotivo').modal('show');
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/get_motivo',
        data: {
            id:id
        },
        success:function(data){  
            $('#txt_motivo').html(data);
        }
    });
}

function cancelar_tras(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de rechazar solicitud de traspaso?'+
                '<br><textarea placeholder="Ingrese un Motivo" id="motivo" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/rechazar_traspaso",
                    data:{id:id,motivo:$("#motivo").val()},
                    success:function(data){
                        swal("Éxito!", "Rechazado correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar solicitud de traspaso?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/delete_record_transpaso",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

var id_traspaso_aux=0;
function modal_apartado(id){
    id_traspaso_aux=id;
    /*$('#modaldetalles_apartado').modal('show');
    $('.text_transpasos_apartado').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/get_transpasos_apartado',
        data: {
            id:id,tipo:0
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){  
            $('.text_transpasos_apartado').html(data);
        }
    });*/
    
    window.open(base_url+'Traspasos/asignaProductosTras/'+id); 
}

function changeCant(tipo,stock,cant,cont){ //validar cantidades solicitadas de lotes y series
    if(tipo==1){
        //console.log("cant: "+cant);
        var cant_change =Number($(".cantidad_serie_"+cont).val());
        if(( cant_change % 9500 ) == 0){
            if(stock>=cant_change){
                
            }else{
                $(".cantidad_serie_"+cont).val(cant);
                swal("¡Atención!", "No se cuenta con los litros suficientes", "error");
                return;
            }
        }else{
            $(".cantidad_serie_"+cont).val(cant);
            swal("¡Atención!", "Solo se permiten solicitud de litros completos (Por tanque)", "error");  
        }

        /*if(Number(stock)<Number(cant)){
            swal("¡Atención!", "Cantidad mayor al stock de sucursal solicitada", "error");
            $(".cantidad_serie_"+cont).val(cant);
            return;
        }*/
    }else{
        var cant_change =Number($(".cantidad_serie_"+cont).val());
        if(Number(stock)<Number(cant_change)){
            swal("¡Atención!", "Cantidad mayor al stock de sucursal solicitada", "error");
            $(".cantidad_serie_"+cont).val(cant);
            return;
        }
    }
}

function validar_cerrar_traspaso(){
    $('.btn_transitoy').attr('disabled',true);
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/update_record_transpaso_transito",
        data:{id:id_traspaso_aux},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            swal("Éxito!", "Guardado Correctamente", "success");
            setTimeout(function(){ 
                tabla.ajax.reload(); 
                $('#modaldetalles_apartado').modal('hide');
                $('.btn_transito').attr('disabled',false);
                $('.btn_transitoy').attr('disabled',false);
            }, 1000); 
        }
    });
}

function update_transito(id,tipo,cont){
    $('.btn_transito_'+cont).attr('disabled',true);
    //actualizar cantidades de historico traslados
    var DATA  = [];
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["id_ht"] = $(this).find("input[id*='id_ht']").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad_serie']").val();
        //console.log("id_ht: "+$(this).find("input[id*='id_ht']").val())
        if($(this).find("input[id*='id_ht']").val()!=undefined && $(this).find("input[id*='id_ht']").val()!=0){
            DATA.push(item);
        }
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Traspasos/editar_cant_solicitud',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
        }
    });
    
    ////////////////// 
    if(tipo==1){
        var num_serie=$("#table_datos_traspasos tbody > .tr_sx").length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidad_serie_'+cont).val();
        var cs=parseFloat(cantidad_serie);
        if(ns==cs){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos tbody > .tr_sx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idseries"] = $(this).find("input[id*='idseriex']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar series", "error");
        }
    }else if(tipo==2){
        var num_lote=$("#table_datos_traspasos_lotes tbody > .tr_lx").length; 
        var nl=parseFloat(num_lote);
        if(nl!=0){
            /*$.ajax({
                type:'POST',
                url: base_url+"Traspasos/update_record_transpaso_transito",
                data:{id:id},
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                    setTimeout(function(){ 
                        tabla.ajax.reload(); 
                        $('#modaldetalles_apartado').modal('hide');
                        $('.btn_transito').attr('disabled',false);
                    }, 1000); 
                }
            }); */

            var DATA  = [];
            var TABLA   = $("#table_datos_traspasos_lotes tbody > .tr_lx");
            TABLA.each(function(){         
                item = {};
                item ["idtraspasos"] = id;
                item ["idlotes"] = $(this).find("input[id*='idlotex']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidadx']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);

            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Traspasos/registro_series_traspasos_lotes',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        //toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        //toastr.error('Error', '500');
                    }
                },
                success: function(data){
                    swal("Éxito!", "Guardado Correctamente", "success");
                }
            }); 
        }else{
            setTimeout(function(){ 
                $('.btn_transito_'+cont).attr('disabled',false);
            }, 1000); 
            swal("¡Atención!", "Falta agregar lote", "error");
        }
    }else{
        swal("Éxito!", "Guardado Correctamente", "success");
        /*$.ajax({
            type:'POST',
            url: base_url+"Traspasos/update_record_transpaso_transito",
            data:{id:id},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    tabla.ajax.reload(); 
                    $('#modaldetalles_apartado').modal('hide');
                    $('.btn_transito').attr('disabled',false);
                }, 1000); 
            }
        }); */
    }
    $(".btn_delete_conc_"+cont).attr("disabled",true);
}

function modal_transito(id){
    $('#modaldetalles_transito').modal('show');
    $('.text_transpasos_transito').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/get_transpasos_transito',
        data: {
            id:id,tipo:0
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){  
            $('.text_transpasos_transito').html(data);
        }
    });
}

function validaCant(val,id_ht){
    //console.log("val: "+val);
    //console.log("id_ht: "+id_ht);
    var cant_comp=Number($("#cantidad_comp_"+id_ht).val());
    //console.log("cant_comp: "+cant_comp);
    if(val<=cant_comp){

    }else{
        swal("Álerta!", "Cantidad ingresada mayor a la de compra", "error");
        $(".cantidad_real_"+id_ht).val(cant_comp);
    }
}

var cont_check=0;
function update_entrada(id,tipo,id_ht,element){
    /*var DATA  = [];
    var TABLA   = $("#table_datos tbody > tr");
    TABLA.each(function(){ 
        item = {};
        item ["idcompra"] = idcompra_aux;
        item ["idsucursal"] = $(this).find("select[class*='idsucursals']").val();
        item ["idproducto_lote"] = $(this).find("input[id*='idproducto_lote']").val();
        item ["idproducto"] = $("#idproducto_padre_"+cont+"").val();
        item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
        DATALO.push(item);
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);*/
    //console.log("id_ht:" +id_ht);

    //var cantidad=$('#cantidad_real').val();
    var cantidad=$(".cantidad_real_"+id_ht).val();
    if(cantidad=='' || cantidad==0){
        swal("¡Atención!", "Por favor de ingresar cantidad real", "error");
    }else{
        //element.attr("disabled",true);
        $("#btn_tra_"+id_ht).attr("disabled",true);
        $('.btn_transito').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+"Traspasos/transferir_cantidad_sucursal_transito",
            data:{id:id,cantidad:cantidad,tipo:tipo,id_ht:id_ht},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                $('.btn_transito').attr('disabled',false);
                cont_check++;
            }
        });
    }
}

function cerrar_entrada(id,tipo,tipo_tras){
    //if(cont_check==$(".table_det_tras tbody > tr").length){
    if(tipo_tras==1){
        $('.realizarventa').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+"Traspasos/cerrar_traslado",
            data:{id:id,tipo:tipo},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado correctamente", "success");
                cont_check=0;
                setTimeout(function(){ 
                    tabla.ajax.reload(); 
                    $('#modaldetalles_transito').modal('hide');
                    $('.realizarventa').attr('disabled',false);
                }, 1000); 
            }
        }); 
    }else{
        $('.realizarventa').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+"Traspasos/cerrar_trasladoSolicitud",
            data:{id:id,tipo:tipo,tipo_tras:tipo_tras},
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado correctamente", "success");
                cont_check=0;
                setTimeout(function(){ 
                    tabla.ajax.reload(); 
                    $('#modaldetalles_transito').modal('hide');
                    $('.realizarventa').attr('disabled',false);
                }, 1000); 
            }
        });  
    }
    //}else{
    //    swal("Álerta!", "Se deben aceptar todos los conceptos", "error");
    //}
}

function deleteProd(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el concepto de la solicitud?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/delete_prod_solicitud",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Eliminando Correctamente", "success");
                        if(tipo==0){
                            $(".rowprod_"+id).remove();
                        }if(tipo==1){
                            $(".rowprod_"+id).remove();
                            $(".rowserie_"+id).remove();
                        }if(tipo==2){
                            $(".rowprod_"+id).remove();
                            $(".rowlote_"+id).remove();
                        }
                        $(".tr_fath_"+id).remove();
                        $(".btn_add_tras_"+id).remove();
                    }
                });
            },
            cancelar: function(){
            }
        }
    });
}

function rechazaProd(id,tipo){
    $(".cont_rechazo_"+id).show();
    $(".btn_addall_"+id).text("Confirmar rechazo");
    $(".btn_addall_"+id).attr("onclick","aceptarRechazo("+id+")");
    /*$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el concepto de la solicitud?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Traspasos/rechaza_prod_solicitud",
                    data:{id:id},
                    success:function(data){
                        swal("Éxito!", "Eliminando Correctamente", "success");
                    }
                });
            },
            cancelar: function(){
            }
        }
    });*/
}

function aceptarRechazo(id){
    if($(".motivo_concep_"+id).val().trim()!=""){
        $.ajax({
            type:'POST',
            url: base_url+"Traspasos/rechaza_prod_solicitud",
            data:{id:id,motivo:$(".motivo_concep_"+id).val()},
            success:function(data){
                swal("Éxito!", "Rechazado correctamente", "success");
                $(".cant_"+id).attr("disabled",true);
                $(".btn_del_"+id).attr("disabled",true);
                $(".btn_recha_"+id).attr("disabled",true);
                $(".selec_"+id).attr("disabled",true);
                $(".btn_add_"+id).attr("disabled",true);
            }
        });
    }else{
        swal("Álerta!", "ingrese un motivo de rechazo", "error");
    }

}

function add_traspasos(cont){
    var idreg=$('#series_x option:selected').val();
    var reg_text=$('#series_x option:selected').text();
    if(idreg!=0){
        var validars=0;
        var tabla_serie = $("#table_datos_traspasos tbody > .tr_sx");
        tabla_serie.each(function(){         
            var serie_x = $(this).find("input[id*='idseriex']").val();
            if(serie_x==idreg){
                validars++;
            }
        });
        var num_serie=$("#table_datos_traspasos tbody > .tr_sx").length; 
        var ns=parseFloat(num_serie);
        var cantidad_serie=$('.cantidad_serie_'+cont).val();

        var cs=parseFloat(cantidad_serie);
        if(ns<cs){
            if(validars<1){
                tabla_traspasos(idreg,reg_text);
            }else{
                swal("¡Atención!", "La serie ya existe", "error");    
            }
        }else{
            swal("¡Atención!", "Solo puedes agregar "+cantidad_serie+"", "error");    
        }
            
    }else{
        swal("¡Atención!", "Falta seleccionar una serie", "error"); 
    }
}

var cont_t=0;
function tabla_traspasos(idreg,reg_text){
    var html='<tr class="tr_sx row_s_'+cont_t+'">\
        <td><input type="hidden" id="idseriex" value="'+idreg+'">'+reg_text+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_serie('+cont_t+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_traspasos').append(html);
    cont_t++; 
}

function delete_serie(cont){
    $('.row_s_'+cont).remove();    
}

function add_traspasos_lotes(cont){
    var idreg=$('#lote_x option:selected').val();
    var cantidad_lote=$('#lote_x option:selected').data('cantidad');
    var reg_text=$('#lote_x option:selected').data('lotetxt');
    var cant_lo=parseFloat(cantidad_lote);
    var cantidad=$('#cantidad_lo').val();
    var cant=parseFloat(cantidad);
    if(idreg!=0){
        if(cantidad_lote>=cant){
            var validars=0;
            var tabla_lote = $("#table_datos_traspasos_lotes tbody > .tr_lx");
            var cantida_x_lote=0;
            tabla_lote.each(function(){         
                var lote_x = $(this).find("input[id*='idlotex']").val();
                var lote_num_x = $(this).find("input[id*='cantidadx']").val();
                cantida_x_lote+=parseFloat(lote_num_x);
                if(lote_x==idreg){
                    validars++;
                }
                
            });
            var suma=parseFloat(cantidad)+parseFloat(cantida_x_lote);
            var nl=parseFloat(suma);
            var cantidad_serie=$('.cantidad_serie_'+cont).val();
            var cs=parseFloat(cantidad_serie);
            var resta=cs-cantida_x_lote;
            if(nl<=cs){
                if(validars<1){
                    tabla_traspasos_lote(idreg,reg_text,cantidad);
                    $('#cantidad_lo').val('');
                }else{
                    swal("¡Atención!", "El lote ya existe", "error");    
                }
            }else{
                swal("¡Atención!", "Solo puedes agregar "+resta+"", "error");    
            }  
        }else{
            swal("¡Atención!", "El lote no cuenta con cantidad suficiente", "error"); 
        } 
    }else{
        swal("¡Atención!", "Falta seleccionar un lote", "error"); 
    }
}

var cont_tl=0;
function tabla_traspasos_lote(idreg,reg_text,cantidad){
    var html='<tr class="tr_lx row_l_'+cont_tl+'">\
        <td><input type="hidden" id="idlotex" value="'+idreg+'">'+reg_text+'</td>\
        <td><input type="hidden" id="cantidadx" value="'+cantidad+'">'+cantidad+'</td>\
        <td><a type="button" class="btn btn-pill btn-danger" onclick="delete_lote('+cont_tl+')"><i class="fa fa-trash icon_font"></i></a></td>\
    </tr>';
    $('#table_datos_traspasos_lotes').append(html);
    cont_tl++; 
}

function delete_lote(cont){
    $('.row_l_'+cont).remove();    
}

function get_traspasos_pdf(id){
    window.open(base_url+'Traspasos/imprimir_traspaso/'+id,'_blank'); 
}

function search(){
    var searchtext = $('#searchtext').val();
    tabla.search(searchtext).draw();
}
