var base_url = $('#base_url').val();
$(document).ready(function() {
    get_tabla_banners();
});

function vistas_add(){
	$.ajax({
        type:'POST',
        url: base_url+'BannersSliders/registrar_datos',
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            var idr=parseFloat(data);
            tabla_banner(idr);
        }
    }); 
}

cont=1;
function tabla_banner(id){
	var activo='';
	if(cont==1){
        activo='active';
	}else{
        activo='';
	}
	
    var html='<li class="nav-item pestana_vista_'+cont+'"><a class="nav-link '+activo+'" id="icon-vista'+cont+'-tab" onclick="img_mostrar('+id+')" data-bs-toggle="tab" href="#icon-vista'+cont+'" role="tab" aria-controls="icon-vista'+cont+'" aria-selected="true"><i class="icofont icofont-ui-vista'+cont+'"></i>Pestaña '+cont+'</a></li>';
	$('.pestana').append(html);
    
    var html2='<div class="tab-pane fade show pestana_vista_'+cont+' '+activo+'" id="icon-vista'+cont+'" role="tabpanel" aria-labelledby="icon-vista'+cont+'-tab">\
            <form id="form_detalles_'+cont+'" method="post">\
              <br>\
              <input class="form-control" type="hidden" name="id" value="'+id+'">\
              <h3>Imágenes del producto pestaña '+cont+'</h3>\
              <div class="row g-3">\
                <div class="col-md-4">\
                  <input type="file" id="files_'+id+'" name="files" accept="image/*">\
                  <h5><b>Insertar imagen de 1567 x 682</b></h5>\
                </div>\
                <div class="col-md-8">\
                  <div class="lis_catacter_'+id+'"></div>\
                </div>\
              </div>\
              <hr style="background-color: #009bdb;">\
              <h3>Título  y descripción</h3>\
              <div class="row g-3">\
                <div class="col-md-12">\
                  <label class="form-label">Título</label>\
                  <input class="form-control titulo_'+id+'" type="text" name="titulo">\
                </div>\
              </div>\
              <div class="row g-3">\
                <div class="col-md-12">\
                  <label class="form-label">Descripción</label>\
                  <input class="form-control descripcion_'+id+'" type="text" name="descripcion">\
                </div>\
              </div>\
              <hr style="background-color: #009bdb;">\
              <h3>Visualización del boton</h3>\
              <div class="row g-3">\
                <div class="col-md-12">\
                  <label class="form-label">Mostrar boton en página</label>\
                  <span class="switch switch-icon">\
                      <label>\
                          <input type="checkbox" class="mostrar_boton_'+id+'" name="mostrar_boton">\
                          <span></span>\
                      </label>\
                  </span>\
                </div>\
              </div>\
              <div class="row g-3">\
                <div class="col-md-12">\
                  <label class="form-label">Nombre</label>\
                  <input class="form-control nombre_'+id+'" type="text" name="nombre">\
                </div>\
              </div>\
              <div class="row g-3">\
                <div class="col-md-12">\
                  <label class="form-label">Link</label>\
                  <input class="form-control link_'+id+'" type="text" name="link">\
                </div>\
              </div>\
              <br>\
            </form>\
            <button class="btn btn-primary btn_ajustes" type="button" onclick="guardar_detalles('+cont+')">Guardar</button>\
            <button class="btn btn-danger" type="button" onclick="eliminar_registro('+cont+','+id+')">Eliminar</button>\
          </div>';
    $('.pestana2').append(html2);
    plugin_img(id);
    upload_data_view(id);
	cont++;
}

function plugin_img(id){
    $("#files_"+id).fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'BannersSliders/baner_imagenes',
        maxFilePreviewSize: 5000,
        minImageWidth: 1567,
        minImageHeight: 682,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        fileActionSettings: {
            removeIcon: '<i class="fa fa-trash"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idreg:id
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(id);
        $("#files_"+id).fileinput('refresh');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(id);
    });
}

function img_mostrar(id){
    upload_data_view(id);
}

function upload_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/BannersSliders/viewimages',
        data: {id:id},
        success:function(data){
            $('.lis_catacter_'+id).html(data);
            setTimeout(function(){ 
                $('.slick-slide').css('width','100%');
            }, 100); 
            //$('.materialboxed').materialbox();
            $(".regular_"+id).slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        }
    });
}

function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/BannersSliders/deleteimgbaner",
                    data: {id:id},
                    success: function (response){
                        swal("Éxito!", "Eliminado correctamente", "success");
                        upload_data_view(id);
                    },
                    error: function(response){
                        swal("Error!", "500", "error");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}


function get_tabla_banners(){
    $.ajax({
        type:'POST',
        url: base_url+"BannersSliders/get_tabla_detalles",
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element){
                    tabla_banner(element.id);
                    var id_aux=element.id;
                    $.ajax({
				        type:'POST',
				        url: base_url+'BannersSliders/get_datos_pestana',
				        data:{id:id_aux},
				        statusCode:{
				            404: function(data){
				                swal("Error!", "No Se encuentra el archivo!", "error");
				            },
				            500: function(){
				                swal("Error!", "500", "error");
				            }
				        },
				        success:function(data){
				        	var array=$.parseJSON(data);
                            if(array.length>0) {
                                array.forEach(function(element){
				                    $('.titulo_'+element.id).val(element.titulo);
                                    $('.descripcion_'+element.id).val(element.descripcion);
                                    $('.nombre_'+element.id).val(element.nombre);
                                    $('.link_'+element.id).val(element.link);
                                    if(element.mostrar_boton==1){
                                        $('.mostrar_boton_'+element.id).prop("checked", true);  
                                    }else{
                                        $('.mostrar_boton_'+element.id).prop("checked", false);  
                                    }
                                    
                                });
                            }
				        }
				    }); 
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function guardar_detalles(id){
	var form_register = $('#form_detalles_'+id);
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
           /* nombre: {
                required: true
            }*/
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $('#form_detalles_'+id).valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_ajustes').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'BannersSliders/registrar_datos_pestana',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var idr=parseFloat(data);
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_ajustes').attr('disabled',false);
                }, 1000);
            }
        }); 
    }
}

function eliminar_registro(cont,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"BannersSliders/delete_record",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                    	var resta=parseFloat(cont-1);
                    	$('#icon-vista'+resta).addClass('active');
                    	$('.pestana_vista_'+cont).remove();
                        swal("Éxito!", "Eliminado Correctamente", "success");
                        setTimeout(function(){ 
                            tabla.ajax.reload(); 
                        }, 1000); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}