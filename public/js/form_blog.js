var base_url = $('#base_url').val();
var idreg = $('#idreg').val();
var idcategoria_aux = $('#idcategoria_aux').val();
$(document).ready(function() {

    $('#articulo').ckeditor();
    $('#resumen').ckeditor();
    cateforia_get(idcategoria_aux);
});   

function cateforia_get(id){ //si es edicion mandar el id de categoria para selected
    $.ajax({
        type:'POST',
        url: base_url+"Blog/select_datos_cat_get",
        data: { id:id},
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.categoria_txt').html(data);
        }
    });  
}

function modal_categoria(){
    $('#modalcategoria').modal('show');
    reload_categoria();
    $('#idcategoria_categoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function add_categoria(){
    var form_register = $('#form_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_categoria').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Blog/addcategoria',
            data: datos,
            statusCode:{
                404: function(data){
                swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    $('.btn_categoria').attr('disabled',false);
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                    $('.vd_red').remove();
                }, 2000);
                cateforia_get(parseFloat(data));
                reload_categoria();
            }
        });
    }
}

function reload_categoria(){
    tabla_categoria=$("#table_categoria").DataTable();
    tabla_categoria.destroy();
    table_categorias();
}
function table_categorias(){
    tabla_categoria=$("#table_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Blog/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.id+'"\
                            data-categoria="'+row.nombre+'"\
                            class="btn btn-primary catec_'+row.id+'" onclick="editar_categoria('+row.id+')"><i class="fa fa-edit icon_font"></i></button>\
                            <button type="button" class="btn btn-flat btn-danger" onclick="delete_registro_categoria('+row.id+');"><i class="fa fa-trash icon_font"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_categoria(id){
    id_categoria=1;
    $('#idcategoria_categoria').val($('.catec_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.catec_'+id).data('categoria'));
}

function delete_registro_categoria(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta categoria?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Blog/delete_categoria",
                    data: {
                        id:id
                    },
                    statusCode: {
                        404: function(data){
                            swal("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            swal("Error!", "500", "error");
                        }
                    },
                    success:function(response){  
                        swal("Éxito!", "Eliminado correctamente", "success");
                        reload_categoria();
                        $('#categoria option[value="'+id+'"]').remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function guardar_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            titulo:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Blog/registro_datos',
            data: datos,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success:function(data){
                var idp=data;
                add_file(idp);
                swal("Éxito!", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Blog';
                }, 3000);

            }
        });
    }   
}


function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#file').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#file')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('file');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Blog/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
