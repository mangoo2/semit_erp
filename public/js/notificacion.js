var base_url = $('#base_url').val();
var tabla;
var perfilid=$("#perfil").val();
var id_solicitud_des=0;
$(document).ready(function() {
    //notificacion();
    notificacion_total();

    if($("#perfil").val()==1 || $("#perfil").val()==4){
        solicitudGarantias();
    }

    if($("#perfil").val()==1 || $("#perfil").val()==2 || $("#perfil").val()==3 || $("#perfil").val()==5){
        notificacionTras();
    }
    if(perfilid==1){
        notificacionsol_des();
    }
    notificacionRenta();
});

function notificacion() {
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alertas",
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            $('.alertas_notifi').html(data);  
        }
    }); 
}

function notificacion_total(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alertas_total",
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            $('.num_notifi').html(data);  
        }
    }); 
}

function solicitudGarantias(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alertasGarantias",
        async:false,
        success:function(data){
            var arr = $.parseJSON(data);
            $('.num_notifiGara').html(arr.cont);
            $('.alertas_notifiGar').html(arr.html);  
        }
    }); 
}

function notificacionTras(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alertasTras",
        async:false,
        success:function(data){
            var arr = $.parseJSON(data);
            $('.num_notifi2').html(arr.cont);
            $('.alertas_notifi2').html(arr.html);  
        }
    }); 
}

function notificacionRenta(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alertaRenta",
        async:false,
        success:function(data){
            var arr = $.parseJSON(data);
            $('.num_notifi3').html(arr.cont);
            $('.alertas_notifi3').html(arr.html);  
        }
    }); 
}
function notificacionsol_des(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_alerta_sol_des",
        async:false,
        success:function(data){
            var arr = $.parseJSON(data);
            if(arr.cont>0){
                $('.notification-box_sol_des').addClass('notification-box');
                $('.notification-box_sol_des .dot-animated_notificacion').addClass('dot-animated');
            }else{
                $('.notification-box_sol_des').removeClass('notification-box');
                $('.notification-box_sol_des .dot-animated_notificacion').removeClass('dot-animated');
            }
            $('.num_sol_des').html(arr.cont);
            $('.alertas_sol_des').html(arr.html);  
        }
    }); 
}
var a_s_cant=0;var a_s_name=''; var a_s_price_cu =0;var a_s_con_iva=0; var a_s_total=0;var a_count_row=0; var a_s_idpro=0;
function ver_sol_der(idsol){
    id_solicitud_des=idsol;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Inicio/ver_sol_der",
        data: { 
            idsol:idsol
        },
        success: function (response){
            var arr = $.parseJSON(response);
            if(arr.length>0){
                var dat=arr[0];
                $('.info_sol_c_ven').html(dat.nombre);
                $('.info_sol_c_suc').html(dat.name_suc);
                $('.a_s_cant').html(dat.cant);
                $('.a_s_name').html(dat.name);
                $('.a_s_price_cu').html(dat.precio);
                $('input[name=a_tipo_descuento][value='+dat.tipo_descuento+']').prop('checked',true).change();
                $('.a_s_price_cu').html(dat.precio);
                $('#a_s_mont_por').val(dat.s_mont_por);
                $('#a_s_mont_efe').val(dat.s_mont_efe);
                $('#a_s_mont_final').val(dat.s_mont_final);
                a_s_cant=dat.cant;
                a_s_name=dat.name;
                a_s_price_cu=dat.precio;
                a_s_con_iva=dat.coniva;
                a_s_total=dat.s_mont_final;
                a_s_idpro=dat.idproduto;
                $('#a_modal_descuentos').modal({backdrop: 'static', keyboard: false});
                $('#a_modal_descuentos').modal('show');
            }
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    }); 
}
//===================================================
    

    function a_calcular_sol_pro(){
        var a_desc=$('#a_s_mont_efe').val();
        if(a_desc==''){
            a_desc=0;
        }
        var a_subpro=(parseFloat(a_s_cant)*parseFloat(a_s_price_cu))-parseFloat(a_desc);
        if(a_s_con_iva==1){
            a_subpro=parseFloat(a_subpro)*1.16;
            a_subpro=redondear(a_subpro,2);
        }
        $('#a_s_mont_final').val(a_subpro);
    }
    function a_tipo_desc(){
        var a_tipo = $('input[name=a_tipo_descuento]:checked').val();
        if(a_tipo==1){
            $('#a_s_mont_por').hide('show');
            $('#a_s_mont_efe').show('show');
        }
        if(a_tipo==2){
            $('#a_s_mont_por').show('show');
            $('#a_s_mont_efe').hide('show');
        }
        
    }
    function a_cal_por_sol(){
        var a_por = $('#a_s_mont_por').val();
        if(a_por>0){
            var a_cant_des =(parseFloat(a_s_cant)*parseFloat(a_s_price_cu))*(parseFloat(a_por)/100);
            a_cant_des=redondear(a_cant_des,2);
        }else{
            var a_cant_des=0;
        }
        $('#a_s_mont_efe').val(a_cant_des);
        a_calcular_sol_pro();
    }
    function ok_autorizar(aurech){
        var a_tipo = $('input[name=a_tipo_descuento]:checked').val();
        var a_por = $('#a_s_mont_por').val();
        var desc_efe=$('#a_s_mont_efe').val();
        var m_final=$('#a_s_mont_final').val();
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasp/soldescuento",
            data: { 
                id_sol:id_solicitud_des,
                tipo_descuento:a_tipo,
                s_mont_por:a_por,
                s_mont_efe:desc_efe,
                s_mont_final:m_final,
                precio:a_s_price_cu,
                aurech:aurech

            },
            success: function (response){
                toastr.success('Solicitud enviada');
                $('#a_modal_descuentos').modal('hide');
            },
            error: function(response){
                toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });
    }
//=========================================================
    //verificar si esta ya se deja como global para quitarla en los otros js
function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}
function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}
//============================================================