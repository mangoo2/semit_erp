/* ***********02-01-2024**************** */
ALTER TABLE `venta_erp_detalle` ADD `id_ps_lote` INT NOT NULL AFTER `tipo`, ADD `id_ps_serie` INT NOT NULL AFTER `id_ps_lote`;
ALTER TABLE `productos_sucursales_serie` ADD `vendido` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `disponible`;

ALTER TABLE `recargas` ADD `orden` TINYINT NOT NULL AFTER `sucursal`;
UPDATE `recargas` SET `estatus` = '0' WHERE `recargas`.`id` = 10;
UPDATE `recargas` SET `estatus` = '0' WHERE `recargas`.`id` = 11;

/* ****************08-01-2024*******************/
ALTER TABLE `productos` ADD `incluye_iva_comp` VARCHAR(1) NOT NULL DEFAULT '1' AFTER `costo_compra`, ADD `iva_comp` FLOAT NOT NULL AFTER `incluye_iva_comp`;

DROP TABLE IF EXISTS `solicitud_traspaso`;
CREATE TABLE IF NOT EXISTS `solicitud_traspaso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_traspaso` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `dia_venta` date NOT NULL,
  `fecha_reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/* ****************11-01-2024*******************/
ALTER TABLE `venta_erp` ADD `id_cliente` INT NOT NULL AFTER `id_razonsocial`;

/* ********************24-01-24*******************************/
ALTER TABLE `venta_erp` ADD `observaciones` TEXT NOT NULL AFTER `total`;

/* *********************26-01-24************************************ */
ALTER TABLE `historial_transpasos` ADD `id_compra` INT NOT NULL AFTER `idtranspasos`;

DROP TABLE IF EXISTS `bitacora_reverse_entradas`;
CREATE TABLE IF NOT EXISTS `bitacora_reverse_entradas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ordencomp` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `bitacora_reverse_entradas` ADD `motivo` TEXT NOT NULL AFTER `fecha`;

/* ********************************* */
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '5', '28'), (NULL, '5', '29');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '5', '30');

UPDATE `menu_sub` SET `Nombre` = 'Rentas' WHERE `menu_sub`.`MenusubId` = 36;
UPDATE `menu_sub` SET `Nombre` = 'Servicios' WHERE `menu_sub`.`MenusubId` = 37;

/* ********************06-02-24*************************** */
ALTER TABLE `productos_sucursales_lote` ADD `reg` DATETIME NOT NULL AFTER `idcompra`;
ALTER TABLE `productos_sucursales_serie` ADD `reg` DATETIME NOT NULL AFTER `vendido`;

ALTER TABLE `traspasos` ADD `rechazado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `status`;
ALTER TABLE `traspasos` ADD `motivo_rechazo` TEXT NOT NULL AFTER `rechazado`;

ALTER TABLE `historial_transpasos` ADD `rechazado` VARCHAR(1) NOT NULL DEFAULT '' COMMENT '0=no, 1=si' AFTER `cantidadingreso`, ADD `motivo_rechazo` TEXT NOT NULL AFTER `rechazado`;
ALTER TABLE `sucursal` ADD `orden` VARCHAR(1) NOT NULL AFTER `direccion`;
UPDATE `sucursal` SET `orden` = '0' WHERE `sucursal`.`id` = 1;
UPDATE `sucursal` SET `orden` = '1' WHERE `sucursal`.`id` = 8;
UPDATE `sucursal` SET `orden` = '2' WHERE `sucursal`.`id` = 2;
UPDATE `sucursal` SET `orden` = '3' WHERE `sucursal`.`id` = 4;
UPDATE `sucursal` SET `orden` = '4' WHERE `sucursal`.`id` = 5;
UPDATE `sucursal` SET `orden` = '5' WHERE `sucursal`.`id` = 3;
UPDATE `sucursal` SET `orden` = '6' WHERE `sucursal`.`id` = 6;
UPDATE `sucursal` SET `orden` = '7' WHERE `sucursal`.`id` = 7;

/* **********14-02-24**************/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '9', 'Garantías', 'Garantias', 'log-in', '3', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '38');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '2', '38');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '38');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '4', '38');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '5', '38');

DROP TABLE IF EXISTS `garantias`;
CREATE TABLE IF NOT EXISTS `garantias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` text COLLATE utf8_spanish_ci NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1=solicitado,2=aceptado,3=rechazado',
  `motivo_rechazo` text COLLATE utf8_spanish_ci NOT NULL,
  `activo` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1=visible,0=no visible',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `garantias_detalle`;
CREATE TABLE IF NOT EXISTS `garantias_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_garantia` int(11) NOT NULL,
  `idprod` int(11) NOT NULL,
  `id_ps_lot` int(11) NOT NULL,
  `id_ps_ser` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `cant` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1=solicitado,2=aceptado,3=rechazado',
  `motivo_rechazo` text COLLATE utf8_spanish_ci NOT NULL,
  `retorno` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '0=no,1=retornado a stock',
  `activo` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1=visble,0=no visible',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `bitacora_garantias`;
CREATE TABLE IF NOT EXISTS `bitacora_garantias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gd` int(11) NOT NULL,
  `tipo_mov` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=retornado,2=reemplazo',
  `fecha` datetime NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `serie` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `comentarios` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `garantias` ADD `id_venta` INT NOT NULL AFTER `id_origen`;
ALTER TABLE `venta_erp_detalle` ADD `solicita_garantia` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `id_ps_serie`;

ALTER TABLE `bitacora_garantias` CHANGE `tipo_mov` `tipo_mov` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT '1=retornado,2=reemplazo,3=reparado,4=devuelto a provee';
ALTER TABLE `bitacora_garantias` ADD `nota_credito` VARCHAR(75) NOT NULL AFTER `serie`;

/* ***********2*-02-24**************** */
ALTER TABLE `cotizacion_detalle` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=producto,1=recarga,3=servicio' AFTER `descuento`, ADD `id_ps_lote` INT NOT NULL AFTER `tipo`, ADD `id_ps_serie` INT NOT NULL AFTER `id_ps_lote`;

/* **************03-04-2024********************* */
ALTER TABLE `bitacora_garantias` CHANGE `tipo_mov` `tipo_mov` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT '1=retornado,2=reemplazo,3=reparado,4=devuelto a provee, 5=no aplica garantia';

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Kardex', 'Kardex', 'fa fa-file', '5', '0', '1', '24');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '39');

/* ***************************************** */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Perfiles', 'Perfiles', 'settings', '8', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '40');
ALTER TABLE `perfiles_detalles` ADD `estatus` VARCHAR(1) NOT NULL DEFAULT '1' AFTER `MenusubId`;
ALTER TABLE `productos_sucursales_lote` ADD `cod_barras` VARCHAR(75) NOT NULL AFTER `caducidad`;

/* *****************20-05-2024*************************/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '3', 'Renta de Equipos', 'Rentas', 'gears', '3', '0', '1', '6');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '41', '1');


DROP TABLE IF EXISTS `rentas`;
CREATE TABLE IF NOT EXISTS `rentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_serie` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `periodo` text COLLATE utf8_spanish_ci NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `deja_docs` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '0=no,1=si',
  `fecha_inicio` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `fecha_fin` date NOT NULL,
  `hora_fin` time NOT NULL,
  `id_personal` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=activo,2=finalizado,3=cancelado',
  `id_personal_fin` int(11) NOT NULL,
  `fecha_finaliza` datetime NOT NULL,
  `id_personal_cancel` int(11) NOT NULL,
  `fecha_cancel` datetime NOT NULL,
  `renovacion` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0' COMMENT '0=no,1=si',
  `id_renta_padre` int(11) NOT NULL,
  `id_sucursal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `rentas` ADD `deposito` DECIMAL(10,2) NOT NULL AFTER `deja_docs`;

DROP TABLE IF EXISTS `docs_rentas`;
CREATE TABLE IF NOT EXISTS `docs_rentas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renta` int(11) NOT NULL,
  `documento` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `docs_rentas` CHANGE `estatus` `estatus` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '1';
 
ALTER TABLE `rentas` ADD `requiere_factura` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `id_cliente`;
ALTER TABLE `perfiles` ADD `tipo_venta` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `nombre`;

/* ****************************************** */
ALTER TABLE `rentas` ADD `pagado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `id_renta_padre`;
ALTER TABLE `rentas` ADD `id_venta` INT NOT NULL AFTER `id_sucursal`;
ALTER TABLE `venta_erp_detalle` CHANGE `tipo` `tipo` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=producto,1=recarga,2=renta';
ALTER TABLE `venta_erp_detalle` ADD `id_serv` INT NOT NULL AFTER `id_ps_serie`;
ALTER TABLE `rentas` ADD `folio` VARCHAR(10) NOT NULL AFTER `id_cliente`;
ALTER TABLE `rentas` ADD `costo_entrega` DECIMAL(8,2) NOT NULL AFTER `id_venta`, ADD `costo_recolecta` DECIMAL(8,2) NOT NULL AFTER `costo_entrega`;


ALTER TABLE `compra_erp` ADD `codigo` VARCHAR(100) NOT NULL AFTER `reg_norecibido`;
ALTER TABLE `rentas` ADD `id_prod` INT NOT NULL AFTER `id_serie`;
ALTER TABLE `compra_erp` ADD `cancelado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `estatus`, ADD `fecha_cancelado` DATETIME NOT NULL AFTER `cancelado`;


ALTER TABLE `traspasos` ADD `carta_porte` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `motivo_rechazo`;
ALTER TABLE `traspasos` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '0=por venta, 1=manual' AFTER `carta_porte`;
ALTER TABLE `traspasos_lotes_historial` ADD `id_historialt` INT NOT NULL AFTER `idtraspasos`;


/* agregar tabla evidencia_garantia*/ 
DROP TABLE IF EXISTS `evidencia_garantia`;
CREATE TABLE IF NOT EXISTS `evidencia_garantia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_garantia` int(11) NOT NULL,
  `imagen` text COLLATE utf8_spanish_ci NOT NULL,
  `id_personal` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/* agregar tabla orden_garantia */
DROP TABLE IF EXISTS `orden_garantia`;
CREATE TABLE IF NOT EXISTS `orden_garantia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_garantia` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observ` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `bitacora_garantias` ADD `id_traslado` INT NOT NULL AFTER `comentarios`;

/* *******************09-08-2024****************************** */
ALTER TABLE `orden_garantia` ADD `nom_solicita` VARCHAR(100) NOT NULL AFTER `observ`, ADD `dir_solicita` VARCHAR(100) NOT NULL AFTER `nom_solicita`, ADD `col_solicita` VARCHAR(75) NOT NULL AFTER `dir_solicita`, ADD `cel_solicita` VARCHAR(20) NOT NULL AFTER `col_solicita`, ADD `mail_solicita` VARCHAR(75) NOT NULL AFTER `cel_solicita`, ADD `rfc_solicita` VARCHAR(50) NOT NULL AFTER `mail_solicita`;
ALTER TABLE `orden_garantia` ADD `tel_solicita` VARCHAR(20) NOT NULL AFTER `cel_solicita`;


/* *******************14-08-2024****************************** */
ALTER TABLE `garantias_detalle` ADD `carta_provee` TEXT NOT NULL AFTER `retorno`;
ALTER TABLE `orden_garantia` ADD `nom_contacto` VARCHAR(100) NOT NULL AFTER `rfc_solicita`, ADD `tel_contacto` VARCHAR(15) NOT NULL AFTER `nom_contacto`;

/* ************************28-08-2024**************************** */
ALTER TABLE `sucursal` ADD `id_alias` VARCHAR(3) NOT NULL AFTER `clave`;
UPDATE `sucursal` SET `id_alias` = '02' WHERE `sucursal`.`id` = 2;
UPDATE `sucursal` SET `id_alias` = '05' WHERE `sucursal`.`id` = 3;
UPDATE `sucursal` SET `id_alias` = '03' WHERE `sucursal`.`id` = 4;
UPDATE `sucursal` SET `id_alias` = '04' WHERE `sucursal`.`id` = 5;
UPDATE `sucursal` SET `id_alias` = '06' WHERE `sucursal`.`id` = 6;
UPDATE `sucursal` SET `id_alias` = '07' WHERE `sucursal`.`id` = 7;
UPDATE `sucursal` SET `id_alias` = '01' WHERE `sucursal`.`id` = 8;


/********************2024-08-28 agb****************/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '5', 'Comisiones', 'Comisiones', NULL, '', '', '', '');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '45', '1');


/* ******************02-09-2024********************* */
INSERT INTO `referencias` (`id`, `name`, `activo`) VALUES (NULL, 'Artículo de Servicio', '1');

DROP TABLE IF EXISTS `bitacora_ins_servs_ventas`;
CREATE TABLE IF NOT EXISTS `bitacora_ins_servs_ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_insumo` int(11) NOT NULL,
  `id_prod_suc` int(11) NOT NULL,
  `id_serv` int(11) NOT NULL,
  `cantidad` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;
ALTER TABLE `bitacora_ins_servs_ventas` ADD CONSTRAINT `insumo_fk_venta` FOREIGN KEY (`id_insumo`) REFERENCES `productos`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;
ALTER TABLE `bitacora_ins_servs_ventas` CHANGE `id_venta` `id_venta` BIGINT(20) NOT NULL;
ALTER TABLE `bitacora_ins_servs_ventas` ADD CONSTRAINT `venta_fk_venta` FOREIGN KEY (`id_venta`) REFERENCES `venta_erp_detalle`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`, `orden`) VALUES (NULL, 'Mantenimientos', 'fa fa-gears', '10');
/*INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '10', 'Mttos. Externos', 'Mantenimientos', 'shopping-cart', '1', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '42', '1');*/

DROP TABLE IF EXISTS `mtto_externo`;
CREATE TABLE IF NOT EXISTS `mtto_externo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` tinyint(4) NOT NULL,
  `descrip` text COLLATE utf8_spanish_ci NOT NULL,
  `marca` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `modelo` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `serie` varchar(70) COLLATE utf8_spanish_ci NOT NULL,
  `id_sucursal` int(11) NOT NULL,
  `a_cuenta` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `observ` text COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  `id_personal` int(11) NOT NULL,
  `activo` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `mtto_externo` CHANGE `observ` `observ_mtto` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `mtto_externo` ADD `id_cliente` INT NOT NULL AFTER `id`;
ALTER TABLE `mtto_externo` ADD CONSTRAINT `cli_fk_mtto` FOREIGN KEY (`id_cliente`) REFERENCES `clientes`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;

DROP TABLE IF EXISTS `orden_trabajo`;
CREATE TABLE IF NOT EXISTS `orden_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mtto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observ` text COLLATE utf8_spanish_ci NOT NULL,
  `nom_solicita` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dir_solicita` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `col_solicita` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `cel_solicita` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tel_solicita` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `mail_solicita` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `rfc_solicita` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nom_contacto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tel_contacto` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `mtto_externo` ADD `id_venta` INT NOT NULL AFTER `id_personal`;
/* hasta acá cargado a productivo y a calidad 25-06-2024 */

ALTER TABLE `mtto_externo` ADD `fecha_serv` DATETIME NOT NULL AFTER `id_venta`, ADD `fecha_repara` DATETIME NOT NULL AFTER `fecha_serv`, ADD `fecha_sin_repara` DATETIME NOT NULL AFTER `fecha_repara`;
ALTER TABLE `bitacora_ins_servs_ventas` ADD `id_mtto` INT NOT NULL AFTER `id_venta`;

ALTER TABLE bitacora_ins_servs_ventas DROP FOREIGN KEY venta_fk_venta;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Vehiculos', 'Cat_complemento/vehiculos', NULL, '0', '0', '1', '42'), (NULL, '2', 'Carta porte', 'Cat_complemento/cat_carta_porte', NULL, '0', '0', '0', '42');

INSERT INTO `perfiles` (`perfilId`, `nombre`, `tipo_venta`) VALUES ('0', 'Técnico Mtto.', '0');
INSERT INTO `perfiles_detalles` (`perfilId`, `MenusubId`, `estatus`) VALUES (9, '3', 1);
INSERT INTO `perfiles_detalles` (`perfilId`, `MenusubId`, `estatus`) VALUES (9, '42', 1);

ALTER TABLE `perfiles` ADD `tipo_tecnico` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `tipo_venta`;
ALTER TABLE `personal` ADD `comision_tecnico` DECIMAL(8,2) NOT NULL AFTER `comision`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '5', 'Comisiones', 'Comisiones', NULL, '0', '0', '0', '0'), (NULL, '10', 'Mttos. Externos', 'Mantenimientos', 'shopping-cart', '1', '0', '0', '0');

ALTER TABLE mtto_externo DROP FOREIGN KEY cli_fk_mtto


ALTER TABLE `sucursal` ADD `con_insumos` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=incluye prods tipo insumo de sucursal propia' AFTER `orden`;
UPDATE `menu_sub` SET `submenutipo` = '0' WHERE `menu_sub`.`MenusubId` = 3;
UPDATE `menu_sub` SET `submenutipo` = '0' WHERE `menu_sub`.`MenusubId` = 41;

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '10', 'Mttos. Internos', 'Mttos_internos', 'fa fa-gears', '1', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '47', '1');


DROP TABLE IF EXISTS `mtto_interno`;
CREATE TABLE IF NOT EXISTS `mtto_interno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_equipo` int(11) NOT NULL,
  `idprod_ss` int(11) NOT NULL,
  `fecha_termino` date NOT NULL,
  `prox_mtto` date NOT NULL,
  `id_personal` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `observ` text COLLATE utf8_spanish_ci NOT NULL,
  `activo` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

DROP TABLE IF EXISTS `bitacora_insumos_mttosint`;
CREATE TABLE IF NOT EXISTS `bitacora_insumos_mttosint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mtto` int(11) NOT NULL,
  `id_insumo` int(11) NOT NULL,
  `id_prod_suc` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `id_personal` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `mtto_interno` ADD `id_sucursal` INT NOT NULL AFTER `observ`;
ALTER TABLE `bitacora_insumos_mttosint` ADD `fecha_del` DATETIME NOT NULL AFTER `id_personal`;
ALTER TABLE `mtto_interno` ADD `tipo` VARCHAR(1) NOT NULL AFTER `prox_mtto`;

DROP TABLE IF EXISTS `orden_trabajo_int`;
CREATE TABLE IF NOT EXISTS `orden_trabajo_int` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mtto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observ` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;



ALTER TABLE `mtto_externo` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `a_cuenta`;

DROP TABLE IF EXISTS `contrato_renta`;
CREATE TABLE IF NOT EXISTS `contrato_renta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_renta` int(11) NOT NULL,
  `en_pagare` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `el_pagare` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `cant_pagare` float NOT NULL,
  `porc_interes` float NOT NULL,
  `nom_pagare` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `dir_pagare` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `pobla_pagare` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `tel_pagare` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

DROP TABLE IF EXISTS `evidencia_mtto`;
CREATE TABLE IF NOT EXISTS `evidencia_mtto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mtto` int(11) NOT NULL,
  `imagen` text COLLATE utf8_spanish_ci NOT NULL,
  `id_personal` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

/* ////////////////////////////////////////// */
UPDATE `perfiles_detalles` SET estatus=0 where MenusubId=8
DELETE FROM `menu_sub` WHERE `menu_sub`.`MenusubId` = 8

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '5', 'Ventas', 'ReporteVentas', NULL, '0', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '48', '1');

ALTER TABLE `sucursal` CHANGE `orden` `orden` TINYINT NOT NULL;

/* hasta acá cargado a calidad 16-10-2024 */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '2', 'Ajustes', 'AjustesProducto', 'shopping-bag', '6', '0', '1', '24');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '51', '1');

DROP TABLE IF EXISTS `bitacora_ajustes`;
CREATE TABLE IF NOT EXISTS `bitacora_ajustes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_ps` int(11) NOT NULL,
  `id_sucursal` int(11) NOT NULL,
  `tipo_prod` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '0=stock,1=serie,2=lote,3=inusmo,4=herramienta',
  `cantidad` int(11) NOT NULL,
  `cantidad_ajuste` int(11) NOT NULL,
  `num_serie` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `serie_ajuste` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_personal` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `bitacora_ajustes` CHANGE `num_serie` `num_serie` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, CHANGE `serie_ajuste` `serie_ajuste` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

UPDATE `menu_sub` SET `Nombre` = 'Bitácora de Productos' WHERE `menu_sub`.`MenusubId` = 24;
ALTER TABLE `bitacora_ajustes` ADD `tipo_recarga` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `fecha_reg`;
/* hasta acá cargado a calidad 22-10-2024 */

ALTER TABLE `categoria` ADD `clave` VARCHAR(10) NOT NULL AFTER `categoria`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '4', 'Categorías', 'Categorias', 'home', '13', '0', '0', '0');

ALTER TABLE `productos` ADD `isr` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `iva_comp`, ADD `ieps` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `isr`;
ALTER TABLE `compra_erp` ADD `isr` DECIMAL(10,2) NOT NULL AFTER `iva`;
/* hasta acá cargado a calidad 27-11-2024 */

ALTER TABLE `garantias_detalle` CHANGE `retorno` `retorno` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '0' COMMENT '0=no,1=retornado a stock';

ALTER TABLE `garantias_detalle` ADD `coment_retorno` TEXT NOT NULL AFTER `carta_provee`;
ALTER TABLE `mtto_externo` ADD `comentarios` TEXT NOT NULL AFTER `fecha_sin_repara`;

/* **************************** */
ALTER TABLE `mtto_interno` ADD `id_renta` INT NOT NULL AFTER `id`;
ALTER TABLE `productos` ADD `concentrador` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `referencia`, ADD `tanque` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `concentrador`, ADD `capacidad` FLOAT NOT NULL AFTER `tanque`;


DROP TABLE IF EXISTS `productos_concentrador`;
CREATE TABLE IF NOT EXISTS `productos_concentrador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_prod` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` float NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos_concentrador`
--

INSERT INTO `productos_concentrador` (`id`, `codigo_prod`, `cantidad`, `estatus`) VALUES
(1, '008051', 1, '1'),
(2, '008256', 1, '1'),
(3, '008006', 1, '1');
COMMIT;


ALTER TABLE `productos_concentrador` CHANGE `codigo_prod` `codigo_prod` VARCHAR(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `compra_erp_detalle` ADD `cant_ini` INT NOT NULL AFTER `cantidad`;
ALTER TABLE `historial_transpasos` ADD `cant_ini` FLOAT NOT NULL AFTER `cantidad`;

ALTER TABLE `bitacora_ins_servs_ventas` ADD `asignado` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no,1=si' AFTER `cantidad`;
ALTER TABLE `bitacora_insumos_mttosint` ADD `cant_ini` FLOAT NOT NULL AFTER `cantidad`;
ALTER TABLE `operadores` ADD `tipoFigura` VARCHAR(2) NOT NULL AFTER `id`; 
/* hasta acá cargado a calidad 05-12-2024 */

ALTER TABLE `bitacora_ins_servs_ventas` ADD `cant_ini` FLOAT NOT NULL AFTER `cantidad`;

DROP TABLE IF EXISTS `bitacora_prods_conce`;
CREATE TABLE IF NOT EXISTS `bitacora_prods_conce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_prod` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `cant_ini` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

DROP TABLE IF EXISTS `bitacora_oxi_tanque`;
CREATE TABLE IF NOT EXISTS `bitacora_oxi_tanque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_recarga` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `cant_ini` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

DROP TABLE IF EXISTS `bitacora_devoluciones_detalles`;
CREATE TABLE IF NOT EXISTS `bitacora_devoluciones_detalles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bitacora` int(11) NOT NULL,
  `id_venta_det` int(11) NOT NULL,
  `cant_ini` float NOT NULL,
  `cantidad` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `garantias_detalle` ADD `cant_ini` FLOAT NOT NULL AFTER `cant`;
ALTER TABLE `f_facturas_servicios` ADD `consin_iva` INT NOT NULL COMMENT '0 exento, 1 con iva' AFTER `iva`;

/* ****************** */
ALTER TABLE `historial_transpasos` CHANGE `cant_ini_d` `cant_ini_d` FLOAT NULL DEFAULT '0';


/* ***************** */
ALTER TABLE `compra_erp_detalle` CHANGE `precio_unitario` `precio_unitario` DECIMAL(14,6) NOT NULL;
ALTER TABLE `recargas` CHANGE `precioc` `precioc` DECIMAL(12,6) NOT NULL;

/* ********************** */
INSERT INTO `perfiles` (`perfilId`, `nombre`, `tipo_venta`, `tipo_tecnico`, `estatus`) VALUES (NULL, 'Página web', '0', '0', '1');

/* ***************30-01-2025******************* */
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '5', 'Saldo Provee.', 'ReporteSaldo', 'shopping-cart', '3', '0', '0', '0');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`, `estatus`) VALUES (NULL, '1', '54', '1');
-------------------------------------------
ALTER TABLE `f_facturas_servicios` ADD `idproducto` INT NULL DEFAULT NULL COMMENT 'solo si aplica, para ventas' AFTER `consin_iva`;


/* ************************13-02-2025**************************************** */
ALTER TABLE `bitacora_prods_conce` ADD `id_renta` INT NOT NULL AFTER `id_venta`;
/* ************************26-02-2025**************************************** */
ALTER TABLE `f_facturas` ADD `LugarExpedicion` VARCHAR(10) NOT NULL AFTER `tim`;